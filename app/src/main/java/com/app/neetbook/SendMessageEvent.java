package com.app.neetbook;

import org.json.JSONObject;

public class SendMessageEvent {
    private String eventName;
    private JSONObject messageObject;
    public String getEventName() {
        return eventName;
    }
    public void setEventName(String eventName) {
        this.eventName = eventName;
    }
    public JSONObject getMessageObject() {
        return messageObject;
    }
    public void setMessageObject(JSONObject messageObject) {
        this.messageObject = messageObject;
    }
}
