package com.app.neetbook.Interfaces;

import com.app.neetbook.Model.Groupslist;
import com.app.neetbook.Model.TabSideMenu;

public interface OnGroupMenuSelectedListener {
    void onGroupMenuSelected(Groupslist tabSideMenu, int position);

}
