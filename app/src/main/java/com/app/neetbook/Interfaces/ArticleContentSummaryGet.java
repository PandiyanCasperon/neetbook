package com.app.neetbook.Interfaces;

public interface ArticleContentSummaryGet {
    void onArticleContentChanged(String summary,int CurrentPosition);
}
