package com.app.neetbook.Interfaces;

public interface StateSaver {
    void saveState(int adapterPosition, int itemPosition, double lastItemAngleShift);
}
