package com.app.neetbook.Interfaces;

import android.util.Log;

public interface TabArtcileChapterClickListener {
    void onChapterClicked(String ChapterName, String ChapterId, String strSuperHeadingName, String strHeadingName);
}
