package com.app.neetbook.Interfaces;

public interface OnTestAttemp {

    void onAnswered(String strQAndA,int count);
    void onSubmitted(String correctAnswer,String wrongAnswer,String questionAttented,String toHundred,double percentage);

}
