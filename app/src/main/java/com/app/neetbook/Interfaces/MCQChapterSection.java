package com.app.neetbook.Interfaces;

import com.app.neetbook.Model.HeadData;
import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.McqExam;
import com.app.neetbook.Model.SuperDetails;

import java.util.ArrayList;

public interface MCQChapterSection {


    boolean isMCQChapterHeader();
    String getName();

    int sectionPosition();
    ArrayList<HeadDetails> getDeadDeatails();
    ArrayList<McqExam> getMcqExamsList();
    String getMCQChapterName();
    String getMCQShortName();
    String getMCQLongrName();
    String getMCQImage();
    String getMCQCount();
    String getMCQChapterId();
    String getMCQIsSuperHeading();
    String getMCQSuperHeadName();
    String getMCQHeadId();
    String getMCQHeadName();
    String getMCQHeadShortName();
    String getMCQHeadLongName();




}
