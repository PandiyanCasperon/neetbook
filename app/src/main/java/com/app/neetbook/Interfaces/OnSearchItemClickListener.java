package com.app.neetbook.Interfaces;

public interface OnSearchItemClickListener {
    void onSearchItemClicked(int position, int menuType);
}
