package com.app.neetbook.Interfaces;

public interface FragmentRefreshListener {
    void onRefresh();
    void onRefreshResume();
}
