package com.app.neetbook.Interfaces;

import com.app.neetbook.Model.subscription.SubscriptionCart;

public interface OnDeleteItemFromCart {
    void onDelete(int position, SubscriptionCart priceList);
}
