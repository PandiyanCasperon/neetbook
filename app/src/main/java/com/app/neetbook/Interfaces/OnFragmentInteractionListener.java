package com.app.neetbook.Interfaces;

import com.app.neetbook.Model.TabSideMenu;

public interface OnFragmentInteractionListener {
    public void onSideMenuSelected(TabSideMenu tabSideMenu,int position);
}
