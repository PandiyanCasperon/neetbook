package com.app.neetbook.Interfaces;

public interface OnSubjectsClickListener {
    void onSubscribed(String id,int position);
    void onRead(String id,int position);
    void onScrollEnd(int position);
}
