package com.app.neetbook.Interfaces;

public interface OnItemCycleMenuStateChangedListener {
    void onOpen(int position);

    void onClose(int position);
}
