package com.app.neetbook.Interfaces;

public interface OnHeadingClickListener {
    void onHeadingClicked(int parentPosition, int childPosition, String headId);
}
