package com.app.neetbook.Interfaces;

public interface Updateable {
    public void update();
}
