package com.app.neetbook.Interfaces;

public interface OnSubmitListioner {
    void onSubmit();
    void onBackPressed();
}
