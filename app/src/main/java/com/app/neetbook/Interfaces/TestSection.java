package com.app.neetbook.Interfaces;

import com.app.neetbook.Model.TestList;

import java.util.ArrayList;

public interface TestSection {
    boolean isTestHeader();
    String getName();

    int sectionPosition();

    String getChapteId();
    String getchapterName();
    String getShortName();
    String getLongName();
    String getImage();
    String getIsFullTest();
    String getTestName();
    String getatestId();
    String getCorrectAnswerMark();
    String getWrongAnswerMark();
    String getQuestionType();
    String getNumOfQuestion();
    String getNumOfChoice();
    String getInstruction();
    String getType();
    String isFinished();
    String getCount();
    String getFullTestCount();
    ArrayList<TestList> getTestArrayList();
}
