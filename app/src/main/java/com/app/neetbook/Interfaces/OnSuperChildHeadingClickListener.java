package com.app.neetbook.Interfaces;

public interface OnSuperChildHeadingClickListener {
    void onSuperChildClicked(int position, String headId);
}
