package com.app.neetbook.Interfaces;

public interface NetworkStateReceiverListener {
    public void networkAvailable();
    public void networkUnavailable();
}
