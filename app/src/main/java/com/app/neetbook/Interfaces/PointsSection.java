package com.app.neetbook.Interfaces;

import com.app.neetbook.Model.PointsContent;

import java.util.ArrayList;

public interface PointsSection {
    //public String _id;
    //  public String chapt_name;
    //  public String long_name;
    //  public String short_name;
    //  public String images;
    boolean isHeader();
    String getCount();
    int sectionPosition();
    String getId();
    String getChaptName();
    String getContent();
    String getLongName();
    String getShortName();
    String getImage();
    ArrayList<PointsContent> getPointsContent();
}
