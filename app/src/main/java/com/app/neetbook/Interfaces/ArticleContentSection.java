package com.app.neetbook.Interfaces;

import java.util.ArrayList;

public interface ArticleContentSection {

    String points();
    int sectionPosition();

   String summary();



   String isHeader();

   String titile();

   ArrayList<String> strImages();
}
