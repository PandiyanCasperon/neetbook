package com.app.neetbook.Interfaces;

public interface Section {
    boolean isHeader();
    String getName();

    int sectionPosition();

}
