package com.app.neetbook.Interfaces;

public interface ItemClickListener {

    public void onParentItemClick(int position);
}
