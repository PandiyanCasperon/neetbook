package com.app.neetbook.Interfaces;

import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.McqExam;

import java.util.ArrayList;

public interface ArticleHeaderSestion {
    boolean isHeader();
    String getName();
    String getShortName();
    String getLongName();
    String getId();
    String getImage();
    ArrayList<HeadDetails> getHeadList();
    ArrayList<McqExam> getExamArrayList();
    String getIsSuperHeading();
    String getHead();
    String getFirsthead();
    String getChapterId();
    String getCount();
    int sectionPosition();
    String getHeaderName();
    Boolean is_expanded();
}
