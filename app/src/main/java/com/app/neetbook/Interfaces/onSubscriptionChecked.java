package com.app.neetbook.Interfaces;

import com.app.neetbook.Model.subscription.Subscription;

public interface onSubscriptionChecked {
    void onItemChecked(boolean checked, String packageId, Subscription subscription,int SelectedPackage,int position);
}
