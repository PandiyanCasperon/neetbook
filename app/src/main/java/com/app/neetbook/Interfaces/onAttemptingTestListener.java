package com.app.neetbook.Interfaces;

public interface onAttemptingTestListener {

    void onSelectedAnswer(int position, String Answer);
    void onMultiChoiceSelectedAnswer(int position, String Answer);
    void onImageClicked(int position);

}
