package com.app.neetbook.PlaystoreUpate;

public interface WSCallerVersionListener {
    public void onGetResponse(boolean isUpdateAvailable);
}