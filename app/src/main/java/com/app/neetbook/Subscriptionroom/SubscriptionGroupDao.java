package com.app.neetbook.Subscriptionroom;

import com.app.neetbook.Model.subscription.SubscriptionCart;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface SubscriptionGroupDao {
    @Query("SELECT * FROM SubscriptionGroupNames")
    List<SubscriptionGroups> getAll();

    @Query("SELECT COUNT(*) from SubscriptionGroupNames")
    int countCart();

    @Query("SELECT * FROM SubscriptionGroupNames WHERE GroupId == :id")
    public abstract SubscriptionGroups getGroupItem(String id);

    @Insert(onConflict = REPLACE)
    void insertAll(SubscriptionGroups... users);

    @Update
    void update(SubscriptionGroups repos);

    @Query("DELETE FROM SubscriptionGroupNames WHERE GroupId = :cartId")
    abstract void deleteByCartId(String cartId);

    @Delete
    void delete(SubscriptionGroups user);

    @Query("DELETE FROM SubscriptionGroupNames")
    public void nukeTable();
}
