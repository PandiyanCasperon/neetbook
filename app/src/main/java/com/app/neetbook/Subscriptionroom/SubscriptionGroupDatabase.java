package com.app.neetbook.Subscriptionroom;


import android.content.Context;

import com.app.neetbook.CartRoom.CartConverters;
import com.app.neetbook.CartRoom.CartDataBase;
import com.app.neetbook.CartRoom.CartListDao;
import com.app.neetbook.Model.subscription.SubscriptionCart;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = SubscriptionGroups.class,exportSchema =  false,version = 1)
public abstract  class SubscriptionGroupDatabase extends RoomDatabase {
    private static SubscriptionGroupDatabase INSTANCE;

    public abstract SubscriptionGroupDao subscriptionGroupDao();

    public static SubscriptionGroupDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), SubscriptionGroupDatabase.class, "SubscriptionGroupDatabase")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
