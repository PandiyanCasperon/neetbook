package com.app.neetbook.Subscriptionroom;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "SubscriptionGroupNames")
public class SubscriptionGroups {

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    @PrimaryKey(autoGenerate = true)
    private int cid;


    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    @ColumnInfo(name = "GroupId")
    String groupid;
    @ColumnInfo(name = "GroupName")
    String groupname;
    @ColumnInfo(name = "ShortName")
    String shortName;
    @ColumnInfo(name = "LongName")
    String longName;
}
