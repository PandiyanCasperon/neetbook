package com.app.neetbook.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.neetbook.Interfaces.ArticleHeaderSestion;
import com.app.neetbook.Model.NewSuperListPojo;
import com.app.neetbook.R;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.widgets.CustomTextView;
import com.app.neetbook.View.ScrollingActivity;
import com.app.neetbook.View.SideMenu.TabMainActivity;

import java.util.ArrayList;

public class NewArticleSectionAdapter extends RecyclerView.Adapter<NewArticleSectionAdapter.ViewHolder> {
    ArrayList<ArticleHeaderSestion> sectionHeaderArticleList;
    private ArrayList<NewSuperListPojo> temArrayList;
    SessionManager sessionManager;
    Context context;
    int parentPosition = 0;
    ArticleHeaderSestion mcqChapterSection;
    String strSubjectId, strSubjectShortName;
    setOnItemClick setOnItemClick;
    String findCategoryList = "";

    public NewArticleSectionAdapter(Context context, setOnItemClick setOnItemClick, ArrayList<NewSuperListPojo> temArrayList, ArrayList<ArticleHeaderSestion> sectionHeaderArticleList, int parentPosition, String strSubjectId, String strSubjectShortName, String findCategoryList) {
        this.temArrayList = temArrayList;
        this.sectionHeaderArticleList = sectionHeaderArticleList;
        this.parentPosition = parentPosition;
        this.strSubjectId = strSubjectId;
        this.context = context;
        this.strSubjectShortName = strSubjectShortName;
        this.setOnItemClick = setOnItemClick;
        this.findCategoryList = findCategoryList;
        sessionManager=new SessionManager(context);
        if (findCategoryList.equals(""))
            this.findCategoryList=sessionManager.getFindCategoryList();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.subscription_recycler_layout, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        if (temArrayList.get(position).getIsSH().equals("0")) {
            if (findCategoryList != null && !findCategoryList.equalsIgnoreCase("") && findCategoryList.equalsIgnoreCase(temArrayList.get(position).getHead_id())) {
                holder.findTitleImageView.setVisibility(View.VISIBLE);
                holder.subList.setText(temArrayList.get(position).getLong_name());
                holder.subList.setTextColor(context.getResources().getColor(R.color.artical_pink));
            } else
                holder.subList.setText(temArrayList.get(position).getLong_name());
        } else if (temArrayList.get(position).getIsSH().equals("1")) {
            holder.subList.setPaintFlags(holder.subList.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            holder.subList.setText(temArrayList.get(position).getLong_name());
        } else if (temArrayList.get(position).getIsSH().equals("11")) {
            if (findCategoryList != null && !findCategoryList.equalsIgnoreCase("") && findCategoryList.equalsIgnoreCase(temArrayList.get(position).getHead_id())) {
                holder.findTitleImageView.setVisibility(View.VISIBLE);
                holder.subList.setText(temArrayList.get(position).getLong_name());
                holder.subList.setTextColor(context.getResources().getColor(R.color.artical_pink));
                holder.subList.setTypeface(holder.subList.getTypeface(), Typeface.ITALIC);
            } else {
                holder.subList.setTypeface(holder.subList.getTypeface(), Typeface.ITALIC);
                holder.subList.setText(temArrayList.get(position).getLong_name());
            }

        }
        holder.subList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!temArrayList.get(position).getIsSH().equals("1")) {
                    sessionManager.setLastReadDetails(temArrayList.get(position).getHead_id());
                    setOnItemClick.onItemClick(context.getResources().getString(R.string.closedialog));
                    TabMainActivity.isReadPage = true;
                    mcqChapterSection = sectionHeaderArticleList.get(parentPosition);
                    ScrollingActivity.headDataArrayList = mcqChapterSection.getHeadList();
                    context.startActivity(new Intent(context, ScrollingActivity.class).putExtra("chapter_name", temArrayList.get(position).getChapter_name()).putExtra("chapter_id", temArrayList.get(position).getChapter_id()).putExtra("head_id", temArrayList.get(position).getHead_id()).putExtra("subject_id", strSubjectId).putExtra("subject_name", strSubjectShortName).putExtra("chapter_name", temArrayList.get(position).getChapter_name()).putExtra("superHeading", "No").putExtra("chapter_short_name",  sectionHeaderArticleList.get(parentPosition).getShortName()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return temArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        CustomTextView subList;
        ImageView findTitleImageView;

        public ViewHolder(View view) {
            super(view);
            subList = view.findViewById(R.id.list_title);
            findTitleImageView = view.findViewById(R.id.findTitleImageView);
        }

    }

    public interface setOnItemClick {
        void onItemClick(String message);
    }


}
