package com.app.neetbook.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.neetbook.Interfaces.TestSection;
import com.app.neetbook.Model.TestActivityLastReadDetails;
import com.app.neetbook.Model.sidemenuFromContent.CategoryTest;
import com.app.neetbook.R;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.widgets.CustomTextView;
import com.app.neetbook.View.SideMenu.SubscriptionTestFragment;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.app.neetbook.View.SideMenu.collMainActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class NewTestSectionAdapter extends RecyclerView.Adapter<NewTestSectionAdapter.ViewHolder> {

    SetOnChildClickListener setOnChildClickListener;
    ArrayList<CategoryTest> mList;
    String strSubjectId, strSubjectShortName, strPendingTestId;
    int parentPosition, length;
    ArrayList<TestSection> sectionArrayList;
    Activity context;

    TestSection testSection;
    int childPosition;
    SessionManager sessionManager;
    String roodId;

    public NewTestSectionAdapter(Activity context, SetOnChildClickListener setOnChildClickListener, ArrayList<CategoryTest> mList, String strSubjectId, String strSubjectShortName, String strPendingTestId, int parentPosition, int length, ArrayList<TestSection> sectionArrayList, int childPosition) {
        this.context = context;
        this.setOnChildClickListener = setOnChildClickListener;
        this.mList = mList;
        this.strSubjectShortName = strSubjectShortName;
        this.strSubjectId = strSubjectId;
        this.parentPosition = parentPosition;
        this.length = length;
        this.sectionArrayList = sectionArrayList;
        sessionManager=new SessionManager(context);
        this.childPosition = childPosition;
        this.strPendingTestId = strPendingTestId;
    }

    @NonNull
    @Override
    public NewTestSectionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.subscription_recycler_layout, parent, false);
        NewTestSectionAdapter.ViewHolder holder = new NewTestSectionAdapter.ViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull NewTestSectionAdapter.ViewHolder holder, int position) {

        try {
            holder.subList.setText(mList.get(parentPosition).testArrayList.get(position).long_name);
            holder.subList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (/*!SubscriptionTestFragment.isTestIdSaved.equals("")*/sessionManager.getBookmarkDetails().get(SessionManager.BOOKMARK_TITLE).equalsIgnoreCase("Test")) {

                        TestActivityLastReadDetails testActivityLastReadDetails=new TestActivityLastReadDetails(sessionManager.getIncompleteTest().get(SessionManager.SUBJECT_ID),sessionManager.getIncompleteTest().get(SessionManager.CHAPTER_ID),sessionManager.getIncompleteTest().get(SessionManager.CHAPTER_NAME1),sessionManager.getIncompleteTest().get(SessionManager.SUBJECT_NAME1),"1",
                                sessionManager.getIncompleteTest().get(SessionManager.PENDINGTESTID),sessionManager.getIncompleteTest().get(SessionManager.TESTID),sessionManager.getIncompleteTest().get(SessionManager.TESTTYPE),sessionManager.getIncompleteTest().get(SessionManager.NO_OF_QUESTION),sessionManager.getIncompleteTest().get(SessionManager.IS_FINISHED),
                                sessionManager.getIncompleteTest().get(SessionManager.QUESTIONTYPE),sessionManager.getIncompleteTest().get(SessionManager.STRINGSTRUCTION),sessionManager.getIncompleteTest().get(SessionManager.STRLONGNAME),sessionManager.getIncompleteTest().get(SessionManager.CRT_ANS_MARK),sessionManager.getIncompleteTest().get(SessionManager.WRNG_ANS_MARK));


                        EventBus.getDefault().post("testpending");
                        EventBus.getDefault().post(testActivityLastReadDetails);


                    }
                    else {
                        setOnChildClickListener.OnChildItemClickListener(context.getResources().getString(R.string.closedialog));
                        testSection = sectionArrayList.get(childPosition);
                        TabMainActivity.isReadPage = true;
                        SubscriptionTestFragment.isPending = "-1";
                        context.startActivity(new Intent(context, collMainActivity.class)
                                .putExtra("subject_id", strSubjectId)
                                .putExtra("chapter_id", testSection.getChapteId())
                                .putExtra("chapter_name", testSection.getShortName())
                                .putExtra("subject_name", strSubjectShortName)
                                .putExtra("pending", "0")
                                .putExtra("pendingTestId", "")
                                .putExtra("testId", testSection.getTestArrayList().get(position)._id)
                                .putExtra("type", testSection.getTestArrayList().get(position).type)
                                .putExtra("no_of_question", testSection.getTestArrayList().get(position).no_of_qstn)
                                .putExtra("isFinished", testSection.getTestArrayList().get(position).is_finished)
                                .putExtra("questionType", testSection.getTestArrayList().get(position).qstn_type)
                                .putExtra("strInstruction", testSection.getTestArrayList().get(position).instruction)
                                .putExtra("strLongName", testSection.getTestArrayList().get(position).long_name)
                                .putExtra("crt_ans_mark", testSection.getTestArrayList().get(position).crt_ans_mark)
                                .putExtra("wrng_ans_mark", testSection.getTestArrayList().get(position).wrng_ans_mark)
                        );
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return length;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        CustomTextView subList;

        public ViewHolder(View view) {
            super(view);
            subList = view.findViewById(R.id.list_title);
        }

    }

    public interface SetOnChildClickListener {
        void OnChildItemClickListener(String message);
    }

}
