package com.app.neetbook.Adapter.sideMenuContent;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.app.neetbook.Model.sidemenuFromContent.SubscriptionPointsBody;
import com.app.neetbook.Model.sidemenuFromContent.SubscriptionPointsHeader;
import com.app.neetbook.R;
import com.app.neetbook.Utils.widgets.CustomTextViewBold;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

public class SubscriptionPointsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private static final int HEADER = 0;
  private static final int ITEM = 1;
  private Context context;
  private List<Object> subscriptionList;
  Typeface tfBold,tfMedium,tfRegular;
  public SubscriptionPointsAdapter(Context context,
      ArrayList<Object> subscriptionList) {
    this.context = context;
    this.subscriptionList = subscriptionList;
    tfBold  = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaBold.otf");
    tfMedium  = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaSemibold.otf");
    tfRegular  = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaRegular.otf");
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    RecyclerView.ViewHolder viewHolder = null;
    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

    switch (viewType) {
      case ITEM:
        View view = layoutInflater.inflate(R.layout.mobile_subscription_points_item, parent, false);
        viewHolder = new PointsViewHolder(view);
        break;

      case HEADER:
        View viewHeader = layoutInflater
            .inflate(R.layout.mobile_subscription_points_header, parent, false);
        viewHolder = new HeaderViewHolder(viewHeader);
        break;
    }
    return viewHolder;
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    switch (getItemViewType(position)) {
      case HEADER:
        final HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
        SubscriptionPointsHeader item = (SubscriptionPointsHeader) subscriptionList.get(position);
        headerViewHolder.headerText.setText(item.header);
        headerViewHolder.headerText
            .setTextColor(context.getResources().getColor(R.color.subscription_green));
        break;

      case ITEM:
        final PointsViewHolder pointsViewHolder = (PointsViewHolder) holder;
        SubscriptionPointsBody itemBody = (SubscriptionPointsBody) subscriptionList.get(position);
        pointsViewHolder.title.setText(itemBody.points);
        break;
    }
  }

  @Override
  public int getItemCount() {
    return subscriptionList.size();
  }

  @Override
  public int getItemViewType(int position) {
    if (subscriptionList.get(position) instanceof SubscriptionPointsHeader) {
      return HEADER;
    } else if (subscriptionList.get(position) instanceof SubscriptionPointsBody) {
      return ITEM;
    }
    return -1;
  }


  class PointsViewHolder extends RecyclerView.ViewHolder {

    private AppCompatTextView title;

    private PointsViewHolder(@NonNull final View itemView) {
      super(itemView);
      title = itemView.findViewById(R.id.points_text);
    }
  }

  public class HeaderViewHolder extends RecyclerView.ViewHolder {

    CustomTextViewBold headerText;

    public HeaderViewHolder(View itemView) {
      super(itemView);
      headerText = itemView.findViewById(R.id.points_header_text);
    }
  }

}
