package com.app.neetbook.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.neetbook.Interfaces.ArticleHeaderSestion;
import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.Interfaces.MCQChapterSection;
import com.app.neetbook.Interfaces.PointsSection;
import com.app.neetbook.R;
import com.app.neetbook.View.SideMenu.SubscriptionDetailActivity;
import com.app.neetbook.View.SideMenu.SubscriptionPointsActivity;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.shuhart.stickyheader.StickyAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class PointsSectionAdapter extends StickyAdapter<RecyclerView.ViewHolder, RecyclerView.ViewHolder> {

    private ParentClickListener parentClickListener;
    private ArrayList<PointsSection> sectionArrayList;
    private static final int LAYOUT_HEADER= 0;
    private static final int LAYOUT_CHILD= 1;
    private int currentSelectedPosition = -1;
    private int chileSeletedPosition = -1;
    private int superChileSeletedPosition = -1;
    private String currentSelectedHeader,indexCount = "1";
    Context context;
    private String strSubId = "",strSubName = "";
    private int index = 0;
    private ItemClickListener itemClickListener;
    public PointsSectionAdapter(Context context, ParentClickListener parentClickListener,ArrayList<PointsSection> sectionArrayList, String strSubId, String strSubName, int currentSelectedPosition, int chileSeletedPosition){

        // inflater = LayoutInflater.from(context);
        this.context = context;
        this.sectionArrayList = sectionArrayList;
        this.strSubId = strSubId;
        this.strSubName = strSubName;
        this.currentSelectedPosition = currentSelectedPosition;
        //this.chileSeletedPosition = chileSeletedPosition;
        currentSelectedHeader = sectionArrayList.get(0).getLongName();
        this.parentClickListener=parentClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (viewType == LAYOUT_HEADER ) {
            return new HeaderViewholder(inflater.inflate(R.layout.points_header_item, parent, false));
        }else {
            return new ItemViewHolder(inflater.inflate(R.layout.points_child_parent_layout, parent, false));
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {


        if (sectionArrayList.get(position).isHeader()) {
            HeaderViewholder headerViewholder = ((HeaderViewholder) holder);

            ((HeaderViewholder) holder).title.setText( sectionArrayList.get(position).getLongName());
            ((HeaderViewholder) holder).textView17.setText( sectionArrayList.get(position).getCount());

            if(position!=0){

                if(sectionArrayList.get(position).sectionPosition()== currentSelectedPosition) {

                    ((HeaderViewholder) holder).ivExpand.setImageResource(R.drawable.ic_arrow_up_article);

                } else {

                    ((HeaderViewholder) holder).ivExpand.setImageResource(R.drawable.ic_arow_down_article);
                }
            }




            Picasso.with(context).load(IConstant.BaseUrl+sectionArrayList.get(position).getImage()).into(headerViewholder.imageView14);
        } else {
            ItemViewHolder itemViewHolder = ((ItemViewHolder) holder);
            if(sectionArrayList.get(position).sectionPosition()== currentSelectedPosition) {

               // setChileView(sectionArrayList.get(position), position, itemViewHolder,0);
            }else
            {

                itemViewHolder.llChildHead.setVisibility(View.GONE);
                //collapse( itemViewHolder.llChildHead);
            }

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentClickListener.onItemClickListener(sectionArrayList.get(position).getId());
             /*   if(currentSelectedPosition!= sectionArrayList.get(position).sectionPosition()) {
                    currentSelectedPosition = sectionArrayList.get(position).sectionPosition();
                    chileSeletedPosition = -1;
                    itemClickListener.onParentItemClick(position);
                }else
                    currentSelectedPosition = -1;
                notifyDataSetChanged();*/
            }
        });
    }
    private void setChileView(final  PointsSection mcqChapterSection, final int position, final ItemViewHolder itemViewHolder,int open_position) {

        if(sectionArrayList.get(position).sectionPosition()== currentSelectedPosition) {
            //itemViewHolder.llChildHead.setVisibility(View.VISIBLE);
            itemViewHolder.llChildHead.removeAllViews();
            if(mcqChapterSection.getPointsContent().size()>0)
            {
                for(int i=0;i<mcqChapterSection.getPointsContent().size();i++)
                {
                    View ChildHeadView = LayoutInflater.from(context).inflate(R.layout.points_child_item,null);
                    ImageView imgArticleChildHead = ChildHeadView.findViewById(R.id.imgArticleChildHead);
                    TextView txt_article_child_head = ChildHeadView.findViewById(R.id.txt_article_child_head);
                    LinearLayout llParent = ChildHeadView.findViewById(R.id.llParent);

                    imgArticleChildHead.setVisibility(i== chileSeletedPosition?View.VISIBLE:View.INVISIBLE);
                    txt_article_child_head.setTextColor(i== chileSeletedPosition?context.getResources().getColor(R.color.points_green):context.getResources().getColor(R.color.black));
                    txt_article_child_head.setText(mcqChapterSection.getPointsContent().get(i).getLong_name());
                    final int finalI = i;
                    llParent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            chileSeletedPosition = finalI;
                            setChileView(mcqChapterSection,position,itemViewHolder,1);
                            //notifyDataSetChanged();
                            TabMainActivity.isReadPage = true;
                            SubscriptionPointsActivity.pointsContentArrayList = mcqChapterSection.getPointsContent();
                            context.startActivity(new Intent(context, SubscriptionPointsActivity.class).putExtra("point_id",mcqChapterSection.getPointsContent().get(finalI).get_id()).putExtra("chapter_id",mcqChapterSection.getId()).putExtra("subject_id",strSubId).putExtra("chapter_name",sectionArrayList.get(position).getShortName()).putExtra("subject_name",strSubName).putExtra("points",mcqChapterSection.getPointsContent()));
                        }
                    });
                    itemViewHolder.llChildHead.addView(ChildHeadView);

                    if(sectionArrayList.size()-1==position){

                        itemViewHolder.llChildHead.setVisibility(View.VISIBLE);

                    }else{

                        if(open_position==0){

                            expand(itemViewHolder.llChildHead);
                        }


                    }

                }
            }
        }else
        {

        }

    }
    @Override
    public int getItemViewType(int position) {
        if(sectionArrayList.get(position).isHeader()) {
            return LAYOUT_HEADER;
        }else
            return LAYOUT_CHILD;
    }

    @Override
    public int getItemCount() {
        return sectionArrayList.size();
    }

    @Override
    public int getHeaderPositionForItem(int itemPosition) {
        Log.d("seeee",""+itemPosition+"......"+sectionArrayList.get(itemPosition).sectionPosition());
        return sectionArrayList.get(itemPosition).sectionPosition();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int headerPosition) {
        HeaderViewholder headerViewholder = ((HeaderViewholder) holder);

        Picasso.with(context).load(IConstant.BaseUrl+sectionArrayList.get(index).getImage()).into(headerViewholder.imageView14);
        headerViewholder.title.setText(currentSelectedHeader);
        headerViewholder.textView17.setText(indexCount);

        if (sectionArrayList.get(headerPosition).sectionPosition() == currentSelectedPosition) {

            ((HeaderViewholder) holder).ivExpand.setImageResource(R.drawable.ic_arrow_up_article);

        } else {

            ((HeaderViewholder) holder).ivExpand.setImageResource(R.drawable.ic_arow_down_article);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return createViewHolder(parent, LAYOUT_HEADER);
    }

    public static class HeaderViewholder extends RecyclerView.ViewHolder {
        private TextView textView17,title;
        private ImageView imageView14;
        private ConstraintLayout pointsItem;
        private LinearLayoutCompat llChildHead;
        private ConstraintLayout llView;
        ImageView ivCollapse,ivExpand;
        HeaderViewholder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tvCategoryTitle);
            textView17 = itemView.findViewById(R.id.textView17);
            imageView14 = itemView.findViewById(R.id.imageView14);
            pointsItem = itemView.findViewById(R.id.pointsItem);
            ivCollapse = itemView.findViewById(R.id.ivCollapse);
            ivExpand = itemView.findViewById(R.id.ivExpand);
        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {


        private LinearLayoutCompat llChildHead;
        private RelativeLayout rlParent;
        ItemViewHolder(View itemView) {
            super(itemView);
            llChildHead = itemView.findViewById(R.id.llChildHead);

        }
    }

    public void setTopItemView(String currentSelectedHeader,String indexCount,int positon)
    {
        this.currentSelectedHeader = currentSelectedHeader;
        this.indexCount = indexCount;
        this.index = positon;
    }
    public void CollapseExpandedView(int position)
    {
        this.currentSelectedPosition = position;
        //notifyDataSetChanged();
    }

    public void setOnItemClickListener(ItemClickListener itemClickListener)
    {
        this.itemClickListener = itemClickListener;
    }

    public void expand(final View v) {


        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? WindowManager.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Expansion speed of 1dp/ms
        //  a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        int s= (int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density);
        System.out.println("value"+s);
        a.setDuration(500);
        v.startAnimation(a);
    }


    public void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Collapse speed of 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);

    }

    public  interface ParentClickListener{
        void onItemClickListener(String rootId);
    }
}
