package com.app.neetbook.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.neetbook.Model.TestScores;
import com.app.neetbook.R;
import com.app.neetbook.Utils.widgets.CustomBoldTextview;
import com.app.neetbook.Utils.widgets.CustomRegularTextview;

import java.util.List;

public class TestScoreAdapter extends
        RecyclerView.Adapter<TestScoreAdapter.ViewHolder> {

    private List<TestScores> mTestScores;
    private Context mContext;
    // Pass in the contact array into the constructor
    public TestScoreAdapter(List<TestScores> testScores, Context context) {
        mTestScores = testScores;
        mContext = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CustomBoldTextview testScore;
        private CustomRegularTextview testScoreTitle;
        private ImageView testScoreIcon;

        private ViewHolder(View itemView) {
            super(itemView);

            testScore = itemView.findViewById(R.id.testBoard_score);
            testScoreTitle = itemView.findViewById(R.id.testBoard_title);
            testScoreIcon = itemView.findViewById(R.id.testBoard_icon);
        }
    }

    @Override
    public TestScoreAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.item_test_question, parent, false);
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TestScoreAdapter.ViewHolder viewHolder, int position) {
        // Get the data model based on position
        TestScores testScores = mTestScores.get(position);

        viewHolder.testScore.setText(testScores.getScore());
        String percent = (testScores.getPercent().isEmpty() || testScores.getPercent() == null) ? "" : "("+testScores.getPercent()+")";
        viewHolder.testScoreTitle.setText(testScores.getTitle() + percent);
        Drawable icon=null;
        switch (mTestScores.get(position).getTitle()){
            case "Correct Answers":
                icon = mContext.getResources().getDrawable(R.drawable.ic_check_mark);
                break;
            case "Wrong Answers":
                icon = mContext.getResources().getDrawable(R.drawable.ic_delete);
                break;
            case "Score":
                icon = mContext.getResources().getDrawable(R.drawable.ic_happy);
                break;
        }

        if(icon == null){
            viewHolder.testScoreIcon.setVisibility(View.GONE);
        }else{
            viewHolder.testScoreIcon.setImageDrawable(icon);
        }
    }

    @Override
    public int getItemCount() {
        if(mTestScores == null){
            return 0;
        }
        return mTestScores.size();
    }

}
