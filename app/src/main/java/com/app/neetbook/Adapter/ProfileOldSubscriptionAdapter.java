package com.app.neetbook.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.neetbook.Model.GroupChildBean;
import com.app.neetbook.R;
import com.app.neetbook.serviceRequest.IConstant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ProfileOldSubscriptionAdapter extends RecyclerView.Adapter<ProfileOldSubscriptionAdapter.ViewHolder>{
    ArrayList<GroupChildBean> groupChildBeanArrayList;
    Context context;
//    OnGroupChildClick onChildClick;

    public ProfileOldSubscriptionAdapter(ArrayList<GroupChildBean> groupChildBeanArrayList, Context context) {

        this.groupChildBeanArrayList = groupChildBeanArrayList;
        this.context = context;
    }
    @NonNull
    @Override
    public ProfileOldSubscriptionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.old_subscription_adapter, parent, false);

        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileOldSubscriptionAdapter.ViewHolder holder, int position) {
        GroupChildBean groupChildBean = groupChildBeanArrayList.get(position);
        if(holder.llBottom!= null)
            holder.llBottom.setVisibility(position==groupChildBeanArrayList.size()-1?View.VISIBLE:View.GONE);
        if(holder.llTop!= null)
            holder.llTop.setVisibility(position==0?View.VISIBLE:View.GONE);
        holder.txtTitleProfileSubscription.setText(groupChildBean.getStrTitle());
        holder.txtDaysRemainingProfileSubscription.setText(groupChildBean.getStrRemainingDays());
        holder.txtHourRemainingProfileSubscription.setText(groupChildBean.getStrRemainingHours());
        holder.txtPricceProfileSubscription.setText(context.getString(R.string.currency_symbol)+" "+groupChildBean.getStrPrice());
        Picasso.with(context).load(IConstant.BaseUrl+groupChildBean.getStrImages()).placeholder(R.drawable.img_placeholder).into(holder.imgProfileSubscription);
    }

    @Override
    public int getItemCount() {
        return groupChildBeanArrayList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtDaysRemainingProfileSubscription,txtHourRemainingProfileSubscription,txtTitleProfileSubscription,txtPricceProfileSubscription;
        ImageView imgProfileSubscription;
        LinearLayout llBottom,llTop;
        public ViewHolder(View childView) {
            super(childView);

            txtDaysRemainingProfileSubscription = childView.findViewById(R.id.txtDaysRemainingProfileSubscription);
            txtHourRemainingProfileSubscription = childView.findViewById(R.id.txtHourRemainingProfileSubscription);
            txtTitleProfileSubscription = childView.findViewById(R.id.txtTitleProfileSubscription);
            imgProfileSubscription = childView.findViewById(R.id.imgProfileSubscription);
            txtPricceProfileSubscription = childView.findViewById(R.id.txtPricceProfileSubscription);
            llBottom = childView.findViewById(R.id.llBottom);
            llTop = childView.findViewById(R.id.llTop);
        }
    }
}
