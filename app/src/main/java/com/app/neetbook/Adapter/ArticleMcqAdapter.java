package com.app.neetbook.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.neetbook.Model.MCQExamIds;
import com.app.neetbook.R;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.serviceRequest.IConstant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ArticleMcqAdapter extends RecyclerView.Adapter<ArticleMcqAdapter.ViewHolder>{

    Context context;
    //    OnGroupChildClick onChildClick;
    ArrayList<MCQExamIds> mcqExamIdsArrayList;
    Typeface tfBold,tfMedium,tfRegular;
    SessionManager sessionManager;
    public ArticleMcqAdapter(ArrayList<MCQExamIds> mcqExamIdsArrayList, Context context) {

        this.mcqExamIdsArrayList = mcqExamIdsArrayList;
        this.context = context;
        sessionManager = new SessionManager(this.context);
        tfBold  = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium  = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular  = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaRegular.otf");
    }
    @NonNull
    @Override
    public ArticleMcqAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.article_mcq_exam_adapter, parent, false);

        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleMcqAdapter.ViewHolder holder, int position) {
        if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
            holder.txt_ref_link.setTextAppearance(context,R.style.textViewSmallArticleInfo);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
            holder.txt_ref_link.setTextAppearance(context,R.style.textViewMediumArticleInfo);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
            holder.txt_ref_link.setTextAppearance(context,R.style.textViewLargeArticleInfo);
        }
        holder.txt_ref_link.setTypeface(tfMedium);
        holder.txt_ref_link.setText(mcqExamIdsArrayList.get(position).getExam_name()+" "+mcqExamIdsArrayList.get(position).getYear_name());
    }

    @Override
    public int getItemCount() {
        return mcqExamIdsArrayList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_ref_link;

        public ViewHolder(View childView) {
            super(childView);

            txt_ref_link = childView.findViewById(R.id.txt_ref_link);

        }
    }
}
