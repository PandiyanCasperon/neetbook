package com.app.neetbook.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.neetbook.Interfaces.MCQChapterSection;
import com.app.neetbook.R;
import com.app.neetbook.Utils.widgets.CustomTextView;
import com.app.neetbook.View.McqMainActivity;
import com.app.neetbook.View.SideMenu.SubscriptionDetailActivity;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.app.neetbook.View.SubscriptionMotionDetailActivity;

import java.util.ArrayList;

public class NewMCQExamsSectionAdapter extends RecyclerView.Adapter<NewMCQExamsSectionAdapter.ViewHolder> {

    ArrayList<MCQChapterSection> mcqExamSectionArrayList;
    setOnItemClick onMcqChildItemClick;
    Context context;
    int length = 0;
    int actualPosition;
    MCQChapterSection mcqChapterSection;
    String strSubjectId;
    String strSubjectShortName;
    String strLoading_type;

    public NewMCQExamsSectionAdapter(Context context, setOnItemClick onMcqChildItemClick, int length, ArrayList<MCQChapterSection> mcqExamSectionArrayList, int actualPosition, String strSubjectId, String strSubjectShortName, String strLoading_type) {
        this.context = context;
        this.onMcqChildItemClick = onMcqChildItemClick;
        this.length = length;
        this.mcqExamSectionArrayList=mcqExamSectionArrayList;
        this.actualPosition=actualPosition;
        this.strSubjectId=strSubjectId;
        this.strSubjectShortName=strSubjectShortName;
        this.strLoading_type=strLoading_type;

    }

    @NonNull
    @Override
    public NewMCQExamsSectionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.subscription_recycler_layout, parent, false);
        NewMCQExamsSectionAdapter.ViewHolder holder = new NewMCQExamsSectionAdapter.ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull NewMCQExamsSectionAdapter.ViewHolder holder, int position) {


          holder.subList.setText(mcqExamSectionArrayList.get(actualPosition).getMcqExamsList().get(position).getLong_name());


        holder.subList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMcqChildItemClick.onMcqChildItemClick(context.getResources().getString(R.string.closedialog));
                TabMainActivity.isReadPage = true;
                mcqChapterSection=mcqExamSectionArrayList.get(actualPosition);
                McqMainActivity.mcqExamArrayList = mcqChapterSection.getMcqExamsList();
                context.startActivity(new Intent(context, McqMainActivity.class)
                        .putExtra("subject_id",strSubjectId)
                        .putExtra("subject_name",strSubjectShortName)
                        .putExtra("head_id",mcqChapterSection.getMcqExamsList().get(position).get_id())
                        .putExtra("chapter_id",mcqChapterSection.getMCQChapterId())
                        .putExtra("chapter_name",mcqChapterSection.getMCQShortName())
                        .putExtra("superHeading","No")
                        .putExtra("loading_type",strLoading_type));

            }
        });
    }

    @Override
    public int getItemCount() {
        return length;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        CustomTextView subList;

        public ViewHolder(View view) {
            super(view);
            subList = view.findViewById(R.id.list_title);
        }

    }

    public interface setOnItemClick {
        void onMcqChildItemClick(String message);
    }


}
