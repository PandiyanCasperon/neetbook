package com.app.neetbook.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.neetbook.Interfaces.PointsSection;
import com.app.neetbook.Model.NewPointSectionModel;
import com.app.neetbook.R;
import com.app.neetbook.Utils.widgets.CustomTextView;
import com.app.neetbook.View.PointsMainActivity;
import com.app.neetbook.View.SideMenu.TabMainActivity;

import java.util.ArrayList;

public class NewPointsSectionAdapter extends RecyclerView.Adapter<NewPointsSectionAdapter.ViewHolder> {

    OnItemClickInterface onItemClickInterface;
    ArrayList<NewPointSectionModel> tempArrayList;
    ArrayList<PointsSection> sectionArrayList;
    String strSubId;
    String strSubName;
    Context context;
    int fakePosition = 0;
    PointsSection mcqChapterSection;
    String rootId;

    public NewPointsSectionAdapter(Context context, OnItemClickInterface onItemClickInterface, ArrayList<NewPointSectionModel> tempArrayList, ArrayList<PointsSection> sectionArrayList, String strSubId, String strSubName, String rootId) {
        this.context = context;
        this.onItemClickInterface = onItemClickInterface;
        this.tempArrayList = tempArrayList;
        this.sectionArrayList = sectionArrayList;
        this.strSubId = strSubId;
        this.strSubName = strSubName;
        this.rootId = rootId;

    }

    @NonNull
    @Override
    public NewPointsSectionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.subscription_recycler_layout, parent, false);
        NewPointsSectionAdapter.ViewHolder holder = new NewPointsSectionAdapter.ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull NewPointsSectionAdapter.ViewHolder holder, int position) {
        holder.subList.setText(tempArrayList.get(position).getLong_name());
        holder.subList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickInterface.onItemClick(context.getResources().getString(R.string.closedialog));
                for (int i = 0; i < sectionArrayList.size(); i++) {
                    if (rootId.equals(sectionArrayList.get(i).getId()))
                        fakePosition = i;
                }
                TabMainActivity.isReadPage = true;
                mcqChapterSection = sectionArrayList.get(fakePosition);
                // SubscriptionPointsActivity.pointsContentArrayList = mcqChapterSection.getPointsContent();
                // context.startActivity(new Intent(context, SubscriptionPointsActivity.class).putExtra("point_id",mcqChapterSection.getPointsContent().get(position).get_id()).putExtra("chapter_id",mcqChapterSection.getId()).putExtra("subject_id",strSubId).putExtra("chapter_name",sectionArrayList.get(position).getShortName()).putExtra("subject_name",strSubName).putExtra("points",mcqChapterSection.getPointsContent()));
                try {
                    PointsMainActivity.pointsContentArrayList = mcqChapterSection.getPointsContent();
                    context.startActivity(new Intent(context, PointsMainActivity.class).putExtra("point_id", mcqChapterSection.getPointsContent().get(position).get_id()).putExtra("chapter_id", mcqChapterSection.getId()).putExtra("subject_id", strSubId).putExtra("chapter_name", sectionArrayList.get(fakePosition).getShortName()).putExtra("subject_name", strSubName).putExtra("points", mcqChapterSection.getPointsContent()));

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return tempArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        CustomTextView subList;
        ImageView findTitleImageView;

        public ViewHolder(View view) {
            super(view);
            subList = view.findViewById(R.id.list_title);
            findTitleImageView = view.findViewById(R.id.findTitleImageView);
        }
    }

    public interface OnItemClickInterface {
        void onItemClick(String message);
    }
}
