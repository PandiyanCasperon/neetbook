package com.app.neetbook.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.neetbook.Interfaces.MCQChapterSection;
import com.app.neetbook.Model.NewMcqSuperListPojo;
import com.app.neetbook.R;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.widgets.CustomTextView;
import com.app.neetbook.View.McqMainActivity;
import com.app.neetbook.View.NewSubscriptionDetailActivity;
import com.app.neetbook.View.SideMenu.SubscriptionDetailActivity;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.app.neetbook.View.SubscriptionMotionDetailActivity;

import java.util.ArrayList;

public class NewMCQSectionAdapter extends RecyclerView.Adapter<NewMCQSectionAdapter.ViewHolder> {

    ArrayList<NewMcqSuperListPojo> tempArrayList;
    ArrayList<MCQChapterSection> mcqChapterSectionArrayList;
    setOnItemClick setOnItemClick;
    String parantID, strSubjectId, strSubjectShortName, SearchCategoryTitle;
    Context context;
    MCQChapterSection mcqChapterSection;
    int parentPosition;
    SessionManager sessionManager;


    public NewMCQSectionAdapter(Context context, setOnItemClick setOnItemClick, ArrayList<NewMcqSuperListPojo> tempArrayList, ArrayList<MCQChapterSection> mcqChapterSectionArrayList, String strSubjectId, String strSubjectShortName, int parentPosition, String SearchCategoryTitle) {
        this.context = context;
        this.setOnItemClick = setOnItemClick;
        this.tempArrayList = tempArrayList;
        this.strSubjectId = strSubjectId;
        this.strSubjectShortName = strSubjectShortName;
        this.mcqChapterSectionArrayList = mcqChapterSectionArrayList;
        this.parentPosition = parentPosition;
        this.SearchCategoryTitle = SearchCategoryTitle;
         sessionManager=new SessionManager(context);
         if(SearchCategoryTitle.equals(""))
             this.SearchCategoryTitle=sessionManager.getMcqLastReadDetails();
    }

    @NonNull
    @Override
    public NewMCQSectionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.subscription_recycler_layout, parent, false);
        NewMCQSectionAdapter.ViewHolder holder = new NewMCQSectionAdapter.ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull NewMCQSectionAdapter.ViewHolder holder, int i) {

        if (tempArrayList.get(i).getIsSH().equals("0")) {
            if (tempArrayList.get(i).getFirsthead().equalsIgnoreCase(SearchCategoryTitle)) {
                holder.subList.setText(tempArrayList.get(i).getLong_name());
                holder.subList.setTextColor(context.getResources().getColor(R.color.mcq_blue_dark));
                holder.findTitleImageView.setVisibility(View.VISIBLE);
                holder.findTitleImageView.setColorFilter(context.getResources().getColor(R.color.mcq_blue_dark));
            } else
                holder.subList.setText(tempArrayList.get(i).getLong_name());
        } else {
            if (tempArrayList.get(i).getIsSH().equals("1")) {
                holder.subList.setPaintFlags(holder.subList.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                holder.subList.setText(tempArrayList.get(i).getLong_name());
                parantID = tempArrayList.get(i).getFirsthead();
            } else if (tempArrayList.get(i).getIsSH().equals("11") && tempArrayList.get(i).getFirsthead().equals(parantID)) {
                if (tempArrayList.get(i).getFirsthead().equalsIgnoreCase(SearchCategoryTitle)) {
                    holder.subList.setText(tempArrayList.get(i).getLong_name());
                    holder.subList.setTypeface(holder.subList.getTypeface(), Typeface.ITALIC);
                    holder.subList.setTextColor(context.getResources().getColor(R.color.mcq_blue_dark));
                    holder.findTitleImageView.setVisibility(View.VISIBLE);
                    holder.findTitleImageView.setColorFilter(context.getResources().getColor(R.color.mcq_blue_dark));
                }else {
                    holder.subList.setTypeface(holder.subList.getTypeface(), Typeface.ITALIC);
                    holder.subList.setText(tempArrayList.get(i).getLong_name());
                }

            }
        }

        holder.subList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!tempArrayList.get(i).getIsSH().equals("1")) {
                    sessionManager.setMcqLastReadDetails(tempArrayList.get(i).getFirsthead());
                    setOnItemClick.newOnItemClick(context.getResources().getString(R.string.closedialog));
                    TabMainActivity.isReadPage = true;
                    mcqChapterSection = mcqChapterSectionArrayList.get(parentPosition);
                    McqMainActivity.mcqHeadList = mcqChapterSection.getDeadDeatails();
                    context.startActivity(new Intent(context, McqMainActivity.class).putExtra("subject_id", strSubjectId).putExtra("subject_name", strSubjectShortName).putExtra("chapter_name", mcqChapterSection.getMCQShortName()).putExtra("head_id", tempArrayList.get(i).getId()).putExtra("loading_type", "1").putExtra("superHeading", "No"));
                }

             /*   for (int i=0;i<mcqChapterSectionArrayList.size();i++){
                    if (rootId.equals(mcqChapterSectionArrayList.get(i).getMCQChapterId()))
                        fakePosition=i;
                }*/

            }
        });
    }

    @Override
    public int getItemCount() {
        return tempArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        CustomTextView subList;
        ImageView findTitleImageView;

        public ViewHolder(View view) {
            super(view);
            subList = view.findViewById(R.id.list_title);
            findTitleImageView = view.findViewById(R.id.findTitleImageView);
        }

    }

    public interface setOnItemClick {
        void newOnItemClick(String message);
    }

}
