package com.app.neetbook.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.neetbook.Model.GroupChildBean;
import com.app.neetbook.Model.ReferNowBean;
import com.app.neetbook.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HomeGroupReferNowAdapter extends RecyclerView.Adapter<HomeGroupReferNowAdapter.ViewHolder>{
    ArrayList<ReferNowBean> groupChildBeanArrayList;
    Context context;
//    OnGroupChildClick onChildClick;


    public HomeGroupReferNowAdapter(ArrayList<ReferNowBean> groupChildBeanArrayList, Context context) {

        this.groupChildBeanArrayList = groupChildBeanArrayList;
        this.context = context;
    }
    @NonNull
    @Override
    public HomeGroupReferNowAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.home_group_horiaondal_adapter, parent, false);

        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeGroupReferNowAdapter.ViewHolder holder, int position) {

        holder.viewBack.setVisibility(position==groupChildBeanArrayList.size()-1?View.VISIBLE:View.GONE);
        holder.viewFront.setVisibility(position==0?View.VISIBLE:View.GONE);
        ReferNowBean groupChildBean = groupChildBeanArrayList.get(position);
        holder.txtReferNow.setText(groupChildBean.getStrCouponCode());
        holder.txtSaveRs.setText(groupChildBean.getStrPromoName());
        holder.txtReferDesc.setText(groupChildBean.getStrTitle());


    }

    @Override
    public int getItemCount() {
        return groupChildBeanArrayList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtReferNow,txtSaveRs,txtReferDesc;
        View viewFront,viewBack;
        public ViewHolder(View childView) {
            super(childView);
            viewFront = childView.findViewById(R.id.viewFront);
            viewBack = childView.findViewById(R.id.viewBack);
            txtReferNow = childView.findViewById(R.id.txtReferNow);
            txtSaveRs = childView.findViewById(R.id.txtSaveRs);
            txtReferDesc = childView.findViewById(R.id.txtReferDesc);

        }
    }


}
