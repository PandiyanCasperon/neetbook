package com.app.neetbook.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.neetbook.Interfaces.ArticleContentSection;
import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.Interfaces.PointsSection;
import com.app.neetbook.R;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.View.SideMenu.SubscriptionPointsActivity;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.shuhart.stickyheader.StickyAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

public class ArticleContentSectionAdaptewr  extends StickyAdapter<RecyclerView.ViewHolder, RecyclerView.ViewHolder> {

    ArrayList<ArticleContentSection> articleSectionArrayList;
    private static final int LAYOUT_HEADER= 0;
    private static final int LAYOUT_CHILD= 1;
    private int currentSelectedPosition = -1;
    private int chileSeletedPosition = -1;
    private int superChileSeletedPosition = -1;
    private String currentSelectedHeader,indexCount = "1";
    Context context;
    private String strSubId = "",strSubName = "";
    private int index = 0;
    private ItemClickListener itemClickListener;
    SessionManager sessionManager;
    String strName;
    Typeface tfBold,tfMedium,tfRegular;
    LinearSnapHelper snapHelper;
    public ArticleContentSectionAdaptewr(Context context, ArrayList<ArticleContentSection> articleSectionArrayList){

        // inflater = LayoutInflater.from(context);
        this.context = context;
        this.articleSectionArrayList = articleSectionArrayList;
        currentSelectedHeader = articleSectionArrayList.get(0).titile();
        this.sessionManager = new SessionManager(this.context);
        tfBold  = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium  = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular  = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaRegular.otf");
        createSnap();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (viewType == LAYOUT_HEADER ) {
            return new HeaderViewholder(inflater.inflate(R.layout.article_content_section_header, parent, false));
        }else {
            return new ItemViewHolder(inflater.inflate(R.layout.article_content_section_child_item, parent, false));
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {


        if (articleSectionArrayList.get(position).isHeader().equals("Yes")) {
            HeaderViewholder headerViewholder = ((HeaderViewholder) holder);
            headerViewholder.points_header_text.setText( articleSectionArrayList.get(position).titile());
            setFontSize(headerViewholder);
        } else {
            ItemViewHolder pointsViewHolder = ((ItemViewHolder) holder);
            pointsViewHolder.article_text.setText((Html.fromHtml(articleSectionArrayList.get(position).points())));
            pointsViewHolder.innerRecycler.setVisibility(articleSectionArrayList.get(position).strImages().size()>0 ? View.VISIBLE : View.GONE);
            if(context.getResources().getBoolean(R.bool.isTablet))
                pointsViewHolder.llRecyclerView.setVisibility(articleSectionArrayList.get(position).strImages().size()>0 ? View.VISIBLE : View.GONE);
            ArrayList<String> singleSectionItems = articleSectionArrayList.get(position).strImages();
            InnerImageListAdapter innerImageListAdapter = new InnerImageListAdapter(context,singleSectionItems,articleSectionArrayList.get(position).titile());
            pointsViewHolder.innerRecycler.setHasFixedSize(true);
            pointsViewHolder.innerRecycler.setLayoutManager(context.getResources().getBoolean(R.bool.isTablet)?new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false): new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            if(!context.getResources().getBoolean(R.bool.isTablet))
                snapHelper.attachToRecyclerView(pointsViewHolder.innerRecycler);
            pointsViewHolder.innerRecycler.setAdapter(innerImageListAdapter);


            setFontSize(pointsViewHolder);

        }


    }

    private void setFontSize(HeaderViewholder headerViewholder) {
        if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
            headerViewholder.points_header_text.setTextAppearance(context,R.style.textViewSmallArticleHeader);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
            headerViewholder.points_header_text.setTextAppearance(context,R.style.textViewSmallArticleHeader);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
            headerViewholder.points_header_text.setTextAppearance(context,R.style.textViewSmallArticleHeader);
        }

        headerViewholder.points_header_text.setTypeface(tfMedium);
    }

    private void setFontSize(ItemViewHolder holder) {
        if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
            holder.article_text.setTextAppearance(context,R.style.textViewSmallArticleContent);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
            holder.article_text.setTextAppearance(context,R.style.textViewMediumArticleContent);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
            holder.article_text.setTextAppearance(context,R.style.textViewLargeArticleContent);
        }

        holder.article_text.setTypeface(tfRegular);
    }
    @Override
    public int getItemViewType(int position) {
        if(articleSectionArrayList.get(position).isHeader().equals("Yes")) {
            return LAYOUT_HEADER;
        }else
            return LAYOUT_CHILD;
    }

    @Override
    public int getItemCount() {
        return articleSectionArrayList.size();
    }

    @Override
    public int getHeaderPositionForItem(int itemPosition) {
        Log.d("seeee",""+itemPosition+"......"+articleSectionArrayList.get(itemPosition).sectionPosition());
        return articleSectionArrayList.get(itemPosition).sectionPosition();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int headerPosition) {
        HeaderViewholder headerViewholder = ((HeaderViewholder) holder);
        /*headerViewholder.points_header_text.setText(articleSectionArrayList.get(headerPosition).titile());*/
        headerViewholder.points_header_text.setText("header");

//       }
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return createViewHolder(parent, LAYOUT_HEADER);
    }

    public static class HeaderViewholder extends RecyclerView.ViewHolder {
        private TextView points_header_text;

        HeaderViewholder(View itemView) {
            super(itemView);
            points_header_text = itemView.findViewById(R.id.points_header_text);

        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView article_text;
        ImageView ivLine, ivCircle;
        LinearLayout article_image_container;
        RecyclerView innerRecycler;
        RelativeLayout llRecyclerView;
        ItemViewHolder(View itemView) {
            super(itemView);
            article_text = itemView.findViewById(R.id.article_text);
            ivCircle = itemView.findViewById(R.id.ivCircle);
            ivLine = itemView.findViewById(R.id.ivLine);
            innerRecycler = itemView.findViewById(R.id.innerRecycler);
            llRecyclerView = itemView.findViewById(R.id.llRecyclerView);

        }
    }

    public void setTopItemView(String currentSelectedHeader,int positon)
    {
        this.currentSelectedHeader = currentSelectedHeader;

        this.index = positon;
    }
    public void CollapseExpandedView(int position)
    {
        this.currentSelectedPosition = position;
        //notifyDataSetChanged();
    }

    public void setOnItemClickListener(ItemClickListener itemClickListener)
    {
        this.itemClickListener = itemClickListener;
    }


    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
    }
}
