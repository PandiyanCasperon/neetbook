package com.app.neetbook.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.neetbook.Interfaces.OnSearchItemClickListener;
import com.app.neetbook.Model.SearchBean;
import com.app.neetbook.R;
import com.app.neetbook.View.ImagesViewActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HomeSearchAdapter extends RecyclerView.Adapter<HomeSearchAdapter.ViewHolder>{

    Context context;
    ArrayList<SearchBean> searchBeanArrayList;
    String strTitle;
    int menuType;
    OnSearchItemClickListener onSearchItemClickListener;
public HomeSearchAdapter(ArrayList<SearchBean> searchBeanArrayList, Context context, int menuType) {

        this.searchBeanArrayList = searchBeanArrayList;
        this.context = context;
        this.menuType = menuType;

        }
@NonNull
@Override
public HomeSearchAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.homea_search_item, parent, false);

        return new ViewHolder(listItem);
        }

@Override
public void onBindViewHolder(@NonNull HomeSearchAdapter.ViewHolder holder, int position) {
        SearchBean searchBean =  searchBeanArrayList.get(position);
        holder.txtTitle.setText(searchBean.getStrHeadLongName().equals("")?searchBean.getStrChaptLongName():searchBean.getStrHeadLongName());
        holder.txtChapter.setText(searchBean.getStrHeadLongName().equals("")?"":searchBean.getStrChaptLongName());
        holder.txtSubject.setText(searchBean.getStrSubjectLongName());
        Picasso.with(context).load(IConstant.BaseUrl+searchBean.getStrMImage()).into(holder.imgSearchItem);
        Picasso.with(context).load(searchBean.getStrIsSubscribed().equalsIgnoreCase("1")?R.drawable.right_mark : R.drawable.ic_delete).into(holder.imgSubscribed);

        }

@Override
public int getItemCount() {
        return searchBeanArrayList.size();
        }
public class ViewHolder extends RecyclerView.ViewHolder {

    ImageView imgSearchItem,imgSubscribed;
    private TextView txtTitle,txtChapter,txtSubject;
    public ViewHolder(View childView) {
        super(childView);

        imgSubscribed = childView.findViewById(R.id.imgSubscribed);
        imgSearchItem = childView.findViewById(R.id.imgSearchItem);
        this.txtTitle = childView.findViewById(R.id.txtTitle);
        this.txtChapter = childView.findViewById(R.id.txtChapter);
        this.txtSubject = childView.findViewById(R.id.txtSubject);
        childView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSearchItemClickListener.onSearchItemClicked(getAdapterPosition(),menuType);
                //context.startActivity(new Intent(context, ImagesViewActivity.class).putExtra("position",""+getAdapterPosition()).putExtra("title",strTitle).putExtra("image",images).putExtra("from","Article"));
            }
        });

    }
}

public void setOnSearchItemClickListener(OnSearchItemClickListener onSearchItemClickListener)
{
    this.onSearchItemClickListener = onSearchItemClickListener;
}
}
