package com.app.neetbook.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.Interfaces.MCQChapterSection;
import com.app.neetbook.R;
import com.app.neetbook.View.SideMenu.SubscriptionDetailActivity;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.shuhart.stickyheader.StickyAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class MCQExamsSectionAdapter   extends StickyAdapter<RecyclerView.ViewHolder, RecyclerView.ViewHolder> {

    private ArrayList<MCQChapterSection> sectionArrayList;
    private static final int LAYOUT_HEADER= 0;
    private static final int LAYOUT_CHILD= 1;
    private int currentSelectedPosition = -1;
    private int chileSeletedPosition = -1;
    private int superChileSeletedPosition = -1;
    Context context;
    private String strSubId = "",strSubName = "",strLoading_type = "";
    private String currentSelectedHeader,indexCount = "1";
    private int index = 0;
    private ItemClickListener itemClickListener;
    private McqExamInterface mcqExamInterface;
    public MCQExamsSectionAdapter(Context context, ArrayList<MCQChapterSection> sectionArrayList, String strSubId, String strSubName, String strLoading_type,McqExamInterface mcqExamInterface){

        // inflater = LayoutInflater.from(context);
        this.context = context;
        this.sectionArrayList = sectionArrayList;
        this.strSubId = strSubId;
        this.strSubName = strSubName;
        this.strLoading_type = strLoading_type;
        this.mcqExamInterface=mcqExamInterface;
        currentSelectedHeader = sectionArrayList.get(0).getMCQLongrName();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (viewType == LAYOUT_HEADER ) {
            return new HeaderViewholder(inflater.inflate(R.layout.mcq_section_adapter, parent, false));
        }else {
            return new ItemViewHolder(inflater.inflate(R.layout.mcq_section_child_item, parent, false));
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        Log.e("isHeader",""+sectionArrayList.get(position).isMCQChapterHeader());

        if (sectionArrayList.get(position).isMCQChapterHeader()) {
            HeaderViewholder headerViewholder = ((HeaderViewholder) holder);
            headerViewholder.title.setText(sectionArrayList.get(position).getMCQLongrName());
            headerViewholder.textView17.setText(sectionArrayList.get(position).getMCQCount());
            Picasso.with(context).load(IConstant.BaseUrl+sectionArrayList.get(position).getMCQImage()).into(headerViewholder.imageView14);
            if(sectionArrayList.get(position).sectionPosition()== currentSelectedPosition) {
                headerViewholder.ivExpand.setVisibility(View.GONE);
                headerViewholder.ivCollapse.setVisibility(View.VISIBLE);
            }else
            {
             //   headerViewholder.ivExpand.setVisibility(View.VISIBLE);
                headerViewholder.ivCollapse.setVisibility(View.GONE);
            }
        } else {
            ItemViewHolder itemViewHolder = ((ItemViewHolder) holder);
            if(sectionArrayList.get(position).sectionPosition()== currentSelectedPosition) {

            //    setChileView(sectionArrayList.get(position), position, itemViewHolder,0);
            }else
            {
                itemViewHolder.rlParent.setVisibility(View.GONE);
                itemViewHolder.llChildHead.setVisibility(View.GONE);
            }


        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mcqExamInterface.parentClickListener(sectionArrayList.get(position).getMCQChapterId());
                /*if(currentSelectedPosition!= sectionArrayList.get(position).sectionPosition()) {
                    itemClickListener.onParentItemClick(position);
                    currentSelectedPosition = sectionArrayList.get(position).sectionPosition();
                    chileSeletedPosition = -1;
                }else
                    currentSelectedPosition = -1;
                notifyDataSetChanged();*/
            }
        });
    }

    private void setChileView(final MCQChapterSection mcqChapterSection, final int position, final ItemViewHolder itemViewHolder,int open_position) {

        if(sectionArrayList.get(position).sectionPosition()== currentSelectedPosition) {
            itemViewHolder.rlParent.setVisibility(View.VISIBLE);
           // itemViewHolder.llChildHead.setVisibility(View.VISIBLE);
            itemViewHolder.llChildHead.removeAllViews();
            if(mcqChapterSection.getMcqExamsList().size()>0)
            {
                for(int i=0;i<mcqChapterSection.getMcqExamsList().size();i++)
                {
                    View ChildHeadView = LayoutInflater.from(context).inflate(R.layout.mcq_chiled_item,null);
                    ImageView imgArticleChildHead = ChildHeadView.findViewById(R.id.imgArticleChildHead);
                    TextView txt_article_child_head = ChildHeadView.findViewById(R.id.txt_article_child_head);
                    final LinearLayoutCompat llChildOfChild = ChildHeadView.findViewById(R.id.llChildOfChild);
                    imgArticleChildHead.setVisibility(i== chileSeletedPosition?View.VISIBLE:View.INVISIBLE);
                    txt_article_child_head.setTextColor(i== chileSeletedPosition?context.getResources().getColor(R.color.mcq_blue):context.getResources().getColor(R.color.black));
                    txt_article_child_head.setText(mcqChapterSection.getMcqExamsList().get(i).getLong_name());
                    final int finalI = i;
                    ChildHeadView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            chileSeletedPosition = finalI;
                          //  setChileView(mcqChapterSection,position,itemViewHolder,1);
                            //notifyDataSetChanged();
                            TabMainActivity.isReadPage = true;
                            SubscriptionDetailActivity.mcqExamArrayList = mcqChapterSection.getMcqExamsList();
                            context.startActivity(new Intent(context, SubscriptionDetailActivity.class)
                                    .putExtra("subject_id",strSubId)
                                    .putExtra("subject_name",strSubName)
                                    .putExtra("head_id",mcqChapterSection.getMcqExamsList().get(finalI).get_id())
                                    .putExtra("chapter_id",mcqChapterSection.getMCQChapterId())
                                    .putExtra("chapter_name",mcqChapterSection.getMCQShortName())
                                    .putExtra("superHeading","No")
                                    .putExtra("loading_type",strLoading_type));
                        }
                    });

                    itemViewHolder.llChildHead.addView(ChildHeadView);

                    if(sectionArrayList.size()-1==position){

                        itemViewHolder.llChildHead.setVisibility(View.VISIBLE);

                    }else{

                        if(open_position==0){

                            expand(itemViewHolder.llChildHead);
                        }


                    }
                }
            }
        }else
        {

        }

    }

    @Override
    public int getItemViewType(int position) {
        if(sectionArrayList.get(position).isMCQChapterHeader()) {
            return LAYOUT_HEADER;
        }else
            return LAYOUT_CHILD;
    }

    @Override
    public int getItemCount() {
        return sectionArrayList.size();
    }

    @Override
    public int getHeaderPositionForItem(int itemPosition) {
        Log.d("seeee",""+itemPosition+"......"+sectionArrayList.get(itemPosition).sectionPosition());
        return sectionArrayList.get(itemPosition).sectionPosition();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int headerPosition) {
        //if( sectionArrayList.get(headerPosition).isHeader()) {

        HeaderViewholder headerViewholder = ((HeaderViewholder) holder);
        /*headerViewholder.title.setText(sectionArrayList.get(headerPosition).getMCQChapterName());
        headerViewholder.textView17.setText(sectionArrayList.get(headerPosition).getMCQCount());
        Picasso.with(context).load(IConstant.BaseUrl+sectionArrayList.get(headerPosition).getMCQImage()).into(headerViewholder.imageView14);*/

        Picasso.with(context).load(IConstant.BaseUrl+sectionArrayList.get(index).getMCQImage()).into(((HeaderViewholder) holder).imageView14);
        ((HeaderViewholder) holder).title.setText( currentSelectedHeader);
        ((HeaderViewholder) holder).textView17.setText(""+indexCount);
        headerViewholder.ivCollapse.setVisibility(View.VISIBLE);

    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return createViewHolder(parent, LAYOUT_HEADER);
    }

    public static class HeaderViewholder extends RecyclerView.ViewHolder {

        private TextView textView17,title;
        private ConstraintLayout llView;
        ImageView ivCollapse,ivExpand,imageView14;
        HeaderViewholder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tvCategoryTitle);
            llView = itemView.findViewById(R.id.llView);
            ivCollapse = itemView.findViewById(R.id.ivCollapse);
            ivExpand = itemView.findViewById(R.id.ivExpand);
            textView17 = itemView.findViewById(R.id.textView17);
            imageView14 = itemView.findViewById(R.id.imageView14);
        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {


        private LinearLayoutCompat llChildHead;
        private RelativeLayout rlParent;
        ItemViewHolder(View itemView) {
            super(itemView);
            llChildHead = itemView.findViewById(R.id.llChildHead);
            rlParent = itemView.findViewById(R.id.rlParent);

        }
    }
    public void setTopItemView(String currentSelectedHeader,String indexCount,int index)
    {
        this.currentSelectedHeader = currentSelectedHeader;
        this.indexCount = indexCount;
        this.index = index;
    }
    public void CollapseExpandedView(int position)
    {
        this.currentSelectedPosition = position;
        //notifyDataSetChanged();
    }

    public void setOnItemClickListener(ItemClickListener itemClickListener)
    {
        this.itemClickListener = itemClickListener;
    }

    public void expand(final View v) {


        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? WindowManager.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Expansion speed of 1dp/ms
        int s= (int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density);
        System.out.println("value"+s);
        a.setDuration(500);
        v.startAnimation(a);



    }


    public void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Collapse speed of 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);

    }
    public interface McqExamInterface{
        void parentClickListener(String rootId);
    }
}
