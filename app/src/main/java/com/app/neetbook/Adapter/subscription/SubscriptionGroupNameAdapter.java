package com.app.neetbook.Adapter.subscription;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.app.neetbook.Interfaces.OnMobileHomeMenuSelectedListioner;
import com.app.neetbook.Model.Groupslist;
import com.app.neetbook.R;
import com.app.neetbook.Subscriptionroom.SubscriptionGroups;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SubscriptionGroupNameAdapter  extends RecyclerView.Adapter<SubscriptionGroupNameAdapter.ViewHolder>{
    List<SubscriptionGroups> groupChildBeanArrayList;
    Context context;
    OnMobileHomeMenuSelectedListioner onGroupMenuSelectedListener;
    int currentPosition ;

    public SubscriptionGroupNameAdapter(List<SubscriptionGroups> groupChildBeanArrayList, Context context,int lastPosition) {

        this.groupChildBeanArrayList = groupChildBeanArrayList;
        this.context = context;
        this.currentPosition = lastPosition;
    }
    @NonNull
    @Override
    public SubscriptionGroupNameAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.homegroupnameadapteritem, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull SubscriptionGroupNameAdapter.ViewHolder holder, final int position) {
        final SubscriptionGroups groupChildBean = groupChildBeanArrayList.get(position);
        holder.viewBack.setVisibility(position==groupChildBeanArrayList.size()-1?View.VISIBLE:View.GONE);
        holder.viewFront.setVisibility(position==0?View.VISIBLE:View.GONE);

        if(currentPosition == position){
            holder.llGroupContainer.setBackground(context.getResources().getDrawable(R.drawable.tab_background_selected));
            holder.txtGroupName.setTextColor(context.getResources().getColor(R.color.white));
        }
        else {
            holder.llGroupContainer.setBackground(context.getResources().getDrawable(R.drawable.tab_background_unselected));
            holder.txtGroupName.setTextColor(context.getResources().getColor(R.color.group_unselected_border));
        }
        holder.txtGroupName.setText(groupChildBean.getShortName());
        holder.llGroupContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onGroupMenuSelectedListener.onGroupSelected(groupChildBean.getGroupid(),position);
                currentPosition = position;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return groupChildBeanArrayList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtGroupName,txtSaveRs,txtReferDesc;
        LinearLayout llGroupContainer;
        View viewFront,viewBack;
        public ViewHolder(View childView) {
            super(childView);
            viewFront = childView.findViewById(R.id.viewFront);
            viewBack = childView.findViewById(R.id.viewBack);
            txtGroupName = childView.findViewById(R.id.txtGroupName);
            llGroupContainer = childView.findViewById(R.id.llGroupContainer);


        }
    }

    public void notifyPosition(int position)
    {
        currentPosition = position;
        notifyDataSetChanged();
    }

    public void setOnGroupMenuSelectedListener(OnMobileHomeMenuSelectedListioner onGroupMenuSelectedListener)
    {
        this.onGroupMenuSelectedListener = onGroupMenuSelectedListener;
    }
}
