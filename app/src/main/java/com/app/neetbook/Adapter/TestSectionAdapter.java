package com.app.neetbook.Adapter;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.neetbook.Interfaces.ArticleHeaderSestion;
import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.Interfaces.PointsSection;
import com.app.neetbook.Interfaces.TestSection;
import com.app.neetbook.R;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.View.SideMenu.SubscriptionPointsActivity;
import com.app.neetbook.View.SideMenu.SubscriptionTestActivity;
import com.app.neetbook.View.SideMenu.SubscriptionTestFragment;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.app.neetbook.View.SideMenu.collMainActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.shuhart.stickyheader.StickyAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class TestSectionAdapter extends StickyAdapter<RecyclerView.ViewHolder, RecyclerView.ViewHolder> {

    private ArrayList<TestSection> sectionArrayList;
    private static final int LAYOUT_HEADER = 0;
    private static final int LAYOUT_CHILD = 1;
    private int currentSelectedPosition = -1;
    private int currentChileHeadSelectedPosition = -1;
    private int superChileSeletedPosition = -1;
    Context context;
    private String subjectId = "";
    private String subjectName = "";
    private String currentSelectedHeader, indexCount = "1";
    private int chileSeletedPosition = -1;
    private int index = 0;
    Typeface tfBold, tfMedium, tfRegular;
    private ItemClickListener itemClickListener;
    private String strPending, strPendingTestId;
    Dialog alertDialog;
    SessionManager sessionManager;

    /////////////////
    SetOnClickInterface setOnClickInterface;

    public TestSectionAdapter(Context context, SetOnClickInterface setOnClickInterface,ArrayList<TestSection> sectionArrayList, String subjectId, String subjectName, int currentSelectedPosition, int chileSeletedPosition, String strPending, String strPendingTestId) {

        // inflater = LayoutInflater.from(context);
        this.context = context;
        this.sectionArrayList = sectionArrayList;
        this.subjectId = subjectId;
        this.subjectName = subjectName;
        this.strPending = strPending;
        this.strPendingTestId = strPendingTestId;
        this.currentSelectedPosition = currentSelectedPosition;
        sessionManager = new SessionManager(context);
        //this.chileSeletedPosition = chileSeletedPosition;
        currentSelectedHeader = sectionArrayList.get(0).getLongName();
        tfBold = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaRegular.otf");
        alertDialog = new Dialog(context);
        this.setOnClickInterface=setOnClickInterface;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (viewType == LAYOUT_HEADER) {
            return new HeaderViewholder(inflater.inflate(R.layout.test_header_item, parent, false));
        } else {
            return new ItemViewHolder(inflater.inflate(R.layout.test_parent_child_layout/*test_chile_item*/, parent, false));
        }
    }

    public int getCurrentSelectedPosition() {
        return currentSelectedPosition;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (sectionArrayList.get(position).isTestHeader()) {
            HeaderViewholder headerViewholder = ((HeaderViewholder) holder);
            if (position != 0) {

                if (sectionArrayList.get(position).getIsFullTest().equals("Yes")) {
                    headerViewholder.textView17.setVisibility(View.GONE);
                    headerViewholder.imageView14.setVisibility(View.GONE);
                    headerViewholder.txtFt.setVisibility(View.VISIBLE);
                    headerViewholder.title.setText(sectionArrayList.get(position).getLongName());
                    headerViewholder.txtFt.setText("FT" + (position + 1) + " ");
                    headerViewholder.title.setTypeface(tfBold);
                } else {
                    headerViewholder.title.setText(sectionArrayList.get(position).getLongName());
                    headerViewholder.textView17.setText(sectionArrayList.get(position).getCount());
                    headerViewholder.textView17.setVisibility(View.VISIBLE);
                    headerViewholder.imageView14.setVisibility(View.VISIBLE);
                    headerViewholder.txtFt.setVisibility(View.INVISIBLE);
                    headerViewholder.title.setTypeface(tfMedium);
                    Picasso.with(context).load(IConstant.BaseUrl + sectionArrayList.get(position).getImage()).into(headerViewholder.imageView14);
                }

                 if (sectionArrayList.get(position).sectionPosition() == currentSelectedPosition) {

                    ((HeaderViewholder) holder).ivExpand.setImageResource(R.drawable.ic_arrow_up_article);

                } else {

                    ((HeaderViewholder) holder).ivExpand.setImageResource(R.drawable.ic_arow_down_article);

                }

                if (headerViewholder.txtFt.getVisibility()==View.VISIBLE) {

                    headerViewholder.ivExpand.setVisibility(View.GONE);

                } else {

                   // headerViewholder.ivExpand.setVisibility(View.VISIBLE);
                }

            }

        } else {
            ItemViewHolder itemViewHolder = ((ItemViewHolder) holder);
            if (sectionArrayList.get(position).sectionPosition() == currentSelectedPosition) {

                //setChileView(sectionArrayList.get(position), position, itemViewHolder,0);
            } else {

               itemViewHolder.llChildHead.setVisibility(View.GONE);
                //slideUp(itemViewHolder.llChildHead);
               // collapse(itemViewHolder.llChildHead);
               // new_collapse(itemViewHolder.llChildHead,1000,-200);
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setOnClickInterface.onParentClickListener(sectionArrayList.get(position).getChapteId(),position);


               /* if (currentSelectedPosition != sectionArrayList.get(position).sectionPosition()) {
                    //if (sectionArrayList.get(sectionArrayList.get(position).sectionPosition()).getIsFullTest() != null && sectionArrayList.get(sectionArrayList.get(position).sectionPosition()).getIsFullTest().equalsIgnoreCase("Yes")) {
                        if (sectionArrayList.get(position).getIsFullTest() != null && sectionArrayList.get(position).getIsFullTest().equalsIgnoreCase("Yes")) {
                        Log.e("ClickedPosition", String.valueOf(sectionArrayList.get(position).sectionPosition()));
                        TabMainActivity.isReadPage = true;
                        if (!SubscriptionTestFragment.isTestIdSaved.equals("")) {
                            alertDialog.setContentView(R.layout.custom_alert);
                            alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                            TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
                            TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
                            textViewTitle.setText(context.getString(R.string.alert_incomplete));
                            textViewDesc.setText(context.getString(R.string.alert_incomplete_message));
                            Button btnOk = alertDialog.findViewById(R.id.btnOk);
                            Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
                            btnOk.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.dismiss();
                                    context.startActivity(new Intent(context, SubscriptionTestActivity.class)
                                            .putExtra("subject_id", subjectId)
                                            .putExtra("chapter_id", sectionArrayList.get(position).getChapteId())
                                            .putExtra("chapter_name", sectionArrayList.get(position).getShortName())
                                            .putExtra("subject_name", subjectName)
                                            .putExtra("pending", "1")
                                            .putExtra("pendingTestId", strPendingTestId)
                                            .putExtra("testId", sectionArrayList.get(position).getTestArrayList().get(0)._id)
                                            .putExtra("type", sectionArrayList.get(position).getTestArrayList().get(0).type)
                                            .putExtra("no_of_question", sectionArrayList.get(position).getTestArrayList().get(0).no_of_qstn)
                                            .putExtra("isFinished", sectionArrayList.get(position).getTestArrayList().get(0).is_finished)
                                            .putExtra("questionType", sectionArrayList.get(position).getTestArrayList().get(0).qstn_type)
                                            .putExtra("strInstruction", sectionArrayList.get(position).getTestArrayList().get(0).instruction)
                                            .putExtra("strLongName", sectionArrayList.get(position).getTestArrayList().get(0).long_name)
                                            .putExtra("crt_ans_mark", sectionArrayList.get(position).getTestArrayList().get(0).crt_ans_mark)
                                            .putExtra("wrng_ans_mark", sectionArrayList.get(position).getTestArrayList().get(0).wrng_ans_mark)
                                    );
                                }
                            });
                            btnCancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.dismiss();
                                    SubscriptionTestFragment.isPending = "-1";
                                    context.startActivity(new Intent(context, SubscriptionTestActivity.class)
                                            .putExtra("subject_id", subjectId)
                                            .putExtra("chapter_id", sectionArrayList.get(position).getChapteId())
                                            .putExtra("chapter_name", sectionArrayList.get(position).getShortName())
                                            .putExtra("subject_name", subjectName)
                                            .putExtra("pending", "0")
                                            .putExtra("pendingTestId", "")
                                            .putExtra("testId", sectionArrayList.get(position).getTestArrayList().get(0)._id)
                                            .putExtra("type", sectionArrayList.get(position).getTestArrayList().get(0).type)
                                            .putExtra("no_of_question", sectionArrayList.get(position).getTestArrayList().get(0).no_of_qstn)
                                            .putExtra("isFinished", sectionArrayList.get(position).getTestArrayList().get(0).is_finished)
                                            .putExtra("questionType", sectionArrayList.get(position).getTestArrayList().get(0).qstn_type)
                                            .putExtra("strInstruction", sectionArrayList.get(position).getTestArrayList().get(0).instruction)
                                            .putExtra("strLongName", sectionArrayList.get(position).getTestArrayList().get(0).long_name)
                                            .putExtra("crt_ans_mark", sectionArrayList.get(position).getTestArrayList().get(0).crt_ans_mark)
                                            .putExtra("wrng_ans_mark", sectionArrayList.get(position).getTestArrayList().get(0).wrng_ans_mark)
                                    );
                                }
                            });
                            if (!alertDialog.isShowing())
                                alertDialog.show();
                        } else {
                            SubscriptionTestFragment.isPending = "-1";
                            context.startActivity(new Intent(context, SubscriptionTestActivity.class)
                                    .putExtra("subject_id", subjectId)
                                    .putExtra("chapter_id", sectionArrayList.get(position).getChapteId())
                                    .putExtra("chapter_name", sectionArrayList.get(position).getShortName())
                                    .putExtra("subject_name", subjectName)
                                    .putExtra("pending", "0")
                                    .putExtra("pendingTestId", "")
                                    .putExtra("testId", sectionArrayList.get(position).getTestArrayList().get(0)._id)
                                    .putExtra("type", sectionArrayList.get(position).getTestArrayList().get(0).type)
                                    .putExtra("no_of_question", sectionArrayList.get(position).getTestArrayList().get(0).no_of_qstn)
                                    .putExtra("isFinished", sectionArrayList.get(position).getTestArrayList().get(0).is_finished)
                                    .putExtra("questionType", sectionArrayList.get(position).getTestArrayList().get(0).qstn_type)
                                    .putExtra("strInstruction", sectionArrayList.get(position).getTestArrayList().get(0).instruction)
                                    .putExtra("strLongName", sectionArrayList.get(position).getTestArrayList().get(0).long_name)
                                    .putExtra("crt_ans_mark", sectionArrayList.get(position).getTestArrayList().get(0).crt_ans_mark)
                                    .putExtra("wrng_ans_mark", sectionArrayList.get(position).getTestArrayList().get(0).wrng_ans_mark)
                            );
                        }

                    } else {
                        itemClickListener.onParentItemClick(position);
                        currentSelectedPosition = sectionArrayList.get(position).sectionPosition();
                        chileSeletedPosition = -1;
                        notifyDataSetChanged();
                    }
                } else {
                    currentSelectedPosition = -1;
                    notifyDataSetChanged();
                }*/
            }
        });
    }

    private void setChileView(final TestSection testSection, final int position, final ItemViewHolder itemViewHolder,int open_position) {

        if (sectionArrayList.get(position).sectionPosition() == currentSelectedPosition) {
            //itemViewHolder.llChildHead.setVisibility(View.VISIBLE);
            itemViewHolder.llChildHead.removeAllViews();
            if (testSection.getTestArrayList().size() > 0) {
                for (int i = 0; i < testSection.getTestArrayList().size(); i++) {
                    View ChildHeadView = LayoutInflater.from(context).inflate(R.layout.test_chile_item, null);
                    ImageView imgArticleChildHead = ChildHeadView.findViewById(R.id.imgArticleChildHead);
                    TextView txt_article_child_head = ChildHeadView.findViewById(R.id.txt_article_child_head);
                    LinearLayout llParent = ChildHeadView.findViewById(R.id.llParent);

                    imgArticleChildHead.setVisibility(i == chileSeletedPosition ? View.VISIBLE : View.INVISIBLE);
                    txt_article_child_head.setTextColor(i == chileSeletedPosition ? context.getResources().getColor(R.color.test_yellow_dark) : context.getResources().getColor(R.color.black));
                    txt_article_child_head.setText(testSection.getTestArrayList().get(i).long_name);
                    final int finalI = i;
                    ChildHeadView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            chileSeletedPosition = finalI;
                            TabMainActivity.isReadPage = true;
                            setChileView(testSection, position, itemViewHolder,1);
                           // notifyDataSetChanged();


                        }
                    });
                    itemViewHolder.llChildHead.addView(ChildHeadView);

                    if(sectionArrayList.size()-1==position){

                        itemViewHolder.llChildHead.setVisibility(View.VISIBLE);

                    }else{

                        if(open_position==0){

                            expand(itemViewHolder.llChildHead);

                        }

                    }
                }
            }
        } else {

        }

    }

    @Override
    public int getItemViewType(int position) {
        if (sectionArrayList.get(position).isTestHeader()) {
            return LAYOUT_HEADER;
        } else
            return LAYOUT_CHILD;
    }

    @Override
    public int getItemCount() {
        return sectionArrayList.size();
    }

    @Override
    public int getHeaderPositionForItem(int itemPosition) {

        return sectionArrayList.get(itemPosition).sectionPosition();
    }


    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int headerPosition) {
        //if( sectionArrayList.get(headerPosition).isHeader()) {
        HeaderViewholder headerViewholder = ((HeaderViewholder) holder);

        if (sectionArrayList.get(headerPosition).getIsFullTest() != null && sectionArrayList.get(index).getIsFullTest().equals("Yes")) {
            headerViewholder.textView17.setVisibility(View.GONE);
            headerViewholder.imageView14.setVisibility(View.GONE);
            headerViewholder.txtFt.setVisibility(View.VISIBLE);
            headerViewholder.txtFt.setText("FT" + (headerPosition+1));
            headerViewholder.title.setText(currentSelectedHeader);
            headerViewholder.title.setTypeface(tfBold);
            headerViewholder.ivExpand.setVisibility(View.GONE);
        } else {
            headerViewholder.title.setText(currentSelectedHeader);
            headerViewholder.textView17.setText(indexCount);
            headerViewholder.textView17.setVisibility(View.VISIBLE);
            headerViewholder.imageView14.setVisibility(View.VISIBLE);
            headerViewholder.txtFt.setVisibility(View.GONE);
            Picasso.with(context).load(IConstant.BaseUrl + sectionArrayList.get(index).getImage()).into(headerViewholder.imageView14);
            headerViewholder.title.setTypeface(tfMedium);
           // headerViewholder.ivExpand.setVisibility(View.VISIBLE);
        }

        Log.e("KSVHeader_position---", String.valueOf(headerPosition));
      //  headerViewholder.ivExpand.setVisibility(View.GONE);

       /* headerViewholder.title.setText(currentSelectedHeader);
        headerViewholder.textView17.setText(indexCount);
        headerViewholder.textView17.setVisibility(View.VISIBLE);
        headerViewholder.imageView14.setVisibility(View.VISIBLE);*/
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return createViewHolder(parent, LAYOUT_HEADER);
    }

    public static class HeaderViewholder extends RecyclerView.ViewHolder {
        TextView textView17;
        ImageView ivCollapse, ivExpand;
        private TextView title, txtFt;
        ImageView imageView14;
        ConstraintLayout parentView;

        HeaderViewholder(View itemView) {
            super(itemView);
            ivCollapse = itemView.findViewById(R.id.ivCollapse);
            ivExpand = itemView.findViewById(R.id.ivExpand);
            parentView = itemView.findViewById(R.id.parentView);
            title = itemView.findViewById(R.id.tvCategoryTitle);

            textView17 = itemView.findViewById(R.id.textView17);
            imageView14 = itemView.findViewById(R.id.imageView14);
            txtFt = itemView.findViewById(R.id.txtFt);

        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {


        private LinearLayoutCompat llChildHead;
        private RelativeLayout rlParent;

        ItemViewHolder(View itemView) {
            super(itemView);
            llChildHead = itemView.findViewById(R.id.llChildHead);

        }
    }

    public void setTopItemView(String currentSelectedHeader, String indexCount, int positon) {
        this.currentSelectedHeader = currentSelectedHeader;

        this.indexCount = indexCount;
        this.index = positon;
    }

    public void CollapseExpandedView(int position) {
        this.currentSelectedPosition = position;
        //notifyDataSetChanged();
    }

    public void setOnItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }


    public void expand(final View v) {


        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? WindowManager.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Expansion speed of 1dp/ms
      //  a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        int s= (int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density);
        System.out.println("value"+s);
        a.setDuration(500);
        v.startAnimation(a);
    }


    public void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Collapse speed of 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);

    }

    public void new_collapse(final View v, int duration, int targetHeight) {

        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);

        int prevHeight  = v.getHeight();
        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight);
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (int) animation.getAnimatedValue();
                v.requestLayout();
            }
        });
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.setDuration(duration);
        valueAnimator.start();
    }

    public void slideUp(View view){
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,  // fromYDelta
                view.getHeight());                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }
    public interface SetOnClickInterface{
        void onParentClickListener(String rootId,int position);
    }
}
