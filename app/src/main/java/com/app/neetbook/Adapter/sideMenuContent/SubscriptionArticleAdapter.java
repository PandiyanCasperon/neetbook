package com.app.neetbook.Adapter.sideMenuContent;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;


import com.app.neetbook.Adapter.InnerImageListAdapter;
import com.app.neetbook.Model.sidemenuFromContent.SubscriptionArticleBody;
import com.app.neetbook.Model.sidemenuFromContent.SubscriptionArticleHeader;
import com.app.neetbook.R;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.widgets.CustomTextViewBold;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

public class SubscriptionArticleAdapter extends RecyclerView.Adapter<ViewHolder> {

  private static final int HEADER = 0;
  private static final int ITEM = 1;
  private Context context;
  private List<Object> subscriptionList;
  SessionManager sessionManager;
  String strName;
  Typeface tfBold,tfMedium,tfRegular;
  LinearSnapHelper snapHelper;
  public SubscriptionArticleAdapter(Context context,
      ArrayList<Object> subscriptionList) {
    this.context = context;
    this.subscriptionList = subscriptionList;
    this.sessionManager = new SessionManager(this.context);
    tfBold  = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaBold.otf");
    tfMedium  = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaSemibold.otf");
    tfRegular  = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaRegular.otf");
    createSnap();
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    ViewHolder viewHolder = null;
    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater
            .inflate(R.layout.mobile_subscription_article_item, parent, false);
        viewHolder = new ArticlesViewHolder(view);

    return viewHolder;
  }

  @SuppressLint("RestrictedApi")
  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

    final ArticlesViewHolder pointsViewHolder = (ArticlesViewHolder) holder;
    SubscriptionArticleBody itemBody = (SubscriptionArticleBody) subscriptionList.get(position);
    pointsViewHolder.points_header_text.setText(itemBody.titile);
    pointsViewHolder.article_text.setText((Html.fromHtml(itemBody.points)));
    pointsViewHolder.innerRecycler.setVisibility(itemBody.strImages.size()>0 ? View.VISIBLE : View.GONE);
    if(context.getResources().getBoolean(R.bool.isTablet))
      pointsViewHolder.llRecyclerView.setVisibility(itemBody.strImages.size()>0 ? View.VISIBLE : View.GONE);
    ArrayList<String> singleSectionItems = itemBody.strImages;
    InnerImageListAdapter innerImageListAdapter = new InnerImageListAdapter(context,singleSectionItems,itemBody.titile);
    pointsViewHolder.innerRecycler.setHasFixedSize(true);
    pointsViewHolder.innerRecycler.setLayoutManager(context.getResources().getBoolean(R.bool.isTablet)?new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false): new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
    if(!context.getResources().getBoolean(R.bool.isTablet))
      snapHelper.attachToRecyclerView(pointsViewHolder.innerRecycler);
    pointsViewHolder.innerRecycler.setAdapter(innerImageListAdapter);


    setFontSize(pointsViewHolder);
  }

  private void setFontSize(ArticlesViewHolder holder) {
    if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
      holder.points_header_text.setTextAppearance(context,R.style.textViewSmallArticleHeader);
      holder.article_text.setTextAppearance(context,R.style.textViewSmallArticleContent);
    }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
      holder.points_header_text.setTextAppearance(context,R.style.textViewSmallArticleHeader);
      holder.article_text.setTextAppearance(context,R.style.textViewMediumArticleContent);
    }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
      holder.points_header_text.setTextAppearance(context,R.style.textViewSmallArticleHeader);
      holder.article_text.setTextAppearance(context,R.style.textViewLargeArticleContent);
    }

    holder.points_header_text.setTypeface(tfMedium);
    holder.article_text.setTypeface(tfRegular);
  }

  @Override
  public int getItemCount() {
    return subscriptionList.size();
  }
/*
  @Override
  public int getItemViewType(int position) {
    if (subscriptionList.get(position) instanceof SubscriptionArticleHeader) {
      return HEADER;
    } else if (subscriptionList.get(position) instanceof SubscriptionArticleBody) {
      return ITEM;
    }
    return -1;
  }*/

  private void createSnap() {
    snapHelper = new LinearSnapHelper() {
      @Override
      public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
        int targetPosition = -1;
        try {
          View centerView = findSnapView(layoutManager);
          if (centerView == null)
            return RecyclerView.NO_POSITION;

          int position = layoutManager.getPosition(centerView);

          if (layoutManager.canScrollHorizontally()) {
            if (velocityX < 0) {
              targetPosition = position - 1;
            } else {
              targetPosition = position + 1;
            }
          }

          if (layoutManager.canScrollVertically()) {
            if (velocityY < 0) {
              targetPosition = position - 1;
            } else {
              targetPosition = position + 1;
            }
          }

          final int firstItem = 0;
          final int lastItem = layoutManager.getItemCount() - 1;
          targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

        }catch (Exception e)
        {
          e.printStackTrace();
        }
        return targetPosition;
      }
    };
  }
  class ArticlesViewHolder extends ViewHolder {

    private TextView points_header_text;
    TextView article_text;
    ImageView ivLine, ivCircle;
    LinearLayout article_image_container;
    RecyclerView innerRecycler;
    RelativeLayout llRecyclerView;
    private ArticlesViewHolder(@NonNull final View itemView) {
      super(itemView);
      points_header_text = itemView.findViewById(R.id.points_header_text);
      article_text = itemView.findViewById(R.id.article_text);
      ivCircle = itemView.findViewById(R.id.ivCircle);
      ivLine = itemView.findViewById(R.id.ivLine);
      innerRecycler = itemView.findViewById(R.id.innerRecycler);
      llRecyclerView = itemView.findViewById(R.id.llRecyclerView);

    }

  }
}
