package com.app.neetbook.Adapter.sideMenuContent;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


import com.app.neetbook.Model.sidemenuFromContent.CategoryArticle;
import com.app.neetbook.Model.sidemenuFromContent.CategoryMcq;
import com.app.neetbook.R;
import com.app.neetbook.Utils.widgets.CustomTextView;
import com.app.neetbook.Utils.widgets.CustomTextViewMedium;
import com.app.neetbook.View.SideMenu.SubscriptionArticleActivity;
import com.app.neetbook.View.SideMenu.SubscriptionDetailActivity;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

public class SubscriptionCategoryMcqAdapter extends RecyclerView.Adapter<SubscriptionCategoryMcqAdapter.McqViewHolder> {

  private Context context;
  private ArrayList<CategoryArticle> itemList;
  private  int currentPosition = -1;
  private  int chileSeletedPosition = -1;
  private int superChileSeletedPosition = -1;
  Animation slideDown ;
  private String strSubId = "",strSubName = "";
  public SubscriptionCategoryMcqAdapter(Context context,
      ArrayList<CategoryArticle> categoryList, String strSubId, String strSubName) {
    this.context = context;
    this.itemList = categoryList;
    this.strSubId = strSubId;
    this.strSubName = strSubName;
    currentPosition = -1;
    slideDown = AnimationUtils.loadAnimation(context, R.anim.articleshowchildanimation);
  }

  @NonNull
  @Override
  public McqViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
    View view = layoutInflater.inflate(R.layout.subscription_category_mcq_item,
        parent, false);
    return new McqViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull McqViewHolder holder, final int position) {
    CategoryArticle mcq = itemList.get(position);
    holder.title.setText(mcq.chapt_name);
    Picasso.with(context).load(IConstant.BaseUrl+mcq.images).into(holder.imageView14);
    holder.textView17.setText(String.valueOf(position+1));
    holder.llView.setVisibility(View.GONE);
    setChildView(position,holder,mcq);
    holder.title.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if(currentPosition == position)
        {
          currentPosition = -1;
          chileSeletedPosition = -1;
          notifyDataSetChanged();
        }else {
          currentPosition = position;
          chileSeletedPosition = -1;
          notifyDataSetChanged();
        }
      }
    });

    holder.llView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

      }
    });

  }
  private void setChildView(final int position, final McqViewHolder holder, final CategoryArticle article) {
    if(currentPosition == position)
    {
      holder.llChildHead.removeAllViews();
      holder.llView.setVisibility(View.VISIBLE);
      holder.ivExpand.setVisibility(View.GONE);
      holder.ivCollapse.setVisibility(View.VISIBLE);
      if(chileSeletedPosition == -1 && superChileSeletedPosition == -1)
        holder.llChildHead.startAnimation(slideDown);
      if(article.headDetailsArrayList.size()>0)
      {
        for(int i=0;i<article.headDetailsArrayList.size();i++)
        {
          View ChildHeadView = LayoutInflater.from(context).inflate(R.layout.mcq_chiled_item,null);
          ImageView imgArticleChildHead = ChildHeadView.findViewById(R.id.imgArticleChildHead);
          TextView txt_article_child_head = ChildHeadView.findViewById(R.id.txt_article_child_head);
          final LinearLayoutCompat llChildOfChild = ChildHeadView.findViewById(R.id.llChildOfChild);
          imgArticleChildHead.setVisibility(article.headDetailsArrayList.get(i).isSH.equals("0") && superChileSeletedPosition== -1 && i== chileSeletedPosition?View.VISIBLE:View.INVISIBLE);
          txt_article_child_head.setTextColor(article.headDetailsArrayList.get(i).isSH.equals("0") && superChileSeletedPosition== -1 && i== chileSeletedPosition?context.getResources().getColor(R.color.mcq_blue):context.getResources().getColor(R.color.black));
          final int finalI = i;
          txt_article_child_head.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              chileSeletedPosition = finalI;
              superChileSeletedPosition = -1;
              setChildView(position,holder,article);
              notifyDataSetChanged();
              if(article.headDetailsArrayList.get(finalI).isSH.equals("0")) {
                TabMainActivity.isReadPage = true;
                SubscriptionDetailActivity.mcqHeadList = article.headDetailsArrayList;
                context.startActivity(new Intent(context, SubscriptionDetailActivity.class).putExtra("subject_id", strSubId).putExtra("subject_name", strSubName).putExtra("chapter_name", article.short_name).putExtra("head_id", article.headDetailsArrayList.get(finalI).headData.get(0)._id).putExtra("loading_type", "1"));
              }
            }
          });

          if(article.headDetailsArrayList.get(i).isSH.equals("0"))
          {
            txt_article_child_head.setText(article.headDetailsArrayList.get(i).headData.get(0).head_name);
            llChildOfChild.setVisibility(View.GONE);
          }else
          {
            txt_article_child_head.setText(article.headDetailsArrayList.get(i).superDetails.supr_head_name);
            llChildOfChild.setVisibility(View.VISIBLE);

            if(article.headDetailsArrayList.get(i).headData.size()>0)
            {
              llChildOfChild.removeAllViews();
              for(int j=0;j<article.headDetailsArrayList.get(i).headData.size();j++)
              {
                View childOfChildView = LayoutInflater.from(context).inflate(R.layout.mcq_super_child,null);
                TextView txtArticleSuperHeading = childOfChildView.findViewById(R.id.txtArticleSuperHeading);
                ImageView imgArticleSuperChild = childOfChildView.findViewById(R.id.imgArticleSuperChild);
                imgArticleSuperChild.setVisibility(chileSeletedPosition == i && j==superChileSeletedPosition?View.VISIBLE:View.INVISIBLE);
                txtArticleSuperHeading.setTextColor(chileSeletedPosition == i && j==superChileSeletedPosition?context.getResources().getColor(R.color.mcq_blue): Color.BLACK);
                txtArticleSuperHeading.setText(article.headDetailsArrayList.get(i).headData.get(j).head_name);
                final int finalJ = j;
                txtArticleSuperHeading.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                    TabMainActivity.isReadPage = true;
                    superChileSeletedPosition = finalJ;
                    chileSeletedPosition = finalI;
                    setChildView(position,holder,article);
                    notifyDataSetChanged();
                    context.startActivity(new Intent(context, SubscriptionDetailActivity.class).putExtra("subject_id",article.headDetailsArrayList.get(finalI).headData.get(finalJ).head_name).putExtra("subject_name",strSubName).putExtra("chapter_name", article.short_name).putExtra("head_id",article.headDetailsArrayList.get(finalI).headData.get(finalJ)._id).putExtra("loading_type", "1"));
                  }
                });
                llChildOfChild.addView(childOfChildView);
              }
            }
          }



          holder.llChildHead.addView(ChildHeadView);
        }
      }



    }else{
      holder.ivExpand.setVisibility(View.VISIBLE);
      holder.ivCollapse.setVisibility(View.GONE);
    }
  }
  @Override
  public int getItemCount() {
    return itemList.size();
  }


  class McqViewHolder extends ViewHolder {
    private LinearLayoutCompat llChildHead;
    private TextView textView17,title;
    private ConstraintLayout llView;
    ImageView ivCollapse,ivExpand,imageView14;
    private McqViewHolder(@NonNull final View itemView) {
      super(itemView);
      title = itemView.findViewById(R.id.tvCategoryTitle);
      llView = itemView.findViewById(R.id.llView);
      llChildHead = itemView.findViewById(R.id.llChildHead);
      ivCollapse = itemView.findViewById(R.id.ivCollapse);
      ivExpand = itemView.findViewById(R.id.ivExpand);
      textView17 = itemView.findViewById(R.id.textView17);
      imageView14 = itemView.findViewById(R.id.imageView14);
    }
  }


}
