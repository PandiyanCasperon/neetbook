package com.app.neetbook.Adapter.sideMenuContent;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.app.neetbook.Adapter.subscription.SubscriptionAdapter;
import com.app.neetbook.Interfaces.onAttemptingTestListener;
import com.app.neetbook.Model.sidemenuFromContent.SubscriptionArticleBody;
import com.app.neetbook.Model.sidemenuFromContent.SubscriptionArticleHeader;
import com.app.neetbook.Model.sidemenuFromContent.SubscriptionTest;
import com.app.neetbook.R;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.widgets.CustomTextViewBold;
import com.app.neetbook.Utils.widgets.CustomTextViewMedium;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;

public class SubscriptionTestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private Context context;
  private ArrayList<SubscriptionTest> subscriptionList;
  private OnItemClickListener listener;
  private onAttemptingTestListener onAttemptingTestListener;
  private SessionManager sessionManager;
  private boolean isCompleted;
  Typeface font;
  Typeface tfBold,tfMedium,tfRegular;
  Drawable mIcon;

  public SubscriptionTestAdapter(Context context,
      ArrayList<SubscriptionTest> subscriptionList,boolean isCompleted) {
    this.context = context;
    this.subscriptionList = subscriptionList;
    sessionManager = new SessionManager(this.context);
    this.isCompleted = isCompleted;
    font = Typeface.createFromAsset(this.context.getAssets(), "fonts/ProximaNovaRegular.otf");
    tfBold  = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaBold.otf");
    tfMedium  = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaSemibold.otf");
    tfRegular  = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaRegular.otf");
    mIcon = ContextCompat.getDrawable(this.context, R.drawable.icn_radio_selected);
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
    //mobile_subscription_test_completed_item
    RecyclerView.ViewHolder viewHolder = null;
    Log.e("viewType",""+viewType);
    switch (viewType) {
      case 1:
        View view = layoutInflater
                .inflate(R.layout.mobile_subscription_test_item, parent, false);
        viewHolder = new TestViewHolder(view);
        break;

      case 2:
        View viewHeader = layoutInflater
                .inflate(R.layout.mobile_subscription_test_completed_item, parent, false);
        viewHolder = new TestCompleteViewHolder(viewHeader);
        break;
    }
    return viewHolder;
  }
  @SuppressLint("RestrictedApi")
  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
    final SubscriptionTest subscription = subscriptionList.get(position);
    switch (getItemViewType(position)) {
      case 1: {
        final TestViewHolder holder1 = (TestViewHolder) holder;

        if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
          holder1.txt_answer_1.setTextAppearance(context,R.style.textViewSmallMcqOption);
          holder1.txt_answer_2.setTextAppearance(context,R.style.textViewSmallMcqOption);
          holder1.txt_answer_3.setTextAppearance(context,R.style.textViewSmallMcqOption);
          holder1.txt_answer_4.setTextAppearance(context,R.style.textViewSmallMcqOption);
          holder1.txt_answer_5.setTextAppearance(context,R.style.textViewSmallMcqOption);
          holder1.txt_option_1.setTextAppearance(context,R.style.textViewSmallMcqOption);
          holder1.txt_option_2.setTextAppearance(context,R.style.textViewSmallMcqOption);
          holder1.txt_option_3.setTextAppearance(context,R.style.textViewSmallMcqOption);
          holder1.txt_option_4.setTextAppearance(context,R.style.textViewSmallMcqOption);
          holder1.txt_option_5.setTextAppearance(context,R.style.textViewSmallMcqOption);
          holder1.title.setTextAppearance(context,R.style.textViewSmallMcqQuestion);
          holder1.questionCount.setTextAppearance(context,R.style.textViewSmallMcqQuestion);

        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
          holder1.txt_answer_1.setTextAppearance(context,R.style.textViewMediumMcqOption);
          holder1.txt_answer_2.setTextAppearance(context,R.style.textViewMediumMcqOption);
          holder1.txt_answer_3.setTextAppearance(context,R.style.textViewMediumMcqOption);
          holder1.txt_answer_4.setTextAppearance(context,R.style.textViewMediumMcqOption);
          holder1.txt_answer_5.setTextAppearance(context,R.style.textViewMediumMcqOption);
          holder1.txt_option_1.setTextAppearance(context,R.style.textViewMediumMcqOption);
          holder1.txt_option_2.setTextAppearance(context,R.style.textViewMediumMcqOption);
          holder1.txt_option_3.setTextAppearance(context,R.style.textViewMediumMcqOption);
          holder1.txt_option_4.setTextAppearance(context,R.style.textViewMediumMcqOption);
          holder1.txt_option_5.setTextAppearance(context,R.style.textViewMediumMcqOption);
          holder1.title.setTextAppearance(context,R.style.textViewMediumMcqQuestion);
          holder1.questionCount.setTextAppearance(context,R.style.textViewMediumMcqQuestion);

        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
          holder1.txt_answer_1.setTextAppearance(context,R.style.textViewLargeMcqOption);
          holder1.txt_answer_2.setTextAppearance(context,R.style.textViewLargeMcqOption);
          holder1.txt_answer_3.setTextAppearance(context,R.style.textViewLargeMcqOption);
          holder1.txt_answer_4.setTextAppearance(context,R.style.textViewLargeMcqOption);
          holder1.txt_answer_5.setTextAppearance(context,R.style.textViewLargeMcqOption);
          holder1.txt_option_1.setTextAppearance(context,R.style.textViewLargeMcqOption);
          holder1.txt_option_2.setTextAppearance(context,R.style.textViewLargeMcqOption);
          holder1.txt_option_3.setTextAppearance(context,R.style.textViewLargeMcqOption);
          holder1.txt_option_4.setTextAppearance(context,R.style.textViewLargeMcqOption);
          holder1.txt_option_5.setTextAppearance(context,R.style.textViewLargeMcqOption);
          holder1.title.setTextAppearance(context,R.style.textViewLargeMcqQuestion);
          holder1.questionCount.setTextAppearance(context,R.style.textViewLargeMcqQuestion);

        }

        holder1.title.setTypeface(tfMedium);
        holder1.questionCount.setTypeface(tfMedium);
        holder1.txt_answer_5.setTypeface(tfRegular);
        holder1.txt_answer_4.setTypeface(tfRegular);
        holder1.txt_answer_3.setTypeface(tfRegular);
        holder1.txt_answer_2.setTypeface(tfRegular);
        holder1.txt_answer_1.setTypeface(tfRegular);
        holder1.txt_option_1.setTypeface(tfRegular);
        holder1.txt_option_2.setTypeface(tfRegular);
        holder1.txt_option_3.setTypeface(tfRegular);
        holder1.txt_option_4.setTypeface(tfRegular);
        holder1.txt_option_5.setTypeface(tfRegular);

        holder1.imageView.setVisibility(subscription.getImageUrls().size()>0?View.VISIBLE:View.GONE);
        holder1.title.setText(subscription.getQuestionTitle());
        holder1.questionCount.setText(subscription.getQuestionCount()+".");
        holder1.txt_answer_1.setText(subscription.getQuestionOptionFirst());
        holder1.txt_answer_2.setText(subscription.getQuestionOptionSecond());
        holder1.txt_answer_3.setText(subscription.getQuestionOptionThird());
        holder1.txt_answer_4.setText(subscription.getQuestionOptionFourth());
        holder1.txt_answer_5.setText(subscription.getQuestionOptionFifth());
        holder1.llOption5.setVisibility(subscription.getQuestionOptionFifth()!= null && !subscription.getQuestionOptionFifth().isEmpty() && !subscription.getQuestionOptionFifth().equalsIgnoreCase("E. ") ? View.VISIBLE : View.GONE);
        Log.e("E",subscription.getQuestionOptionFifth());

        Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionOne);
        Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionTwo);
        Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionThree);
        Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionFour);
        Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionFive);

            switch (subscription.getUserAnswer()) {

              case "A": {
                Picasso.with(context).load(R.drawable.icn_radio_selected).into(holder1.imgTestOptionOne);
                if(isCompleted)
                  holder1.txt_answer_1.setTextColor(subscription.getUserAnswer().equalsIgnoreCase(subscription.getAnswer())?context.getResources().getColor(R.color.choiceGreen):context.getResources().getColor(R.color.test_completed_red));
              }
                break;

              case "B": {
               Picasso.with(context).load(R.drawable.icn_radio_selected).into(holder1.imgTestOptionTwo);
                if(isCompleted)
                  holder1.txt_answer_2.setTextColor(subscription.getUserAnswer().equalsIgnoreCase(subscription.getAnswer())?context.getResources().getColor(R.color.choiceGreen):context.getResources().getColor(R.color.test_completed_red));

              }
                break;
              case "C":{
                Picasso.with(context).load(R.drawable.icn_radio_selected).into(holder1.imgTestOptionThree);
                if(isCompleted)
                  holder1.txt_answer_3.setTextColor(subscription.getUserAnswer().equalsIgnoreCase(subscription.getAnswer())?context.getResources().getColor(R.color.choiceGreen):context.getResources().getColor(R.color.test_completed_red));

              }
                break;
              case "D": {
                Picasso.with(context).load(R.drawable.icn_radio_selected).into(holder1.imgTestOptionFour);
                if(isCompleted)
                  holder1.txt_answer_4.setTextColor(subscription.getUserAnswer().equalsIgnoreCase(subscription.getAnswer())?context.getResources().getColor(R.color.choiceGreen):context.getResources().getColor(R.color.test_completed_red));

              }
                break;
              case "E": {
                Picasso.with(context).load(R.drawable.icn_radio_selected).into(holder1.imgTestOptionFive);
                if(isCompleted)
                  holder1.txt_answer_5.setTextColor(subscription.getUserAnswer().equalsIgnoreCase(subscription.getAnswer())?context.getResources().getColor(R.color.choiceGreen):context.getResources().getColor(R.color.test_completed_red));
                }
                break;
            }

        if(isCompleted)
        {
            switch (subscription.getAnswer()) {

              case "A": {
                holder1.txt_answer_1.setTextColor(context.getResources().getColor(R.color.choiceGreen));

                /*SpannableString ss = new SpannableString(subscription.getQuestionOptionFirst());
                ss.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.choiceGreen)), 0, 1, 0);
                holder1.optionOne.setText(ss);*/
              }
                break;
              case "B": {
                holder1.txt_answer_2.setTextColor(context.getResources().getColor(R.color.choiceGreen));

              }
                break;
              case "C":{
                holder1.txt_answer_3.setTextColor(context.getResources().getColor(R.color.choiceGreen));

              }
                break;
              case "D": {
                holder1.txt_answer_4.setTextColor(context.getResources().getColor(R.color.choiceGreen));

              }
                break;
              case "E": {
                holder1.txt_answer_5.setTextColor(context.getResources().getColor(R.color.choiceGreen));

              }
                break;
            }
          }
        if(!isCompleted) {
          holder1.llOption1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              subscription.setUserAnswer(subscription.getUserAnswer().equalsIgnoreCase("A")?"":"A");
              onAttemptingTestListener.onSelectedAnswer(position, subscription.getUserAnswer());

              Picasso.with(context).load(subscription.getUserAnswer().equalsIgnoreCase("A")?R.drawable.icn_radio_selected : R.drawable.icn_radio_unselected).into(holder1.imgTestOptionOne);
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionTwo);
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionThree);
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionFour);
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionFive);
              //notifyDataSetChanged();
            }
          });
          holder1.llOption2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              subscription.setUserAnswer(subscription.getUserAnswer().equalsIgnoreCase("B")?"":"B");
              onAttemptingTestListener.onSelectedAnswer(position, subscription.getUserAnswer());
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionOne);
              Picasso.with(context).load(subscription.getUserAnswer().equalsIgnoreCase("B")?R.drawable.icn_radio_selected : R.drawable.icn_radio_unselected).into(holder1.imgTestOptionTwo);
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionThree);
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionFour);
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionFive);
              //notifyDataSetChanged();
            }
          });
          holder1.llOption3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              subscription.setUserAnswer(subscription.getUserAnswer().equalsIgnoreCase("C")?"":"C");
              onAttemptingTestListener.onSelectedAnswer(position, subscription.getUserAnswer());
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionOne);
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionTwo);
              Picasso.with(context).load(subscription.getUserAnswer().equalsIgnoreCase("C")?R.drawable.icn_radio_selected : R.drawable.icn_radio_unselected).into(holder1.imgTestOptionThree);
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionFour);
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionFive);
             // notifyDataSetChanged();
            }
          });
          holder1.llOption4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              subscription.setUserAnswer(subscription.getUserAnswer().equalsIgnoreCase("D")?"":"D");
              onAttemptingTestListener.onSelectedAnswer(position, subscription.getUserAnswer());
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionOne);
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionTwo);
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionThree);
              Picasso.with(context).load(subscription.getUserAnswer().equalsIgnoreCase("D")?R.drawable.icn_radio_selected : R.drawable.icn_radio_unselected).into(holder1.imgTestOptionFour);
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionFive);
              //notifyDataSetChanged();
            }
          });
          holder1.llOption5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              subscription.setUserAnswer(subscription.getUserAnswer().equalsIgnoreCase("E")?"":"E");
              onAttemptingTestListener.onSelectedAnswer(position, subscription.getUserAnswer());
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionOne);
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionTwo);
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionThree);
              Picasso.with(context).load(R.drawable.icn_radio_unselected).into(holder1.imgTestOptionFour);
              Picasso.with(context).load(subscription.getUserAnswer().equalsIgnoreCase("E")?R.drawable.icn_radio_selected : R.drawable.icn_radio_unselected).into(holder1.imgTestOptionFive);
              //notifyDataSetChanged();
            }
          });
        }

      }
      break;
      case 2: {
        final TestCompleteViewHolder holder1 = (TestCompleteViewHolder) holder;
        if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
          holder1.optionOne.setTextAppearance(context, R.style.textViewSmallMcqOption);
          holder1.txt_option_1.setTextAppearance(context, R.style.textViewSmallMcqOption);
          holder1.txt_option_2.setTextAppearance(context, R.style.textViewSmallMcqOption);
          holder1.txt_option_3.setTextAppearance(context, R.style.textViewSmallMcqOption);
          holder1.txt_option_4.setTextAppearance(context, R.style.textViewSmallMcqOption);
          holder1.txt_option_5.setTextAppearance(context, R.style.textViewSmallMcqOption);
          holder1.optionTwo.setTextAppearance(context, R.style.textViewSmallMcqOption);
          holder1.optionThree.setTextAppearance(context, R.style.textViewSmallMcqOption);
          holder1.optionFour.setTextAppearance(context, R.style.textViewSmallMcqOption);
          holder1.optionFive.setTextAppearance(context, R.style.textViewSmallMcqOption);
          holder1.title.setTextAppearance(context, R.style.textViewSmallMcqQuestion);
          holder1.questionCount.setTextAppearance(context, R.style.textViewSmallMcqQuestion);

        } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
          holder1.optionOne.setTextAppearance(context, R.style.textViewMediumMcqOption);
          holder1.optionTwo.setTextAppearance(context, R.style.textViewMediumMcqOption);
          holder1.optionThree.setTextAppearance(context, R.style.textViewMediumMcqOption);
          holder1.optionFour.setTextAppearance(context, R.style.textViewMediumMcqOption);
          holder1.optionFour.setTextAppearance(context, R.style.textViewMediumMcqOption);
          holder1.optionFive.setTextAppearance(context, R.style.textViewMediumMcqOption);
          holder1.title.setTextAppearance(context, R.style.textViewMediumMcqQuestion);
          holder1.questionCount.setTextAppearance(context, R.style.textViewMediumMcqQuestion);
          holder1.txt_option_1.setTextAppearance(context, R.style.textViewMediumMcqOption);
          holder1.txt_option_2.setTextAppearance(context, R.style.textViewMediumMcqOption);
          holder1.txt_option_3.setTextAppearance(context, R.style.textViewMediumMcqOption);
          holder1.txt_option_4.setTextAppearance(context, R.style.textViewMediumMcqOption);
          holder1.txt_option_5.setTextAppearance(context, R.style.textViewMediumMcqOption);

        } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
          holder1.optionOne.setTextAppearance(context, R.style.textViewLargeMcqOption);
          holder1.optionTwo.setTextAppearance(context, R.style.textViewLargeMcqOption);
          holder1.optionThree.setTextAppearance(context, R.style.textViewLargeMcqOption);
          holder1.optionFour.setTextAppearance(context, R.style.textViewLargeMcqOption);
          holder1.optionFour.setTextAppearance(context, R.style.textViewLargeMcqOption);
          holder1.optionFive.setTextAppearance(context, R.style.textViewLargeMcqOption);
          holder1.title.setTextAppearance(context, R.style.textViewLargeMcqQuestion);
          holder1.questionCount.setTextAppearance(context, R.style.textViewLargeMcqQuestion);
          holder1.txt_option_1.setTextAppearance(context, R.style.textViewLargeMcqOption);
          holder1.txt_option_2.setTextAppearance(context, R.style.textViewLargeMcqOption);
          holder1.txt_option_3.setTextAppearance(context, R.style.textViewLargeMcqOption);
          holder1.txt_option_4.setTextAppearance(context, R.style.textViewLargeMcqOption);
          holder1.txt_option_5.setTextAppearance(context, R.style.textViewLargeMcqOption);

        }
        holder1.title.setTypeface(tfMedium);
        holder1.questionCount.setTypeface(tfMedium);
        holder1.optionFive.setTypeface(tfRegular);
        holder1.optionFour.setTypeface(tfRegular);
        holder1.optionThree.setTypeface(tfRegular);
        holder1.optionTwo.setTypeface(tfRegular);
        holder1.optionOne.setTypeface(tfRegular);
        holder1.txt_option_1.setTypeface(tfRegular);
        holder1.txt_option_2.setTypeface(tfRegular);
        holder1.txt_option_3.setTypeface(tfRegular);
        holder1.txt_option_4.setTypeface(tfRegular);
        holder1.txt_option_5.setTypeface(tfRegular);
        holder1.imageView.setVisibility(subscription.getImageUrls().size()>0?View.VISIBLE:View.GONE);
        holder1.title.setText(subscription.getQuestionTitle());
        holder1.questionCount.setText(subscription.getQuestionCount()+".");
        holder1.optionOne.setText(subscription.getQuestionOptionFirst());
        holder1.optionTwo.setText(subscription.getQuestionOptionSecond());
        holder1.optionThree.setText(subscription.getQuestionOptionThird());
        holder1.optionFour.setText(subscription.getQuestionOptionFourth());
        holder1.optionFive.setText(subscription.getQuestionOptionFifth());
        holder1.optionFive.setVisibility(subscription.getQuestionOptionFifth() != null && !subscription.getQuestionOptionFifth().isEmpty() ? View.VISIBLE : View.GONE);

        Picasso.with(context).load(subscription.getUserAnswer() != null && !subscription.getUserAnswer().isEmpty() && subscription.getUserAnswer().contains("A") ? R.drawable.yellow_chacked : R.drawable.yellow_unchacked).into(holder1.checkBoxOne);
        Picasso.with(context).load(subscription.getUserAnswer() != null && !subscription.getUserAnswer().isEmpty() && subscription.getUserAnswer().contains("B") ? R.drawable.yellow_chacked : R.drawable.yellow_unchacked).into(holder1.checkBoxTwo);
        Picasso.with(context).load(subscription.getUserAnswer() != null && !subscription.getUserAnswer().isEmpty() && subscription.getUserAnswer().contains("C") ? R.drawable.yellow_chacked : R.drawable.yellow_unchacked).into(holder1.checkBoxThree);
        Picasso.with(context).load(subscription.getUserAnswer() != null && !subscription.getUserAnswer().isEmpty() && subscription.getUserAnswer().contains("D") ? R.drawable.yellow_chacked : R.drawable.yellow_unchacked).into(holder1.checkBoxFour);
        Picasso.with(context).load(subscription.getUserAnswer() != null && !subscription.getUserAnswer().isEmpty() && subscription.getUserAnswer().contains("E") ? R.drawable.yellow_chacked : R.drawable.yellow_unchacked).into(holder1.checkBoxFive);

        if (!isCompleted) {
          holder1.llOptionOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //subscription.setUserAnswer(subscription.getUserAnswer()+"A");
              onAttemptingTestListener.onMultiChoiceSelectedAnswer(position, "A");
            }
          });
          holder1.llOptionTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //subscription.setUserAnswer(subscription.getUserAnswer()+"B");
              onAttemptingTestListener.onMultiChoiceSelectedAnswer(position, "B");
            }
          });
          holder1.llOptionThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //subscription.setUserAnswer(subscription.getUserAnswer()+"C");
              onAttemptingTestListener.onMultiChoiceSelectedAnswer(position, "C");
            }
          });
          holder1.llOptionFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //subscription.setUserAnswer(subscription.getUserAnswer()+"D");
              onAttemptingTestListener.onMultiChoiceSelectedAnswer(position, "D");
            }
          });

          holder1.llOptionFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //subscription.setUserAnswer(subscription.getUserAnswer()+"D");
              onAttemptingTestListener.onMultiChoiceSelectedAnswer(position, "E");
            }
          });
        }else
        {

          ArrayList<String> AnswerChars = new ArrayList<>();
          ArrayList<String> UserAnswerChars = new ArrayList<>();
          ArrayList<String> CorrectAnswer = new ArrayList<>();
          ArrayList<String> WrongAnswer = new ArrayList<>();
          for (char ch : subscription.getAnswer().replaceAll(",","").toCharArray()) {
            AnswerChars.add(String.valueOf(ch));
          }

          for (char ch : subscription.getUserAnswer().toCharArray()) {
            UserAnswerChars.add(String.valueOf(ch));
          }


          for (String person2 : UserAnswerChars) {
            // Loop arrayList1 items
            boolean found = false;
            for (String person1 : AnswerChars) {
              if (person2.equalsIgnoreCase(person1)) {
                found = true;
                CorrectAnswer.add(person2);
              }
            }
            if (!found) {
              WrongAnswer.add(person2);
            }
          }

          for(int i=0;i<AnswerChars.size();i++)
          {

              switch (AnswerChars.get(i))
              {
                case "A":
                  holder1.optionOne.setTextColor(context.getResources().getColor(R.color.choiceGreen));
                  break;

                case "B":
                  holder1.optionTwo.setTextColor(context.getResources().getColor(R.color.choiceGreen));
                  break;
                case "C":
                  holder1.optionThree.setTextColor(context.getResources().getColor(R.color.choiceGreen));
                  break;

                case "D":
                  holder1.optionFour.setTextColor(context.getResources().getColor(R.color.choiceGreen));
                  break;
                case "E":
                  holder1.optionFive.setTextColor(context.getResources().getColor(R.color.choiceGreen));
                  break;
              }

          }
          for(int i=0;i<CorrectAnswer.size();i++)
          {

              switch (CorrectAnswer.get(i))
              {
                case "A":
                  Picasso.with(context).load(subscription.getUserAnswer() != null && !subscription.getUserAnswer().isEmpty() && subscription.getUserAnswer().contains("A") ? R.drawable.green_checked_box : R.drawable.yellow_unchacked).into(holder1.checkBoxOne);
                  break;

                case "B":
                  Picasso.with(context).load(subscription.getUserAnswer() != null && !subscription.getUserAnswer().isEmpty() && subscription.getUserAnswer().contains("B") ? R.drawable.green_checked_box : R.drawable.yellow_unchacked).into(holder1.checkBoxTwo);
                  break;
                case "C":
                  Picasso.with(context).load(subscription.getUserAnswer() != null && !subscription.getUserAnswer().isEmpty() && subscription.getUserAnswer().contains("C") ? R.drawable.green_checked_box : R.drawable.yellow_unchacked).into(holder1.checkBoxThree);
                  break;

                case "D":
                  Picasso.with(context).load(subscription.getUserAnswer() != null && !subscription.getUserAnswer().isEmpty() && subscription.getUserAnswer().contains("D") ? R.drawable.green_checked_box : R.drawable.yellow_unchacked).into(holder1.checkBoxFour);
                  break;
                case "E":
                  Picasso.with(context).load(subscription.getUserAnswer() != null && !subscription.getUserAnswer().isEmpty() && subscription.getUserAnswer().contains("E") ? R.drawable.green_checked_box : R.drawable.yellow_unchacked).into(holder1.checkBoxFive);
                  break;
              }

          }

          for(int i=0;i<WrongAnswer.size();i++)
          {

              switch (WrongAnswer.get(i))
              {
                case "A":
                  Picasso.with(context).load(subscription.getUserAnswer() != null && !subscription.getUserAnswer().isEmpty() && subscription.getUserAnswer().contains("A") ? R.drawable.red_checked_box : R.drawable.yellow_unchacked).into(holder1.checkBoxOne);
                  break;

                case "B":
                  Picasso.with(context).load(subscription.getUserAnswer() != null && !subscription.getUserAnswer().isEmpty() && subscription.getUserAnswer().contains("B") ? R.drawable.red_checked_box : R.drawable.yellow_unchacked).into(holder1.checkBoxTwo);
                  break;
                case "C":
                  Picasso.with(context).load(subscription.getUserAnswer() != null && !subscription.getUserAnswer().isEmpty() && subscription.getUserAnswer().contains("C") ? R.drawable.red_checked_box : R.drawable.yellow_unchacked).into(holder1.checkBoxThree);
                  break;

                case "D":
                  Picasso.with(context).load(subscription.getUserAnswer() != null && !subscription.getUserAnswer().isEmpty() && subscription.getUserAnswer().contains("D") ? R.drawable.red_checked_box : R.drawable.yellow_unchacked).into(holder1.checkBoxFour);
                  break;
                case "E":
                  Picasso.with(context).load(subscription.getUserAnswer() != null && !subscription.getUserAnswer().isEmpty() && subscription.getUserAnswer().contains("E") ? R.drawable.red_checked_box : R.drawable.yellow_unchacked).into(holder1.checkBoxFive);
                  break;
              }

          }
        }
      }
      break;
    }
  }

  @Override
  public int getItemCount() {
    return subscriptionList.size();
  }
  @Override
  public int getItemViewType(int position) {
    if (subscriptionList.get(position).getQuestionType().equals("1")) {
      return 1;
    } else if (subscriptionList.get(position).getQuestionType().equals("2")) {
      return 2;
    }
    return -1;
  }
  // Define the method that allows the parent activity or fragment to define the listener
  public void setOnItemClickListener(OnItemClickListener listener) {
    this.listener = listener;
  }

  public interface OnItemClickListener {

    void onItemClick(View itemView, int position);
  }

  class TestViewHolder extends RecyclerView.ViewHolder {

    private CustomTextViewBold title;
    private CustomTextViewBold questionCount;

    LinearLayout llOption1,llOption2,llOption3,llOption4,llOption5;
    private ImageView imgTestOptionOne,imgTestOptionTwo,imgTestOptionThree,imgTestOptionFour,imgTestOptionFive;
    private ImageView imageView;
    TextView txt_answer_1,txt_answer_2,txt_answer_3,txt_answer_4,txt_answer_5;
    TextView txt_option_1,txt_option_2,txt_option_3,txt_option_4,txt_option_5;


    private TestViewHolder(@NonNull final View itemView) {
      super(itemView);

      title = itemView.findViewById(R.id.txt_question_title);
      questionCount = itemView.findViewById(R.id.txt_question_count);
      llOption1 = itemView.findViewById(R.id.llOption1);
      llOption2 = itemView.findViewById(R.id.llOption2);
      llOption3 = itemView.findViewById(R.id.llOption3);
      llOption4 = itemView.findViewById(R.id.llOption4);
      llOption5 = itemView.findViewById(R.id.llOption5);
      imgTestOptionOne = itemView.findViewById(R.id.imgTestOptionOne);
      imgTestOptionTwo = itemView.findViewById(R.id.imgTestOptionTwo);
      imgTestOptionThree = itemView.findViewById(R.id.imgTestOptioThree);
      imgTestOptionFour = itemView.findViewById(R.id.imgTestOptionFour);
      imgTestOptionFive = itemView.findViewById(R.id.imgTestOptionFive);
      txt_answer_1 = itemView.findViewById(R.id.txt_answer_1);
      txt_answer_2 = itemView.findViewById(R.id.txt_answer_2);
      txt_answer_3 = itemView.findViewById(R.id.txt_answer_3);
      txt_answer_4 = itemView.findViewById(R.id.txt_answer_4);
      txt_answer_5 = itemView.findViewById(R.id.txt_answer_5);
      txt_option_1 = itemView.findViewById(R.id.txt_option_1);
      txt_option_2 = itemView.findViewById(R.id.txt_option_2);
      txt_option_3 = itemView.findViewById(R.id.txt_option_3);
      txt_option_4 = itemView.findViewById(R.id.txt_option_4);
      txt_option_5 = itemView.findViewById(R.id.txt_option_5);

      imageView = itemView.findViewById(R.id.imageView);



      imageView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          onAttemptingTestListener.onImageClicked(getAdapterPosition());
        }
      });
      /*final ColorStateList colorStateList = new ColorStateList(
          new int[][]{new int[]{-android.R.attr.state_checked},
              new int[]{android.R.attr.state_checked}
          },
          new int[]{
              R.color.test_completed_yellow,
              R.color.subscription_green,
          }
      );

      mRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
        @SuppressLint("RestrictedApi")
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
          AppCompatRadioButton rb = group.findViewById(checkedId);
          if (null != rb && checkedId != -1) {
            rb.setSupportButtonTintList(colorStateList);
          }
        }
      });*/
    }
  }
  class TestCompleteViewHolder extends RecyclerView.ViewHolder {

    private TextView title;
    private TextView questionCount;
    private TextView optionOne, optionTwo, optionThree, optionFour, optionFive;
    private TextView txt_option_1, txt_option_2, txt_option_3, txt_option_4, txt_option_5;
    private ImageView imageView,checkBoxOne, checkBoxTwo, checkBoxThree, checkBoxFour, checkBoxFive;
    ConstraintLayout llOptionOne,llOptionTwo,llOptionThree,llOptionFour,llOptionFive;

    private TestCompleteViewHolder(@NonNull final View itemView) {
      super(itemView);

      title = itemView.findViewById(R.id.txt_question_title);
      questionCount = itemView.findViewById(R.id.txt_question_count);
      optionOne = itemView.findViewById(R.id.txt_answer_1);
      optionTwo = itemView.findViewById(R.id.txt_answer_2);
      optionThree = itemView.findViewById(R.id.txt_answer_3);
      optionFour = itemView.findViewById(R.id.txt_answer_4);
      optionFive = itemView.findViewById(R.id.txt_answer_5);

      txt_option_1 = itemView.findViewById(R.id.txt_option_1);
      txt_option_2 = itemView.findViewById(R.id.txt_option_2);
      txt_option_3 = itemView.findViewById(R.id.txt_option_3);
      txt_option_4 = itemView.findViewById(R.id.txt_option_4);
      txt_option_5 = itemView.findViewById(R.id.txt_option_5);

      llOptionOne = itemView.findViewById(R.id.llOptionOne);
      llOptionTwo = itemView.findViewById(R.id.llOptionTwo);
      llOptionThree = itemView.findViewById(R.id.llOptionThree);
      llOptionFour = itemView.findViewById(R.id.llOptionFour);
      llOptionFive = itemView.findViewById(R.id.llOptionFive);
      checkBoxOne = itemView.findViewById(R.id.checkbox_option1);
      checkBoxTwo = itemView.findViewById(R.id.checkbox_option2);
      checkBoxThree = itemView.findViewById(R.id.checkbox_option3);
      checkBoxFour = itemView.findViewById(R.id.checkbox_option4);
      checkBoxFive = itemView.findViewById(R.id.checkbox_option5);
      imageView = itemView.findViewById(R.id.imageView);

      imageView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          onAttemptingTestListener.onImageClicked(getAdapterPosition());
        }
      });
    }
  }
  public void setOnAttemptingTestListener(onAttemptingTestListener onAttemptingTestListener)
  {
    this.onAttemptingTestListener = onAttemptingTestListener;
  }
}
