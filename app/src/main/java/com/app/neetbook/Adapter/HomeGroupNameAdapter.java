package com.app.neetbook.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.neetbook.Interfaces.OnGroupMenuSelectedListener;
import com.app.neetbook.Interfaces.OnMobileHomeMenuSelectedListioner;
import com.app.neetbook.Model.Groupslist;
import com.app.neetbook.Model.ReferNowBean;
import com.app.neetbook.R;
import com.app.neetbook.Utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HomeGroupNameAdapter extends RecyclerView.Adapter<HomeGroupNameAdapter.ViewHolder>{
    List<Groupslist> groupChildBeanArrayList;
    Context context;
    OnMobileHomeMenuSelectedListioner onGroupMenuSelectedListener;
    int currentPosition ;
    Typeface tfBold,tfMedium,tfRegular;
    SessionManager sessionManager;

    public HomeGroupNameAdapter(List<Groupslist> groupChildBeanArrayList, Context context,int lastPosition) {

        this.groupChildBeanArrayList = groupChildBeanArrayList;
        this.context = context;
        this.currentPosition = lastPosition;
        sessionManager = new SessionManager(context);
        tfBold  = Typeface.createFromAsset(this.context.getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium  = Typeface.createFromAsset(this.context.getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular  = Typeface.createFromAsset(this.context.getAssets(), "fonts/ProximaNovaRegular.otf");
    }
    @NonNull
    @Override
    public HomeGroupNameAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.homegroupnameadapteritem, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeGroupNameAdapter.ViewHolder holder, final int position) {
        final Groupslist groupChildBean = groupChildBeanArrayList.get(position);

        holder.viewBack.setVisibility(position==groupChildBeanArrayList.size()-1?View.VISIBLE:View.GONE);
        holder.viewFront.setVisibility(position==0?View.VISIBLE:View.GONE);
        if(currentPosition == position){
            holder.llGroupContainer.setBackground(context.getResources().getDrawable(R.drawable.tab_background_selected));
            holder.txtGroupName.setTextColor(context.getResources().getColor(R.color.white));
        }
        else {
            holder.llGroupContainer.setBackground(context.getResources().getDrawable(R.drawable.tab_background_unselected));
            holder.txtGroupName.setTextColor(context.getResources().getColor(R.color.group_unselected_border));
        }
      /*  if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
            holder.txtGroupName.setTextAppearance(context,R.style.textViewSmallArticleContent);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
            holder.txtGroupName.setTextAppearance(context,R.style.textViewSmallArticleContent);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
            holder.txtGroupName.setTextAppearance(context,R.style.textViewSmallArticleContent);
        }*/
        holder.txtGroupName.setTypeface(tfMedium);
        holder.txtGroupName.setText(groupChildBean.getHomeshortName());
        holder.llGroupContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onGroupMenuSelectedListener.onGroupSelected(groupChildBean.getHomegroupid(),position);
                currentPosition = position;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return groupChildBeanArrayList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtGroupName,txtSaveRs,txtReferDesc;
        LinearLayout llGroupContainer;
        View viewFront,viewBack;

        public ViewHolder(View childView) {
            super(childView);
            txtGroupName = childView.findViewById(R.id.txtGroupName);
            llGroupContainer = childView.findViewById(R.id.llGroupContainer);
            viewFront = childView.findViewById(R.id.viewFront);
            viewBack = childView.findViewById(R.id.viewBack);


        }
    }

    public void setCurrentSelectedPosition(int position)
    {
        currentPosition = position;
        notifyDataSetChanged();
    }
    public void setOnGroupMenuSelectedListener(OnMobileHomeMenuSelectedListioner onGroupMenuSelectedListener)
    {
        this.onGroupMenuSelectedListener = onGroupMenuSelectedListener;
    }

    public void notifyPosition(int position)
    {
        currentPosition = position;
        notifyDataSetChanged();
    }

    public void notifyAll(List<Groupslist> groupChildBeanArrayList)
    {
        this.groupChildBeanArrayList=groupChildBeanArrayList;
        notifyDataSetChanged();

    }
}
