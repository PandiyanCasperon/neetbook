package com.app.neetbook.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.Interfaces.MCQChapterSection;
import com.app.neetbook.Interfaces.TestSection;
import com.app.neetbook.R;
import com.app.neetbook.View.SideMenu.SubscriptionDetailActivity;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.shuhart.stickyheader.StickyAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class MCQSectionAdapter  extends StickyAdapter<RecyclerView.ViewHolder, RecyclerView.ViewHolder> {

    private ArrayList<MCQChapterSection> sectionArrayList;
    private static final int LAYOUT_HEADER= 0;
    private static final int LAYOUT_CHILD= 1;
    private int currentSelectedPosition = -1;
    private int chileSeletedPosition = -1;
    private int superChileSeletedPosition = -1;
    Context context;
    private int index;
    private String strSubId = "",strSubName = "";
    private String currentSelectedHeader,indexCount = "1";
    LinearLayout.LayoutParams params,params1;
    private ItemClickListener itemClickListener;
    private int width1;
    private OnItemClickInterface onItemClickInterface;
    public MCQSectionAdapter(Context context, OnItemClickInterface onItemClickInterface,ArrayList<MCQChapterSection> sectionArrayList, String strSubId, String strSubName, int width1,int currentSelectedPosition, int chileSeletedPosition, int superChileSeletedPosition){

        // inflater = LayoutInflater.from(context);
        this.context = context;
        this.sectionArrayList = sectionArrayList;
        this.strSubId = strSubId;
        this.strSubName = strSubName;
        this.width1 = width1;
        this.currentSelectedPosition = currentSelectedPosition;
        this.chileSeletedPosition = chileSeletedPosition;
        this.superChileSeletedPosition = superChileSeletedPosition;
        this.onItemClickInterface=onItemClickInterface;
        currentSelectedHeader = sectionArrayList.get(0).getMCQLongrName();
        params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (viewType == LAYOUT_HEADER ) {
            return new HeaderViewholder(inflater.inflate(R.layout.mcq_section_adapter, parent, false));
        }else {
            return new ItemViewHolder(inflater.inflate(R.layout.mcq_section_child_item, parent, false));
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        Log.e("isHeader",""+sectionArrayList.get(position).isMCQChapterHeader());

        if (sectionArrayList.get(position).isMCQChapterHeader()) {

            HeaderViewholder headerViewholder = ((HeaderViewholder) holder);
            headerViewholder.title.setText(sectionArrayList.get(position).getMCQLongrName());
            headerViewholder.textView17.setText(sectionArrayList.get(position).getMCQCount());
            Picasso.with(context).load(IConstant.BaseUrl+sectionArrayList.get(position).getMCQImage()).into(headerViewholder.imageView14);

            if(position!=0){

                if(sectionArrayList.get(position).sectionPosition()== currentSelectedPosition) {

                    ((HeaderViewholder) holder).ivExpand.setImageResource(R.drawable.ic_arrow_up_article);

                }else
                {
                    ((HeaderViewholder) holder).ivExpand.setImageResource(R.drawable.ic_arow_down_article);
                }

            }

        } else {
            ItemViewHolder itemViewHolder = ((ItemViewHolder) holder);
            if(sectionArrayList.get(position).sectionPosition()== currentSelectedPosition) {

               // setChileView(sectionArrayList.get(position), position, itemViewHolder,0);
            }else
            {
                itemViewHolder.rlParent.setVisibility(View.GONE);
                itemViewHolder.rlParent.setVisibility(View.GONE);
                itemViewHolder.llChildHead.setVisibility(View.GONE);
            }


        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickInterface.onItemClick(sectionArrayList.get(position).getMCQChapterId());
                /*if(currentSelectedPosition!= sectionArrayList.get(position).sectionPosition()) {
                    currentSelectedHeader = sectionArrayList.get(position).getMCQLongrName();
                    currentSelectedPosition = sectionArrayList.get(position).sectionPosition();
                    chileSeletedPosition = -1;
                    itemClickListener.onParentItemClick(position);
                }else
                    currentSelectedPosition = -1;
                notifyDataSetChanged();*/
            }
        });
    }

    private void setChileView(final MCQChapterSection mcqChapterSection, final int position, final ItemViewHolder itemViewHolder,int open_position) {

        if(sectionArrayList.get(position).sectionPosition()== currentSelectedPosition) {
            itemViewHolder.rlParent.setVisibility(View.VISIBLE);
           // itemViewHolder.llChildHead.setVisibility(View.VISIBLE);
            itemViewHolder.llChildHead.removeAllViews();
            if (mcqChapterSection.getDeadDeatails() !=  null && mcqChapterSection.getDeadDeatails().size() > 0) {
                for (int i = 0; i < mcqChapterSection.getDeadDeatails().size(); i++) {
                    View ChildHeadView = LayoutInflater.from(context).inflate(R.layout.mcq_chiled_item, null);
                    ImageView imgArticleChildHead = ChildHeadView.findViewById(R.id.imgArticleChildHead);
                    TextView txt_article_child_head = ChildHeadView.findViewById(R.id.txt_article_child_head);
                    final LinearLayoutCompat llChildOfChild = ChildHeadView.findViewById(R.id.llChildOfChild);
                    imgArticleChildHead.setVisibility(mcqChapterSection.getDeadDeatails().get(i).isSH.equals("0") && superChileSeletedPosition == -1 && i == chileSeletedPosition ? View.VISIBLE : View.INVISIBLE);
                    txt_article_child_head.setTextColor(mcqChapterSection.getDeadDeatails().get(i).isSH.equals("0") && superChileSeletedPosition == -1 && i == chileSeletedPosition ? context.getResources().getColor(R.color.mcq_blue) : context.getResources().getColor(R.color.black));
                    final int finalI = i;
                    ChildHeadView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            chileSeletedPosition = finalI;
                            superChileSeletedPosition = -1;
                          //  setChileView(mcqChapterSection, position, itemViewHolder,1);
                           // notifyDataSetChanged();
                            if (mcqChapterSection.getDeadDeatails().get(finalI).isSH.equals("0")) {
                                TabMainActivity.isReadPage = true;
                                SubscriptionDetailActivity.mcqHeadList = mcqChapterSection.getDeadDeatails();
                                context.startActivity(new Intent(context, SubscriptionDetailActivity.class).putExtra("subject_id", strSubId).putExtra("subject_name", strSubName).putExtra("chapter_name",mcqChapterSection.getMCQShortName()).putExtra("head_id", mcqChapterSection.getDeadDeatails().get(finalI).headData.get(0)._id).putExtra("loading_type", "1").putExtra("superHeading", "No"));
                            }
                        }
                    });

                    if (mcqChapterSection.getDeadDeatails().get(i).isSH.equals("0")) {
                        txt_article_child_head.setText(mcqChapterSection.getDeadDeatails().get(i).headData.get(0).long_name);
                        llChildOfChild.setVisibility(View.GONE);
                       /* params.setMargins(0, 0, 0, 0);
                        txt_article_child_head.setLayoutParams(params);*/
                    } else {
                        SpannableString mySpannableString = new SpannableString(mcqChapterSection.getDeadDeatails().get(i).superDetails.long_name);
                        mySpannableString.setSpan(new UnderlineSpan(), 0, mySpannableString.length(), 0);
                        txt_article_child_head.setText(mySpannableString);
                        llChildOfChild.setVisibility(View.VISIBLE);
                        /*params1.setMargins((int) (width1*(0.1)), 0, 0, 0);
                        txt_article_child_head.setLayoutParams(params1);*/
                        if (mcqChapterSection.getDeadDeatails().get(i).headData.size() > 0) {
                            llChildOfChild.removeAllViews();
                            for (int j = 0; j < mcqChapterSection.getDeadDeatails().get(i).headData.size(); j++) {
                                View childOfChildView = LayoutInflater.from(context).inflate(R.layout.mcq_super_child, null);
                                TextView txtArticleSuperHeading = childOfChildView.findViewById(R.id.txtArticleSuperHeading);
                                ImageView imgArticleSuperChild = childOfChildView.findViewById(R.id.imgArticleSuperChild);
                                imgArticleSuperChild.setVisibility(chileSeletedPosition == i && j == superChileSeletedPosition ? View.VISIBLE : View.INVISIBLE);
                                txtArticleSuperHeading.setTextColor(chileSeletedPosition == i && j == superChileSeletedPosition ? context.getResources().getColor(R.color.mcq_blue) : Color.BLACK);
                                txtArticleSuperHeading.setText(mcqChapterSection.getDeadDeatails().get(i).headData.get(j).long_name);
                                final int finalJ = j;
                                childOfChildView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        TabMainActivity.isReadPage = true;
                                        superChileSeletedPosition = finalJ;
                                        chileSeletedPosition = finalI;
                                      //  setChileView(mcqChapterSection, position, itemViewHolder,1);
                                        //notifyDataSetChanged();
                                        SubscriptionDetailActivity.mcqHeadList = mcqChapterSection.getDeadDeatails();
                                        context.startActivity(new Intent(context, SubscriptionDetailActivity.class).putExtra("subject_id",strSubId).putExtra("subject_name",strSubName).putExtra("chapter_name", mcqChapterSection.getMCQShortName()).putExtra("head_id",mcqChapterSection.getDeadDeatails().get(finalI).headData.get(finalJ)._id).putExtra("loading_type", "1").putExtra("superHeading", mcqChapterSection.getDeadDeatails().get(finalI).superDetails.long_name));
                                    }
                                });
                                llChildOfChild.addView(childOfChildView);
                            }
                        }
                    }


                    itemViewHolder.llChildHead.addView(ChildHeadView);


                    if(sectionArrayList.size()-1==position){

                        itemViewHolder.llChildHead.setVisibility(View.VISIBLE);

                    }else{

                        if(open_position==0){

                            expand(itemViewHolder.llChildHead);
                        }
                    }
                }
            }
        }else
        {

        }

    }

    @Override
    public int getItemViewType(int position) {
        if(sectionArrayList.get(position).isMCQChapterHeader()) {
            return LAYOUT_HEADER;
        }else
            return LAYOUT_CHILD;
    }

    @Override
    public int getItemCount() {
        return sectionArrayList.size();
    }

    @Override
    public int getHeaderPositionForItem(int itemPosition) {
        Log.d("seeee",""+itemPosition+"......"+sectionArrayList.get(itemPosition).sectionPosition());
        return sectionArrayList.get(itemPosition).sectionPosition();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int headerPosition) {
        //if( sectionArrayList.get(headerPosition).isHeader()) {

        HeaderViewholder headerViewholder = ((HeaderViewholder) holder);
        /*headerViewholder.title.setText(sectionArrayList.get(sectionArrayList.get(headerPosition).sectionPosition()).getMCQChapterName());
        headerViewholder.textView17.setText(sectionArrayList.get(sectionArrayList.get(headerPosition).sectionPosition()).getMCQCount());
        Picasso.with(context).load(IConstant.BaseUrl+sectionArrayList.get(sectionArrayList.get(headerPosition).sectionPosition()).getMCQImage()).into(headerViewholder.imageView14);*/
        Picasso.with(context).load(IConstant.BaseUrl+sectionArrayList.get(index).getMCQImage()).into(((HeaderViewholder) holder).imageView14);
        ((HeaderViewholder) holder).title.setText( currentSelectedHeader);
        ((HeaderViewholder) holder).textView17.setText(""+indexCount);
      //  headerViewholder.ivExpand.setVisibility(View.VISIBLE);

        if (sectionArrayList.get(headerPosition).sectionPosition() == currentSelectedPosition) {

            ((HeaderViewholder) holder).ivExpand.setImageResource(R.drawable.ic_arrow_up_article);

        } else {

            ((HeaderViewholder) holder).ivExpand.setImageResource(R.drawable.ic_arow_down_article);
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return createViewHolder(parent, LAYOUT_HEADER);
    }

    public static class HeaderViewholder extends RecyclerView.ViewHolder {

        private TextView textView17,title;
        private ConstraintLayout llView;
        ImageView ivCollapse,ivExpand,imageView14;
        View llViewLIne;
        HeaderViewholder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tvCategoryTitle);
            ivCollapse = itemView.findViewById(R.id.ivCollapse);
           ivExpand = itemView.findViewById(R.id.ivExpand);
            textView17 = itemView.findViewById(R.id.textView17);
            imageView14 = itemView.findViewById(R.id.imageView14);
        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {


        private LinearLayoutCompat llChildHead;
        private RelativeLayout rlParent;
        ItemViewHolder(View itemView) {
            super(itemView);
            llChildHead = itemView.findViewById(R.id.llChildHead);
            rlParent = itemView.findViewById(R.id.rlParent);

        }
    }
    public void setTopItemView(String currentSelectedHeader,String indexCount,int index)
    {
        this.currentSelectedHeader = currentSelectedHeader;
        this.indexCount = indexCount;
        this.index = index;
    }
    public void CollapseExpandedView(int position)
    {
        this.currentSelectedPosition = position;
        //notifyDataSetChanged();
    }

    public void setOnItemClickListener(ItemClickListener itemClickListener)
    {
        this.itemClickListener = itemClickListener;
    }

    public void expand(final View v) {


        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? WindowManager.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Expansion speed of 1dp/ms
        //  a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        int s= (int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density);
        System.out.println("value"+s);
        a.setDuration(s);
        v.startAnimation(a);
    }


    public void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Collapse speed of 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);

    }
    public interface OnItemClickInterface{
        void onItemClick(String rootId);
    }
}
