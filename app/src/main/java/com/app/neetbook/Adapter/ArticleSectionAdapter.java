package com.app.neetbook.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.neetbook.Interfaces.ArticleHeaderSestion;
import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.Interfaces.MCQChapterSection;
import com.app.neetbook.Interfaces.Section;
import com.app.neetbook.Model.ArticleHeaderModel;
import com.app.neetbook.R;
import com.app.neetbook.StickeyHeader.AdapterDataProvider;
import com.app.neetbook.View.ScrollingActivity;
import com.app.neetbook.View.SideMenu.ArticleFragment;
import com.app.neetbook.View.SideMenu.SubscriptionArticleActivity;
import com.app.neetbook.View.SideMenu.SubscriptionDetailActivity;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.shuhart.stickyheader.StickyAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;

public class ArticleSectionAdapter extends StickyAdapter<RecyclerView.ViewHolder, RecyclerView.ViewHolder> {

    private ArrayList<ArticleHeaderSestion> sectionArrayList;
    private static final int LAYOUT_HEADER = 0;
    private static final int LAYOUT_CHILD = 1;
    private int currentSelectedPosition = -1;
    private int chileSeletedPosition = -1;
    private int superChileSeletedPosition = -1;
    Context context;
    private String currentSelectedHeader, indexCount = "1";
    private String strSubId = "", strSubName = "";
    Typeface tfItalicRegular, tfMedium;
    LinearLayout.LayoutParams params;
    private int index = 0;
    private ItemClickListener itemClickListener;
    int width1;
    private final List<Object> dataList = new ArrayList<>();
    private String strHeadId, strChapterId;
    ArticleFragment articleFragment;
    ArticleSectionAdapterListener articleSectionAdapterListener;

    public ArticleSectionAdapter(Context context, ArticleSectionAdapterListener articleSectionAdapterListener, ArrayList<ArticleHeaderSestion> sectionArrayList, String strSubId, String strSubName, int height, List<Object> items, int currentSelectedPosition, int chileSeletedPosition, int superChileSeletedPosition, ArticleFragment articleFragment) {

        // inflater = LayoutInflater.from(context);
        this.context = context;
        this.sectionArrayList = sectionArrayList;
        this.strSubId = strSubId;
        this.strSubName = strSubName;
        this.width1 = height;
        this.currentSelectedPosition = currentSelectedPosition;
        this.chileSeletedPosition = chileSeletedPosition;
        this.superChileSeletedPosition = superChileSeletedPosition;
        this.articleFragment=articleFragment;
        this.dataList.addAll(items);
        this.articleSectionAdapterListener=articleSectionAdapterListener;
        currentSelectedHeader = sectionArrayList.get(0).getLongName();
        tfItalicRegular = Typeface.createFromAsset(this.context.getAssets(), "fonts/proximanova_regitalic.otf");
        tfMedium = Typeface.createFromAsset(this.context.getAssets(), "fonts/ProximaNovaSemibold.otf");
        params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (viewType == LAYOUT_HEADER) {
            return new HeaderViewholder(inflater.inflate(R.layout.recycler_view_header_item, parent, false));
        } else {
            return new ItemViewHolder(inflater.inflate(R.layout.article_section_child_body, parent, false));
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        try {

            if (sectionArrayList.get(position).isHeader()) {

                HeaderViewholder headerViewholder = ((HeaderViewholder) holder);

                Log.e("tvTitle", sectionArrayList.get(position).getLongName());
                ((HeaderViewholder) holder).tvCategoryTitle.setText(sectionArrayList.get(position).getLongName());
                ((HeaderViewholder) holder).textView17.setText(sectionArrayList.get(position).getCount());

               if(position!=0){

                    if (sectionArrayList.get(position).sectionPosition() == currentSelectedPosition) {

                        ((HeaderViewholder) holder).ivExpand.setImageResource(R.drawable.ic_arrow_up_article);

                    } else {

                        ((HeaderViewholder) holder).ivExpand.setImageResource(R.drawable.ic_arow_down_article);

                    }

                }

                Picasso.with(context).load(IConstant.BaseUrl + sectionArrayList.get(position).getImage()).into(headerViewholder.imageView14);

            } else {

                ItemViewHolder itemViewHolder = ((ItemViewHolder) holder);
                if (sectionArrayList.get(position).sectionPosition() == currentSelectedPosition) {
                 //   setChileView(sectionArrayList.get(position), position, itemViewHolder,0);
                } else {
                   itemViewHolder.rlParent.setVisibility(View.GONE);
                  // itemViewHolder.llChildHead.setVisibility(View.GONE);
                  //  collapse(itemViewHolder.llChildHead);
                }

            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    articleSectionAdapterListener.onItemParentListener(position,sectionArrayList.get(position).getId(),sectionArrayList.get(position).getLongName(),sectionArrayList.get(position).getImage());
                 //   EventBus.getDefault().post(position+","+sectionArrayList.get(position).getId()+","+sectionArrayList.get(position).getLongName()+","+sectionArrayList.get(position).getImage());

                    /*if (currentSelectedPosition != sectionArrayList.get(position).sectionPosition()) {
                        currentSelectedHeader = sectionArrayList.get(position).getLongName();
                        currentSelectedPosition = sectionArrayList.get(position).sectionPosition();
                        superChileSeletedPosition = -1;
                        chileSeletedPosition = -1;
                        itemClickListener.onParentItemClick(position);
                    } else
                        currentSelectedPosition = -1;
                    //notifyDataSetChanged();*/
                }
            });

        } catch (Exception e) {

            Log.e("Error", e.toString());
        }


    }

    private void setChileView(final ArticleHeaderSestion mcqChapterSection, final int position, final ItemViewHolder itemViewHolder,final int open_position) {

        if (sectionArrayList.get(position).sectionPosition() == currentSelectedPosition) {
            itemViewHolder.rlParent.setVisibility(View.VISIBLE);

            //itemViewHolder.llChildHead.setVisibility(View.VISIBLE);
            itemViewHolder.llChildHead.removeAllViews();
            if (mcqChapterSection.getHeadList() != null && mcqChapterSection.getHeadList().size() > 0) {
                for (int i = 0; i < mcqChapterSection.getHeadList().size(); i++) {
                    View ChildHeadView = LayoutInflater.from(context).inflate(R.layout.article_chile_head, null);
                    ImageView imgArticleChildHead = ChildHeadView.findViewById(R.id.imgArticleChildHead);
                    TextView txt_article_child_head = ChildHeadView.findViewById(R.id.txt_article_child_head);
                    final LinearLayoutCompat llChildOfChild = ChildHeadView.findViewById(R.id.llChildOfChild);


                    imgArticleChildHead.setVisibility(mcqChapterSection.getHeadList().get(i).isSH.equals("0") && superChileSeletedPosition == -1 && i == chileSeletedPosition ? View.VISIBLE : View.INVISIBLE);
                    txt_article_child_head.setTextColor(mcqChapterSection.getHeadList().get(i).isSH.equals("0") && superChileSeletedPosition == -1 && i == chileSeletedPosition ? context.getResources().getColor(R.color.artical_pink) : context.getResources().getColor(R.color.black));
                    final int finalI = i;
                    ChildHeadView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            chileSeletedPosition = finalI;
                            superChileSeletedPosition = -1;
                            setChileView(mcqChapterSection, position, itemViewHolder,1);
                            //notifyDataSetChanged();
                            if (mcqChapterSection.getHeadList().get(finalI).isSH.equals("0")) {
                                TabMainActivity.isReadPage = true;
                                ScrollingActivity.headDataArrayList = mcqChapterSection.getHeadList();
                                context.startActivity(new Intent(context, ScrollingActivity.class).putExtra("chapter_name", mcqChapterSection.getShortName()).putExtra("chapter_id", mcqChapterSection.getId()).putExtra("head_id", mcqChapterSection.getHeadList().get(finalI).headData.get(0)._id).putExtra("subject_id", strSubId).putExtra("subject_name", strSubName).putExtra("chapter_name", mcqChapterSection.getShortName()).putExtra("superHeading", "No"));

                            }
                        }
                    });

                    if (mcqChapterSection.getHeadList().get(i).isSH.equals("0")) {
                        txt_article_child_head.setText(mcqChapterSection.getHeadList().get(i).headData.get(0).long_name);
                        llChildOfChild.setVisibility(View.GONE);

                        /*params.setMargins(0, 0, 0, 0);
                        txt_article_child_head.setLayoutParams(params);*/
                        //txt_article_child_head.setTypeface(tfMedium);
                    } else {
                      /*  LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT
                        );
                        params.setMargins((int) (width1*(0.1)), 0, 0, 0);
                        txt_article_child_head.setLayoutParams(params);*/
                        //txt_article_child_head.setTypeface(tfItalicRegular);
                        SpannableString mySpannableString = new SpannableString(mcqChapterSection.getHeadList().get(i).superDetails.long_name);
                        mySpannableString.setSpan(new UnderlineSpan(), 0, mySpannableString.length(), 0);
                        txt_article_child_head.setText(mySpannableString);
                        llChildOfChild.setVisibility(View.VISIBLE);

                        if (mcqChapterSection.getHeadList().get(i).headData.size() > 0) {
                            llChildOfChild.removeAllViews();
                            for (int j = 0; j < mcqChapterSection.getHeadList().get(i).headData.size(); j++) {
                                View childOfChildView = LayoutInflater.from(context).inflate(R.layout.article_super_headings, null);
                                TextView txtArticleSuperHeading = childOfChildView.findViewById(R.id.txtArticleSuperHeading);
                                ImageView imgArticleSuperChild = childOfChildView.findViewById(R.id.imgArticleSuperChild);
                                imgArticleSuperChild.setVisibility(chileSeletedPosition == i && j == superChileSeletedPosition ? View.VISIBLE : View.INVISIBLE);
                                txtArticleSuperHeading.setTextColor(chileSeletedPosition == i && j == superChileSeletedPosition ? context.getResources().getColor(R.color.artical_pink) : Color.BLACK);
                                txtArticleSuperHeading.setText(mcqChapterSection.getHeadList().get(i).headData.get(j).long_name);
                                final int finalJ = j;
                                childOfChildView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        TabMainActivity.isReadPage = true;
                                        superChileSeletedPosition = finalJ;
                                        chileSeletedPosition = finalI;
                                        setChileView(mcqChapterSection, position, itemViewHolder,1);
                                       // notifyDataSetChanged();
                                        ScrollingActivity.headDataArrayList = mcqChapterSection.getHeadList();
                                        context.startActivity(new Intent(context, ScrollingActivity.class).putExtra("chapter_id", mcqChapterSection.getId()).putExtra("chapter_name", mcqChapterSection.getShortName()).putExtra("head_id", mcqChapterSection.getHeadList().get(finalI).headData.get(finalJ)._id).putExtra("subject_id", strSubId).putExtra("subject_name", strSubName).putExtra("superHeading", mcqChapterSection.getHeadList().get(finalI).superDetails.short_name));
                                    }
                                });
                                llChildOfChild.addView(childOfChildView);
                            }
                        }
                    }


                    itemViewHolder.llChildHead.addView(ChildHeadView);

                    if(sectionArrayList.size()-1==position){

                        itemViewHolder.llChildHead.setVisibility(View.VISIBLE);

                    }else{

                        if(open_position==0){

                            expand(itemViewHolder.llChildHead);
                        }

                    }

                }
            }
        } else {



            //collapse(itemViewHolder.llChildHead);

        }

    }

    @Override
    public int getItemViewType(int position) {
        if (sectionArrayList.get(position).isHeader()) {
            return LAYOUT_HEADER;
        } else
            return LAYOUT_CHILD;
    }

    @Override
    public int getItemCount() {
        return sectionArrayList.size();
    }

    @Override
    public int getHeaderPositionForItem(int itemPosition) {
        Log.d("seeee", "" + itemPosition + "......" + sectionArrayList.get(itemPosition).sectionPosition());
        return 0;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int headerPosition) {

        Picasso.with(context).load(IConstant.BaseUrl + sectionArrayList.get(index).getImage()).into(((HeaderViewholder) holder).imageView14);
        Picasso.with(context).load(R.drawable.ic_arrow_up_article).into(((HeaderViewholder) holder).ivCollapse);
        ((HeaderViewholder) holder).tvCategoryTitle.setText(currentSelectedHeader);
        ((HeaderViewholder) holder).textView17.setText("" + indexCount);

        if (sectionArrayList.get(headerPosition).sectionPosition() == currentSelectedPosition) {

            ((HeaderViewholder) holder).ivExpand.setImageResource(R.drawable.ic_arrow_up_article);

        } else {

            ((HeaderViewholder) holder).ivExpand.setImageResource(R.drawable.ic_arow_down_article);
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return createViewHolder(parent, LAYOUT_HEADER);
    }



    public static class HeaderViewholder extends RecyclerView.ViewHolder {
        TextView tvCategoryTitle, textView17;
        ImageView imageView14;
        ImageView ivCollapse, ivExpand;

        HeaderViewholder(View itemView) {
            super(itemView);
            tvCategoryTitle = itemView.findViewById(R.id.tvCategoryTitle);
            textView17 = itemView.findViewById(R.id.textView17);
            imageView14 = itemView.findViewById(R.id.imageView14);
            ivCollapse = itemView.findViewById(R.id.ivCollapse);
            ivExpand = itemView.findViewById(R.id.ivExpand);
        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        private LinearLayoutCompat llChildHead;
        private RelativeLayout rlParent;

        ItemViewHolder(View itemView) {
            super(itemView);
            llChildHead = itemView.findViewById(R.id.llChildHead);
            rlParent = itemView.findViewById(R.id.rlParent);
        }
    }

    public void setTopItemView(String currentSelectedHeader, String indexCount, int positon) {
        this.currentSelectedHeader = currentSelectedHeader;
        this.indexCount = indexCount;
        this.index = positon;
        Log.e("Header", this.currentSelectedHeader);
    }

    public void CollapseExpandedView(int position) {
        this.currentSelectedPosition = position;
        //notifyDataSetChanged();
    }

    public void setOnItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }


    public void expand(final View v) {

        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? WindowManager.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Expansion speed of 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);



    }


    public void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Collapse speed of 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);

    }

  public   interface ArticleSectionAdapterListener{
        void onItemParentListener(int position,String parentId,String ParentLongName,String ParentImageUlrl);
    }
}
