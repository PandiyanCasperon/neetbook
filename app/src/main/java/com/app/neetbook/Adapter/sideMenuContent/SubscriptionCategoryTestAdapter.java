package com.app.neetbook.Adapter.sideMenuContent;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.neetbook.Adapter.InnerImageListAdapter;
import com.app.neetbook.Model.TestList;
import com.app.neetbook.Model.sidemenuFromContent.CategoryTest;
import com.app.neetbook.R;
import com.app.neetbook.Utils.widgets.CustomTextViewMedium;
import com.app.neetbook.View.ImagesViewActivity;
import com.app.neetbook.View.SideMenu.SubscriptionTestActivity;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

public class SubscriptionCategoryTestAdapter extends RecyclerView.Adapter<SubscriptionCategoryTestAdapter.TestViewHolder> {

  private Context context;
  private ArrayList<CategoryTest> itemList;
  private String subjectId = "";
  private String subjectName = "";
  private int currentSelectedPosition = -1;
  private String currentSelectedHeader,indexCount = "1";
  private int chileSeletedPosition = -1;
  private onSelectedListener onSelectedListener;
  public interface onSelectedListener
  {
    void onSelected(int position,boolean isFullTest,String Title, String count);
  }
  public void setOnSelectedListener(onSelectedListener onSelectedListener)
  {
    this.onSelectedListener = onSelectedListener;
  }
  public SubscriptionCategoryTestAdapter(Context context,
      ArrayList<CategoryTest> list,String subjectId,String subjectName) {
    this.context = context;
    this.itemList = list;
    this.subjectId = subjectId;
    this.subjectName = subjectName;
  }

  @NonNull
  @Override
  public TestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
    View view = layoutInflater.inflate(R.layout.subscription_category_test_item,
        parent, false);
    return new TestViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull TestViewHolder holder, final int position) {
    final CategoryTest categoryTest = itemList.get(position);
    if(categoryTest.getIsFullTest().equals("Yes")) {
      holder.title.setVisibility(View.GONE);
      holder.textView17.setVisibility(View.GONE);
      holder.imageView14.setVisibility(View.INVISIBLE);
      holder.tvCategoryFullTestTitle.setVisibility(View.VISIBLE);
      holder.txtFt.setVisibility(View.VISIBLE);
      holder.tvCategoryFullTestTitle.setText(categoryTest.getFulltest_name());
      holder.txtFt.setText("FT"+(position+1));

    }else
    {
      holder.title.setText(categoryTest.long_name);
      holder.textView17.setText(categoryTest.getChapterCount());
      holder.title.setVisibility(View.VISIBLE);
      holder.textView17.setVisibility(View.VISIBLE);
      holder.imageView14.setVisibility(View.VISIBLE);
      holder.tvCategoryFullTestTitle.setVisibility(View.GONE);
      holder.txtFt.setVisibility(View.INVISIBLE);
      Picasso.with(context).load(IConstant.BaseUrl+categoryTest.images).into(holder.imageView14);
    }


    if(position == currentSelectedPosition)
    { holder.ivExpand.setVisibility(View.GONE);
      holder.ivCollapse.setVisibility(View.VISIBLE);
      setChileView(categoryTest, position, holder);
      //setChildRecyclerView(holder,position);


      /*if(categoryTest.testArrayList.size()>0)
      {
        HeadingsAdapter innerImageListAdapter = new HeadingsAdapter(context,categoryTest.testArrayList);
        holder.childRecyclerView.setHasFixedSize(true);
        holder.childRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        holder.childRecyclerView.setAdapter(innerImageListAdapter);
       *//* holder.childRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
          @Override
          public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            LinearLayoutManager layoutManager=LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
            int totalItemCount = layoutManager.getItemCount();
            int lastVisible = layoutManager.findLastVisibleItemPosition();

            boolean endHasBeenReached = lastVisible + 5 >= totalItemCount;
            Log.e("lastVisible",""+lastVisible);


          }
        });*//*

        Log.e("get bottom",""+holder.childRecyclerView.getBottom());
      }*/


    }else
    { holder.ivExpand.setVisibility(View.VISIBLE);
      holder.ivCollapse.setVisibility(View.GONE);
     // holder.llChildHead.setVisibility(View.GONE);
      collapse(holder.llChildHead);
    }


    holder.parentView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        if(currentSelectedPosition!= position) {
          if (categoryTest.getIsFullTest() != null && categoryTest.getIsFullTest().equalsIgnoreCase("Yes")) {

            TabMainActivity.isReadPage = true;

            context.startActivity(new Intent(context, SubscriptionTestActivity.class)
                    .putExtra("subject_id", subjectId)
                    .putExtra("chapter_id", categoryTest._id)
                    .putExtra("chapter_name", categoryTest.short_name)
                    .putExtra("subject_name", subjectName)
                    .putExtra("testId",categoryTest.testArrayList.get(0)._id)
                    .putExtra("type",categoryTest.testArrayList.get(0).type)
                    .putExtra("no_of_question",categoryTest.testArrayList.get(0).no_of_qstn)
                    .putExtra("isFinished",categoryTest.testArrayList.get(0).is_finished)
                    .putExtra("questionType",categoryTest.testArrayList.get(0).qstn_type)
                    .putExtra("strInstruction",categoryTest.testArrayList.get(0).instruction)
                    .putExtra("strLongName",categoryTest.testArrayList.get(0).long_name)
                    .putExtra("crt_ans_mark",categoryTest.testArrayList.get(0).crt_ans_mark)
                    .putExtra("wrng_ans_mark",categoryTest.testArrayList.get(0).wrng_ans_mark)
            );
          } else {
            onSelectedListener.onSelected(position,false,categoryTest.long_name,categoryTest.getChapterCount());
            currentSelectedPosition = position;
            chileSeletedPosition = -1;
            notifyDataSetChanged();
          }
        }else {
          currentSelectedPosition = -1;
          notifyDataSetChanged();
        }

      }
    });
  }


  private void setChildRecyclerView() {

  }

  private void setChileView(final CategoryTest categoryTest, final int position, final TestViewHolder holder) {
    //holder.llChildHead.setVisibility(View.VISIBLE);
    holder.llChildHead.removeAllViews();
      if(categoryTest.testArrayList.size()>0)
      {
        for(int i=0;i<categoryTest.testArrayList.size();i++)
        {
          View ChildHeadView = LayoutInflater.from(context).inflate(R.layout.test_chile_item,null);
          ImageView imgArticleChildHead = ChildHeadView.findViewById(R.id.imgArticleChildHead);
          TextView txt_article_child_head = ChildHeadView.findViewById(R.id.txt_article_child_head);
          LinearLayout llParent = ChildHeadView.findViewById(R.id.llParent);

          imgArticleChildHead.setVisibility(i== chileSeletedPosition?View.VISIBLE:View.INVISIBLE);
          txt_article_child_head.setTextColor(i== chileSeletedPosition?context.getResources().getColor(R.color.test_yellow_dark):context.getResources().getColor(R.color.black));
          txt_article_child_head.setText(categoryTest.testArrayList.get(i).long_name);
          final int finalI = i;
          ChildHeadView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              chileSeletedPosition = finalI;
              TabMainActivity.isReadPage = true;
              setChileView(categoryTest,position,holder);
              notifyDataSetChanged();

              context.startActivity(new Intent(context, SubscriptionTestActivity.class)
                      .putExtra("subject_id",subjectId)
                      .putExtra("chapter_id",categoryTest._id)
                      .putExtra("chapter_name",categoryTest.short_name)
                      .putExtra("subject_name",subjectName)
                      .putExtra("testId",categoryTest.testArrayList.get(finalI)._id)
                      .putExtra("type",categoryTest.testArrayList.get(finalI).type)
                      .putExtra("no_of_question",categoryTest.testArrayList.get(finalI).no_of_qstn)
                      .putExtra("isFinished",categoryTest.testArrayList.get(finalI).is_finished)
                      .putExtra("questionType",categoryTest.testArrayList.get(finalI).qstn_type)
                      .putExtra("strInstruction",categoryTest.testArrayList.get(finalI).instruction)
                      .putExtra("strLongName",categoryTest.testArrayList.get(finalI).long_name)
                      .putExtra("crt_ans_mark",categoryTest.testArrayList.get(finalI).crt_ans_mark)
                      .putExtra("wrng_ans_mark",categoryTest.testArrayList.get(finalI).wrng_ans_mark)
              );
            }
          });


          holder.llChildHead.addView(ChildHeadView);

          expand( holder.llChildHead);
        }
      }

  }

  public int getCurrentSelectedPosition()
  {
    return currentSelectedPosition;
  }
  @Override
  public int getItemCount() {
    return itemList.size();
  }

  class TestViewHolder extends ViewHolder {

    private TextView tvCategoryFullTestTitle,title,textView17,txtFt;
    ImageView imageView14;
    ConstraintLayout parentView;
    LinearLayoutCompat llChildHead;
    ImageView ivCollapse,ivExpand;

    RecyclerView childRecyclerView;
    private TestViewHolder(@NonNull final View itemView) {
      super(itemView);
      llChildHead = itemView.findViewById(R.id.llChildHead);
      parentView = itemView.findViewById(R.id.parentView);
      title = itemView.findViewById(R.id.tvCategoryTitle);
      textView17 = itemView.findViewById(R.id.textView17);
      imageView14 = itemView.findViewById(R.id.imageView14);
      txtFt = itemView.findViewById(R.id.txtFt);
      tvCategoryFullTestTitle = itemView.findViewById(R.id.tvCategoryFullTestTitle);
      childRecyclerView = itemView.findViewById(R.id.childRecyclerView);
      ivCollapse = itemView.findViewById(R.id.ivCollapse);
      ivExpand = itemView.findViewById(R.id.ivExpand);
    }
  }
  public class HeadingsAdapter extends RecyclerView.Adapter<HeadingsAdapter.SingleItemRowHolder> {

    private ArrayList<TestList> testArrayList;
    public Context mContext;


    public HeadingsAdapter(Context context, ArrayList<TestList> testArrayList) {
      this.testArrayList = testArrayList;
      this.mContext = context;

    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
      View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.test_chile_item, null);
      SingleItemRowHolder mh = new SingleItemRowHolder(v);
      return mh;
    }

    @Override
    public void onBindViewHolder(final SingleItemRowHolder holder, final int i) {


      holder.txt_article_child_head.setTextColor(i== chileSeletedPosition?context.getResources().getColor(R.color.test_yellow_dark):context.getResources().getColor(R.color.black));
      holder.txt_article_child_head.setText(testArrayList.get(i).long_name);
      holder.imgArticleChildHead.setVisibility(i== chileSeletedPosition?View.VISIBLE:View.INVISIBLE);
      holder.rlContainer.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          chileSeletedPosition = i;
          TabMainActivity.isReadPage = true;
          notifyDataSetChanged();

          context.startActivity(new Intent(context, SubscriptionTestActivity.class)
                  .putExtra("subject_id",subjectId)
                  .putExtra("chapter_id",testArrayList.get(i)._id)
                  .putExtra("chapter_name",testArrayList.get(i).short_name)
                  .putExtra("subject_name",subjectName)
                  .putExtra("testId",testArrayList.get(i)._id)
                  .putExtra("type",testArrayList.get(i).type)
                  .putExtra("no_of_question",testArrayList.get(i).no_of_qstn)
                  .putExtra("isFinished",testArrayList.get(i).is_finished)
                  .putExtra("questionType",testArrayList.get(i).qstn_type)
                  .putExtra("strInstruction",testArrayList.get(i).instruction)
                  .putExtra("strLongName",testArrayList.get(i).long_name)
                  .putExtra("crt_ans_mark",testArrayList.get(i).crt_ans_mark)
                  .putExtra("wrng_ans_mark",testArrayList.get(i).wrng_ans_mark)
          );
        }
      });



    }

    @Override
    public int getItemCount() {
      return (null != testArrayList ? testArrayList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

      ImageView imgArticleChildHead;
      TextView txt_article_child_head;
      ConstraintLayout rlContainer;


      public SingleItemRowHolder(View view) {
        super(view);


        imgArticleChildHead = view.findViewById(R.id.imgArticleChildHead);
        txt_article_child_head = view.findViewById(R.id.txt_article_child_head);
        rlContainer = view.findViewById(R.id.rlContainer);




      }

    }
  }


  public void expand(final View v) {


    int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
    int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
    v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
    final int targetHeight = v.getMeasuredHeight();

    // Older versions of android (pre API 21) cancel animations for views with a height of 0.
    v.getLayoutParams().height = 1;
    v.setVisibility(View.VISIBLE);
    Animation a = new Animation() {
      @Override
      protected void applyTransformation(float interpolatedTime, Transformation t) {
        v.getLayoutParams().height = interpolatedTime == 1
                ? WindowManager.LayoutParams.WRAP_CONTENT
                : (int) (targetHeight * interpolatedTime);
        v.requestLayout();
      }

      @Override
      public boolean willChangeBounds() {
        return true;
      }
    };

    // Expansion speed of 1dp/ms
    a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
    v.startAnimation(a);



  }


  public void collapse(final View v) {
    final int initialHeight = v.getMeasuredHeight();

    Animation a = new Animation() {
      @Override
      protected void applyTransformation(float interpolatedTime, Transformation t) {
        if (interpolatedTime == 1) {
          v.setVisibility(View.GONE);
        } else {
          v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
          v.requestLayout();
        }
      }

      @Override
      public boolean willChangeBounds() {
        return true;
      }
    };

    // Collapse speed of 1dp/ms
    a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
    v.startAnimation(a);

  }
}
