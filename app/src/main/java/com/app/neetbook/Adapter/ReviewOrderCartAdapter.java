package com.app.neetbook.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.neetbook.Interfaces.OnDeleteItemFromCart;
import com.app.neetbook.Model.subscription.SubscriptionCart;
import com.app.neetbook.R;
import com.app.neetbook.Utils.widgets.CustomTextViewMedium;
import com.app.neetbook.Utils.widgets.DisplayOrientation;
import com.app.neetbook.View.ReviewOrder;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ReviewOrderCartAdapter  extends
        RecyclerView.Adapter<ReviewOrderCartAdapter.CartPriceViewHolder> {

    private Context context;
    private List<SubscriptionCart> subscriptionList;
    private OnDeleteItemFromCart onDeleteItemFromCart;
    private int height1;



    public ReviewOrderCartAdapter(Context context,
                                          List<SubscriptionCart> subscriptionList,int height1) {
        this.context = context;
        this.subscriptionList = subscriptionList;
        this.height1 = height1;
    }

    @NonNull
    @Override
    public CartPriceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.cart_items_review_order, parent, false);
        ViewGroup.LayoutParams params=view.getLayoutParams();
        if(!context.getResources().getBoolean(R.bool.isTablet)) {
            params.height = (int) (height1 * (height1<1300 ? 0.10 : 0.08));
            Log.e("Adapter height", "" + params.height);
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            view.setLayoutParams(params);
            view.setPadding((int) (height1 * (0.02)),(int) (height1 * (0.01)),(int) (height1 * (0.02)),(int) (height1 * (0.01)));
        }
        return new CartPriceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CartPriceViewHolder holder, final int position) {
        final SubscriptionCart subscription = subscriptionList.get(position);
        if(!context.getResources().getBoolean(R.bool.isTablet)) {
            holder.txt_subscription_title.setTextSize(height1<1300 ? 14 : 18);
            holder.txt_subsription_duration.setTextSize(height1<1300 ? 12 : 14);
            holder.txt_subscription_price.setTextSize(height1<1300 ? 14 : 16);
        }
        holder.txt_subscription_title.setText(subscription.getTitle());
        holder.txt_subsription_duration.setText(subscription.getSubscription());
        holder.txt_subscription_price.setText(context.getString(R.string.currency_symbol)+" "+Math.round(Float.parseFloat(subscription.getPrice())));
        if(subscription.getSubscription().equalsIgnoreCase("Not Available"))
        {
            holder.imgDelete.setVisibility(View.VISIBLE);
            holder.txt_subscription_price.setVisibility(View.GONE);
            holder.txt_subsription_duration.setTextColor(Color.RED);
        }else {
            holder.txt_subsription_duration.setTextColor(context.getResources().getColor(R.color.black_overlay));
            holder.txt_subscription_price.setVisibility(View.VISIBLE);
            holder.imgDelete.setVisibility(View.GONE);
        }
        holder.txt_subsription_duration.setText(subscription.getSubscription());

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDeleteItemFromCart.onDelete(position,subscription);
            }
        });
    }

    @Override
    public int getItemCount() {
        return subscriptionList.size();
    }

    public interface OnSubscriptionClick {

        void OnViewitemClickListioner(int position);
    }

    class CartPriceViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_subscription_title;
        private TextView txt_subsription_duration;
        private TextView txt_subscription_price;
        private ImageView imgDelete;


        private CartPriceViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_subscription_title = itemView.findViewById(R.id.txt_subscription_title);
            txt_subsription_duration = itemView.findViewById(R.id.txt_subsription_duration);
            txt_subscription_price = itemView.findViewById(R.id.txt_subscription_price);
            imgDelete = itemView.findViewById(R.id.imgDelete);

        }
    }


    public void setOnDeleteItemFromCart(OnDeleteItemFromCart onDeleteItemFromCart)
    {
        this.onDeleteItemFromCart = onDeleteItemFromCart;
    }
}
