package com.app.neetbook.Adapter.subscription;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build.VERSION_CODES;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.app.neetbook.Adapter.subscription.SubscriptionAdapter.SubscriptionViewHolder;
import com.app.neetbook.Interfaces.onSubscriptionChecked;
import com.app.neetbook.Model.subscription.Subscription;
import com.app.neetbook.R;
import com.app.neetbook.Utils.widgets.CustomTextViewMedium;
import com.app.neetbook.serviceRequest.IConstant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SubscriptionAdapter extends
    RecyclerView.Adapter<SubscriptionViewHolder> {

  private Context context;
  private ArrayList<Subscription> subscriptionList;
  private OnItemClickListener listener;
  private onSubscriptionChecked onSubscriptionChecked;
  public SubscriptionAdapter(Context context,
      ArrayList<Subscription> subscriptionList) {
    this.subscriptionList = subscriptionList;
    this.context = context;
  }

  @NonNull
  @Override
  public SubscriptionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

    View view = layoutInflater.inflate(R.layout.mobile_subcription_child_adapter, parent, false);
    return new SubscriptionViewHolder(view);
  }

  @RequiresApi(api = VERSION_CODES.LOLLIPOP)
  @Override
  public void onBindViewHolder(@NonNull SubscriptionViewHolder holder, int position) {
//    holder.subscriptionAdapterBottomView.setVisibility(position == subscriptionList.size()-1?View.VISIBLE:View.GONE);
    Subscription subscription = subscriptionList.get(position);


    holder.title.setText(subscription.getTitle().length()<36 & !context.getResources().getBoolean(R.bool.isTablet) ? subscription.getTitle() : subscription.getTitle().length()>35 &!context.getResources().getBoolean(R.bool.isTablet)? subscription.getStrShortName() : subscription.getTitle());
    holder.chapters.setText(subscription.getChapter()+" "+ context.getString(R.string.chapters));
    holder.topics.setText(subscription.getTopics()+" "+context.getString(R.string.topics));
    holder.questions.setText(subscription.getQuestions()+" "+context.getString(R.string.mcqs));
    Picasso.with(context).load(IConstant.BaseUrl+subscription.getStrImage()).into(holder.iv_subscription);

    if(subscription.getStrIsSubcribed().equalsIgnoreCase("Yes")){
//      holder.miniSubscriptionCheck.setVisibility(View.GONE);
//      holder.largeSubscriptionCheck.setVisibility(View.GONE);
      holder.subscriptionDuration.setVisibility(View.VISIBLE);
      holder.subscriptionVerify.setVisibility(View.VISIBLE);
      holder.imgPackageOneChecked.setVisibility(View.GONE);
      holder.imgPackageOneUnChecked.setVisibility(View.GONE);
      holder.imgPackageTwoUnChecked.setVisibility(View.GONE);
      holder.imgPackageTwoUnChecked.setVisibility(View.GONE);
      holder.llLarge_subscription.setVisibility(View.GONE);
      holder.llLarge_subscription.setVisibility(View.GONE);
      holder.subscriptionDuration.setText(subscription.getStrRemainingDays()+" "+context.getString(R.string.days)+" "+subscription.getStrRemainingHours());
    }else
    {
//      holder.miniSubscriptionCheck.setVisibility(View.VISIBLE);
//      holder.largeSubscriptionCheck.setVisibility(View.VISIBLE);
      holder.subscriptionDuration.setVisibility(View.GONE);
      holder.subscriptionVerify.setVisibility(View.GONE);

      setupMiniSubscriptionCheckbox(subscription, holder,position);
      setupLargeSubscriptionCheckbox(subscription, holder,position);
    }

  }


  @RequiresApi(api = VERSION_CODES.LOLLIPOP)
  private void setupMiniSubscriptionCheckbox(final Subscription subscription,
                                             final SubscriptionViewHolder holder, final int position) {
      //holder.miniSubscriptionCheck.setChecked(subscription.isMiniSubscriptionChecked());

    if(subscription.isMiniSubscriptionChecked())
    {
      holder.imgPackageOneChecked.setVisibility(View.VISIBLE);
      holder.imgPackageOneUnChecked.setVisibility(View.GONE);
    }else {
      holder.imgPackageOneChecked.setVisibility(View.GONE);
      holder.imgPackageOneUnChecked.setVisibility(View.VISIBLE);
    }

    if(subscription.getMiniSubscription() != null && !subscription.getMiniSubscription().isEmpty()) {
      holder.llMini_subscription.setVisibility(View.VISIBLE);
      holder.txtMiniSubscription.setText(subscription.getMiniSubscription() + ", " + context.getString(R.string.days)+" "+subscription.getStrMiniHours() + " " + context.getString(R.string.hours));
      holder.miniSubscriptionPrice.setText(context.getString(R.string.currency_symbol) + " " + Math.round(Double.parseDouble(subscription.getMiniSubscriptionPrice())));
    }else
      holder.llMini_subscription.setVisibility(View.GONE);


    holder.imgPackageOneChecked.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        onSubscriptionChecked.onItemChecked(false,subscription.getStrPackageOneId(),subscription,1,position);

        subscription.setMiniSubscriptionChecked(false);
        notifyItemChanged(position);

      }
    });
    holder.imgPackageOneUnChecked.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        if(holder.imgPackageTwoChecked.getVisibility() == View.VISIBLE)
          onSubscriptionChecked.onItemChecked(false,subscription.getStrPackageTwoId(),subscription,2,position);
        subscription.setMiniSubscriptionChecked(true);
        subscription.setLargeSubscriptionChecked(false);

        onSubscriptionChecked.onItemChecked(true,subscription.getStrPackageOneId(),subscription,1,position);
        notifyItemChanged(position);
      }
    });
//    holder.miniSubscriptionCheck.setOnClickListener(new OnClickListener() {
//      @Override
//      public void onClick(View v) {
//        onSubscriptionChecked.onItemChecked(holder.miniSubscriptionCheck.isChecked(),subscription.getStrPackageOneId(),subscription,1,position);
//      }
//    });

  }

  @RequiresApi(api = VERSION_CODES.LOLLIPOP)
  private void setupLargeSubscriptionCheckbox(final Subscription subscription,
                                              final SubscriptionViewHolder holder, final int position) {
      //holder.largeSubscriptionCheck.setChecked(subscription.isLargeSubscriptionChecked());

    if(subscription.isLargeSubscriptionChecked())
    {
      holder.imgPackageTwoChecked.setVisibility(View.VISIBLE);
      holder.imgPackageTwoUnChecked.setVisibility(View.GONE);
    }else {
      holder.imgPackageTwoChecked.setVisibility(View.GONE);
      holder.imgPackageTwoUnChecked.setVisibility(View.VISIBLE);
    }
    if(subscription.getLargeSubscription() != null && !subscription.getLargeSubscription().isEmpty()) {
      holder.llLarge_subscription.setVisibility(View.VISIBLE);
      holder.txtLargeSubscription.setText(subscription.getLargeSubscription() + " " + context.getString(R.string.days)+" "+subscription.getStrLargeHours() + " " + context.getString(R.string.hours));
      holder.largeSubscriptionPrice.setText(context.getString(R.string.currency_symbol) + " " + Math.round(Double.parseDouble(subscription.getLargeSubscriptionPrice())));
    }else
      holder.llLarge_subscription.setVisibility(View.GONE);

    holder.imgPackageTwoUnChecked.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        if(holder.imgPackageOneChecked.getVisibility() == View.VISIBLE)
          onSubscriptionChecked.onItemChecked(false,subscription.getStrPackageOneId(),subscription,1,position);

        subscription.setLargeSubscriptionChecked(true);
        subscription.setMiniSubscriptionChecked(false);
        onSubscriptionChecked.onItemChecked(true,subscription.getStrPackageTwoId(),subscription,2,position);
        notifyItemChanged(position);
      }
    });
    holder.imgPackageTwoChecked.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {

        subscription.setLargeSubscriptionChecked(false);

        onSubscriptionChecked.onItemChecked(false,subscription.getStrPackageTwoId(),subscription,2,position);
        notifyItemChanged(position);
      }
    });
//    holder.largeSubscriptionCheck.setOnClickListener(new OnClickListener() {
//      @Override
//      public void onClick(View v) {
//        onSubscriptionChecked.onItemChecked(holder.largeSubscriptionCheck.isChecked(),subscription.getStrPackageTwoId(),subscription,2,position);
//      }
//    });
  }


  @Override
  public int getItemCount() {
    return subscriptionList.size();
  }
  @Override
  public int getItemViewType(int position) {
    return (position==subscriptionList.size()-1) ? 1 : 0;
  }
  public void setOnItemClickListener(SubscriptionAdapter.OnItemClickListener listener) {
    this.listener = listener;
  }

  public interface OnItemClickListener {

    void onItemClick(View itemView, int position);
  }

  public class SubscriptionViewHolder extends RecyclerView.ViewHolder {

    private CustomTextViewMedium title;
    private CustomTextViewMedium chapters;
    private CustomTextViewMedium topics;
    private CustomTextViewMedium questions;
    private CustomTextViewMedium miniSubscriptionPrice;
    private CustomTextViewMedium largeSubscriptionPrice;
//    private AppCompatCheckBox miniSubscriptionCheck;
//    private AppCompatCheckBox largeSubscriptionCheck;
    private ImageView iv_subscription,imgPackageOneUnChecked,imgPackageOneChecked,imgPackageTwoUnChecked,imgPackageTwoChecked;

    private CustomTextViewMedium subscriptionDuration;
    private CustomTextViewMedium subscriptionVerify;
    private LinearLayout llLarge_subscription,llMini_subscription;

    private TextView txtMiniSubscription,txtLargeSubscription;
    private View subscriptionAdapterBottomView;
    public SubscriptionViewHolder(@NonNull final View itemView) {
      super(itemView);

      title = itemView.findViewById(R.id.txt_title);
      chapters = itemView.findViewById(R.id.txt_chapters);
      topics = itemView.findViewById(R.id.txt_topics);
      questions = itemView.findViewById(R.id.txt_questions);
      txtLargeSubscription = itemView.findViewById(R.id.txtLargeSubscription);
      txtMiniSubscription = itemView.findViewById(R.id.txtMiniSubscription);
      iv_subscription = itemView.findViewById(R.id.iv_subscription);
      imgPackageOneChecked = itemView.findViewById(R.id.imgPackageOneChecked);
      imgPackageOneUnChecked = itemView.findViewById(R.id.imgPackageOneUnChecked);
      imgPackageTwoUnChecked = itemView.findViewById(R.id.imgPackageTwoUnChecked);
      imgPackageTwoChecked = itemView.findViewById(R.id.imgPackageTwoChecked);
      miniSubscriptionPrice = itemView.findViewById(R.id.txt_mini_subscription_price);
      largeSubscriptionPrice = itemView.findViewById(R.id.txt_full_subscription_price);
//      miniSubscriptionCheck = itemView.findViewById(R.id.checkbox_mini_subscription);
//      largeSubscriptionCheck = itemView.findViewById(R.id.checkbox_large_subscription);
      subscriptionDuration = itemView.findViewById(R.id.txt_subcribed_duration);
      subscriptionVerify = itemView.findViewById(R.id.txt_subscription_verify);
      llMini_subscription = itemView.findViewById(R.id.llMini_subscription);
      llLarge_subscription = itemView.findViewById(R.id.llLarge_subscription);

      itemView.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          if (listener != null) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
              listener.onItemClick(itemView, position);
            }
          }
        }
      });
    }
  }
public void setOnPackageSelectListioner(onSubscriptionChecked onSubscriptionChecked)
{
  this.onSubscriptionChecked = onSubscriptionChecked;
}

}
