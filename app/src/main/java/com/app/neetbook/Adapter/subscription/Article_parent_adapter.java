package com.app.neetbook.Adapter.subscription;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.app.neetbook.Model.HeadData;
import com.app.neetbook.R;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

public class Article_parent_adapter extends RecyclerView.Adapter<Article_parent_adapter.ViewHolder> {

    private ArrayList<HeadData> childData;
    private ArrayList<HeadData> childDataBk;

    public Article_parent_adapter(ArrayList<HeadData> childData) {
        this.childData = childData;
        childDataBk = new ArrayList<>(childData);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvChild;
        ImageView tvExpandCollapseToggle;

        public ViewHolder(View itemView) {
            super(itemView);
            tvChild = (TextView) itemView.findViewById(R.id.txt_article_child_head);
            tvExpandCollapseToggle = (ImageView) itemView.findViewById(R.id.imgArticleChildHead);
        }

    }

    @Override
    public Article_parent_adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.article_chile_head, parent, false);

        Article_parent_adapter.ViewHolder cavh = new Article_parent_adapter.ViewHolder(itemLayoutView);
        return cavh;
    }





    @Override
    public void onBindViewHolder(Article_parent_adapter.ViewHolder holder, final int position) {
        final ViewHolder vh = (ViewHolder) holder;



        HeadData c = childData.get(position);
        vh.tvChild.setText(c.head_name);


    }

    @Override
    public int getItemCount() {
        return childData.size();
    }
}
