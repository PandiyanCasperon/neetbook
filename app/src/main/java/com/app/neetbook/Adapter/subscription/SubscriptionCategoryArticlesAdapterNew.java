package com.app.neetbook.Adapter.subscription;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.neetbook.Adapter.sideMenuContent.SubscriptionCategoryArticlesAdapter;
import com.app.neetbook.Model.sidemenuFromContent.CategoryArticle;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ItemOffsetDecoration;
import com.app.neetbook.View.ScrollingActivity;
import com.app.neetbook.View.SideMenu.SubscriptionArticleActivity;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

public class SubscriptionCategoryArticlesAdapterNew extends RecyclerView.Adapter<SubscriptionCategoryArticlesAdapterNew.ArticleViewHolder> {

  private Context context;
  private ArrayList<CategoryArticle> categoryArticlesList;
  private static int currentPosition = 0;
  private static int chileSeletedPosition = 0;
  Animation slideDown ;
  Configuration configuration;

  public SubscriptionCategoryArticlesAdapterNew(Context context,
                                                ArrayList<CategoryArticle> categoryList) {
    this.context = context;
    this.categoryArticlesList = categoryList;
    slideDown = AnimationUtils.loadAnimation(context, R.anim.articleshowchildanimation);
    configuration = context.getResources().getConfiguration();
  }

  @NonNull
  @Override
  public ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
    View view = layoutInflater.inflate(R.layout.article_parent_adapter,
        parent, false);
    return new ArticleViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull final ArticleViewHolder holder, final int position) {
    CategoryArticle article = categoryArticlesList.get(position);
    holder.title.setText(article.chapt_name);
    holder.llView.setVisibility(View.GONE);
    setChildView(position,holder,article);
    holder.constrainHead.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        currentPosition = position;
        chileSeletedPosition = -1;
        notifyDataSetChanged();
      }
    });

    holder.llView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        context.startActivity(new Intent(context, ScrollingActivity.class));

      }
    });


  }

  private void setChildView(final int position, final ArticleViewHolder holder,CategoryArticle article) {

    if(currentPosition == position) {
      if (article.headDetailsArrayList.size() > 0) {
        Article_parent_adapter article_parent_adapter = new Article_parent_adapter(article.headDetailsArrayList.get(position).headData);
        holder.llChildHead.setLayoutManager(new LinearLayoutManager(context,
                RecyclerView.VERTICAL, false));
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(context, R.dimen._4sdp);
        holder.llChildHead.addItemDecoration(itemDecoration);
        holder.llChildHead.setAdapter(article_parent_adapter);
      }
    }
  }

  @Override
  public int getItemCount() {
    return categoryArticlesList.size();
  }


  class ArticleViewHolder extends ViewHolder {

    private TextView title;
    private RecyclerView llChildHead;
    private LinearLayoutCompat llChildOfChild;
    private ConstraintLayout constrainHead;
    private ConstraintLayout llView;
    private ImageView ivExpand,imageView14;

    private ArticleViewHolder(@NonNull final View itemView) {
      super(itemView);
      title = itemView.findViewById(R.id.tvCategoryTitle);
      llChildHead = itemView.findViewById(R.id.llChildHead);
      constrainHead = itemView.findViewById(R.id.constrainHead);

      llView = itemView.findViewById(R.id.llView);
      ivExpand = itemView.findViewById(R.id.ivExpand);
      imageView14 = itemView.findViewById(R.id.imageView14);
    }
  }


}
