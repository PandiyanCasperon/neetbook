package com.app.neetbook.Adapter.subscription;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.app.neetbook.Adapter.subscription.SubscriptionBottomSheetAdapter.SubscriptionPriceViewHolder;
import com.app.neetbook.Interfaces.OnDeleteItemFromCart;
import com.app.neetbook.Model.subscription.SubscriptionCart;
import com.app.neetbook.R;
import com.app.neetbook.Utils.widgets.CustomTextViewMedium;
import java.util.ArrayList;
import java.util.List;

public class SubscriptionBottomSheetAdapter extends
    RecyclerView.Adapter<SubscriptionPriceViewHolder> {

  private Context context;
  private List<SubscriptionCart> subscriptionList;
  private OnDeleteItemFromCart onDeleteItemFromCart;



  public SubscriptionBottomSheetAdapter(Context context,
      List<SubscriptionCart> subscriptionList) {
    this.context = context;
    this.subscriptionList = subscriptionList;
  }

  @NonNull
  @Override
  public SubscriptionPriceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
    View view = layoutInflater.inflate(R.layout.subscription_price_list, parent, false);
    return new SubscriptionPriceViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull SubscriptionPriceViewHolder holder, final int position) {
    final SubscriptionCart subscription = subscriptionList.get(position);

    holder.title.setText(subscription.getTitle());
    if(subscription.getSubscription().equalsIgnoreCase("Not Available"))
    {
      holder.subscription.setTextColor(Color.RED);
      holder.price.setText("");
    }else {
      holder.price.setText(context.getString(R.string.currency_symbol)+" "+Math.round(Double.parseDouble(subscription.getPrice())));
      holder.subscription.setTextColor(context.getResources().getColor(R.color.black_overlay));
    }
    holder.subscription.setText(subscription.getSubscription());

    holder.appCompatImageView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onDeleteItemFromCart.onDelete(position,subscription);
      }
    });
  }

  @Override
  public int getItemCount() {
    return subscriptionList.size();
  }

  public interface OnSubscriptionClick {

    void OnViewitemClickListioner(int position);
  }

  class SubscriptionPriceViewHolder extends RecyclerView.ViewHolder {

    private CustomTextViewMedium title;
    private CustomTextViewMedium subscription;
    private CustomTextViewMedium price;
    private ImageView appCompatImageView;


    private SubscriptionPriceViewHolder(@NonNull View itemView) {
      super(itemView);

      title = itemView.findViewById(R.id.txt_subscription_title);
      subscription = itemView.findViewById(R.id.txt_subsription_duration);
      price = itemView.findViewById(R.id.txt_subscription_price);
      appCompatImageView = itemView.findViewById(R.id.appCompatImageView);

    }
  }


  public void setOnDeleteItemFromCart(OnDeleteItemFromCart onDeleteItemFromCart)
  {
    this.onDeleteItemFromCart = onDeleteItemFromCart;
  }
}
