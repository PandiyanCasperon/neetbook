package com.app.neetbook.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.neetbook.Model.GroupChildBean;
import com.app.neetbook.R;
import com.app.neetbook.View.ImagesViewActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ArticleImagesAdaper extends RecyclerView.Adapter<ArticleImagesAdaper.ViewHolder>{

    Context context;
//    OnGroupChildClick onChildClick;
    ArrayList<String> images;
    String strTitle;
    public ArticleImagesAdaper(ArrayList<String> images, Context context,String strTitle) {

        this.images = images;
        this.context = context;
        this.strTitle = strTitle;
    }
    @NonNull
    @Override
    public ArticleImagesAdaper.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.article_images_adapter, parent, false);

        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleImagesAdaper.ViewHolder holder, int position) {
        String imageUrl =  images.get(position);
        //holder.llFirstViewSpace.setVisibility(position==0?View.VISIBLE:View.GONE);
        //holder.llLastViewSpace.setVisibility(position==images.size()-1?View.VISIBLE:View.GONE);
        Picasso.with(context).load(IConstant.BaseUrl+imageUrl).into(holder.image_ref_answer);
    }

    @Override
    public int getItemCount() {
        return images.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image_ref_answer;
        private LinearLayout llLastViewSpace,llFirstViewSpace;
        public ViewHolder(View childView) {
            super(childView);

            image_ref_answer = childView.findViewById(R.id.image_ref_answer);
            //this.llLastViewSpace = childView.findViewById(R.id.llLastViewSpace);
            //this.llFirstViewSpace = childView.findViewById(R.id.llFirstViewSpace);
            image_ref_answer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, ImagesViewActivity.class).putExtra("position",""+getAdapterPosition()).putExtra("title",strTitle).putExtra("image",images).putExtra("from","Article"));
                }
            });

        }
    }
}
