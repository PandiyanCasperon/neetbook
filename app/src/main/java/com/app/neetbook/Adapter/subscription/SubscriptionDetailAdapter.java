package com.app.neetbook.Adapter.subscription;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.app.neetbook.Adapter.subscription.SubscriptionDetailAdapter.DetailViewHolder;
import com.app.neetbook.Model.subscription.SubscriptionDetail;
import com.app.neetbook.R;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.widgets.CustomTextViewBold;
import com.app.neetbook.Utils.widgets.CustomTextViewMedium;
import com.app.neetbook.View.ImagesViewActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class SubscriptionDetailAdapter extends RecyclerView.Adapter<DetailViewHolder> {

  private Context context;
  private ArrayList<SubscriptionDetail> subscriptionList;
  private OnItemClickListener listener;
  private SessionManager sessionManager;
  Typeface tfBold,tfMedium,tfRegular;
  public SubscriptionDetailAdapter(Context context, ArrayList<SubscriptionDetail> subscriptionList) {
    this.context = context;
    this.subscriptionList = subscriptionList;
    sessionManager = new SessionManager(this.context);
    tfBold  = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaBold.otf");
    tfMedium  = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaSemibold.otf");
    tfRegular  = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaRegular.otf");
  }

  @NonNull
  @Override
  public DetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
    View view = layoutInflater.inflate(R.layout.mobile_subscription_category_item, parent, false);
    return new DetailViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull DetailViewHolder holder, int position) {
    final SubscriptionDetail subscription = subscriptionList.get(position);
    final String[] CorrectAnswers = subscription.getAnswers().split(",");
    final ArrayList<String> userAnswered = new ArrayList<>();
    holder.title.setText(subscription.getQuestionTitle());
    holder.questionCount.setText((position+1)+".");
    holder.optionOne.setText(subscription.getQuestionOptionFirst());
    holder.optionSecond.setText(subscription.getQuestionOptionSecond());
    holder.optionThird.setText(subscription.getQuestionOptionThird());
    holder.optionFourth.setText(subscription.getQuestionOptionFourth());
    holder.optionFifth.setText(subscription.getQuestionOptionFifth() != null && !subscription.getQuestionOptionFifth().isEmpty()?subscription.getQuestionOptionFifth() : "");

    holder.optionFifth.setVisibility(subscription.getQuestionOptionFifth() != null && !subscription.getQuestionOptionFifth().isEmpty() ? View.VISIBLE : View.GONE);
    holder.txt_E.setVisibility(subscription.getQuestionOptionFifth() != null && !subscription.getQuestionOptionFifth().isEmpty() ? View.VISIBLE : View.GONE);
    holder.imageView.setVisibility(subscription.getImageUrls().size()>0 ? View.VISIBLE : View.GONE);
    holder.optionOne.setTextColor(context.getResources().getColor(R.color.subcription_black_body));
    holder.optionSecond.setTextColor(context.getResources().getColor(R.color.subcription_black_body));
    holder.optionThird.setTextColor(context.getResources().getColor(R.color.subcription_black_body));
    holder.optionFourth.setTextColor(context.getResources().getColor(R.color.subcription_black_body));
    holder.optionFifth.setTextColor(context.getResources().getColor(R.color.subcription_black_body));

    if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
      holder.txt_option_1.setTextAppearance(context,R.style.textViewSmallMcqOption);
      holder.txt_option_2.setTextAppearance(context,R.style.textViewSmallMcqOption);
      holder.txt_option_3.setTextAppearance(context,R.style.textViewSmallMcqOption);
      holder.txt_option_4.setTextAppearance(context,R.style.textViewSmallMcqOption);
      holder.txt_option_5.setTextAppearance(context,R.style.textViewSmallMcqOption);
      holder.optionOne.setTextAppearance(context,R.style.textViewSmallMcqOption);
      holder.optionSecond.setTextAppearance(context,R.style.textViewSmallMcqOption);
      holder.optionThird.setTextAppearance(context,R.style.textViewSmallMcqOption);
      holder.optionFourth.setTextAppearance(context,R.style.textViewSmallMcqOption);
      holder.optionFifth.setTextAppearance(context,R.style.textViewSmallMcqOption);
      holder.title.setTextAppearance(context,R.style.textViewSmallMcqQuestion);
      holder.questionCount.setTextAppearance(context,R.style.textViewSmallMcqQuestion);

    }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {

      holder.txt_option_1.setTextAppearance(context,R.style.textViewMediumMcqOption);
      holder.txt_option_2.setTextAppearance(context,R.style.textViewMediumMcqOption);
      holder.txt_option_3.setTextAppearance(context,R.style.textViewMediumMcqOption);
      holder.txt_option_4.setTextAppearance(context,R.style.textViewMediumMcqOption);
      holder.txt_option_5.setTextAppearance(context,R.style.textViewMediumMcqOption);
      holder.optionOne.setTextAppearance(context,R.style.textViewMediumMcqOption);
      holder.optionSecond.setTextAppearance(context,R.style.textViewMediumMcqOption);
      holder.optionThird.setTextAppearance(context,R.style.textViewMediumMcqOption);
      holder.optionFourth.setTextAppearance(context,R.style.textViewMediumMcqOption);
      holder.optionFifth.setTextAppearance(context,R.style.textViewMediumMcqOption);
      holder.title.setTextAppearance(context,R.style.textViewMediumMcqQuestion);
      holder.questionCount.setTextAppearance(context,R.style.textViewMediumMcqQuestion);

    }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
      holder.txt_option_1.setTextAppearance(context,R.style.textViewLargeMcqOption);
      holder.txt_option_2.setTextAppearance(context,R.style.textViewLargeMcqOption);
      holder.txt_option_3.setTextAppearance(context,R.style.textViewLargeMcqOption);
      holder.txt_option_4.setTextAppearance(context,R.style.textViewLargeMcqOption);
      holder.txt_option_5.setTextAppearance(context,R.style.textViewLargeMcqOption);
      holder.optionOne.setTextAppearance(context,R.style.textViewLargeMcqOption);
      holder.optionSecond.setTextAppearance(context,R.style.textViewLargeMcqOption);
      holder.optionThird.setTextAppearance(context,R.style.textViewLargeMcqOption);
      holder.optionFourth.setTextAppearance(context,R.style.textViewLargeMcqOption);
      holder.optionFifth.setTextAppearance(context,R.style.textViewLargeMcqOption);
      holder.title.setTextAppearance(context,R.style.textViewLargeMcqQuestion);
      holder.questionCount.setTextAppearance(context,R.style.textViewLargeMcqQuestion);

    }

    holder.txt_option_1.setTypeface(tfRegular);
    holder.txt_option_2.setTypeface(tfRegular);
    holder.txt_option_3.setTypeface(tfRegular);
    holder.txt_option_4.setTypeface(tfRegular);
    holder.txt_option_5.setTypeface(tfRegular);
    holder.optionOne.setTypeface(tfRegular);
    holder.optionSecond.setTypeface(tfRegular);
    holder.optionThird.setTypeface(tfRegular);
    holder.optionFourth.setTypeface(tfRegular);
    holder.optionFifth.setTypeface(tfRegular);
    holder.title.setTypeface(tfMedium);
    holder.questionCount.setTypeface(tfMedium);
    if(subscription.getAnswers().replaceAll(",","").length() == 1 && subscription.getUserAnswers() != null && subscription.getUserAnswers().length()==1)
    {
      switch (subscription.getUserAnswers()) {
        case "A":
          holder.optionOne.setTextColor(CorrectAnswers[0].equalsIgnoreCase(subscription.getUserAnswers())?context.getResources().getColor(R.color.choiceGreen) : context.getResources().getColor(R.color.choiceRed));
          break;

        case "B":
          holder.optionSecond.setTextColor(CorrectAnswers[0].equalsIgnoreCase(subscription.getUserAnswers())?context.getResources().getColor(R.color.choiceGreen) : context.getResources().getColor(R.color.choiceRed));
          break;

        case "C":
          holder.optionThird.setTextColor(CorrectAnswers[0].equalsIgnoreCase(subscription.getUserAnswers())?context.getResources().getColor(R.color.choiceGreen) : context.getResources().getColor(R.color.choiceRed));
          break;

        case "D":
          holder.optionFourth.setTextColor(CorrectAnswers[0].equalsIgnoreCase(subscription.getUserAnswers())?context.getResources().getColor(R.color.choiceGreen) : context.getResources().getColor(R.color.choiceRed));
          break;
        case "E":
          holder.optionFifth.setTextColor(CorrectAnswers[0].equalsIgnoreCase(subscription.getUserAnswers())?context.getResources().getColor(R.color.choiceGreen) : context.getResources().getColor(R.color.choiceRed));
          break;
      }
    }
    else if(CorrectAnswers.length > 1 && subscription.getUserAnswers() != null)
    {
      ArrayList<Character> chars = new ArrayList<>();
      ArrayList<String> charsCorrect = new ArrayList<>();
      ArrayList<String> charsWrong = new ArrayList<>();

      for (char ch: subscription.getUserAnswers().toCharArray()) {
        chars.add(ch);
      }
      HashSet<Character> hashSet = new HashSet(chars);
      chars.clear();
      for (Character employee : hashSet) {
        System.out.println(employee);
        chars.add(employee);
      }
      if(chars.size()>0){
      for(int j=0;j<chars.size();j++)
      {
        for (int k=0;k<CorrectAnswers.length;k++)
        {
          if(chars.get(j).toString().equals(CorrectAnswers[k]))
          {
            Log.e("Matched Removed",chars.get(j).toString());
            charsCorrect.add(chars.get(j).toString());
          }
        }
      }

        for (Character person2 : chars) {
          // Loop arrayList1 items
          boolean found = false;
          for (String person1 : charsCorrect) {
            if (person2.toString().equalsIgnoreCase(person1)) {
              found = true;
            }
          }
          if (!found) {
            charsWrong.add(person2.toString());
          }
        }


    /*  if(subscription.getUserAnswers().length() == CorrectAnswers.length) {*/
        if (charsCorrect.size() == CorrectAnswers.length) {
          for (int i = 0; i < charsCorrect.size(); i++) {
            switch (charsCorrect.get(i)) {
              case "A":
                holder.optionOne.setTextColor(context.getResources().getColor(R.color.choiceGreen));
                break;

              case "B":
                holder.optionSecond.setTextColor(context.getResources().getColor(R.color.choiceGreen));
                break;

              case "C":
                holder.optionThird.setTextColor(context.getResources().getColor(R.color.choiceGreen));
                break;

              case "D":
                holder.optionFourth.setTextColor(context.getResources().getColor(R.color.choiceGreen));
                break;

              case "E":
                holder.optionFifth.setTextColor(context.getResources().getColor(R.color.choiceGreen));
                break;
            }
          }
        } else {
          if (charsCorrect.size() > 0) {
            for (int i = 0; i < charsCorrect.size(); i++) {
              switch (charsCorrect.get(i)) {
                case "A":
                  holder.optionOne.setTextColor(context.getResources().getColor(R.color.choiceYellow));
                  break;

                case "B":
                  holder.optionSecond.setTextColor(context.getResources().getColor(R.color.choiceYellow));
                  break;

                case "C":
                  holder.optionThird.setTextColor(context.getResources().getColor(R.color.choiceYellow));
                  break;

                case "D":
                  holder.optionFourth.setTextColor(context.getResources().getColor(R.color.choiceYellow));
                  break;

                case "E":
                  holder.optionFifth.setTextColor(context.getResources().getColor(R.color.choiceYellow));
                  break;
              }
            }
          }
          if (charsWrong.size() > 0) {
           // for (int i = 0; i < charsWrong.size(); i++) {
              switch (charsWrong.get(charsWrong.size()-1)) {
                case "A":
                  holder.optionOne.setTextColor(context.getResources().getColor(R.color.choiceRed));
                  break;

                case "B":
                  holder.optionSecond.setTextColor(context.getResources().getColor(R.color.choiceRed));
                  break;

                case "C":
                  holder.optionThird.setTextColor(context.getResources().getColor(R.color.choiceRed));
                  break;

                case "D":
                  holder.optionFourth.setTextColor(context.getResources().getColor(R.color.choiceRed));
                  break;

                case "E":
                  holder.optionFifth.setTextColor(context.getResources().getColor(R.color.choiceRed));
                  break;
             // }
            }
          }
        }
      }

    }

  }
  public String sortString(String inputString)
  {
    // convert input string to char array
    Log.e("Before Short",inputString);
    char tempArray[] = inputString.toCharArray();

    // sort tempArray
    Arrays.sort(tempArray);

    Log.e("After Short",new String(tempArray));
    // return new sorted string
    return new String(tempArray);
  }
  @Override
  public int getItemCount() {
    return subscriptionList.size();
  }

  // Define the method that allows the parent activity or fragment to define the listener
  public void setOnItemClickListener(OnItemClickListener listener) {
    this.listener = listener;
  }

  public interface OnItemClickListener {

    void onItemClick(View itemView, int position);
    void onAnswerSelected(int position,String Answer);
    void onMultiAnswerSelected(int position,String Answer);
    void onImageClicked(int position);
  }

  class DetailViewHolder extends RecyclerView.ViewHolder {

    private TextView title;
    private TextView questionCount;
    private TextView optionOne,txt_option_1;
    private TextView optionSecond,txt_option_2;
    private TextView optionThird,txt_option_3;
    private TextView optionFourth,txt_option_4;
    private TextView optionFifth,txt_option_5;
    private TextView txt_E;
    private ImageView infoIcon,imageView;


    private DetailViewHolder(@NonNull final View itemView) {
      super(itemView);

      title = itemView.findViewById(R.id.txt_question_title);
      questionCount = itemView.findViewById(R.id.txt_question_count);
      optionOne = itemView.findViewById(R.id.txt_answer_1);
      txt_option_1 = itemView.findViewById(R.id.txt_option_1);
      txt_option_2 = itemView.findViewById(R.id.txt_option_2);
      txt_option_3 = itemView.findViewById(R.id.txt_option_3);
      txt_option_4 = itemView.findViewById(R.id.txt_option_4);
      txt_option_5 = itemView.findViewById(R.id.txt_option_5);
      optionSecond = itemView.findViewById(R.id.txt_answer_2);
      optionThird = itemView.findViewById(R.id.txt_answer_3);
      optionFourth = itemView.findViewById(R.id.txt_answer_4);
      optionFifth = itemView.findViewById(R.id.txt_answer_5);
      txt_E = itemView.findViewById(R.id.txt_option_5);
      infoIcon = itemView.findViewById(R.id.image_info);
      imageView = itemView.findViewById(R.id.imageView);

      infoIcon.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          if (listener != null) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
              listener.onItemClick(itemView, position);
            }
          }
        }
      });
      optionOne.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          if (listener != null) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
              if(subscriptionList.get(position).getAnswers().split(",").length == 1)
                listener.onAnswerSelected(position,"A") ;
              else
                listener.onMultiAnswerSelected(position,"A");
            }
          }
        }
      });
      optionSecond.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          if (listener != null) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
              if(subscriptionList.get(position).getAnswers().split(",").length == 1)
                listener.onAnswerSelected(position,"B") ;
              else
                listener.onMultiAnswerSelected(position,"B");
            }
          }
        }
      });optionThird.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          if (listener != null) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
              if(subscriptionList.get(position).getAnswers().split(",").length == 1)
                listener.onAnswerSelected(position,"C") ;
              else
                listener.onMultiAnswerSelected(position,"C");
            }
          }
        }
      });optionFourth.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          if (listener != null) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
              if(subscriptionList.get(position).getAnswers().split(",").length == 1)
                listener.onAnswerSelected(position,"D") ;
              else
                listener.onMultiAnswerSelected(position,"D");
            }
          }
        }
      });


      optionFifth.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          if (listener != null) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
              if(subscriptionList.get(position).getAnswers().split(",").length == 1)
                listener.onAnswerSelected(position,"E") ;
              else
                listener.onMultiAnswerSelected(position,"E");
            }
          }
        }
      });

      imageView.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          listener.onImageClicked(getAdapterPosition());
        }
      });

    }
    
    
    
  }

}
