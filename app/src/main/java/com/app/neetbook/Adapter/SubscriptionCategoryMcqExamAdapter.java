package com.app.neetbook.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


import com.app.neetbook.Model.HeadData;
import com.app.neetbook.Model.sidemenuFromContent.CategoryArticle;
import com.app.neetbook.R;
import com.app.neetbook.Utils.widgets.CustomTextViewMedium;
import com.app.neetbook.View.SideMenu.SubscriptionDetailActivity;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class SubscriptionCategoryMcqExamAdapter  extends RecyclerView.Adapter<SubscriptionCategoryMcqExamAdapter.McqViewHolder> {

    private Context context;
    private ArrayList<CategoryArticle> itemList;
    private int currentPosition = -1;
    private int chileSeletedPosition = -1;
    Animation slideDown ;
    private String strSubId = "",strSubName = "",strLoading_type = "";
    public SubscriptionCategoryMcqExamAdapter(Context context,
                                          ArrayList<CategoryArticle> categoryList, String strSubId, String strSubName, String strLoading_type) {
        this.context = context;
        this.itemList = categoryList;
        currentPosition = -1;
        this.strSubId = strSubId;
        this.strSubName = strSubName;
        this.strLoading_type = strLoading_type;
        slideDown = AnimationUtils.loadAnimation(context, R.anim.articleshowchildanimation);
    }

    @NonNull
    @Override
    public McqViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.subscription_category_mcq_item,
                parent, false);
        return new McqViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull McqViewHolder holder, final int position) {
        CategoryArticle mcq = itemList.get(position);
        holder.title.setText(mcq.chapt_name);
        holder.llView.setVisibility(View.GONE);
        holder.textView17.setText(String.valueOf(position+1));
        Picasso.with(context).load(IConstant.BaseUrl+mcq.images).into(holder.imageView14);
        setChildView(position,holder,mcq);
        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentPosition == position)
                {
                    currentPosition = -1;
                    chileSeletedPosition = -1;
                    notifyDataSetChanged();
                }else {
                    currentPosition = position;
                    chileSeletedPosition = -1;
                    notifyDataSetChanged();
                }
            }
        });

        holder.llView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // context.startActivity(new Intent(context, SubscriptionDetailActivity.class));
            }
        });

    }
    private void setChildView(final int position, final McqViewHolder holder, final CategoryArticle article) {
        if(currentPosition == position)
        {
            holder.llChildHead.removeAllViews();
            holder.llView.setVisibility(View.VISIBLE);
            holder.ivExpand.setVisibility(View.GONE);
            holder.ivCollapse.setVisibility(View.VISIBLE);
            if(chileSeletedPosition == -1)
                holder.llChildHead.startAnimation(slideDown);
            if(article.mcqExamArrayList.size()>0)
            {
                for(int i=0;i<article.mcqExamArrayList.size();i++)
                {
                    View ChildHeadView = LayoutInflater.from(context).inflate(R.layout.mcq_chiled_item,null);
                    ImageView imgArticleChildHead = ChildHeadView.findViewById(R.id.imgArticleChildHead);
                    TextView txt_article_child_head = ChildHeadView.findViewById(R.id.txt_article_child_head);
                    final LinearLayoutCompat llChildOfChild = ChildHeadView.findViewById(R.id.llChildOfChild);
                    imgArticleChildHead.setVisibility(i== chileSeletedPosition?View.VISIBLE:View.INVISIBLE);
                    txt_article_child_head.setTextColor(i== chileSeletedPosition?context.getResources().getColor(R.color.mcq_blue):context.getResources().getColor(R.color.black));
                    txt_article_child_head.setText(article.mcqExamArrayList.get(i).getExam_cat_name());
                    final int finalI = i;
                    txt_article_child_head.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            chileSeletedPosition = finalI;
                            setChildView(position,holder,article);
                            notifyDataSetChanged();
                            TabMainActivity.isReadPage = true;
                            SubscriptionDetailActivity.mcqExamArrayList = article.mcqExamArrayList;
                            context.startActivity(new Intent(context, SubscriptionDetailActivity.class)
                                    .putExtra("subject_id",strSubId)
                                    .putExtra("subject_name",strSubName)
                                    .putExtra("head_id",article.mcqExamArrayList.get(finalI).get_id())
                                    .putExtra("chapter_id",article._id)
                                    .putExtra("chapter_name",article.short_name)
                                    .putExtra("loading_type",strLoading_type));
                        }
                    });
                    holder.llChildHead.addView(ChildHeadView);
                }
            }



        }else{
            holder.ivExpand.setVisibility(View.VISIBLE);
            holder.ivCollapse.setVisibility(View.GONE);
        }
    }
    @Override
    public int getItemCount() {
        return itemList.size();
    }


    class McqViewHolder extends RecyclerView.ViewHolder {
        private LinearLayoutCompat llChildHead;
        private TextView title,textView17;
        private ConstraintLayout llView;
        ImageView ivCollapse,ivExpand,imageView14;
        private McqViewHolder(@NonNull final View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tvCategoryTitle);
            llView = itemView.findViewById(R.id.llView);
            llChildHead = itemView.findViewById(R.id.llChildHead);
            ivCollapse = itemView.findViewById(R.id.ivCollapse);
            ivExpand = itemView.findViewById(R.id.ivExpand);
            imageView14 = itemView.findViewById(R.id.imageView14);
            textView17 = itemView.findViewById(R.id.textView17);
        }
    }


}
