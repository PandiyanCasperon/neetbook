package com.app.neetbook.Adapter.sideMenuContent;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


import com.app.neetbook.Model.sidemenuFromContent.CategoryPoints;
import com.app.neetbook.R;
import com.app.neetbook.Utils.widgets.CustomTextViewMedium;
import com.app.neetbook.View.SideMenu.SubscriptionDetailActivity;
import com.app.neetbook.View.SideMenu.SubscriptionPointsActivity;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

public class SubscriptionCategoryPointsAdapter extends RecyclerView.Adapter<SubscriptionCategoryPointsAdapter.PointsViewHolder> {

  private Context context;
  private ArrayList<CategoryPoints> itemList;
  private String strSubId = "",strSubName = "";
  private  int currentPosition = -1;
  private  int chileSeletedPosition = -1;
  private int superChileSeletedPosition = -1;
  Animation slideDown ;
  public SubscriptionCategoryPointsAdapter(Context context,
      ArrayList<CategoryPoints> list, String strSubId, String strSubName) {
    this.context = context;
    this.itemList = list;
    this.strSubId = strSubId;
    this.strSubName = strSubName;
    currentPosition = -1;
    slideDown = AnimationUtils.loadAnimation(context, R.anim.articleshowchildanimation);
  }

  @NonNull
  @Override
  public PointsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
    View view = layoutInflater.inflate(R.layout.subscription_category_points_item,
        parent, false);
    return new PointsViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull PointsViewHolder holder, int position) {
    final CategoryPoints points = itemList.get(position);
    holder.title.setText(points.long_name);
    holder.textView17.setText(String.valueOf(position+1));
    Picasso.with(context).load(IConstant.BaseUrl+points.images).into(holder.imageView14);
    /*setChildView(position,holder,points);*/
    holder.pointsItem.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        TabMainActivity.isReadPage = true;
        SubscriptionPointsActivity.pointsContentArrayList = points.pointsContentArrayList;
        context.startActivity(new Intent(context, SubscriptionPointsActivity.class).putExtra("chapter_id",points._id).putExtra("subject_id",strSubId).putExtra("chapter_name",points.short_name).putExtra("subject_name",strSubName).putExtra("points",points.pointsContentArrayList));
      }
    });
  }

  private void setChildView(final int position, final PointsViewHolder holder, final CategoryPoints points) {
    if(currentPosition == position)
    {
      holder.llChildHead.removeAllViews();
      holder.llView.setVisibility(View.VISIBLE);
      holder.ivExpand.setVisibility(View.GONE);
      holder.ivCollapse.setVisibility(View.VISIBLE);
      if(chileSeletedPosition == -1 && superChileSeletedPosition == -1)
        holder.llChildHead.startAnimation(slideDown);
      if(points.pointsContentArrayList.size()>0)
      {
        for(int i=0;i<points.pointsContentArrayList.size();i++)
        {
          View ChildHeadView = LayoutInflater.from(context).inflate(R.layout.mcq_chiled_item,null);
          ImageView imgArticleChildHead = ChildHeadView.findViewById(R.id.imgArticleChildHead);
          TextView txt_article_child_head = ChildHeadView.findViewById(R.id.txt_article_child_head);
          final LinearLayoutCompat llChildOfChild = ChildHeadView.findViewById(R.id.llChildOfChild);
          final int finalI = i;
          txt_article_child_head.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              chileSeletedPosition = finalI;
              superChileSeletedPosition = -1;
              setChildView(position,holder,points);
              notifyDataSetChanged();
                TabMainActivity.isReadPage = true;
                SubscriptionPointsActivity.pointsContentArrayList = points.pointsContentArrayList;
                context.startActivity(new Intent(context, SubscriptionPointsActivity.class).putExtra("chapter_id",points._id).putExtra("subject_id",strSubId).putExtra("chapter_name",points.short_name).putExtra("subject_name",strSubName).putExtra("points",points.pointsContentArrayList));

            }
          });

          holder.llChildHead.addView(ChildHeadView);
        }
      }



    }else{
      holder.ivExpand.setVisibility(View.VISIBLE);
      holder.ivCollapse.setVisibility(View.GONE);
    }
  }

  @Override
  public int getItemCount() {
    return itemList.size();
  }

  class PointsViewHolder extends ViewHolder {

    private TextView textView17,title;
    private ImageView imageView14;
    private ConstraintLayout pointsItem;
    private LinearLayoutCompat llChildHead;
    private ConstraintLayout llView;
    ImageView ivCollapse,ivExpand;
    private PointsViewHolder(@NonNull final View itemView) {
      super(itemView);
      title = itemView.findViewById(R.id.tvCategoryTitle);
      textView17 = itemView.findViewById(R.id.textView17);
      imageView14 = itemView.findViewById(R.id.imageView14);
      pointsItem = itemView.findViewById(R.id.pointsItem);
      /*llView = itemView.findViewById(R.id.llView);
      llChildHead = itemView.findViewById(R.id.llChildHead);
      ivCollapse = itemView.findViewById(R.id.ivCollapse);
      ivExpand = itemView.findViewById(R.id.ivExpand);*/
    }
  }

}
