package com.app.neetbook.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.neetbook.R;
import com.app.neetbook.View.ImagesViewActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.zip.GZIPOutputStream;

import androidx.recyclerview.widget.RecyclerView;

public class InnerImageListAdapter extends RecyclerView.Adapter<InnerImageListAdapter.SingleItemRowHolder> {

    private ArrayList<String> itemsList;
    public Context mContext;
    private String titleName;

    public InnerImageListAdapter(Context context, ArrayList<String> itemsList, String titleName) {
        this.itemsList = itemsList;
        this.mContext = context;
        this.titleName = titleName;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.article_inner_recycler_image_item, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {




       // holder.llFirstViewSpace.setVisibility(i==0?View.VISIBLE:View.GONE);
       // holder.llLastViewSpace.setVisibility(i==itemsList.size()-1?View.VISIBLE:View.GONE);

        Glide.with(mContext)
                .load(IConstant.BaseUrl+itemsList.get(i))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                /*.error(R.drawable.bg)*/
                .into(holder.itemImage);
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle;

        protected ImageView itemImage;
        private LinearLayout llLastViewSpace,llFirstViewSpace;


        public SingleItemRowHolder(View view) {
            super(view);


            this.itemImage = (ImageView) view.findViewById(R.id.img);
         


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.startActivity(new Intent(mContext, ImagesViewActivity.class).putExtra("position",""+getAdapterPosition()).putExtra("title",titleName).putExtra("image",itemsList).putExtra("from","Article"));
                }
            });


        }

    }
}
