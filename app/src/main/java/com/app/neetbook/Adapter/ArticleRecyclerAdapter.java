package com.app.neetbook.Adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BulletSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.neetbook.Model.ArticleContent.ArticleChildItem;
import com.app.neetbook.Model.ArticleContent.ArticleItem;
import com.app.neetbook.R;
import com.app.neetbook.StickeyHeader.AdapterDataProvider;
import com.app.neetbook.Utils.CustomTagHandler;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.Thread.AppExecutors;

import net.nightwhistler.htmlspanner.HtmlSpanner;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.core.text.HtmlCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import org.sufficientlysecure.htmltextview.HtmlAssetsImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

public class ArticleRecyclerAdapter extends RecyclerView.Adapter<ArticleRecyclerAdapter.BaseViewHolder> implements AdapterDataProvider {

    private final List<Object> dataList = new ArrayList<>();
    int currentPosition = -1;
    SessionManager sessionManager;
    String strName;
    Typeface tfBold, tfMedium, tfRegular;
    LinearSnapHelper snapHelper;
    Context context;
    private AppExecutors appexector;
    private Boolean loaded;

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.article_content_section_child_item, parent, false));
        } else {
            return new HeaderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.article_content_section_header, parent, false));
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(final BaseViewHolder holder, final int position) {

        final Object item = dataList.get(position);

        if (item instanceof ArticleChildItem) {

            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

            //itemViewHolder.html_text.setHtml(((ArticleChildItem) item).content,new HtmlAssetsImageGetter( itemViewHolder.html_text));

            if (sessionManager.isNightModeEnabled()) {

                appexector.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {

                        String font_size = "14";

                        if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("small")) {

                            font_size = "14";

                        } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {

                            font_size = "15";

                        } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("large")) {

                            font_size = "16";

                        }

                        String finalHtml = "<html><head>"
                                + "<style type=\"text/css\">li{color: #fff} span {color: #000}"
                                + "</style></head>"
                                + "<body style='font-size:" + font_size + "px" + "'>" + "<font color='white'>"
                                + ((ArticleChildItem) item).content + "</font>"
                                + "</body></html>";

                        //((ItemViewHolder) holder).webView.loadData(finalHtml,"text/html", "UTF-8");
                        ((ItemViewHolder) holder).webView.loadDataWithBaseURL(null, finalHtml, "text/html", "UTF-8", null);
                        ((ItemViewHolder) holder).webView.setBackgroundColor(context.getColor(R.color.night_color_new));
                    }
                });

            } else {

               /* appexector.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {*/

                String font_size = "14";

                if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("small")) {

                    font_size = "14";

                } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {

                    font_size = "15";

                } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("large")) {

                    font_size = "16";

                }

                String finalHtml = "<html><head>"
                        + "<style type=\"text/css\">li{color: #000} span {color: #000}"
                        + "</style></head>"
                        + "<body style='font-size:" + font_size + "px" + "'>" + "<font color='black'>"
                        + ((ArticleChildItem) item).content + "</font>"
                        + "</body></html>";


                //((ItemViewHolder) holder).webView.loadData(finalHtml,"text/html", "UTF-8");
               // if (!((ArticleChildItem) item).is_webview_loaded) {

                    ((ItemViewHolder) holder).webView.loadDataWithBaseURL(null, finalHtml, "text/html", "UTF-8", null);


               // }

                ((ItemViewHolder) holder).webView.setBackgroundColor(context.getColor(R.color.white_color_new));


                ((ItemViewHolder) holder).webView.setWebChromeClient(new WebChromeClient() {
                    public void onProgressChanged(WebView view, int newProgress) {
                        super.onProgressChanged(view, newProgress);
                        Log.e("Webview_Pre_Position", String.valueOf(position));
                        if (newProgress == 100) {
                            //When the loading is 100% completed; todo

                         /*   final Object item = dataList.get(position);

                            if (item instanceof ArticleChildItem) {

                                ((ArticleChildItem) item).is_webview_loaded = true;
                            }*/
                           // ((ArticleChildItem) item).is_webview_loaded = true;
                            loaded = true;
                            Log.e("Webview_Position", String.valueOf(position));

                        }
                    }
                });


                        /*   }
                });
*/
            }

            itemViewHolder.innerRecycler.setVisibility(((ArticleChildItem) item).images.size() > 0 ? View.VISIBLE : View.GONE);
            if (context.getResources().getBoolean(R.bool.isTablet))
                itemViewHolder.llRecyclerView.setVisibility(((ArticleChildItem) item).images.size() > 0 ? View.VISIBLE : View.GONE);
            ArrayList<String> singleSectionItems = ((ArticleChildItem) item).images;
            InnerImageListAdapter innerImageListAdapter = new InnerImageListAdapter(context, singleSectionItems, ((ArticleChildItem) item).title);
            itemViewHolder.innerRecycler.setHasFixedSize(true);
            itemViewHolder.innerRecycler.setLayoutManager(context.getResources().getBoolean(R.bool.isTablet) ? new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false) : new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            itemViewHolder.innerRecycler.setAdapter(innerImageListAdapter);
            setFontChild(itemViewHolder);

            ((ItemViewHolder) holder).webView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });
            ((ItemViewHolder) holder).webView.setLongClickable(false);

        } else if (item instanceof ArticleItem) {

            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;

            if (((ArticleItem) item).title != null && !((ArticleItem) item).title.equals("")) {

                headerViewHolder.points_header_text.setVisibility(View.VISIBLE);
                headerViewHolder.ivCircle.setVisibility(View.VISIBLE);
                headerViewHolder.ivLine.setVisibility(View.VISIBLE);
                headerViewHolder.points_header_text.setText(((ArticleItem) item).title);

            } else {

                headerViewHolder.points_header_text.setVisibility(View.GONE);
                headerViewHolder.ivCircle.setVisibility(View.GONE);
                headerViewHolder.ivLine.setVisibility(View.GONE);
                headerViewHolder.ivCircle.setVisibility(View.GONE);
            }

            setFontHeader(headerViewHolder);

        }
    }

    public static int spToPx(float sp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
    }

    private void setFontHeader(HeaderViewHolder holder) {
        if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
            holder.points_header_text.setTextAppearance(context, R.style.textViewSmallArticleHeader);
        } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
            holder.points_header_text.setTextAppearance(context, R.style.textViewSmallArticleHeader);
        } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
            holder.points_header_text.setTextAppearance(context, R.style.textViewSmallArticleHeader);
        }

        holder.points_header_text.setTypeface(tfMedium);

    }

    private void setFontChild(ItemViewHolder holder) {
        /*final WebSettings webSettings = holder.webView.getSettings();
        Resources res = context.getResources();
        int fontSize = 0;*/
        if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
            holder.article_text.setTextAppearance(context, R.style.textViewSmallArticleContent);
            /* fontSize = (int) res.getDimension(R.dimen.textSizeSmall);*/
        } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
            holder.article_text.setTextAppearance(context, R.style.textViewMediumArticleContent);
            /*fontSize = (int) res.getDimension(R.dimen.textSizeMedium);*/
        } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
            holder.article_text.setTextAppearance(context, R.style.textViewLargeArticleContent);
            /* fontSize = (int) res.getDimension(R.dimen.textSizeLarge);*/
        }

        holder.article_text.setTypeface(tfRegular);
        /*webSettings.setDefaultFontSize(fontSize);*/
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return dataList.get(position) instanceof ArticleChildItem ? 0 : 1;
    }

    @Override
    public List<?> getAdapterData() {
        return dataList;
    }

    public void setDataList(List<Object> items, Context context) {
        dataList.clear();
        dataList.addAll(items);
        this.context = context;
        this.sessionManager = new SessionManager(this.context);
        appexector = new AppExecutors();
        tfBold = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaRegular.otf");
        createSnap();
        notifyDataSetChanged();
    }

    public void addDataList(List<Object> items) {
        if (items != null) {
            int start = dataList.size();
            dataList.addAll(items);
            notifyItemRangeInserted(start, items.size());
        }
    }

    private static final class ItemViewHolder extends BaseViewHolder {

        TextView article_text;
        ImageView ivLine, ivCircle;
        LinearLayout article_image_container;
        RecyclerView innerRecycler;
        RelativeLayout llRecyclerView;
        HtmlTextView html_text;
        WebView webView;

        ItemViewHolder(View itemView) {
            super(itemView);
            webView = itemView.findViewById(R.id.webView);
            article_text = itemView.findViewById(R.id.article_text);
            ivCircle = itemView.findViewById(R.id.ivCircle);
            ivLine = itemView.findViewById(R.id.ivLine);
            innerRecycler = itemView.findViewById(R.id.innerRecycler);
            llRecyclerView = itemView.findViewById(R.id.llRecyclerView);
            html_text = itemView.findViewById(R.id.html_text);

        }
    }

    private static final class HeaderViewHolder extends BaseViewHolder {

        TextView points_header_text;
        ImageView ivLine, ivCircle;

        HeaderViewHolder(View itemView) {
            super(itemView);
            points_header_text = itemView.findViewById(R.id.points_header_text);
            ivCircle = itemView.findViewById(R.id.ivCircle);
            ivLine = itemView.findViewById(R.id.ivLine);

        }
    }

    static class BaseViewHolder extends RecyclerView.ViewHolder {

        BaseViewHolder(View itemView) {
            super(itemView);
        }
    }

    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
    }
}