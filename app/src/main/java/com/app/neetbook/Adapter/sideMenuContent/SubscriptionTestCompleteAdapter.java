package com.app.neetbook.Adapter.sideMenuContent;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.app.neetbook.Model.sidemenuFromContent.SubscriptionTestComplete;
import com.app.neetbook.R;
import com.app.neetbook.Utils.widgets.CustomTextViewBold;
import com.app.neetbook.Utils.widgets.CustomTextViewMedium;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;

public class SubscriptionTestCompleteAdapter extends RecyclerView.Adapter<SubscriptionTestCompleteAdapter.TestCompleteViewHolder> {

  private Context context;
  private ArrayList<SubscriptionTestComplete> subscriptionList;
  private OnItemClickListener listener;

  public SubscriptionTestCompleteAdapter(Context context,
      ArrayList<SubscriptionTestComplete> subscriptionList) {
    this.context = context;
    this.subscriptionList = subscriptionList;
  }

  @NonNull
  @Override
  public TestCompleteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
    View view = layoutInflater
        .inflate(R.layout.mobile_subscription_test_completed_item, parent, false);
    return new TestCompleteViewHolder(view);
  }

  @SuppressLint("RestrictedApi")
  @Override
  public void onBindViewHolder(@NonNull TestCompleteViewHolder holder, int position) {
    SubscriptionTestComplete subscription = subscriptionList.get(position);

    holder.title.setText(subscription.getQuestionTitle());
    holder.questionCount.setText(subscription.getQuestionCount());
    holder.optionOne.setText(subscription.getQuestionOptionFirst());
    holder.optionTwo.setText(subscription.getQuestionOptionSecond());
    holder.optionThree.setText(subscription.getQuestionOptionThird());
    holder.optionFour.setText(subscription.getQuestionOptionFourth());

    holder.checkBoxOne
        .setSupportButtonTintList(ColorStateList.valueOf(Color.parseColor("#777777")));
    holder.checkBoxTwo
        .setSupportButtonTintList(ColorStateList.valueOf(Color.parseColor("#777777")));
    holder.checkBoxThree
        .setSupportButtonTintList(ColorStateList.valueOf(Color.parseColor("#777777")));
    holder.checkBoxFour
        .setSupportButtonTintList(ColorStateList.valueOf(Color.parseColor("#777777")));
  }

  @Override
  public int getItemCount() {
    return subscriptionList.size();
  }

  // Define the method that allows the parent activity or fragment to define the listener
  public void setOnItemClickListener(OnItemClickListener listener) {
    this.listener = listener;
  }


  public interface OnItemClickListener {

    void onItemClick(View itemView, int position);
  }

  class TestCompleteViewHolder extends RecyclerView.ViewHolder {

    private CustomTextViewBold title;
    private CustomTextViewBold questionCount;
    private CustomTextViewMedium optionOne, optionTwo, optionThree, optionFour;
    private AppCompatCheckBox checkBoxOne, checkBoxTwo, checkBoxThree, checkBoxFour;

    private TestCompleteViewHolder(@NonNull final View itemView) {
      super(itemView);

      title = itemView.findViewById(R.id.txt_question_title);
      questionCount = itemView.findViewById(R.id.txt_question_count);
      optionOne = itemView.findViewById(R.id.txt_answer_1);
      optionTwo = itemView.findViewById(R.id.txt_answer_2);
      optionThree = itemView.findViewById(R.id.txt_answer_3);
      optionFour = itemView.findViewById(R.id.txt_answer_4);

      checkBoxOne = itemView.findViewById(R.id.checkbox_option1);
      checkBoxTwo = itemView.findViewById(R.id.checkbox_option2);
      checkBoxThree = itemView.findViewById(R.id.checkbox_option3);
      checkBoxFour = itemView.findViewById(R.id.checkbox_option4);

    }
  }

}
