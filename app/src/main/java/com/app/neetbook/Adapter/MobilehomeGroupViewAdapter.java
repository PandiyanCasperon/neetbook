package com.app.neetbook.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.neetbook.Interfaces.OnSubjectsClickListener;
import com.app.neetbook.Model.GroupChildBean;
import com.app.neetbook.R;
import com.app.neetbook.serviceRequest.IConstant;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class MobilehomeGroupViewAdapter extends RecyclerView.Adapter<MobilehomeGroupViewAdapter.ViewHolder>{
    ArrayList<GroupChildBean> groupChildBeanArrayList;
    Context context;
    OnSubjectsClickListener onSubjectsClickListener;
    int called = 0;

    public MobilehomeGroupViewAdapter( ArrayList<GroupChildBean> groupChildBeanArrayList, Context context) {

        this.groupChildBeanArrayList = groupChildBeanArrayList;
        this.context = context;
        called = 0;
    }
    @NonNull
    @Override
    public MobilehomeGroupViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.new_mobile_homegroup_child_adapter, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull final MobilehomeGroupViewAdapter.ViewHolder holder, final int position) {
        final GroupChildBean groupChildBean = groupChildBeanArrayList.get(position);

       // holder.rlBottom.setVisibility(position==groupChildBeanArrayList.size()-1?View.VISIBLE:View.GONE);
       // holder.rlTop.setVisibility(position==0?View.VISIBLE:View.GONE);
        if(position == groupChildBeanArrayList.size()-1 && called == 0)
        {
            called = 1;
            onSubjectsClickListener.onScrollEnd(position);
        }
        holder.txtTitle.setText(groupChildBean.getStrLongName());
        holder.txtChapters.setText(groupChildBean.getStrChapter()+" "+ context.getString(R.string.chapters));
        holder.txtTopics.setText(groupChildBean.getStrTopics()+" "+ context.getString(R.string.topics));
        holder.txtMCQ.setText(groupChildBean.getStrMCQ()+" "+ context.getString(R.string.mcqs));
        //setPropertiesEnable(false,holder);
        holder.txtActionSubscribeOrRead.setVisibility(View.GONE);
        holder.txtNotAvail.setVisibility(View.VISIBLE);
        if(groupChildBean.getStrWeekOne() != null && !groupChildBean.getStrWeekOne().isEmpty()){
            holder.txtWeeksOneTitle.setText(groupChildBean.getStrWeekOne()+" "+ context.getString(R.string.days));
            holder.txtWeeksOneAmount.setText(context.getString(R.string.currency_symbol)+" "+Math.round(Double.parseDouble(groupChildBean.getStrWeekontAmount())));
            holder.txtActionSubscribeOrRead.setVisibility(View.VISIBLE);
            holder.txtNotAvail.setVisibility(View.GONE);
            //setPropertiesEnable(true,holder);
        }
        if(groupChildBean.getStrWeekTwo() != null && !groupChildBean.getStrWeekTwo().isEmpty()){
            holder.txtWeeksTwoTitle.setText(groupChildBean.getStrWeekTwo()+" "+ context.getString(R.string.days));
            holder.txtWeeksTwoAmount.setText(context.getString(R.string.currency_symbol)+" "+Math.round(Double.parseDouble(groupChildBean.getStrWeekTwoAmount())));
            holder.txtActionSubscribeOrRead.setVisibility(View.VISIBLE);
            holder.txtNotAvail.setVisibility(View.GONE);
            //setPropertiesEnable(true,holder);
        }

        holder.constrainPlan.setVisibility(groupChildBean.getStrIsSubcribed().equalsIgnoreCase("No") ? View.VISIBLE : View.GONE);
        holder.constrainRemaining.setVisibility(groupChildBean.getStrIsSubcribed().equalsIgnoreCase("Yes") ? View.VISIBLE : View.GONE);

        if(groupChildBean.getStrTrial().equals("1"))
        {
            holder.txtActionSubscribeOrRead.setVisibility(View.VISIBLE);
            holder.txtNotAvail.setVisibility(View.GONE);
            holder.constrainRemaining.setVisibility(View.GONE);
            holder.constrainPlan.setVisibility(View.GONE);
            holder.txtActionSubscribeOrRead.setText(context.getString(R.string.read));
            holder.txtActionSubscribeOrRead.setTextColor(context.getResources().getColor(R.color.white));
            holder.txtActionSubscribeOrRead.setBackground(context.getResources().getDrawable(R.drawable.home_read_btn_bg));
        }else
        {
            if(groupChildBean.getStrIsSubcribed().equalsIgnoreCase("Yes")){
                holder.txtHoursRemaining.setText(groupChildBean.getStrRemainingHours());
                holder.txDaysRemaining.setText(groupChildBean.getStrRemainingDays()+" "+context.getString(R.string.days)+"\n"+context.getString(R.string.remaining));
                holder.txtActionSubscribeOrRead.setText(context.getString(R.string.read));
                holder.txtActionSubscribeOrRead.setTextColor(context.getResources().getColor(R.color.white));
                holder.txtActionSubscribeOrRead.setBackground(context.getResources().getDrawable(R.drawable.home_read_btn_bg));
            }else
            {
                holder.txtActionSubscribeOrRead.setText(context.getString(R.string.subscribe));
                holder.txtActionSubscribeOrRead.setTextColor(context.getResources().getColor(R.color.white));
                holder.txtActionSubscribeOrRead.setBackground(context.getResources().getDrawable(R.drawable.home_subscribe_btn_bg));
            }
        }
        Log.e("Home List",IConstant.BaseUrl+groupChildBean.getStrImages());
        Picasso.with(context).load(IConstant.BaseUrl+groupChildBean.getStrImages()).placeholder(R.drawable.img_placeholder).into(holder.imgView);
        holder.txtActionSubscribeOrRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.txtActionSubscribeOrRead.getText().toString().equalsIgnoreCase(context.getString(R.string.read))){
                    onSubjectsClickListener.onRead(groupChildBean.getStrId(),position);
                }else
                {
                    onSubjectsClickListener.onSubscribed(groupChildBean.getStrId(),position);
                }
            }
        });
    }

    private void setPropertiesEnable(boolean isShow, ViewHolder holder)
    {
        holder.txtTitle.setVisibility(isShow? View.VISIBLE:View.GONE);
        holder.txtChapters.setVisibility(isShow? View.VISIBLE:View.GONE);
        holder.txtTopics.setVisibility(isShow? View.VISIBLE:View.GONE);
        holder.txtMCQ.setVisibility(isShow? View.VISIBLE:View.GONE);
    }
    @Override
    public int getItemCount() {
        return groupChildBeanArrayList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtNotAvail,txtTitle,txtChapters,txtTopics,txtMCQ,txtHoursRemaining,txDaysRemaining,txtWeeksOneTitle,txtWeeksOneAmount,txtWeeksTwoTitle,txtActionSubscribeOrRead,txtWeeksTwoAmount;
        LinearLayout constrainRemaining,constrainPlan;
        ImageView imgView;
       // RelativeLayout rlTop,rlBottom;
        public ViewHolder(View childView) {
            super(childView);

           // rlTop = childView.findViewById(R.id.rlTop);
            //rlBottom = childView.findViewById(R.id.rlBottom);
            imgView = childView.findViewById(R.id.imgView);
            txtTitle = childView.findViewById(R.id.txtTitle);
            txtNotAvail = childView.findViewById(R.id.txtNotAvail);
            txtChapters = childView.findViewById(R.id.txtChapters);
            txtTopics = childView.findViewById(R.id.txtTopics);
            txtMCQ = childView.findViewById(R.id.txtMCQ);
            txtHoursRemaining = childView.findViewById(R.id.txtHoursRemaining);
            txDaysRemaining = childView.findViewById(R.id.txDaysRemaining);
            constrainRemaining = childView.findViewById(R.id.constrainRemaining);
            constrainPlan = childView.findViewById(R.id.constrainPlan);
            txtWeeksOneTitle = childView.findViewById(R.id.txtWeeksOneTitle);
            txtWeeksOneAmount = childView.findViewById(R.id.txtWeeksOneAmount);
            txtWeeksTwoTitle = childView.findViewById(R.id.txtWeeksTwoTitle);
            txtWeeksTwoAmount = childView.findViewById(R.id.txtWeeksTwoAmount);
            txtActionSubscribeOrRead = childView.findViewById(R.id.txtActionSubscribeOrRead);
        }
    }

    public void setOnChildClick(OnSubjectsClickListener onSubjectsClickListener) {
        this.onSubjectsClickListener = onSubjectsClickListener;
    }




    private String calculateHourMinute(long milliseconds)
    {
        return (new SimpleDateFormat("HH:mm")).format(new Date(milliseconds));
    }
}
