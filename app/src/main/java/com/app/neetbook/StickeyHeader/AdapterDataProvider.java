package com.app.neetbook.StickeyHeader;

import java.util.List;

public interface AdapterDataProvider {

    List<?> getAdapterData();
}
