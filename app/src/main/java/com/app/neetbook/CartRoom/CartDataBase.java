package com.app.neetbook.CartRoom;

import android.content.Context;

import com.app.neetbook.Model.CartList;
import com.app.neetbook.Model.subscription.SubscriptionCart;
import com.app.neetbook.homepageRoom.GroupslistDao;
import com.app.neetbook.homepageRoom.GroupslistDatabase;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = SubscriptionCart.class,exportSchema =  false,version = 1)
@TypeConverters({CartConverters.class})
public abstract class CartDataBase extends RoomDatabase {

    private static CartDataBase INSTANCE;

    public abstract CartListDao cartListDao();

    public static CartDataBase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), CartDataBase.class, "CartDatabase")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
