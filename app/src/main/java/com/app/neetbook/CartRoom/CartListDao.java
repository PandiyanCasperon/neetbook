package com.app.neetbook.CartRoom;

import com.app.neetbook.Model.CartList;
import com.app.neetbook.Model.subscription.SubscriptionCart;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface CartListDao {

    @Query("SELECT * FROM CartItems")
    List<SubscriptionCart> getAll();

    @Query("SELECT COUNT(*) from CartItems")
    int countCart();

    @Query("SELECT * FROM CartItems WHERE strId == :id")
    public abstract SubscriptionCart getCartItem(String id);

    @Insert(onConflict = REPLACE)
    void insertAll(SubscriptionCart... users);

    @Update
    void update(SubscriptionCart repos);

    @Query("DELETE FROM CartItems WHERE strId = :cartId")
    abstract void deleteByCartId(String cartId);

    @Delete
    void delete(SubscriptionCart user);

    @Query("DELETE FROM CartItems")
    public void nukeTable();
}
