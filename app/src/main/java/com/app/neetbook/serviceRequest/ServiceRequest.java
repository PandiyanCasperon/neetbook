package com.app.neetbook.serviceRequest;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.alerter.Alert;
import com.app.neetbook.Utils.alerter.AlertBox;
import com.app.neetbook.Utils.alerter.Alerter;
import com.app.neetbook.View.ContactUs;
import com.app.neetbook.View.DrawerContentSlideActivity;
import com.app.neetbook.View.MobilePagesActivity;
import com.app.neetbook.View.NoInterNetActivity;
import com.app.neetbook.View.SplashActivity;
import com.app.neetbook.View.TabletHome;
import com.app.neetbook.View.TermsConditions;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceRequest {
    private static final Object TAG =ServiceRequest.class.getSimpleName() ;
    private Context context;
    private ServiceListener mServiceListener;
    private StringRequest stringRequest;
    SessionManager session;
    private String UserID = "", fcmID = "";
    private boolean isDemoEnabled = true;
    private String userID = "";
    private Dialog dialog;
    private RequestQueue mRequestQueue;

    ConnectionDetector cd;

    public interface ServiceListener {
        void onCompleteListener(String response);
        void onErrorListener(String errorMessage);
    }

    public ServiceRequest(Context context) {
        this.context = context;
        session=new SessionManager(context);
        cd = new ConnectionDetector(context);
        HashMap<String, String> user = session.getUserDetails();

        userID = user.get(SessionManager.KEY_UID);
        if(cd.isConnectingToInternet()) {

        }else{
            context.startActivity(new Intent(context, NoInterNetActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            if(DrawerContentSlideActivity.drawerContentSlideActivity != null)
                DrawerContentSlideActivity.drawerContentSlideActivity.finish();
            if(MobilePagesActivity.mobilePagesActivity != null)
                MobilePagesActivity.mobilePagesActivity.finish();
            if(TabletHome.PagesActivity != null)
                TabletHome.PagesActivity.finish();
            if(TermsConditions.PagesActivity != null)
                TermsConditions.PagesActivity.finish();
            if(ContactUs.PagesActivity != null)
                ContactUs.PagesActivity.finish();

        }
    }

    public void cancelRequest()
    {
        if (stringRequest != null) {
            stringRequest.cancel();
        }
    }
    public void makeServiceRequest(final String url, int method, final HashMap<String, String> param,ServiceListener listener) {

        Log.e("Request Url",url);
        if(param != null)
            Log.e("Post params",param.toString());
        this.mServiceListener=listener;

        stringRequest = new StringRequest(method, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.getString("status").equals("02")) {
                    session.logoutUser();
                    }else
                        mServiceListener.onCompleteListener(response);

                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String strError = "";
                try {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        strError = context.getString(R.string.timeOutError);
                    } else if (error instanceof AuthFailureError) {
                        strError = context.getString(R.string.authFailureError);
                    } else if (error instanceof ServerError) {
                        strError = context.getString(R.string.serverError);
                    } else if (error instanceof NetworkError) {
                        strError = context.getString(R.string.networkError);
                    } else if (error instanceof ParseError) {
                        strError = context.getString(R.string.parseError);
                    }
                } catch (Exception e) {
                }


                mServiceListener.onErrorListener(strError);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return param;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Log.e("Header",session.getApiHeader().toString());
                return session.getApiHeader();
            }
        };



        //to avoid repeat request Multiple Time
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(retryPolicy);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(15000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        addToRequestQueue(stringRequest);
    }


    public void makeServiceRequestExceptHeader(final String url, int method, final HashMap<String, String> param,ServiceListener listener) {

        Log.e("Request Url",url);
        if(param != null)
            Log.e("Post params",param.toString());
        this.mServiceListener=listener;

        stringRequest = new StringRequest(method, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.getString("status").equals("02")) {
                    session.logoutUser();
                    }else
                        mServiceListener.onCompleteListener(response);

                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String strError = "";
                try {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        strError = context.getString(R.string.timeOutError);
                    } else if (error instanceof AuthFailureError) {
                        strError = context.getString(R.string.authFailureError);
                    } else if (error instanceof ServerError) {
                        strError = context.getString(R.string.serverError);
                    } else if (error instanceof NetworkError) {
                        strError = context.getString(R.string.networkError);
                    } else if (error instanceof ParseError) {
                        strError = context.getString(R.string.parseError);
                    }
                } catch (Exception e) {
                }


                mServiceListener.onErrorListener(strError);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return param;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> user = new HashMap<String, String>();

                user.put("cache-control", "no-cache");
                user.put("postman-token", "e0328e45-1a24-8b91-3485-181c043aa01e");

                return user;

            }
        };



        //to avoid repeat request Multiple Time
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(retryPolicy);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(15000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        addToRequestQueue(stringRequest);
    }



    private ServiceListener updateAvailablityServiceListener = new ServiceListener() {
        @Override
        public void onCompleteListener(String response) {


            session.logoutUser();

            //context.stopService(new Intent(context, XMPPService.class));


            ServiceStop();

            dialog.dismiss();

            Intent intent = new Intent(context, SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        }

        @Override
        public void onErrorListener(String errorMessage) {

            session.logoutUser();
            // context.stopService(new Intent(context, XMPPService.class));

            ServiceStop();

            dialog.dismiss();

            Intent intent = new Intent(context, SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);

        }

    };


    private void ServiceStop(){

//        HashMap<String, String> domain = session.getXmpp();
//        HashMap<String, String> user = session.getUserDetails();
//
//        String xmppHostName = domain.get(SessionManager.KEY_HOST_NAME);
//        String xmppHostAddress = domain.get(SessionManager.KEY_HOST_URL);
//        String xmppUserId = user.get(SessionManager.KEY_USERID);
//        String xmppPassword = user.get(SessionManager.KEY_XMPP_SEC_KEY);
    }



    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(context);
        }

        return mRequestQueue;
    }
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
