package com.app.neetbook.serviceRequest;

public class IConstant {
//    public static String BaseUrl = "http://192.168.1.32:1001/";
    public static String BaseUrl = "https://doctor.casperon.co/";

    public static String SOCKET_HOST_URL = BaseUrl+"neetbook";
    public static String walkThrough = BaseUrl+"user/get/walkthrough";
    public static String signUpPhoneVerification = BaseUrl+"user/number/otpgenerate";
    public static String signUpEmailVerification = BaseUrl+"user/email/otpgenerate";
    public static String signUpUrl = BaseUrl+"user/signup";
    public static String linkDevice = BaseUrl+"user/link_device";
    public static String signInPhoneEmailVerification = BaseUrl+"user/signin/otpgenerate";
    public static String SignInUrl = BaseUrl+"user/signin";
    public static String signOutUrl = BaseUrl+"user/signout";
    public static String getCountryList = BaseUrl+"user/address/getcountrylist";
    public static String getCountrySateList = BaseUrl+"user/address/getstatelist";
    public static String getCityList = BaseUrl+"user/address/getcitylist";
    public static String getStateList = BaseUrl+"user/getstatelist";
    public static String getCollegeList = BaseUrl+"user/getcollegelist";
    public static String completeProfile = BaseUrl+"user/completeprofile";
    public static String getUserProfileData = BaseUrl+"user/myprofile";
    public static String updateProfileAddress = BaseUrl+"user/updateaddress";
    public static String changeMobileOtp = BaseUrl+"user/changenumber/otpgenerate";
    public static String changeMobileNumber = BaseUrl+"user/changenumber";
    public static String groupList = BaseUrl+"user/home/grouplist";
    public static String subjectList = BaseUrl+"user/home/subjectlist";
    public static String contactUsQuery = BaseUrl+"contact_us/querylist";
    public static String contactUsSubmit = BaseUrl+"contact_us/submit";
    public static String terms_and_conditions = BaseUrl+"user/page/terms-and-condition";
    public static String about_us = BaseUrl+"user/page/about-us";
    public static String promoList = BaseUrl+"user/home/promolist";
    public static String content_feedbackGetSubjectList = BaseUrl+"content_feedback/getsubjectlist";
    public static String content_feedback = BaseUrl+"content_feedback/submit";
    public static String add_package = BaseUrl+"user/cart/add_package";
    public static String add_packages = BaseUrl+"user/cart/add_packages";
    public static String remove_package = BaseUrl+"user/cart/remove_package";
    public static String cartDetails = BaseUrl+"user/get/cartdetails";
    public static String applyPromo = BaseUrl+"user/cart/applypromo";
    public static String removePromo = BaseUrl+"user/cart/removepromo";
    public static String completePayment = BaseUrl+"user/payment/completed";
    public static String articleIndex = BaseUrl+"user/get/articleindex";
    public static String allPoints = BaseUrl+"user/get/allpoint";
    public static String testIndex = BaseUrl+"user/get/testindex";
    public static String headingIndex = BaseUrl+"user/get/mcq/headingindex";
    public static String examIndex = BaseUrl+"user/get/mcq/examindex";
    public static String yearIndex = BaseUrl+"user/get/mcq/yearindex";
    public static String currentSubscription = BaseUrl+"user/get/currentsubscription";
    public static String oldSubscription = BaseUrl+"user/get/oldsubscription";
    public static String articleContent = BaseUrl+"user/get/article";
    public static String pointsContent = BaseUrl+"user/get/chapterpoint";
    public static String mcqHeading = BaseUrl+"user/get/mcq/heading";
    public static String mcqExam = BaseUrl+"user/get/mcq/exam";
    public static String mcqYear = BaseUrl+"user/get/mcq/year";
    public static String examyearindex = BaseUrl+"user/get/mcq/examyearindex";
    public static String examyear = BaseUrl+"user/get/mcq/examyear";
    public static String testQuestions = BaseUrl+"user/get/testquestions";

    public static String startSession = BaseUrl+"user/start/session";
    public static String updateSession = BaseUrl+"user/update/session";
    public static String homeSearch = BaseUrl+"user/search";
    public static String incomplete = BaseUrl+"user/update/incomplete";
    public static String remove_incomplete = BaseUrl+"user/remove/incomplete";

    public static String getPaymentDetails = "https://rzp_test_IltK5ri6eSjMKq:pxImD9Hai6c8dycNbNfGvTqJ@api.razorpay.com/v1/payments/";

   public static Boolean isForceUpdate=false;

}
