package com.app.neetbook.Utils.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextViewMediumLato extends TextView {
    public CustomTextViewMediumLato(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTextViewMediumLato(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTextViewMediumLato(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/LatoMedium.ttf");
        setTypeface(tf);
    }
}
