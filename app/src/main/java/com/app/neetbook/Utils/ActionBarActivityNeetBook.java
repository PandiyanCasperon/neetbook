package com.app.neetbook.Utils;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.neetbook.Interfaces.FragmentRefreshListener;
import com.app.neetbook.Interfaces.NetworkStateReceiverListener;
import com.app.neetbook.View.EnterPin;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.SocketHandler;

import androidx.appcompat.app.AppCompatActivity;

public class ActionBarActivityNeetBook extends AppCompatActivity implements LogOutTimerUtil.LogOutListener, NetworkStateReceiverListener {


    public static String isInBackground = "No";
    public static String isSessionOut = "No";
    public static String forceLogout = "No";
    public static final String TAG = ActionBarActivityNeetBook.class.getSimpleName();
    private NetworkStateReceiver networkStateReceiver;
    private FragmentRefreshListener fragmentRefreshListener;
    private SessionManager sessionManager;
    Loader loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try{
            isSessionOut = "No";
            super.onCreate(savedInstanceState);
            sessionManager = new SessionManager(this);
            loader = new Loader(this);
        /*if(TabMainActivity.isStarted)
        {
            startTimerThread();
            networkStateReceiver = new NetworkStateReceiver();
            networkStateReceiver.addListener(this);
            this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        }*/

        }catch(Exception e){

        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        sessionManager = new SessionManager(this);
        if(forceLogout.equalsIgnoreCase("Yes"))
        {
            forceLogout = "No";
            sessionManager.logoutUser();
        }else {
            /*if (isInBackground.equalsIgnoreCase("Yes")) {
                isInBackground = "No";
                if (EnterPin.currentView.equalsIgnoreCase("No") && EnterPin.IsShowing.equalsIgnoreCase("true") && !getLocalClassName().equalsIgnoreCase("View.SplashActivity") && !getLocalClassName().equalsIgnoreCase("View.EnterPin") && sessionManager.isLoggedIn()) {
                    LogOutTimerUtil.stopLogoutTimer();
                    Intent intent = new Intent(this, EnterPin.class);
                    intent.putExtra("Background", "Yes");
                    startActivity(intent);
                    overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                }
            }
            LogOutTimerUtil.startLogoutTimer(this, this);*/
        }
    }
    @Override
    protected void onStart() {
        super.onStart();

        LogOutTimerUtil.startLogoutTimer(this, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        isInBackground = "No";
        LogOutTimerUtil.startLogoutTimer(this, this);
        /*if(TabMainActivity.isStarted)
        {
            new AsyncUpdateSessionRunner("1").execute();
        }*/
    }
    @Override
    protected void onStop() {
        super.onStop();
        isInBackground = "No";
        LogOutTimerUtil.startLogoutTimer(this, this);
    }
    @Override
    protected void onDestroy() {

        isInBackground = "No";
        super.onDestroy();
       /* if(TabMainActivity.isStarted)
        {
            networkStateReceiver.removeListener(this);
            this.unregisterReceiver(networkStateReceiver);
            Thread.interrupted();
        }*/
    }
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        isInBackground = "No";
        LogOutTimerUtil.startLogoutTimer(this, this);
    }
    @Override
    public void doLogout() {
        sessionManager = new SessionManager(this);
        try {
            if(sessionManager != null) {
                isSessionOut = "Yes";
               if (new LogOutTimerUtil.ForegroundCheckTask().execute(this).get()) {

                    if (EnterPin.currentView.equalsIgnoreCase("No") && EnterPin.IsShowing.equalsIgnoreCase("true") && !getLocalClassName().equalsIgnoreCase("View.SplashActivity") && !getLocalClassName().equalsIgnoreCase("View.EnterPin") && sessionManager.isLoggedIn()) {
                        LogOutTimerUtil.stopLogoutTimer();
                        Intent intent = new Intent(this, EnterPin.class);
                        intent.putExtra("Background", "Yes");
                        startActivity(intent);
                        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                    }
                    isInBackground = "No";

                } else {

                isInBackground = "yes";
                   BackgroundCheckClass.pinpage_launch=true;
            }
            }
        }
        catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();

        }
    }

    @Override
    public void networkAvailable() {
       /* Log.e("networkAvailable", "true");

        Log.e("session Value",sessionManager.getMilliseconds());
        //TabMainActivity.packageDays = TabMainActivity.startingLevelTime - Long.parseLong(sessionManager.getMilliseconds());
        Log.e("startingLevelTime",""+TabMainActivity.startingLevelTime+" , packageTime : "+TabMainActivity.packageTime);
        sessionManager.updateTimer("0","0");
        if(getFragmentRefreshListener()!=null){
            getFragmentRefreshListener().onRefreshResume();
        }*/
    }

    @Override
    public void networkUnavailable() {
       /* Log.e("networkUnavailable", "false");
        Log.e("startingLevelTime",""+TabMainActivity.startingLevelTime+" , packageTime : "+TabMainActivity.packageTime);
        Log.e("session Value",String.valueOf(TabMainActivity.startingLevelTime-TabMainActivity.packageTime));
        sessionManager.updateTimer(String.valueOf(TabMainActivity.startingLevelTime-TabMainActivity.packageTime),TabMainActivity.strSessionId);

        if(getFragmentRefreshListener()!=null){
            getFragmentRefreshListener().onRefresh();
        }*/
    }



    private void startTimerThread() {
        Thread t = new Thread() {
            @Override
            public void run() {
                while (!isInterrupted()) {
                    try {
                        Thread.sleep(180000);  //1000ms = 1 sec
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new AsyncUpdateSessionRunner("0").execute();

                            }
                        });

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        t.start();
    }
    public FragmentRefreshListener getFragmentRefreshListener() {
        return fragmentRefreshListener;
    }

    public void setFragmentRefreshListener(FragmentRefreshListener fragmentRefreshListener) {
        this.fragmentRefreshListener = fragmentRefreshListener;
    }

    private class AsyncUpdateSessionRunner extends AsyncTask<String, String, String> {
        String result = "";
        String callingType = "";

        public AsyncUpdateSessionRunner(String s) {
            callingType = s;
        }

        @Override
        protected String doInBackground(String... strings) {
            ServiceRequest mRequest = new ServiceRequest(getApplicationContext());
            HashMap<String, String> params = new HashMap<>();
            params.put("session_id", TabMainActivity.strSessionId);

            params.put("time", String.valueOf(TabMainActivity.startingLevelTime-TabMainActivity.packageTime));
            params.put("close",callingType);

            Log.e("startingLevelTime",""+TabMainActivity.startingLevelTime+" , packageTime : "+TabMainActivity.packageTime);
            TabMainActivity.startingLevelTime = TabMainActivity.packageTime;
            mRequest.makeServiceRequest(IConstant.updateSession, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("updateSession"+"-------- Response----------------" + response);
                    String sStatus = "";
                    String message = "";
                    try {
                        JSONObject obj = new JSONObject(response);
                        sStatus = obj.getString("status");
                        message = obj.has("message")?obj.getString("message"):"";
                        result = sStatus;

                        if (sStatus.equalsIgnoreCase("1")) {
                            //JSONObject object = obj.getJSONObject("response");


                        } else if(sStatus.equals("00")){
                            //customAlert.singleLoginAlertLogout();
                        }else if(sStatus.equalsIgnoreCase("01")){
                            //customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                        } else {
                            //customAlert.showAlertOk(getString(R.string.alert_oops),message);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onErrorListener(String errorMessage) {
                    // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
                }
            });
            return result;


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            // new AsyncSubjectsRunner(s).execute();
        }
    }
}
