package com.app.neetbook.Utils.Data;

public class MyProfileData {
    public static String strFName = "";
    public static String strLName = "";
    public static String strAddress1 = "";
    public static String strAddress2 = "";
    public static String strcountry = "";
    public static String strCountryId = "";
    public static String strState = "";
    public static String strStateId= "";
    public static String strCity = "";
    public static String strCityId = "";
    public static String strPost = "";
    public static String strCurrentSubs = "open";
    public static String strOldSubs = "close";
    public static String strWallet = "close";
    public static int currentView = 1;


    public static void clearData()
    {
        strFName = "";
        strLName = "";
        strAddress1 = "";
        strAddress2 = "";
        strcountry = "";
        strCountryId = "";
        strState = "";
        strStateId = "";
        strCity = "";
        strCityId = "";
        strPost = "";
        strCurrentSubs = "open";
        strOldSubs = "close";
        strWallet = "close";
        currentView = 1;
    }

}
