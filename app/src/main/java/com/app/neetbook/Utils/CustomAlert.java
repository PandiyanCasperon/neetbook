package com.app.neetbook.Utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.neetbook.Model.TabSideMenu;
import com.app.neetbook.R;
import com.app.neetbook.Utils.Data.ChangeMobileData;
import com.app.neetbook.Utils.Data.ChangePinData;
import com.app.neetbook.Utils.Data.CompleteProfileData;
import com.app.neetbook.Utils.Data.ContactUsData;
import com.app.neetbook.Utils.Data.DrawerActivityData;
import com.app.neetbook.Utils.Data.LoginData;
import com.app.neetbook.Utils.Data.MyProfileData;
import com.app.neetbook.View.DrawerContentSlideActivity;
import com.app.neetbook.View.EditProfileActivity;
import com.app.neetbook.View.MobilePagesActivity;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.squareup.picasso.Picasso;

import java.nio.Buffer;

public class CustomAlert {
    Context activity;
    private static Dialog alertDialog;
    SessionManager sessionManager;
    public  CustomAlert(Context activity)
    {
        this.activity = activity;
        sessionManager = new SessionManager(this.activity);
        alertDialog = new Dialog(activity);
    }

    public void showAlertLogout(String title, String desc)
    {

        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(activity.getString(R.string.alert));
        textViewDesc.setText(desc);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnOk.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 alertDialog.dismiss();
                 CompleteProfileData.clearData();
                 MyProfileData.clearData();
                 LoginData.clearData();
                 ContactUsData.clearData();
                 ChangeMobileData.cleatData();
                 ChangePinData.clearData();
                 DrawerActivityData.ClearDrawerData();
                 new SessionManager(activity).logoutUser();

             }
         });
        btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
        if(!alertDialog.isShowing())
            alertDialog.show();
    }
    public void UserInActiveAlertLogout(String title,String msg)
    {
        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(title);
        textViewDesc.setText(msg);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                CompleteProfileData.clearData();
                MyProfileData.clearData();
                LoginData.clearData();
                ContactUsData.clearData();
                ChangeMobileData.cleatData();
                ChangePinData.clearData();
                DrawerActivityData.ClearDrawerData();
                new SessionManager(activity).logoutUser();

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        if(!alertDialog.isShowing())
            alertDialog.show();
    }
    public void singleLoginAlertLogout()
    {
        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(activity.getString(R.string.Logout));
        textViewDesc.setText(activity.getString(R.string.newSIgnInAlert));
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 alertDialog.dismiss();
                 CompleteProfileData.clearData();
                 MyProfileData.clearData();
                 LoginData.clearData();
                 ContactUsData.clearData();
                 ChangeMobileData.cleatData();
                 ChangePinData.clearData();
                 DrawerActivityData.ClearDrawerData();
                 new SessionManager(activity).logoutUser();

             }
         });
        btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
        if(!alertDialog.isShowing())
            alertDialog.show();
    }

    public void showAlert(String title, String desc)
    {

        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(title);
        textViewDesc.setText(desc);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnOk.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 alertDialog.dismiss();
             }
         });
        btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
        if(!alertDialog.isShowing())
            alertDialog.show();
    }
    public void showAlertOk(String title, String desc)
    {

        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(title);
        textViewDesc.setText(desc);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 alertDialog.dismiss();
             }
         });
        if(!alertDialog.isShowing())
            alertDialog.show();
    }

    public void showAlertTimeOver(String title, String desc)
    {

        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(title);
        textViewDesc.setText(desc);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 alertDialog.dismiss();
                 TabMainActivity.tabMainActivity.finish();
             }
         });
        if(!alertDialog.isShowing())
            alertDialog.show();
    }

    public void showAlertSuccess(String title, String desc)
    {

        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        ImageView imgAlert = alertDialog.findViewById(R.id.imgAlert);
        Picasso.with(activity).load(R.drawable.right_mark).into(imgAlert);
        textViewTitle.setText(title);
        textViewDesc.setText(desc);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 alertDialog.dismiss();
             }
         });
        if(!alertDialog.isShowing())
            alertDialog.show();
    }
    public void showAlertSuccessCompleteProfile(String title, String desc, final String from)
    {


        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        ImageView imgAlert = alertDialog.findViewById(R.id.imgAlert);
        Picasso.with(activity).load(R.drawable.right_mark).into(imgAlert);
        textViewTitle.setText(title);
        textViewDesc.setText(desc);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 alertDialog.dismiss();
                 TabSideMenu tabSideMenu2 = new TabSideMenu();
                 tabSideMenu2.setId("2");
                 tabSideMenu2.setName((sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) == null || sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("nill"))?activity.getString(R.string.complete_profile): sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("")?activity.getString(R.string.complete_profile):activity.getString(R.string.my_profile));
                 tabSideMenu2.setImage_id_inactive(R.drawable.complete_proile_inactive);
                 tabSideMenu2.setImage_id_acative(R.drawable.complete_profile_active);

                 Bundle bundle = new Bundle();
                 bundle.putSerializable("sideMenu",tabSideMenu2);
                 if(activity.getResources().getBoolean(R.bool.isTablet)) {

                     if(from.equals("editProfile")) {
                         EditProfileActivity.editProfileActivity.finish();
                         DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                         DrawerActivityData.lastSelectedMenuId = "2";
                         DrawerActivityData.currentIndex = 1;
                         Bundle bundle1 = new Bundle();
                         TabSideMenu tabSideMenu = new TabSideMenu();
                         tabSideMenu.setId("2");
                         tabSideMenu.setImage_id_acative(R.drawable.complete_proile_inactive);
                         tabSideMenu.setImage_id_inactive(R.drawable.complete_profile_active);
                         tabSideMenu.setName(activity.getString(R.string.my_profile));

                         bundle1.putSerializable("sideMenu", tabSideMenu);
                         DrawerActivityData.bundle = bundle;
                         DrawerActivityData.isFirstTimeLoading = true;

                         activity.startActivity(new Intent(activity, DrawerContentSlideActivity.class).putExtra("sideMenu", tabSideMenu2));

                     }
                     else {
                         DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                         DrawerActivityData.lastSelectedMenuId = "2";
                         DrawerActivityData.currentIndex = 1;
                         Bundle bundle1 = new Bundle();
                         TabSideMenu tabSideMenu = new TabSideMenu();
                         tabSideMenu.setId("2");
                         tabSideMenu.setImage_id_acative(R.drawable.complete_proile_inactive);
                         tabSideMenu.setImage_id_inactive(R.drawable.complete_profile_active);
                         tabSideMenu.setName(activity.getString(R.string.my_profile));
                         bundle1.putSerializable("sideMenu", tabSideMenu);
                         DrawerActivityData.bundle = bundle;
                         activity.startActivity(new Intent(activity, DrawerContentSlideActivity.class).putExtra("sideMenu",tabSideMenu2));
                     }

                 }else
                 {
                     //activity.startActivity(new Intent(activity, MobilePagesActivity.class).putExtra("sideMenu",tabSideMenu2));
                     if(from.equals("editProfile")) {
                         EditProfileActivity.editProfileActivity.finish();
                         DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                         DrawerActivityData.lastSelectedMenuId = "2";
                         DrawerActivityData.currentIndex = 1;
                         Bundle bundle1 = new Bundle();
                         TabSideMenu tabSideMenu = new TabSideMenu();
                         tabSideMenu.setId("2");
                         tabSideMenu.setImage_id_acative(R.drawable.complete_proile_inactive);
                         tabSideMenu.setImage_id_inactive(R.drawable.home_inactive);
                         tabSideMenu.setName(activity.getString(R.string.home));

                         bundle1.putSerializable("sideMenu", tabSideMenu);
                         DrawerActivityData.bundle = bundle;
                         DrawerActivityData.isFirstTimeLoading = true;
                         activity.startActivity(new Intent(activity, DrawerContentSlideActivity.class).putExtra("sideMenu", tabSideMenu2));
                     }
                     else {
                         DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                         DrawerActivityData.lastSelectedMenuId = "2";
                         DrawerActivityData.isFirstTimeLoading = true;
                         Bundle bundle1 = new Bundle();
                         TabSideMenu tabSideMenu = new TabSideMenu();
                         tabSideMenu.setId("2");
                         tabSideMenu.setImage_id_acative(R.drawable.complete_proile_inactive);
                         tabSideMenu.setImage_id_inactive(R.drawable.home_inactive);
                         tabSideMenu.setName(activity.getString(R.string.home));

                         bundle1.putSerializable("sideMenu", tabSideMenu);
                         DrawerActivityData.bundle = bundle;
                         DrawerActivityData.isFirstTimeLoading = true;
                         activity.startActivity(new Intent(activity, DrawerContentSlideActivity.class).putExtra("sideMenu",tabSideMenu2));
                     }
                 }
             }
         });
        if(!alertDialog.isShowing())
            alertDialog.show();
    }
    public void showFontSize()
    {
        final SessionManager sessionManager = new SessionManager(activity);
        alertDialog.setContentView(R.layout.select_font_size);
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        LinearLayout llOne = alertDialog.findViewById(R.id.llOne);
        ImageView close = alertDialog.findViewById(R.id.close);
        final ImageView imgSmall = alertDialog.findViewById(R.id.imgSmall);
        LinearLayout llTwo = alertDialog.findViewById(R.id.llTwo);
        final ImageView imgMedium = alertDialog.findViewById(R.id.imgMedium);
        LinearLayout llThree = alertDialog.findViewById(R.id.llThree);
        final ImageView imgLarge = alertDialog.findViewById(R.id.imgLarge);
        imgSmall.setVisibility(sessionManager.getFontSize().equalsIgnoreCase("small")?View.VISIBLE:View.GONE);
        imgMedium.setVisibility(sessionManager.getFontSize().equalsIgnoreCase("medium")?View.VISIBLE:View.GONE);
        imgLarge.setVisibility(sessionManager.getFontSize().equalsIgnoreCase("large")?View.VISIBLE:View.GONE);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        llOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.setFontSize("small");
                imgSmall.setVisibility(View.VISIBLE);
                imgMedium.setVisibility(View.GONE);
                imgLarge.setVisibility(View.GONE);
                alertDialog.dismiss();
            }
        });
        llTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.setFontSize("medium");
                imgSmall.setVisibility(View.GONE);
                imgMedium.setVisibility(View.VISIBLE);
                imgLarge.setVisibility(View.GONE);
                alertDialog.dismiss();
            }
        });
        llThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.setFontSize("large");
                imgSmall.setVisibility(View.GONE);
                imgMedium.setVisibility(View.GONE);
                imgLarge.setVisibility(View.VISIBLE);
                alertDialog.dismiss();
            }
        });
        if(!alertDialog.isShowing())
            alertDialog.show();
    }
}
