package com.app.neetbook.Utils.Data;

public class CompleteProfileData {
    public static String firstName = "";
    public static String lastName = "";
    public static String address1 = "";
    public static String address2 = "";
    public static String country = "COUNTRY";
    public static String countryId = "";
    public static String state1 = "STATE";
    public static String state1Id = "";
    public static String city = "CITY";
    public static String cityId = "";
    public static String state2 = "STATE";
    public static String state2Id = "";
    public static String college = "COLLEGE";
    public static String pinCode = "";


    public static void clearData()
    {
         firstName = "";
         lastName = "";
         address1 = "";
         address2 = "";
         country = "COUNTRY";
        countryId = "";
         state1 = "STATE";
         state1Id = "";
         city = "CITY";
        cityId = "";
         state2 = "STATE";
         state2Id = "";
         college = "COLLEGE";
         pinCode = "";
    }
}
