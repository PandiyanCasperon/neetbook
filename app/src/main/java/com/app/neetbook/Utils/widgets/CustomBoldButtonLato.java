package com.app.neetbook.Utils.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class CustomBoldButtonLato extends Button {
    public CustomBoldButtonLato(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomBoldButtonLato(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomBoldButtonLato(Context context) {
        super(context);
        init();
    }

    @SuppressLint("WrongConstant")
    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/LatoBold.otf");
        setTypeface(tf, 1);
    }
}
