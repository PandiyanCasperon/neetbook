package com.app.neetbook.Utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.MailTo;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class AppWebViewClients extends WebViewClient {
    Loader loader;
    Activity activity;
    Context context;

    public AppWebViewClients(Loader loader, Context context) {
        this.loader=loader;
        this.context=context;
        this.loader.showLoader();
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        // TODO Auto-generated method stub
        super.onPageFinished(view, url);
        loader.dismissLoader();
    }


    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.startsWith("mailto:")) {

            Intent intent = new Intent(Intent.ACTION_SENDTO);

            // For only email app handle this intent
            intent.setData(Uri.parse("mailto:"));
            String to = url.split("[:?]")[1];
            if(!TextUtils.isEmpty(to)){
                String[] toArray = to.split(";");
                // Put the primary email addresses array into intent
                intent.putExtra(Intent.EXTRA_EMAIL,toArray);

            }

//            MailTo mailTo = MailTo.parse(url);
//            String emailAddress = mailTo.getTo();
//            String subject = mailTo.getSubject();
//            String body = mailTo.getBody();
//            String cc = mailTo.getCc();
//            Intent mail = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//            mail.setType("application/octet-stream");
//            mail.putExtra(Intent.EXTRA_EMAIL, new String[]{emailAddress});
//            mail.putExtra(Intent.EXTRA_SUBJECT, subject);
//            mail.putExtra(Intent.EXTRA_TEXT, body);
//            mail.putExtra(Intent.EXTRA_CC, cc);
            context.startActivity(intent);
            return true;


        }
        return false;
    }

}
