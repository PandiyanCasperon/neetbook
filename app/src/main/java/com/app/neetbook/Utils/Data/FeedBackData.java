package com.app.neetbook.Utils.Data;

import java.io.File;

public class FeedBackData {

    public static String strSubjects = "";
    public static String strSubjectsId = "";
    public static String strTopics = "";
    public static String strTopicsId = "";
    public static String strSelectedFile = "";
    public static String strDesc = "";
    public static File file = null;
    public static byte[] byteArray = null;


    public static void clearData()
    {
        strSubjects = "";
        strTopics = "";
        strSelectedFile = "";
        strDesc = "";
        strSubjectsId = "";
        strTopicsId = "";
        file = null;
        byteArray = null;
    }

}
