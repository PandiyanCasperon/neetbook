package com.app.neetbook.Utils.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class CustomBoldButton extends Button {
    public CustomBoldButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomBoldButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomBoldButton(Context context) {
        super(context);
        init();
    }

    @SuppressLint("WrongConstant")
    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/ProximaNovaBold.otf");
        setTypeface(tf, 1);
    }
}
