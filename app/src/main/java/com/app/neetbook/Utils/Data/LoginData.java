package com.app.neetbook.Utils.Data;

public class LoginData {

    // SignUp
    public static String otp="";
    public static String mobile_otp="";
    public static String SignUpField1 = "";
    public static String SignUpField2 = "";
    public static String SignUpField3 = "";
    public static String SignUpField4 = "";
    public static String SignUpField5 = "";
    public static boolean isChecked = false;
    public static boolean SignUpTermsLink = false;
    public static  String SignUpResendVisible = "No";

    //SignIn
    public static String SignIn_otp="";
    public static String SignInUserName="";
    public static String SignInType="";
    public static String SignInField1 = "";
    public static String SignInField2 = "";
    public static String SignInField3 = "";
    public static  String currentView = "SignIn";
    public static  String SignInResendVisible = "No";
    public static long countDown = 45000;
    public static long countDownTimer = countDown;


    public static String strUserId = "";
    public static String mail = "";
    public static String mailotp = "";
    public static String mobile = "";
    public static String mobileotp = "";
    public static String pin = "";
    public static String pinSignUp = "";
    public static void clearData()
    {
         otp="";
         mobile_otp="";
         SignUpField1 = "";
         SignUpField2 = "";
         SignUpField3 = "";
         SignUpField4 = "";
         SignUpField5 = "";
        isChecked = false;
        SignUpTermsLink = false;
        //SignIn
         SignIn_otp="";
         SignInUserName="";
         SignInType="";
        SignInField2="";
        SignInField3="";
        mail="";
        mailotp="";
        mobile="";
        mobileotp="";
        pin="";
        pinSignUp="";
       currentView = "SignIn";
        SignUpResendVisible = "No";
        SignInResendVisible = "No";
        countDownTimer = countDown;
    }

}
