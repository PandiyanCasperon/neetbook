package com.app.neetbook.Utils.alerter

import android.content.Context

interface RoomInterface {
    val context: Context

    fun shortToast(message: String) {

    }

    fun longToast(message: String) {

    }
}