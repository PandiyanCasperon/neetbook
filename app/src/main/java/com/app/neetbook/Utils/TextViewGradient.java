package com.app.neetbook.Utils;

import android.content.Context;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.widget.TextView;

import com.app.neetbook.R;

public class TextViewGradient {

    public static void setTextGradient(Context c,TextView textView)
    {
        Shader shader = new LinearGradient(0,0,textView.getWidth(),textView.getHeight(),
                c.getResources().getColor(R.color.primary_meroon), c.getResources().getColor(R.color.mcq_blue), Shader.TileMode.CLAMP);
        textView.getPaint().setShader(shader);

    }

}
