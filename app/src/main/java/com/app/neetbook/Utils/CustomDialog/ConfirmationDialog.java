package com.app.neetbook.Utils.CustomDialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.neetbook.R;

public class ConfirmationDialog {

    private Context mContext;
    private View view;
    private Dialog dialog;

    private ImageView close_dialog_image;

    public ConfirmationDialog(Context context){

        mContext=context;
    }

    public void CustomDialoginitialize(){

        DisplayMetrics metrics=mContext.getResources().getDisplayMetrics();
        int screen_width= (int) (metrics.widthPixels *0.90);
        view= View.inflate(mContext, R.layout.confirmation_dialog_layout, null);
        dialog= new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().gravity = Gravity.LEFT|Gravity.CENTER_VERTICAL;
        dialog.getWindow().setLayout(screen_width, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        close_dialog_image=(ImageView)dialog.findViewById(R.id.close_dialog_image);

        close_dialog_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

    }

    public void dialogshow(){

        dialog.show();
    }

    public void dialogdismiss(){

        dialog.dismiss();
    }
}
