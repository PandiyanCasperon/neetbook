package com.app.neetbook.Utils.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomRegularTextview extends TextView {

    public CustomRegularTextview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    public CustomRegularTextview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public CustomRegularTextview(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/ProximaNovaRegular.otf");
        setTypeface(tf);
    }
}
