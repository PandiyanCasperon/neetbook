package com.app.neetbook.Utils.Data;

import android.os.Bundle;

public class DrawerActivityData {
    public static String lastSelectedMenuId = "1";
    public static Bundle bundle;
    public static int currentIndex = 1;
    public static int before_logout_pressed = 1;
    public static boolean isFirstTimeLoading = true;

    public static void ClearDrawerData()
    {
        lastSelectedMenuId  = "1";
        bundle = null;
        currentIndex = 1;
        before_logout_pressed = 1;
        isFirstTimeLoading = true;
    }
}
