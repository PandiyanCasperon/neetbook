package com.app.neetbook.Utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;


import com.app.neetbook.R;
import com.wang.avi.AVLoadingIndicatorView;

public class Loader {
    Context activity;
    Dialog progressDialog;
    AVLoadingIndicatorView avi;



    public  Loader(Context activity)
    {
        this.activity = activity;
    }


    public void showLoader()
    {
//        progressDialog = new Dialog(activity,android.R.style.Theme_Light_NoTitleBar_Fullscreen);//full screen
        progressDialog = new Dialog(activity);
        progressDialog.setContentView(R.layout.custom_progress);






    //    if(progressDialog.getWindow() != null)
       //     progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
       // startAnimation();
        progressDialog.setCancelable(false);

      //  if(!progressDialog.isShowing())
         //   progressDialog.show();
    }

    public void dismissLoader()
    {
        try {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }


}
