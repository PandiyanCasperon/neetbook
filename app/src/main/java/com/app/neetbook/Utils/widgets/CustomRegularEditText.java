package com.app.neetbook.Utils.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class CustomRegularEditText extends EditText {

    public CustomRegularEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    public CustomRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public CustomRegularEditText(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/ProximaNovaRegular.otf");
        setTypeface(tf);
    }
}
