package com.app.neetbook.Utils.alerter;

import android.app.Activity;
import android.content.Context;

import androidx.fragment.app.FragmentTransaction;

import com.app.neetbook.R;
import com.app.neetbook.View.Fragments.NoInternetAvailFragment;

public class AlertBox {

    public static void showSnackBox(Activity activity,String title, String content)
    {
        Alerter.create(activity)
                .setText(content)
                .setTitle(title)
                .setDismissable(true)
                .setDuration(2000)
                .enableSwipeToDismiss().disableOutsideTouch()
                .setIcon(R.drawable.alert_blue)
                .setIconColorFilter(0) // Optional - Removes white tint
                .setIconSize(R.dimen.alerter_alert_icn_size) // Optional - default is 38dp
                .show();
    }
    public static void showSnackBoxPrimaryBa(Activity activity,String title, String content)
    {
        Alerter.create(activity)
                .setText(content)
                .setTitle(title)
                .setTitleColor(activity.getResources().getColor(R.color.white))
                .setTextColor(activity.getResources().getColor(R.color.white))
                .setDismissable(true)
                .setDuration(2000)
                .setBackgroundColorInt(activity.getResources().getColor(R.color.primary_meroon))
                .setBackgroundColorRes(R.color.primary_meroon)
                .enableSwipeToDismiss().disableOutsideTouch()
                .setIcon(R.drawable.alert_blue)
                .setIconColorFilter(0) // Optional - Removes white tint
                .setIconSize(R.dimen.alerter_alert_icn_size) // Optional - default is 38dp
                .show();
    }

    public static void ShowNoInternetPage(Context activity, String title, String content)
    {

    }

}
