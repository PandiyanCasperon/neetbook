package com.app.neetbook.Utils.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by user129 on 5/7/2018.
 */

public class CustomEdittextBold extends EditText {
    public CustomEdittextBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomEdittextBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomEdittextBold(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/ProximaNovaBold.otf");
            setTypeface(tf);
        }
    }
}
