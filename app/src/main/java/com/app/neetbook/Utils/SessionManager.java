package com.app.neetbook.Utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.Request;
import com.app.neetbook.CartRoom.CartDataBase;
import com.app.neetbook.View.DrawerContentSlideActivity;
import com.app.neetbook.View.EnterPin;
import com.app.neetbook.View.MobilePagesActivity;
import com.app.neetbook.View.SignInSighUp;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.app.neetbook.socket.SocketManager;
import com.razorpay.Checkout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by R.Navaneethakrishnan on 10/1/2019.
 */

public class SessionManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    int PRIVATE_MODE = 0;

    public static final String PREF_NAME = "NeetBook_Pref";

    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String IS_IntroCompleted = "IsIntro";

    public static final String KEY_FNAME = "firstname";
    public static final String KEY_LNAME = "lastname";

    public static final String KEY_EMAIL = "email";
    public static final String KEY_MOBILE = "mobile";
    public static final String KEY_UID = "user_id";
    public static final String KEY_ID = "_id";
    public static final String KEY_TIME = "time";
    public static final String KEY_PIN = "pin";
    public static final String START_TIME = "starttime";
    public static final String KEY_THUMPNILE = "profile_img";
    public static final String KEY_JWT = "jwt_token";
    public static final String KEY_CURRENCY = "currency_id";

    /*Login View*/
    public static final String KEY_LOGIN_VIEW = "login_view";
    public static final String KEY_LOGIN_EMAIL_MOBILE = "signIn_email_mobile";
    public static final String KEY_LOGIN_OTP = "signIn_otp";
    public static final String KEY_SIGN_UP_EMAIL = "signUp_email";
    public static final String KEY_SIGN_UP_MOBILE = "signUp_mobile";
    public static final String KEY_SIGN_UP_OTP = "signUp_otp";
    public static final String KEY_SIGN_UP_MOBILE_OTP = "signUpMobile_otp";
    public static final String KEY_SIGN_IN_UP_PIN = "signIn_Up_pin";
    public static final String KEY_MILLISECONDS = "milliSeconds";
    public static final String KEY_PACKAGE_ID = "subsPackageId";

    public static final String HEAD_ID = "head_id";
    public static final String BOOKMARK_TITLE = "bookmark_title";
    public static final String SUBJECT_ID = "subject_id";
    public static final String LONG_NAME = "long_name";
    public static final String HEAD_LIST = "head_list";
    public static final String BOOKMARK_CLICKED = "bookmark_clicked";
    public static final String CHAPTER_ID = "ChapterId";
    public static final String SUBJECT_NAME1 = "Subject_name";
    public static final String CHAPTER_NAME1 = "ChapterName";
    public static final String PENDING = "Pending";
    public static final String PENDINGTESTID = "PendingTestid";
    public static final String TESTID = "TestID";
    public static final String TESTTYPE = "testtype";
    public static final String NO_OF_QUESTION = "no_of_question";
    public static final String IS_FINISHED = "is_finished";
    public static final String QUESTIONTYPE = "questiontype";
    public static final String STRINGSTRUCTION = "stringstruction";
    public static final String STRLONGNAME = "strlongname";
    public static final String CRT_ANS_MARK = "crt_ans_mark";
    public static final String WRNG_ANS_MARK = "wrng_ans_mark";
    public static final String CHAPTER_NAME = "ChapterName";
    public static final String CHAPTER_SUPERHEADING = "superHeading";
    public static final String FONT_SIZE = "font_size";
    public static final String POINT_ID = "point_id";
    public static final String LOADING_TYPE = "loading_type";
    public static final String TEST_TYPE = "test_type";
    public static final String TEST_ID = "test_id";
    public static final String TEST_INSTRUCTION = "instruction_t";
    public static final String TEST_LONG_NAME = "test_long_name";
    public static final String TEST_NUM_OF_Q = "num_of_q";
    public static final String TEST_IS_FINISHED = "test)is_finished";
    public static final String TEST_Q_TYPE = "q_type";
    public static final String TEST_C_A__MARK = "correctAnswer";
    public static final String TEST_W_A__MARK = "wrongAnswer";
    private Loader loader;
    public static final String NIGHT_MODE = "NIGHT_MODE";


    public static final String READ_SUBJECT_NAME = "read_subject_name";
    public static final String READ_SUBJECT_SHORT_NAME = "read_subject_short_name";
    public static final String READ_TRIAL_STATUS = "read_trial_status";

    public static final String LAST_READ_SUBJECT_NAME = "last_read_subject_name";
    public static final String LAST_READ_SUBJECT_SHORT_NAME = "last_read_subject_short_name";
    public static final String LAST_READ_TRIAL_STATUS = "last_read_trial_status";
    public static String SearchCategoryTitle = "";
    private String LastReadTestId = "LastReadTestId";
    public static final String POS = "poss";
    public  String mcqFindCategoryList = "";
    public  String findCategoryList = "";


    public SessionManager(Context context) {
        try {

            this._context = context;
            pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
            editor = pref.edit();
            loader = new Loader(context);

        } catch (Exception e) {

        }

    }

    public void setLastReadDetails( String listTitle) {
        editor.putString(findCategoryList, listTitle);
        editor.commit();
    }
    public void setMcqLastReadDetails( String listTitle) {
        editor.putString(mcqFindCategoryList, listTitle);
        editor.commit();
    }
    public String getMcqLastReadDetails() {
        return   pref.getString(mcqFindCategoryList, "");
    }

    public String getFindCategoryList() {
        return   pref.getString(findCategoryList, "");
    }



    public void createLoginSession(String user_id, String id, String phone, String email, String jwt_token, String pin, String f_name, String l_name, String time) {
        editor.putBoolean(IS_LOGIN, true);


        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_MOBILE, phone);
        editor.putString(KEY_UID, user_id);
        editor.putString(KEY_ID, id);
        editor.putString(KEY_JWT, jwt_token);
        editor.putString(KEY_PIN, pin);
        editor.putString(KEY_FNAME, f_name);
        editor.putString(KEY_LNAME, l_name);
        editor.putString(KEY_TIME, time);
        editor.putString(KEY_MILLISECONDS, "");
        editor.putString(KEY_PACKAGE_ID, "");
        editor.putString(FONT_SIZE, "small");

        editor.commit();
    }

    public void updateName(String f_name, String l_name) {
        editor.putString(KEY_FNAME, f_name);
        editor.putString(KEY_LNAME, l_name);

        editor.commit();
    }

    public String getUserId(){
        return  pref.getString(KEY_UID, null);
    }

    public void setNightMode(boolean isNightModeEnabled) {
        editor.putBoolean(NIGHT_MODE, isNightModeEnabled);

        editor.commit();
    }

    public boolean isNightModeEnabled() {
        return pref.getBoolean(NIGHT_MODE, false);
    }

    public void setLastReadTestId(String testId) {
        editor.putString(LastReadTestId, testId);
        editor.commit();
    }

    public String getLastReadTestId() {
        return pref.getString(LastReadTestId, "");
    }

    public void setloginview(String login_view, String login_email_mobile, String login_otp, String signUp_email, String signUp_mobile, String signUp_otp, String signIn_Up_Pin, String sign_Up_Mobile_OTP) {


        editor.putString(KEY_LOGIN_VIEW, login_view);

        editor.putString(KEY_LOGIN_EMAIL_MOBILE, login_email_mobile);
        editor.putString(KEY_LOGIN_OTP, login_otp);
        editor.putString(KEY_SIGN_UP_EMAIL, signUp_email);
        editor.putString(KEY_SIGN_UP_MOBILE, signUp_mobile);
        editor.putString(KEY_SIGN_UP_OTP, signUp_otp);
        editor.putString(KEY_SIGN_IN_UP_PIN, signIn_Up_Pin);
        editor.putString(KEY_SIGN_UP_MOBILE_OTP, sign_Up_Mobile_OTP);

        editor.commit();
    }

    public HashMap<String, String> getSignInUpPageDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_LOGIN_VIEW, pref.getString(KEY_LOGIN_VIEW, null));
        user.put(KEY_LOGIN_EMAIL_MOBILE, pref.getString(KEY_LOGIN_EMAIL_MOBILE, null));
        user.put(KEY_LOGIN_OTP, pref.getString(KEY_LOGIN_OTP, null));
        user.put(KEY_SIGN_UP_EMAIL, pref.getString(KEY_SIGN_UP_EMAIL, null));
        user.put(KEY_SIGN_UP_MOBILE, pref.getString(KEY_SIGN_UP_MOBILE, null));
        user.put(KEY_SIGN_UP_OTP, pref.getString(KEY_SIGN_UP_OTP, null));
        user.put(KEY_SIGN_IN_UP_PIN, pref.getString(KEY_SIGN_IN_UP_PIN, null));
        user.put(KEY_SIGN_UP_MOBILE_OTP, pref.getString(KEY_SIGN_UP_MOBILE_OTP, null));


        return user;
    }

    public void updateToken(String strToken) {

        editor.putString(KEY_JWT, strToken);


        editor.commit();
    }

    public void updatePin(String pin) {

        editor.putString(KEY_PIN, pin);


        editor.commit();
    }
   public void setStarttime(String starttime) {

        editor.putString(START_TIME, starttime);


        editor.commit();
    }

    public void setViewToBookMark() {

        editor.putString(BOOKMARK_CLICKED, "Yes");


        editor.commit();
    }

    public String getViewToBookMark() {

        return pref.getString(BOOKMARK_CLICKED, null);
    }

    public String getFontSize() {

        return pref.getString(FONT_SIZE, null);
    }

    public void closeViewToBookMark() {

        editor.putString(BOOKMARK_CLICKED, "No");
        editor.commit();
    }

    public void setFontSize(String size) {

        editor.putString(FONT_SIZE, size);


        editor.commit();
    }

    public void ReadSubjectBookmark(String subject_name, String subject_short_name, String subject_trial_status) {

        editor.putString(READ_SUBJECT_NAME, subject_name);
        editor.putString(READ_SUBJECT_SHORT_NAME, subject_short_name);
        editor.putString(READ_TRIAL_STATUS, subject_trial_status);

        editor.commit();
    }


    public void IncompleteTest(String subject_id, String chapter_id, String chapter_name, String subject_name, String pending,
                               String pendingTestId, String testId, String type, String no_of_question, String isFinished, String questionType, String strInstruction, String strLongName, String crt_ans_mark, String wrng_ans_mark) {
        editor.putString(SUBJECT_ID, subject_id);
        editor.putString(CHAPTER_ID, chapter_id);
        editor.putString(CHAPTER_NAME1, chapter_name);
        editor.putString(SUBJECT_NAME1, subject_name);
        editor.putString(PENDING, pending);
        editor.putString(PENDINGTESTID, pendingTestId);
        editor.putString(TESTID, testId);
        editor.putString(TESTTYPE, type);
        editor.putString(NO_OF_QUESTION, no_of_question);
        editor.putString(IS_FINISHED, isFinished);
        editor.putString(QUESTIONTYPE, questionType);
        editor.putString(STRINGSTRUCTION, strInstruction);
        editor.putString(STRLONGNAME, strLongName);
        editor.putString(CRT_ANS_MARK, crt_ans_mark);
        editor.putString(WRNG_ANS_MARK, wrng_ans_mark);
        editor.commit();
    }


    public HashMap<String, String> getIncompleteTest() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(SUBJECT_ID, pref.getString(SUBJECT_ID, null));
        user.put(CHAPTER_ID, pref.getString(CHAPTER_ID, null));
        user.put(CHAPTER_NAME1, pref.getString(CHAPTER_NAME1, null));
        user.put(SUBJECT_NAME1, pref.getString(SUBJECT_NAME1, null));
        user.put(PENDING, pref.getString(PENDING, null));
        user.put(PENDINGTESTID, pref.getString(PENDINGTESTID, null));
        user.put(TESTID, pref.getString(TESTID, null));
        user.put(TESTTYPE, pref.getString(TESTTYPE, null));
        user.put(NO_OF_QUESTION, pref.getString(NO_OF_QUESTION, null));
        user.put(IS_FINISHED, pref.getString(IS_FINISHED, null));
        user.put(QUESTIONTYPE, pref.getString(QUESTIONTYPE, null));
        user.put(STRINGSTRUCTION, pref.getString(STRINGSTRUCTION, null));
        user.put(STRLONGNAME, pref.getString(STRLONGNAME, null));
        user.put(CRT_ANS_MARK, pref.getString(CRT_ANS_MARK, null));
        user.put(WRNG_ANS_MARK, pref.getString(WRNG_ANS_MARK, null));
        return user;
    }


    public void LastReadSubjectBookmark(String subject_name, String subject_short_name, String subject_trial_status) {

        editor.putString(LAST_READ_SUBJECT_NAME, subject_name);
        editor.putString(LAST_READ_SUBJECT_SHORT_NAME, subject_short_name);
        editor.putString(LAST_READ_TRIAL_STATUS, subject_trial_status);

        editor.commit();
    }

    public HashMap<String, String> getReadBookmarkDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(READ_SUBJECT_NAME, pref.getString(READ_SUBJECT_NAME, null));
        user.put(READ_SUBJECT_SHORT_NAME, pref.getString(READ_SUBJECT_SHORT_NAME, null));
        user.put(READ_TRIAL_STATUS, pref.getString(READ_TRIAL_STATUS, null));
        return user;
    }

    public HashMap<String, String> getLastReadBookmarkDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(LAST_READ_SUBJECT_NAME, pref.getString(LAST_READ_SUBJECT_NAME, null));
        user.put(LAST_READ_SUBJECT_SHORT_NAME, pref.getString(LAST_READ_SUBJECT_SHORT_NAME, null));
        user.put(LAST_READ_TRIAL_STATUS, pref.getString(LAST_READ_TRIAL_STATUS, null));
        return user;
    }

    public void updateBookMark(String title, String headID, String SubjectId, String longName, String headList, String strChapterId, String strChapterName, String strSuperHeading, String strPointId, String strLoadingType) {
        editor.putString(HEAD_ID, headID);
        editor.putString(BOOKMARK_TITLE, title);
        editor.putString(SUBJECT_ID, SubjectId);
        editor.putString(LONG_NAME, longName);
        editor.putString(HEAD_LIST, headList);
        editor.putString(CHAPTER_ID, strChapterId);
        editor.putString(CHAPTER_NAME, strChapterName);
        editor.putString(CHAPTER_SUPERHEADING, strSuperHeading);
        editor.putString(POINT_ID, strPointId);
        editor.putString(LOADING_TYPE, strLoadingType);
        editor.commit();
    }

    public void updateBookMarkTest(String testId, String type, String no_of_question, String isFinished, String questionType, String strInstruction, String strLongName, String crt_ans_mark, String wrng_ans_mark) {

        editor.putString(TEST_ID, testId);
        editor.putString(TEST_TYPE, type);
        editor.putString(TEST_NUM_OF_Q, no_of_question);
        editor.putString(TEST_IS_FINISHED, isFinished);
        editor.putString(TEST_Q_TYPE, questionType);
        editor.putString(TEST_INSTRUCTION, strInstruction);
        editor.putString(TEST_LONG_NAME, strLongName);
        editor.putString(TEST_C_A__MARK, crt_ans_mark);
        editor.putString(TEST_W_A__MARK, wrng_ans_mark);
        editor.commit();
    }


    public HashMap<String, String> getBookmarkDetailsTest() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(TEST_ID, pref.getString(TEST_ID, null));
        user.put(TEST_TYPE, pref.getString(TEST_TYPE, null));
        user.put(TEST_NUM_OF_Q, pref.getString(TEST_NUM_OF_Q, null));
        user.put(TEST_IS_FINISHED, pref.getString(TEST_IS_FINISHED, null));
        user.put(TEST_Q_TYPE, pref.getString(TEST_Q_TYPE, null));
        user.put(TEST_INSTRUCTION, pref.getString(TEST_INSTRUCTION, null));
        user.put(TEST_LONG_NAME, pref.getString(TEST_LONG_NAME, null));
        user.put(TEST_C_A__MARK, pref.getString(TEST_C_A__MARK, null));
        user.put(TEST_W_A__MARK, pref.getString(TEST_W_A__MARK, null));

        return user;
    }

    public void setSearchCategoryTitle(String title) {
        editor.putString(SearchCategoryTitle, title);
        editor.commit();
    }

    public String getSearchCategoryTitle() {
        return pref.getString(SearchCategoryTitle, "");
    }

    public HashMap<String, String> getBookmarkDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(HEAD_ID, pref.getString(HEAD_ID, null));
        user.put(BOOKMARK_TITLE, pref.getString(BOOKMARK_TITLE, null));
        user.put(SUBJECT_ID, pref.getString(SUBJECT_ID, null));
        user.put(LONG_NAME, pref.getString(LONG_NAME, null));
        user.put(HEAD_LIST, pref.getString(HEAD_LIST, null));
        user.put(CHAPTER_ID, pref.getString(CHAPTER_ID, null));
        user.put(CHAPTER_NAME, pref.getString(CHAPTER_NAME, null));
        user.put(CHAPTER_SUPERHEADING, pref.getString(CHAPTER_SUPERHEADING, null));
        user.put(POINT_ID, pref.getString(POINT_ID, null));
        user.put(LOADING_TYPE, pref.getString(LOADING_TYPE, null));
        return user;
    }


    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();

        user.put(KEY_UID, pref.getString(KEY_UID, null));
        user.put(KEY_ID, pref.getString(KEY_ID, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_MOBILE, pref.getString(KEY_MOBILE, null));
        user.put(KEY_JWT, pref.getString(KEY_JWT, null));
        user.put(KEY_FNAME, pref.getString(KEY_FNAME, null));
        user.put(KEY_LNAME, pref.getString(KEY_LNAME, null));

        return user;
    }

    public HashMap<String, String> getApiHeader() {
        HashMap<String, String> user = new HashMap<String, String>();

        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put("authorization", pref.getString(KEY_JWT, null));
        user.put("userId", pref.getString(KEY_ID, null));
        return user;
    }


    public String getPin() {
        return pref.getString(KEY_PIN, null);
    }
    public String udatestarttime() {
        return pref.getString(START_TIME, "0");
    }

    public String getTime() {
        return pref.getString(KEY_TIME, null);
    }

    public String getMilliseconds() {
        return pref.getString(KEY_MILLISECONDS, null);
    }

    public String getKeyPackageId() {
        return pref.getString(KEY_PACKAGE_ID, null);
    }

    public void updateTimer(String milliseconds, String sessionId) {
        editor.putString(KEY_MILLISECONDS, milliseconds);
        editor.putString(KEY_PACKAGE_ID, sessionId);

        editor.commit();
    }

    public void logoutUser() {

        // After logout redirect user to Loing Activity

        logout();

        // new AsyntaskRunner().execute();
//        Intent i = new Intent(_context, SignInSighUp.class);
//        // Closing all the Activities
//        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//        // Add new Flag to start new Activity
//        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
//        // Staring Login Activity
//        _context.startActivity(i);
//
//        if(DrawerContentSlideActivity.drawerContentSlideActivity != null)
//            DrawerContentSlideActivity.drawerContentSlideActivity.finish();
//        if(EnterPin.enterPin != null)
//            EnterPin.enterPin.finish();
//        if(MobilePagesActivity.mobilePagesActivity != null)
//            MobilePagesActivity.mobilePagesActivity.finish();

    }

    private void logout() {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(_context);
        HashMap<String, String> params = new HashMap<>();
        params.put("id", pref.getString(KEY_ID, null));
        params.put("time", pref.getString(KEY_TIME, null));
        mRequest.makeServiceRequest(IConstant.signOutUrl, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------signOutUrl Response----------------" + response);
                String sStatus = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    loader.dismissLoader();
                    if (sStatus.equals("1")) {
                        CartDataBase cartlistDatabase = CartDataBase.getAppDatabase(_context);
                        cartlistDatabase.cartListDao().nukeTable();
                        new SocketManager(_context).disconnect();
                        editor.clear();
                        editor.commit();
                        Checkout.clearUserData(_context);
                        Intent i = new Intent(_context, SignInSighUp.class);
                        // Closing all the Activities
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        // Add new Flag to start new Activity
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                        // Staring Login Activity
                        _context.startActivity(i);

                        if (DrawerContentSlideActivity.drawerContentSlideActivity != null)
                            DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                        if (EnterPin.enterPin != null)
                            EnterPin.enterPin.finish();
                        if (MobilePagesActivity.mobilePagesActivity != null)
                            MobilePagesActivity.mobilePagesActivity.finish();
                    }


                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                Log.e("Logout Error", errorMessage);
                loader.dismissLoader();

            }
        });
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    private class AsyntaskRunner extends AsyncTask<String, String, String> {

        String result = "";

        @Override
        protected String doInBackground(String... strings) {
            ServiceRequest mRequest = new ServiceRequest(_context);
            HashMap<String, String> params = new HashMap<>();
            params.put("id", pref.getString(KEY_ID, null));
            params.put("time", pref.getString(KEY_TIME, null));
            mRequest.makeServiceRequest(IConstant.signOutUrl, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("------------signOutUrl Response----------------" + response);
                    String sStatus = "";
                    try {

                        editor.clear();
                        editor.commit();
                        JSONObject object = new JSONObject(response);
                        result = object.getString("status");

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                }

                @Override
                public void onErrorListener(String errorMessage) {
                    Log.e("Logout Error", errorMessage);

                }
            });
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equals("1")) {

                new SocketManager(_context).disconnect();
                Intent i = new Intent(_context, SignInSighUp.class);
                // Closing all the Activities
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                // Add new Flag to start new Activity
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                // Staring Login Activity
                _context.startActivity(i);

                if (DrawerContentSlideActivity.drawerContentSlideActivity != null)
                    DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                if (EnterPin.enterPin != null)
                    EnterPin.enterPin.finish();
                if (MobilePagesActivity.mobilePagesActivity != null)
                    MobilePagesActivity.mobilePagesActivity.finish();
            }

        }
    }


    public void setpositios(String subject_name, String subject_short_name, String subject_trial_status) {

        editor.putString(LAST_READ_SUBJECT_NAME, subject_name);

        editor.commit();
    }

    public HashMap<String, String> getpositions() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(READ_SUBJECT_NAME, pref.getString(READ_SUBJECT_NAME, null));
        return user;
    }


}
