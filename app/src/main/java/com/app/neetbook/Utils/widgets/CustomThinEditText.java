package com.app.neetbook.Utils.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class CustomThinEditText extends EditText {
    public CustomThinEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    public CustomThinEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public CustomThinEditText(Context context) {
        super(context);
        init();
    }
    @SuppressLint("WrongConstant")
    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/ProximaNovaThin.otf");
        setTypeface(tf ,1);
    }
}
