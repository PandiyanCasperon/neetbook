package com.app.neetbook.Utils.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class CustomMediumButton extends Button {
    public CustomMediumButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomMediumButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomMediumButton(Context context) {
        super(context);
        init();
    }

    @SuppressLint("WrongConstant")
    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/ProximaNovaSemibold.otf");
        setTypeface(tf, 1);
    }
}
