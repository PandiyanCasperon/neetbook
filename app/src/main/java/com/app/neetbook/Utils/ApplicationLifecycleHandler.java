package com.app.neetbook.Utils;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Messenger;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import com.app.neetbook.R;
import com.app.neetbook.ReceviceMessageEvent;
import com.app.neetbook.View.EnterPin;
import com.app.neetbook.socket.SocketHandler;
import com.app.neetbook.socket.SocketManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.POWER_SERVICE;

public class ApplicationLifecycleHandler implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {

    private static final String TAG = ApplicationLifecycleHandler.class.getSimpleName();
    private static boolean isInBackground = false;
    private static Activity activity;
    //  private static SocketHandler socketHandler;
    private static ConnectionDetector cd;
    public static SocketManager manager;
    private static SessionManager sessionManager;
    private static Handler mHandler;
    private static Runnable mRunnable;
    public static boolean isOverThreeMun = false;
    public static boolean forceLogout = false;
    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        this.activity = activity;


        if (!activity.getResources().getBoolean(R.bool.isTablet))
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        cd = new ConnectionDetector(this.activity);
        sessionManager = new SessionManager(activity);

        //     manager = new SocketManager(callBack, activity);
        manager = getInstance(activity);


        if (cd.isConnectingToInternet() && !manager.getconnected()) {
            manager.connect();
        }
        Log.e(TAG, " manager.connect()" + manager.getconnected());

      /*  if (cd.isConnectingToInternet()&& !socketHandler.getSocketManager().isConnected ) {
            socketHandler.getSocketManager().connect();
        }
        socketHandler.getSocketManager().onJoinNeet("");*/
    }


    public static SocketManager getInstance(Activity activity) {
        if (manager == null) {
            manager = new SocketManager(callBack, activity);
        }
        return manager;
    }

    @Override
    public void onActivityStarted(Activity activity) {

        this.activity = activity;
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
        if (!activity.getResources().getBoolean(R.bool.isTablet))
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Log.e(TAG, " manager.connect()" + manager.getconnected());

    }

    public static  SocketManager.SocketCallBack callBack = new SocketManager.SocketCallBack() {
        boolean isChat = false;

        @Override
        public void onSuccessListener(String eventName,Object response) {


            if (response instanceof JSONObject) {
                final JSONObject jsonObject = (JSONObject) response;
                if(eventName.equalsIgnoreCase("onLogout"))
                {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if(sessionManager.isLoggedIn()) {
                                    long newSessionTime = Long.parseLong(jsonObject.getString("time"));
                                    long currentSessionTime = Long.parseLong(sessionManager.getTime());
                                    if (currentSessionTime < newSessionTime)
                                    {
                                        if(ActionBarActivityNeetBook.isSessionOut.equalsIgnoreCase("No")) {
                                            new CustomAlert(activity).singleLoginAlertLogout();
                                        }
                                        else{
                                            ActionBarActivityNeetBook.forceLogout = "Yes";
                                            ActionBarActivityNeetBook.isSessionOut = "No";
                                        }


                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                }
                System.out.println("ApplicationLifecycleHandler -----------SocketHandlerjsonObject------------" + jsonObject);

            }
        }
    };
//"time":1575882498195
    @Override
    public void onActivityResumed(Activity activity) {

//        if (isInBackground) {
//            Log.e(TAG, "app went to foreground");
//            Log.e("Current Class", activity.getLocalClassName());
//            isInBackground = false;
//            if (sessionManager.isLoggedIn() && EnterPin.IsShowing.equalsIgnoreCase("true") && !activity.getLocalClassName().equalsIgnoreCase("View.SplashActivity") && !activity.getLocalClassName().equalsIgnoreCase("View.EnterPin") && !activity.getLocalClassName().equalsIgnoreCase("View.SignInSighUp")) {
//                Log.e("Calling from","ApplicationLifeCycleHandler");
//                Intent intent = new Intent(activity, EnterPin.class);
//                intent.putExtra("Background", "Yes");
//                activity.startActivity(intent);
//            }
//        }

    }

    @Override
    public void onActivityPaused(Activity activity) {
        forceLogout = false;
    }

    @Override
    public void onActivityStopped(Activity activity) {
//        PowerManager powerManager = (PowerManager) activity.getSystemService(POWER_SERVICE);
//        boolean isScreenOn;
//        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
//            isScreenOn = powerManager.isInteractive();
//            Log.e(TAG, "isScreenOn"+isScreenOn);
//        } else {
//            isScreenOn = powerManager.isScreenOn();
//            Log.e(TAG, "isScreenOn"+isScreenOn);
//        }
//
//        if (!isScreenOn && activity.getResources().getBoolean(R.bool.isTablet) && sessionManager.isLoggedIn()) {
//
//            EnterPin.IsShowing = "true";
//            Log.e(TAG, "app went to background");
//            isInBackground = true;
//        }

        if(EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);


    }



    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    @Override
    public void onConfigurationChanged(Configuration configuration) {
    }

    @Override
    public void onLowMemory() {

    }

    @Override
    public void onTrimMemory(int i) {
//        if (i == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) {
//            EnterPin.IsShowing = "true";
//            Log.e(TAG, "app went to background");
//            isInBackground = true;
//        }
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void customEventReceived(MyCustomEvent event) {
        Toast.makeText(activity, event.data, Toast.LENGTH_SHORT).show();
    }




}




