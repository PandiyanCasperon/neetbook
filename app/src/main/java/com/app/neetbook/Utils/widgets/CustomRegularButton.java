package com.app.neetbook.Utils.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class CustomRegularButton extends Button {
    public CustomRegularButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    public CustomRegularButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public CustomRegularButton(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/ProximaNovaRegular.otf");
        setTypeface(tf);
    }
}
