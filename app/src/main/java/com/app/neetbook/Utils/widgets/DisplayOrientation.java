package com.app.neetbook.Utils.widgets;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

import com.app.neetbook.R;

public class DisplayOrientation {
    public static int getDisplayOrientation(Context activity) {
        return ((WindowManager) activity.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
    }
}
