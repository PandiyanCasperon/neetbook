package com.app.neetbook.Utils.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomBoldTextview extends TextView {

        public CustomBoldTextview(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            init();
        }
        public CustomBoldTextview(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }
        public CustomBoldTextview(Context context) {
            super(context);
            init();
        }
        @SuppressLint("WrongConstant")
        public void init() {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/ProximaNovaBold.otf");
            setTypeface(tf ,1);
        }
}
