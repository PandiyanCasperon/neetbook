package com.app.neetbook.Utils.widgets;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;

import androidx.core.text.HtmlCompat;

public class HTMLTextConverter {
    public static Spanned stripHtml(String html) {
        if (!TextUtils.isEmpty(html)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                return HtmlCompat.fromHtml(html, HtmlCompat.FROM_HTML_MODE_COMPACT);
            } else {
                return Html.fromHtml(html);
            }
        }
        return null;
    }
}
