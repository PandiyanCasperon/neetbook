package com.app.neetbook.Model.sidemenuFromContent;

import androidx.annotation.Nullable;

public class SubscriptionTestComplete {

  @Nullable
  private String title;
  @Nullable
  private String questionTitle;
  @Nullable
  private String questionCount;
  @Nullable
  private String questionOptionFirst;
  @Nullable
  private String questionOptionSecond;
  @Nullable
  private String questionOptionThird;
  @Nullable
  private String questionOptionFourth;
  private String imageUrl;

  public String getQuestionCount() {
    return questionCount;
  }

  public void setQuestionCount(String questionCounnt) {
    this.questionCount = questionCounnt;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getQuestionTitle() {
    return questionTitle;
  }

  public void setQuestionTitle(String questionTitle) {
    this.questionTitle = questionTitle;
  }

  public String getQuestionOptionFirst() {
    return questionOptionFirst;
  }

  public void setQuestionOptionFirst(String questionOptionFirst) {
    this.questionOptionFirst = questionOptionFirst;
  }

  public String getQuestionOptionSecond() {
    return questionOptionSecond;
  }

  public void setQuestionOptionSecond(String questionOptionSecond) {
    this.questionOptionSecond = questionOptionSecond;
  }

  public String getQuestionOptionThird() {
    return questionOptionThird;
  }

  public void setQuestionOptionThird(String questionOptionThird) {
    this.questionOptionThird = questionOptionThird;
  }

  public String getQuestionOptionFourth() {
    return questionOptionFourth;
  }

  public void setQuestionOptionFourth(String questionOptionFourth) {
    this.questionOptionFourth = questionOptionFourth;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }
}
