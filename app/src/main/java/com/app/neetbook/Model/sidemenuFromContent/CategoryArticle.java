package com.app.neetbook.Model.sidemenuFromContent;

import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.McqExam;

import java.io.Serializable;
import java.util.ArrayList;

public class CategoryArticle implements Serializable {
  public String _id;
  public String chapt_name;
  public String long_name;
  public String short_name;
  public ArrayList<HeadDetails> headDetailsArrayList;
  public ArrayList<McqExam> mcqExamArrayList;
  public String images;
}
