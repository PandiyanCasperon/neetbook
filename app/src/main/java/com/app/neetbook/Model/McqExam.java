package com.app.neetbook.Model;

import java.io.Serializable;

public class McqExam implements Serializable {
    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getExam_cat_name() {
        return exam_cat_name;
    }

    public void setExam_cat_name(String exam_cat_name) {
        this.exam_cat_name = exam_cat_name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getLong_name() {
        return long_name;
    }

    public void setLong_name(String long_name) {
        this.long_name = long_name;
    }

    String _id;
    String exam_cat_name;
    String short_name;
    String long_name;
}
