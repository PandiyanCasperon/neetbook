package com.app.neetbook.Model;


import com.app.neetbook.homepageRoom.Converters;

import java.io.Serializable;
import java.util.List;

import androidx.databinding.BaseObservable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

@Entity(tableName = "GroupsList")
public class Groupslist implements Serializable {

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    @PrimaryKey(autoGenerate = true)
    private int cid;

    public String getHomegroupid() {
        return homegroupid;
    }

    public void setHomegroupid(String homegroupid) {
        this.homegroupid = homegroupid;
    }

    public String getHomegroupname() {
        return homegroupname;
    }

    public void setHomegroupname(String homegroupname) {
        this.homegroupname = homegroupname;
    }

    public String getHomeshortName() {
        return homeshortName;
    }

    public void setHomeshortName(String homeshortName) {
        this.homeshortName = homeshortName;
    }

    public String getHomelongName() {
        return homelongName;
    }

    public void setHomelongName(String homelongName) {
        this.homelongName = homelongName;
    }

    @ColumnInfo(name = "homeGroupId")
    String homegroupid;
    @ColumnInfo(name = "homeGroupName")
    String homegroupname;
    @ColumnInfo(name = "homeShortName")
    String homeshortName;
    @ColumnInfo(name = "homeLongName")
    String homelongName;







}
