package com.app.neetbook.Model;

import java.io.Serializable;

public class TestScores implements Serializable {

    private String score;
    private String title;
    private String percent;

    public TestScores(String score, String title, String percent) {
        this.score = score;
        this.title = title;
        this.percent = percent;
    }

    public String getPercent() { return percent; }

    public void setPercent(String percent) { this.percent = percent; }

    public String getScore() { return score; }

    public void setScore(String score) { this.score = score; }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

}
