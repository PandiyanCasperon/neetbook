package com.app.neetbook.Model;

public class GroupChildBean {
    public String getStrImages() {
        return strImages;
    }

    public void setStrImages(String strImages) {
        this.strImages = strImages;
    }

    public String getStrTitle() {
        return strTitle;
    }

    public void setStrTitle(String strTitle) {
        this.strTitle = strTitle;
    }

    public String getStrChapter() {
        return strChapter;
    }

    public void setStrChapter(String strChapter) {
        this.strChapter = strChapter;
    }

    public String getStrMCQ() {
        return strMCQ;
    }

    public void setStrMCQ(String strMCQ) {
        this.strMCQ = strMCQ;
    }

    public String getStrTopics() {
        return strTopics;
    }

    public void setStrTopics(String strTopics) {
        this.strTopics = strTopics;
    }

    public String getStrIsSubcribed() {
        return strIsSubcribed;
    }

    public void setStrIsSubcribed(String strIsSubcribed) {
        this.strIsSubcribed = strIsSubcribed;
    }

    public String getStrWeekOne() {
        return strWeekOne;
    }

    public void setStrWeekOne(String strWeekOne) {
        this.strWeekOne = strWeekOne;
    }

    public String getStrWeekTwo() {
        return strWeekTwo;
    }

    public void setStrWeekTwo(String strWeekTwo) {
        this.strWeekTwo = strWeekTwo;
    }

    public String getStrWeekontAmount() {
        return strWeekontAmount;
    }

    public void setStrWeekontAmount(String strWeekontAmount) {
        this.strWeekontAmount = strWeekontAmount;
    }

    public String getStrWeekTwoAmount() {
        return strWeekTwoAmount;
    }

    public void setStrWeekTwoAmount(String strWeekTwoAmount) {
        this.strWeekTwoAmount = strWeekTwoAmount;
    }

    public String getStrRemainingHours() {
        return strRemainingHours;
    }

    public void setStrRemainingHours(String strRemainingHours) {
        this.strRemainingHours = strRemainingHours;
    }

    public String getStrRemainingDays() {
        return strRemainingDays;
    }

    public void setStrRemainingDays(String strRemainingDays) {
        this.strRemainingDays = strRemainingDays;
    }

    public String getStrId() {
        return strId;
    }

    public void setStrId(String strId) {
        this.strId = strId;
    }

    String strId;

    public String getStrTrial() {
        return strTrial;
    }

    public void setStrTrial(String strTrial) {
        this.strTrial = strTrial;
    }

    String strTrial;

    public String getStrGroupId() {
        return strGroupId;
    }

    public void setStrGroupId(String strGroupId) {
        this.strGroupId = strGroupId;
    }

    public String getStrShortName() {
        return strShortName;
    }

    public void setStrShortName(String strShortName) {
        this.strShortName = strShortName;
    }

    public String getStrLongName() {
        return strLongName;
    }

    public void setStrLongName(String strLongName) {
        this.strLongName = strLongName;
    }

    public String getStrPackageOneId() {
        return strPackageOneId;
    }

    public void setStrPackageOneId(String strPackageOneId) {
        this.strPackageOneId = strPackageOneId;
    }

    public String getStrPackegeTwoId() {
        return strPackegeTwoId;
    }

    public void setStrPackegeTwoId(String strPackegeTwoId) {
        this.strPackegeTwoId = strPackegeTwoId;
    }

    public String getStrTabImages() {
        return strTabImages;
    }

    public void setStrTabImages(String strTabImages) {
        this.strTabImages = strTabImages;
    }

    String strTabImages;

    public String getStrPrice() {
        return strPrice;
    }

    public void setStrPrice(String strPrice) {
        this.strPrice = strPrice;
    }

    String strPrice;
    String strGroupId,strShortName,strLongName,strPackageOneId,strPackegeTwoId;
    String strImages,strTitle,strChapter,strMCQ,strTopics,strIsSubcribed,strWeekOne,strWeekTwo,strWeekontAmount,strWeekTwoAmount,strRemainingHours,strRemainingDays;
}
