package com.app.neetbook.Model;

public class NewSuperListPojo {
    String isSH;
    String long_name;
    String id;
    String firsthead;
    String chapter_name;
    String chapter_id;
    String head_id;
    public NewSuperListPojo(String isSH, String long_name, String id, String firsthead, String chapter_name, String chapter_id, String head_id) {
        this.isSH = isSH;
        this.long_name = long_name;
        this.id = id;
        this.firsthead = firsthead;
        this.chapter_name = chapter_name;
        this.chapter_id = chapter_id;
        this.head_id = head_id;

    }


    public String getChapter_name() {
        return chapter_name;
    }

    public void setChapter_name(String chapter_name) {
        this.chapter_name = chapter_name;
    }

    public String getChapter_id() {
        return chapter_id;
    }

    public void setChapter_id(String chapter_id) {
        this.chapter_id = chapter_id;
    }

    public String getHead_id() {
        return head_id;
    }

    public void setHead_id(String head_id) {
        this.head_id = head_id;
    }


    public String getIsSH() {
        return isSH;
    }

    public void setIsSH(String isSH) {
        this.isSH = isSH;
    }

    public String getLong_name() {
        return long_name;
    }

    public void setLong_name(String long_name) {
        this.long_name = long_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirsthead() {
        return firsthead;
    }

    public void setFirsthead(String firsthead) {
        this.firsthead = firsthead;
    }

}
