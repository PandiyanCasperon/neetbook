package com.app.neetbook.Model.ArticleContent;

import java.util.ArrayList;

public class ArticleChildItem {
    public String isHeader;
    public String title;
    public String summary;
    public String content;
    public ArrayList<String> images;
    public int headerPosition;
    public Boolean is_webview_loaded;

    public ArticleChildItem(String title, String isHeader,String summary,String content, int hPosition,ArrayList<String> images,Boolean is_webview_loaded) {
        this.title = title;
        this.isHeader = isHeader;
        this.summary = summary;
        this.content = content;
        this.images = images;
        this.headerPosition = headerPosition;
        this.is_webview_loaded = is_webview_loaded;
    }
}
