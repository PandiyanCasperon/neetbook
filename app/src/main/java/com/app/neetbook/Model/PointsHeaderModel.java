package com.app.neetbook.Model;

import com.app.neetbook.Interfaces.PointsSection;

import java.util.ArrayList;

public class PointsHeaderModel implements PointsSection {
    public PointsHeaderModel(int section) {
        this.section = section;
    }
    public void setId(String id) {
        this.id = id;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public void setChaptName(String chaptName) {
        ChaptName = chaptName;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setPointsContentArrayList(ArrayList<PointsContent> pointsContentArrayList) {
        this.pointsContentArrayList = pointsContentArrayList;
    }

    String id;
    String shortName;
    String longName;
    String ChaptName;
    String image;
    String content;

    public void setCount(String count) {
        this.count = count;
    }

    String count;
    int section;
    ArrayList<PointsContent> pointsContentArrayList;

    @Override
    public boolean isHeader() {
        return true;
    }

    @Override
    public String getCount() {
        return count;
    }

    @Override
    public int sectionPosition() {
        return section;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getChaptName() {
        return ChaptName;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public String getLongName() {
        return longName;
    }

    @Override
    public String getShortName() {
        return shortName;
    }

    @Override
    public String getImage() {
        return image;
    }

    @Override
    public ArrayList<PointsContent> getPointsContent() {
        return pointsContentArrayList;
    }
}
