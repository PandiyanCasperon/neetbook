package com.app.neetbook.Model.subscription;

import java.io.Serializable;

import androidx.annotation.Nullable;

public class Subscription implements Serializable {

  @Nullable
  private String title;

  @Nullable
  private int Image;

  @Nullable
  private String chapter;

  @Nullable
  private String topics;

  @Nullable
  private String questions;

  @Nullable
  private String miniSubscription;

  @Nullable
  private String miniSubscriptionPrice;

  @Nullable
  public String getStrMiniHours() {
    return strMiniHours;
  }

  public void setStrMiniHours(@Nullable String strMiniHours) {
    this.strMiniHours = strMiniHours;
  }

  @Nullable
  public String getStrLargeHours() {
    return strLargeHours;
  }

  public void setStrLargeHours(@Nullable String strLargeHours) {
    this.strLargeHours = strLargeHours;
  }

  @Nullable
  private String strMiniHours;

  @Nullable
  private String strLargeHours;

  @Nullable
  private String largeSubscription;

  @Nullable
  private String largeSubscriptionPrice;

  private boolean isMiniSubscriptionChecked;

  private boolean isLargeSubscriptionChecked;

  private boolean isSubscribed;

  public boolean isSubscribed() {
    return isSubscribed;
  }

  public void setSubscribed(boolean subscribed) {
    isSubscribed = subscribed;
  }

  public int getImage() {
    return Image;
  }

  public void setImage(int image) {
    Image = image;
  }

  public String getMiniSubscriptionPrice() {
    return miniSubscriptionPrice;
  }

  public void setMiniSubscriptionPrice(String miniSubscriptionPrice) {
    this.miniSubscriptionPrice = miniSubscriptionPrice;
  }

  public String getLargeSubscriptionPrice() {
    return largeSubscriptionPrice;
  }

  public void setLargeSubscriptionPrice(String largeSubscriptionPrice) {
    this.largeSubscriptionPrice = largeSubscriptionPrice;
  }

  public boolean isMiniSubscriptionChecked() {
    return isMiniSubscriptionChecked;
  }

  public void setMiniSubscriptionChecked(boolean miniSubscriptionChecked) {
    isMiniSubscriptionChecked = miniSubscriptionChecked;
  }

  public boolean isLargeSubscriptionChecked() {
    return isLargeSubscriptionChecked;
  }

  public void setLargeSubscriptionChecked(boolean largeSubscriptionChecked) {
    isLargeSubscriptionChecked = largeSubscriptionChecked;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getChapter() {
    return chapter;
  }

  public void setChapter(String chapter) {
    this.chapter = chapter;
  }

  public String getTopics() {
    return topics;
  }

  public void setTopics(String topics) {
    this.topics = topics;
  }

  public String getQuestions() {
    return questions;
  }

  public void setQuestions(String questions) {
    this.questions = questions;
  }

  public String getMiniSubscription() {
    return miniSubscription;
  }

  public void setMiniSubscription(String miniSubscription) {
    this.miniSubscription = miniSubscription;
  }

  public String getLargeSubscription() {
    return largeSubscription;
  }

  public void setLargeSubscription(String largeSubscription) {
    this.largeSubscription = largeSubscription;
  }


  private String strId;
  private String strShortName;
  private String strLongName;
  private String strImage;
  private String strGroupId;
  private String strTrial;
  private String strIsSubcribed;
  private String strPackageOneId;
  private String strPackageTwoId;
  private String strRemainingDays;

  public String getStrId() {
    return strId;
  }

  public void setStrId(String strId) {
    this.strId = strId;
  }

  public String getStrShortName() {
    return strShortName;
  }

  public void setStrShortName(String strShortName) {
    this.strShortName = strShortName;
  }

  public String getStrLongName() {
    return strLongName;
  }

  public void setStrLongName(String strLongName) {
    this.strLongName = strLongName;
  }

  public String getStrImage() {
    return strImage;
  }

  public void setStrImage(String strImage) {
    this.strImage = strImage;
  }

  public String getStrGroupId() {
    return strGroupId;
  }

  public void setStrGroupId(String strGroupId) {
    this.strGroupId = strGroupId;
  }

  public String getStrTrial() {
    return strTrial;
  }

  public void setStrTrial(String strTrial) {
    this.strTrial = strTrial;
  }

  public String getStrIsSubcribed() {
    return strIsSubcribed;
  }

  public void setStrIsSubcribed(String strIsSubcribed) {
    this.strIsSubcribed = strIsSubcribed;
  }

  public String getStrPackageOneId() {
    return strPackageOneId;
  }

  public void setStrPackageOneId(String strPackageOneId) {
    this.strPackageOneId = strPackageOneId;
  }

  public String getStrPackageTwoId() {
    return strPackageTwoId;
  }

  public void setStrPackageTwoId(String strPackageTwoId) {
    this.strPackageTwoId = strPackageTwoId;
  }

  public String getStrRemainingDays() {
    return strRemainingDays;
  }

  public void setStrRemainingDays(String strRemainingDays) {
    this.strRemainingDays = strRemainingDays;
  }

  public String getStrRemainingHours() {
    return strRemainingHours;
  }

  public void setStrRemainingHours(String strRemainingHours) {
    this.strRemainingHours = strRemainingHours;
  }

  private String strRemainingHours;
}
