package com.app.neetbook.Model.subscription;

import com.app.neetbook.CartRoom.CartConverters;

import java.io.Serializable;

import androidx.annotation.Nullable;
import androidx.databinding.BaseObservable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

@Entity(tableName = "CartItems")
public class SubscriptionCart  {

  public int getCid() {
    return cid;
  }

  public void setCid(int cid) {
    this.cid = cid;
  }

  @PrimaryKey(autoGenerate = true)
  private int cid;

  @ColumnInfo(name = "title")
  private String title;

  @ColumnInfo(name = "subscription")
  private String subscription;

  @ColumnInfo(name = "price")
  private String price;

  @ColumnInfo(name = "strId")
  private String strId;


  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSubscription() {
    return subscription;
  }

  public void setSubscription(String subscription) {
    this.subscription = subscription;
  }

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public String getStrId() {
    return strId;
  }

  public void setStrId(String strId) {
    this.strId = strId;
  }


}
