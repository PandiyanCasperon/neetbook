package com.app.neetbook.Model;

public class NewMcqSuperListPojo {
    String isSH;
    String long_name;
    String id;
    String firsthead;

    public String getIsSH() {
        return isSH;
    }

    public void setIsSH(String isSH) {
        this.isSH = isSH;
    }

    public String getLong_name() {
        return long_name;
    }

    public void setLong_name(String long_name) {
        this.long_name = long_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirsthead() {
        return firsthead;
    }

    public void setFirsthead(String firsthead) {
        this.firsthead = firsthead;
    }

    public NewMcqSuperListPojo(String isSH, String long_name, String id, String firsthead) {
        this.isSH = isSH;
        this.long_name = long_name;
        this.id = id;
        this.firsthead = firsthead;
    }
}
