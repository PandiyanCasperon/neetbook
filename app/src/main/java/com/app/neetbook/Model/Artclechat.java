package com.app.neetbook.Model;

public class Artclechat {

    String head_name;
    String long_name;
    String short_name;

    public Artclechat(String head_name, String long_name, String short_name, String chapter_id, String _id, String position, String isSH, String firsthead, String overallid) {
        this.head_name = head_name;
        this.long_name = long_name;
        this.short_name = short_name;
        this.chapter_id = chapter_id;
        this._id = _id;
        this.position = position;
        this.isSH = isSH;
        this.firsthead = firsthead;
        this.overallid = overallid;
    }

    public String getLong_name() {
        return long_name;
    }

    public void setLong_name(String long_name) {
        this.long_name = long_name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getChapter_id() {
        return chapter_id;
    }

    public void setChapter_id(String chapter_id) {
        this.chapter_id = chapter_id;
    }

    public String get_id() {
        return _id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getIsSH() {
        return isSH;
    }

    public void setIsSH(String isSH) {
        this.isSH = isSH;
    }

    public String getFirsthead() {
        return firsthead;
    }

    public void setFirsthead(String firsthead) {
        this.firsthead = firsthead;
    }

    public String getOverallid() {
        return overallid;
    }

    public void setOverallid(String overallid) {
        this.overallid = overallid;
    }

    String chapter_id;
    String _id;
    String position;
    String isSH;
    String firsthead;
    String overallid;

    public String get_head_name() {
        return head_name;
    }

    public void set_id(String _head_name) {
        this.head_name = head_name;
    }





}
