package com.app.neetbook.Model.sidemenuFromContent;

import java.util.ArrayList;

import androidx.annotation.Nullable;

public class SubscriptionArticleBody {

  @Nullable
  public String points;

  @Nullable
  public String summary;


  @Nullable
  public String isHeader;

  @Nullable
  public String titile;
  @Nullable
  public ArrayList<String> strImages;

  public SubscriptionArticleBody(@Nullable String titile,@Nullable String points,@Nullable String summary, ArrayList<String> strImages, String isHeader) {
    this.titile = titile;
    this.points = points;
    this.strImages = strImages;
    this.summary = summary;
    this.isHeader = isHeader;
  }



}


