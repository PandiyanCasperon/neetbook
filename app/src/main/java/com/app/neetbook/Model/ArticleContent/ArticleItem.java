package com.app.neetbook.Model.ArticleContent;

import com.app.neetbook.StickeyHeader.StickyHeaderModel;

public class ArticleItem implements StickyHeaderModel {

    public final String title;
    public final String summary;

    public ArticleItem(String title,String summary) {
        this.title = title;
        this.summary = summary;
    }
}