package com.app.neetbook.Model.sidemenuFromContent;

import androidx.annotation.Nullable;

public class SubscriptionArticleHeader {

  @Nullable
  public String header;

  public SubscriptionArticleHeader(@Nullable String header) {
    this.header = header;
  }
}


