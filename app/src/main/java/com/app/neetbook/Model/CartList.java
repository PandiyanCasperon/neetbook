package com.app.neetbook.Model;

import com.app.neetbook.CartRoom.CartConverters;
import com.app.neetbook.Model.subscription.SubscriptionCart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import androidx.databinding.BaseObservable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
@Entity(tableName = "CartItems")
public class CartList {

    public CartList(List<SubscriptionCart> mSubscriptionCartList) {
        this.subscriptionCartList = mSubscriptionCartList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @PrimaryKey(autoGenerate = true)
    private int id;

    public List<SubscriptionCart> getSubscriptionCartList() {
        return subscriptionCartList;
    }

    public void setSubscriptionCartList(List<SubscriptionCart> subscriptionCartList) {
        this.subscriptionCartList = subscriptionCartList;
    }

    @ColumnInfo(name = "carts")
    private List<SubscriptionCart> subscriptionCartList;
}
