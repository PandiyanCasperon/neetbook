package com.app.neetbook.Model;

public class ReferNowBean {
    public String getStrId() {
        return strId;
    }

    public void setStrId(String strId) {
        this.strId = strId;
    }

    public String getStrImage() {
        return strImage;
    }

    public void setStrImage(String strImage) {
        this.strImage = strImage;
    }

    public String getStrTitle() {
        return strTitle;
    }

    public void setStrTitle(String strTitle) {
        this.strTitle = strTitle;
    }

    public String getStrDesc() {
        return strDesc;
    }

    public void setStrDesc(String strDesc) {
        this.strDesc = strDesc;
    }

    String strId,strImage,strTitle,strDesc;

    public String getStrCouponCode() {
        return strCouponCode;
    }

    public void setStrCouponCode(String strCouponCode) {
        this.strCouponCode = strCouponCode;
    }

    String strCouponCode;

    public String getStrPromoName() {
        return strPromoName;
    }

    public void setStrPromoName(String strPromoName) {
        this.strPromoName = strPromoName;
    }

    String strPromoName;
}
