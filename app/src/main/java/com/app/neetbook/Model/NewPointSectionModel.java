package com.app.neetbook.Model;

import java.util.ArrayList;

public class NewPointSectionModel {
    String _id;
    String point_name;
    String short_name;
    String long_name;
    String status;
    String cstatus;
    String content;
    ArrayList<PointsList> pontsTitleContentList;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getPoint_name() {
        return point_name;
    }

    public void setPoint_name(String point_name) {
        this.point_name = point_name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getLong_name() {
        return long_name;
    }

    public void setLong_name(String long_name) {
        this.long_name = long_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCstatus() {
        return cstatus;
    }

    public void setCstatus(String cstatus) {
        this.cstatus = cstatus;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public NewPointSectionModel(String _id, String point_name, String short_name, String long_name, String status, String cstatus, String content) {
        this._id = _id;
        this.point_name = point_name;
        this.short_name = short_name;
        this.long_name = long_name;
        this.status = status;
        this.cstatus = cstatus;
        this.content = content;
    }


}
