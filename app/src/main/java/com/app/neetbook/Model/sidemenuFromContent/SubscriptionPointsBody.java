package com.app.neetbook.Model.sidemenuFromContent;

import androidx.annotation.Nullable;

public class SubscriptionPointsBody {

  @Nullable
  public String points;

  public SubscriptionPointsBody(@Nullable String points) {
    this.points = points;
  }
}


