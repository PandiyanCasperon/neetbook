package com.app.neetbook.Model.subscription;

import com.app.neetbook.Model.MCQExamIds;

import java.util.ArrayList;

import androidx.annotation.Nullable;

public class SubscriptionDetail {

  @Nullable
  public String getId() {
    return id;
  }

  public void setId(@Nullable String id) {
    this.id = id;
  }

  @Nullable
  private String id;
  @Nullable
  private String title;
  @Nullable
  private String questionTitle;
  @Nullable
  private String questionCount;
  @Nullable
  private String questionOptionFirst;
  @Nullable
  private String questionOptionSecond;
  @Nullable
  private String questionOptionThird;
  @Nullable
  private String questionOptionFourth;

  @Nullable
  public String getQuestionOptionFifth() {
    return questionOptionFifth;
  }

  public void setQuestionOptionFifth(@Nullable String questionOptionFifth) {
    this.questionOptionFifth = questionOptionFifth;
  }

  @Nullable
  public String getAnswers() {
    return answers;
  }

  public void setAnswers(@Nullable String answers) {
    this.answers = answers;
  }

  @Nullable
  public String getExplanation() {
    return explanation;
  }

  public void setExplanation(@Nullable String explanation) {
    this.explanation = explanation;
  }

  @Nullable
  public String getReference() {
    return reference;
  }

  public void setReference(@Nullable String reference) {
    this.reference = reference;
  }

  @Nullable
  public ArrayList<String> getImageUrls() {
    return imageUrls;
  }

  public void setImageUrls(@Nullable ArrayList<String> imageUrls) {
    this.imageUrls = imageUrls;
  }

  public ArrayList<MCQExamIds> getExmyr_ids() {
    return exmyr_ids;
  }

  public void setExmyr_ids(ArrayList<MCQExamIds> exmyr_ids) {
    this.exmyr_ids = exmyr_ids;
  }

  @Nullable
  private String questionOptionFifth;
  @Nullable
  private String answers;

  @Nullable
  public String getUserAnswers() {
    return userAnswers;
  }

  public void setUserAnswers(@Nullable String userAnswers) {
    this.userAnswers = userAnswers;
  }

  @Nullable
  private String userAnswers;
  @Nullable
  private String explanation;
  @Nullable
  private String reference;

  public boolean isAnswered() {
    return isAnswered;
  }

  public void setAnswered(boolean answered) {
    isAnswered = answered;
  }

  @Nullable
  public ArrayList<String> getUserSelectedAnswer() {
    return userSelectedAnswer;
  }

  public void setUserSelectedAnswer(@Nullable ArrayList<String> userSelectedAnswer) {
    this.userSelectedAnswer = userSelectedAnswer;
  }

  @Nullable
  private boolean isAnswered;

  @Nullable
  private ArrayList<String> userSelectedAnswer;

  @Nullable
  private ArrayList<String> imageUrls;
  private ArrayList<MCQExamIds> exmyr_ids;
  private String imageUrl;

  public String getQuestionCount() {
    return questionCount;
  }

  public void setQuestionCount(String questionCounnt) {
    this.questionCount = questionCounnt;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getQuestionTitle() {
    return questionTitle;
  }

  public void setQuestionTitle(String questionTitle) {
    this.questionTitle = questionTitle;
  }

  public String getQuestionOptionFirst() {
    return questionOptionFirst;
  }

  public void setQuestionOptionFirst(String questionOptionFirst) {
    this.questionOptionFirst = questionOptionFirst;
  }

  public String getQuestionOptionSecond() {
    return questionOptionSecond;
  }

  public void setQuestionOptionSecond(String questionOptionSecond) {
    this.questionOptionSecond = questionOptionSecond;
  }

  public String getQuestionOptionThird() {
    return questionOptionThird;
  }

  public void setQuestionOptionThird(String questionOptionThird) {
    this.questionOptionThird = questionOptionThird;
  }

  public String getQuestionOptionFourth() {
    return questionOptionFourth;
  }

  public void setQuestionOptionFourth(String questionOptionFourth) {
    this.questionOptionFourth = questionOptionFourth;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }
}
