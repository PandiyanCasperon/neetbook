package com.app.neetbook.Model;

import com.app.neetbook.Interfaces.ArticleContentSection;

import java.util.ArrayList;

public class ArticleContentmodel implements ArticleContentSection {
    int section;
    public ArticleContentmodel(int section) {
        this.section = section;
    }
    @Override
    public int sectionPosition() {
        return section;
    }

    public void setSection(int section) {
        this.section = section;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setIsHeader(String isHeader) {
        this.isHeader = isHeader;
    }

    public void setTitile(String titile) {
        this.titile = titile;
    }

    public void setStrImages(ArrayList<String> strImages) {
        this.strImages = strImages;
    }

    public String points;


    public String summary;



    public String isHeader;


    public String titile;

    public ArrayList<String> strImages;

    @Override
    public String points() {
        return points;
    }

    @Override
    public String summary() {
        return summary;
    }

    @Override
    public String isHeader() {
        return isHeader;
    }

    @Override
    public String titile() {
        return titile;
    }

    @Override
    public ArrayList<String> strImages() {
        return strImages;
    }
}
