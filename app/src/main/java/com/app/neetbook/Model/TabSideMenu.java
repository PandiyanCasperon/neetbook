package com.app.neetbook.Model;

import java.io.Serializable;

public class TabSideMenu implements Serializable {
    String id,name,image;

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    String shortName,longName;

    public int getImage_id_acative() {
        return image_id_acative;
    }

    public void setImage_id_acative(int image_id_acative) {
        this.image_id_acative = image_id_acative;
    }

    public int getImage_id_inactive() {
        return image_id_inactive;
    }

    public void setImage_id_inactive(int image_id_inactive) {
        this.image_id_inactive = image_id_inactive;
    }

    int image_id_acative;
    int image_id_inactive;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
