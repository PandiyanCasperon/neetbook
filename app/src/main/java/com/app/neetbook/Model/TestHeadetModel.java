package com.app.neetbook.Model;

import com.app.neetbook.Interfaces.TestSection;

import java.util.ArrayList;

public class TestHeadetModel implements TestSection {
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public void setIsFullTest(String isFullTest) {
        this.isFullTest = isFullTest;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public void setCorrectAnswerMark(String correctAnswerMark) {
        this.correctAnswerMark = correctAnswerMark;
    }

    public void setWrongAnsweMark(String wrongAnsweMark) {
        this.wrongAnsweMark = wrongAnsweMark;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public void setNumOfQuestion(String numOfQuestion) {
        this.numOfQuestion = numOfQuestion;
    }

    public void setNumOfChoice(String numOfChoice) {
        this.numOfChoice = numOfChoice;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setIsFinished(String isFinished) {
        this.isFinished = isFinished;
    }

    String shortName;
    String chapterId;
    String longName;
    String isFullTest;
    String testName;
    String image;
    String chapterName;
    String testId;
    String correctAnswerMark;
    String wrongAnsweMark;
    String questionType;
    String numOfQuestion;
    String numOfChoice;
    String instruction;
    String type;
    String isFinished;

    public void setFullTestCount(String fullTestCount) {
        this.fullTestCount = fullTestCount;
    }

    String fullTestCount;

    @Override
    public String getFullTestCount() {
        return null;
    }

    public TestHeadetModel(int section) {
        this.section = section;
    }
    int section;
    @Override
    public boolean isTestHeader() {
        return true;
    }

    @Override
    public String getName() {
        return testName;
    }

    @Override
    public int sectionPosition() {
        return section;
    }

    @Override
    public String getChapteId() {
        return chapterId;
    }

    @Override
    public String getchapterName() {
        return chapterName;
    }

    @Override
    public String getShortName() {
        return shortName;
    }

    @Override
    public String getLongName() {
        return longName;
    }

    @Override
    public String getImage() {
        return image;
    }

    @Override
    public String getIsFullTest() {
        return isFullTest;
    }

    @Override
    public String getTestName() {
        return testName;
    }

    @Override
    public String getatestId() {
        return testId;
    }

    @Override
    public String getCorrectAnswerMark() {
        return correctAnswerMark;
    }

    @Override
    public String getWrongAnswerMark() {
        return wrongAnsweMark;
    }

    @Override
    public String getQuestionType() {
        return questionType;
    }

    @Override
    public String getNumOfQuestion() {
        return numOfQuestion;
    }

    @Override
    public String getNumOfChoice() {
        return numOfChoice;
    }

    @Override
    public String getInstruction() {
        return instruction;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String isFinished() {
        return isFinished;
    }
    public void setCount(String count) {
        this.count = count;
    }

    String count;
    @Override
    public String getCount() {
        return count;
    }



    public void setTestArrayList(ArrayList<TestList> testArrayList) {
        this.testArrayList = testArrayList;
    }

    ArrayList<TestList> testArrayList;
    @Override
    public ArrayList<TestList> getTestArrayList() {
        return testArrayList;
    }
}
