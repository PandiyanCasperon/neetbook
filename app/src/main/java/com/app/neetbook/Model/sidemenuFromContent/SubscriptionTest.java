package com.app.neetbook.Model.sidemenuFromContent;

import java.util.ArrayList;

import androidx.annotation.Nullable;

public class SubscriptionTest {

  @Nullable
  public String get_id() {
    return _id;
  }

  public void set_id(@Nullable String _id) {
    this._id = _id;
  }

  @Nullable
  public String getTesttype() {
    return testtype;
  }

  public void setTesttype(@Nullable String testtype) {
    this.testtype = testtype;
  }

  @Nullable
  public String getQuestionOptionFifth() {
    return questionOptionFifth;
  }

  public void setQuestionOptionFifth(@Nullable String questionOptionFifth) {
    this.questionOptionFifth = questionOptionFifth;
  }

  @Nullable
  public String getAnswer() {
    return answer;
  }

  public void setAnswer(@Nullable String answer) {
    this.answer = answer;
  }

  public ArrayList<String> getImageUrls() {
    return imageUrls;
  }

  public void setImageUrls(ArrayList<String> imageUrls) {
    this.imageUrls = imageUrls;
  }

  @Nullable
  private String _id;

  @Nullable
  public String getQuestionType() {
    return questionType;
  }

  public void setQuestionType(@Nullable String questionType) {
    this.questionType = questionType;
  }

  @Nullable
  private String questionType;
  @Nullable
  private String testtype;
  @Nullable
  private String questionOptionFifth;
  @Nullable
  private String answer;
  ArrayList<String> imageUrls;

  public ArrayList<String> getSubmittedAnswerList() {
    return submittedAnswerList;
  }

  public void setSubmittedAnswerList(ArrayList<String> submittedAnswerList) {
    this.submittedAnswerList = submittedAnswerList;
  }

  ArrayList<String> submittedAnswerList;

  @Nullable
  private boolean isAnswered = false;
  @Nullable
  private String title;

  public boolean isAnswered() {
    return isAnswered;
  }

  public void setAnswered(boolean answered) {
    isAnswered = answered;
  }

  @Nullable
  public String getUserAnswer() {
    return userAnswer;
  }

  public void setUserAnswer(@Nullable String userAnswer) {
    this.userAnswer = userAnswer;
  }

  @Nullable
  private String userAnswer = "";

  @Nullable
  public String getTestId() {
    return testId;
  }

  public void setTestId(@Nullable String testId) {
    this.testId = testId;
  }

  @Nullable
  private String testId = "";

  @Nullable
  public String[] getSubmittedAnsAnswer() {
    return submittedAnsAnswer;
  }

  public void setSubmittedAnsAnswer(@Nullable String[] submittedAnsAnswer) {
    this.submittedAnsAnswer = submittedAnsAnswer;
  }

  @Nullable
  private String[] submittedAnsAnswer;

  @Nullable
  private String questionTitle;
  @Nullable
  private String questionCount;
  @Nullable
  private String questionOptionFirst;
  @Nullable
  private String questionOptionSecond;
  @Nullable
  private String questionOptionThird;
  @Nullable
  private String questionOptionFourth;
  private String imageUrl;

  public String getQuestionCount() {
    return questionCount;
  }

  public void setQuestionCount(String questionCounnt) {
    this.questionCount = questionCounnt;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getQuestionTitle() {
    return questionTitle;
  }

  public void setQuestionTitle(String questionTitle) {
    this.questionTitle = questionTitle;
  }

  public String getQuestionOptionFirst() {
    return questionOptionFirst;
  }

  public void setQuestionOptionFirst(String questionOptionFirst) {
    this.questionOptionFirst = questionOptionFirst;
  }

  public String getQuestionOptionSecond() {
    return questionOptionSecond;
  }

  public void setQuestionOptionSecond(String questionOptionSecond) {
    this.questionOptionSecond = questionOptionSecond;
  }

  public String getQuestionOptionThird() {
    return questionOptionThird;
  }

  public void setQuestionOptionThird(String questionOptionThird) {
    this.questionOptionThird = questionOptionThird;
  }

  public String getQuestionOptionFourth() {
    return questionOptionFourth;
  }

  public void setQuestionOptionFourth(String questionOptionFourth) {
    this.questionOptionFourth = questionOptionFourth;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }
}
