package com.app.neetbook.Model.sidemenuFromContent;

import androidx.annotation.Nullable;

public class SubscriptionPointsHeader {

  @Nullable
  public String header;

  public SubscriptionPointsHeader(@Nullable String header) {
    this.header = header;
  }
}


