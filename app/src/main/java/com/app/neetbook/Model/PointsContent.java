package com.app.neetbook.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class PointsContent implements Serializable {
    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getPoint_name() {
        return point_name;
    }

    public void setPoint_name(String point_name) {
        this.point_name = point_name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getLong_name() {
        return long_name;
    }

    public void setLong_name(String long_name) {
        this.long_name = long_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCstatus() {
        return cstatus;
    }

    public void setCstatus(String cstatus) {
        this.cstatus = cstatus;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    String _id;
    String point_name;
    String short_name;
    String long_name;
    String status;
    String cstatus;
    String content;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    String url;

    ArrayList<PointsList> pontsTitleContentList;
    public ArrayList<PointsList> getPointsTitleContentList() {
        return pontsTitleContentList;
    }

    public void setPointsTitleContentList(ArrayList<PointsList> pintsTitleContentList) {
        this.pontsTitleContentList = pintsTitleContentList;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    String count;
}
