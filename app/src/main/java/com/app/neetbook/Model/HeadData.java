package com.app.neetbook.Model;

import java.io.Serializable;

public class HeadData implements Serializable {
    public String head_name;
    public String long_name;
    public String short_name;
    public String chapter_id;
    public String _id;
    public String position;
    public String isSH;
    public String superHeadingName;
    public String firsthead;
    public String url;
    public String summary;
    public String count;

}
