package com.app.neetbook.Model;

import com.app.neetbook.Interfaces.MCQChapterSection;

import java.util.ArrayList;

public class MCQHeaderModel implements MCQChapterSection {

    int section;

    public void setSection(int section) {
        this.section = section;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public void setChapterShortName(String chapterShortName) {
        this.chapterShortName = chapterShortName;
    }

    public void setChapterShorLongname(String chapterShorLongname) {
        this.chapterShorLongname = chapterShorLongname;
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public void setChapterImage(String chapterImage) {
        this.chapterImage = chapterImage;
    }

    public void setIsSuperHeading(String isSuperHeading) {
        this.isSuperHeading = isSuperHeading;
    }

    public void setSuperHeadName(String superHeadName) {
        this.superHeadName = superHeadName;
    }

    public void setHeadId(String headId) {
        this.headId = headId;
    }

    public void setHeadName(String headName) {
        this.headName = headName;
    }

    public void setHeadShortName(String headShortName) {
        this.headShortName = headShortName;
    }

    public void setHeadLongName(String headLongName) {
        this.headLongName = headLongName;
    }

    public void setMcqCount(String mcqCount) {
        McqCount = mcqCount;
    }

    String chapterName;
    String chapterShortName;
    String chapterShorLongname;
    String chapterId;
    String chapterImage;
    String isSuperHeading;
    String superHeadName;
    String headId;
    String headName;
    String headShortName;
    String headLongName;
    String McqCount;

    public MCQHeaderModel(int section) {
        this.section = section;
    }
    @Override
    public boolean isMCQChapterHeader() {
        return true;
    }

    @Override
    public String getName() {
        return chapterName;
    }

    @Override
    public int sectionPosition() {
        return section;
    }

    public void setHeadDetailsArrayList(ArrayList<HeadDetails> headDetailsArrayList) {
        this.headDetailsArrayList = headDetailsArrayList;
    }

    ArrayList<HeadDetails> headDetailsArrayList;
    @Override
    public ArrayList<HeadDetails> getDeadDeatails() {
        return headDetailsArrayList;
    }

    public void setMcqExamArrayList(ArrayList<McqExam> mcqExamArrayList) {
        this.mcqExamArrayList = mcqExamArrayList;
    }

    ArrayList<McqExam> mcqExamArrayList;
    @Override
    public ArrayList<McqExam> getMcqExamsList() {
        return mcqExamArrayList;
    }

    @Override
    public String getMCQChapterName() {
        return chapterName;
    }

    @Override
    public String getMCQShortName() {
        return chapterShortName;
    }

    @Override
    public String getMCQLongrName() {
        return chapterShorLongname;
    }

    @Override
    public String getMCQImage() {
        return chapterImage;
    }

    @Override
    public String getMCQCount() {
        return McqCount;
    }

    @Override
    public String getMCQChapterId() {
        return chapterId;
    }

    @Override
    public String getMCQIsSuperHeading() {
        return isSuperHeading;
    }

    @Override
    public String getMCQSuperHeadName() {
        return superHeadName;
    }

    @Override
    public String getMCQHeadId() {
        return headId;
    }

    @Override
    public String getMCQHeadName() {
        return headName;
    }

    @Override
    public String getMCQHeadShortName() {
        return headShortName;
    }

    @Override
    public String getMCQHeadLongName() {
        return headLongName;
    }
}
