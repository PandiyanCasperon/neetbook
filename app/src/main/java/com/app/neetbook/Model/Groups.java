package com.app.neetbook.Model;

import java.util.ArrayList;
import java.util.List;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Groups")
public class Groups
{
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "groups")
    private List<Groupslist> groupslists;

    public Groups(List<Groupslist> groupslists)
    {
        this.groupslists = groupslists;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Groupslist> getGroupslists() {
        return groupslists;
    }

    public void setGroupslists(List<Groupslist> groupslists) {
        this.groupslists = groupslists;
    }


}
