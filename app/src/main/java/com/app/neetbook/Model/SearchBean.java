package com.app.neetbook.Model;

public class SearchBean {
    public String getStrHeadId() {
        return strHeadId;
    }

    public void setStrHeadId(String strHeadId) {
        this.strHeadId = strHeadId;
    }

    public String getStrHeadShortName() {
        return strHeadShortName;
    }

    public void setStrHeadShortName(String strHeadShortName) {
        this.strHeadShortName = strHeadShortName;
    }

    public String getStrHeadLongName() {
        return strHeadLongName;
    }

    public void setStrHeadLongName(String strHeadLongName) {
        this.strHeadLongName = strHeadLongName;
    }

    public String getStrChaptShortName() {
        return strChaptShortName;
    }

    public void setStrChaptShortName(String strChaptShortName) {
        this.strChaptShortName = strChaptShortName;
    }

    public String getStrChaptLongName() {
        return strChaptLongName;
    }

    public void setStrChaptLongName(String strChaptLongName) {
        this.strChaptLongName = strChaptLongName;
    }

    public String getStrSubjectShortName() {
        return strSubjectShortName;
    }

    public void setStrSubjectShortName(String strSubjectShortName) {
        this.strSubjectShortName = strSubjectShortName;
    }

    public String getStrSubjectLongName() {
        return strSubjectLongName;
    }

    public void setStrSubjectLongName(String strSubjectLongName) {
        this.strSubjectLongName = strSubjectLongName;
    }

    public String getStrIsSubscribed() {
        return strIsSubscribed;
    }

    public void setStrIsSubscribed(String strIsSubscribed) {
        this.strIsSubscribed = strIsSubscribed;
    }

    public String getStrImage() {
        return strImage;
    }

    public void setStrImage(String strImage) {
        this.strImage = strImage;
    }

    public String getStrSubjectId() {
        return strSubjectId;
    }

    public void setStrSubjectId(String strSubjectId) {
        this.strSubjectId = strSubjectId;
    }

    public String getStrChaptId() {
        return strChaptId;
    }

    public void setStrChaptId(String strChaptId) {
        this.strChaptId = strChaptId;
    }

    String strSubjectId;
    String strChaptId;

    public String getStrMImage() {
        return strMImage;
    }

    public void setStrMImage(String strMImage) {
        this.strMImage = strMImage;
    }

    public String getStrTImage() {
        return strTImage;
    }

    public void setStrTImage(String strTImage) {
        this.strTImage = strTImage;
    }

    String strMImage;
    String strTImage;
    String strHeadId, strHeadShortName, strHeadLongName, strChaptShortName, strChaptLongName, strSubjectShortName, strSubjectLongName, strIsSubscribed, strImage;
}
