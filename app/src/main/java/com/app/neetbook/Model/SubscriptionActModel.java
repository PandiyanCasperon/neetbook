package com.app.neetbook.Model;

public class SubscriptionActModel {
    String chapter_name;
    String chapter_id;
    String head_id;
    String subject_id;
    String subject_name;
    String superHeading;
    String long_name;


    public SubscriptionActModel(String chapter_name, String chapter_id, String head_id, String subject_id, String subject_name, String superHeading, String long_name) {
        this.chapter_name = chapter_name;
        this.chapter_id = chapter_id;
        this.head_id = head_id;
        this.subject_id = subject_id;
        this.subject_name = subject_name;
        this.superHeading = superHeading;
        this.long_name = long_name;
    }

    public String getChapter_name() {
        return chapter_name;
    }

    public void setChapter_name(String chapter_name) {
        this.chapter_name = chapter_name;
    }

    public String getChapter_id() {
        return chapter_id;
    }

    public void setChapter_id(String chapter_id) {
        this.chapter_id = chapter_id;
    }

    public String getHead_id() {
        return head_id;
    }

    public void setHead_id(String head_id) {
        this.head_id = head_id;
    }

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public String getSuperHeading() {
        return superHeading;
    }

    public void setSuperHeading(String superHeading) {
        this.superHeading = superHeading;
    }

    public String getLong_name() {
        return long_name;
    }

    public void setLong_name(String long_name) {
        this.long_name = long_name;
    }

}
