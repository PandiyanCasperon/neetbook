package com.app.neetbook.Model.sidemenuFromContent;

import com.app.neetbook.Model.PointsContent;

import java.io.Serializable;
import java.security.SecureRandom;
import java.util.ArrayList;

public class CategoryPoints implements Serializable {
  public String _id;
  public String chapt_name;
  public String long_name;
  public String short_name;
  public String images;
  public ArrayList<PointsContent> pointsContentArrayList;
}
