package com.app.neetbook.Model;

public class TestActivityLastReadDetails {
    String subject_id;
    String chapter_id;

    public TestActivityLastReadDetails(String subject_id, String chapter_id, String chapter_name, String subject_name, String pending, String pendingTestId, String testId, String type, String no_of_question, String isFinished, String questionType, String strInstruction, String strLongName, String crt_ans_mark, String wrng_ans_mark) {
        this.subject_id = subject_id;
        this.chapter_id = chapter_id;
        this.chapter_name = chapter_name;
        this.subject_name = subject_name;
        this.pending = pending;
        this.pendingTestId = pendingTestId;
        this.testId = testId;
        this.type = type;
        this.no_of_question = no_of_question;
        this.isFinished = isFinished;
        this.questionType = questionType;
        this.strInstruction = strInstruction;
        this.strLongName = strLongName;
        this.crt_ans_mark = crt_ans_mark;
        this.wrng_ans_mark = wrng_ans_mark;
    }

    String chapter_name;

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public String getChapter_id() {
        return chapter_id;
    }

    public void setChapter_id(String chapter_id) {
        this.chapter_id = chapter_id;
    }

    public String getChapter_name() {
        return chapter_name;
    }

    public void setChapter_name(String chapter_name) {
        this.chapter_name = chapter_name;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public String getPendingTestId() {
        return pendingTestId;
    }

    public void setPendingTestId(String pendingTestId) {
        this.pendingTestId = pendingTestId;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNo_of_question() {
        return no_of_question;
    }

    public void setNo_of_question(String no_of_question) {
        this.no_of_question = no_of_question;
    }

    public String getIsFinished() {
        return isFinished;
    }

    public void setIsFinished(String isFinished) {
        this.isFinished = isFinished;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public String getStrInstruction() {
        return strInstruction;
    }

    public void setStrInstruction(String strInstruction) {
        this.strInstruction = strInstruction;
    }

    public String getStrLongName() {
        return strLongName;
    }

    public void setStrLongName(String strLongName) {
        this.strLongName = strLongName;
    }

    public String getCrt_ans_mark() {
        return crt_ans_mark;
    }

    public void setCrt_ans_mark(String crt_ans_mark) {
        this.crt_ans_mark = crt_ans_mark;
    }

    public String getWrng_ans_mark() {
        return wrng_ans_mark;
    }

    public void setWrng_ans_mark(String wrng_ans_mark) {
        this.wrng_ans_mark = wrng_ans_mark;
    }

    String subject_name;
    String pending;
    String pendingTestId;
    String testId;
    String type;
    String no_of_question;
    String isFinished;
    String questionType;
    String strInstruction;
    String strLongName;
    String crt_ans_mark;
    String wrng_ans_mark;

}
