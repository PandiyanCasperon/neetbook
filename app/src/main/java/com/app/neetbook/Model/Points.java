package com.app.neetbook.Model;

import java.util.ArrayList;

public class Points {
   String _id;
    String point_name;
    String short_name;
    String long_name;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    String url;
   ArrayList<ArticlePointsList> content;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getPoint_name() {
        return point_name;
    }

    public void setPoint_name(String point_name) {
        this.point_name = point_name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getLong_name() {
        return long_name;
    }

    public void setLong_name(String long_name) {
        this.long_name = long_name;
    }

    public ArrayList<ArticlePointsList> getContent() {
        return content;
    }

    public void setContent(ArrayList<ArticlePointsList> content) {
        this.content = content;
    }

    public Points(String _id, String point_name, String short_name, String long_name,String url ,ArrayList<ArticlePointsList> content) {
        this._id = _id;
        this.point_name = point_name;
        this.short_name = short_name;
        this.long_name = long_name;
        this.content = content;
        this.url=url;
    }
}
