package com.app.neetbook.Model;

public class SuperDetails {

    public String _id;
    public String supr_head_name;
    public String long_name;
    public String short_name;
    public String chapter;
    public String head;
    public String firsthead;
    public String summary;
    public String count;
}
