package com.app.neetbook.Model;

import java.io.Serializable;

public class PointsList implements Serializable {
    public String getStrBody() {
        return strBody;
    }

    public void setStrBody(String strBody) {
        this.strBody = strBody;
    }

    public String getStrTitile() {
        return strTitile;
    }

    public void setStrTitile(String strTitile) {
        this.strTitile = strTitile;
    }

    String strBody, strTitile;
}
