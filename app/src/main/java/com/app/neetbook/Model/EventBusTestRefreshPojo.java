package com.app.neetbook.Model;

public class EventBusTestRefreshPojo {
    String strRefresh;

    public String getStrRefresh() {
        return strRefresh;
    }

    public void setStrRefresh(String strRefresh) {
        this.strRefresh = strRefresh;
    }

    public String getStrChapterId() {
        return strChapterId;
    }

    public void setStrChapterId(String strChapterId) {
        this.strChapterId = strChapterId;
    }

    public String getStrLabelId() {
        return strLabelId;
    }

    public void setStrLabelId(String strLabelId) {
        this.strLabelId = strLabelId;
    }

    String strChapterId;
    String strLabelId;
}
