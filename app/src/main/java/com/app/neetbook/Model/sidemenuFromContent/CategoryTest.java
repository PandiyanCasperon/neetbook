package com.app.neetbook.Model.sidemenuFromContent;

import com.app.neetbook.Model.TestList;

import java.util.ArrayList;

public class CategoryTest {
  public String _id;
  public String chapt_name;
  public String long_name;
  public String short_name;
  public ArrayList<TestList> testArrayList;
  public String images;

  public String getChapterCount() {
    return chapterCount;
  }

  public void setChapterCount(String chapterCount) {
    this.chapterCount = chapterCount;
  }

  private String chapterCount;

  public String getIsFullTest() {
    return isFullTest;
  }

  public void setIsFullTest(String isFullTest) {
    this.isFullTest = isFullTest;
  }

  public String getFulltest_name() {
    return fulltest_name;
  }

  public void setFulltest_name(String fulltest_name) {
    this.fulltest_name = fulltest_name;
  }

  private String isFullTest;
  private String fulltest_name;

}
