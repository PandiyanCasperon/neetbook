package com.app.neetbook.Model;

public class TestList {

    public String _id;
    public String qstn_type;
    public String crt_ans_mark;
    public String wrng_ans_mark;
    public String test_name;
    public String short_name;
    public String long_name;
    public String no_of_qstn;
    public String no_of_choice;
    public String instruction;
    public String type;
    public String is_finished;
    public String[] submittedAnswer;
}
