package com.app.neetbook.Model;

import com.app.neetbook.Interfaces.ArticleHeaderSestion;

import java.util.ArrayList;

public class ArticleHeaderModel  implements ArticleHeaderSestion{


    public void set_expanded(Boolean status) {
        this.is_expaned = status;
    }

    public void setHeaderName(String header) {
        this.headerName = header;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setIsSuperHeading(String isSuperHeading) {
        this.isSuperHeading = isSuperHeading;
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public void setHeadDetails(ArrayList<HeadDetails> headDetails) {
        this.headDetails = headDetails;
    }

    public void setMcqExams(ArrayList<McqExam> mcqExams) {
        this.mcqExams = mcqExams;
    }

    String headerName;
    String shortName;
    String longName;
    String id;
    String image;
    String isSuperHeading;
    String chapterId;
    String head;
    private Boolean is_expaned=false;

    public void setCount(String count) {
        this.count = count;
    }

    String count;
    ArrayList<HeadDetails> headDetails;
    ArrayList<McqExam> mcqExams;
    private int section;

    public ArticleHeaderModel(int section) {
        this.section = section;
    }
    @Override
    public boolean isHeader() {
        return true;
    }

    @Override
    public String getName() {
        return headerName;
    }

    @Override
    public String getShortName() {
        return shortName;
    }

    @Override
    public String getLongName() {
        return longName;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getImage() {
        return image;
    }

    @Override
    public ArrayList<HeadDetails> getHeadList() {
        return headDetails;
    }

    @Override
    public ArrayList<McqExam> getExamArrayList() {
        return mcqExams;
    }

    @Override
    public String getIsSuperHeading() {
        return isSuperHeading;
    }

    @Override
    public String getHead() {
        return null;
    }

    @Override
    public String getFirsthead() {
        return head;
    }

    @Override
    public String getChapterId() {
        return chapterId;
    }

    @Override
    public String getCount() {
        return count;
    }


    @Override
    public int sectionPosition() {
        return section;
    }

    @Override
    public String getHeaderName() {
        return headerName;
    }

    @Override
    public Boolean is_expanded() {
        return is_expaned;
    }


}
