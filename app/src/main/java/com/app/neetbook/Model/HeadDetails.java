package com.app.neetbook.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class HeadDetails implements Serializable {

    public ArrayList<HeadData> headData;
    public SuperDetails superDetails;
    public String isSH;
}
