package com.app.neetbook.TAbAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.app.TabPojo.TabItemPojo;
import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.sidemenuFromContent.CategoryArticle;
import com.app.neetbook.R;

import com.app.neetbook.View.Fragments.subscription.Tab1Fragment;
import com.app.neetbook.View.SideMenu.ArticleFragment;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class TabFragmentChildRecyclerAdapter extends RecyclerView.Adapter<TabFragmentChildRecyclerAdapter.ViewHolder> implements TabFragmentChildRecyclerListItemAdapter.OnSuperChildHeadingClickListener {

    private Context context;

    private ArrayList<CategoryArticle> articleList;
    private ItemClickListener clicklistener;
    private TabFragmentChildRecyclerListItemAdapter child_adapter;
    private int selected_parent_position;
    private String strSubId = "",strSubName = "";
    OnHeadingClickListener onHeadingClickListener;

    public interface OnHeadingClickListener
    {
        void onHeadingClicked(int parentPosition, int childPosition, String headId);
    }
    // RecyclerView recyclerView;
    public TabFragmentChildRecyclerAdapter(Context context, ArrayList<CategoryArticle> headDetailsArrayList, ArticleFragment tab1Fragment,
                                           int selected_parent_position,String strSubId,String strSubName) {
        this.context=context;
        this.articleList = headDetailsArrayList;
        this.selected_parent_position = selected_parent_position;
        this.strSubName = strSubName;
        this.strSubId = strSubId;
       // clicklistener=tab1Fragment;
        onHeadingClickListener=tab1Fragment;
    }

    @Override
    public TabFragmentChildRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.fragment_child_recycler_layout, parent, false);
        TabFragmentChildRecyclerAdapter.ViewHolder viewHolder = new TabFragmentChildRecyclerAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TabFragmentChildRecyclerAdapter.ViewHolder holder, final int position) {

        CategoryArticle myListData = articleList.get(position);


        SetAdapter(holder.child_recycler_view,myListData.headDetailsArrayList,position);

        if(selected_parent_position==position){

            holder.child_layout.setVisibility(View.VISIBLE);
            holder.dummy_view.setVisibility(View.GONE);

            if(selected_parent_position==0){

                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.child_layout.getLayoutParams();
                params.setMargins(0, 10, 0, 0); //substitute parameters for left, top, right, bottom
                holder.child_layout.setLayoutParams(params);

            } else {

                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.child_layout.getLayoutParams();
                params.setMargins(0, 10, 0, 0); //substitute parameters for left, top, right, bottom
                holder.child_layout.setLayoutParams(params);

            }


        } else {

            holder.child_layout.setVisibility(View.GONE);
            holder.dummy_view.setVisibility(View.VISIBLE);
        }

    }

    private void SetAdapter(RecyclerView child_recycler_view, ArrayList<HeadDetails> headDetailsArrayList,int position){

        child_recycler_view.setLayoutManager(new LinearLayoutManager(context,
                RecyclerView.VERTICAL, false));

        child_adapter=new TabFragmentChildRecyclerListItemAdapter(context,headDetailsArrayList,articleList.get(position).long_name,articleList.get(position)._id,strSubId,strSubName,this);
        child_recycler_view.setAdapter(child_adapter);

    }


    @Override
    public int getItemCount() {
        return articleList.size();
    }

    @Override
    public void onSuperChildClicked(int position,String headId) {
        onHeadingClickListener.onHeadingClicked(selected_parent_position,position,headId);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public RecyclerView child_recycler_view;
        public LinearLayout child_layout;
        public LinearLayout dummy_view;


        public ViewHolder(View itemView) {
            super(itemView);

            this.child_recycler_view=(RecyclerView)itemView.findViewById(R.id.child_recycler_view);
            this.child_layout=(LinearLayout)itemView.findViewById(R.id.child_layout);
            this.dummy_view=(LinearLayout)itemView.findViewById(R.id.dummy_view);

        }
    }
}