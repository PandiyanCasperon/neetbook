package com.app.neetbook.TAbAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.Model.sidemenuFromContent.CategoryPoints;
import com.app.neetbook.Model.sidemenuFromContent.CategoryTest;
import com.app.neetbook.R;
import com.app.neetbook.View.SideMenu.TestFragment;
import com.app.neetbook.serviceRequest.IConstant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class TabTestmainAdapter extends RecyclerView.Adapter<TabTestmainAdapter.ViewHolder>{

    private Context context;
    private ArrayList<CategoryTest> itemList;
    private ItemClickListener clicklistener;
    private TabFragmentChildRecyclerAdapter child_adapter;
    private int currentSelectedPosition = 0;

    // RecyclerView recyclerView;
    public TabTestmainAdapter(Context context, ArrayList<CategoryTest> itemList, TestFragment tab1Fragment) {
        this.context=context;
        this.itemList = itemList;
        clicklistener=tab1Fragment;
        currentSelectedPosition = 0;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.tab_test_main_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        CategoryTest categoryTest = itemList.get(position);

        holder.question_tv.setText(categoryTest.long_name);
        if(categoryTest.getIsFullTest().equals("Yes")) {
            holder.question_tv.setVisibility(View.GONE);
            holder.txtFullTestName.setVisibility(View.VISIBLE);
            holder.txtTestFCount.setVisibility(View.VISIBLE);
            holder.txtFullTestName.setText(categoryTest.getFulltest_name());
            holder.txtTestFCount.setText("FT");
        }else
        {
            holder.question_tv.setText(categoryTest.long_name);
            holder.question_tv.setVisibility(View.VISIBLE);
            holder.txtFullTestName.setVisibility(View.GONE);
            holder.txtTestFCount.setVisibility(View.GONE);
        }
        if(currentSelectedPosition == position){
            holder.main_view.setBackground(ContextCompat.getDrawable(context, R.drawable.tab_parent_recyclerview_item_bg));
        } else {
            holder.main_view.setBackground(ContextCompat.getDrawable(context, R.drawable.tab_parent_item_unselect_bg));
        }
        holder.main_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentSelectedPosition = position;
                clicklistener.onParentItemClick(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override    public int getItemCount() {
        return itemList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView question_tv,txtTestFCount,txtFullTestName;
        public LinearLayout main_view;
        public RecyclerView child_recycler_view;
        public LinearLayout child_layout;
        public LinearLayout parent_main_layout;

        public ViewHolder(View itemView) {
            super(itemView);

            this.question_tv = (TextView) itemView.findViewById(R.id.question_tv);
            this.txtTestFCount = (TextView) itemView.findViewById(R.id.txtTestFCount);
            this.txtFullTestName = (TextView) itemView.findViewById(R.id.txtFullTestName);
            this.main_view = (LinearLayout) itemView.findViewById(R.id.main_view);
            this.child_recycler_view=(RecyclerView)itemView.findViewById(R.id.child_recycler_view);
            this.child_layout=(LinearLayout)itemView.findViewById(R.id.child_layout);
            this.parent_main_layout=(LinearLayout)itemView.findViewById(R.id.parent_main_layout);

        }
    }
}
