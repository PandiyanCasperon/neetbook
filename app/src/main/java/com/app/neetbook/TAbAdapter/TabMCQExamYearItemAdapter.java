package com.app.neetbook.TAbAdapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.neetbook.Interfaces.OnSuperChildHeadingClickListener;
import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.McqExam;
import com.app.neetbook.R;
import com.app.neetbook.View.SideMenu.SubscriptionArticleActivity;
import com.app.neetbook.View.SideMenu.SubscriptionDetailActivity;
import com.app.neetbook.View.SideMenu.TabMainActivity;

import java.util.ArrayList;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

public class TabMCQExamYearItemAdapter  extends RecyclerView.Adapter<TabMCQExamYearItemAdapter.ViewHolder>{

    private Context context;
    private ArrayList<McqExam> headDetailsArrayList;
    private int chileSeletedPosition = -1;
    private int superChileSeletedPosition = -1;
    Typeface tfItalicRegular,tfMedium;
    LinearLayout.LayoutParams params;
    OnSuperChildHeadingClickListener onSuperChildHeadingClickListener;

    private String strSubId = "",strSubName = "",strChapterName = "",strChapterId = "";
    // RecyclerView recyclerView;
    public TabMCQExamYearItemAdapter(Context context, ArrayList<McqExam> headDetailsArrayList,String strChapterName,String strChapterId,String strSubId,String strSubName,TAbMCQExamYearAdapter tabFragmentChildRecyclerAdapter) {
        this.context=context;
        this.headDetailsArrayList = headDetailsArrayList;
        this.strChapterName = strChapterName;
        this.strChapterId = strChapterId;
        this.strSubName = strSubName;
        this.strSubId = strSubId;
        this.onSuperChildHeadingClickListener = tabFragmentChildRecyclerAdapter;
        chileSeletedPosition = -1;
        superChileSeletedPosition = -1;
        tfItalicRegular  = Typeface.createFromAsset(this.context.getAssets(), "fonts/proximanova_regitalic.otf");
        tfMedium  = Typeface.createFromAsset(this.context.getAssets(), "fonts/ProximaNovaSemibold.otf");
        params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
    }
    @Override
    public TabMCQExamYearItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.mcq_chiled_item, parent, false);
        TabMCQExamYearItemAdapter.ViewHolder viewHolder = new TabMCQExamYearItemAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TabMCQExamYearItemAdapter.ViewHolder holder, final int i) {



        final McqExam headDetails = headDetailsArrayList.get(i);

        Log.e("NAME",headDetails.getLong_name());
        holder.imgArticleChildHead.setVisibility(i== chileSeletedPosition?View.VISIBLE:View.INVISIBLE);
        holder.txt_article_child_head.setTextColor(i== chileSeletedPosition?context.getResources().getColor(R.color.mcq_blue):context.getResources().getColor(R.color.black));
        holder.txt_article_child_head.setText(headDetails.getLong_name());
        final int finalI = i;
        holder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chileSeletedPosition = finalI;
                notifyDataSetChanged();
                TabMainActivity.isReadPage = true;
                SubscriptionDetailActivity.mcqExamArrayList = headDetailsArrayList;
                onSuperChildHeadingClickListener.onSuperChildClicked(finalI,headDetails.get_id());
            }
        });

    }


    @Override
    public int getItemCount() {
        return headDetailsArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgArticleChildHead;
        TextView txt_article_child_head ;
        LinearLayoutCompat llChildOfChild;
        LinearLayout llParent;
        public ViewHolder(View itemView) {
            super(itemView);

            this.imgArticleChildHead =  itemView.findViewById(R.id.imgArticleChildHead);
            this.txt_article_child_head =  itemView.findViewById(R.id.txt_article_child_head);
            this.llChildOfChild =  itemView.findViewById(R.id.llChildOfChild);
            this.llParent =  itemView.findViewById(R.id.llParent);


        }
    }
}
