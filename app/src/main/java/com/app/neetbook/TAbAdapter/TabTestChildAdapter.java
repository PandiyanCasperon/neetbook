package com.app.neetbook.TAbAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.Interfaces.OnHeadingClickListener;
import com.app.neetbook.Interfaces.OnSuperChildHeadingClickListener;
import com.app.neetbook.Model.McqExam;
import com.app.neetbook.Model.TestList;
import com.app.neetbook.Model.sidemenuFromContent.CategoryArticle;
import com.app.neetbook.Model.sidemenuFromContent.CategoryTest;
import com.app.neetbook.R;
import com.app.neetbook.View.SideMenu.McqFragment;
import com.app.neetbook.View.SideMenu.TestFragment;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class TabTestChildAdapter extends RecyclerView.Adapter<TabTestChildAdapter.ViewHolder> implements OnSuperChildHeadingClickListener {

    private Context context;

    private ArrayList<CategoryTest> itemList;
    private ItemClickListener clicklistener;
    private TabTestChildItemAdapter child_adapter;
    private int selected_parent_position;
    private String strSubId = "",strSubName = "";
    OnHeadingClickListener onHeadingClickListener;
    private int loadingType = 1;

    // RecyclerView recyclerView;
    public TabTestChildAdapter(Context context, ArrayList<CategoryTest> itemList, TestFragment tab1Fragment,
                                 int selected_parent_position, String strSubId, String strSubName) {
        this.context=context;
        this.itemList = itemList;
        this.selected_parent_position = selected_parent_position;
        this.strSubName = strSubName;
        this.strSubId = strSubId;
        this.onHeadingClickListener = tab1Fragment;
        // clicklistener=tab1Fragment;
    }

    @Override
    public TabTestChildAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.fragment_child_recycler_layout, parent, false);
        TabTestChildAdapter.ViewHolder viewHolder = new TabTestChildAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TabTestChildAdapter.ViewHolder holder, final int position) {

        CategoryTest myListData = itemList.get(position);


        SetAdapter(holder.child_recycler_view,myListData.testArrayList,position);

        if(selected_parent_position==position){

            holder.child_layout.setVisibility(View.VISIBLE);
            holder.dummy_view.setVisibility(View.GONE);

            if(selected_parent_position==0){

                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.child_layout.getLayoutParams();
                params.setMargins(0, 10, 0, 0); //substitute parameters for left, top, right, bottom
                holder.child_layout.setLayoutParams(params);

            } else{

                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.child_layout.getLayoutParams();
                params.setMargins(0, 10, 0, 0); //substitute parameters for left, top, right, bottom
                holder.child_layout.setLayoutParams(params);
            }



        } else {

            holder.child_layout.setVisibility(View.GONE);
            holder.dummy_view.setVisibility(View.VISIBLE);
        }

    }

    private void SetAdapter(RecyclerView child_recycler_view, ArrayList<TestList> testArrayList, int position){

        child_recycler_view.setLayoutManager(new LinearLayoutManager(context,
                RecyclerView.VERTICAL, false));

        child_adapter=new TabTestChildItemAdapter(context,testArrayList,itemList.get(position).long_name,itemList.get(position)._id,strSubId,strSubName,this);
        child_recycler_view.setAdapter(child_adapter);

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public void onSuperChildClicked(int position,String headId) {
        onHeadingClickListener.onHeadingClicked(selected_parent_position,position,headId);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public RecyclerView child_recycler_view;
        public LinearLayout child_layout;
        public LinearLayout dummy_view;


        public ViewHolder(View itemView) {
            super(itemView);

            this.child_recycler_view=(RecyclerView)itemView.findViewById(R.id.child_recycler_view);
            this.child_layout=(LinearLayout)itemView.findViewById(R.id.child_layout);
            this.dummy_view=(LinearLayout)itemView.findViewById(R.id.dummy_view);

        }
    }
}
