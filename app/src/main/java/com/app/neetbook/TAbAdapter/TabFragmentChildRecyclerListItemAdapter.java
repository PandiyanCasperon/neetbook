package com.app.neetbook.TAbAdapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.R;
import com.app.neetbook.View.ScrollingActivity;
import com.app.neetbook.View.SideMenu.SubscriptionArticleActivity;
import com.app.neetbook.View.SideMenu.TabMainActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

public class TabFragmentChildRecyclerListItemAdapter extends RecyclerView.Adapter<TabFragmentChildRecyclerListItemAdapter.ViewHolder>{

    private Context context;
    private ArrayList<HeadDetails> headDetailsArrayList;
    private int chileSeletedPosition = -1;
    private int superChileSeletedPosition = -1;
    Typeface tfItalicRegular,tfMedium;
    LinearLayout.LayoutParams params;
    OnSuperChildHeadingClickListener onSuperChildHeadingClickListener;
    public interface OnSuperChildHeadingClickListener
    {
        void onSuperChildClicked(int position, String headId);
    }
    private String strSubId = "",strSubName = "",strChapterName = "",strChapterId = "";
    // RecyclerView recyclerView;
    public TabFragmentChildRecyclerListItemAdapter(Context context, ArrayList<HeadDetails> headDetailsArrayList,String strChapterName,String strChapterId,String strSubId,String strSubName,TabFragmentChildRecyclerAdapter tabFragmentChildRecyclerAdapter) {
        this.context=context;
        this.headDetailsArrayList = headDetailsArrayList;
        this.strChapterName = strChapterName;
        this.strChapterId = strChapterId;
        this.strSubName = strSubName;
        this.strSubId = strSubId;
        this.onSuperChildHeadingClickListener = tabFragmentChildRecyclerAdapter;
        chileSeletedPosition = -1;
        superChileSeletedPosition = -1;
        tfItalicRegular  = Typeface.createFromAsset(this.context.getAssets(), "fonts/proximanova_regitalic.otf");
        tfMedium  = Typeface.createFromAsset(this.context.getAssets(), "fonts/ProximaNovaSemibold.otf");
        params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
    }
    @Override
    public TabFragmentChildRecyclerListItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.article_chile_head, parent, false);
        TabFragmentChildRecyclerListItemAdapter.ViewHolder viewHolder = new TabFragmentChildRecyclerListItemAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TabFragmentChildRecyclerListItemAdapter.ViewHolder holder, final int position) {


        final HeadDetails headDetails = headDetailsArrayList.get(position);

        holder.imgArticleChildHead.setVisibility(headDetails.isSH.equals("0") && superChileSeletedPosition == -1 && position == chileSeletedPosition ? View.VISIBLE : View.INVISIBLE);
        holder.txt_article_child_head.setTextColor(headDetails.isSH.equals("0") && superChileSeletedPosition == -1 && position == chileSeletedPosition ? context.getResources().getColor(R.color.artical_pink) : context.getResources().getColor(R.color.black));
        final int finalI = position;
        holder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chileSeletedPosition = finalI;
                superChileSeletedPosition = -1;
                notifyDataSetChanged();
                if(headDetailsArrayList.get(finalI).isSH.equals("0")) {
                    ScrollingActivity.headDataArrayList = headDetailsArrayList;
                    onSuperChildHeadingClickListener.onSuperChildClicked(position,headDetails.headData.get(0)._id);
                    /*TabMainActivity.isReadPage = true;
                    SubscriptionArticleActivity.headDataArrayList = headDetailsArrayList;
                    context.startActivity(new Intent(context, SubscriptionArticleActivity.class).putExtra("chapter_name",strChapterName).putExtra("chapter_id", strChapterId).putExtra("head_id", headDetails.headData.get(0)._id).putExtra("subject_id", strSubId).putExtra("subject_name", strSubName).putExtra("chapter_name", strChapterName).putExtra("superHeading", "No"));*/
                }
            }
        });

        if (headDetails.isSH.equals("0")) {
            holder.txt_article_child_head.setText(headDetails.headData.get(0).long_name);
            holder.llChildOfChild.setVisibility(View.GONE);

            params.setMargins(0, 0, 0, 0);
            holder.txt_article_child_head.setLayoutParams(params);
            //txt_article_child_head.setTypeface(tfMedium);
        } else {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(25, 0, 0, 0);
            holder.txt_article_child_head.setLayoutParams(params);
            //txt_article_child_head.setTypeface(tfItalicRegular);
            holder.txt_article_child_head.setText(headDetails.superDetails.long_name);
            holder.llChildOfChild.setVisibility(View.VISIBLE);

            if (headDetails.headData.size() > 0) {
                holder.llChildOfChild.removeAllViews();
                for (int j = 0; j < headDetails.headData.size(); j++) {
                    View childOfChildView = LayoutInflater.from(context).inflate(R.layout.article_super_headings,null);
                    TextView txtArticleSuperHeading = childOfChildView.findViewById(R.id.txtArticleSuperHeading);
                    ImageView imgArticleSuperChild = childOfChildView.findViewById(R.id.imgArticleSuperChild);
                    imgArticleSuperChild.setVisibility(chileSeletedPosition == position && j==superChileSeletedPosition?View.VISIBLE:View.INVISIBLE);
                    txtArticleSuperHeading.setTextColor(chileSeletedPosition == position && j==superChileSeletedPosition?context.getResources().getColor(R.color.artical_pink): Color.BLACK);
                    txtArticleSuperHeading.setText(headDetails.headData.get(j).long_name);
                    final int finalJ = j;
                    childOfChildView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            TabMainActivity.isReadPage = true;
                            superChileSeletedPosition = finalJ;
                            chileSeletedPosition = finalI;
                            notifyDataSetChanged();
                            ScrollingActivity.headDataArrayList = headDetailsArrayList;
                            onSuperChildHeadingClickListener.onSuperChildClicked(position,headDetails.headData.get(finalJ)._id);
                            //context.startActivity(new Intent(context, SubscriptionArticleActivity.class).putExtra("chapter_id",strChapterId).putExtra("chapter_name",strChapterName).putExtra("head_id",headDetails.headData.get(finalJ)._id).putExtra("subject_id",strSubId).putExtra("subject_name",strSubName).putExtra("superHeading", headDetails.superDetails.short_name));
                        }
                    });
                    holder.llChildOfChild.addView(childOfChildView);
                }
            }
        }




    }


    @Override
    public int getItemCount() {
        return headDetailsArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgArticleChildHead;
        TextView txt_article_child_head ;
        LinearLayoutCompat llChildOfChild;
        LinearLayout llParent;
        public ViewHolder(View itemView) {
            super(itemView);

            this.imgArticleChildHead =  itemView.findViewById(R.id.imgArticleChildHead);
            this.txt_article_child_head =  itemView.findViewById(R.id.txt_article_child_head);
            this.llChildOfChild =  itemView.findViewById(R.id.llChildOfChild);
            this.llParent =  itemView.findViewById(R.id.llParent);


        }
    }
}
