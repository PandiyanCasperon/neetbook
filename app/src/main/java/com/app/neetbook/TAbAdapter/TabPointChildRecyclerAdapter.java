package com.app.neetbook.TAbAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.Interfaces.OnSuperChildHeadingClickListener;
import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.PointsContent;
import com.app.neetbook.Model.sidemenuFromContent.CategoryArticle;
import com.app.neetbook.Model.sidemenuFromContent.CategoryPoints;
import com.app.neetbook.R;
import com.app.neetbook.View.SideMenu.ArticleFragment;
import com.app.neetbook.View.SideMenu.PointsFragment;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class TabPointChildRecyclerAdapter  extends RecyclerView.Adapter<TabPointChildRecyclerAdapter.ViewHolder> implements OnSuperChildHeadingClickListener {

    private Context context;

    private ArrayList<CategoryPoints> mList;
    private ItemClickListener clicklistener;
    private TabPointsChildItemAdapter child_adapter;
    private int selected_parent_position;
    private String strSubId = "",strSubName = "";
    OnHeadingClickListener onHeadingClickListener;
    public interface OnHeadingClickListener
    {
        void onHeadingClicked(int parentPosition, int childPosition, String headId);
    }
    // RecyclerView recyclerView;
    public TabPointChildRecyclerAdapter(Context context, ArrayList<CategoryPoints> mList, PointsFragment tab1Fragment,
                                           int selected_parent_position,String strSubId,String strSubName) {
        this.context=context;
        this.mList = mList;
        this.selected_parent_position = selected_parent_position;
        this.strSubName = strSubName;
        this.strSubId = strSubId;
        this.onHeadingClickListener = tab1Fragment;
        // clicklistener=tab1Fragment;
    }

    @Override
    public TabPointChildRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.fragment_child_recycler_layout, parent, false);
        TabPointChildRecyclerAdapter.ViewHolder viewHolder = new TabPointChildRecyclerAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TabPointChildRecyclerAdapter.ViewHolder holder, final int position) {

        CategoryPoints myListData = mList.get(position);


        SetAdapter(holder.child_recycler_view,myListData.pointsContentArrayList,position);

        if(selected_parent_position==position){

            holder.child_layout.setVisibility(View.VISIBLE);
            holder.dummy_view.setVisibility(View.GONE);

            if(selected_parent_position==0){

                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.child_layout.getLayoutParams();
                params.setMargins(0, 10, 0, 0); //substitute parameters for left, top, right, bottom
                holder.child_layout.setLayoutParams(params);

            } else{

                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.child_layout.getLayoutParams();
                params.setMargins(0, 10, 0, 0); //substitute parameters for left, top, right, bottom
                holder.child_layout.setLayoutParams(params);
            }



        } else {

            holder.child_layout.setVisibility(View.GONE);
            holder.dummy_view.setVisibility(View.VISIBLE);
        }

    }

    private void SetAdapter(RecyclerView child_recycler_view, ArrayList<PointsContent> pointsContentArrayList, int position){

        child_recycler_view.setLayoutManager(new LinearLayoutManager(context,
                RecyclerView.VERTICAL, false));

        child_adapter=new TabPointsChildItemAdapter(context,pointsContentArrayList,mList.get(position).long_name,mList.get(position)._id,strSubId,strSubName,this);
        child_recycler_view.setAdapter(child_adapter);

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onSuperChildClicked(int position,String headId) {
        onHeadingClickListener.onHeadingClicked(selected_parent_position,position,headId);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public RecyclerView child_recycler_view;
        public LinearLayout child_layout;
        public LinearLayout dummy_view;


        public ViewHolder(View itemView) {
            super(itemView);

            this.child_recycler_view=(RecyclerView)itemView.findViewById(R.id.child_recycler_view);
            this.child_layout=(LinearLayout)itemView.findViewById(R.id.child_layout);
            this.dummy_view=(LinearLayout)itemView.findViewById(R.id.dummy_view);

        }
    }

}
