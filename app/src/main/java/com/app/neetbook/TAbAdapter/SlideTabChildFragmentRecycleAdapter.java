package com.app.neetbook.TAbAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.TabPojo.SlideTapChildFragmentRecyclerPojo;
import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.R;

import com.app.neetbook.View.SideMenu.TabMainActivity;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

public class SlideTabChildFragmentRecycleAdapter extends RecyclerView.Adapter<SlideTabChildFragmentRecycleAdapter.ViewHolder>{

    private Context context;
    private ArrayList<SlideTapChildFragmentRecyclerPojo> listdata;
    private ItemClickListener activity;

    // RecyclerView recyclerView;
    public SlideTabChildFragmentRecycleAdapter(Context context, ArrayList<SlideTapChildFragmentRecyclerPojo> listdata, TabMainActivity activity) {
        this.context=context;
        this.listdata = listdata;
        this.context=activity;
    }
    @Override
    public SlideTabChildFragmentRecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.slidetab_child_fragment_recycle_item_layout, parent, false);
        SlideTabChildFragmentRecycleAdapter.ViewHolder viewHolder = new SlideTabChildFragmentRecycleAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SlideTabChildFragmentRecycleAdapter.ViewHolder holder, final int position) {

        SlideTapChildFragmentRecyclerPojo myListData = listdata.get(position);

        holder.question_title_tv.setText(myListData.getQuestion_Title());


        holder.gallery_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                activity.onParentItemClick(position);
            }
        });

    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView question_no_tv;
        public TextView question_title_tv;
        public TextView A_type_answer;
        public TextView B_type_answer;
        public TextView C_type_answer;
        public TextView D_type_answer;
        public ImageView gallery_image;
        public ImageView alert_image;

        public ViewHolder(View itemView) {
            super(itemView);

            this.question_no_tv = (TextView) itemView.findViewById(R.id.question_no_tv);
            this.question_title_tv = (TextView) itemView.findViewById(R.id.question_title_tv);
            this.A_type_answer = (TextView) itemView.findViewById(R.id.A_type_answer);
            this.B_type_answer = (TextView) itemView.findViewById(R.id.B_type_answer);
            this.C_type_answer = (TextView) itemView.findViewById(R.id.C_type_answer);
            this.D_type_answer = (TextView) itemView.findViewById(R.id.D_type_answer);
            this.gallery_image=(ImageView)itemView.findViewById(R.id.gallery_image);
            this.alert_image=(ImageView)itemView.findViewById(R.id.alert_image);

        }
    }
}