package com.app.neetbook.TAbAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.Model.sidemenuFromContent.CategoryArticle;
import com.app.neetbook.Model.sidemenuFromContent.CategoryPoints;
import com.app.neetbook.R;
import com.app.neetbook.View.SideMenu.ArticleFragment;
import com.app.neetbook.View.SideMenu.PointsFragment;

import java.util.ArrayList;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class TabPointMainAdapter  extends RecyclerView.Adapter<TabPointMainAdapter.ViewHolder>{

    private Context context;
    private ArrayList<CategoryPoints> mList;
    private ItemClickListener clicklistener;
    private TabFragmentChildRecyclerAdapter child_adapter;
    private int currentSelectedPosition = 0;

    // RecyclerView recyclerView;
    public TabPointMainAdapter(Context context, ArrayList<CategoryPoints> mList, PointsFragment tab1Fragment) {
        this.context=context;
        this.mList = mList;
        clicklistener=tab1Fragment;
        currentSelectedPosition = 0;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.tabrecyclerview_list_item_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        CategoryPoints myListData = mList.get(position);

        holder.question_tv.setText(myListData.long_name);


        if(currentSelectedPosition == position){
            holder.main_view.setBackground(ContextCompat.getDrawable(context, R.drawable.tab_parent_recyclerview_item_bg));
        } else {
            holder.main_view.setBackground(ContextCompat.getDrawable(context, R.drawable.tab_parent_item_unselect_bg));
        }

        holder.main_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentSelectedPosition = position;
                clicklistener.onParentItemClick(position);
                notifyDataSetChanged();
            }
        });



    }



    @Override    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView question_tv;
        public LinearLayout main_view;
        public RecyclerView child_recycler_view;
        public LinearLayout child_layout;
        public LinearLayout parent_main_layout;

        public ViewHolder(View itemView) {
            super(itemView);

            this.question_tv = (TextView) itemView.findViewById(R.id.question_tv);
            this.main_view = (LinearLayout) itemView.findViewById(R.id.main_view);
            this.child_recycler_view=(RecyclerView)itemView.findViewById(R.id.child_recycler_view);
            this.child_layout=(LinearLayout)itemView.findViewById(R.id.child_layout);
            this.parent_main_layout=(LinearLayout)itemView.findViewById(R.id.parent_main_layout);

        }
    }
}
