package com.app.neetbook.TAbAdapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.neetbook.Interfaces.OnSuperChildHeadingClickListener;
import com.app.neetbook.Model.PointsContent;
import com.app.neetbook.Model.TestList;
import com.app.neetbook.Model.sidemenuFromContent.CategoryTest;
import com.app.neetbook.R;
import com.app.neetbook.View.SideMenu.SubscriptionPointsActivity;
import com.app.neetbook.View.SideMenu.SubscriptionTestActivity;
import com.app.neetbook.View.SideMenu.TabMainActivity;

import java.util.ArrayList;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

public class TabTestChildItemAdapter extends RecyclerView.Adapter<TabTestChildItemAdapter.ViewHolder>{

    private Context context;
    private ArrayList<TestList> testArrayList;
    private int chileSeletedPosition = -1;
    private int superChileSeletedPosition = -1;
    Typeface tfItalicRegular,tfMedium;
    LinearLayout.LayoutParams params;
    OnSuperChildHeadingClickListener onSuperChildHeadingClickListener;
    private String strSubId = "",strSubName = "",strChapterName = "",strChapterId = "";
    // RecyclerView recyclerView;
    public TabTestChildItemAdapter(Context context, ArrayList<TestList> testArrayList, String strChapterName, String strChapterId, String strSubId, String strSubName, TabTestChildAdapter tabFragmentChildRecyclerAdapter) {
        this.context=context;
        this.testArrayList = testArrayList;
        this.strChapterName = strChapterName;
        this.strChapterId = strChapterId;
        this.strSubName = strSubName;
        this.strSubId = strSubId;
        chileSeletedPosition = -1;
        superChileSeletedPosition = -1;
        onSuperChildHeadingClickListener = tabFragmentChildRecyclerAdapter;
        tfItalicRegular  = Typeface.createFromAsset(this.context.getAssets(), "fonts/proximanova_regitalic.otf");
        tfMedium  = Typeface.createFromAsset(this.context.getAssets(), "fonts/ProximaNovaSemibold.otf");
        params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
    }
    @Override
    public TabTestChildItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.test_chile_item, parent, false);
        TabTestChildItemAdapter.ViewHolder viewHolder = new TabTestChildItemAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TabTestChildItemAdapter.ViewHolder holder, final int i) {


        final TestList headDetails = testArrayList.get(i);

        holder.imgArticleChildHead.setVisibility(i== chileSeletedPosition? View.VISIBLE:View.INVISIBLE);
        holder.txt_article_child_head.setTextColor(i== chileSeletedPosition?context.getResources().getColor(R.color.test_yellow):context.getResources().getColor(R.color.black));
        holder.txt_article_child_head.setText(headDetails.long_name);
        final int finalI = i;
        holder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chileSeletedPosition = finalI;
                notifyDataSetChanged();
                TabMainActivity.isReadPage = true;
                onSuperChildHeadingClickListener.onSuperChildClicked(i,headDetails._id);
                // context.startActivity(new Intent(context, SubscriptionPointsActivity.class).putExtra("point_id",mcqChapterSection.getPointsContent().get(finalI).get_id()).putExtra("chapter_id",mcqChapterSection.getId()).putExtra("subject_id",strSubId).putExtra("chapter_name",sectionArrayList.get(position).getShortName()).putExtra("subject_name",strSubName).putExtra("points",mcqChapterSection.getPointsContent()));
            }
        });




    }


    @Override
    public int getItemCount() {
        return testArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgArticleChildHead;
        TextView txt_article_child_head ;
        LinearLayoutCompat llChildOfChild;
        LinearLayout llParent;

        public ViewHolder(View itemView) {
            super(itemView);

            this.imgArticleChildHead =  itemView.findViewById(R.id.imgArticleChildHead);
            this.txt_article_child_head =  itemView.findViewById(R.id.txt_article_child_head);
            this.llChildOfChild =  itemView.findViewById(R.id.llChildOfChild);
            this.llParent =  itemView.findViewById(R.id.llParent);


        }
    }
}
