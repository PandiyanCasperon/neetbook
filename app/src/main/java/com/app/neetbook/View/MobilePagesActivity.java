package com.app.neetbook.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.neetbook.CartRoom.CartDataBase;
import com.app.neetbook.Interfaces.OnMobileNumberChanged;
import com.app.neetbook.Interfaces.OnPinChanged;
import com.app.neetbook.Model.TabSideMenu;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ActionBarActivityNeetBook;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Data.ChangeMobileData;
import com.app.neetbook.Utils.Data.ChangePinData;
import com.app.neetbook.Utils.Data.CompleteProfileData;
import com.app.neetbook.Utils.Data.ContactUsData;
import com.app.neetbook.Utils.Data.DrawerActivityData;
import com.app.neetbook.Utils.Data.MyProfileData;
import com.app.neetbook.Utils.LogOutTimerUtil;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.View.Fragments.AboutUsFragment;
import com.app.neetbook.View.Fragments.ChangeMobileFragment;
import com.app.neetbook.View.Fragments.ChangePinFragment;
import com.app.neetbook.View.Fragments.CompleteProfileFragment;
import com.app.neetbook.View.Fragments.ContactUsFragment;
import com.app.neetbook.View.Fragments.FeedBackFragment;
import com.app.neetbook.View.Fragments.HelpFragment;
import com.app.neetbook.View.Fragments.MyProfileFragment;
import com.app.neetbook.View.Fragments.TermsAndConditions;
import com.app.neetbook.View.Fragments.subscription.SubscriptionFragment;

public class MobilePagesActivity extends ActionBarActivityNeetBook implements View.OnClickListener, OnPinChanged, OnMobileNumberChanged {

   TextView txtPageTitle;
   String strMenuName = "";
    RelativeLayout rlBack;
    TabSideMenu tabSideMenu;
    SessionManager sessionManager;
    public static MobilePagesActivity mobilePagesActivity;
    private int pageIndex = 0;
    String strTopic = "0";
    String isFromSideMenu = "No";
    CartDataBase cartlistDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StatusBarColorChange.setMobilePagesStatusBarGradiant(MobilePagesActivity.this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_pages);
        if(!getResources().getBoolean(R.bool.isTablet))
        {
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        mobilePagesActivity = this;
        init();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }

    private void init() {
        txtPageTitle = findViewById(R.id.txtPageTitle);
        rlBack = findViewById(R.id.rlBack);

        rlBack.setOnClickListener(this);
        cartlistDatabase = CartDataBase.getAppDatabase(MobilePagesActivity.this);
        sessionManager = new SessionManager(this);
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        if(intent.hasExtra("topic"))
            strTopic = intent.getStringExtra("topic");
        tabSideMenu = (TabSideMenu)bundle.getSerializable("sideMenu");
        Log.e("Name",tabSideMenu.getName());
        strMenuName = tabSideMenu.getName();
        txtPageTitle.setText(tabSideMenu.getName().equalsIgnoreCase(getString(R.string.change_mobile_no))?getString(R.string.change_mobile_number):tabSideMenu.getName());

        switchToFragment();

    }



    private void switchToFragment() {
        Fragment fragment;
        switch (tabSideMenu.getId())
        {

            case "1":
                fragment = new CompleteProfileFragment();
                setFragment(fragment);
                break;
            case "2":
                fragment = sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) == null || sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("")? new CompleteProfileFragment() : new MyProfileFragment();
                setFragment(fragment);
                break;
            case "3":
                fragment = new ChangePinFragment();
                setFragment(fragment);
                break;

            case "4":
                fragment = new ChangeMobileFragment();
                setFragment(fragment);
                break;

            case "5":
                fragment = new AboutUsFragment();
                setFragment(fragment);
                break;

            case "6":
                fragment = new TermsAndConditions();
                setFragment(fragment);
                break;

            case "7":
                fragment = new HelpFragment();
                setFragment(fragment);
                break;

            case "8":
                fragment = new ContactUsFragment();
                setFragment(fragment);
                break;
            case "9":

                fragment = new FeedBackFragment(strTopic,getIntent().hasExtra("topic")?"Yes":"No",getIntent().hasExtra("subjectId")?getIntent().getStringExtra("subjectId"):"nill");  /*Navigate to home or getting back to TabMainActivity*/
                setFragment(fragment);
                break;

            case "11":
                fragment = new SubscriptionFragment();
                setFragment(fragment);
                break;
            case "10":
                logout();
                break;


        }
    }

    private void logout() {
        CustomAlert customAlert = new CustomAlert(MobilePagesActivity.this);
        customAlert.showAlertLogout(getString(R.string.alert_alert),getString(R.string.logout_alert));
    }

    private void setFragment(Fragment fragment) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameContainer, fragment);
        fragmentTransaction.commit();

    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.rlBack:

                if(tabSideMenu.getId().equals("11") && cartlistDatabase.cartListDao().countCart()>0)
                {
                    showCartAlert();
                }else {

                    CompleteProfileData.clearData();
                    MyProfileData.clearData();
                    ChangePinData.clearData();
                    ChangeMobileData.cleatData();
                    ContactUsData.clearData();
                    finish();
                }
                break;
        }
    }

    private void showCartAlert() {
        final Dialog alertDialog =  new Dialog(MobilePagesActivity.this);
        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(getString(R.string.alert));
        textViewDesc.setText("Packages added in Cart will be deleted. Are you sure to go back?");
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                cartlistDatabase.cartListDao().nukeTable();
                CompleteProfileData.clearData();
                MyProfileData.clearData();
                ChangePinData.clearData();
                ChangeMobileData.cleatData();
                ContactUsData.clearData();
                finish();

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        if(!alertDialog.isShowing())
            alertDialog.show();
    }


    @Override
    public void onPinChanged() {
        finish();
        DrawerContentSlideActivity.drawerContentSlideActivity.finish();
        DrawerActivityData.lastSelectedMenuId = "1";
        DrawerActivityData.isFirstTimeLoading = true;
        Bundle bundle1 = new Bundle();
        TabSideMenu tabSideMenu = new TabSideMenu();
        tabSideMenu.setId("1");
        tabSideMenu.setImage_id_acative(R.drawable.home_active);
        tabSideMenu.setImage_id_inactive(R.drawable.home_inactive);
        tabSideMenu.setName(getString(R.string.home));

        bundle1.putSerializable("sideMenu", tabSideMenu);
        DrawerActivityData.bundle = bundle1;

        startActivity(new Intent(MobilePagesActivity.this, DrawerContentSlideActivity.class).putExtra("sideMenu",tabSideMenu));
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
    }

    @Override
    public void onMobileNumberChanged() {
        finish();
        DrawerContentSlideActivity.drawerContentSlideActivity.finish();
        DrawerActivityData.lastSelectedMenuId = "1";
        Bundle bundle1 = new Bundle();
        TabSideMenu tabSideMenu = new TabSideMenu();
        tabSideMenu.setId("1");
        tabSideMenu.setImage_id_acative(R.drawable.home_active);
        tabSideMenu.setImage_id_inactive(R.drawable.home_inactive);
        tabSideMenu.setName(getString(R.string.home));

        bundle1.putSerializable("sideMenu", tabSideMenu);
        DrawerActivityData.bundle = bundle1;
        DrawerActivityData.isFirstTimeLoading = true;
        startActivity(new Intent(MobilePagesActivity.this, DrawerContentSlideActivity.class).putExtra("sideMenu",tabSideMenu));
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
    }
    @Override
    public void onBackPressed()
    {
        if(tabSideMenu.getId().equals("11") && cartlistDatabase.cartListDao().countCart()>0)
        {
            showCartAlert();
        }else
        {
            CompleteProfileData.clearData();
            MyProfileData.clearData();
            ChangePinData.clearData();
            ChangeMobileData.cleatData();
            ContactUsData.clearData();
            super.onBackPressed();
            return;
        }

    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

}
