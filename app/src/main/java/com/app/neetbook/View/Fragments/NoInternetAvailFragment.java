package com.app.neetbook.View.Fragments;


import android.app.Dialog;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.app.neetbook.R;
import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.View.SplashActivity;


public class NoInternetAvailFragment extends DialogFragment implements View.OnClickListener {
    public static String TAG = NoInternetAvailFragment.class.getSimpleName();
    private ImageView buttonClose;
    private Dialog dialog;
    Button btnRefresh;
    ConnectionDetector cd;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.NoInternetDialog);
        cd = new ConnectionDetector(getActivity());
    }
    public NoInternetAvailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_blank, container, false);
        btnRefresh = rootView.findViewById(R.id.btnRefresh);
        btnRefresh.setOnClickListener(this);
        return rootView;
    }
    @Override
    public void onStart() {
        super.onStart();
        dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnRefresh:
                if (cd.isConnectingToInternet())
                    dialog.dismiss();
                else {

                }

                break;
        }
    }
}
