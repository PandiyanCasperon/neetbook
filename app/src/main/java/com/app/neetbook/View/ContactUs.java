package com.app.neetbook.View;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.PowerManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.neetbook.Model.StateBean;
import com.app.neetbook.Model.TabSideMenu;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ActionBarActivityNeetBook;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Data.ChangeMobileData;
import com.app.neetbook.Utils.Data.ChangePinData;
import com.app.neetbook.Utils.Data.CompleteProfileData;
import com.app.neetbook.Utils.Data.ContactUsData;
import com.app.neetbook.Utils.Data.DrawerActivityData;
import com.app.neetbook.Utils.Data.MyProfileData;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.Utils.alerter.AlertBox;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ContactUs extends ActionBarActivityNeetBook implements View.OnClickListener {

    RelativeLayout rlBackContactUs;
    EditText etContactUsEmail,etContactUsMobile,etContactUsDesc;
    RelativeLayout RlContactUsSubmit;
    Loader loader;
    ArrayList<String> contactUsList = new ArrayList<>();
    ArrayList<StateBean> contactUsBeanList = new ArrayList<>();
    CustomAlert customAlert;
    Spinner spinnerContactUs;
    private boolean isSelected = false;
    private PowerManager.WakeLock wl;
    TextView txtContactUsSpinHint;
    RelativeLayout rlContactUsSelectQuery;
    public static ContactUs PagesActivity;
    @SuppressLint("InvalidWakeLockTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(getResources().getBoolean(R.bool.isTablet))
        {
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }else {
            StatusBarColorChange.setStatusBarGradiant(ContactUs.this);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        PagesActivity = this;
        init();
    }

    private void init() {
        rlBackContactUs = findViewById(R.id.rlBackContactUs);
        etContactUsMobile = findViewById(R.id.etContactUsMobile);
        etContactUsEmail = findViewById(R.id.etContactUsEmail);
        etContactUsDesc = findViewById(R.id.etContactUsDesc);
        RlContactUsSubmit = findViewById(R.id.RlContactUsSubmit);
        spinnerContactUs = findViewById(R.id.spinnerContactUs);
        rlContactUsSelectQuery = findViewById(R.id.rlContactUsSelectQuery);
        txtContactUsSpinHint = findViewById(R.id.txtContactUsSpinHint);
        loader = new Loader(ContactUs.this);
        customAlert = new CustomAlert(ContactUs.this);
        rlBackContactUs.setOnClickListener(this);
        RlContactUsSubmit.setOnClickListener(this);
        generateDroptown();
        if(getResources().getBoolean(R.bool.isTablet))
            manageOrientation();
        etContactUsDesc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ContactUsData.contactUsDesc = s.toString().isEmpty()?"":s.toString();
                setButtonEnable();
//                if(!etContactUsEmail.getText().toString().isEmpty() && !etContactUsMobile.getText().toString().isEmpty() && !etContactUsDesc.getText().toString().isEmpty())
//                    RlContactUsSubmit.setBackground(getResources().getDrawable(R.drawable.login_btn_gratient));
//                else
//                    RlContactUsSubmit.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
            }
        });
        etContactUsEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ContactUsData.contactUsEmail = s.toString().isEmpty()?"":s.toString();
                setButtonEnable();
//                if(!etContactUsEmail.getText().toString().isEmpty() && !etContactUsMobile.getText().toString().isEmpty() && !etContactUsDesc.getText().toString().isEmpty())
//                    RlContactUsSubmit.setBackground(getResources().getDrawable(R.drawable.login_btn_gratient));
//                else
//                    RlContactUsSubmit.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
            }
        });
        etContactUsMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ContactUsData.contactUsMobile = s.toString().isEmpty()?"":s.toString();
                setButtonEnable();
                if(!s.toString().isEmpty() && s.toString().length()==10)
                    StatusBarColorChange.hideSoftKeyboard(ContactUs.this);

            }
        });

        rlContactUsSelectQuery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerContactUs.performClick();
            }
        });
        spinnerContactUs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                ContactUsData.contactUsSelectedItem = spinnerContactUs.getSelectedItem().toString();
                txtContactUsSpinHint.setText(spinnerContactUs.getSelectedItem().toString());
                txtContactUsSpinHint.setTextColor(position>0?getResources().getColor(R.color.black):getResources().getColor(R.color.hint_color));
                setButtonEnable();
//                if(!etContactUsDesc.getText().toString().isEmpty() && !txtContactUsSpinHint.getText().toString().equalsIgnoreCase("Select Query"))
//                    RlContactUsSubmit.setBackground(getResources().getDrawable(R.drawable.login_btn_gratient));
//                else
//                    RlContactUsSubmit.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void setButtonEnable() {
        if(!etContactUsEmail.getText().toString().isEmpty() && etContactUsEmail.getText().toString().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$") && !etContactUsMobile.getText().toString().isEmpty() && etContactUsMobile.getText().toString().length() == 10 && !etContactUsDesc.getText().toString().isEmpty()&& !txtContactUsSpinHint.getText().toString().equalsIgnoreCase("Select Query"))
            RlContactUsSubmit.setBackground(getResources().getDrawable(R.drawable.login_btn_gratient));
        else
            RlContactUsSubmit.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
    }

    private void manageOrientation() {
        etContactUsDesc.setText(ContactUsData.contactUsDesc);
        etContactUsEmail.setText(ContactUsData.contactUsEmail);
        etContactUsMobile.setText(ContactUsData.contactUsMobile);
        txtContactUsSpinHint.setText(ContactUsData.contactUsSelectedItem);
        txtContactUsSpinHint.setTextColor(ContactUsData.contactUsSelectedItem.equalsIgnoreCase("Select Query")?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.rlBackContactUs:
                CompleteProfileData.clearData();
                MyProfileData.clearData();
                ChangePinData.clearData();
                ChangeMobileData.cleatData();
                ContactUsData.clearData();
                finish();
                break;

            case R.id.RlContactUsSubmit:
                if (txtContactUsSpinHint.getText().toString().equalsIgnoreCase("Select Query"))
                    AlertBox.showSnackBoxPrimaryBa(ContactUs.this, getString(R.string.alert_oops), getString(R.string.contactUsSelectTopicAlert));
                else if (etContactUsEmail.getText().toString().isEmpty())
                    AlertBox.showSnackBoxPrimaryBa(ContactUs.this, getString(R.string.alert_oops), getString(R.string.alert_enter_email));
                else if (!etContactUsEmail.getText().toString().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"))
                    AlertBox.showSnackBoxPrimaryBa(ContactUs.this,getString(R.string.alert_alert),getString(R.string.alert_enter_valid_email));
                else if (etContactUsMobile.getText().toString().isEmpty())
                    AlertBox.showSnackBoxPrimaryBa(ContactUs.this, getString(R.string.alert_oops), getString(R.string.alert_enter_mobile));
                else if (etContactUsMobile.getText().toString().length()<10)
                    AlertBox.showSnackBoxPrimaryBa(ContactUs.this,getString(R.string.alert_alert),getString(R.string.alert_enter_valid_mobile));
                else if (etContactUsDesc.getText().toString().isEmpty())
                    AlertBox.showSnackBoxPrimaryBa(ContactUs.this, getString(R.string.alert_oops), getString(R.string.contactUsEnterDescAlert));
                else
                    submitContact();
                break;
        }
    }
    private void generateDroptown() {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(ContactUs.this);

        mRequest.makeServiceRequest(IConstant.contactUsQuery, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------contactUsQuery Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1")) {
                        contactUsList.add("SELECT QUERY");
                        StateBean stateBean1 = new StateBean();
                        stateBean1.setState_name("SELECT");
                        stateBean1.set_id("_id");
                        contactUsBeanList.add(stateBean1);
                        JSONArray listArray = object.getJSONArray("list");
                        if(listArray.length()>0)
                        {
                            for(int i=0;i<listArray.length();i++) {
                                JSONObject jsonObject = listArray.getJSONObject(i);
                                StateBean stateBean = new StateBean();
                                contactUsList.add(jsonObject.getString("description"));
                                stateBean.setState_name(jsonObject.getString("description"));
                                stateBean.set_id(jsonObject.getString("_id"));
                                contactUsBeanList.add(stateBean);
                            }
                            ArrayAdapter countryAdapter = new ArrayAdapter(ContactUs.this, R.layout.spinner_item,contactUsList);
                            countryAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                            spinnerContactUs.setAdapter(countryAdapter);
                            spinnerContactUs.setSelection(contactUsList.contains(ContactUsData.contactUsSelectedItem)?contactUsList.indexOf(ContactUsData.contactUsSelectedItem):0);
                        }
                    } else if(sStatus.equals("00")){
                        loader.dismissLoader();
                        customAlert.singleLoginAlertLogout();
                    }else if(sStatus.equalsIgnoreCase("01")){
                        loader.dismissLoader();
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    } else {
                        loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(ContactUs.this,getString(R.string.alert_oops),errorMessage);
            }
        });

    }
    private void submitContact() {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(ContactUs.this);
        HashMap<String,String> param = new HashMap<>();
        param.put("query_id", contactUsBeanList.get(contactUsList.indexOf(txtContactUsSpinHint.getText().toString())).get_id());
        param.put("email",etContactUsEmail.getText().toString());
        param.put("phone",etContactUsMobile.getText().toString());
        param.put("comment",etContactUsDesc.getText().toString());
        mRequest.makeServiceRequest(IConstant.contactUsSubmit, Request.Method.POST, param, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------contactUsSubmit Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1")) {
                        alertSubmitted(getString(R.string.alert_success),message);
                    } else if(sStatus.equals("00")){
                        customAlert.singleLoginAlertLogout();
                    }else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(ContactUs.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void alertSubmitted(String title, String desc)
    {
        final Dialog alertDialog = new Dialog(ContactUs.this);
        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        ImageView imgAlert = alertDialog.findViewById(R.id.imgAlert);
        Picasso.with(ContactUs.this).load(R.drawable.right_mark).into(imgAlert);
        textViewTitle.setText(title);
        textViewDesc.setText(desc);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                CompleteProfileData.clearData();
                MyProfileData.clearData();
                ChangePinData.clearData();
                ChangeMobileData.cleatData();
                ContactUsData.clearData();
                finish();
            }
        });

        alertDialog.show();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if(ev.getAction() == MotionEvent.ACTION_UP) {
            final View view = getCurrentFocus();

            if(view != null) {
                final boolean consumed = super.dispatchTouchEvent(ev);

                final View viewTmp = getCurrentFocus();
                final View viewNew = viewTmp != null ? viewTmp : view;

                if(viewNew.equals(view)) {
                    final Rect rect = new Rect();
                    final int[] coordinates = new int[2];

                    view.getLocationOnScreen(coordinates);

                    rect.set(coordinates[0], coordinates[1], coordinates[0] + view.getWidth(), coordinates[1] + view.getHeight());

                    final int x = (int) ev.getX();
                    final int y = (int) ev.getY();

                    if(rect.contains(x, y)) {
                        return consumed;
                    }
                }
                else if(viewNew instanceof EditText || viewNew instanceof EditText) {
                    return consumed;
                }

                final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                inputMethodManager.hideSoftInputFromWindow(viewNew.getWindowToken(), 0);

                viewNew.clearFocus();

                return consumed;
            }
        }

        return super.dispatchTouchEvent(ev);
    }

    public void onBackPressed()
    {
        CompleteProfileData.clearData();
        MyProfileData.clearData();
        ChangePinData.clearData();
        ChangeMobileData.cleatData();
        ContactUsData.clearData();
        super.onBackPressed();
    }



}
