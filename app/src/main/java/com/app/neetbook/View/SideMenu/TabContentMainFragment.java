package com.app.neetbook.View.SideMenu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.app.neetbook.Adapter.ArticleTabsAdapter;
import com.app.neetbook.Interfaces.FragmentRefreshListener;
import com.app.neetbook.Interfaces.NetworkStateReceiverListener;
import com.app.neetbook.Interfaces.OnArtickeHeadingChanged;
import com.app.neetbook.Interfaces.OnFragmentInteractionListener;
import com.app.neetbook.Interfaces.OnMobileHomeMenuSelectedListioner;
import com.app.neetbook.Interfaces.OnTestAttemp;
import com.app.neetbook.Model.HeadData;
import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.PointsContent;
import com.app.neetbook.R;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.NetworkStateReceiver;
import com.app.neetbook.Utils.OnSwipeTouchListener;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.Thread.AppExecutors;
import com.app.neetbook.View.DynamicFragmentActivity;
import com.app.neetbook.View.ScrollingActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.cleveroad.sy.cyclemenuwidget.CycleMenuWidget;
import com.cleveroad.sy.cyclemenuwidget.OnMenuItemClickListener;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TabContentMainFragment extends Fragment implements OnMenuItemClickListener, View.OnClickListener {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Context context;
    private String strArticleId = "";
    Loader loader;
    private SessionManager sessionManager;
    private CustomAlert customAlert;
    private int page = 1;
    int loadMoreEnable = 0;
    private int totalSubsCount;
    ArrayList<PointsContent> pointsContents;
    String strSubjectId= "",strChapterId = "" ,strHeadId= "",strSubjectName= "",strHeadName= "",strLongName= "",strsuperHeading= "",chapter_name= "";
    private ImageView imageView2,imageView3,imageViewBookmark;
    private TextView textView,textView3,txtSurgical;
    private ArrayList<HeadData> headDataList;
    private NetworkStateReceiver networkStateReceiver;
    private FragmentRefreshListener fragmentRefreshListener;

    private int currentSelectedPosition = 0;
    ArticleTabsAdapter homeGroupNameAdapter;
    LinearLayoutManager layoutManager;
    LinearSnapHelper snapHelper;
    RecyclerView recyclerViewGroups;
    Typeface tfBold,tfMedium,tfRegular;
    private AppExecutors appExecutors;
    private CycleMenuWidget mCycleMenuWidget;
    private ArrayList<PointsContent> mPointsList = new ArrayList<>();
    private RelativeLayout rlParentRight,rlParent;

    private OnArtickeHeadingChanged onArtickeHeadingChanged;

    public TabContentMainFragment(String strHeadId, String strChapterId,String strChapterName, String strSubjectName, String subjectId, String strSuperHeading) {
        // Required empty public constructor
        this.strHeadId = strHeadId;
        this.strChapterId = strChapterId;
        this.chapter_name = strChapterName;
        this.strSubjectName = strSubjectName;
        this.strSubjectId = subjectId;
        this.strsuperHeading = strSuperHeading;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab_content_main, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        tfBold  = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium  = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular  = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaRegular.otf");
        sessionManager = new SessionManager(getActivity());
        customAlert = new CustomAlert(getActivity());
        loader = new Loader(getActivity());
        networkStateReceiver = new NetworkStateReceiver();
        viewPager = view.findViewById(R.id.viewpager_subscription_category);
        imageViewBookmark = view.findViewById(R.id.imageViewBookmark);
        tabLayout = view.findViewById(R.id.tabs_subscription_category);
        imageView2 = view.findViewById(R.id.imageView2);
        imageView3 = view.findViewById(R.id.imageView3);
        textView = view.findViewById(R.id.textView);
        txtSurgical = view.findViewById(R.id.textView2);
        textView3 = view.findViewById(R.id.textView3);
        rlParent = view.findViewById(R.id.rlParent);
        rlParentRight = view.findViewById(R.id.rlParentRight);
        recyclerViewGroups = view.findViewById(R.id.recyclerViewGroups);
        mCycleMenuWidget = (CycleMenuWidget)view.findViewById(R.id.itemCycleMenuWidget);
        mCycleMenuWidget.setVisibility(View.GONE);
        mCycleMenuWidget.setCorner(CycleMenuWidget.CORNER.RIGHT_TOP);
     //   mCycleMenuWidget.setMode(sessionManager.isNightModeEnabled()?1:0);
        mCycleMenuWidget.setAlpha(0.9f);
        mCycleMenuWidget.setOnMenuItemClickListener(this);
        headDataList = new ArrayList<>();//superHeading

            txtSurgical.setText(chapter_name);
            textView3.setText(strsuperHeading);
            textView3.setVisibility(!strsuperHeading.equalsIgnoreCase("No")?View.VISIBLE:View.GONE);
            currentSelectedPosition = 0;
            if(sessionManager.getViewToBookMark() != null && !sessionManager.getViewToBookMark().isEmpty() && sessionManager.getViewToBookMark().equalsIgnoreCase("Yes"))
            {
                ScrollingActivity.headDataArrayList =
                        new Gson().fromJson(sessionManager.getBookmarkDetails().get(SessionManager.HEAD_LIST), new TypeToken<List<HeadDetails>>() {
                        }.getType());
            }
        appExecutors = new AppExecutors();

        getPoints();
        createSnap();
        setupViewPager(viewPager);

        listener();

        setFontSize(); 
    }
    
    private void getPoints() {

        appExecutors.networkIO().execute(new Runnable() {
            @Override
            public void run() {

                mPointsList.clear();
                ServiceRequest mRequest = new ServiceRequest(getActivity());
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
                params.put("subject_id",strSubjectId);
                params.put("chapter_id",strChapterId);

                mRequest.makeServiceRequest(IConstant.pointsContent, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                    @Override
                    public void onCompleteListener(String response) {
                        System.out.println("------------pointsContent Response----------------" + response);
                        String sStatus = "";
                        String message = "";
                        try {
                            JSONObject obj = new JSONObject(response);
                            sStatus = obj.getString("status");
                            message = obj.has("message")?obj.getString("message"):"";

                            if (sStatus.equalsIgnoreCase("1")) {
                                JSONObject object = obj.getJSONObject("response");
                                totalSubsCount = object.getInt("atotal");
                                JSONArray jsonArray = object.getJSONArray("points");

                                if(jsonArray.length()>0) {
                                    for(int j=jsonArray.length()-1;j>=0;j--)
                                    {
                                        PointsContent pointsContent = new PointsContent();
                                        pointsContent.set_id(jsonArray.getJSONObject(j).getString("_id"));
                                        pointsContent.setShort_name(jsonArray.getJSONObject(j).getString("short_name"));
                                        pointsContent.setLong_name(jsonArray.getJSONObject(j).getString("long_name"));
                                        pointsContent.setContent(jsonArray.getJSONObject(j).getString("content"));
//                    pointsContent.setStatus(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("status"));
//                    pointsContent.setCstatus(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("cstatus"));
                                        pointsContent.setPoint_name(jsonArray.getJSONObject(j).getString("point_name"));
                                        mPointsList.add(pointsContent);
                                    }


                                    //mCycleMenuWidget.setMenuRes(R.menu.points_menu,mPointsList.size());
                                    mCycleMenuWidget.setMenuRes(R.menu.points_menu);
                                    //mCycleMenuWidget.open(true);
                                }

                            }

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onErrorListener(String errorMessage) {
                        // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
                    }
                });

            }
        });



    }

    private void setFontSize() {

        if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
            textView.setTextAppearance(getActivity(),R.style.textViewSmallSubjectName);
            txtSurgical.setTextAppearance(getActivity(),R.style.textViewSmallChaptName);
            textView3.setTextAppearance(getActivity(),R.style.textViewSmallChaptName);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
            textView.setTextAppearance(getActivity(),R.style.textViewMediumSubjectName);
            txtSurgical.setTextAppearance(getActivity(),R.style.textViewMediumChaptName);
            textView3.setTextAppearance(getActivity(),R.style.textViewMediumChaptName);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
            textView.setTextAppearance(getActivity(),R.style.textViewLargeSubjectName);
            txtSurgical.setTextAppearance(getActivity(),R.style.textViewLargeChaptName);
            textView3.setTextAppearance(getActivity(),R.style.textViewLargeChaptName);
        }

        textView.setTypeface(tfMedium);
        txtSurgical.setTypeface(tfMedium);
        textView3.setTypeface(tfMedium);
    }

    private void listener() {

        imageView3.setOnClickListener(this);
        imageView2.setOnClickListener(this);
        imageViewBookmark.setOnClickListener(this);

        new AppExecutors().mainThread().execute(new Runnable() {
            @Override
            public void run() {


      /*  rlParent.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            public void onSwipeTop() {
                //Toast.makeText(getActivity(), "top", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeRight() {
                //Toast.makeText(getActivity(), "right", Toast.LENGTH_SHORT).show();
                //finish();
            }
            public void onSwipeLeft() {
                //startActivity(new Intent(getActivity(), DynamicFragmentActivity.class).putExtra("subject_id",strSubjectId).putExtra("head_id",strHeadId).putExtra("title",strHeadName).putExtra("long_name",strLongName).putExtra("isPoints","No").putExtra("chapterName",chapter_name));
            }
            public void onSwipeBottom() {
                //Toast.makeText(getActivity(), "bottom", Toast.LENGTH_SHORT).show();
            }

        });
        rlParentRight.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            public void onSwipeTop() {
                //Toast.makeText(getActivity(), "top", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeRight() {

            }
            public void onSwipeLeft() {
                //startActivity(new Intent(getActivity(), DynamicFragmentActivity.class).putExtra("subject_id",strSubjectId).putExtra("head_id",strHeadId).putExtra("title",strHeadName).putExtra("long_name",strLongName).putExtra("isPoints","No").putExtra("chapterName",chapter_name));
            }
            public void onSwipeBottom() {
                //Toast.makeText(getActivity(), "bottom", Toast.LENGTH_SHORT).show();
            }

        });*/

                mCycleMenuWidget.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCycleMenuWidget.setVisibility(View.GONE);
                    }
                });

            }
        });

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageView2:
                //finish();
                break;

            case R.id.imageView3:
                //finish();
                break;

            case R.id.imageViewBookmark:
                mCycleMenuWidget.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mCycleMenuWidget.open(true);
                    }
                },100);


                //sessionManager.updateBookMark(strHeadId,strSubjectId,strLongName,new Gson().toJson(headDataArrayList),strArticleId,chapter_name);
                break;

        }
    }

    private void setupViewPager(final ViewPager viewPager) {

        appExecutors.mainThread().execute(new Runnable() {
            @Override
            public void run() {

                final ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getFragmentManager(),
                        getActivity());


                if(ScrollingActivity.headDataArrayList.size()>0)
                {
                    Log.e("headDataArrayList",new Gson().toJson(ScrollingActivity.headDataArrayList));
                    for(int i=0;i<ScrollingActivity.headDataArrayList.size();i++)
                    {
                        if(ScrollingActivity.headDataArrayList.get(i).isSH.equals("Yes")) {
                            HeadData data = new HeadData();
                            data._id = ScrollingActivity.headDataArrayList.get(i).headData.get(0)._id;
                            data.short_name = ScrollingActivity.headDataArrayList.get(i).headData.get(0).short_name;
                            data.long_name = ScrollingActivity.headDataArrayList.get(i).headData.get(0).long_name;
                            data.url = ScrollingActivity.headDataArrayList.get(i).headData.get(0).url;
                            headDataList.add(data);
                            viewPagerAdapter.addFragment(new SubscriptionArticleFragment(ScrollingActivity.headDataArrayList.get(i).headData.get(0)._id, strSubjectId,ScrollingActivity.headDataArrayList.get(i).headData.get(0).long_name,i,ScrollingActivity.headDataArrayList.get(i).headData.get(0).url,Integer.parseInt(headDataList.get(0).count)), ScrollingActivity.headDataArrayList.get(i).headData.get(0).short_name);
                        }
                        else
                        {
                            if(ScrollingActivity.headDataArrayList.get(i).headData.size()>0) {
                                for (int j = 0; j < ScrollingActivity.headDataArrayList.get(i).headData.size(); j++) {
                                    HeadData data = new HeadData();
                                    data._id = ScrollingActivity.headDataArrayList.get(i).headData.get(j)._id;
                                    data.short_name = ScrollingActivity.headDataArrayList.get(i).headData.get(j).short_name;
                                    data.long_name = ScrollingActivity.headDataArrayList.get(i).headData.get(j).long_name;
                                    data.url = ScrollingActivity.headDataArrayList.get(i).headData.get(j).url;
                                    headDataList.add(data);
                                    viewPagerAdapter.addFragment(new SubscriptionArticleFragment(ScrollingActivity.headDataArrayList.get(i).headData.get(j)._id,strSubjectId,ScrollingActivity.headDataArrayList.get(i).headData.get(0).long_name,i,ScrollingActivity.headDataArrayList.get(i).headData.get(0).url,Integer.parseInt(headDataList.get(0).count)), ScrollingActivity.headDataArrayList.get(i).headData.get(j).short_name);
                                }
                            }
                        }
                    }

                }


                if(headDataList.size()>0)
                {
                    for(int i=0;i<headDataList.size();i++)
                    {
                        if(strHeadId.equals(headDataList.get(i)._id))
                            currentSelectedPosition = i;
                    }
                }
                sessionManager.updateBookMark("Article",headDataList.get(0)._id,strSubjectId,headDataList.get(0).long_name,new Gson().toJson(ScrollingActivity.headDataArrayList),strArticleId,chapter_name,strsuperHeading,"","");
                viewPager.setAdapter(viewPagerAdapter);

                tabLayout.setupWithViewPager(viewPager);
                for (int i = 0; i < tabLayout.getTabCount(); i++) {
                    TabLayout.Tab tab = tabLayout.getTabAt(i);
                    if (tab != null) {
                        tab.setCustomView(viewPagerAdapter.getTabView(i));
                    }
                }
                setGroupAdapter(currentSelectedPosition);
                viewPager.setCurrentItem(currentSelectedPosition);
                viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                        setGroupAdapter(position);
                        layoutManager = ((LinearLayoutManager)recyclerViewGroups.getLayoutManager());
                        int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                        int centeredItemPosition = totalVisibleItems / 2;
                        recyclerViewGroups.smoothScrollToPosition(position != 0 ?position-1 : position);
                        strHeadId = headDataList.get(position)._id;
                        strHeadName = headDataList.get(position).short_name;
                        strLongName = headDataList.get(position).long_name;
                        sessionManager.updateBookMark("Article",strHeadId,strSubjectId,strLongName,new Gson().toJson(ScrollingActivity.headDataArrayList),strArticleId,chapter_name,strsuperHeading,"","");
                        onArtickeHeadingChanged.onArticleHeadingChanged(strChapterId,chapter_name,strHeadId,"1","","","",strHeadName,strSubjectId);
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
            }
        });


    }

    @Override
    public void onPause() {
        // new AsyncUpdateSessionRunner("1").execute();
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        // Log.e("onStopMainActivity","Called");
    }

    private void setGroupAdapter(final int position) {
        appExecutors.mainThread().execute(new Runnable() {
            @Override
            public void run() {
                homeGroupNameAdapter = new ArticleTabsAdapter(headDataList, getActivity(), position);
                recyclerViewGroups.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
                snapHelper.attachToRecyclerView(recyclerViewGroups);
                recyclerViewGroups.setAdapter(homeGroupNameAdapter);

                recyclerViewGroups.scrollToPosition(position);
                homeGroupNameAdapter.setOnGroupMenuSelectedListener(new OnMobileHomeMenuSelectedListioner() {
                    @Override
                    public void onGroupSelected(String groupId, int position) {
                        viewPager.setCurrentItem(position);
                        layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
                        int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                        int centeredItemPosition = totalVisibleItems / 2;
                        recyclerViewGroups.smoothScrollToPosition(position != 0 ? position - 1 : position);
                        recyclerViewGroups.setScrollY(centeredItemPosition);
                    }
                });
            }
            });
    }

    @Override
    public void onMenuItemClick(View view, int itemPosition) {
        startActivity(new Intent(getActivity(),DynamicFragmentActivity.class).putExtra("isPoints","Yes").putExtra("pointsTitle",mPointsList.get(itemPosition).getLong_name()).putExtra("pointsContent",mPointsList.get(itemPosition).getContent()).putExtra("chapterName",chapter_name));
        getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
    }

    @Override
    public void onMenuItemLongClick(View view, int itemPosition) {

    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        List<Fragment> fragmentList = new ArrayList<>();
        List<String> fragmentTitles = new ArrayList<>();
        Context mContext;
        TextView tabTitles;


        ViewPagerAdapter(FragmentManager fragmentManager,
                         FragmentActivity activity) {
            super(fragmentManager);
            mContext = activity;
        }

        @Override
        public Fragment getItem(int position) {
            return new SubscriptionArticleFragment(headDataList.get(position)._id,strSubjectId,headDataList.get(position).long_name,position,headDataList.get(position).url,Integer.parseInt(headDataList.get(position).count));
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitles.get(position);
        }

        void addFragment(Fragment fragment, String name) {
            fragmentList.add(fragment);
            fragmentTitles.add(name);
        }

        View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab_items, null);
            tabTitles = v.findViewById(R.id.txtTabTitle);
            tabTitles.setText(fragmentTitles.get(position));
            tabTitles.setTextColor(getResources().getColor(R.color.white));
            return v;
        }
    }

    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof OnArtickeHeadingChanged){
            onArtickeHeadingChanged = (OnArtickeHeadingChanged) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onArtickeHeadingChanged = null;
    }
}
