package com.app.neetbook.View;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.neetbook.R;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.alerter.AlertBox;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class PaymentActivity extends AppCompatActivity{

    ImageView imgPaymentStatus;
    TextView txtPaymentFailed,txtPaymentSuccess,txtPaymentId;
    Handler handler;
    Button btnClose;
    Loader loader;
    SessionManager sessionManager;
    CustomAlert customAlert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        init();
//payId
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }

    private void init() {

        sessionManager = new SessionManager(PaymentActivity.this);
        loader = new Loader(PaymentActivity.this);
        customAlert = new CustomAlert(PaymentActivity.this);
        imgPaymentStatus = findViewById(R.id.imgPaymentStatus);
        txtPaymentSuccess = findViewById(R.id.txtPaymentSuccess);
        txtPaymentFailed = findViewById(R.id.txtPaymentFailed);
        txtPaymentId = findViewById(R.id.txtPaymentId);
        btnClose = findViewById(R.id.btnClose);
        btnClose.setText("Please Wait...");
        btnClose.setEnabled(false);
        if(getIntent().getExtras() != null)
        {
            if(getIntent().getStringExtra("status").equals("success"))
            {
                Picasso.with(this).load(R.drawable.payment_sccess).into(imgPaymentStatus);
                txtPaymentFailed.setVisibility(View.INVISIBLE);
                txtPaymentSuccess.setVisibility(View.VISIBLE);
                if(getIntent().getStringExtra("amount").equals("0")) {
                    txtPaymentId.setVisibility(View.INVISIBLE);
                    txtPaymentSuccess.setText("Packages Subscribed Successfully");
                }
                else
                {
                    txtPaymentId.setVisibility(View.VISIBLE);
                    txtPaymentId.setText("Pay_ID : "+getIntent().getStringExtra("payId"));
                }
                savePaymentDetails();
            }else
            {
                btnClose.setText("Close");
                btnClose.setEnabled(true);
                Picasso.with(this).load(R.drawable.payment_fail).into(imgPaymentStatus);
                txtPaymentFailed.setVisibility(View.VISIBLE);
                txtPaymentFailed.setText(getIntent().getStringExtra("status"));
                txtPaymentSuccess.setVisibility(View.INVISIBLE);
                txtPaymentId.setVisibility(View.INVISIBLE);
            }
        }

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getIntent().getStringExtra("status").equals("success")) {
                    MobilePagesActivity.mobilePagesActivity.finish();
                    ReviewOrder.reviewOrder.finish();
                    finish();
                }
                else {
                    MobilePagesActivity.mobilePagesActivity.finish();
                    ReviewOrder.reviewOrder.finish();
                    finish();
                }

            }
        });

    }
    private void savePaymentDetails() {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(PaymentActivity.this);
        HashMap<String,String> params = new HashMap<>();
        params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("cart_id",getIntent().getStringExtra("cart_id"));
        params.put("gst", getIntent().getStringExtra("gst"));
        params.put("service", getIntent().getStringExtra("service"));
        params.put("sgst", getIntent().getStringExtra("sgst"));
        params.put("wallet_amount", getIntent().getStringExtra("wallet_amount"));
        params.put("amount", getIntent().getStringExtra("amount"));
        params.put("payment_id",getIntent().getStringExtra("payId"));
        params.put("key_id","rzp_test_IltK5ri6eSjMKq");
        params.put("secret_key","pxImD9Hai6c8dycNbNfGvTqJ");
        mRequest.makeServiceRequest(IConstant.completePayment, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------completePayment Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    loader.dismissLoader();
                    btnClose.setText("Close");
                    if (sStatus.equalsIgnoreCase("1")) {
                        btnClose.setEnabled(true);
                    } else if(sStatus.equals("00")){
                        customAlert.singleLoginAlertLogout();
                    } else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    } else {
                        btnClose.setEnabled(true);
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    btnClose.setEnabled(true);
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                btnClose.setEnabled(true);
                AlertBox.showSnackBoxPrimaryBa(PaymentActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }
    private void navigate() {
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(getIntent().getStringExtra("status").equals("success")) {
//                    MobilePagesActivity.mobilePagesActivity.finish();
//                    finish();
                }
                else
                {//finish();
                     }
            }
        },2000);
    }

}
