package com.app.neetbook.View.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.neetbook.Adapter.TestQuestionsAdapter;
import com.app.neetbook.Adapter.TestScoreAdapter;
import com.app.neetbook.Model.TestScores;
import com.app.neetbook.R;

import java.util.ArrayList;
import java.util.List;

public class TestTabFragment extends Fragment {

    int position;
    private TextView textView;
    private RecyclerView testScrorBoardRecyclerView, testQRecyclerView;
    public static Fragment getInstance(int position) {
        Log.e("getinstance",position+"");
        Bundle bundle = new Bundle();
        bundle.putInt("pos", position);
        TestTabFragment testTabFragment = new TestTabFragment();
        testTabFragment.setArguments(bundle);
        return testTabFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt("pos");
        Log.e("Position",position+"");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_test, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        textView = view.findViewById(R.id.testText);
//        testScrorBoardRecyclerView = view.findViewById(R.id.testScrorBoardRecycler);
//        testQRecyclerView = view.findViewById(R.id.testQRecycler);

        populateTestScores();
        populateTestQuestions();
    }

    private void populateTestScores(){
        List<TestScores> scoreList = new ArrayList<>();
        scoreList.add(new TestScores("16", "Correct Answers", ""));
        scoreList.add(new TestScores("20/25", "Questions Attended", ""));
        scoreList.add(new TestScores("03", "Wrong Answers", ""));
        scoreList.add(new TestScores("61/100", "Score", "61%"));

        TestScoreAdapter adapter = new TestScoreAdapter(scoreList, requireContext());
        testScrorBoardRecyclerView.setAdapter(adapter);
        testScrorBoardRecyclerView.setLayoutManager(new GridLayoutManager(requireContext(), 2));
    }

    private void populateTestQuestions(){
      /*  TestQuestionsAdapter adapter = new TestQuestionsAdapter(contacts);
        testQRecyclerView.setAdapter(adapter);
        testQRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));*/
    }
}
