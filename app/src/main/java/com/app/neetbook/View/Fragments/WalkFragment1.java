package com.app.neetbook.View.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;

import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.neetbook.R;
import com.app.neetbook.View.WalkthroughActivity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class WalkFragment1 extends Fragment {
    ImageView img_man;
    ImageView img_wman;
    ImageView img_table;
    ImageView imgBg;
    View view;
    TextView txtArticle,txtDesc;
    ViewGroup container;
    LayoutInflater inflater;


    public WalkFragment1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Display display = ((WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int rotation = display.getRotation();
        if(rotation == 1)
            view = inflater.inflate(R.layout.fragment_walk_fragment1, container, false);
        else if(getResources().getBoolean(R.bool.isTablet))
            view = inflater.inflate(R.layout.fragment_walk_fragment1_portrait, container, false);
        else
            view = inflater.inflate(R.layout.fragment_walk_fragment1, container, false);
        img_man = view.findViewById(R.id.img_man);
        img_wman = view.findViewById(R.id.img_girl);
        img_table = view.findViewById(R.id.img_table);
        imgBg = view.findViewById(R.id.imgBg);
        txtArticle = view.findViewById(R.id.txtArticle);
        txtDesc = view.findViewById(R.id.txtDesc);
        setTitleAndDesc();
        Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
        img_man.setVisibility(View.GONE);
        img_wman.setVisibility(View.GONE);
        img_table.setVisibility(View.GONE);
        txtDesc.setVisibility(View.GONE);
        txtArticle.setVisibility(View.GONE);

        img_man.startAnimation(RightSwipe);
        img_man.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                img_wman.setVisibility(View.VISIBLE);
                img_wman.startAnimation(RightSwipe);
            }
        },200);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                img_table.setVisibility(View.VISIBLE);
                img_table.startAnimation(RightSwipe);
            }
        },300);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                txtArticle.setVisibility(View.VISIBLE);
                txtArticle.startAnimation(RightSwipe);
            }
        },400);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                txtDesc.setVisibility(View.VISIBLE);
                txtDesc.startAnimation(RightSwipe);
            }
        },500);
//        Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
//        img_man.startAnimation(RightSwipe);
//        img_man.setVisibility(View.VISIBLE);
        return view;
    }

    private void setTitleAndDesc() {

        try {
            if(WalkthroughActivity.jsonWalkThrough!= null) {
                txtArticle.setText(WalkthroughActivity.jsonWalkThrough.getString("heading1"));
                txtDesc.setText(WalkthroughActivity.jsonWalkThrough.getString("content1"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putBundle("newBundy", newBundy);
    }

//    @Override
//    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//        savedInstanceState.getBundle("newBundy");
//    }
@Override
public void setUserVisibleHint(boolean isFragmentVisible_) {
    super.setUserVisibleHint(true);
    if (this.isVisible()) {
        // we check that the fragment is becoming visible
        if (isFragmentVisible_) {
            Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
            img_man.setVisibility(View.GONE);
            img_wman.setVisibility(View.GONE);
            img_table.setVisibility(View.GONE);
            txtDesc.setVisibility(View.GONE);
            txtArticle.setVisibility(View.GONE);

            img_man.startAnimation(RightSwipe);
            img_man.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                    img_wman.setVisibility(View.VISIBLE);
                    img_wman.startAnimation(RightSwipe);
                }
            },200);
        new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                    img_table.setVisibility(View.VISIBLE);
                    img_table.startAnimation(RightSwipe);
                }
            },300);
           new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                    txtArticle.setVisibility(View.VISIBLE);
                    txtArticle.startAnimation(RightSwipe);
                }
            },400);
           new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                    txtDesc.setVisibility(View.VISIBLE);
                    txtDesc.startAnimation(RightSwipe);
                }
            },500);




        }
    }
}
}
