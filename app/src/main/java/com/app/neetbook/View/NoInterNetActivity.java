package com.app.neetbook.View;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.app.neetbook.R;
import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.Utils.Data.EnterPinData;
import com.app.neetbook.Utils.Data.LoginData;
import com.app.neetbook.Utils.PrefManager;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.widgets.DisplayOrientation;
import com.app.neetbook.View.SideMenu.TabMainActivity;

public class NoInterNetActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnRefresh;
    ConnectionDetector cd;
    SessionManager sessionManager;
    PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TabMainActivity.isDestroyed = true;
        if(DisplayOrientation.getDisplayOrientation(NoInterNetActivity.this) == 1 || DisplayOrientation.getDisplayOrientation(NoInterNetActivity.this) == 3) {
            setContentView(R.layout.activity_no_inter_net);
        }else if(getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(NoInterNetActivity.this) == 2 || DisplayOrientation.getDisplayOrientation(NoInterNetActivity.this) ==0)) {
            setContentView(R.layout.activity_no_inter_net_portrait);
        }else{
            setContentView(R.layout.activity_no_inter_net);
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);}

        init();
    }

    private void init() {
        cd = new ConnectionDetector(NoInterNetActivity.this);
        sessionManager = new SessionManager(NoInterNetActivity.this);
        prefManager = new PrefManager(this);
        btnRefresh = findViewById(R.id.btnRefresh);
        btnRefresh.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnRefresh:
                if (cd.isConnectingToInternet()) {
                    if (prefManager.isFirstTimeLaunch()){

                        startActivity(new Intent(NoInterNetActivity.this, WalkthroughActivity.class));
                        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                    }

                    else {
                        LoginData.clearData();
                        EnterPinData.clearData();
                        if (sessionManager.isLoggedIn()) {
                            if (sessionManager.getPin() != null && !sessionManager.getPin().equals("") && sessionManager.getPin().length() == 4)
                            {

                                startActivity(new Intent(NoInterNetActivity.this, EnterPin.class));
                                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                            }

                            else{

                                startActivity(new Intent(NoInterNetActivity.this, SignInSighUp.class));
                                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                            }

                        } else {
                            startActivity(new Intent(NoInterNetActivity.this, SignInSighUp.class));
                            overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                        }
                    }
                    finish();
                }
                else {

                }

                break;
        }
    }

    public void onBackPressed()
    {

    }
}
