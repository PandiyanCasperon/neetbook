package com.app.neetbook.View;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.app.neetbook.Adapter.HomeGroupNameAdapter;
import com.app.neetbook.Adapter.HomeGroupReferNowAdapter;
import com.app.neetbook.Adapter.HomeSearchAdapter;
import com.app.neetbook.CartRoom.CartDataBase;
import com.app.neetbook.Interfaces.OnFragmentInteractionListener;
import com.app.neetbook.Interfaces.OnMobileHomeMenuSelectedListioner;
import com.app.neetbook.Interfaces.OnMobileNumberChanged;
import com.app.neetbook.Interfaces.OnPinChanged;
import com.app.neetbook.Interfaces.OnSearchItemClickListener;
import com.app.neetbook.Model.Groupslist;
import com.app.neetbook.Model.ReferNowBean;
import com.app.neetbook.Model.SearchBean;
import com.app.neetbook.Model.TabSideMenu;
import com.app.neetbook.PlaystoreUpate.GooglePlayStoreAppVersionNameLoader;
import com.app.neetbook.PlaystoreUpate.WSCallerVersionListener;
import com.app.neetbook.R;
import com.app.neetbook.SendMessageEvent;
import com.app.neetbook.Utils.ActionBarActivityNeetBook;
import com.app.neetbook.Utils.ApplicationLifecycleHandler;
import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Data.ChangeMobileData;
import com.app.neetbook.Utils.Data.ChangePinData;
import com.app.neetbook.Utils.Data.CompleteProfileData;
import com.app.neetbook.Utils.Data.ContactUsData;
import com.app.neetbook.Utils.Data.DrawerActivityData;
import com.app.neetbook.Utils.Data.FeedBackData;
import com.app.neetbook.Utils.Data.GroupMenuData;
import com.app.neetbook.Utils.Data.HelpData;
import com.app.neetbook.Utils.Data.MyProfileData;
import com.app.neetbook.Utils.Data.SubscriptionFragData;
import com.app.neetbook.Utils.EventBusPojo.ActionEvent;
import com.app.neetbook.Utils.LogOutTimerUtil;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.Utils.ThreadExecutor.AppExecutors;
import com.app.neetbook.Utils.widgets.Advance3DDrawerLayout;
import com.app.neetbook.View.Fragments.AboutUsFragment;
import com.app.neetbook.View.Fragments.ChangeMobileFragment;
import com.app.neetbook.View.Fragments.ChangePinFragment;
import com.app.neetbook.View.Fragments.CompleteProfileFragment;
import com.app.neetbook.View.Fragments.ContactUsFragment;
import com.app.neetbook.View.Fragments.FeedBackFragment;
import com.app.neetbook.View.Fragments.HelpFragment;
import com.app.neetbook.View.Fragments.MobileHomeChildFragment;
import com.app.neetbook.View.Fragments.MyProfileFragment;
import com.app.neetbook.View.Fragments.TabletSideMenuFrament;
import com.app.neetbook.View.Fragments.TermsAndConditions;
import com.app.neetbook.View.SideMenu.SubscriptionDetailActivity;
import com.app.neetbook.View.SideMenu.SubscriptionPointsActivity;
import com.app.neetbook.View.SideMenu.SubscriptionTestActivity;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.app.neetbook.View.SideMenu.collMainActivity;
import com.app.neetbook.homepageRoom.GroupslistDatabase;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.tabs.TabLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;


public class DrawerContentSlideActivity extends FragmentActivity implements OnFragmentInteractionListener, OnPinChanged, OnMobileNumberChanged, LogOutTimerUtil.LogOutListener, WSCallerVersionListener {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public String TAG = "DrawerContentSlideActivity";
    ViewPagerAdapter viewPagerAdapter;
    Bundle savedInstanceState;
    MobileHomeChildFragment mobileHomeChildFragment;
    Fragment fragmentMenuSelected = null;
    public static DrawerContentSlideActivity drawerContentSlideActivity;
    SessionManager sessionManager;
    Advance3DDrawerLayout drawerLayout;
    LinearLayout content;
    private boolean doubleBackToExitPressedOnce = false;
    RecyclerView mobileHomeRecyclerViewHorizontal;
    ArrayList<ReferNowBean> referNowList;
    HomeGroupReferNowAdapter homeGroupReferNowAdapter;
    AppExecutors exector;
    GroupslistDatabase groupslistDatabase;
    private List<Groupslist> homeMenus = new ArrayList<>();
    private int totalCount = 0;
    private int totalCountSearch = 0;
    private int skip = 0;
    private int page = 1;
    private int limit = 10;
    private int loadMoreEnable = 0;
    private CustomAlert customAlert;
    ConnectionDetector cd;
    public static boolean isFirstime = true;
    RecyclerView recyclerViewGroups;
    int lastPosition = 0;
    int isPaused = 0;
    HomeGroupNameAdapter homeGroupNameAdapter;
    LinearLayoutManager layoutManager;
    LinearSnapHelper snapHelper;
    ImageView imgBook;
    private Guideline guide2;
    private Handler handler;
    /*private ConstraintLayout llConstrainSearch;*/
    private CardView cardViewSearch;
    private EditText etSearch;
    private ImageView imgSearchClose, imgSearch;
    private LinearLayout llSearchText;
    private TextView txtSearchPoints, txtSearchTest, txtSearchMCQ, txtSearchArticle;
    private Dialog alertDialog;
    private ArrayList<SearchBean> searchBeanArrayList = new ArrayList<>();
    private HomeSearchAdapter homeSearchAdapter;
    private RecyclerView homeSearchRecyclerView;
    ShimmerFrameLayout mShimmerViewContainer;

    private Animation FadeinAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (getResources().getBoolean(R.bool.isTablet)) {
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            StatusBarColorChange.setStatusBarGradiant(DrawerContentSlideActivity.this);
        }

        super.onCreate(savedInstanceState);

        this.savedInstanceState = savedInstanceState;
        setContentView(R.layout.activity_drawer_content_slide);

        FadeinAnimation = AnimationUtils.loadAnimation(this,
                eu.long1.spacetablayout.R.anim.fade_in);

        ActionBarActivityNeetBook.isSessionOut = "No";
        drawerContentSlideActivity = this;
        sessionManager = new SessionManager(this);
        imgBook = findViewById(R.id.imgBook);
        cd = new ConnectionDetector(DrawerContentSlideActivity.this);
        customAlert = new CustomAlert(this);
        exector = new AppExecutors();
        EventBus.getDefault().register(this);
        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int rotation = display.getRotation();

        if (sessionManager.getBookmarkDetails().get(SessionManager.BOOKMARK_TITLE)!=null){
           if (sessionManager.getBookmarkDetails().get(SessionManager.BOOKMARK_TITLE).equalsIgnoreCase("Test"))
               imgBook.setImageResource(R.drawable.bookmark);
        } else
            imgBook.setImageResource(R.drawable.bookmark_inactive);

        new GooglePlayStoreAppVersionNameLoader(getApplicationContext(), this).execute();

        setNightDayMode();

        if (getResources().getBoolean(R.bool.isTablet) && (rotation == 2 || rotation == 0)) {
            FrameLayout frameLayout = findViewById(R.id.framelayout_left);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) frameLayout.getLayoutParams();
            params.height = FrameLayout.LayoutParams.MATCH_PARENT;
            params.width = getResources().getDimensionPixelSize(R.dimen._45sdp);
            frameLayout.setLayoutParams(params);
        }
        if (!getResources().getBoolean(R.bool.isTablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

            prepareReferNowList();
            createSnap();
            TextView txtHi = findViewById(R.id.txtHi);
            TextView txtHi1 = findViewById(R.id.txtHi1);
            TextView txtName = findViewById(R.id.txtName);
            TextView txtName1 = findViewById(R.id.txtName1);
            Typeface tfMedium  = Typeface.createFromAsset(this.getAssets(), "fonts/ProximaNovaSemibold.otf");
            txtHi.setTypeface(tfMedium);
            txtHi1.setTypeface(tfMedium);
            txtName.setTypeface(tfMedium);
            txtName1.setTypeface(tfMedium);

            mobileHomeRecyclerViewHorizontal = findViewById(R.id.mobileHomeRecyclerViewHorizontal);
            recyclerViewGroups = findViewById(R.id.recyclerViewGroups);

            layoutManager = new LinearLayoutManager(DrawerContentSlideActivity.this, RecyclerView.HORIZONTAL, false);
            recyclerViewGroups.setLayoutManager(layoutManager);

            prepareSearch();
            txtHi.setVisibility(View.GONE);
            txtHi1.setVisibility(View.GONE);
            cardViewSearch.setVisibility(View.GONE);
            txtName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigateToprofile();
                }
            });
            txtName1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigateToprofile();
                }
            });
            if (sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) == null || sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("")) {
                //txtName.setVisibility(View.GONE);
                txtHi.setVisibility(View.GONE);
                //txtName1.setVisibility(View.GONE);
                txtHi1.setVisibility(View.GONE);
                txtName.setText(sessionManager.getUserDetails().get(SessionManager.KEY_EMAIL));
                txtName1.setText(sessionManager.getUserDetails().get(SessionManager.KEY_EMAIL));
            } else {
                txtName1.setText(sessionManager.getUserDetails().get(SessionManager.KEY_FNAME));
                txtName.setText(sessionManager.getUserDetails().get(SessionManager.KEY_FNAME));
            }

            imgBook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (sessionManager.getBookmarkDetails().get(SessionManager.HEAD_ID) != null && !sessionManager.getBookmarkDetails().get(SessionManager.HEAD_ID).isEmpty()) {
                        sessionManager.setViewToBookMark();
                        if (sessionManager.getBookmarkDetails().get(SessionManager.BOOKMARK_TITLE).equalsIgnoreCase("Article"))
                            startActivity(new Intent(DrawerContentSlideActivity.this, ScrollingActivity.class).putExtra("chapter_id", sessionManager.getBookmarkDetails().get(SessionManager.CHAPTER_ID)).putExtra("chapter_name", sessionManager.getBookmarkDetails().get(SessionManager.CHAPTER_NAME)).putExtra("head_id", sessionManager.getBookmarkDetails().get(SessionManager.HEAD_ID)).putExtra("subject_id", sessionManager.getBookmarkDetails().get(SessionManager.SUBJECT_ID)).putExtra("subject_name", sessionManager.getBookmarkDetails().get(SessionManager.LONG_NAME)).putExtra("superHeading", sessionManager.getBookmarkDetails().get(SessionManager.CHAPTER_SUPERHEADING))
                                    .putExtra("coming_page", "homepage"));
                        else if (sessionManager.getBookmarkDetails().get(SessionManager.BOOKMARK_TITLE).equalsIgnoreCase("Points"))
                            startActivity(new Intent(DrawerContentSlideActivity.this, SubscriptionPointsActivity.class).putExtra("chapter_id", sessionManager.getBookmarkDetails().get(SessionManager.CHAPTER_ID)).putExtra("chapter_name", sessionManager.getBookmarkDetails().get(SessionManager.CHAPTER_NAME)).putExtra("head_id", sessionManager.getBookmarkDetails().get(SessionManager.HEAD_ID)).putExtra("subject_id", sessionManager.getBookmarkDetails().get(SessionManager.SUBJECT_ID)).putExtra("subject_name", sessionManager.getBookmarkDetails().get(SessionManager.LONG_NAME)).putExtra("point_id", sessionManager.getBookmarkDetails().get(SessionManager.POINT_ID))
                                    .putExtra("coming_page", "homepage"));
                        else if (sessionManager.getBookmarkDetails().get(SessionManager.BOOKMARK_TITLE).equalsIgnoreCase("MCQ"))
                            startActivity(new Intent(DrawerContentSlideActivity.this, SubscriptionDetailActivity.class).putExtra("chapter_id", sessionManager.getBookmarkDetails().get(SessionManager.CHAPTER_ID)).putExtra("chapter_name", sessionManager.getBookmarkDetails().get(SessionManager.CHAPTER_NAME)).putExtra("head_id", sessionManager.getBookmarkDetails().get(SessionManager.HEAD_ID)).putExtra("subject_id", sessionManager.getBookmarkDetails().get(SessionManager.SUBJECT_ID)).putExtra("subject_name", sessionManager.getBookmarkDetails().get(SessionManager.LONG_NAME)).putExtra("loading_type", sessionManager.getBookmarkDetails().get(SessionManager.LOADING_TYPE))
                                    .putExtra("coming_page", "homepage"));
                        else if (sessionManager.getBookmarkDetails().get(SessionManager.BOOKMARK_TITLE).equalsIgnoreCase("Test"))
                            /*startActivity(new Intent(DrawerContentSlideActivity.this, collMainActivity.class).putExtra("chapter_id",
                                    sessionManager.getBookmarkDetails().get(SessionManager.CHAPTER_ID)).putExtra("chapter_name",
                                    sessionManager.getBookmarkDetails().get(SessionManager.CHAPTER_NAME)).putExtra("subject_id",
                                    sessionManager.getBookmarkDetails().get(SessionManager.SUBJECT_ID)).putExtra("subject_name", sessionManager.getBookmarkDetails().get(SessionManager.LONG_NAME))
                                    .putExtra("testId", sessionManager.getBookmarkDetailsTest().get(SessionManager.TEST_ID))
                                    .putExtra("type", sessionManager.getBookmarkDetailsTest().get(SessionManager.TEST_TYPE))
                                    .putExtra("no_of_question", sessionManager.getBookmarkDetailsTest().get(SessionManager.TEST_NUM_OF_Q))
                                    .putExtra("isFinished", sessionManager.getBookmarkDetailsTest().get(SessionManager.TEST_IS_FINISHED))
                                    .putExtra("questionType", sessionManager.getBookmarkDetailsTest().get(SessionManager.TEST_Q_TYPE))
                                    .putExtra("strInstruction", sessionManager.getBookmarkDetailsTest().get(SessionManager.TEST_INSTRUCTION))
                                    .putExtra("strLongName", sessionManager.getBookmarkDetailsTest().get(SessionManager.TEST_LONG_NAME))
                                    .putExtra("crt_ans_mark", sessionManager.getBookmarkDetailsTest().get(SessionManager.TEST_C_A__MARK))
                                    .putExtra("wrng_ans_mark", sessionManager.getBookmarkDetailsTest().get(SessionManager.TEST_W_A__MARK))
                                    .putExtra("coming_page", "homepage")
                            );*/
                            startActivity(new Intent(DrawerContentSlideActivity.this, collMainActivity.class)
                                    .putExtra("subject_id",sessionManager.getIncompleteTest().get(SessionManager.SUBJECT_ID))
                                    .putExtra("chapter_id", sessionManager.getIncompleteTest().get(SessionManager.CHAPTER_ID))
                                    .putExtra("chapter_name",sessionManager.getIncompleteTest().get(SessionManager.CHAPTER_NAME1))
                                    .putExtra("subject_name", sessionManager.getIncompleteTest().get(SessionManager.SUBJECT_NAME1))
                                    .putExtra("pending", "1")
                                    .putExtra("coming_page", "homepage")
                                    .putExtra("pendingTestId", sessionManager.getIncompleteTest().get(SessionManager.PENDINGTESTID))
                                    .putExtra("testId",sessionManager.getIncompleteTest().get(SessionManager.TESTID))
                                    .putExtra("type",sessionManager.getIncompleteTest().get(SessionManager.TESTTYPE))
                                    .putExtra("no_of_question", sessionManager.getIncompleteTest().get(SessionManager.NO_OF_QUESTION))
                                    .putExtra("isFinished", sessionManager.getIncompleteTest().get(SessionManager.IS_FINISHED))
                                    .putExtra("questionType", sessionManager.getIncompleteTest().get(SessionManager.QUESTIONTYPE))
                                    .putExtra("strInstruction",sessionManager.getIncompleteTest().get(SessionManager.STRINGSTRUCTION))
                                    .putExtra("strLongName",sessionManager.getIncompleteTest().get(SessionManager.STRLONGNAME))
                                    .putExtra("crt_ans_mark", sessionManager.getIncompleteTest().get(SessionManager.CRT_ANS_MARK))
                                    .putExtra("wrng_ans_mark", sessionManager.getIncompleteTest().get(SessionManager.WRNG_ANS_MARK))
                            );
                    }
                }
            });
            promoListListener();
        }
        TabletSideMenuFrament tabletSideMenuFrament = new TabletSideMenuFrament();

        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.add(R.id.framelayout_left, tabletSideMenuFrament);
        fragmentTransaction.commit();

        setDrawer();

        if (ApplicationLifecycleHandler.manager.getconnected()) {
            //Emit
            SendMessageEvent mevent = new SendMessageEvent();
            try {
                JSONObject mJson = new JSONObject();

                mJson.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
                mJson.put("time", sessionManager.getTime());
                mevent.setMessageObject(mJson);

                Log.e("mJson", "mJson" + mJson);
                //    ApplicationLifecycleHandler.manager.onJoinNeet(mJson);
                ApplicationLifecycleHandler.manager.onJoinNeet(mevent.getMessageObject());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }

    private void prepareSearch() {

        etSearch = findViewById(R.id.etSearch);
        imgSearchClose = findViewById(R.id.imgSearchClose);
        llSearchText = findViewById(R.id.llSearchText);
        imgSearch = findViewById(R.id.imgSearch);
        txtSearchArticle = findViewById(R.id.txtSearchArticle);
        txtSearchPoints = findViewById(R.id.txtSearchPoints);
        txtSearchMCQ = findViewById(R.id.txtSearchMCQ);
        txtSearchTest = findViewById(R.id.txtSearchTest);
        cardViewSearch = findViewById(R.id.cardViewSearch);
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                etSearch.requestFocus();
                cardViewSearch.setVisibility(View.VISIBLE);
                llSearchText.setVisibility(View.GONE);

            }
        });
        imgSearchClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSearch.setText(null);
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                cardViewSearch.setVisibility(View.GONE);
            }
        });
        txtSearchArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSearchList(1, etSearch.getText().toString());
            }
        });
        txtSearchPoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSearchList(2, etSearch.getText().toString());
            }
        });
        txtSearchMCQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSearchList(3, etSearch.getText().toString());
            }
        });
        txtSearchTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSearchList(4, etSearch.getText().toString());
            }
        });
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!etSearch.getText().toString().isEmpty() && etSearch.getText().toString().length() > 3) {
                    llSearchText.setVisibility(View.VISIBLE);
                    txtSearchArticle.setText(s.toString() + " in Article");
                    txtSearchPoints.setText(s.toString() + " in Points");
                    txtSearchMCQ.setText(s.toString() + " in MCQ");
                    txtSearchTest.setText(s.toString() + " in Test");
                } else {
                    llSearchText.setVisibility(View.GONE);
                }
            }
        });
    }

    private void openSearchList(final int type, String searchKey) {
        etSearch.setText("");
        cardViewSearch.setVisibility(View.GONE);
        alertDialog = new Dialog(DrawerContentSlideActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        alertDialog.setContentView(R.layout.homesearchlistdialog);
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(false);
           /* if(alertDialog.getWindow() != null) {
                alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }*/
        homeSearchRecyclerView = alertDialog.findViewById(R.id.homeSearchRecyclerView);
        final RelativeLayout rlEmptySearch = alertDialog.findViewById(R.id.rlEmptySearch);
        final ImageView imgSearchResultClose = alertDialog.findViewById(R.id.imgSearchResultClose);
        imgSearchResultClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog.isShowing())
                    alertDialog.dismiss();
            }
        });
        searchBeanArrayList.clear();
        ServiceRequest mRequest = new ServiceRequest(DrawerContentSlideActivity.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("page", "" + page);
        params.put("search", "" + type);
        params.put("keyword", searchKey);
        mRequest.makeServiceRequest(IConstant.homeSearch, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------homeSearch Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalCountSearch = Integer.parseInt(object.getString("total"));

                        JSONArray testArray = object.getJSONArray("content");
                        if (testArray.length() > 0) {
                            rlEmptySearch.setVisibility(View.GONE);
                            for (int i = 0; i < testArray.length(); i++) {
                                SearchBean searchBean = new SearchBean();
                                JSONObject jsonObject = testArray.getJSONObject(i);
                                searchBean.setStrSubjectId(jsonObject.getString("subject_id"));
                                searchBean.setStrSubjectShortName(jsonObject.getString("subject_short_name"));
                                searchBean.setStrSubjectLongName(jsonObject.getString("subject_long_name"));
                                searchBean.setStrMImage(jsonObject.getString("mob_img"));
                                searchBean.setStrTImage(jsonObject.getString("tab_img"));
                                searchBean.setStrChaptId(jsonObject.getString("chapter_id"));
                                searchBean.setStrChaptLongName(jsonObject.getString("chapter_long_name"));
                                searchBean.setStrChaptShortName(jsonObject.getString("chapter_short_name"));
                                searchBean.setStrHeadId(jsonObject.has("heading_id") ? jsonObject.getString("heading_id") : "");
                                searchBean.setStrHeadLongName(jsonObject.has("heading_long_name") ? jsonObject.getString("heading_long_name") : "");
                                searchBean.setStrHeadShortName(jsonObject.has("heading_short_name") ? jsonObject.getString("heading_short_name") : "");
                                searchBean.setStrIsSubscribed(jsonObject.getString("subscribed_user"));
                                searchBeanArrayList.add(searchBean);
                            }

                            setupSearchAdapter(type);
                        } else {
                            rlEmptySearch.setVisibility(View.VISIBLE);
                        }

                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
                Toast.makeText(DrawerContentSlideActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
            }
        });
        if (!alertDialog.isShowing())
            alertDialog.show();

    }

    private void setupSearchAdapter(int menuType) {

        homeSearchAdapter = new HomeSearchAdapter(searchBeanArrayList, DrawerContentSlideActivity.this, menuType);
        homeSearchRecyclerView.setLayoutManager(new LinearLayoutManager(DrawerContentSlideActivity.this, LinearLayoutManager.VERTICAL, false));
        snapHelper.attachToRecyclerView(homeSearchRecyclerView);
        homeSearchRecyclerView.setAdapter(homeSearchAdapter);
        homeSearchRecyclerView.setVisibility(View.VISIBLE);

        performSearchClick();
    }

    private void performSearchClick() {
        homeSearchAdapter.setOnSearchItemClickListener(new OnSearchItemClickListener() {
            @Override
            public void onSearchItemClicked(int position, int menuType) {
                if (alertDialog != null && alertDialog.isShowing())
                    alertDialog.dismiss();
                startActivity(new Intent(DrawerContentSlideActivity.this, TabMainActivity.class)
                        .putExtra("subjectId", searchBeanArrayList.get(position).getStrSubjectId())
                        .putExtra("subjectName", searchBeanArrayList.get(position).getStrSubjectLongName())
                        .putExtra("isTrial", "0")
                        .putExtra("menuType", "" + menuType)
                        .putExtra("headId", searchBeanArrayList.get(position).getStrHeadId())
                        .putExtra("chapterId", searchBeanArrayList.get(position).getStrChaptId())
                        .putExtra("subjectShortName", searchBeanArrayList.get(position).getStrSubjectShortName())
                        .putExtra("heading_long_name", searchBeanArrayList.get(position).getStrHeadId()));

            }
        });
    }

    private void setNightDayMode() {
        if (sessionManager.isNightModeEnabled()) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
    }

    private void promoListListener() {

        mobileHomeRecyclerViewHorizontal.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();

                boolean endHasBeenReached = lastVisible + 5 >= totalItemCount;
                Log.e("totalItemCount", "" + totalItemCount);
                Log.e("lastVisible", "" + lastVisible);
                Log.e("endHasBeenReached", "" + endHasBeenReached);
                if (lastVisible == totalItemCount - 1 && totalCount > referNowList.size() && loadMoreEnable == 0) {
                    page = page + 1;
                    Log.e("loadMoreSubjects()", "Called");
                    loadMoreEnable = 1;
                    loadMorePromo(totalItemCount);
                }
            }
        });
    }

    private void loadMorePromo(final int lastLoadedPosition) {
        ServiceRequest mRequest = new ServiceRequest(DrawerContentSlideActivity.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("skip", "" + skip);
        params.put("limit", "" + limit);
        params.put("page", "" + page);
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));

        mRequest.makeServiceRequest(IConstant.promoList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------loadMorepromoList Response----------------" + response);
                String sStatus = "";

                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalCount = object.getInt("total");
                        JSONArray jsonArray = object.getJSONArray("content");
                        if (jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                ReferNowBean referNowBean = new ReferNowBean();
                                referNowBean.setStrTitle(jsonObject.getString("disp_name"));
                                referNowBean.setStrId(jsonObject.getString("_id"));
                                referNowBean.setStrPromoName(jsonObject.getString("promo_name"));
                                referNowBean.setStrCouponCode(jsonObject.getString("coupon_code"));
                                referNowBean.setStrDesc("");

                                referNowList.add(referNowBean);

                            }
                            loadMoreEnable = 0;
                            homeGroupReferNowAdapter.notifyDataSetChanged();
                            mobileHomeRecyclerViewHorizontal.scrollToPosition(lastLoadedPosition);
                        }
                    } else if (sStatus.equals("00")) {

                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {

                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), obj.getString("message"));
                    } else {

                        customAlert.showAlertOk(getString(R.string.alert_oops), obj.getString("message"));
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void navigateToprofile() {

        drawerLayout.closeDrawers();
        TabSideMenu tabSideMenu2 = new TabSideMenu();
        tabSideMenu2.setId("2");
        tabSideMenu2.setName(sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) != null && sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("") ? getString(R.string.complete_profile) : getString(R.string.my_profile));
        tabSideMenu2.setImage_id_inactive(R.drawable.complete_proile_inactive);
        tabSideMenu2.setImage_id_acative(R.drawable.complete_profile_active);

        startActivity(new Intent(DrawerContentSlideActivity.this, MobilePagesActivity.class).putExtra("sideMenu", tabSideMenu2));

    }


    private void setDrawer() {
        if (findViewById(R.id.framelayout_right) == null) {
            drawerLayout = (Advance3DDrawerLayout) findViewById(R.id.drawerLayout);
            content = (LinearLayout) findViewById(R.id.content);
            //final TextView txt_option1 = (TextView) findViewById(R.id.txt_option1);
            final ImageView img_toggle = (ImageView) findViewById(R.id.imgHome);

            drawerLayout.useCustomBehavior(GravityCompat.START);
            drawerLayout.useCustomBehavior(GravityCompat.END);

            drawerLayout.setRadius(GravityCompat.START, 25);

            drawerLayout.setViewRotation(Gravity.START, 15);


            /*ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close) {
                private float scaleFactor = 5f;

                @Override
                public void onDrawerSlide(View drawerView, float slideOffset) {
                    super.onDrawerSlide(drawerView, slideOffset);
                    float slideX = drawerView.getWidth() * slideOffset;
                    content.setTranslationX(slideX);
                    content.setScaleX(1 - (slideOffset / scaleFactor));
                    content.setScaleY(1 - (slideOffset / scaleFactor));

                    Log.e("content slideX",""+slideX);
                    Log.e("content Scale X",""+(1 - (slideOffset / scaleFactor)));
                    Log.e("content Scale Y",""+(1 - (slideOffset / scaleFactor)));


                }
            };

            drawerLayout.setScrimColor(Color.TRANSPARENT);
            drawerLayout.setDrawerElevation(5f);
            drawerLayout.addDrawerListener(actionBarDrawerToggle);*/

            img_toggle.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("WrongConstant")
                @Override
                public void onClick(View v) {
                    //drawerLayout.openDrawer(Gravity.LEFT);
                    drawerLayout.openDrawer(Gravity.START, true);
                }
            });


            viewPager = (ViewPager) findViewById(R.id.viewpager);
            mShimmerViewContainer = findViewById(R.id.mShimmerViewContainer12);
            handler = new Handler();

            //setupViewPager(viewPager);
        } else {
            selectDrawerMenu(DrawerActivityData.lastSelectedMenuId, DrawerActivityData.bundle);
            if (DrawerActivityData.isFirstTimeLoading) {
                DrawerActivityData.lastSelectedMenuId = "2";
                TabSideMenu tabSideMenu2 = new TabSideMenu();
                tabSideMenu2.setId("2");
                tabSideMenu2.setName(sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) != null && sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("") ? getString(R.string.complete_profile) : getString(R.string.my_profile));
                tabSideMenu2.setImage_id_inactive(R.drawable.complete_proile_inactive);
                tabSideMenu2.setImage_id_acative(R.drawable.complete_profile_active);
                Bundle bundle = new Bundle();
                bundle.putSerializable("sideMenu", tabSideMenu2);
                DrawerActivityData.bundle = bundle;
                fragmentMenuSelected = (sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) == null || sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("")) ? new CompleteProfileFragment() : new MyProfileFragment();
                fragmentMenuSelected.setArguments(bundle);
                DrawerActivityData.currentIndex = 1;
                setFragment("2", fragmentMenuSelected);
            }
        }
    }

    private void selectDrawerMenu(String drawerId, Bundle bundle) {
        switch (drawerId) {

            case "1":
                if (bundle == null) {
                    bundle = new Bundle();
                    TabSideMenu tabSideMenu = new TabSideMenu();
                    tabSideMenu.setId("1");
                    tabSideMenu.setImage_id_acative(R.drawable.home_inactive);
                    tabSideMenu.setImage_id_inactive(R.drawable.home_inactive);
                    tabSideMenu.setName(getString(R.string.home));

                    bundle.putSerializable("sideMenu", tabSideMenu);
                }
                GroupMenuData.clearData();

                DrawerActivityData.bundle = bundle;
                fragmentMenuSelected = (sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) == null || sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("")) ? new CompleteProfileFragment() : new MyProfileFragment();
                fragmentMenuSelected.setArguments(bundle);
                DrawerActivityData.currentIndex = 1;
                DrawerActivityData.lastSelectedMenuId = "2";
                DrawerActivityData.isFirstTimeLoading = false;
                startActivity(new Intent(DrawerContentSlideActivity.this, TabletHome.class));

                break;
            case "2":
                DrawerActivityData.lastSelectedMenuId = drawerId;
                DrawerActivityData.bundle = bundle;
                fragmentMenuSelected = (sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) == null || sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("")) ? new CompleteProfileFragment() : new MyProfileFragment();
                fragmentMenuSelected.setArguments(bundle);
                DrawerActivityData.currentIndex = 1;
                DrawerActivityData.isFirstTimeLoading = false;
                break;
            case "3":
                DrawerActivityData.lastSelectedMenuId = drawerId;
                DrawerActivityData.bundle = bundle;
                DrawerActivityData.currentIndex = 2;
                fragmentMenuSelected = new ChangePinFragment();
                fragmentMenuSelected.setArguments(bundle);
                DrawerActivityData.isFirstTimeLoading = false;
                break;

            case "4":
                DrawerActivityData.currentIndex = 3;
                DrawerActivityData.lastSelectedMenuId = drawerId;
                DrawerActivityData.bundle = bundle;
                fragmentMenuSelected = new ChangeMobileFragment();
                fragmentMenuSelected.setArguments(bundle);
                DrawerActivityData.isFirstTimeLoading = false;
                break;

            case "5":
                DrawerActivityData.currentIndex = 4;
                DrawerActivityData.lastSelectedMenuId = drawerId;
                DrawerActivityData.bundle = bundle;
                fragmentMenuSelected = new AboutUsFragment();
                fragmentMenuSelected.setArguments(bundle);
                DrawerActivityData.isFirstTimeLoading = false;
                break;

            case "6":
                DrawerActivityData.currentIndex = 5;
                DrawerActivityData.lastSelectedMenuId = drawerId;
                DrawerActivityData.bundle = bundle;
                fragmentMenuSelected = new TermsAndConditions();
                fragmentMenuSelected.setArguments(bundle);
                DrawerActivityData.isFirstTimeLoading = false;
                break;

            case "7":
                DrawerActivityData.currentIndex = 6;
                DrawerActivityData.lastSelectedMenuId = drawerId;
                DrawerActivityData.bundle = bundle;
                DrawerActivityData.isFirstTimeLoading = false;
                fragmentMenuSelected = new HelpFragment();
                fragmentMenuSelected.setArguments(bundle);
                break;

            case "8":
                DrawerActivityData.currentIndex = 7;
                DrawerActivityData.lastSelectedMenuId = drawerId;
                DrawerActivityData.bundle = bundle;
                fragmentMenuSelected = new ContactUsFragment();
                fragmentMenuSelected.setArguments(bundle);
                DrawerActivityData.isFirstTimeLoading = false;
                break;
            case "9":
                DrawerActivityData.currentIndex = 8;
                DrawerActivityData.lastSelectedMenuId = drawerId;
                DrawerActivityData.bundle = bundle;
                fragmentMenuSelected = new FeedBackFragment("0", "No", "nill");
                fragmentMenuSelected.setArguments(bundle);
                DrawerActivityData.isFirstTimeLoading = false;
                break;
            case "10":
                logout();
                break;
        }
        if (fragmentMenuSelected != null && !drawerId.equals("10"))
            setFragment(drawerId, fragmentMenuSelected);
    }

    private void logout() {
        DrawerActivityData.ClearDrawerData();
        CustomAlert customAlert = new CustomAlert(DrawerContentSlideActivity.this);
        customAlert.showAlertLogout(getString(R.string.alert_alert), getString(R.string.logout_alert));
    }

    private void setFragment(String id, Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.framelayout_right, fragment);
        fragmentTransaction.commit();
        switch (id) {
            case "1":
                CompleteProfileData.clearData();
                ContactUsData.clearData();
                ChangeMobileData.cleatData();
                ChangePinData.clearData();
                MyProfileData.clearData();
                HelpData.clearData();
                FeedBackData.clearData();
                break;
            case "2":
                ContactUsData.clearData();
                ChangeMobileData.cleatData();
                ChangePinData.clearData();
                HelpData.clearData();
                FeedBackData.clearData();
                break;

            case "3":
                CompleteProfileData.clearData();
                HelpData.clearData();
                ContactUsData.clearData();
                ChangeMobileData.cleatData();
                MyProfileData.clearData();
                FeedBackData.clearData();
                break;
            case "4":
                CompleteProfileData.clearData();
                HelpData.clearData();
                ContactUsData.clearData();
                ChangePinData.clearData();
                MyProfileData.clearData();
                FeedBackData.clearData();
                break;

            case "5":
                CompleteProfileData.clearData();
                HelpData.clearData();
                ContactUsData.clearData();
                ChangeMobileData.cleatData();
                ChangePinData.clearData();
                MyProfileData.clearData();
                FeedBackData.clearData();
                break;
            case "6":
                CompleteProfileData.clearData();
                HelpData.clearData();
                ContactUsData.clearData();
                ChangeMobileData.cleatData();
                ChangePinData.clearData();
                MyProfileData.clearData();
                FeedBackData.clearData();
                break;

            case "7":
                CompleteProfileData.clearData();
                ContactUsData.clearData();
                ChangeMobileData.cleatData();
                ChangePinData.clearData();
                MyProfileData.clearData();
                FeedBackData.clearData();
                break;
            case "8":
                CompleteProfileData.clearData();
                HelpData.clearData();
                ChangeMobileData.cleatData();
                ChangePinData.clearData();
                MyProfileData.clearData();
                FeedBackData.clearData();
                break;
            case "9":
                CompleteProfileData.clearData();
                ChangeMobileData.cleatData();
                ChangePinData.clearData();
                MyProfileData.clearData();
                HelpData.clearData();
                break;
            case "10":
                CompleteProfileData.clearData();
                ContactUsData.clearData();
                ChangeMobileData.cleatData();
                ChangePinData.clearData();
                MyProfileData.clearData();
                HelpData.clearData();
                FeedBackData.clearData();
                break;
        }

    }

    private void setupViewPager(final ViewPager viewPager) {

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), DrawerContentSlideActivity.this);

        for (int i = 0; i < homeMenus.size(); i++) {

            MobileHomeChildFragment fView = new MobileHomeChildFragment(homeMenus.get(i).getHomegroupid(), DrawerContentSlideActivity.this);
            Bundle bundle = new Bundle();
            bundle.putString("groupId", homeMenus.get(i).getHomegroupid());
            fView.setArguments(bundle);
            viewPagerAdapter.addFrag(fView, homeMenus.get(i).getHomeshortName());
        }

        if (homeMenus.size() == 0) {

            MobileHomeChildFragment fView = new MobileHomeChildFragment();
            Bundle bundle = new Bundle();
            bundle.putString("groupId", "");
            fView.setArguments(bundle);
            viewPagerAdapter.addFrag(fView, "");
        }

        viewPager.setAdapter(viewPagerAdapter);
        setGroupAdapter(lastPosition);
        // viewPager.setOffscreenPageLimit(viewPagerAdapter.getCount());
        if (mShimmerViewContainer.getVisibility() == View.VISIBLE) {
            mShimmerViewContainer.setVisibility(View.GONE);
        }
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setTabTextColors(
                ContextCompat.getColor(getApplicationContext(), R.color.white),
                ContextCompat.getColor(getApplicationContext(), R.color.white));

        tabLayout.setupWithViewPager(viewPager);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(viewPagerAdapter.getTabView(i));
        }
        viewPager.setCurrentItem(lastPosition);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
//                Log.e("Clicked",homeMenus.get(tab.getPosition()).getShortName());
//                Log.e("Clicked",homeMenus.get(tab.getPosition()).getGroupid());
//                MobileHomeChildFragment.strGroupId = homeMenus.get(tab.getPosition()).getGroupid();
//                Intent intent = new Intent("STRING_ID_FOR_BRODCAST");
//                intent.putExtra("groupId",homeMenus.get(tab.getPosition()).getGroupid());
//                sendBroadcast(intent);
                //viewPager.getAdapter().notifyDataSetChanged();

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                setGroupAdapter(position);
                layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
                int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                int centeredItemPosition = totalVisibleItems / 2;
                // recyclerViewGroups.smoothScrollToPosition(position != 0 ?position-1 : position);
                // recyclerViewGroups.setScrollY(centeredItemPosition);
                scrollToCenter(layoutManager, recyclerViewGroups, position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

     /*   viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                if (position != 0 && position != homeMenus.size() - 1) {
                    page.setAlpha(0f);
                    page.setVisibility(View.VISIBLE);

                    // Start Animation for a short period of time
                    page.animate()
                            .alpha(1f)
                            .setDuration(page.getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }
        });*/

        // viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
    }

    private void setGroupAdapter(final int position) {

        if (homeGroupNameAdapter == null) {
            homeGroupNameAdapter = new HomeGroupNameAdapter(homeMenus, DrawerContentSlideActivity.this, position);

            //snapHelper.attachToRecyclerView(recyclerViewGroups);
            recyclerViewGroups.setAdapter(homeGroupNameAdapter);

            recyclerViewGroups.scrollToPosition(position);

        } else {
            if (recyclerViewGroups.getVisibility() == View.GONE || recyclerViewGroups.getVisibility() == View.INVISIBLE)
                recyclerViewGroups.setVisibility(View.VISIBLE);
            homeGroupNameAdapter.notifyPosition(position);
            homeGroupNameAdapter.notifyAll(homeMenus);
            homeGroupNameAdapter.notifyDataSetChanged();
            scrollToCenter(layoutManager, recyclerViewGroups, position);

        }

      /*  if(position != 1 || isPaused == 1) {
            homeGroupNameAdapter = null;
            isPaused = 0;
            homeGroupNameAdapter = new HomeGroupNameAdapter(homeMenus, DrawerContentSlideActivity.this, position);
            // recyclerViewGroups.setLayoutManager(new LinearLayoutManager(DrawerContentSlideActivity.this, RecyclerView.HORIZONTAL, false));
            LinearLayoutManager lm = new LinearLayoutManager(DrawerContentSlideActivity.this, RecyclerView.HORIZONTAL, false);
            lm.setStackFromEnd(true);
            recyclerViewGroups.setLayoutManager(lm);

            recyclerViewGroups.setAdapter(homeGroupNameAdapter);
            recyclerViewGroups.scrollToPosition(position);
        }else
            homeGroupNameAdapter.setCurrentSelectedPosition(position);*/


        //  recyclerViewGroups.smoothScrollToPosition(position);
        /*handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                recyclerViewGroups.setVisibility(View.VISIBLE);
            }
        },1000);*/

        homeGroupNameAdapter.setOnGroupMenuSelectedListener(new OnMobileHomeMenuSelectedListioner() {
            @Override
            public void onGroupSelected(String groupId, int position) {

                viewPager.setVisibility(View.GONE);


                try {

                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            viewPager.setVisibility(View.VISIBLE);
                        }
                    }, 500);

                viewPager.setCurrentItem(position);
                layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
                int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                final int centeredItemPosition = totalVisibleItems / 2;
               /* recyclerViewGroups.smoothScrollToPosition(position != 0 ?position-1 : position);
                recyclerViewGroups.setScrollY(centeredItemPosition);*/

                //smoothScroll(recyclerViewGroups,position,280);

                scrollToCenter(layoutManager, recyclerViewGroups, position);

                  } catch (Exception e) {

                    Log.e("Exception", e.toString());

                }

            }
        });


    }


    public void scrollToCenter(LinearLayoutManager layoutManager, RecyclerView recyclerList, int clickPosition) {
        RecyclerView.SmoothScroller smoothScroller = createSnapScroller(recyclerList, layoutManager);

        if (smoothScroller != null) {
            smoothScroller.setTargetPosition(clickPosition);
            layoutManager.startSmoothScroll(smoothScroller);
        }
    }

    // This number controls the speed of smooth scroll
    private static final float MILLISECONDS_PER_INCH = 200f;

    private final static int DIMENSION = 2;
    private final static int HORIZONTAL = 0;
    private final static int VERTICAL = 1;

    @Nullable
    private LinearSmoothScroller createSnapScroller(RecyclerView mRecyclerView, final RecyclerView.LayoutManager layoutManager) {
        if (!(layoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider)) {
            return null;
        }
        return new LinearSmoothScroller(mRecyclerView.getContext()) {
            @Override
            protected void onTargetFound(View targetView, RecyclerView.State state, Action action) {
                int[] snapDistances = calculateDistanceToFinalSnap(layoutManager, targetView);
                final int dx = snapDistances[HORIZONTAL];
                final int dy = snapDistances[VERTICAL];
                final int time = calculateTimeForDeceleration(Math.max(Math.abs(dx), Math.abs(dy)));
                if (time > 0) {
                    action.update(dx, dy, time, mDecelerateInterpolator);
                }
            }


            @Override
            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
            }
        };
    }


    private int[] calculateDistanceToFinalSnap(@NonNull RecyclerView.LayoutManager layoutManager, @NonNull View targetView) {
        int[] out = new int[DIMENSION];
        if (layoutManager.canScrollHorizontally()) {
            out[HORIZONTAL] = distanceToCenter(layoutManager, targetView,
                    OrientationHelper.createHorizontalHelper(layoutManager));
        }

        if (layoutManager.canScrollVertically()) {
            out[VERTICAL] = distanceToCenter(layoutManager, targetView,
                    OrientationHelper.createHorizontalHelper(layoutManager));
        }
        return out;
    }


    private int distanceToCenter(@NonNull RecyclerView.LayoutManager layoutManager,
                                 @NonNull View targetView, OrientationHelper helper) {
        final int childCenter = helper.getDecoratedStart(targetView)
                + (helper.getDecoratedMeasurement(targetView) / 2);
        final int containerCenter;
        if (layoutManager.getClipToPadding()) {
            containerCenter = helper.getStartAfterPadding() + helper.getTotalSpace() / 2;
        } else {
            containerCenter = helper.getEnd() / 2;
        }
        return childCenter - containerCenter;
    }


    private static void smoothScroll(RecyclerView rv, int toPos, final int duration) throws IllegalArgumentException {

        try {

            final int TARGET_SEEK_SCROLL_DISTANCE_PX = 10000;     // See androidx.recyclerview.widget.LinearSmoothScroller
            int itemHeight = rv.getChildAt(0).getHeight();  // Height of first visible view! NB: ViewGroup method!
            itemHeight = itemHeight + 33;                   // Example pixel Adjustment for decoration?
            int fvPos = ((LinearLayoutManager) rv.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
            int i = Math.abs((fvPos - toPos) * itemHeight);
            if (i == 0) {
                i = (int) Math.abs(rv.getChildAt(0).getY());
            }
            final int totalPix = i;                         // Best guess: Total number of pixels to scroll
            RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(rv.getContext()) {
                @Override
                protected int getVerticalSnapPreference() {
                    return LinearSmoothScroller.SNAP_TO_START;
                }

                @Override
                protected int calculateTimeForScrolling(int dx) {
                    int ms = (int) (duration * dx / (float) totalPix);
                    // Now double the interval for the last fling.
                    if (dx < TARGET_SEEK_SCROLL_DISTANCE_PX) {
                        ms = ms * 2;
                    } // Crude deceleration!
                    //lg(format("For dx=%d we allot %dms", dx, ms));
                    return ms;
                }
            };
            //lg(format("Total pixels from = %d to %d = %d [ itemHeight=%dpix ]", fvPos, toPos, totalPix, itemHeight));
            smoothScroller.setTargetPosition(toPos);
            rv.getLayoutManager().startSmoothScroll(smoothScroller);

        } catch (NullPointerException e) {

            Log.e("Exception", e.toString());

        } catch (Exception e) {

            Log.e("Exception", e.toString());
        }


    }

    @Override
    public void onSideMenuSelected(TabSideMenu tabSideMenu, int position) {

        Bundle bundle = new Bundle();
        bundle.putSerializable("sideMenu", tabSideMenu);
        if (findViewById(R.id.framelayout_right) != null) {

            selectDrawerMenu(tabSideMenu.getId(), bundle);

        } else {

            if (tabSideMenu.getId().equals("10"))
                logout();
            else {
                SubscriptionFragData.clearData();
//                startActivity(new Intent(DrawerContentSlideActivity.this, MobilePagesActivity.class).putExtra("sideMenu", tabSideMenu));
                if (tabSideMenu.getId().equals("11") && sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) != null && sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("")) {
                    showAlertOOps(getString(R.string.alert_oops), "Complete your profile");

                } else
                    startActivity(new Intent(DrawerContentSlideActivity.this, MobilePagesActivity.class).putExtra("sideMenu", tabSideMenu));
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    drawerLayout.closeDrawers();
                }
            }, 1000);

        }
    }

    private void showAlertOOps(String title, String desc) {
        final Dialog alertDialog = new Dialog(DrawerContentSlideActivity.this);
        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(title);
        textViewDesc.setText(desc);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (getResources().getBoolean(R.bool.isTablet)) {
                    TabletHome.PagesActivity.finish();
                    DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                    DrawerActivityData.lastSelectedMenuId = "2";
                    DrawerActivityData.currentIndex = 1;
                    Bundle bundle1 = new Bundle();
                    TabSideMenu tabSideMenu = new TabSideMenu();
                    tabSideMenu.setId("2");
                    tabSideMenu.setImage_id_acative(R.drawable.complete_proile_inactive);
                    tabSideMenu.setImage_id_inactive(R.drawable.complete_profile_active);
                    tabSideMenu.setName(getString(R.string.complete_profile));
                    bundle1.putSerializable("sideMenu", tabSideMenu);
                    DrawerActivityData.bundle = bundle1;
                    selectDrawerMenu(tabSideMenu.getId(), bundle1);
                    //startActivity(new Intent(getActivity(), DrawerContentSlideActivity.class).putExtra("sideMenu", tabSideMenu));
                } else {
                    TabSideMenu tabSideMenu2 = new TabSideMenu();
                    tabSideMenu2.setId("2");
                    tabSideMenu2.setName(sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) != null && sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("") ? getString(R.string.complete_profile) : getString(R.string.my_profile));
                    tabSideMenu2.setImage_id_inactive(R.drawable.complete_proile_inactive);
                    tabSideMenu2.setImage_id_acative(R.drawable.complete_profile_active);
                    startActivity(new Intent(DrawerContentSlideActivity.this, MobilePagesActivity.class).putExtra("sideMenu", tabSideMenu2));

                }
            }
        });
        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    @Override
    public void onPinChanged() {
        if (getResources().getBoolean(R.bool.isTablet)) {
            DrawerActivityData.currentIndex = 1;
            TabSideMenu tabSideMenu = new TabSideMenu();
            tabSideMenu.setId("2");
            tabSideMenu.setImage_id_acative(R.drawable.home_inactive);
            tabSideMenu.setImage_id_inactive(R.drawable.home_inactive);
            tabSideMenu.setName(getString(R.string.home));
            Bundle bundle = new Bundle();
            selectDrawerMenu("2", bundle);
            finish();
            startActivity(getIntent());
        }
    }

    @Override
    public void onMobileNumberChanged() {
        if (getResources().getBoolean(R.bool.isTablet)) {
            DrawerActivityData.currentIndex = 1;
            TabSideMenu tabSideMenu = new TabSideMenu();
            tabSideMenu.setId("2");
            tabSideMenu.setImage_id_acative(R.drawable.home_inactive);
            tabSideMenu.setImage_id_inactive(R.drawable.home_inactive);
            tabSideMenu.setName(getString(R.string.home));
            Bundle bundle = new Bundle();
            selectDrawerMenu("2", bundle);
            finish();
            startActivity(getIntent());
        }
    }


    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<MobileHomeChildFragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        Context c;

        public ViewPagerAdapter(FragmentManager manager, Context c) {
            super(manager);
            this.c = c;
        }


        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(MobileHomeChildFragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(c).inflate(R.layout.custom_tab_items, null);
            TextView tv = (TextView) v.findViewById(R.id.txtTabTitle);
            tv.setText(mFragmentTitleList.get(position));
            return v;
        }

        @Override
        public Fragment getItem(int position) {
            if (homeMenus.size() == 0) {

                return new MobileHomeChildFragment();
            }
            if (homeMenus.size() == position)
                position = position - 1;
            return new MobileHomeChildFragment(homeMenus.get(position).getHomegroupid(), DrawerContentSlideActivity.this);
        }

      /*  @Override
        public int getItemPosition(Object object) {
            MobileHomeChildFragment f = (MobileHomeChildFragment) object;
            if (f != null) {
                f.update();
            }
            return super.getItemPosition(object);
        }*/
    }

    private void prepareReferNowList() {
        referNowList = new ArrayList<ReferNowBean>();
        referNowList.clear();
        ServiceRequest mRequest = new ServiceRequest(DrawerContentSlideActivity.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("skip", "" + skip);
        params.put("limit", "" + limit);
        params.put("page", "" + page);
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));

        mRequest.makeServiceRequest(IConstant.promoList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------promoList Response----------------" + response);
                String sStatus = "";

                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalCount = object.getInt("total");
                        JSONArray jsonArray = object.getJSONArray("content");
                        if (jsonArray.length() > 0) {
                            // mobileHomeRecyclerViewHorizontal.setVisibility(View.VISIBLE);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                ReferNowBean referNowBean = new ReferNowBean();
                                referNowBean.setStrTitle(jsonObject.getString("disp_name"));
                                referNowBean.setStrId(jsonObject.getString("_id"));
                                referNowBean.setStrPromoName(jsonObject.getString("promo_name"));
                                referNowBean.setStrCouponCode(jsonObject.getString("coupon_code"));
                                referNowBean.setStrDesc("");

                                referNowList.add(referNowBean);

                            }
                            setReferNoewListAdapter();
                        } else
                            mobileHomeRecyclerViewHorizontal.setVisibility(View.GONE);
                    } else if (sStatus.equals("00")) {

                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {

                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), obj.getString("message"));
                    } else {

                        customAlert.showAlertOk(getString(R.string.alert_oops), obj.getString("message"));
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                Log.e("Error", errorMessage);
                customAlert.showAlertOk(getString(R.string.alert_oops), errorMessage);
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void setReferNoewListAdapter() {
        homeGroupReferNowAdapter = new HomeGroupReferNowAdapter(referNowList, DrawerContentSlideActivity.this);
        mobileHomeRecyclerViewHorizontal.setLayoutManager(new LinearLayoutManager(DrawerContentSlideActivity.this, RecyclerView.HORIZONTAL, false));
        mobileHomeRecyclerViewHorizontal.setAdapter(homeGroupReferNowAdapter);
        homeGroupReferNowAdapter.notifyDataSetChanged();
    }

    private class AsyncGroupRunner extends AsyncTask<String, String, String> {
        GroupslistDatabase groupslistDatabase = GroupslistDatabase.getAppDatabase(getApplicationContext());
        String result = "";
        List<Groupslist> groupslists = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            homeMenus.clear();
            //  setupViewPager(viewPager);
        }

        @Override
        protected String doInBackground(String... strings) {
            ServiceRequest mRequest = new ServiceRequest(DrawerContentSlideActivity.this);
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
            params.put("page_type", "1");
            params.put("page", "1");
            mRequest.makeServiceRequest(IConstant.groupList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("------------groupList Response----------------" + response);
                    String sStatus = "";
                    try {

                        JSONObject object = new JSONObject(response);
                        sStatus = object.getString("status");

                        groupslistDatabase.groupslistDao().nukeTable();
                        if (sStatus.equalsIgnoreCase("1")) {
                            groupslists.clear();
                            JSONObject jsonObject = new JSONObject(response);
                            final JSONArray result = jsonObject.getJSONObject("response").getJSONArray("groupslist");
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    TabSideMenu tabSideMenu = new TabSideMenu();
                                    tabSideMenu.setName(result.getJSONObject(i).getString("g_name"));
                                    tabSideMenu.setShortName(result.getJSONObject(i).getString("short_name"));
                                    tabSideMenu.setLongName(result.getJSONObject(i).getString("long_name"));
                                    tabSideMenu.setId(result.getJSONObject(i).getString("_id"));

                                    Groupslist groupslist = new Groupslist();
                                    groupslist.setHomegroupid(result.getJSONObject(i).getString("_id"));
                                    groupslist.setHomegroupname(result.getJSONObject(i).getString("g_name"));
                                    groupslist.setHomeshortName(result.getJSONObject(i).getString("short_name"));
                                    groupslist.setHomelongName(result.getJSONObject(i).getString("long_name"));

                                    Groupslist subscriptionCart = groupslistDatabase.groupslistDao().getGroupItem(result.getJSONObject(i).getString("_id"));
                                    if (subscriptionCart != null && subscriptionCart.getHomegroupid() != null && !subscriptionCart.getHomegroupid().isEmpty() && subscriptionCart.getHomegroupid().equalsIgnoreCase(result.getJSONObject(i).getString("_id")))
                                        groupslistDatabase.groupslistDao().update(groupslist);
                                    else {
                                        groupslistDatabase.groupslistDao().insertAll(groupslist);
                                    }
//                                    Groupslist groupslist = new Groupslist(result.getJSONObject(i).getString("_id"),
//                                            result.getJSONObject(i).getString("g_name"),
//                                            result.getJSONObject(i).getString("short_name"),
//                                            result.getJSONObject(i).getString("long_name"));
//                                    groupslists.add(groupslist);
                                }

                                try {

//                                    exector.diskIO().execute(new Runnable() {
//                                        @Override
//                                        public void run() {
//
//                                            Groups groupslist = new Groups(groupslists);
//                                            groupslistDatabase.groupslistDao().insertGroupsList(groupslist);
//                                        }
//                                    });

                                } catch (Exception e) {

                                    System.out.println("Exception---" + e.toString());

                                }
                            }


                            getGroupList();

                        } else {
                        }
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                }

                @Override
                public void onErrorListener(String errorMessage) {
                    // AlertBox.showSnackBoxPrimaryBa(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
                }
            });
            return result;


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            // new AsyncSubjectsRunner(s).execute();
        }
    }

    private void getGroupList() {
        try {
            groupslistDatabase = GroupslistDatabase.getAppDatabase(getApplicationContext());
            exector.diskIO().execute(new Runnable() {
                @Override
                public void run() {

                    try {
                        homeMenus.clear();
                        homeMenus = groupslistDatabase.groupslistDao().getAll();

                        ActionEvent event = new ActionEvent();
                        event.setAction("success1");
                        EventBus.getDefault().post(event);
                        //  homeGroupNameAdapter.notifyAll(homeMenus);

                    } catch (Exception e) {

                        System.out.println("Exception---" + e.toString());
                    }

                }
            });

        } catch (Exception e) {

            System.out.println("Exception---" + e.toString());

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        ActionBarActivityNeetBook.isInBackground = "No";
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ActionEvent event) {

        if (event.getAction().equals("success1")) {
            if (!getResources().getBoolean(R.bool.isTablet))
                setupViewPager(viewPager);

        } else if (event.getAction().equals("viewpagerload")) {

            if (recyclerViewGroups.getVisibility() == View.GONE) {

                recyclerViewGroups.setVisibility(View.VISIBLE);
                recyclerViewGroups.startAnimation(FadeinAnimation);
            }

            if (mobileHomeRecyclerViewHorizontal.getVisibility() == View.GONE) {

                mobileHomeRecyclerViewHorizontal.setVisibility(View.VISIBLE);

                mobileHomeRecyclerViewHorizontal.startAnimation(FadeinAnimation);
            }


        }
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_UP) {
            final View view = getCurrentFocus();

            if (view != null) {
                final boolean consumed = super.dispatchTouchEvent(ev);

                final View viewTmp = getCurrentFocus();
                final View viewNew = viewTmp != null ? viewTmp : view;

                if (viewNew.equals(view)) {
                    final Rect rect = new Rect();
                    final int[] coordinates = new int[2];

                    view.getLocationOnScreen(coordinates);

                    rect.set(coordinates[0], coordinates[1], coordinates[0] + view.getWidth(), coordinates[1] + view.getHeight());

                    final int x = (int) ev.getX();
                    final int y = (int) ev.getY();

                    if (rect.contains(x, y)) {
                        return consumed;
                    }
                } else if (viewNew instanceof EditText || viewNew instanceof EditText) {
                    return consumed;
                }

                final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                inputMethodManager.hideSoftInputFromWindow(viewNew.getWindowToken(), 0);

                viewNew.clearFocus();

                return consumed;
            }
        }

        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            CompleteProfileData.clearData();
            ContactUsData.clearData();
            ChangeMobileData.cleatData();
            ChangePinData.clearData();
            MyProfileData.clearData();
            finishAffinity();
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getResources().getString(R.string.on_back_pressed), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 4000);
    }
     /*public void logoutInactivity() {
        Intent intent = new Intent(DrawerContentSlideActivity.this, EnterPin.class);
        intent.putExtra("Background", "Yes");
        startActivity(intent);
        getSharedPreference().edit().remove(ActionBarActivityNeetBook.KEY_SP_LAST_INTERACTION_TIME).apply();
    }
   @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (isValidLogin())
            getSharedPreference().edit().putLong(ActionBarActivityNeetBook.KEY_SP_LAST_INTERACTION_TIME, System.currentTimeMillis()).apply();
        else
            logoutInactivity();
    }
    public SharedPreferences getSharedPreference() {
        return getSharedPreferences(ActionBarActivityNeetBook.PREF_FILE, MODE_PRIVATE);
    }

    public boolean isValidLogin() {
        long last_edit_time = getSharedPreference().getLong(ActionBarActivityNeetBook.KEY_SP_LAST_INTERACTION_TIME, 0);
        return last_edit_time == 0 || System.currentTimeMillis() - last_edit_time < ActionBarActivityNeetBook.TIMEOUT_IN_MILLI;
    }*/


    public void onResume() {
        super.onResume();
        CartDataBase cartlistDatabase = CartDataBase.getAppDatabase(DrawerContentSlideActivity.this);
        cartlistDatabase.cartListDao().nukeTable();
        Log.e("DrawerAct", "onResume()");
        if (ActionBarActivityNeetBook.forceLogout.equalsIgnoreCase("Yes")) {
            ActionBarActivityNeetBook.forceLogout = "No";
            sessionManager.logoutUser();
        } else {
            if (ActionBarActivityNeetBook.isInBackground.equalsIgnoreCase("Yes")) {
                ActionBarActivityNeetBook.isInBackground = "No";
                if (EnterPin.currentView.equalsIgnoreCase("No") && EnterPin.IsShowing.equalsIgnoreCase("true") && !getLocalClassName().equalsIgnoreCase("View.SplashActivity") && !getLocalClassName().equalsIgnoreCase("View.EnterPin") && sessionManager.isLoggedIn()) {
                    LogOutTimerUtil.stopLogoutTimer();
                    Intent intent = new Intent(this, EnterPin.class);
                    intent.putExtra("Background", "Yes");
                    startActivity(intent);
                }
            }
            LogOutTimerUtil.startLogoutTimer(this, this);
        }

        if (sessionManager.getBookmarkDetails().get(SessionManager.BOOKMARK_TITLE)!=null && !sessionManager.getBookmarkDetails().get(SessionManager.BOOKMARK_TITLE).equals("")){
            if (sessionManager.getBookmarkDetails().get(SessionManager.BOOKMARK_TITLE).equalsIgnoreCase("Test"))
                imgBook.setImageResource(R.drawable.bookmark);
        } else
            imgBook.setImageResource(R.drawable.bookmark_inactive);


        if (!getResources().getBoolean(R.bool.isTablet)) {
            new AsyncGroupRunner().execute();
           /* if (sessionManager.getBookmarkDetails().get(SessionManager.HEAD_ID) != null && !sessionManager.getBookmarkDetails().get(SessionManager.HEAD_ID).isEmpty())
                imgBook.setImageResource(R.drawable.bookmark);
            else
                imgBook.setImageResource(R.drawable.bookmark_inactive);*/
        }


    }

    public void onStop() {

        super.onStop();

        ActionBarActivityNeetBook.isInBackground = "No";
        LogOutTimerUtil.startLogoutTimer(this, this);
       /* if(!getResources().getBoolean(R.bool.isTablet)) {
            recyclerViewGroups.setVisibility(View.GONE);
        }*/

    }

    public void onPause() {
        super.onPause();
        Log.e("DrawerAct", "onPause()");
        ActionBarActivityNeetBook.isInBackground = "No";
        LogOutTimerUtil.startLogoutTimer(this, this);
        if (viewPager != null)
            lastPosition = viewPager.getCurrentItem();
        isPaused = 1;
    }


    @Override
    protected void onStart() {
        super.onStart();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        LogOutTimerUtil.startLogoutTimer(this, this);
    }

    @Override
    public void doLogout() {
        try {
            ActionBarActivityNeetBook.isSessionOut = "Yes";
            if (new LogOutTimerUtil.ForegroundCheckTask().execute(this).get()) {
                if (EnterPin.currentView.equalsIgnoreCase("No") && EnterPin.IsShowing.equalsIgnoreCase("true") && !getLocalClassName().equalsIgnoreCase("View.SplashActivity") && !getLocalClassName().equalsIgnoreCase("View.EnterPin") && sessionManager.isLoggedIn()) {
                    LogOutTimerUtil.stopLogoutTimer();
                    Intent intent = new Intent(this, EnterPin.class);
                    intent.putExtra("Background", "Yes");
                    startActivity(intent);
                }
                ActionBarActivityNeetBook.isInBackground = "No";
            } else {
                ActionBarActivityNeetBook.isInBackground = "yes";
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
    }

    @Override
    public void onGetResponse(boolean isUpdateAvailable) {

        Log.e("ResultAPPMAIN", String.valueOf(isUpdateAvailable));
        if (isUpdateAvailable) {
            showUpdateDialog();
        }
    }

    public void showUpdateDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DrawerContentSlideActivity.this);

        alertDialogBuilder.setTitle(DrawerContentSlideActivity.this.getString(R.string.UpdatedTitle));
        alertDialogBuilder.setMessage(DrawerContentSlideActivity.this.getString(R.string.UpdatedMessage));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton(R.string.alert_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (IConstant.isForceUpdate) {
                    finish();
                }
                dialog.dismiss();
            }
        });
        alertDialogBuilder.show();
    }
}
