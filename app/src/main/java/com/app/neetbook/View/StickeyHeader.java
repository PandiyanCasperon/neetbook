package com.app.neetbook.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;

import com.app.neetbook.Adapter.SectionAdapter;
import com.app.neetbook.Interfaces.Section;
import com.app.neetbook.Model.ChildModel;
import com.app.neetbook.Model.HeaderModel;
import com.app.neetbook.R;
import com.shuhart.stickyheader.StickyHeaderItemDecorator;

import java.util.ArrayList;

public class StickeyHeader extends AppCompatActivity {

    RecyclerView recyclerView;

    private String[] vehicleTypes = new String[]{"Cars", "Bikes",
            "Air Crafts","Old Vehicles","Cars", "Bikes",
            "Air Crafts","Old Vehicles","Air Crafts","Air Crafts","Old Vehicles"};

    private ArrayList<Section> sectionArrayList;

    private String[] childnames = new String[]{"Range Rover", "Lamborghini","Range Rover", "Lamborghini","Range Rover", "Lamborghini","Range Rover", "Lamborghini",
            "Rolls Royce","Ferrari","Harley davidson","Ducati","BMW","Honda","Boeing","Airbus","Royal Air","Space X","Horse",
            "Elephant","Camel","Donkey","Khachhar","Horse 2","Camel 2","Donkey 2","Tesla","Mercedes","Range Rover", "Lamborghini",
            "Rolls Royce","Ferrari","Harley davidson","Ducati","BMW","Honda","Boeing","Airbus","Royal Air","Space X","Horse",
            "Elephant","Camel","Donkey","Khachhar","Horse 2","Camel 2","Donkey 2","Tesla","Mercedes"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stickey_header);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        sectionArrayList = new ArrayList<>();
        populateList();

        SectionAdapter adapter = new SectionAdapter(this,sectionArrayList);
        recyclerView.setAdapter(adapter);
        StickyHeaderItemDecorator decorator = new StickyHeaderItemDecorator(adapter);
        decorator.attachToRecyclerView(recyclerView);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }

    private void populateList(){

        int headerdone = 0, childdone = 0;

        for(int i = 0; i < 50; i++){

            if(i == 0 || i == 5 | i == 10 | i == 15| i == 20 | i == 25 ||i == 30 | i == 35 | i == 50| i == 45 | i == 47){

                HeaderModel vehicleModel = new HeaderModel(i);
                vehicleModel.setheader(vehicleTypes[headerdone]);
                sectionArrayList.add(vehicleModel);
                headerdone = headerdone + 1;
            }else {

                ChildModel childModel = null;
                if(i == 1 || i == 2 || i ==3 || i == 4 || i == 6 || i == 7 || i == 8 || i == 9){
                    childModel = new ChildModel(0);
                }else if(i == 6 || i == 7 || i == 8 || i == 9 || i == 19 || i == 20 || i == 21 || i == 22){
                    childModel = new ChildModel(5);
                }else if(i == 11 || i == 12 || i == 13 || i == 14){
                    childModel = new ChildModel(10);
                }else if(i == 15 || i == 16 || i == 17 || i == 18){
                    childModel = new ChildModel(15);
                }else if(i == 19 || i == 20 || i == 21 || i == 22){
                    childModel = new ChildModel(20);
                }else if(i == 23 || i == 24 || i == 25 || i == 26){
                    childModel = new ChildModel(25);
                }else if(i == 27 || i == 28 || i == 29 || i == 30){
                    childModel = new ChildModel(30);
                }else if(i == 31 || i == 32 || i == 33 || i == 34){
                    childModel = new ChildModel(35);
                }else if(i == 35 || i == 36 || i == 37 || i == 38){
                    childModel = new ChildModel(40);
                }else if(i == 39 || i == 40 || i == 41 || i == 42){
                    childModel = new ChildModel(45);
                }else {
                    childModel = new ChildModel(47);
                }

                childModel.setChild(childnames[childdone]);
                sectionArrayList.add(childModel);
                childdone = childdone + 1;
            }
        }

    }
}
