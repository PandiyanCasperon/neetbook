package com.app.neetbook.View.SideMenu;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.app.TabPojo.EventbusPojo;
import com.app.neetbook.Adapter.ArticleSectionAdapter;
import com.app.neetbook.Adapter.HomeSearchAdapter;
import com.app.neetbook.Adapter.NewArticleSectionAdapter;
import com.app.neetbook.Adapter.sideMenuContent.SubscriptionCategoryArticlesAdapter;
import com.app.neetbook.Interfaces.APMTSearchListioner;
import com.app.neetbook.Interfaces.ArticleHeaderSestion;
import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.Interfaces.OnSearchItemClickListener;
import com.app.neetbook.Interfaces.TabArtcileChapterClickListener;
import com.app.neetbook.Model.Artclechat;
import com.app.neetbook.Model.ArticleChildModel;
import com.app.neetbook.Model.ArticleHeaderModel;
import com.app.neetbook.Model.HeadData;
import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.NewSuperListPojo;
import com.app.neetbook.Model.SearchBean;
import com.app.neetbook.Model.SubscriptionActModel;
import com.app.neetbook.Model.SuperDetails;
import com.app.neetbook.Model.sidemenuFromContent.CategoryArticle;
import com.app.neetbook.R;
import com.app.neetbook.StickeyHeader.StickyLinearLayoutManager;
import com.app.neetbook.TAbAdapter.TabFragmentChildRecyclerAdapter;
import com.app.neetbook.TAbAdapter.TabItemLoadAdapter;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.ItemOffsetDecoration;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.SmoothScroll.LinearLayoutManagerWithSmoothScroller;
import com.app.neetbook.Utils.Thread.AppExecutors;
import com.app.neetbook.View.ActivitySubscriptionListDialog;
import com.app.neetbook.View.CustomDialogActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.shuhart.stickyheader.StickyHeaderItemDecorator;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class ArticleFragment extends Fragment implements ItemClickListener, TabFragmentChildRecyclerAdapter.OnHeadingClickListener, APMTSearchListioner, View.OnClickListener, ArticleSectionAdapter.ArticleSectionAdapterListener {


    public Context mContext;
    private ArrayList<NewSuperListPojo> temArrayList;
    private ArrayList<Artclechat> newarticleList = new ArrayList<>();
    private ArrayList<CategoryArticle> articleList = new ArrayList<>();
    List<Object> items = new ArrayList<>();
    private ArrayList<ArticleHeaderSestion> sectionHeaderArticleList = new ArrayList<>();
    private SubscriptionCategoryArticlesAdapter categoryAdapter;
    private ArticleSectionAdapter articleSectionAdapter;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private Context context;
    Loader loader;
    Dialog progressDialog;
    SessionManager sessionManager;
    private CustomAlert customAlert;
    private int page = 1;
    int loadMoreEnable = 0;
    private int totalSubsCount;
    String strSubjectId = "", strSubjectName = "", strisTrial = "", strSubjectShortName = "";
    //  private TextView textView11;
    ImageView imageView8;
    SwipeRefreshLayout mSwipeRefreshLayout;
    ItemOffsetDecoration itemDecoration;

    TabArtcileChapterClickListener tabArtcileChapterClickListener;
    private RecyclerView fragment_child_recyclerview;
//    TextView textView12,textView14,textView13,textView15;
//    ImageView imageView9,imageView10;

    CountDownTimer cTimer;
    Dialog alertDialog;
    LinearSnapHelper snapHelper;
    int index = 0;
    int currentSelectedPosition = 0;
    int firstVisibleInListview = 0;

    /*tab view*/
    private TabFragmentChildRecyclerAdapter child_adapter;
    private List<String> child_item_list;

    private Boolean item_is_selected = false;
    private int selected_parent_position = 0;
    private TabItemLoadAdapter adapter;
    Display display;
    int height1, width1;

    private AppExecutors appexector;
    private String strHeadId = "", strChapterId = "",headLongName;
    private int currentChapterSelectedPosition = -1, currentHeadSelectedPosition = -1, currentSuperHeadSelectedPosition = -1;
    ConstraintLayout llConstrainSearch;
    EditText etSearch;
    private ImageView imgSearchClose, imgSearchGo;
    private HomeSearchAdapter homeSearchAdapter;
    private RecyclerView homeSearchRecyclerView;
    private int totalCountSearch = 0;
    private ArrayList<SearchBean> searchBeanArrayList = new ArrayList<>();

    private LinearLayout place_holder_layout;
    //private ShimmerFrameLayout shimmer_view_container;
    private NewArticleSectionAdapter newSubcriptionListAdapter;
    private ActivitySubscriptionListDialog activitySubscriptionListDialog;
    private ArrayList<SubscriptionActModel> list;
    Handler handler = new Handler();

    public ArticleFragment(String stringExtra, String strSubjectName, String strisTrial, String strSubjectShortName, String strHeadId, String strChapterId,String heading_long_name) {
        strSubjectId = stringExtra;
        this.strSubjectName = strSubjectName;
        this.strSubjectShortName = strSubjectShortName;
        this.strHeadId = strHeadId;
        this.strChapterId = strChapterId;
        this.headLongName=heading_long_name;

    }

    public ArticleFragment(){

    }

    private void CreateLoader() {
        progressDialog = new Dialog(getActivity());
        progressDialog.setContentView(R.layout.custom_progress);
        if (progressDialog.getWindow() != null)
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // startAnimation();
        progressDialog.setCancelable(false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getActivity();
        appexector = new AppExecutors();
        sessionManager = new SessionManager(getActivity());

        /*strSubjectId=sessionManager.getSubjectId();
        strSubjectName=sessionManager.getSubjectName();
        strSubjectShortName=sessionManager.getSubjectShortName();
        strHeadId=sessionManager.getStrHeadId();
        strChapterId=sessionManager.getStrChapterId();
        headLongName=sessionManager.getHeadLongName();*/


        customAlert = new CustomAlert(getActivity());
        loader = new Loader(getActivity());
        alertDialog = new Dialog(getActivity());
        display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width1 = size.x;
        height1 = size.y;
        linearLayoutManager = new LinearLayoutManager(getActivity(),
                RecyclerView.VERTICAL, false);
        createSnap();
        setupStatusBarColor();
    }

    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_article, container, false);
      //  CreateLoader();
        if (getActivity().getResources().getBoolean(R.bool.isTablet))
            initTab(root);
        else
            init(root);
        return root;
    }

    private void initTab(View root) {
        mRecyclerView = root.findViewById(R.id.parent_recyclerview);

        // mSwipeRefreshLayout = root.findViewById(R.id.mSwipeRefreshLayout);


        fragment_child_recyclerview = root.findViewById(R.id.fragment_child_recyclerview);


        page = 1;
        populateList();
        //swipeRefresh();

        LoadMoreListener();
        onScrollRecyclerview();

    }

    public void transformPage(View view, float position) {
        view.setTranslationX(view.getWidth() * -position);

        if (position <= -1.0F || position >= 1.0F) {
            view.setAlpha(0.0F);
        } else if (position == 0.0F) {
            view.setAlpha(1.0F);
        } else {
            // position is between -1.0F & 0.0F OR 0.0F & 1.0F
            view.setAlpha(1.0F - Math.abs(position));
        }
    }

    private void init(View rootView) {
        temArrayList = new ArrayList<>();
        mRecyclerView = rootView.findViewById(R.id.recyclerViewArticle);

        mSwipeRefreshLayout = rootView.findViewById(R.id.mSwipeRefreshLayout);
        mSwipeRefreshLayout.setEnabled(false);
        mSwipeRefreshLayout.setRefreshing(false);

        imageView8 = rootView.findViewById(R.id.imageView8);
        llConstrainSearch = rootView.findViewById(R.id.llConstrainSearch);
        etSearch = rootView.findViewById(R.id.etSearch);
        imgSearchClose = rootView.findViewById(R.id.imgSearchClose);
        imgSearchGo = rootView.findViewById(R.id.imgSearchGo);
        place_holder_layout = rootView.findViewById(R.id.place_holder_layout);
      //  shimmer_view_container = rootView.findViewById(R.id.shimmer_view_container);
      //  shimmer_view_container.startShimmer();


        imgSearchClose.setOnClickListener(this);
        imgSearchGo.setOnClickListener(this);
        itemDecoration = new ItemOffsetDecoration(context, R.dimen._4sdp);
        imageView8.setOnClickListener(this);
        list = new ArrayList();


        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!etSearch.getText().toString().isEmpty() && etSearch.getText().toString().length() > 3) {
                    imgSearchClose.setVisibility(View.GONE);
                    imgSearchGo.setVisibility(View.VISIBLE);
                }
            }
        });
        page = 1;
        populateList();
        swipeRefresh();
       // CreateLoader();
      //  LoadMoreListener();
    }


    private void LoadMoreListener() {

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();
                if (layoutManager.findFirstVisibleItemPosition() >= 0 && sectionHeaderArticleList.size() > 0 && !getActivity().getResources().getBoolean(R.bool.isTablet)) {
                    articleSectionAdapter.setTopItemView(sectionHeaderArticleList.get(layoutManager.findFirstVisibleItemPosition()).getLongName(), sectionHeaderArticleList.get(layoutManager.findFirstVisibleItemPosition()).getCount(), layoutManager.findFirstVisibleItemPosition());
                }
                boolean endHasBeenReached = lastVisible + 5 >= totalItemCount;

                if (dy > 0) {
                    {
                        if (currentSelectedPosition < layoutManager.findFirstVisibleItemPosition())
                            articleSectionAdapter.CollapseExpandedView(-1);
                    }
                } else {
                    if (currentSelectedPosition > lastVisible)
                        articleSectionAdapter.CollapseExpandedView(-1);
                }

                if ((lastVisible == totalItemCount - 1 || lastVisible == totalItemCount - 2) && totalSubsCount > index && loadMoreEnable == 0) {
                    page = page + 1;
                    loadMoreEnable = 1;
                  //  loadMoreArticle(index);
                }
            }
        });
    }

    private void loadMoreArticle(final int totalItemCount) {
    /*if(!progressDialog.isShowing())
      progressDialog.show();*/
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("subject_id", strSubjectId);
        params.put("page", "" + page);
        params.put("skip", "0");
        params.put("limit", "10");

        mRequest.makeServiceRequest(IConstant.articleIndex, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------articleIndex Response----------------" + response);
                String sStatus = "";
                String message = "";
                /*progressDialog.dismiss();*/
                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");

                        JSONArray jsonArray = object.getJSONArray("chapters");
                        if (jsonArray.length() > 0) {
                            //llEmpty.setVisibility(View.GONE);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                index = index + 1;
                                JSONObject chaptersObject = jsonArray.getJSONObject(i);
                                CategoryArticle listItem = new CategoryArticle();

                                listItem._id = chaptersObject.getString("_id");
                                listItem.chapt_name = chaptersObject.getString("chapt_name");
                                listItem.short_name = chaptersObject.getString("short_name");
                                listItem.long_name = chaptersObject.getString("long_name");
                                listItem.images = chaptersObject.getString("img");

                                ArticleHeaderModel articleHeaderModel = new ArticleHeaderModel(index);
                                articleHeaderModel.setHeaderName(chaptersObject.getString("chapt_name"));
                                articleHeaderModel.setShortName(chaptersObject.getString("short_name"));
                                articleHeaderModel.setLongName(chaptersObject.getString("long_name"));
                                articleHeaderModel.setImage(chaptersObject.getString("img"));
                                articleHeaderModel.setId(chaptersObject.getString("_id"));
                                articleHeaderModel.setIsSuperHeading("0");
                                articleHeaderModel.setCount("" + (index));
                                articleHeaderModel.set_expanded(false);
                                sectionHeaderArticleList.add(articleHeaderModel);

                                ArticleChildModel articleChildModel = new ArticleChildModel(index);

                                articleChildModel.setId(chaptersObject.getString("_id"));
                                articleChildModel.setChapterId(chaptersObject.getString("_id"));
                                articleChildModel.setShortName(chaptersObject.getString("short_name"));
                                articleChildModel.setLongName(articleHeaderModel.getLongName());
                                articleChildModel.setHeaderName(chaptersObject.getString("chapt_name"));
                                articleChildModel.setImage(chaptersObject.getString("img"));
                                articleChildModel.setCount("" + (index));
                                ArrayList<HeadDetails> headDetailsArrayList = new ArrayList<HeadDetails>();
                                JSONArray headDetailsArray = chaptersObject.getJSONArray("headDetails");

                                if (headDetailsArray.length() > 0) {
                                    for (int j = 0; j < headDetailsArray.length(); j++) {
                                        HeadDetails headDetails = new HeadDetails();
                                        JSONObject headDetailsObject = headDetailsArray.getJSONObject(j);
                                        headDetails.isSH = headDetailsObject.getString("isSH");
                                        SuperDetails superDetails = new SuperDetails();
                                        if (headDetailsObject.getString("isSH").equals("1")) {
                                            JSONObject superDetailsObject = headDetailsObject.getJSONObject("superDetails");
                                            superDetails._id = superDetailsObject.getString("_id");
                                            superDetails.chapter = superDetailsObject.getString("chapter");
                                            superDetails.supr_head_name = superDetailsObject.getString("supr_head_name");
                                            superDetails.short_name = superDetailsObject.getString("short_name");
                                            superDetails.long_name = superDetailsObject.getString("long_name");
                                            superDetails.firsthead = superDetailsObject.getString("firsthead");
                                            superDetails.head = superDetailsObject.getString("head");

                                        }
                                        headDetails.superDetails = superDetails;
                                        JSONArray headDataArray = headDetailsObject.getJSONArray("headData");
                                        ArrayList<HeadData> headDataArrayList = new ArrayList<HeadData>();
                                        if (headDataArray.length() > 0) {
                                            for (int k = 0; k < headDataArray.length(); k++) {
                                                HeadData headData = new HeadData();
                                                JSONObject headDataObject = headDataArray.getJSONObject(k);
                                                headData._id = headDataObject.getString("_id");
                                                headData.head_name = headDataObject.getString("head_name");
                                                headData.long_name = headDataObject.getString("long_name");
                                                headData.short_name = headDataObject.getString("short_name");
                                                headData.chapter_id = headDataObject.getString("chapter_id");
                                                headData.position = headDataObject.getString("position");
                                                headData.isSH = headDataObject.getString("isSH");
                                                headData.firsthead = headDataObject.getString("firsthead");
                                                headDataArrayList.add(headData);

                                            }
                                        }
                                        headDetails.headData = headDataArrayList;
                                        headDetailsArrayList.add(headDetails);
                                    }
                                }
                                listItem.headDetailsArrayList = headDetailsArrayList;
                                articleList.add(listItem);
                                articleChildModel.setHeadDetails(headDetailsArrayList);
                                sectionHeaderArticleList.add(articleChildModel);
                            }
                            loadMoreEnable = 0;
                            if (getActivity().getResources().getBoolean(R.bool.isTablet))
                                setAdapter(1);
                            else
                                place_holder_layout.setVisibility(View.GONE);
                                setupRecyclerView(totalItemCount);
                        }

                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else if (sStatus.equalsIgnoreCase("2")) {
                        packageExpiredAlert(message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                /*progressDialog.dismiss();*/
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void swipeRefresh() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                loadMoreEnable = 0;
                articleList = new ArrayList<>();
                sectionHeaderArticleList = new ArrayList<>();
                populateList();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void populateList() {

        // place_holder_layout.setVisibility(View.VISIBLE);

        appexector.networkIO().execute(new Runnable() {
            @Override
            public void run() {


                newarticleList.clear();
                articleList.clear();
                sectionHeaderArticleList.clear();
                items.clear();
                index = 0;
                ServiceRequest mRequest = new ServiceRequest(getActivity());
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
                params.put("subject_id", strSubjectId);
                params.put("page", "" + page);
                params.put("skip", "0");
                params.put("limit", "10");
                mRequest.makeServiceRequest(IConstant.articleIndex, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                    @Override
                    public void onCompleteListener(String response) {
                        System.out.println("------------articleIndex Response----------------" + response);
                        String sStatus = "";
                        String message = "";

                        try {

                            JSONObject obj = new JSONObject(response);
                            sStatus = obj.getString("status");
                            message = obj.has("message") ? obj.getString("message") : "";


                            if (sStatus.equalsIgnoreCase("1")) {
                                JSONObject object = obj.getJSONObject("response");
                                totalSubsCount = object.getInt("ctotal");
                                JSONArray jsonArray = object.getJSONArray("chapters");
                                if (jsonArray.length() > 0) {
                                    //llEmpty.setVisibility(View.GONE);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        index = index + 1;
                                        JSONObject chaptersObject = jsonArray.getJSONObject(i);
                                        CategoryArticle listItem = new CategoryArticle();
                                        listItem._id = chaptersObject.getString("_id");
                                        listItem.chapt_name = chaptersObject.getString("chapt_name");
                                        listItem.short_name = chaptersObject.getString("short_name");
                                        listItem.long_name = chaptersObject.getString("long_name");
                                        listItem.images = chaptersObject.getString("img");

                                        ArticleHeaderModel articleHeaderModel = new ArticleHeaderModel(i);
                                        articleHeaderModel.setHeaderName(chaptersObject.getString("chapt_name"));
                                        articleHeaderModel.setShortName(chaptersObject.getString("short_name"));
                                        articleHeaderModel.setLongName(chaptersObject.getString("long_name"));
                                        articleHeaderModel.setImage(chaptersObject.getString("img"));
                                        articleHeaderModel.setId(chaptersObject.getString("_id"));
                                        articleHeaderModel.setIsSuperHeading("0");
                                        articleHeaderModel.setCount("" + (i + 1));
                                        articleHeaderModel.set_expanded(false);
                                        if (chaptersObject.getString("_id").equalsIgnoreCase(strChapterId))
                                            currentChapterSelectedPosition = i;
                                        sectionHeaderArticleList.add(articleHeaderModel);
                                        items.add(articleHeaderModel);
                                        ArticleChildModel articleChildModel = new ArticleChildModel(i);

                                        articleChildModel.setId(chaptersObject.getString("_id"));
                                        articleChildModel.setChapterId(chaptersObject.getString("_id"));
                                        articleChildModel.setShortName(chaptersObject.getString("short_name"));
                                        articleChildModel.setLongName(chaptersObject.getString("long_name"));
                                        articleChildModel.setHeaderName(chaptersObject.getString("chapt_name"));
                                        articleChildModel.setImage(chaptersObject.getString("img"));
                                        articleChildModel.setCount("" + (i + 1));
                                        ArrayList<HeadDetails> headDetailsArrayList = new ArrayList<HeadDetails>();
                                        JSONArray headDetailsArray = chaptersObject.getJSONArray("headDetails");

                                        if (headDetailsArray.length() > 0) {
                                            for (int j = 0; j < headDetailsArray.length(); j++) {
                                                HeadDetails headDetails = new HeadDetails();
                                                JSONObject headDetailsObject = headDetailsArray.getJSONObject(j);
                                                headDetails.isSH = headDetailsObject.getString("isSH");
                                                SuperDetails superDetails = new SuperDetails();
                                                if (headDetailsObject.getString("isSH").equals("1")) {
                                                    JSONObject superDetailsObject = headDetailsObject.getJSONObject("superDetails");
                                                    superDetails._id = superDetailsObject.getString("_id");
                                                    superDetails.chapter = superDetailsObject.getString("chapter");
                                                    superDetails.supr_head_name = superDetailsObject.getString("supr_head_name");
                                                    superDetails.short_name = superDetailsObject.getString("short_name");
                                                    superDetails.long_name = superDetailsObject.getString("long_name");
                                                    superDetails.firsthead = superDetailsObject.getString("firsthead");
                                                    superDetails.head = superDetailsObject.getString("head");
                                                    if (superDetailsObject.has("summary"))
                                                    superDetails.summary = superDetailsObject.getString("summary");
                                                    if (superDetailsObject.has("charcount"))
                                                        superDetails.count = superDetailsObject.getString("charcount");

                                                }
                                                headDetails.superDetails = superDetails;
                                                JSONArray headDataArray = headDetailsObject.getJSONArray("headData");
                                                ArrayList<HeadData> headDataArrayList = new ArrayList<HeadData>();
                                                if (headDataArray.length() > 0) {
                                                    for (int k = 0; k < headDataArray.length(); k++) {
                                                        HeadData headData = new HeadData();
                                                        JSONObject headDataObject = headDataArray.getJSONObject(k);
                                                        headData._id = headDataObject.getString("_id");
                                                        headData.head_name = headDataObject.getString("head_name");
                                                        headData.long_name = headDataObject.getString("long_name");
                                                        headData.short_name = headDataObject.getString("short_name");
                                                        headData.chapter_id = headDataObject.getString("chapter_id");
                                                        headData.position = headDataObject.getString("position");
                                                        headData.isSH = headDataObject.getString("isSH");
                                                        headData.firsthead = headDataObject.getString("firsthead");
                                                        headData.url = headDataObject.getString("url");
                                                        headData.summary = headDataObject.getString("summary");
                                                        if (headDataObject.has("charcount"))
                                                            headData.count = headDataObject.getString("charcount");
                                                        headDataArrayList.add(headData);
                                                        if (headDataObject.getString("isSH").equalsIgnoreCase("1")) {

                                                            if (headDataObject.getString("_id").equalsIgnoreCase(strHeadId)) {
                                                                currentChapterSelectedPosition = i;
                                                                currentHeadSelectedPosition = j;
                                                                currentSuperHeadSelectedPosition = k;
                                                            }


                                                        } else {
                                                            if (headDataObject.getString("_id").equalsIgnoreCase(strHeadId)) {
                                                                currentChapterSelectedPosition = i;
                                                                currentHeadSelectedPosition = j;
                                                                currentSuperHeadSelectedPosition = -1;
                                                            }
                                                        }


                                                        newarticleList.add(new Artclechat(headDataObject.getString("head_name"), headDataObject.getString("long_name"), headDataObject.getString("short_name"), headDataObject.getString("chapter_id"), headDataObject.getString("_id"), headDataObject.getString("position"), headDataObject.getString("isSH"), headDataObject.getString("firsthead"), chaptersObject.getString("_id")));

                                                    }
                                                }
                                                headDetails.headData = headDataArrayList;
                                                headDetailsArrayList.add(headDetails);
                                            }
                                        }
                                        listItem.headDetailsArrayList = headDetailsArrayList;
                                        articleList.add(listItem);

                                        articleChildModel.setHeadDetails(headDetailsArrayList);

                                        sectionHeaderArticleList.add(articleChildModel);
                                        items.add(articleChildModel);
                                    }
                                    loadMoreEnable = 0;

                                    if (getActivity().getResources().getBoolean(R.bool.isTablet))
                                        setAdapter(0);
                                    else
                                 //       shimmer_view_container.setVisibility(View.GONE);
                                       setupRecyclerView(0);
                                }

                            } else if (sStatus.equals("00")) {
                                customAlert.singleLoginAlertLogout();
                            } else if (sStatus.equalsIgnoreCase("01")) {
                                customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                            } else if (sStatus.equalsIgnoreCase("2")) {
                                packageExpiredAlert(message);
                            } else {
                                customAlert.showAlertOk(getString(R.string.alert_oops), message);
                            }

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                       // shimmer_view_container.setVisibility(View.GONE);
                        place_holder_layout.setVisibility(View.GONE);

                    }

                    @Override
                    public void onErrorListener(String errorMessage) {
                        place_holder_layout.setVisibility(View.GONE);
                        // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
                    }
                });

            }
        });


        //setupRecyclerView();
    }

    private void setAdapter(final int loadMore) {

        if (loadMore == 0) {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                    RecyclerView.VERTICAL, false));
            // mRecyclerView.setNestedScrollingEnabled(false);
            adapter = new TabItemLoadAdapter(mContext, articleList, this);
            mRecyclerView.setAdapter(adapter);
            /*set Child View*/
            ChildAdapterSet(0);
            tabArtcileChapterClickListener.onChapterClicked(articleList.get(0).long_name, articleList.get(0)._id, "", "");
        } else {
            adapter.notifyDataSetChanged();
        }

        item_is_selected = true;


  /*  int viewHeight = articleList.size() * adapter.getItemCount();
    mRecyclerView.getLayoutParams().height = 4000;*/


    }

    private void setupRecyclerView(int lastPosition) {

        mRecyclerView.setVisibility(View.VISIBLE);
   /* mRecyclerView.setLayoutManager(linearLayoutManager);
    snapHelper.attachToRecyclerView(mRecyclerView);
    mRecyclerView.removeItemDecoration(itemDecoration);
    mRecyclerView.addItemDecoration(itemDecoration);
    categoryAdapter = new SubscriptionCategoryArticlesAdapter(context, articleList,strSubjectId,strSubjectShortName);
    mRecyclerView.setAdapter(categoryAdapter);
    mRecyclerView.scrollToPosition(lastPosition);*/
        if (lastPosition > 0)
            articleSectionAdapter.notifyDataSetChanged();
        else {



            /*articleSectionAdapter = new ArticleSectionAdapter(context, this::onItemParentListener, sectionHeaderArticleList, strSubjectId, strSubjectShortName, width1, items, currentChapterSelectedPosition, currentHeadSelectedPosition, currentSuperHeadSelectedPosition,
                    this);
            mRecyclerView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(context));
            mRecyclerView.setAdapter(articleSectionAdapter);
            findCategoryList(strChapterId,strSubjectName);*/

            articleSectionAdapter = new ArticleSectionAdapter(context, this::onItemParentListener, sectionHeaderArticleList, strSubjectId, strSubjectShortName, width1, items, currentChapterSelectedPosition, currentHeadSelectedPosition, currentSuperHeadSelectedPosition,
                    this);
          //  StickyLinearLayoutManager stickyLinearLayoutManager = new StickyLinearLayoutManager(getActivity(), articleSectionAdapter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            // mRecyclerView.setLayoutManager(stickyLinearLayoutManager);
            mRecyclerView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(context));

            mRecyclerView.setAdapter(articleSectionAdapter);
            /*StickyHeaderItemDecorator decorator = new StickyHeaderItemDecorator(articleSectionAdapter);
            decorator.attachToRecyclerView(mRecyclerView);*/
            performClickAction(linearLayoutManager);
           /* stickyLinearLayoutManager.setStickyHeaderListener(new StickyLinearLayoutManager.StickyHeaderListener() {
                @Override
                public void headerAttached(View headerView, int adapterPosition) {
                    Log.e("headerAttached", "adapterPosition > " + adapterPosition);
                }

                @Override
                public void headerDetached(View headerView, int adapterPosition) {
                    Log.e("headerDetached", "adapterPosition > " + adapterPosition);
                }
            });*/
            findCategoryList(strChapterId,headLongName);
        }

        if (currentChapterSelectedPosition > 0) {
            // mRecyclerView.scrollToPosition(currentChapterSelectedPosition);
          //  mRecyclerView.smoothScrollToPosition(currentChapterSelectedPosition);
        }

    }

    private void performClickAction(final LinearLayoutManager linearLayoutManager) {
        articleSectionAdapter.setOnItemClickListener(new ItemClickListener() {
            @Override
            public void onParentItemClick(int position) {
                currentSelectedPosition = position;
                int offset = position - linearLayoutManager.findFirstVisibleItemPosition();
                if (linearLayoutManager.findFirstVisibleItemPosition() > 0) offset -= 1;
                // linearLayoutManager.scrollToPositionWithOffset(position, offset);

                smoothScroll(mRecyclerView, position, 280);
            }
        });
    }

    private static void smoothScroll(RecyclerView rv, int toPos, final int duration) throws IllegalArgumentException {
        final int TARGET_SEEK_SCROLL_DISTANCE_PX = 10000;     // See androidx.recyclerview.widget.LinearSmoothScroller
        int itemHeight = rv.getChildAt(0).getHeight();  // Height of first visible view! NB: ViewGroup method!
        itemHeight = itemHeight + 33;                   // Example pixel Adjustment for decoration?
        int fvPos = ((LinearLayoutManager) rv.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        int i = Math.abs((fvPos - toPos) * itemHeight);
        if (i == 0) {
            i = (int) Math.abs(rv.getChildAt(0).getY());
        }
        final int totalPix = i;                         // Best guess: Total number of pixels to scroll
        RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(rv.getContext()) {
            @Override
            protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }

            @Override
            protected int calculateTimeForScrolling(int dx) {
                int ms = (int) (duration * dx / (float) totalPix);
                // Now double the interval for the last fling.
                if (dx < TARGET_SEEK_SCROLL_DISTANCE_PX) {
                    ms = ms * 2;
                } // Crude deceleration!
                //lg(format("For dx=%d we allot %dms", dx, ms));
                return ms;
            }
        };
        //lg(format("Total pixels from = %d to %d = %d [ itemHeight=%dpix ]", fvPos, toPos, totalPix, itemHeight));
        smoothScroller.setTargetPosition(toPos);
        rv.getLayoutManager().startSmoothScroll(smoothScroller);
    }

    private void onScrollRecyclerview() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            mRecyclerView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                    if (item_is_selected) {


                    }

                }
            });

        } else {

            mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    if (item_is_selected) {

                       /* item_is_selected=false;

                        ChildViewGone();*/
                    }
                }
            });
        }
    }

    private void ChildAdapterSet(int SelectedPosition) {

        // fragment_child_recyclerview.setHasFixedSize(true);

        fragment_child_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(),
                RecyclerView.VERTICAL, false));

 /*       LinearLayout.LayoutParams lp =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 1000);
        fragment_child_recyclerview.setLayoutParams(lp);*/


        //fragment_child_recyclerview.setNestedScrollingEnabled(false);

        child_adapter = new TabFragmentChildRecyclerAdapter(getActivity(), articleList, this, SelectedPosition, strSubjectId, strSubjectName);
        fragment_child_recyclerview.setAdapter(child_adapter);

    /*int viewHeight = articleList.size() * child_adapter.getItemCount();
    fragment_child_recyclerview.getLayoutParams().height = 4000;*/
    }

    private void setupStatusBarColor() {
        ((TabMainActivity) context).updateStatusBarColor(context.getResources()
                .getColor(R.color.category_article_header_maroon));
    }

    /*@Override
    public void onAttach(@NonNull Context context) {
      super.onAttach(context);
      this.context = context;
    }*/
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity;
        if (activity instanceof TabArtcileChapterClickListener) {
            tabArtcileChapterClickListener = (TabArtcileChapterClickListener) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        tabArtcileChapterClickListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        linearLayoutManager = new LinearLayoutManager(getActivity(),
                RecyclerView.VERTICAL, false);
    }



    @Override
    public void onItemParentListener(int position, String parentId, String parentLongName, String parentImageUlrl) {
        int parentPosition = 0, totalViews = 0;
        for (int i = 0; i < sectionHeaderArticleList.size(); i++) {
            if (parentId.equalsIgnoreCase(sectionHeaderArticleList.get(i).getId()))
                parentPosition = i;
        }

        temArrayList.clear();
        for (int i = 0; i < sectionHeaderArticleList.get(parentPosition).getHeadList().size(); i++) {
            if (sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).isSH.equalsIgnoreCase("0"))
                temArrayList.add(new NewSuperListPojo("0", sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(0).long_name, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(0)._id, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(0).firsthead, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(0).short_name, parentId, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(0)._id));
            else {
                String chapterId = sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).superDetails._id;
                temArrayList.add(new NewSuperListPojo("1", sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).superDetails.long_name, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).superDetails._id, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).superDetails.firsthead, "", chapterId, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).superDetails._id));
                for (int j = 0; j < sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.size(); j++)
                    temArrayList.add(new NewSuperListPojo("11", sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(j).long_name, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(j)._id, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(j).firsthead, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(j).short_name, chapterId, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(j)._id));
            }
        }

        CustomDialogActivity.sectionHeaderArticleList=sectionHeaderArticleList;
        CustomDialogActivity.temArrayList=temArrayList;
        CustomDialogActivity.ParentPosition=parentPosition/2;
        CustomDialogActivity.strSubjectId=strSubjectId;
        CustomDialogActivity.strSubjectShortName=strSubjectShortName;
        CustomDialogActivity.currentPage=getResources().getString(R.string.artical);
        CustomDialogActivity.MainParentPosition=parentPosition;
        CustomDialogActivity.ParentLongName=parentLongName;
        CustomDialogActivity.ImageUrl=parentImageUlrl;
        CustomDialogActivity.SearchHeadingLongName="";
        //sessionManager.setSearchCategoryTitle("");
        startActivity(new Intent(getActivity(), CustomDialogActivity.class));


        // newSubcriptionListAdapter = new NewArticleSectionAdapter(temArrayList, sectionHeaderArticleList, parentPosition, strSubjectId, strSubjectShortName);
       //sessionManager.setSubscriptionPageDetails(list1,parentLongName,parentImageUlrl,getResources().getString(R.string.artical),strSubjectId,strSubjectShortName,parentPosition);

      //  getActivity().overridePendingTransition(R.anim.dialog_scale_up_anim,R.anim.dialog_scale_down_anim );

       /* activitySubscriptionListDialog = new ActivitySubscriptionListDialog(getActivity(), newSubcriptionListAdapter, parentLongName, parentPosition/2, parentImageUlrl,getResources().getString(R.string.artical));
        activitySubscriptionListDialog.setCanceledOnTouchOutside(true);
        activitySubscriptionListDialog.show();*/
        //  showDialog(getActivity(), newSubcriptionListAdapter, parentLongName, parentPosition/2, parentImageUlrl,getResources().getString(R.string.artical));

    }

    private void findCategoryList(String strChapterId ,String head_Id) {
      if (strChapterId!=null && !strChapterId.equals("")){
          int parentPosition = 0;
          for (int i = 0; i < sectionHeaderArticleList.size(); i++) {
              if (strChapterId.equalsIgnoreCase(sectionHeaderArticleList.get(i).getChapterId()))
                  parentPosition = i;
          }

          temArrayList.clear();
          for (int i = 0; i < sectionHeaderArticleList.get(parentPosition).getHeadList().size(); i++) {
              if (sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).isSH.equalsIgnoreCase("0"))
                  temArrayList.add(new NewSuperListPojo("0", sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(0).long_name, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(0)._id, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(0).firsthead, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(0).short_name, sectionHeaderArticleList.get(parentPosition).getChapterId(), sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(0)._id));
              else {
                  String chapterId = sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).superDetails._id;
                  temArrayList.add(new NewSuperListPojo("1", sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).superDetails.long_name, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).superDetails._id, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).superDetails.firsthead, "", chapterId, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).superDetails._id));
                  for (int j = 0; j < sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.size(); j++)
                      temArrayList.add(new NewSuperListPojo("11", sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(j).long_name, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(j)._id, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(j).firsthead, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(j).short_name, chapterId, sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(j)._id));
              }
          }
          this.strChapterId="";
          this.headLongName="";
          CustomDialogActivity.sectionHeaderArticleList=sectionHeaderArticleList;
          CustomDialogActivity.temArrayList=temArrayList;
          CustomDialogActivity.ParentPosition=parentPosition/2;
          CustomDialogActivity.strSubjectId=strSubjectId;
          CustomDialogActivity.strSubjectShortName=strSubjectShortName;
          CustomDialogActivity.currentPage=getResources().getString(R.string.artical);
          CustomDialogActivity.MainParentPosition=parentPosition;

          CustomDialogActivity.ParentLongName=sectionHeaderArticleList.get(parentPosition).getLongName();
          CustomDialogActivity.ImageUrl=sectionHeaderArticleList.get(parentPosition).getImage();
          CustomDialogActivity.SearchHeadingLongName=head_Id;
          // sessionManager.setSearchCategoryTitle(heading_long_name);
          startActivity(new Intent(getActivity(), CustomDialogActivity.class));
         // getActivity().overridePendingTransition(R.anim.dialog_scale_up_anim,0);
          InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
          assert imm != null;
          imm.hideSoftInputFromWindow(requireView().getWindowToken(), 0);
      }

    }



    @Override
    public void onPause() {

        super.onPause();
        if (cTimer != null && !TabMainActivity.isReadPage) {
            cTimer.cancel();
            cTimer = null;
        }
       /* if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }*/
    }

    @Override
    public void onStop() {
        super.onStop();
        if (cTimer != null && !TabMainActivity.isReadPage) {
            cTimer.cancel();
            cTimer = null;
        }

    }

    private void packageExpiredAlert(String message) {

        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(getString(R.string.alert_oops));
        textViewDesc.setText(message);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
                new AsyncUpdateSessionRunner("1").execute();
                TabMainActivity.tabMainActivity.finish();
            }
        });
        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    @Override
    public void onParentItemClick(int position) {
        ChildAdapterSet(position);
        tabArtcileChapterClickListener.onChapterClicked(articleList.get(position).long_name, articleList.get(position)._id, "", "");
        Log.e("ArticleFragment", "onParentItemClick : " + position);
    }

    @Override
    public void onHeadingClicked(int parentPosition, int childPosition, String headId) {
        EventbusPojo pojo = new EventbusPojo();
        pojo.setChapterId(articleList.get(parentPosition)._id);
        pojo.setHeadId(headId);
        pojo.setChapterName(articleList.get(parentPosition).long_name);
        pojo.setIsSuperHeading(articleList.get(parentPosition).headDetailsArrayList.get(childPosition).isSH);
        pojo.setSuperHeadingName(articleList.get(parentPosition).headDetailsArrayList.get(childPosition).superDetails.long_name);
        pojo.setFragment_name("Article");
        EventBus.getDefault().post(pojo);
    }

    @Override
    public void onSearch(String headId, String ChapterId) {
        Log.e("onSearchItemClicked", "Clicked:" + headId);
        if (sectionHeaderArticleList.size() > 0) {
            for (int i = 0; i < sectionHeaderArticleList.size(); i++) {
                if (sectionHeaderArticleList.get(i).getHeadList().size() > 0) {
                    for (int j = 0; j < sectionHeaderArticleList.get(i).getHeadList().size(); j++) {
                        if (sectionHeaderArticleList.get(i).getHeadList().get(j).isSH.equalsIgnoreCase("1")) {

                            if (sectionHeaderArticleList.get(i).getHeadList().get(j).headData.size() > 0) {
                                for (int k = 0; k < sectionHeaderArticleList.get(i).getHeadList().get(j).headData.size(); k++) {
                                    if (sectionHeaderArticleList.get(i).getHeadList().get(j).headData.get(k)._id.endsWith(headId)) {
                                        currentChapterSelectedPosition = i;
                                        currentHeadSelectedPosition = j;
                                        currentSuperHeadSelectedPosition = k;
                                    }
                                }
                            }
                        } else {
                            if (sectionHeaderArticleList.get(i).getHeadList().get(j).headData.size() > 0) {
                                for (int k = 0; k < sectionHeaderArticleList.get(i).getHeadList().get(j).headData.size(); k++) {
                                    if (sectionHeaderArticleList.get(i).getHeadList().get(j).headData.get(k)._id.endsWith(headId)) {
                                        currentChapterSelectedPosition = i;
                                        currentHeadSelectedPosition = j;
                                        currentSuperHeadSelectedPosition = -1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            articleSectionAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageView8:
                openSearch();
                break;

            case R.id.imgSearchClose:
                closeSearch();
                break;

            case R.id.imgSearchGo:
                searchAPMT();
                break;
        }
    }

    private void openSearch() {
        InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
        etSearch.requestFocus();
        llConstrainSearch.setVisibility(View.VISIBLE);

    }

    private void closeSearch() {
        etSearch.setText(null);
        InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(requireView().getWindowToken(), 0);
        llConstrainSearch.setVisibility(View.GONE);
    }

    private void searchAPMT() {
        llConstrainSearch.setVisibility(View.GONE);
        openSearchList(1, etSearch.getText().toString().trim());
    }

    private void openSearchList(final int type, String searchKey) {
        etSearch.setText("");
        llConstrainSearch.setVisibility(View.GONE);
        alertDialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        alertDialog.setContentView(R.layout.homesearchlistdialog);
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(false);
           /* if(alertDialog.getWindow() != null) {
                alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }*/
        homeSearchRecyclerView = alertDialog.findViewById(R.id.homeSearchRecyclerView);
        final RelativeLayout rlEmptySearch = alertDialog.findViewById(R.id.rlEmptySearch);
        final ImageView imgSearchResultClose = alertDialog.findViewById(R.id.imgSearchResultClose);
        imgSearchResultClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog.isShowing())
                    alertDialog.dismiss();
            }
        });
        searchBeanArrayList.clear();
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("page", "" + page);
        params.put("search", "" + type);
        params.put("keyword", searchKey);
        mRequest.makeServiceRequest(IConstant.homeSearch, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------homeSearch Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalCountSearch = Integer.parseInt(object.getString("total"));

                        JSONArray testArray = object.getJSONArray("content");
                        if (testArray.length() > 0) {
                            rlEmptySearch.setVisibility(View.GONE);
                            for (int i = 0; i < testArray.length(); i++) {
                                SearchBean searchBean = new SearchBean();
                                JSONObject jsonObject = testArray.getJSONObject(i);
                                searchBean.setStrSubjectId(jsonObject.getString("subject_id"));
                                searchBean.setStrSubjectShortName(jsonObject.getString("subject_short_name"));
                                searchBean.setStrSubjectLongName(jsonObject.getString("subject_long_name"));
                                searchBean.setStrMImage(jsonObject.getString("mob_img"));
                                searchBean.setStrTImage(jsonObject.getString("tab_img"));
                                searchBean.setStrChaptId(jsonObject.getString("chapter_id"));
                                if (jsonObject.has("chapter_long_name"))
                                searchBean.setStrChaptLongName(jsonObject.getString("chapter_long_name"));
                                if (jsonObject.has("chapter_short_name"))
                                searchBean.setStrChaptShortName(jsonObject.getString("chapter_short_name"));
                                searchBean.setStrHeadId(jsonObject.has("heading_id") ? jsonObject.getString("heading_id") : "");
                                searchBean.setStrHeadLongName(jsonObject.has("heading_long_name") ? jsonObject.getString("heading_long_name") : "");
                                searchBean.setStrHeadShortName(jsonObject.has("heading_short_name") ? jsonObject.getString("heading_short_name") : "");
                                searchBean.setStrIsSubscribed(jsonObject.getString("subscribed_user"));
                                searchBeanArrayList.add(searchBean);

                            }
                            setupSearchAdapter(type);
                        } else {
                            rlEmptySearch.setVisibility(View.VISIBLE);
                        }

                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }


            @Override
            public void onErrorListener(String errorMessage) {
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);

            }
        });
        if (!alertDialog.isShowing())
            alertDialog.show();

    }

    private void setupSearchAdapter(int menuType) {

        homeSearchAdapter = new HomeSearchAdapter(searchBeanArrayList, getActivity(), menuType);
        homeSearchRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        //snapHelper.attachToRecyclerView(homeSearchRecyclerView);
        homeSearchRecyclerView.setAdapter(homeSearchAdapter);
        homeSearchRecyclerView.setVisibility(View.VISIBLE);
        performSearchClick();
    }

    private void performSearchClick() {
        homeSearchAdapter.setOnSearchItemClickListener(new OnSearchItemClickListener() {
            @Override
            public void onSearchItemClicked(int position, int menuType) {
                if (alertDialog != null && alertDialog.isShowing())
                    alertDialog.dismiss();
                strHeadId = searchBeanArrayList.get(position).getStrHeadId();
                strChapterId = searchBeanArrayList.get(position).getStrChaptId();
               // populateList();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        findCategoryList(searchBeanArrayList.get(position).getStrChaptId(),searchBeanArrayList.get(position).getStrHeadId());
                    }
                } , 200);

            }
        });


    }


    private class AsyncUpdateSessionRunner extends AsyncTask<String, String, String> {
        String result = "";
        String callingType = "";

        public AsyncUpdateSessionRunner(String s) {
            callingType = s;
        }

        @Override
        protected String doInBackground(String... strings) {
            ServiceRequest mRequest = new ServiceRequest(getActivity());
            HashMap<String, String> params = new HashMap<>();
            params.put("session_id", TabMainActivity.strSessionId);

            params.put("time", String.valueOf(TabMainActivity.startingLevelTime - TabMainActivity.packageTime));
            params.put("close", callingType);

            Log.e("startingLevelTime", "" + TabMainActivity.startingLevelTime + " , packageTime : " + TabMainActivity.packageTime);
            TabMainActivity.startingLevelTime = TabMainActivity.packageTime;
            mRequest.makeServiceRequest(IConstant.updateSession, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("updateSession" + "-------- Response----------------" + response);
                    String sStatus = "";
                    String message = "";
                    try {
                        JSONObject obj = new JSONObject(response);
                        sStatus = obj.getString("status");
                        message = obj.has("message") ? obj.getString("message") : "";
                        result = sStatus;

                        if (sStatus.equalsIgnoreCase("1")) {
                            //JSONObject object = obj.getJSONObject("response");


                        } else if (sStatus.equals("00")) {
                            //customAlert.singleLoginAlertLogout();
                        } else if (sStatus.equalsIgnoreCase("01")) {
                            //customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                        } else {
                            //customAlert.showAlertOk(getString(R.string.alert_oops),message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onErrorListener(String errorMessage) {
                    // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
                }
            });
            return result;


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("status", s);

            // new AsyncSubjectsRunner(s).execute();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}