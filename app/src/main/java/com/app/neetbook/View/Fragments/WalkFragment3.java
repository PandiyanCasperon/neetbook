package com.app.neetbook.View.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;

import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.neetbook.R;
import com.app.neetbook.View.WalkthroughActivity;

import org.json.JSONException;

/**
 * A simple {@link Fragment} subclass.
 */
public class WalkFragment3 extends Fragment {

    ImageView imgLeaf,imgBg,imgGirl,imgMobile;
    View view;
    ViewGroup container;
    LayoutInflater inflater;
    TextView txtMcqDesc,txtMcq;
    public WalkFragment3() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Display display = ((WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int rotation = display.getRotation();
        if(rotation == 1)
            view = inflater.inflate(R.layout.fragment_walk_fragment3, container, false);
        else if(getResources().getBoolean(R.bool.isTablet))
            view = inflater.inflate(R.layout.fragment_walk_fragment3_portrait, container, false);
        else
            view = inflater.inflate(R.layout.fragment_walk_fragment3, container, false);
        imgBg = view.findViewById(R.id.imgBg);
        imgGirl = view.findViewById(R.id.imgGirl);
        imgLeaf = view.findViewById(R.id.imgLeaf);
        imgMobile = view.findViewById(R.id.imgMobile);
        txtMcq = view.findViewById(R.id.txtMcq);
        txtMcqDesc = view.findViewById(R.id.txtMcqDesc);
        setTitleAndDesc();
        return view;
    }
    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);

        if(isFragmentVisible_){
            imgGirl.setVisibility(View.VISIBLE);
            imgLeaf.setVisibility(View.GONE);
            imgMobile.setVisibility(View.GONE);
            txtMcq.setVisibility(View.GONE);
            txtMcqDesc.setVisibility(View.GONE);
//
            Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
            imgGirl.setVisibility(View.VISIBLE);
//            imgBg.startAnimation(RightSwipe);
            imgGirl.startAnimation(RightSwipe);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                    imgMobile.setVisibility(View.VISIBLE);
                    imgMobile.startAnimation(RightSwipe);;
                }
            },200);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                    imgLeaf.setVisibility(View.VISIBLE);
                    imgLeaf.startAnimation(RightSwipe);
                }
            },300);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                    txtMcq.setVisibility(View.VISIBLE);
                    txtMcq.startAnimation(RightSwipe);
                }
            },400);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                    txtMcqDesc.setVisibility(View.VISIBLE);
                    txtMcqDesc.startAnimation(RightSwipe);
                }
            },500);


        }else {

        }
    }

//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            view = inflater.inflate(R.layout.fragment_walk_fragment3, container, false);
//            //onSaveInstanceState(newBundy);
//
//        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
//            view = inflater.inflate(R.layout.fragment_walk_fragment3_portrait, container, false);
//            //onSaveInstanceState(newBundy);
//        }
//    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putBundle("newBundy", newBundy);
    }

    private void setTitleAndDesc() {

        try {
            if(WalkthroughActivity.jsonWalkThrough!= null) {
                txtMcq.setText(WalkthroughActivity.jsonWalkThrough.getString("heading3"));
                txtMcqDesc.setText(WalkthroughActivity.jsonWalkThrough.getString("content3"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
