package com.app.neetbook.View;

import android.os.Bundle;

import com.app.neetbook.Adapter.TestTabAdapter;
import com.app.neetbook.R;
import com.app.neetbook.View.Fragments.TestTabFragment;
import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

public class TestActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        viewPager = findViewById(R.id.viewpager);
        TestTabAdapter adapter = new TestTabAdapter(getSupportFragmentManager());
        adapter.addFragment(TestTabFragment.getInstance(0), "Test1");
        adapter.addFragment(TestTabFragment.getInstance(1), "Test2");
        adapter.addFragment(TestTabFragment.getInstance(2), "Test3");
        viewPager.setAdapter(adapter);

        tabLayout = findViewById(R.id.testTabs);
        tabLayout.setupWithViewPager(viewPager);

    }
}
