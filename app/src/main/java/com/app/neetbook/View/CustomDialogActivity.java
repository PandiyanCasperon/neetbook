package com.app.neetbook.View;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.neetbook.Adapter.NewArticleSectionAdapter;
import com.app.neetbook.Adapter.NewMCQExamsSectionAdapter;
import com.app.neetbook.Adapter.NewMCQSectionAdapter;
import com.app.neetbook.Adapter.NewPointsSectionAdapter;
import com.app.neetbook.Adapter.NewTestSectionAdapter;
import com.app.neetbook.Interfaces.ArticleHeaderSestion;
import com.app.neetbook.Interfaces.MCQChapterSection;
import com.app.neetbook.Interfaces.PointsSection;
import com.app.neetbook.Interfaces.TestSection;
import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.NewMcqSuperListPojo;
import com.app.neetbook.Model.NewPointSectionModel;
import com.app.neetbook.Model.NewSuperListPojo;
import com.app.neetbook.Model.sidemenuFromContent.CategoryTest;
import com.app.neetbook.R;
import com.app.neetbook.Utils.EventBusPojo.ActionEvent;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.View.SideMenu.SubscriptionTestFragment;
import com.app.neetbook.View.SideMenu.collMainActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Objects;

public class CustomDialogActivity extends AppCompatActivity implements NewArticleSectionAdapter.setOnItemClick, NewPointsSectionAdapter.OnItemClickInterface, NewMCQSectionAdapter.setOnItemClick, NewMCQExamsSectionAdapter.setOnItemClick, NewTestSectionAdapter.SetOnChildClickListener {
    LinearLayout rootLayout,parentLinearLayout;
    RelativeLayout headerLayout,headerSquareViewLayout;
    TextView categoryPosition,categoryTitle;
    ImageView categoryImageIcon,closeDialogIcon;
    RecyclerView subscriptionChildList;
    SessionManager sessionManager;
    RecyclerView recyclerView;
    int lastReadPosition=0;
    public static int ParentPosition,MainParentPosition,childPosition,intSelectPosition;
    public static  String strSubjectId,strSubjectShortName,currentPage,ParentLongName,ImageUrl,rooId,strLoading_type,strPendingTestId,SearchHeadingLongName;
    public static boolean booleanSelectPosition;
    ///////////////////////// Article
    NewArticleSectionAdapter newSubcriptionListAdapter;
    public static  ArrayList<NewSuperListPojo> temArrayList;
    public static  ArrayList<ArticleHeaderSestion> sectionHeaderArticleList;
    ///////////////////////////Points
    private NewPointsSectionAdapter newPointsSectionAdapter;
    public static ArrayList<NewPointSectionModel> pointsSectionList;
    public static ArrayList<PointsSection> pointsSectionArrayList;
    ////////////////////////////Mcq
    public static ArrayList<MCQChapterSection> mcqChapterSectionArrayList;
    public static ArrayList<NewMcqSuperListPojo> mcqTempArrayList;
    public static ArrayList<HeadDetails> headDetailsArrayList;
    public static ArrayList<MCQChapterSection> mcqExamSectionArrayList ;
    public static ArrayList<MCQChapterSection> mcqYearSectionArrayList;
    public static NewMCQExamsSectionAdapter newMCQExamsSectionAdapter;
    NewMCQSectionAdapter newMCQSectionAdapter;
    //////////////////////////Test
    public static ArrayList<CategoryTest> mList;
    public static ArrayList<TestSection> sectionHeaderTestListList;
    NewTestSectionAdapter newTestSectionAdapter;





    Animation animation,animation1;
    Handler handler = new Handler();



    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_dialog);

        ////////////////////////////// declare part
        sessionManager = new SessionManager(this);
        rootLayout = findViewById(R.id.rootLayout);
        parentLinearLayout = findViewById(R.id.parentLinearLayout);
        headerLayout = findViewById(R.id.headerLayout);
        headerSquareViewLayout = findViewById(R.id.headerSquareViewLayout);
        categoryPosition = findViewById(R.id.categoryPosition);
        categoryTitle = findViewById(R.id.categoryTitle);
        categoryImageIcon = findViewById(R.id.categoryImageIcon);
        closeDialogIcon = findViewById(R.id.closeDialogIcon);
        subscriptionChildList=findViewById(R.id.subscriptionChildList);
        rootLayout = findViewById(R.id.rootLayout);
        recyclerView=findViewById(R.id.subscriptionChildList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        animation = AnimationUtils.loadAnimation(this, R.anim.dialog_scale_up_anim);
        parentLinearLayout.setAnimation(animation);
        parentLinearLayout.animate();
        animation.start();
        /////////////////////////////

       /* View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);*/

        if (currentPage.equals(getResources().getString(R.string.artical))){
            parentLinearLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.article_dialog_root_background));
            headerLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.article_dialog_head_layout_background));
            headerSquareViewLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.article_dialog_head_square));
            categoryPosition.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_curve_articles));
            closeDialogIcon.setColorFilter(ContextCompat.getColor(this, R.color.primary_meroon));
            categoryImageIcon.setColorFilter(ContextCompat.getColor(this, R.color.artical_pink));
            newSubcriptionListAdapter = new NewArticleSectionAdapter(this,this,temArrayList, sectionHeaderArticleList,MainParentPosition,strSubjectId ,strSubjectShortName,SearchHeadingLongName);
            recyclerView.setAdapter(newSubcriptionListAdapter);
           try {
               if (SearchHeadingLongName.equals("")){
                   if (sessionManager.getFindCategoryList()!=null && !sessionManager.getFindCategoryList().equals("")){
                       for (int i=0;i<temArrayList.size();i++){
                           if (sessionManager.getFindCategoryList().equals(temArrayList.get(i).getHead_id()))
                               lastReadPosition=i;
                       }
                   }

               }else {
                   for (int i=0;i<temArrayList.size();i++){
                       if (SearchHeadingLongName.equals(temArrayList.get(i).getHead_id()))
                           lastReadPosition=i;
                   }
               }
               Objects.requireNonNull(recyclerView.getLayoutManager()).scrollToPosition(lastReadPosition);
           }catch (Exception e){
               e.printStackTrace();
           }

            //recyclerView.setVerticalScrollbarThumbDrawable(ContextCompat.getDrawable(this,R.drawable.article_dialog_head_square));
            categoryPosition.setText(String.valueOf(ParentPosition+1));
            categoryTitle.setText(ParentLongName);
            Picasso.with(this).load(IConstant.BaseUrl +ImageUrl).into(categoryImageIcon);
        } else if (currentPage.equals(getResources().getString(R.string.points))){
            parentLinearLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.points_dialog_root_background));
            headerLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.points_dialog_head_layout_background));
            headerSquareViewLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.points_dialog_head_square));
            categoryPosition.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_curve_points));
            closeDialogIcon.setColorFilter(ContextCompat.getColor(this, R.color.points_green));
            categoryImageIcon.setColorFilter(ContextCompat.getColor(this, R.color.new_points_green_dark));
            newPointsSectionAdapter = new NewPointsSectionAdapter(this,this,pointsSectionList,pointsSectionArrayList, strSubjectId, strSubjectShortName,rooId);
            recyclerView.setAdapter(newPointsSectionAdapter);
        }else if (currentPage.equals(getResources().getString(R.string.mcq1))||currentPage.equals(getResources().getString(R.string.mcq2))||currentPage.equals(getResources().getString(R.string.mcq3))){
            parentLinearLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.mcq_dialog_root_background));
            headerLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.mcq_dialog_head_layout_background));
            headerSquareViewLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.mcq_dialog_head_square));
            categoryPosition.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_curve_mcq));
            closeDialogIcon.setColorFilter(ContextCompat.getColor(this, R.color.mcq_blue_dark));
            categoryImageIcon.setColorFilter(ContextCompat.getColor(this, R.color.mcq_blue));
            if (currentPage.equals(getResources().getString(R.string.mcq1))){
                newMCQSectionAdapter = new NewMCQSectionAdapter(this, this, mcqTempArrayList,mcqChapterSectionArrayList, strSubjectId, strSubjectShortName,MainParentPosition,SearchHeadingLongName);
                recyclerView.setAdapter(newMCQSectionAdapter);
                try {
                    if (SearchHeadingLongName.equals("")){
                        if (sessionManager.getMcqLastReadDetails()!=null && !sessionManager.getMcqLastReadDetails().equals("")){
                            for (int i=0;i<mcqTempArrayList.size();i++){
                                if (sessionManager.getFindCategoryList().equals(mcqTempArrayList.get(i).getFirsthead()))
                                    lastReadPosition=i;
                            }
                        }

                    }else {
                        for (int i=0;i<temArrayList.size();i++){
                            if (SearchHeadingLongName.equals(mcqTempArrayList.get(i).getFirsthead()))
                                lastReadPosition=i;
                        }
                    }
                    Objects.requireNonNull(recyclerView.getLayoutManager()).scrollToPosition(lastReadPosition);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }else if(currentPage.equals(getResources().getString(R.string.mcq2))){
                newMCQExamsSectionAdapter=new NewMCQExamsSectionAdapter(this,this,mcqExamSectionArrayList.get(MainParentPosition).getMcqExamsList().size(),mcqExamSectionArrayList,MainParentPosition,strSubjectId, strSubjectShortName,strLoading_type);
                recyclerView.setAdapter(newMCQExamsSectionAdapter);
            }else if (currentPage.equals(getResources().getString(R.string.mcq3))){
                newMCQExamsSectionAdapter=new NewMCQExamsSectionAdapter(this,this,mcqYearSectionArrayList.get(MainParentPosition).getMcqExamsList().size(),mcqYearSectionArrayList,MainParentPosition,strSubjectId, strSubjectShortName,strLoading_type);
                recyclerView.setAdapter(newMCQExamsSectionAdapter);
            }
        }else if (currentPage.equals(getResources().getString(R.string.test))){
            parentLinearLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.test_dialog_root_background));
            headerLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.test_dialog_head_layout_background));
            headerSquareViewLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.test_dialog_head_square));
            categoryPosition.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_curve_test));
            closeDialogIcon.setColorFilter(ContextCompat.getColor(this, R.color.test_yellow));
            categoryImageIcon.setColorFilter(ContextCompat.getColor(this, R.color.test_yellow));
            newTestSectionAdapter = new NewTestSectionAdapter(CustomDialogActivity.this, this, mList, strSubjectId, strSubjectShortName, strPendingTestId, MainParentPosition, mList.get(MainParentPosition).testArrayList.size(), sectionHeaderTestListList, childPosition);
            recyclerView.setAdapter(newTestSectionAdapter);

        }
        categoryPosition.setText(String.valueOf(ParentPosition+1));
        categoryTitle.setText(ParentLongName);
        Picasso.with(this).load(IConstant.BaseUrl +ImageUrl).into(categoryImageIcon);


        categoryImageIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animation1 = AnimationUtils.loadAnimation(CustomDialogActivity.this, R.anim.dialog_scale_down_anim);
                parentLinearLayout.startAnimation(animation1);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                        overridePendingTransition(0,0);
                    }
                } , 800);
            }
        });

        rootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                animation1 = AnimationUtils.loadAnimation(CustomDialogActivity.this, R.anim.dialog_scale_down_anim);
                parentLinearLayout.startAnimation(animation1);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                        overridePendingTransition(0,0);
                    }
                } , 800);

            }
        });
    }

    @Override
    public void onItemClick(String message) {
        if (message.equalsIgnoreCase(getResources().getString(R.string.closedialog))) {
            finish();
        }
    }

    @Override
    public void newOnItemClick(String message) {
        if (message.equalsIgnoreCase(getResources().getString(R.string.closedialog))) {
            finish();
        }
    }

    @Override
    public void onMcqChildItemClick(String message) {
        if (message.equalsIgnoreCase(getResources().getString(R.string.closedialog))) {
            finish();
        }
    }

    @Override
    public void OnChildItemClickListener(String message) {
        if (message.equalsIgnoreCase(getResources().getString(R.string.closedialog))) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String  event) {
        if(event.startsWith("testpending"))
        {
            finish();

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().register(this);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().unregister(this);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().unregister(this);
        }

    }
}
