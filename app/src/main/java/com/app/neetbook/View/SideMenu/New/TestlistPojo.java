package com.app.neetbook.View.SideMenu.New;

public class TestlistPojo implements Section {

    private String test_id="";
    private String test_qstn_type="";
    private String test_crt_ans_mark="";
    private String test_wrng_ans_mark="";
    private String test_test_name="";
    private String test_short_name="";
    private String test_long_name="";
    private String test_no_of_qstn="";
    private String test_no_of_choice="";
    private String test_instruction="";
    private String test_type="";
    private String test_pending="";

    private int section;

    public TestlistPojo(int section) {
        this.section = section;
    }

    public String getTest_id() {
        return test_id;
    }

    public void setTest_id(String test_id) {
        this.test_id = test_id;
    }

    public String getTest_qstn_type() {
        return test_qstn_type;
    }

    public void setTest_qstn_type(String test_qstn_type) {
        this.test_qstn_type = test_qstn_type;
    }

    public String getTest_crt_ans_mark() {
        return test_crt_ans_mark;
    }

    public void setTest_crt_ans_mark(String test_crt_ans_mark) {
        this.test_crt_ans_mark = test_crt_ans_mark;
    }

    public String getTest_wrng_ans_mark() {
        return test_wrng_ans_mark;
    }

    public void setTest_wrng_ans_mark(String test_wrng_ans_mark) {
        this.test_wrng_ans_mark = test_wrng_ans_mark;
    }

    public String getTest_test_name() {
        return test_test_name;
    }

    public void setTest_test_name(String test_test_name) {
        this.test_test_name = test_test_name;
    }

    public String getTest_short_name() {
        return test_short_name;
    }

    public void setTest_short_name(String test_short_name) {
        this.test_short_name = test_short_name;
    }

    public String getTest_long_name() {
        return test_long_name;
    }

    public void setTest_long_name(String test_long_name) {
        this.test_long_name = test_long_name;
    }

    public String getTest_no_of_qstn() {
        return test_no_of_qstn;
    }

    public void setTest_no_of_qstn(String test_no_of_qstn) {
        this.test_no_of_qstn = test_no_of_qstn;
    }

    public String getTest_no_of_choice() {
        return test_no_of_choice;
    }

    public void setTest_no_of_choice(String test_no_of_choice) {
        this.test_no_of_choice = test_no_of_choice;
    }

    public String getTest_instruction() {
        return test_instruction;
    }

    public void setTest_instruction(String test_instruction) {
        this.test_instruction = test_instruction;
    }

    public String getTest_type() {
        return test_type;
    }

    public void setTest_type(String test_type) {
        this.test_type = test_type;
    }

    public String getTest_pending() {
        return test_pending;
    }

    public void setTest_pending(String test_pending) {
        this.test_pending = test_pending;
    }

    @Override
    public int type() {
        return ITEM;
    }

    @Override
    public int sectionPosition() {
        return section;
    }
}
