package com.app.neetbook.View;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.neetbook.Adapter.MyViewPagerAdapter;
import com.app.neetbook.R;

import com.app.neetbook.Utils.PrefManager;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.View.Fragments.WalkFragment1;
import com.app.neetbook.View.Fragments.WalkFragment2;
import com.app.neetbook.View.Fragments.WalkFragment3;
import com.app.neetbook.View.Fragments.WalkthroughTest;

import org.json.JSONObject;

public class WalkthroughActivity extends AppCompatActivity implements View.OnClickListener {
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private Button btnSkip, btnNext;
    private ViewPager mPager;
    int currentPage = 0;
    SessionManager sessionManager;
    PrefManager prefManager;
    TextView txtSkip,txtNext,txtArticleIndicator,txtPointsIndicator,txtMCQIndicator,txtTestIndicator;
    public static JSONObject jsonWalkThrough = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(getResources().getBoolean(R.bool.isTablet))
        {
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walkthrough);
        if(!getResources().getBoolean(R.bool.isTablet))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initwithfrag();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }

    private void initwithfrag() {
        mPager = findViewById(R.id.pager);
        txtSkip = findViewById(R.id.txtSkip);
        txtNext = findViewById(R.id.txtNext);
        txtPointsIndicator = findViewById(R.id.txtPointsIndicator);
        txtMCQIndicator = findViewById(R.id.txtMCQIndicator);
        txtArticleIndicator = findViewById(R.id.txtArticleIndicator);
        txtTestIndicator = findViewById(R.id.txtTestIndicator);
        txtSkip.setOnClickListener(this);
        txtNext.setOnClickListener(this);
        sessionManager = new SessionManager(WalkthroughActivity.this);
        prefManager = new PrefManager(this);
        prefManager.setFirstTimeLaunch(false);
        mPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
                currentPage = i;
                txtSkip.setVisibility(View.VISIBLE);
                setCustomIndicator();
                if(i==3){
                        txtSkip.setText(getResources().getString(R.string.Signin));

                }
               else if(i==2) {
                    txtSkip.setText(getResources().getString(R.string.skip));
                }
                else if(i==1) {
                    txtSkip.setText(getResources().getString(R.string.skip));
                }
                else {
                    txtSkip.setText(getResources().getString(R.string.skip));
                }

            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    private void setCustomIndicator() {

        if(currentPage == 0) {
            txtSkip.setTextColor(getResources().getColor(R.color.black));
            setArticalIndicator();

        }else if(currentPage == 1) {
            txtSkip.setTextColor(getResources().getColor(R.color.black));
            setPopintsIndicator();
        }else if(currentPage == 2) {
            txtSkip.setTextColor(getResources().getColor(R.color.black));
            setMcqIdicator();
        }else if(currentPage == 3) {
            txtSkip.setTextColor(getResources().getColor(R.color.mcq_blue));
            setTestIndicator();

        }
    }

    private void setArticalIndicator() {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) txtArticleIndicator.getLayoutParams();
        LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) txtPointsIndicator.getLayoutParams();
        LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) txtMCQIndicator.getLayoutParams();
        LinearLayout.LayoutParams params3 = (LinearLayout.LayoutParams) txtTestIndicator.getLayoutParams();
        params.height = getResources().getDimensionPixelSize(R.dimen._8sdp);
        params.width = getResources().getDimensionPixelSize(R.dimen._8sdp);

        params1.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params1.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        params2.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params2.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        params3.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params3.width = getResources().getDimensionPixelSize(R.dimen._6sdp);


        txtArticleIndicator.setBackground(getResources().getDrawable(R.drawable.artical_indicator_dark));
        txtPointsIndicator.setBackground(getResources().getDrawable(R.drawable.points_indicator));
        txtMCQIndicator.setBackground(getResources().getDrawable(R.drawable.mcq_indicator));
        txtTestIndicator.setBackground(getResources().getDrawable(R.drawable.test_indicator));

        txtArticleIndicator.setLayoutParams(params);
        txtPointsIndicator.setLayoutParams(params1);
        txtMCQIndicator.setLayoutParams(params2);
        txtTestIndicator.setLayoutParams(params3);
    }

    private void setPopintsIndicator() {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) txtArticleIndicator.getLayoutParams();
        LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) txtPointsIndicator.getLayoutParams();
        LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) txtMCQIndicator.getLayoutParams();
        LinearLayout.LayoutParams params3 = (LinearLayout.LayoutParams) txtTestIndicator.getLayoutParams();
        params.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        params1.height = getResources().getDimensionPixelSize(R.dimen._8sdp);
        params1.width = getResources().getDimensionPixelSize(R.dimen._8sdp);

        params2.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params2.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        params3.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params3.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        txtArticleIndicator.setBackground(getResources().getDrawable(R.drawable.artical_indicator));
        txtPointsIndicator.setBackground(getResources().getDrawable(R.drawable.points_indicator_dark));
        txtMCQIndicator.setBackground(getResources().getDrawable(R.drawable.mcq_indicator));
        txtTestIndicator.setBackground(getResources().getDrawable(R.drawable.test_indicator));

        txtArticleIndicator.setLayoutParams(params);
        txtPointsIndicator.setLayoutParams(params1);
        txtMCQIndicator.setLayoutParams(params2);
        txtTestIndicator.setLayoutParams(params3);
    }

    private void setMcqIdicator() {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) txtArticleIndicator.getLayoutParams();
        LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) txtPointsIndicator.getLayoutParams();
        LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) txtMCQIndicator.getLayoutParams();
        LinearLayout.LayoutParams params3 = (LinearLayout.LayoutParams) txtTestIndicator.getLayoutParams();
        params.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        params1.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params1.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        params2.height = getResources().getDimensionPixelSize(R.dimen._8sdp);
        params2.width = getResources().getDimensionPixelSize(R.dimen._8sdp);

        params3.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params3.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        txtArticleIndicator.setBackground(getResources().getDrawable(R.drawable.artical_indicator));
        txtPointsIndicator.setBackground(getResources().getDrawable(R.drawable.points_indicator));
        txtMCQIndicator.setBackground(getResources().getDrawable(R.drawable.mcq_indicator_dark));
        txtTestIndicator.setBackground(getResources().getDrawable(R.drawable.test_indicator));

        txtArticleIndicator.setLayoutParams(params);
        txtPointsIndicator.setLayoutParams(params1);
        txtMCQIndicator.setLayoutParams(params2);
        txtTestIndicator.setLayoutParams(params3);
    }

    private void setTestIndicator() {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) txtArticleIndicator.getLayoutParams();
        LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) txtPointsIndicator.getLayoutParams();
        LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) txtMCQIndicator.getLayoutParams();
        LinearLayout.LayoutParams params3 = (LinearLayout.LayoutParams) txtTestIndicator.getLayoutParams();
        params.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        params1.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params1.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        params2.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params2.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        params3.height = getResources().getDimensionPixelSize(R.dimen._8sdp);
        params3.width = getResources().getDimensionPixelSize(R.dimen._8sdp);

        txtArticleIndicator.setBackground(getResources().getDrawable(R.drawable.artical_indicator));
        txtPointsIndicator.setBackground(getResources().getDrawable(R.drawable.points_indicator));
        txtMCQIndicator.setBackground(getResources().getDrawable(R.drawable.mcq_indicator));
        txtTestIndicator.setBackground(getResources().getDrawable(R.drawable.test_indicator_dark));
        txtArticleIndicator.setLayoutParams(params);
        txtPointsIndicator.setLayoutParams(params1);
        txtMCQIndicator.setLayoutParams(params2);
        txtTestIndicator.setLayoutParams(params3);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txtSkip:
                startActivity(new Intent(WalkthroughActivity.this, SignInSighUp.class));
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                finish();
                break;

            case R.id.txtNext:
                if(txtNext.getText().toString().equalsIgnoreCase("Next")) {
                    mPager.setCurrentItem(getNextPossibleItemIndex(1), true);
                }
                else {
                    startActivity(new Intent(WalkthroughActivity.this, SignInSighUp.class));
                    overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                    finish();
                }
                break;
        }
    }
    private int getNextPossibleItemIndex (int change) {

        int currentIndex = mPager.getCurrentItem();
        int total = mPager.getAdapter().getCount();
        if (currentPage + change < 0) {
            return 0;
        }

        return Math.abs((currentIndex + change) % total) ;
    }
    class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:

                    WalkFragment1 fm = new WalkFragment1();

                    return fm;

                case 1:

                    WalkFragment2 fragmentTab2 = new WalkFragment2();

                    return fragmentTab2;

                case 2:

                    WalkFragment3 fragmentTab23 = new WalkFragment3();

                    return fragmentTab23;
                case 3:

                    WalkthroughTest walkthroughTest = new WalkthroughTest();

                    return walkthroughTest;
////                case 3:
//
//                    Bundle bundle_4 = new Bundle();
//                    bundle_4.putString("fastpayhome", jsonpojo);
//                    FragmentTab4 fragmentTab4 = new FragmentTab4();
//                    fragmentTab4.setArguments(bundle_4);
//                    return fragmentTab4;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 4;
        }








    }
}
