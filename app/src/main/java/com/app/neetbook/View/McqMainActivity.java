package com.app.neetbook.View;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.app.neetbook.Adapter.ArticleTabsAdapter;
import com.app.neetbook.Interfaces.OnMobileHomeMenuSelectedListioner;
import com.app.neetbook.Model.HeadData;
import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.McqExam;
import com.app.neetbook.R;
import com.app.neetbook.Utils.OnSwipeTouchListener;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.View.SideMenu.HeaderView;
import com.app.neetbook.View.SideMenu.SubscrptionDetailListFragment;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class McqMainActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {

    Context context;
    private static final String TAG = "1";
    private WindowInsetsCompat mLastInsets;
    private int mTitleLeftCollapsed;
    private int mTitleTopCollapsed;
    private int mTitleLeftExpanded;
    private int mTitleTopExpanded;


    protected McqHeaderView toolbarHeaderView;
    protected McqHeaderView floatHeaderView;
    protected AppBarLayout appBarLayout;
    protected Toolbar toolbar;
    protected CollapsingToolbarLayout collapsingToolbarLayout;
    private boolean isHideToolbarView = false;

    Display display;
    int height1, width1;
    SessionManager sessionManager;
    Typeface tfBold, tfMedium, tfRegular;
    private ViewPager viewPager;
    RecyclerView recyclerViewGroups;
    private ArrayList<HeadData> headDataList;
    private TextView textView, content_title_tv, content_title_tv1, superTitle,headnames,chapterNames;
    String strSubjectId = "", strSubjectName = "", strChaptortName = "", strHeadId = "", strChapterId = "", loadingType = "", is_coming_page = "", strHeadName = "";
    public static ArrayList<HeadDetails> mcqHeadList;
    public static ArrayList<McqExam> mcqExamArrayList;
    LinearSnapHelper snapHelper;
    ViewPagerAdapter viewPagerAdapter;
    String fullupordown = "0";
    private int currentSelectedPosition = 0;
    ArticleTabsAdapter homeGroupNameAdapter;
    LinearLayoutManager layoutManager;
    private static final float MILLISECONDS_PER_INCH = 200f;
    ImageView homeBack,ToolBarLayout;
    LinearLayout recylerview;
    RelativeLayout headinglin;

    Float expandpercentage = 0f;
    private final static int DIMENSION = 2;
    private final static int HORIZONTAL = 0;
    private final static int VERTICAL = 1;
    RelativeLayout rlParent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mcq_activity_main);
        context = getApplicationContext();
        StatusBarColorChange.updateStatusBarColor(McqMainActivity.this,getResources().getColor(R.color.mcq_blue));

      init();
        rlParent = (RelativeLayout) findViewById(R.id.rlParent);
        toolbarHeaderView=(McqHeaderView)findViewById(R.id.toolbar_header_view);
        floatHeaderView=(McqHeaderView)findViewById(R.id.float_header_view);
        appBarLayout=(AppBarLayout) findViewById(R.id.appbar);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        collapsingToolbarLayout=(CollapsingToolbarLayout)findViewById(R.id.collapsing_toolbar);



        initUi();

        rlParent.setOnTouchListener(new OnSwipeTouchListener(McqMainActivity.this) {
            public void onSwipeTop() {
                //Toast.makeText(SubscriptionArticleActivity.this, "top", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeRight() {
                //Toast.makeText(SubscriptionArticleActivity.this, "right", Toast.LENGTH_SHORT).show();
                finish();
            }

            public void onSwipeLeft() {
                finish();
                // startActivity(new Intent(PointsMainActivity.this, DynamicFragmentActivity.class).putExtra("subject_id", strSubjectId).putExtra("head_id", strHeadId).putExtra("title", strHeadName).putExtra("long_name", strLongName).putExtra("isPoints", "No").putExtra("chapterName", chapter_name));
            }

            public void onSwipeBottom() {
                //Toast.makeText(SubscriptionArticleActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });



        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.menu_32);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mTitleLeftCollapsed = getResources().getDimensionPixelOffset(R.dimen.title_left_margin_collapsed);
        mTitleTopCollapsed = getResources().getDimensionPixelOffset(R.dimen.title_top_margin_collapsed);




    }
    private void initUi() {
        appBarLayout.addOnOffsetChangedListener(this);

    }
    public static int darker (int color, float factor) {
        int a = Color.alpha( color );
        int r = Color.red( color );
        int g = Color.green( color );
        int b = Color.blue( color );

        return Color.argb( a,
                Math.max( (int)(r * factor), 0 ),
                Math.max( (int)(g * factor), 0 ),
                Math.max( (int)(b * factor), 0 ) );
    }

    public static int getDominantColor(Bitmap bitmap) {
        Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, 1, 1, true);
        final int color = newBitmap.getPixel(0, 0);
        newBitmap.recycle();
        return color;
    }



    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(verticalOffset) / (float) maxScroll;
        System.out.println("percentage--->" + percentage);


        expandpercentage = percentage;
        if(percentage == 1)
        {
            fullupordown = "1";
        }
        else  if(percentage == 0)
        {
            fullupordown = "0";
        }

         if (percentage < 0.9)
             chapterNames.setVisibility(View.VISIBLE);
         else
             chapterNames.setVisibility(View.GONE);

        if (percentage > 0.7) {


            ViewGroup.MarginLayoutParams params2 = (ViewGroup.MarginLayoutParams) content_title_tv.getLayoutParams();
            //   params2.rightMargin = 100;
            headinglin.setVisibility(View.INVISIBLE);

            if (content_title_tv.length() > 40) {
                content_title_tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header_view_end_size_minimum));
                content_title_tv1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header_view_end_size_minimum));
            } else {
                content_title_tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header_view_end_size_minimum));
                content_title_tv1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header_view_end_size_minimum));
            }
            ViewGroup.MarginLayoutParams paramsd = (ViewGroup.MarginLayoutParams) content_title_tv.getLayoutParams();
            paramsd.rightMargin = 320;
        } else {
            if (content_title_tv.length() > 40) {
                content_title_tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header_view_end_size_minimum));
                content_title_tv1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header_view_end_size_minimum));
            } else {
                content_title_tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header_view_end_size_minimum));
                content_title_tv1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header_view_end_size_minimum));
            }
            ViewGroup.MarginLayoutParams paramsd = (ViewGroup.MarginLayoutParams) content_title_tv.getLayoutParams();
            paramsd.rightMargin = 100;
            headinglin.setVisibility(View.VISIBLE);


            ViewGroup.MarginLayoutParams params2 = (ViewGroup.MarginLayoutParams) content_title_tv.getLayoutParams();
            //  params2.rightMargin = 0;
        }

        if (percentage <= 0.35f) {
            recylerview.setVisibility(View.VISIBLE);
        } else {
            recylerview.setVisibility(View.GONE);
        }
        content_title_tv.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.point_title_txt_color));
        if (percentage > 0.35f) {
            content_title_tv.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        } else {
            content_title_tv.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.point_title_txt_color));
        }
        if (percentage == 1f && isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.VISIBLE);
            isHideToolbarView = !isHideToolbarView;
        } else if (percentage < 1f && !isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.GONE);
            isHideToolbarView = !isHideToolbarView;
        }
    }





    private static ViewOffsetHelper getViewOffsetHelper(View view) {
        ViewOffsetHelper offsetHelper = (ViewOffsetHelper) view.getTag(R.id.view_offset_helper);
        if (offsetHelper == null) {
            offsetHelper = new ViewOffsetHelper(view);
            view.setTag(R.id.view_offset_helper, offsetHelper);
        }
        return offsetHelper;
    }



    static class ViewOffsetHelper {

        private final View mView;

        private int mLayoutTop;
        private int mLayoutLeft;
        private int mOffsetTop;
        private int mOffsetLeft;

        public ViewOffsetHelper(View view) {
            mView = view;
        }

        public void onViewLayout() {
            // Now grab the intended top
            mLayoutTop = mView.getTop();
            mLayoutLeft = mView.getLeft();

            // And offset it as needed
            updateOffsets();
        }

        private void updateOffsets() {
            ViewCompat.offsetTopAndBottom(mView, mOffsetTop - (mView.getTop() - mLayoutTop));
            ViewCompat.offsetLeftAndRight(mView, mOffsetLeft - (mView.getLeft() - mLayoutLeft));

            // Manually invalidate the view and parent to make sure we get drawn pre-M
            if (Build.VERSION.SDK_INT < 23) {
                tickleInvalidationFlag(mView);
                final ViewParent vp = mView.getParent();
                if (vp instanceof View) {
                    tickleInvalidationFlag((View) vp);
                }
            }
        }

        private static void tickleInvalidationFlag(View view) {
            final float y = ViewCompat.getTranslationY(view);
            ViewCompat.setTranslationY(view, y + 1);
            ViewCompat.setTranslationY(view, y);
        }

        /**
         * Set the top and bottom offset for this {@link ViewOffsetHelper}'s view.
         *
         * @param offset the offset in px.
         * @return true if the offset has changed
         */
        public boolean setTopAndBottomOffset(int offset) {
            if (mOffsetTop != offset) {
                mOffsetTop = offset;
                updateOffsets();
                return true;
            }
            return false;
        }

        /**
         * Set the left and right offset for this {@link ViewOffsetHelper}'s view.
         *
         * @param offset the offset in px.
         * @return true if the offset has changed
         */
        public boolean setLeftAndRightOffset(int offset) {
            if (mOffsetLeft != offset) {
                mOffsetLeft = offset;
                updateOffsets();
                return true;
            }
            return false;
        }

        public int getTopAndBottomOffset() {
            return mOffsetTop;
        }

        public int getLeftAndRightOffset() {
            return mOffsetLeft;
        }
    }


    private void init() {
        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width1 = size.x;
        height1 = size.y;
        sessionManager = new SessionManager(McqMainActivity.this);
        tfBold = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaRegular.otf");

        headnames = (TextView) findViewById(R.id.headnames);
        chapterNames= (TextView) findViewById(R.id.chapternames);
        recylerview = (LinearLayout) findViewById(R.id.recylerview);
        headinglin = findViewById(R.id.headinglin);

        content_title_tv1 = findViewById(R.id.content_title_tv1);
        content_title_tv = findViewById(R.id.content_title_tv);
        superTitle = findViewById(R.id.superTitleTxt);

        viewPager = findViewById(R.id.viewpager_subscription_category);

        recyclerViewGroups = findViewById(R.id.recyclerViewGroups);

        headDataList = new ArrayList<>();

        content_title_tv1.setText(mcqHeadList.get(0).headData.get(0).long_name);
        content_title_tv1.setText(mcqHeadList.get(0).headData.get(0).long_name);

        if (getIntent().getExtras() != null) {
            strHeadId = getIntent().getStringExtra("head_id");
            strSubjectId = getIntent().getStringExtra("subject_id");
            strSubjectName = getIntent().getStringExtra("subject_name");
            strChaptortName = getIntent().getStringExtra("chapter_name");
            headnames.setText(strSubjectName);
            chapterNames.setText(strChaptortName);
            loadingType = getIntent().getStringExtra("loading_type");

            if (getIntent().getExtras().containsKey("coming_page")) {

                is_coming_page = getIntent().getStringExtra("coming_page");
            }


            if (!getIntent().getStringExtra("loading_type").equals("1")) {
                strChapterId = getIntent().getStringExtra("chapter_id");
            }
            if (sessionManager.getViewToBookMark() != null && !sessionManager.getViewToBookMark().isEmpty() && sessionManager.getViewToBookMark().equalsIgnoreCase("Yes")) {
                if (sessionManager.getBookmarkDetails().get(SessionManager.LOADING_TYPE).equals("1")) {
                    mcqHeadList = new Gson().fromJson(sessionManager.getBookmarkDetails().get(SessionManager.HEAD_LIST), new TypeToken<List<HeadDetails>>() {
                    }.getType());
                } else {
                    mcqExamArrayList = new Gson().fromJson(sessionManager.getBookmarkDetails().get(SessionManager.HEAD_LIST), new TypeToken<List<McqExam>>() {
                    }.getType());
                }
            }
        }
        createSnap();
        setupViewPager(viewPager);
        setFontSize();


    }

    private void setFontSize() {
        headnames.setTypeface(tfMedium);
        chapterNames.setTypeface(tfMedium);
        content_title_tv1.setTypeface(tfMedium);
        content_title_tv.setTypeface(tfMedium);
        superTitle.setTypeface(tfMedium);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String event) {
        if(event.equals("UPTOUCH") && fullupordown.equals("0"))
        {
            if (expandpercentage > 0.1) {
                appBarLayout.setExpanded(false);
            }


        }
        else  if(event.equals("UPTOUCH") && fullupordown.equals("1"))
        {
            if (expandpercentage != 1f) {
                appBarLayout.setExpanded(true);
            }
        }
    };
    @Override

    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }


    private void setupViewPager(final ViewPager viewPager) {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this);

        if (loadingType.equals("1")) {
            if (mcqHeadList.size() > 0) {
                for (int i = 0; i < mcqHeadList.size(); i++) {
                    if (mcqHeadList.get(i).isSH.equals("Yes")) {

                        HeadData data = new HeadData();
                        data._id = mcqHeadList.get(i).headData.get(0)._id;
                        data.short_name = mcqHeadList.get(i).headData.get(0).short_name;
                        data.long_name = mcqHeadList.get(i).headData.get(0).long_name;
                        data.firsthead = mcqHeadList.get(i).headData.get(0).firsthead;
                        data.superHeadingName = mcqHeadList.get(i).isSH.equals("1") ? mcqHeadList.get(i).superDetails.short_name : "";
                        headDataList.add(data);
                        viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(strSubjectId, strChapterId, mcqHeadList.get(i).headData.get(0)._id, mcqHeadList.get(i).headData.get(0).long_name, loadingType), mcqHeadList.get(i).headData.get(0).short_name);

                    } else {
                        if (mcqHeadList.get(i).headData.size() > 0) {
                            for (int j = 0; j < mcqHeadList.get(i).headData.size(); j++) {

                                HeadData data = new HeadData();
                                data._id = mcqHeadList.get(i).headData.get(j)._id;
                                data.short_name = mcqHeadList.get(i).headData.get(j).short_name;
                                data.long_name = mcqHeadList.get(i).headData.get(j).long_name;
                                data.firsthead = mcqHeadList.get(i).headData.get(j).firsthead;
                                data.superHeadingName = mcqHeadList.get(i).isSH.equals("1") ? mcqHeadList.get(i).superDetails.short_name : "";
                                headDataList.add(data);
                                viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(strSubjectId, strChapterId, mcqHeadList.get(i).headData.get(j)._id, mcqHeadList.get(i).headData.get(j).long_name, loadingType), mcqHeadList.get(i).headData.get(j).short_name);
                            }
                        }
                    }
                }
            }
        } else if (loadingType.equals("2") || loadingType.equals("3")) {
            if (mcqExamArrayList.size() > 0) {
                for (int i = 0; i < mcqExamArrayList.size(); i++) {
                    HeadData data = new HeadData();
                    data._id = mcqExamArrayList.get(i).get_id();
                    data.short_name = mcqExamArrayList.get(i).getShort_name();
                    data.long_name = mcqExamArrayList.get(i).getLong_name();
                    data.superHeadingName = "";
                    headDataList.add(data);
                    viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(strSubjectId, strChapterId, mcqExamArrayList.get(i).get_id(), mcqExamArrayList.get(i).getLong_name(), loadingType), mcqExamArrayList.get(i).getLong_name());

                }

            }
        }


        if (headDataList.size() > 0) {
            for (int i = 0; i < headDataList.size(); i++) {
                if (strHeadId.equalsIgnoreCase(headDataList.get(i)._id)) {
                    currentSelectedPosition = i;
                    content_title_tv.setText(headDataList.get(i).long_name);
                    content_title_tv1.setText(headDataList.get(i).long_name);
                    strHeadName = headDataList.get(i).long_name;
                    sessionManager.updateBookMark("MCQ", strHeadId, strSubjectId, getIntent().getStringExtra("subject_name"), new Gson().toJson(loadingType.equals("1") ? mcqHeadList : mcqExamArrayList), strChapterId, getIntent().getStringExtra("chapter_name"), headDataList.get(i).superHeadingName.equals("") ? "" : headDataList.get(i).superHeadingName, "", loadingType);
                    sessionManager.LastReadSubjectBookmark(sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME), sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME), sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME));
                }
            }
        }

        setGroupAdapter(currentSelectedPosition);
        layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
        int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
        int centeredItemPosition = totalVisibleItems / 2;
        recyclerViewGroups.smoothScrollToPosition(currentSelectedPosition != 0 ? currentSelectedPosition - 1 : currentSelectedPosition);
        recyclerViewGroups.setScrollY(centeredItemPosition);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(currentSelectedPosition);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setGroupAdapter(position);
                sessionManager.setMcqLastReadDetails(headDataList.get(position).firsthead);
                layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
                int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                int centeredItemPosition = totalVisibleItems / 2;
                recyclerViewGroups.smoothScrollToPosition(position != 0 ? position - 1 : position);

                content_title_tv.setText(headDataList.get(position).long_name);
                content_title_tv1.setText(headDataList.get(position).long_name);
                if (headDataList.get(position).superHeadingName != null && !headDataList.get(position).superHeadingName.equals(""))
                    superTitle.setText(headDataList.get(position).superHeadingName);
                else
                    superTitle.setText("");
                sessionManager.updateBookMark("MCQ", headDataList.get(position)._id, strSubjectId, getIntent().getStringExtra("subject_name"), new Gson().toJson(loadingType.equals("1") ? mcqHeadList : mcqExamArrayList), strChapterId, getIntent().getStringExtra("chapter_name"), headDataList.get(position).superHeadingName.equals("") ? "" : headDataList.get(position).superHeadingName, "", loadingType);

                sessionManager.LastReadSubjectBookmark(sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                        sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                        sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME));

                scrollToCenter(layoutManager, recyclerViewGroups, position);
                appBarLayout.setExpanded(true);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        /*viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                if (position != 0 && position != headDataList.size() - 1) {
                    page.setAlpha(0f);
                    page.setVisibility(View.VISIBLE);

                    // Start Animation for a short period of time
                    page.animate()
                            .alpha(1f)
                            .setDuration(page.getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }
        });*/
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        List<Fragment> fragmentList = new ArrayList<>();
        List<String> fragmentTitles = new ArrayList<>();
        Context mContext;
        TextView tabTitile;


        ViewPagerAdapter(FragmentManager fragmentManager,
                         FragmentActivity activity) {
            super(fragmentManager);
            mContext = activity;
        }

        @Override
        public Fragment getItem(int i) {
            return new SubscrptionDetailListFragment(strSubjectId, strChapterId, loadingType.equals("1") ? headDataList.get(i)._id : mcqExamArrayList.get(i).get_id(), loadingType.equals("1") ? headDataList.get(i).long_name : mcqExamArrayList.get(i).getLong_name(), loadingType);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            return fragmentTitles.get(position);
        }

        void addFragment(Fragment fragment, String name) {
            fragmentList.add(fragment);
            fragmentTitles.add(name);
        }

        View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_items, null);
            tabTitile = v.findViewById(R.id.txtTabTitle);
            tabTitile.setText(fragmentTitles.get(position));
            tabTitile.setTextColor(getResources().getColor(R.color.white));
            return v;
        }
    }

    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
    }



    private void setGroupAdapter(int position) {


        if (homeGroupNameAdapter == null) {
            homeGroupNameAdapter = new ArticleTabsAdapter(headDataList, McqMainActivity.this, position);
            layoutManager = new LinearLayoutManager(McqMainActivity.this, RecyclerView.HORIZONTAL, false);
            //layoutManager.setStackFromEnd(true);
            recyclerViewGroups.setLayoutManager(layoutManager);
            recyclerViewGroups.setAdapter(homeGroupNameAdapter);
            recyclerViewGroups.scrollToPosition(position);

        } else {
            homeGroupNameAdapter.notifyPosition(position);
            homeGroupNameAdapter.notifyDataSetChanged();
            scrollToCenter(layoutManager, recyclerViewGroups, position);
        }


        homeGroupNameAdapter.setOnGroupMenuSelectedListener(new OnMobileHomeMenuSelectedListioner() {
            @Override
            public void onGroupSelected(String groupId, int position) {

                viewPager.setCurrentItem(position);
                layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
                assert layoutManager != null;
                int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                int centeredItemPosition = totalVisibleItems / 2;
                scrollToCenter(layoutManager, recyclerViewGroups, position);
            }
        });

    }

    public void scrollToCenter(LinearLayoutManager layoutManager, RecyclerView recyclerList, int clickPosition) {
        RecyclerView.SmoothScroller smoothScroller = createSnapScroller(recyclerList, layoutManager);

        if (smoothScroller != null) {
            smoothScroller.setTargetPosition(clickPosition);
            layoutManager.startSmoothScroll(smoothScroller);
        }
    }

    @Nullable
    private LinearSmoothScroller createSnapScroller(RecyclerView mRecyclerView, final RecyclerView.LayoutManager layoutManager) {
        if (!(layoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider)) {
            return null;
        }
        return new LinearSmoothScroller(mRecyclerView.getContext()) {
            @Override
            protected void onTargetFound(View targetView, RecyclerView.State state, Action action) {
                int[] snapDistances = calculateDistanceToFinalSnap(layoutManager, targetView);
                final int dx = snapDistances[HORIZONTAL];
                final int dy = snapDistances[VERTICAL];
                final int time = calculateTimeForDeceleration(Math.max(Math.abs(dx), Math.abs(dy)));
                if (time > 0) {
                    action.update(dx, dy, time, mDecelerateInterpolator);
                }
            }


            @Override
            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
            }
        };
    }

    private int[] calculateDistanceToFinalSnap(@NonNull RecyclerView.LayoutManager layoutManager, @NonNull View targetView) {
        int[] out = new int[DIMENSION];
        if (layoutManager.canScrollHorizontally()) {
            out[HORIZONTAL] = distanceToCenter(layoutManager, targetView,
                    OrientationHelper.createHorizontalHelper(layoutManager));
        }

        if (layoutManager.canScrollVertically()) {
            out[VERTICAL] = distanceToCenter(layoutManager, targetView,
                    OrientationHelper.createHorizontalHelper(layoutManager));
        }
        return out;
    }

    private int distanceToCenter(@NonNull RecyclerView.LayoutManager layoutManager,
                                 @NonNull View targetView, OrientationHelper helper) {
        final int childCenter = helper.getDecoratedStart(targetView)
                + (helper.getDecoratedMeasurement(targetView) / 2);
        final int containerCenter;
        if (layoutManager.getClipToPadding()) {
            containerCenter = helper.getStartAfterPadding() + helper.getTotalSpace() / 2;
        } else {
            containerCenter = helper.getEnd() / 2;
        }
        return childCenter - containerCenter;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (is_coming_page.equals("homepage")) {

            Intent s = new Intent(getApplicationContext(), TabMainActivity.class);
            s.putExtra("subjectId", strSubjectId);
            s.putExtra("subjectName", sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_NAME));
            s.putExtra("isTrial", sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_TRIAL_STATUS));
            s.putExtra("subjectShortName", sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_SHORT_NAME));
            s.putExtra("showing_page_position", "0");
            startActivity(s);
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        } else {

            finish();
        }
    }



}



