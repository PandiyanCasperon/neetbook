package com.app.neetbook.View.SideMenu;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.text.HtmlCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.neetbook.Adapter.ArticleImagesAdaper;
import com.app.neetbook.Model.MCQExamIds;
import com.app.neetbook.R;
import com.app.neetbook.Utils.OnSwipeTouchListener;
import com.app.neetbook.Utils.SessionManager;

import java.util.ArrayList;

public class SubscriptionCategoryItemInfoFragment extends DialogFragment {


    public static String TAG = "FullScreenDialog";
    private AppCompatImageView buttonClose;
    private Dialog dialog;
    TextView txt_description, txt_explanation, txt_correct_answer_title, txt_correct_answer, txtExamAndYear, txt_explanation1, txt_description1;
    String strCorrectAnswers = "", explanations = "";
    ArrayList<String> answers;
    ArrayList<String> images;
    ArrayList<MCQExamIds> mcqExamIds;
    RecyclerView mcqIdRecyclerView, imagesRecyclrView, imagesRecyclrView1;
    String strTitle;
    Typeface tfBold, tfMedium, tfRegular;
    SessionManager sessionManager;
    RelativeLayout rlParent, ll_maxline;
    Display display;
    LinearLayout ll_minline;
    int height1, width1;
    private LinearLayout llParent;
    DisplayMetrics displayMetrics;
    int screenHeight = 0;
    int screenWidth = 0;

    public SubscriptionCategoryItemInfoFragment(String answers, String explanations, ArrayList<String> images, ArrayList<MCQExamIds> mcqExamIds, String strTitle) {
        this.strCorrectAnswers = answers;
        this.explanations = explanations;
        this.images = images;
        this.mcqExamIds = mcqExamIds;
        this.strTitle = strTitle;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        sessionManager = new SessionManager(getActivity());
        tfBold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaRegular.otf");
    }


    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.new_fragment_subscription_category_item_info,
                container, false);


        displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        screenHeight = displayMetrics.heightPixels;
        screenWidth = displayMetrics.widthPixels;

        //rlParent = view.findViewById(R.id.rlParent);
        llParent = view.findViewById(R.id.llParent);
        buttonClose = view.findViewById(R.id.btn_close);
        txt_correct_answer = view.findViewById(R.id.txt_correct_answer);
        txt_correct_answer.setMovementMethod(new ScrollingMovementMethod());
        txt_correct_answer_title = view.findViewById(R.id.txt_correct_answer_title);

        ll_minline = view.findViewById(R.id.ll_minline);
        ll_maxline = view.findViewById(R.id.ll_maxline);

        txt_explanation = view.findViewById(R.id.txt_explanation);
        txt_explanation1 = view.findViewById(R.id.txt_explanation1);
        txt_description1 = view.findViewById(R.id.txt_description1);
        txt_description = view.findViewById(R.id.txt_description);
        txt_description.setMovementMethod(new ScrollingMovementMethod());
        //txt_description.setMaxLines(80);
        imagesRecyclrView = view.findViewById(R.id.imagesRecyclrView);
        imagesRecyclrView1 = view.findViewById(R.id.imagesRecyclrView1);
        txtExamAndYear = view.findViewById(R.id.txtExamAndYear);
        txtExamAndYear.setMovementMethod(new ScrollingMovementMethod());
        txt_correct_answer.setText(strCorrectAnswers);
        Log.e("explanations length", "" + explanations.length());
        System.out.println("explanations length===" + explanations.length());
        //setNestedHeight();
        if (explanations.length() > 800) {
            ll_maxline.setVisibility(View.VISIBLE);
            ll_minline.setVisibility(View.GONE);
        } else {
            ll_minline.setVisibility(View.VISIBLE);
            ll_maxline.setVisibility(View.GONE);
        }

        txt_description.setText(HtmlCompat.fromHtml(explanations,Html.FROM_HTML_MODE_LEGACY));
        txt_description1.setText(HtmlCompat.fromHtml(explanations,Html.FROM_HTML_MODE_LEGACY));
        if (txt_description.getText().toString().isEmpty()) {
            txt_description.setText("Explanation not available for this question.");
            txt_description.setTextColor(getActivity().getResources().getColor(R.color.light_gray));
        }
        if (txt_description1.getText().toString().isEmpty()) {
            txt_description1.setText("Explanation not available for this question.");
            txt_description1.setTextColor(getActivity().getResources().getColor(R.color.light_gray));
        }
        setFont();
        imagesRecyclrView.setVisibility(images.size() > 0 ? View.VISIBLE : View.GONE);
        imagesRecyclrView1.setVisibility(images.size() > 0 ? View.VISIBLE : View.GONE);
        populateImages();

        listener();
        return view;
    }

    private void setNestedHeight() {

        display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width1 = size.x;
        height1 = size.y;

        if (height1 < 1200) {
            if (images.size() > 0) {
                if (explanations.length() > 700) {
                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) txt_description.getLayoutParams();
                    params.height = (int) (height1 * (0.26));
                    txt_description.setLayoutParams(params);
                    txt_description.setMovementMethod(new ScrollingMovementMethod());
                }
            } else {
                if (explanations.length() > 1000) {
                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) txt_description.getLayoutParams();
                    params.height = (int) (height1 * (0.29));
                    txt_description.setLayoutParams(params);
                    txt_description.setMovementMethod(new ScrollingMovementMethod());
                }
            }
        } else if (height1 < 1700 && height1 > 1200) {
            if (images.size() > 0) {
                if (explanations.length() > 700) {
                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) txt_description.getLayoutParams();
                    params.height = (int) (height1 * (0.29));
                    txt_description.setLayoutParams(params);
                    txt_description.setMovementMethod(new ScrollingMovementMethod());
                }
            } else {
                if (explanations.length() > 1000) {
                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) txt_description.getLayoutParams();
                    params.height = (int) (height1 * (0.29));
                    txt_description.setLayoutParams(params);
                    txt_description.setMovementMethod(new ScrollingMovementMethod());
                }
            }
        } else {
            if (explanations.length() > 1000) {
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) txt_description.getLayoutParams();

                if (txt_correct_answer.length() < 3) {

                    params.height = (int) (height1 * (0.68));

                } else {

                    params.height = (int) (height1 * (0.60));
                }

                txt_description.setLayoutParams(params);
                txt_description.setMovementMethod(new ScrollingMovementMethod());
            }
        }
    }

    private void listener() {

        buttonClose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        llParent.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            public void onSwipeTop() {
                //Toast.makeText(SubscriptionArticleActivity.this, "top", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeRight() {
                //Toast.makeText(SubscriptionArticleActivity.this, "right", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }

            public void onSwipeLeft() {

            }

            public void onSwipeBottom() {
                //Toast.makeText(SubscriptionArticleActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });
    }

    private void setFont() {
        if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
            txt_correct_answer.setTextAppearance(getActivity(), R.style.textViewSmallArticleInfo);
            txt_description.setTextAppearance(getActivity(), R.style.textViewSmallArticleInfo);
            txt_description1.setTextAppearance(getActivity(), R.style.textViewSmallArticleInfo);
            txtExamAndYear.setTextAppearance(getActivity(), R.style.textViewSmallArticleInfo);
            txt_correct_answer_title.setTextAppearance(getActivity(), R.style.textViewSmallCorrectAnsTitle);
            txt_explanation.setTextAppearance(getActivity(), R.style.textViewSmallCorrectAnsTitle);
            txt_explanation1.setTextAppearance(getActivity(), R.style.textViewSmallCorrectAnsTitle);

        } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
            txt_correct_answer.setTextAppearance(getActivity(), R.style.textViewMediumArticleInfo);
            txt_description.setTextAppearance(getActivity(), R.style.textViewMediumArticleInfo);
            txt_description1.setTextAppearance(getActivity(), R.style.textViewMediumArticleInfo);
            txtExamAndYear.setTextAppearance(getActivity(), R.style.textViewMediumArticleInfo);
            txt_correct_answer_title.setTextAppearance(getActivity(), R.style.textViewMediumCorrectAnsTitle);
            txt_explanation.setTextAppearance(getActivity(), R.style.textViewMediumCorrectAnsTitle);
            txt_explanation1.setTextAppearance(getActivity(), R.style.textViewMediumCorrectAnsTitle);
        } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
            txt_correct_answer.setTextAppearance(getActivity(), R.style.textViewLargeArticleInfo);
            txt_description.setTextAppearance(getActivity(), R.style.textViewLargeArticleInfo);
            txt_description1.setTextAppearance(getActivity(), R.style.textViewLargeArticleInfo);
            txtExamAndYear.setTextAppearance(getActivity(), R.style.textViewLargeArticleInfo);
            txt_correct_answer_title.setTextAppearance(getActivity(), R.style.textViewLargeCorrectAnsTitle);
            txt_explanation.setTextAppearance(getActivity(), R.style.textViewLargeCorrectAnsTitle);
            txt_explanation1.setTextAppearance(getActivity(), R.style.textViewLargeCorrectAnsTitle);
        }

        txt_correct_answer.setTypeface(tfRegular);
        txt_description.setTypeface(tfRegular);
        txt_description1.setTypeface(tfRegular);
        txt_correct_answer_title.setTypeface(tfRegular);
        txt_explanation.setTypeface(tfRegular);
        txt_explanation1.setTypeface(tfRegular);
        txtExamAndYear.setTypeface(tfMedium);
    }

    private void populateImages() {
        if (sessionManager.isNightModeEnabled())
            txtExamAndYear.setTextColor(getResources().getColor(R.color.white));
        else
            txtExamAndYear.setTextColor(getResources().getColor(R.color.mcq_blue));
        ArticleImagesAdaper articleImagesAdaper = new ArticleImagesAdaper(images, getActivity(), strTitle);
        imagesRecyclrView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        imagesRecyclrView.setAdapter(articleImagesAdaper);
        imagesRecyclrView1.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        imagesRecyclrView1.setAdapter(articleImagesAdaper);

        String comma = "";
        String bufferedString = "";

        if (mcqExamIds.size() > 0) {
            for (int i = 0; i < mcqExamIds.size(); i++) {
                bufferedString = bufferedString + comma + mcqExamIds.get(i).getExam_name() + " " + mcqExamIds.get(i).getYear_name();
                comma = ", ";
            }
            txtExamAndYear.setVisibility(View.VISIBLE);
            txtExamAndYear.setText(bufferedString);
        } else
            txtExamAndYear.setVisibility(View.GONE);

    }

    @Override
    public void onStart() {
        super.onStart();
        dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

}
