package com.app.neetbook.View.SideMenu;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.app.TabPojo.EventbusPojo;
import com.app.neetbook.Adapter.HomeSearchAdapter;
import com.app.neetbook.Adapter.NewPointsSectionAdapter;
import com.app.neetbook.Adapter.PointsSectionAdapter;
import com.app.neetbook.Adapter.sideMenuContent.SubscriptionCategoryPointsAdapter;
import com.app.neetbook.Interfaces.APMTSearchListioner;
import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.Interfaces.OnSearchItemClickListener;
import com.app.neetbook.Interfaces.PointsSection;
import com.app.neetbook.Interfaces.TabArtcileChapterClickListener;
import com.app.neetbook.Model.NewPointSectionModel;
import com.app.neetbook.Model.PointsChildModel;
import com.app.neetbook.Model.PointsContent;
import com.app.neetbook.Model.PointsHeaderModel;
import com.app.neetbook.Model.PointsList;
import com.app.neetbook.Model.SearchBean;
import com.app.neetbook.Model.sidemenuFromContent.CategoryPoints;
import com.app.neetbook.R;
import com.app.neetbook.TAbAdapter.TabPointChildRecyclerAdapter;
import com.app.neetbook.TAbAdapter.TabPointMainAdapter;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.ItemOffsetDecoration;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.SmoothScroll.LinearLayoutManagerWithSmoothScroller;
import com.app.neetbook.View.ActivitySubscriptionListDialog;
import com.app.neetbook.View.CustomDialogActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.shuhart.stickyheader.StickyHeaderItemDecorator;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class PointsFragment extends Fragment implements ItemClickListener, TabPointChildRecyclerAdapter.OnHeadingClickListener, APMTSearchListioner, View.OnClickListener, PointsSectionAdapter.ParentClickListener {

    private ArrayList<NewPointSectionModel> tempArrayList;
    private ArrayList<CategoryPoints> mList = new ArrayList<>();
    private ArrayList<PointsSection> pointsSectionArrayList = new ArrayList<>();
    private SubscriptionCategoryPointsAdapter mAdapter;
    PointsSectionAdapter pointsSectionAdapter;
    private RecyclerView mRecyclerView;
    private Context context;
  //  Loader loader;
    Dialog progressDialog;
    SessionManager sessionManager;
    private CustomAlert customAlert;
    private int page = 1;
    int loadMoreEnable = 0;
    Dialog alertDialog;
    private int totalSubsCount = 0;
    int currentSelectedPosition = 0;

    ///////////////////////////////////////
    private NewPointsSectionAdapter newPointsSectionAdapter;
    private ActivitySubscriptionListDialog activitySubscriptionListDialog;

    //////////////////////////////////////

    String strSubjectId = "", strSubjectName = "", strisTrial = "", strSubjectShortName = "";
    //  private TextView textView11;
//  ImageView imageView7;
    SwipeRefreshLayout mSwipeRefreshLayout;
    ItemOffsetDecoration itemDecoration;

    //  TextView textView12,textView14,textView13,textView15;
    ImageView imageView8, imageView10;
    //  CountDownTimer cTimer;
    LinearSnapHelper snapHelper;
    LinearLayoutManager linearLayoutManager;
    private int index = 0;
    private RecyclerView fragment_child_recyclerview;

    /*tabVersion*/
    private TabPointMainAdapter adapter;
    TabArtcileChapterClickListener tabArtcileChapterClickListener;
    private TabPointChildRecyclerAdapter tabPointChildRecyclerAdapter;
    private Boolean item_is_selected = false;
    private String strHeadId = "", strChapterId = "";
    private int currentChapterSelectedPosition = -1, currentHeadSelectedPosition = -1, currentSuperHeadSelectedPosition = -1;
    ConstraintLayout llConstrainSearch;
    EditText etSearch;
    private ImageView imgSearchClose, imgSearchGo;
    private HomeSearchAdapter homeSearchAdapter;
    private RecyclerView homeSearchRecyclerView;
    private int totalCountSearch = 0;
    private ArrayList<SearchBean> searchBeanArrayList = new ArrayList<>();

    public PointsFragment(String subjectId, String subjectName, String strisTrial, String strSubjectShortName, String strHeadId, String strChapterId) {
        this.strSubjectId = subjectId;
        this.strSubjectName = subjectName;
        this.strSubjectShortName = strSubjectShortName;
        this.strisTrial = strisTrial;
        this.strHeadId = strHeadId;
        this.strChapterId = strChapterId;
    }
    public PointsFragment(){

    }

    private void CreateLoader() {
     //   progressDialog = new Dialog(getActivity());
       // progressDialog.setContentView(R.layout.custom_progress);


        /*if (progressDialog.getWindow() != null)
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));*/
        // startAnimation();
       // progressDialog.setCancelable(false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupStatusBarColor();
        sessionManager = new SessionManager(getActivity());
        customAlert = new CustomAlert(getActivity());

        linearLayoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        alertDialog = new Dialog(getActivity());
        createSnap();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_points, container, false);
       // CreateLoader();
        if (getActivity().getResources().getBoolean(R.bool.isTablet))
            initTab(root);
        else
            init(root);
        return root;
    }

    private void init(View rootView) {

        tempArrayList=new ArrayList<>();
        mSwipeRefreshLayout = rootView.findViewById(R.id.mSwipeRefreshLayout);
        mSwipeRefreshLayout.setEnabled(false);
        mSwipeRefreshLayout.setRefreshing(false);

        mRecyclerView = rootView.findViewById(R.id.recyclerViewPoints);

        imageView8 = rootView.findViewById(R.id.imageView8);
        llConstrainSearch = rootView.findViewById(R.id.llConstrainSearch);
        etSearch = rootView.findViewById(R.id.etSearch);
        imgSearchClose = rootView.findViewById(R.id.imgSearchClose);
        imgSearchGo = rootView.findViewById(R.id.imgSearchGo);


        imgSearchClose.setOnClickListener(this);
        imgSearchGo.setOnClickListener(this);
        itemDecoration = new ItemOffsetDecoration(context, R.dimen._4sdp);
        imageView8.setOnClickListener(this);


        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!etSearch.getText().toString().isEmpty() && etSearch.getText().toString().length() > 3) {
                    imgSearchClose.setVisibility(View.GONE);
                    imgSearchGo.setVisibility(View.VISIBLE);
                }
            }
        });
        page = 1;
       // loader = new Loader(getActivity());
       // CreateLoader();
        populateList();
        swipeRefresh();
        LoadMoreListener();
    }

    private void initTab(View root) {
        mRecyclerView = root.findViewById(R.id.parent_recyclerview);

        // mSwipeRefreshLayout = root.findViewById(R.id.mSwipeRefreshLayout);


        fragment_child_recyclerview = root.findViewById(R.id.fragment_child_recyclerview);


        page = 1;
        populateList();
        //swipeRefresh();

        LoadMoreListener();
        //onScrollRecyclerview();

    }

    private void swipeRefresh() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadMoreEnable = 0;
                page = 1;
                mList = new ArrayList<>();
                pointsSectionArrayList = new ArrayList<>();
                populateList();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void populateList() {

        mList.clear();
        pointsSectionArrayList.clear();
        index = 0;
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("subject_id", strSubjectId);
        params.put("page", "" + page);
        params.put("skip", "0");
        params.put("limit", "10");
        mRequest.makeServiceRequest(IConstant.allPoints, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------allPoints Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalSubsCount = object.getInt("ctotal");
                        JSONArray jsonArray = object.getJSONArray("chapters");
                        if (jsonArray.length() > 0) {
                            //llEmpty.setVisibility(View.GONE);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                index = index + 1;
                                CategoryPoints listItem = new CategoryPoints();
                                PointsHeaderModel pointsHeaderModel = new PointsHeaderModel(index);
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                listItem._id = jsonObject.getString("_id");
                                listItem.chapt_name = jsonObject.getString("chapt_name");
                                listItem.long_name = jsonObject.getString("long_name");
                                listItem.short_name = jsonObject.getString("short_name");
                                listItem.images = jsonObject.getString("img");

                                pointsHeaderModel.setId(jsonObject.getString("_id"));
                                pointsHeaderModel.setChaptName(jsonObject.getString("chapt_name"));
                                pointsHeaderModel.setLongName(jsonObject.getString("long_name"));
                                pointsHeaderModel.setShortName(jsonObject.getString("short_name"));
                                pointsHeaderModel.setImage(jsonObject.getString("img"));
                                pointsHeaderModel.setCount(String.valueOf(index));
                                pointsSectionArrayList.add(pointsHeaderModel);

                                PointsChildModel pointsChildModel = new PointsChildModel(index);

                                pointsChildModel.setId(jsonObject.getString("_id"));
                                pointsChildModel.setChaptName(jsonObject.getString("chapt_name"));
                                pointsChildModel.setLongName(jsonObject.getString("long_name"));
                                pointsChildModel.setShortName(jsonObject.getString("short_name"));
                                pointsChildModel.setImage(jsonObject.getString("img"));
                                pointsChildModel.setCount(String.valueOf(index));
                                if (jsonObject.getString("_id").equalsIgnoreCase(strChapterId))
                                    currentChapterSelectedPosition = i + 1;
                                ArrayList<PointsContent> pointsContents = new ArrayList<>();
                                if (jsonObject.getJSONArray("pointDetails").length() > 0) {
                                    for (int j = 0; j < jsonObject.getJSONArray("pointDetails").length(); j++) {
                                        PointsContent pointsContent = new PointsContent();
                                        pointsContent.set_id(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("_id"));
                                        pointsContent.setShort_name(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("short_name"));
                                        pointsContent.setLong_name(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("long_name"));
                                        pointsContent.setUrl(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("url"));
                                        pointsContent.setCount(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("charcount"));
                                        //Object content = jsonObject.getJSONArray("pointDetails").getJSONObject(j).get("content");


                                       /* if (content instanceof JSONArray) {
                                            JSONArray contentArray = jsonObject.getJSONArray("pointDetails").getJSONObject(j).getJSONArray("content");
                                            if (contentArray.length() > 0) {
                                                ArrayList<PointsList> pointsTitleContentList = new ArrayList<>();

                                                for (int k = 0; k < contentArray.length(); k++) {
                                                    JSONObject jsonObject1 = contentArray.getJSONObject(k);
                                                    PointsList pointsList = new PointsList();
                                                    pointsList.setStrTitile(jsonObject1.getString("title"));
                                                    pointsList.setStrBody(jsonObject1.getString("body"));
                                                    pointsTitleContentList.add(pointsList);
                                                }
                                                pointsContent.setPointsTitleContentList(pointsTitleContentList);
                                            }
                                        } else {
                                            pointsContent.setContent(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("content"));
                                        }*/
//                    pointsContent.setStatus(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("status"));
//                    pointsContent.setCstatus(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("cstatus"));
                                        pointsContent.setPoint_name(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("point_name"));
                                        pointsContents.add(pointsContent);


                                    }
                                }
                                pointsChildModel.setPointsContentArrayList(pointsContents);
                                pointsSectionArrayList.add(pointsChildModel);
                                listItem.pointsContentArrayList = pointsContents;
                                mList.add(listItem);
                            }
                            mRecyclerView.setVisibility(View.VISIBLE);
                            if (getActivity().getResources().getBoolean(R.bool.isTablet))
                                setAdapter(0);
                            else
                                setupRecyclerView(0);
                        }

                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else if (sStatus.equalsIgnoreCase("2")) {
                        packageExpiredAlert(message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {

                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });

    }

    private void setAdapter(int loadMore) {
        if (loadMore == 0) {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                    RecyclerView.VERTICAL, false));


            mRecyclerView.setNestedScrollingEnabled(false);


            adapter = new TabPointMainAdapter(getActivity(), mList, this);
            mRecyclerView.setAdapter(adapter);
            /*set Child View*/
            ChildAdapterSet(0);
            tabArtcileChapterClickListener.onChapterClicked(mList.get(0).long_name, mList.get(0)._id, "", "");
        } else {
            adapter.notifyDataSetChanged();
        }

        item_is_selected = true;


   /* int viewHeight = articleList.size() * adapter.getItemCount();
    mRecyclerView.getLayoutParams().height = viewHeight;*/


    }

    private void ChildAdapterSet(int SelectedPosition) {

        // fragment_child_recyclerview.setHasFixedSize(true);

        fragment_child_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(),
                RecyclerView.VERTICAL, false));

 /*       LinearLayout.LayoutParams lp =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 1000);
        fragment_child_recyclerview.setLayoutParams(lp);*/


        fragment_child_recyclerview.setNestedScrollingEnabled(false);

        tabPointChildRecyclerAdapter = new TabPointChildRecyclerAdapter(getActivity(), mList, this, SelectedPosition, strSubjectId, strSubjectName);
        fragment_child_recyclerview.setAdapter(tabPointChildRecyclerAdapter);

    /*int viewHeight = articleList.size() * child_adapter.getItemCount();
    fragment_child_recyclerview.getLayoutParams().height = 4000;*/
    }

    private void setupRecyclerView(int lastPosition) {
   /* mRecyclerView.setLayoutManager(linearLayoutManager);
      snapHelper.attachToRecyclerView(mRecyclerView);

    mRecyclerView.removeItemDecoration(itemDecoration);
    mRecyclerView.addItemDecoration(itemDecoration);

    mAdapter = new SubscriptionCategoryPointsAdapter(getActivity(), mList,strSubjectId,strSubjectShortName);
    mRecyclerView.setAdapter(mAdapter);
    mRecyclerView.scrollToPosition(lastPosition);*/

        if (lastPosition == 0) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            //mRecyclerView.setLayoutManager(linearLayoutManager);
            mRecyclerView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(context));
            pointsSectionAdapter = new PointsSectionAdapter(context, this::onItemClickListener, pointsSectionArrayList, strSubjectId, strSubjectShortName, currentChapterSelectedPosition, currentHeadSelectedPosition);
            mRecyclerView.setAdapter(pointsSectionAdapter);
            StickyHeaderItemDecorator decorator = new StickyHeaderItemDecorator(pointsSectionAdapter);
            decorator.attachToRecyclerView(mRecyclerView);
            performClickAction(linearLayoutManager);
            findCategoryList(strChapterId);
        } else
            pointsSectionAdapter.notifyDataSetChanged();
        if (currentChapterSelectedPosition > 0) {
            //mRecyclerView.scrollToPosition(currentChapterSelectedPosition);
            mRecyclerView.smoothScrollToPosition(currentChapterSelectedPosition);
        }

    }

    private void performClickAction(final LinearLayoutManager linearLayoutManager) {
        pointsSectionAdapter.setOnItemClickListener(new ItemClickListener() {
            @Override
            public void onParentItemClick(int position) {
                currentSelectedPosition = position;
                int offset = position - linearLayoutManager.findFirstVisibleItemPosition();
                if (linearLayoutManager.findFirstVisibleItemPosition() > 0) offset -= 1;
                //linearLayoutManager.scrollToPositionWithOffset(position, offset);

                smoothScroll(mRecyclerView, position, 280);
            }
        });
    }

    private static void smoothScroll(RecyclerView rv, int toPos, final int duration) throws IllegalArgumentException {
        final int TARGET_SEEK_SCROLL_DISTANCE_PX = 10000;     // See androidx.recyclerview.widget.LinearSmoothScroller
        int itemHeight = rv.getChildAt(0).getHeight();  // Height of first visible view! NB: ViewGroup method!
        itemHeight = itemHeight + 33;                   // Example pixel Adjustment for decoration?
        int fvPos = ((LinearLayoutManager) rv.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        int i = Math.abs((fvPos - toPos) * itemHeight);
        if (i == 0) {
            i = (int) Math.abs(rv.getChildAt(0).getY());
        }
        final int totalPix = i;                         // Best guess: Total number of pixels to scroll
        RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(rv.getContext()) {
            @Override
            protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }

            @Override
            protected int calculateTimeForScrolling(int dx) {
                int ms = (int) (duration * dx / (float) totalPix);
                // Now double the interval for the last fling.
                if (dx < TARGET_SEEK_SCROLL_DISTANCE_PX) {
                    ms = ms * 2;
                } // Crude deceleration!
                //lg(format("For dx=%d we allot %dms", dx, ms));
                return ms;
            }
        };
        //lg(format("Total pixels from = %d to %d = %d [ itemHeight=%dpix ]", fvPos, toPos, totalPix, itemHeight));
        smoothScroller.setTargetPosition(toPos);
        rv.getLayoutManager().startSmoothScroll(smoothScroller);
    }

    private void LoadMoreListener() {
        if (mRecyclerView != null) {
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                    try {

                        LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                        int totalItemCount = layoutManager.getItemCount();
                        int lastVisible = layoutManager.findLastVisibleItemPosition();
                        if (layoutManager.findFirstVisibleItemPosition() >= 0 && !getActivity().getResources().getBoolean(R.bool.isTablet))
                            pointsSectionAdapter.setTopItemView(pointsSectionArrayList.get(layoutManager.findFirstVisibleItemPosition()).getLongName(), pointsSectionArrayList.get(layoutManager.findFirstVisibleItemPosition()).getCount(), layoutManager.findFirstVisibleItemPosition());
                        boolean endHasBeenReached = lastVisible + 5 >= totalItemCount;
                        if (dy > 0) {
                            {
                                if (currentSelectedPosition < layoutManager.findFirstVisibleItemPosition())
                                    pointsSectionAdapter.CollapseExpandedView(-1);
                            }
                        } else {
                            if (currentSelectedPosition > lastVisible)
                                pointsSectionAdapter.CollapseExpandedView(-1);
                        }
                        if ((lastVisible == totalItemCount - 1 || lastVisible == totalItemCount - 2) && totalSubsCount > index && loadMoreEnable == 0) {
                            page = page + 1;
                            Log.e("loadMoreSubjects()", "Called");
                            loadMoreEnable = 1;
                            loadMoreArticle(index);
                        }

                    } catch (Exception e) {

                        Log.e("Exception", e.toString());
                    }


                }
            });
        }
    }

    private void loadMoreArticle(final int position) {

        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("subject_id", strSubjectId);
        params.put("page", "" + page);
        params.put("skip", "0");
        params.put("limit", "10");
        mRequest.makeServiceRequest(IConstant.allPoints, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------allPoints Response----------------" + response);
                String sStatus = "";
                String message = "";

                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalSubsCount = object.getInt("ctotal");
                        JSONArray jsonArray = object.getJSONArray("chapters");
                        if (jsonArray.length() > 0) {
                            //llEmpty.setVisibility(View.GONE);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                index = index + 1;
                                CategoryPoints listItem = new CategoryPoints();
                                PointsHeaderModel pointsHeaderModel = new PointsHeaderModel(index);
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                listItem._id = jsonObject.getString("_id");
                                listItem.chapt_name = jsonObject.getString("chapt_name");
                                listItem.long_name = jsonObject.getString("long_name");
                                listItem.short_name = jsonObject.getString("short_name");
                                listItem.images = jsonObject.getString("img");


                                pointsHeaderModel.setId(jsonObject.getString("_id"));
                                pointsHeaderModel.setChaptName(jsonObject.getString("chapt_name"));
                                pointsHeaderModel.setLongName(jsonObject.getString("long_name"));
                                pointsHeaderModel.setShortName(jsonObject.getString("short_name"));
                                pointsHeaderModel.setImage(jsonObject.getString("img"));
                                pointsHeaderModel.setCount(String.valueOf(index));
                                pointsSectionArrayList.add(pointsHeaderModel);

                                PointsChildModel pointsChildModel = new PointsChildModel(index);
                                pointsChildModel.setId(jsonObject.getString("_id"));
                                pointsChildModel.setChaptName(jsonObject.getString("chapt_name"));
                                pointsChildModel.setLongName(jsonObject.getString("long_name"));
                                pointsChildModel.setShortName(jsonObject.getString("short_name"));
                                pointsChildModel.setImage(jsonObject.getString("img"));
                                pointsChildModel.setCount(String.valueOf(index));
                                ArrayList<PointsContent> pointsContents = new ArrayList<>();
                                if (jsonObject.getJSONArray("pointDetails").length() > 0) {
                                    for (int j = 0; j < jsonObject.getJSONArray("pointDetails").length(); j++) {
                                        PointsContent pointsContent = new PointsContent();
                                        pointsContent.set_id(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("_id"));
                                        pointsContent.setShort_name(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("short_name"));
                                        pointsContent.setLong_name(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("long_name"));
                                        pointsContent.setContent(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("content"));
//                    pointsContent.setStatus(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("status"));
//                    pointsContent.setCstatus(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("cstatus"));
                                        pointsContent.setPoint_name(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("point_name"));
                                        pointsContents.add(pointsContent);


                                    }
                                }
                                pointsChildModel.setPointsContentArrayList(pointsContents);
                                pointsSectionArrayList.add(pointsChildModel);
                                listItem.pointsContentArrayList = pointsContents;
                                mList.add(listItem);
                            }
                            loadMoreEnable = 0;
                            setupRecyclerView(position);
                        }

                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else if (sStatus.equalsIgnoreCase("2")) {
                        packageExpiredAlert(message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {

                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });

    }

    private void setupStatusBarColor() {
        ((TabMainActivity) context).updateStatusBarColor(context.getResources()
                .getColor(R.color.category_points_header_green));
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onResume() {
        super.onResume();
     /*TabMainActivity.isReadPage = false;
    setTimer();*/

        linearLayoutManager = new LinearLayoutManager(getActivity(),
                RecyclerView.VERTICAL, false);


    }

    @Override
    public void onPause() {
   /* if(cTimer != null && !TabMainActivity.isReadPage) {
      cTimer.cancel();
      cTimer = null;
    }*/

        super.onPause();
    }

    @Override
    public void onStop() {

        super.onStop();

    /*if(cTimer != null && !TabMainActivity.isReadPage) {
      cTimer.cancel();
      cTimer = null;
    }*/
    }

    private void packageExpiredAlert(String message) {

        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(getString(R.string.alert_oops));
        textViewDesc.setText(message);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                new AsyncUpdateSessionRunner("1").execute();
                TabMainActivity.tabMainActivity.finish();
            }
        });
        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity;
        if (activity instanceof TabArtcileChapterClickListener) {
            tabArtcileChapterClickListener = (TabArtcileChapterClickListener) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        tabArtcileChapterClickListener = null;
    }

    @Override
    public void onParentItemClick(int position) {
        ChildAdapterSet(position);
        tabArtcileChapterClickListener.onChapterClicked(mList.get(position).long_name, mList.get(position)._id, "", "");
    }

    @Override
    public void onHeadingClicked(int parentPosition, int childPosition, String headId) {
        EventbusPojo pojo = new EventbusPojo();
        pojo.setChapterId(mList.get(parentPosition)._id);
        pojo.setHeadId(headId);
        pojo.setChapterName(mList.get(parentPosition).long_name);
        pojo.setFragment_name("Points");
        EventBus.getDefault().post(pojo);
    }

    @Override
    public void onSearch(String headId, String ChapterId) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageView8:
                openSearch();
                break;

            case R.id.imgSearchClose:
                closeSearch();
                break;

            case R.id.imgSearchGo:
                searchAPMT();
                break;
        }
    }

    private void openSearch() {
        InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
        etSearch.requestFocus();
        llConstrainSearch.setVisibility(View.VISIBLE);
    }

    private void closeSearch() {
        etSearch.setText(null);
        InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(requireView().getWindowToken(), 0);
        llConstrainSearch.setVisibility(View.GONE);
    }

    private void searchAPMT() {
        llConstrainSearch.setVisibility(View.GONE);
        openSearchList(2, etSearch.getText().toString());
    }

    private void openSearchList(final int type, String searchKey) {
        etSearch.setText("");
        llConstrainSearch.setVisibility(View.GONE);
        alertDialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        alertDialog.setContentView(R.layout.homesearchlistdialog);
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(false);
           /* if(alertDialog.getWindow() != null) {
                alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }*/
        homeSearchRecyclerView = alertDialog.findViewById(R.id.homeSearchRecyclerView);
        final RelativeLayout rlEmptySearch = alertDialog.findViewById(R.id.rlEmptySearch);
        final ImageView imgSearchResultClose = alertDialog.findViewById(R.id.imgSearchResultClose);
        imgSearchResultClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog.isShowing())
                    alertDialog.dismiss();
            }
        });
        searchBeanArrayList.clear();
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("page", "" + page);
        params.put("search", "" + type);
        params.put("keyword", searchKey);
        mRequest.makeServiceRequest(IConstant.homeSearch, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------homeSearch Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalCountSearch = Integer.parseInt(object.getString("total"));

                        JSONArray testArray = object.getJSONArray("content");
                        if (testArray.length() > 0) {
                            rlEmptySearch.setVisibility(View.GONE);
                            for (int i = 0; i < testArray.length(); i++) {
                                SearchBean searchBean = new SearchBean();
                                JSONObject jsonObject = testArray.getJSONObject(i);
                                searchBean.setStrSubjectId(jsonObject.getString("subject_id"));
                                searchBean.setStrSubjectShortName(jsonObject.getString("subject_short_name"));
                                searchBean.setStrSubjectLongName(jsonObject.getString("subject_long_name"));
                                searchBean.setStrMImage(jsonObject.getString("mob_img"));
                                searchBean.setStrTImage(jsonObject.getString("tab_img"));
                                searchBean.setStrChaptId(jsonObject.getString("chapter_id"));
                                searchBean.setStrChaptLongName(jsonObject.getString("chapter_long_name"));
                                searchBean.setStrChaptShortName(jsonObject.getString("chapter_short_name"));
                                searchBean.setStrHeadId(jsonObject.has("heading_id") ? jsonObject.getString("heading_id") : "");
                                searchBean.setStrHeadLongName(jsonObject.has("heading_long_name") ? jsonObject.getString("heading_long_name") : "");
                                searchBean.setStrHeadShortName(jsonObject.has("heading_short_name") ? jsonObject.getString("heading_short_name") : "");
                                searchBean.setStrIsSubscribed(jsonObject.getString("subscribed_user"));
                                searchBeanArrayList.add(searchBean);

                            }

                            setupSearchAdapter(type);
                        } else {
                            rlEmptySearch.setVisibility(View.VISIBLE);
                        }

                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);

            }
        });
        if (!alertDialog.isShowing())
            alertDialog.show();

    }

    private void setupSearchAdapter(int menuType) {

        homeSearchAdapter = new HomeSearchAdapter(searchBeanArrayList, getActivity(), menuType);
        homeSearchRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        //snapHelper.attachToRecyclerView(homeSearchRecyclerView);
        homeSearchRecyclerView.setAdapter(homeSearchAdapter);
        homeSearchRecyclerView.setVisibility(View.VISIBLE);
        performSearchClick();
    }

    private void performSearchClick() {
        homeSearchAdapter.setOnSearchItemClickListener(new OnSearchItemClickListener() {
            @Override
            public void onSearchItemClicked(int position, int menuType) {
                if (alertDialog != null && alertDialog.isShowing())
                    alertDialog.dismiss();
                strHeadId = searchBeanArrayList.get(position).getStrHeadId();
                strChapterId = searchBeanArrayList.get(position).getStrChaptId();
                findCategoryList(searchBeanArrayList.get(position).getStrChaptId());
               // populateList();
            }
        });
    }


    //PointsSectionAdapter listener
    @Override
    public void onItemClickListener(String rootId) {
        tempArrayList.clear();
        int rooPosition = 0;
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i)._id.equalsIgnoreCase(rootId)) {
                rooPosition = i;
                break;
            }
        }

        for (int j=0;j<mList.get(rooPosition).pointsContentArrayList.size();j++){
            tempArrayList.add(new NewPointSectionModel(mList.get(rooPosition).pointsContentArrayList.get(j).get_id(),mList.get(rooPosition).pointsContentArrayList.get(j).getPoint_name(),mList.get(rooPosition).pointsContentArrayList.get(j).getShort_name(),mList.get(rooPosition).pointsContentArrayList.get(j).getLong_name(),mList.get(rooPosition).pointsContentArrayList.get(j).getStatus(),mList.get(rooPosition).pointsContentArrayList.get(j).getCstatus(),mList.get(rooPosition).pointsContentArrayList.get(j).getContent()));
        }


        CustomDialogActivity.pointsSectionList=tempArrayList;
        CustomDialogActivity.pointsSectionArrayList=pointsSectionArrayList;
        CustomDialogActivity.strSubjectId=strSubjectId;
        CustomDialogActivity.strSubjectShortName=strSubjectShortName;
        CustomDialogActivity.rooId=rootId;
        CustomDialogActivity.ParentLongName=mList.get(rooPosition).long_name;
        CustomDialogActivity.ImageUrl=mList.get(rooPosition).images;
        CustomDialogActivity.ParentPosition=rooPosition;
        CustomDialogActivity.currentPage=getResources().getString(R.string.points);
        CustomDialogActivity.SearchHeadingLongName="";
        startActivity(new Intent(getActivity(), CustomDialogActivity.class));


      //  newPointsSectionAdapter = new NewPointsSectionAdapter(getActivity(),tempArrayList,pointsSectionArrayList, strSubjectId, strSubjectShortName,rootId);
      //  activitySubscriptionListDialog=new ActivitySubscriptionListDialog(getActivity(),newPointsSectionAdapter,mList.get(rooPosition).long_name,rooPosition,mList.get(rooPosition).images,getResources().getString(R.string.points));
       // activitySubscriptionListDialog.setCanceledOnTouchOutside(true);
       // activitySubscriptionListDialog.show();
      /*  pointsSectionAdapter = new PointsSectionAdapter(context, this::onItemClickListener,pointsSectionArrayList, strSubjectId, strSubjectShortName,currentChapterSelectedPosition,currentHeadSelectedPosition);
        mRecyclerView.setAdapter(pointsSectionAdapter);*/


    }
    public void findCategoryList(String rootId){
      if (rootId!=null && !rootId.equals("")){
          tempArrayList.clear();
          int rooPosition = 0;
          for (int i = 0; i < mList.size(); i++) {
              if (mList.get(i)._id.equalsIgnoreCase(rootId)) {
                  rooPosition = i;
                  break;
              }
          }
          for (int j=0;j<mList.get(rooPosition).pointsContentArrayList.size();j++){
              tempArrayList.add(new NewPointSectionModel(mList.get(rooPosition).pointsContentArrayList.get(j).get_id(),mList.get(rooPosition).pointsContentArrayList.get(j).getPoint_name(),mList.get(rooPosition).pointsContentArrayList.get(j).getShort_name(),mList.get(rooPosition).pointsContentArrayList.get(j).getLong_name(),mList.get(rooPosition).pointsContentArrayList.get(j).getStatus(),mList.get(rooPosition).pointsContentArrayList.get(j).getCstatus(),mList.get(rooPosition).pointsContentArrayList.get(j).getContent()));
          }
          CustomDialogActivity.pointsSectionList=tempArrayList;
          CustomDialogActivity.pointsSectionArrayList=pointsSectionArrayList;
          CustomDialogActivity.strSubjectId=strSubjectId;
          CustomDialogActivity.strSubjectShortName=strSubjectShortName;
          CustomDialogActivity.rooId=rootId;
          CustomDialogActivity.ParentLongName=mList.get(rooPosition).long_name;
          CustomDialogActivity.ImageUrl=mList.get(rooPosition).images;
          CustomDialogActivity.ParentPosition=rooPosition;
          CustomDialogActivity.currentPage=getResources().getString(R.string.points);
          CustomDialogActivity.SearchHeadingLongName="";
          strChapterId="";
          startActivity(new Intent(getActivity(), CustomDialogActivity.class));
      }

    }

    private class AsyncUpdateSessionRunner extends AsyncTask<String, String, String> {
        String result = "";
        String callingType = "";

        public AsyncUpdateSessionRunner(String s) {
            callingType = s;
        }

        @Override
        protected String doInBackground(String... strings) {
            ServiceRequest mRequest = new ServiceRequest(getActivity());
            HashMap<String, String> params = new HashMap<>();
            params.put("session_id", TabMainActivity.strSessionId);

            params.put("time", String.valueOf(TabMainActivity.startingLevelTime - TabMainActivity.packageTime));
            params.put("close", callingType);

            Log.e("startingLevelTime", "" + TabMainActivity.startingLevelTime + " , packageTime : " + TabMainActivity.packageTime);
            TabMainActivity.startingLevelTime = TabMainActivity.packageTime;
            mRequest.makeServiceRequest(IConstant.updateSession, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("updateSession" + "-------- Response----------------" + response);
                    String sStatus = "";
                    String message = "";
                    try {
                        JSONObject obj = new JSONObject(response);
                        sStatus = obj.getString("status");
                        message = obj.has("message") ? obj.getString("message") : "";
                        result = sStatus;

                        if (sStatus.equalsIgnoreCase("1")) {
                            //JSONObject object = obj.getJSONObject("response");


                        } else if (sStatus.equals("00")) {
                            //customAlert.singleLoginAlertLogout();
                        } else if (sStatus.equalsIgnoreCase("01")) {
                            //customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                        } else {
                            //customAlert.showAlertOk(getString(R.string.alert_oops),message);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onErrorListener(String errorMessage) {
                    // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
                }
            });
            return result;


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Log.e("status", s);
            // new AsyncSubjectsRunner(s).execute();
        }
    }

    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
    }

    private void onScrollRecyclerview() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            mRecyclerView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                    if (item_is_selected) {


                    }

                }
            });

        } else {

            mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    if (item_is_selected) {

                       /* item_is_selected=false;

                        ChildViewGone();*/
                    }
                }
            });
        }
    }
}