package com.app.neetbook.View.Fragments.subscription;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.neetbook.R;
import com.app.neetbook.Utils.ActionBarActivityNeetBook;
import com.app.neetbook.View.TestActivity;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

public class SubscriptionDetailFragment extends ActionBarActivityNeetBook implements View.OnClickListener {

  private TabLayout tabLayout;
  private ViewPager viewPager;
  private Context context;
  ImageView imageView2;

  public SubscriptionDetailFragment() {
    // Required empty public constructor
  }

  //
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.fragment_subscription_detail);
    viewPager = findViewById(R.id.viewpager_subscription_category);
    tabLayout = findViewById(R.id.tabs_subscription_category);
    imageView2 = findViewById(R.id.imageView2);
    setupViewPager(viewPager);
    imageView2.setOnClickListener(this);
  }

  private void setupViewPager(final ViewPager viewPager) {
    final ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(),
        this);
    viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(), "Patho ");
    viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(), "Clini ");
    viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(), "Tendo ");
    viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(), "Arthr ");
    viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(), "Foot ");
    viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(), "Knee ");
    viewPager.setAdapter(viewPagerAdapter);

    tabLayout.setupWithViewPager(viewPager);
    for (int i = 0; i < tabLayout.getTabCount(); i++) {
      TabLayout.Tab tab = tabLayout.getTabAt(i);
      if (tab != null) {
        tab.setCustomView(viewPagerAdapter.getTabView(i));
      }
    }
  }

  @Override
  public void onClick(View v) {
    switch (v.getId())
    {
      case R.id.imageView2:
        startActivity(new Intent(SubscriptionDetailFragment.this, TestActivity.class));
    }
  }

  private class ViewPagerAdapter extends FragmentPagerAdapter {

    List<Fragment> fragmentList = new ArrayList<>();
    List<String> fragmentTitles = new ArrayList<>();
    Context mContext;
    TextView tabTitile;

    ViewPagerAdapter(FragmentManager fragmentManager,
                     FragmentActivity activity) {
      super(fragmentManager);
      mContext = activity;
    }

    @Override
    public Fragment getItem(int position) {
      return SubscrptionDetailListFragment.newInstance(position + 1);
    }

    @Override
    public int getCount() {
      return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
      return fragmentTitles.get(position);
    }

    void addFragment(Fragment fragment, String name) {
      fragmentList.add(fragment);
      fragmentTitles.add(name);
    }

    View getTabView(int position) {
      // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
      View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_items, null);
      tabTitile = v.findViewById(R.id.txtTabTitle);
      tabTitile.setText(fragmentTitles.get(position));
      tabTitile.setTextColor(getResources().getColor(R.color.white));
      return v;
    }
  }


}
