package com.app.neetbook.View.SideMenu;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.neetbook.Adapter.ArticleTabsAdapter;
import com.app.neetbook.Interfaces.OnMobileHomeMenuSelectedListioner;
import com.app.neetbook.Model.HeadData;
import com.app.neetbook.Model.PointsContent;
import com.app.neetbook.Model.sidemenuFromContent.CategoryPoints;
import com.app.neetbook.R;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.OnSwipeTouchListener;
import com.app.neetbook.Utils.SessionManager;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;


public class TabPointsMainFragment extends Fragment implements View.OnClickListener {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Context context;
    Loader loader;
    SessionManager sessionManager;
    private CustomAlert customAlert;
    private int page = 1;
    int loadMoreEnable = 0;
    private int totalSubsCount;
    String strSubjectId= "",strChapterId= "";
    private ImageView imageView2,imageView3;
    private TextView textView,txtSurgical;
    ArrayList<CategoryPoints> pointsArrayList;
    
    ArticleTabsAdapter homeGroupNameAdapter;
    LinearLayoutManager layoutManager;
    LinearSnapHelper snapHelper;
    RecyclerView recyclerViewGroups;
    private ArrayList<HeadData> headDataList;
    Typeface tfBold,tfMedium,tfRegular;
    private int currentSelectedPosition = 0;
    LinearLayout llParent;
    private String strPoint_id, strChapterName, strSubjectName;
    private RelativeLayout rlParentRight,rlParent;
    public TabPointsMainFragment(String strPoint_id,String strChapterId,String strSubjectId,String strChapterName,String strSubjectName) {
        // Required empty public constructor
        this.strPoint_id = strPoint_id;
        this.strChapterId = strChapterId;
        this.strSubjectId = strSubjectId;
        this.strChapterName = strChapterName;
        this.strSubjectName = strSubjectName;
    }

   

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab_points_main, container, false);
        init(view);
        return view;
    }

    private void init(View view) {

        tfBold  = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium  = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular  = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaRegular.otf");
        sessionManager = new SessionManager(getActivity());
        customAlert = new CustomAlert(getActivity());
        loader = new Loader(getActivity());
        headDataList = new ArrayList<>();
        viewPager = view.findViewById(R.id.viewpager_subscription_category);
        tabLayout = view.findViewById(R.id.tabs_subscription_category);
        imageView2 = view.findViewById(R.id.imageView2);
        imageView3 = view.findViewById(R.id.imageView3);
        textView = view.findViewById(R.id.textView);
        txtSurgical = view.findViewById(R.id.textView2);
        llParent = view.findViewById(R.id.llParent);
        /*rlParent = view.findViewById(R.id.rlParent);*/
        recyclerViewGroups = view.findViewById(R.id.recyclerViewGroups);

        createSnap();
        setupViewPager(viewPager);

        listener();
        setFontSize();
        
    }

    private void setFontSize() {

        if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
            textView.setTextAppearance(getActivity(),R.style.textViewSmallSubjectName);
            txtSurgical.setTextAppearance(getActivity(),R.style.textViewSmallChaptName);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
            textView.setTextAppearance(getActivity(),R.style.textViewMediumSubjectName);
            txtSurgical.setTextAppearance(getActivity(),R.style.textViewMediumChaptName);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
            textView.setTextAppearance(getActivity(),R.style.textViewLargeSubjectName);
            txtSurgical.setTextAppearance(getActivity(),R.style.textViewLargeChaptName);
        }

        textView.setTypeface(tfMedium);
        txtSurgical.setTypeface(tfMedium);
    }
    private void listener() {

        imageView3.setOnClickListener(this);
        imageView2.setOnClickListener(this);

        /*rlParent.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            public void onSwipeTop() {
                //Toast.makeText(SubscriptionArticleActivity.this, "top", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeRight() {
                //Toast.makeText(SubscriptionArticleActivity.this, "right", Toast.LENGTH_SHORT).show();
               // finish();
            }
            public void onSwipeLeft() {

            }
            public void onSwipeBottom() {
                //Toast.makeText(SubscriptionArticleActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });*/
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageView2:
               // finish();
                break;

            case R.id.imageView3:
                //finish();
                break;
        }
    }


    private void setupViewPager(final ViewPager viewPager) {
        final ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager(),
                getActivity());

        if(SubscriptionPointsActivity.pointsContentArrayList.size()>0)
        {
            for (int i = 0; i < SubscriptionPointsActivity.pointsContentArrayList.size(); i++) {
                HeadData data = new HeadData();
                data._id = SubscriptionPointsActivity.pointsContentArrayList.get(i).get_id();
                data.short_name = SubscriptionPointsActivity.pointsContentArrayList.get(i).getShort_name();
                data.long_name = SubscriptionPointsActivity.pointsContentArrayList.get(i).getLong_name();
                headDataList.add(data);
                viewPagerAdapter.addFragment(new SubscriptionPointsFragment(strSubjectId,strChapterId,SubscriptionPointsActivity.pointsContentArrayList.get(i).get_id(),SubscriptionPointsActivity.pointsContentArrayList.get(i).getLong_name(), SubscriptionPointsActivity.pointsContentArrayList.get(i).getContent(),SubscriptionPointsActivity.pointsContentArrayList.get(i).getPointsTitleContentList(),SubscriptionPointsActivity.pointsContentArrayList.get(i).getUrl(),0), SubscriptionPointsActivity.pointsContentArrayList.get(i).getShort_name());
                if(strPoint_id.equalsIgnoreCase(SubscriptionPointsActivity.pointsContentArrayList.get(i).get_id()))
                    currentSelectedPosition = i;

            }
        }
        setGroupAdapter(currentSelectedPosition);
        layoutManager = ((LinearLayoutManager)recyclerViewGroups.getLayoutManager());
        int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
        int centeredItemPosition = totalVisibleItems / 2;
        recyclerViewGroups.smoothScrollToPosition(currentSelectedPosition != 0 ?currentSelectedPosition-1 : currentSelectedPosition);
        viewPager.setAdapter(viewPagerAdapter);

        tabLayout.setupWithViewPager(viewPager);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(viewPagerAdapter.getTabView(i));
            }
        }

        viewPager.setCurrentItem(currentSelectedPosition);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setGroupAdapter(position);
                layoutManager = ((LinearLayoutManager)recyclerViewGroups.getLayoutManager());
                int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                int centeredItemPosition = totalVisibleItems / 2;
                recyclerViewGroups.smoothScrollToPosition(position != 0 ?position-1 : position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    private void setGroupAdapter(int position) {
        homeGroupNameAdapter = new ArticleTabsAdapter(headDataList, getActivity(),position);
        recyclerViewGroups.setLayoutManager(new LinearLayoutManager(getActivity(),RecyclerView.HORIZONTAL,false));
        snapHelper.attachToRecyclerView(recyclerViewGroups);
        recyclerViewGroups.setAdapter(homeGroupNameAdapter);

        recyclerViewGroups.scrollToPosition(position);
        homeGroupNameAdapter.setOnGroupMenuSelectedListener(new OnMobileHomeMenuSelectedListioner() {
            @Override
            public void onGroupSelected(String groupId, int position) {
                viewPager.setCurrentItem(position);
                layoutManager = ((LinearLayoutManager)recyclerViewGroups.getLayoutManager());
                int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                int centeredItemPosition = totalVisibleItems / 2;
                recyclerViewGroups.smoothScrollToPosition(position != 0 ?position-1 : position);
                recyclerViewGroups.setScrollY(centeredItemPosition);
            }
        });

    }
    private class ViewPagerAdapter extends FragmentPagerAdapter {

        List<Fragment> fragmentList = new ArrayList<>();
        List<String> fragmentTitles = new ArrayList<>();
        Context mContext;
        TextView tabTitles;


        ViewPagerAdapter(FragmentManager fragmentManager,
                         FragmentActivity activity) {
            super(fragmentManager);
            mContext = activity;
        }

        @Override
        public Fragment getItem(int i) {
            return new SubscriptionPointsFragment(strSubjectId,strChapterId,SubscriptionPointsActivity.pointsContentArrayList.get(i).get_id(),SubscriptionPointsActivity.pointsContentArrayList.get(i).getLong_name(), SubscriptionPointsActivity.pointsContentArrayList.get(i).getContent(), SubscriptionPointsActivity.pointsContentArrayList.get(i).getPointsTitleContentList(),SubscriptionPointsActivity.pointsContentArrayList.get(i).getUrl(),0);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitles.get(position);
        }

        void addFragment(Fragment fragment, String name) {
            fragmentList.add(fragment);
            fragmentTitles.add(name);
        }

        View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_items, null);
            tabTitles = v.findViewById(R.id.txtTabTitle);
            tabTitles.setText(fragmentTitles.get(position));
            tabTitles.setTextColor(getResources().getColor(R.color.white));
            return v;
        }
    }

    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
    }
}
