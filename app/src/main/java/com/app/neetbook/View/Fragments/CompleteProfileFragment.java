package com.app.neetbook.View.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.app.neetbook.Interfaces.OnSpinerItemClick;
import com.app.neetbook.Model.CollegeBean;
import com.app.neetbook.Model.StateBean;
import com.app.neetbook.Model.TabSideMenu;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Data.CompleteProfileData;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.Utils.alerter.AlertBox;
import com.app.neetbook.Utils.widgets.DisplayOrientation;
import com.app.neetbook.Utils.widgets.SpinnerDialog;
import com.app.neetbook.View.DrawerContentSlideActivity;
import com.app.neetbook.View.NoInterNetActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class CompleteProfileFragment extends Fragment implements View.OnClickListener {

    ArrayList<String> countryList;
    ArrayList<StateBean> countryBeanList;
    ArrayList<String> stateList;
    ArrayList<String> countryStateList;
    ArrayList<StateBean> countryStateBeanList;
    ArrayList<StateBean> stateBeanList;
    ArrayList<String> cityList;
    ArrayList<StateBean> cityBeanList;
    ArrayList<String> collegeList;
    ArrayList<CollegeBean> collegeBeanList;
    TextView txtMail,txtMobileNumber,txtResetCompleteProfile;
    Spinner spinnerStateEducation,spinnerState,spinnerCountry,spinnerCity,spinnerCollege;
    TextView txtStateHint,txtEduStateHint,txtCityHint,txtCountryHint,txtEduCollegeHint;
    RelativeLayout rlCountryCompleteProfile,rlStateCompleteProfile,rlCityCompleteProfile,rlEduStateCompleteProfile,rlEduCollegeCompleteProfile;
    EditText etAddress2,etFirstName,etLastName,etAddress1,etPincode;
    RelativeLayout rlCompleteProfileSubmit;
    Loader loader;
    CustomAlert customAlert;
    SessionManager sessionManager;
    SpinnerDialog dialogCountry,dialogState,dialogCity,dialogEduState,dialogEduCollege;
    ConnectionDetector cd;
    private boolean showLoader = false;
    //RelativeLayout rlEduStateSpinner,rlCollegeSpinner,rlCountryStateSpinner,rlCountrySpinner,rlCitySpinner;
    public CompleteProfileFragment() {
        // Required empty public constructor
    }


    public static CompleteProfileFragment newInstance(String param1, String param2) {
        CompleteProfileFragment fragment = new CompleteProfileFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getActivity());
        cd = new ConnectionDetector(getActivity());
        loader = new Loader(getActivity());
        customAlert = new CustomAlert(getActivity());
        showLoader = false;

    }

    private void prepareSpinners() {
        countryList = new ArrayList<>();
        countryBeanList = new ArrayList<>();
        countryStateList = new ArrayList<>();
        countryStateBeanList = new ArrayList<>();
        stateList = new ArrayList<>();
        stateBeanList = new ArrayList<StateBean>();
        cityList = new ArrayList<>();
        cityBeanList = new ArrayList<>();
        collegeList = new ArrayList<>();
        collegeBeanList = new ArrayList<CollegeBean>();

        dialogCountry = new SpinnerDialog(getActivity(), countryList,
                "COUNTRY");
        dialogState = new SpinnerDialog(getActivity(), countryStateList,
                "STATE");
        dialogCity = new SpinnerDialog(getActivity(), cityList,
                "CITY");
        dialogEduState = new SpinnerDialog(getActivity(), stateList,
                "STATE");
        dialogEduCollege = new SpinnerDialog(getActivity(), collegeList,
                "COLLEGE");
        if(cd.isConnectingToInternet()) {
            getCountryList();
            getStateList();
        }else{
            startActivity(new Intent(getContext(), NoInterNetActivity.class));
            DrawerContentSlideActivity.drawerContentSlideActivity.finish();
        }
        setListeners();
        setNameandmobile();

    }

    private void getCountryList()
    {
        //loader.showLoader();
        countryList.clear();
        countryBeanList.clear();
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String,String> params = new HashMap<>();

        mRequest.makeServiceRequest(IConstant.getCountryList, Request.Method.POST, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------getCountryList Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1") && message.equalsIgnoreCase("Success")) {

                        countryList.add("COUNTRY");
                        StateBean stateBean1 = new StateBean();
                        stateBean1.set_id("-1");
                        stateBean1.setState_name("COUNTRY");
                        countryBeanList.add(stateBean1);
                        JSONArray stateListArray = object.getJSONArray("list");
                        if(stateListArray.length()>0)
                        {
                            for(int index =0; index<stateListArray.length();index++)
                            {
                                countryList.add(stateListArray.getJSONObject(index).getString("country_name"));
                                StateBean stateBean = new StateBean();
                                stateBean.set_id(stateListArray.getJSONObject(index).getString("_id"));
                                stateBean.setState_name(stateListArray.getJSONObject(index).getString("country_name"));
                                countryBeanList.add(stateBean);
                            }
                        }
                        //loader.dismissLoader();
//                        ArrayAdapter stateAdapter = new ArrayAdapter(getActivity(), R.layout.spinner_item,countryList);
//                        stateAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
//                        spinnerCountry.setAdapter(stateAdapter);
//
//                        if(!CompleteProfileData.country.equals(""))
//                        {
//                            spinnerCountry.setSelection(countryList.contains(CompleteProfileData.country)?countryList.indexOf(CompleteProfileData.country):0);
//                        }


                    } else if(sStatus.equals("00")){
                        customAlert.singleLoginAlertLogout();
                    } else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    }else {

                       // loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    //loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
//                AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
            }
        });
    }
    private void getCountryStateList(String id) {
        if(showLoader)
            loader.showLoader();
        countryStateList.clear();
        countryStateBeanList.clear();
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String,String> params = new HashMap<>();
        params.put("country_id",id);

        mRequest.makeServiceRequest(IConstant.getCountrySateList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------getCountrySateList Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1") && message.equalsIgnoreCase("Success")) {

                        countryStateList.add("STATE");
                        StateBean stateBean1 = new StateBean();
                        stateBean1.set_id("-1");
                        stateBean1.setState_name("STATE");
                        countryStateBeanList.add(stateBean1);
                        JSONArray stateListArray = object.getJSONArray("list");
                        if(stateListArray.length()>0)
                        {
                            for(int index =0; index<stateListArray.length();index++)
                            {
                                countryStateList.add(stateListArray.getJSONObject(index).getString("state_name"));
                                StateBean stateBean = new StateBean();
                                stateBean.set_id(stateListArray.getJSONObject(index).getString("_id"));
                                stateBean.setState_name(stateListArray.getJSONObject(index).getString("state_name"));
                                countryStateBeanList.add(stateBean);
                            }
                        }


                        //loader.dismissLoader();
//                        ArrayAdapter stateAdapter = new ArrayAdapter(getActivity(), R.layout.spinner_item,countryStateList);
//                        stateAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
//                        spinnerState.setAdapter(stateAdapter);
//
//                        if(!CompleteProfileData.state1.equals(""))
//                        {
//                            spinnerState.setSelection(countryStateList.contains(CompleteProfileData.state1)?countryStateList.indexOf(CompleteProfileData.state1):0);
//                        }

                    } else if(sStatus.equals("00")){
                        customAlert.singleLoginAlertLogout();
                    } else {

                        // loader.dismissLoader();

                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.showLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.showLoader();
//                AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void getCityList(String id) {

        cityBeanList.clear();
        cityList.clear();
        if(showLoader)
            loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String,String> params = new HashMap<>();
        params.put("state_id",id);

        mRequest.makeServiceRequest(IConstant.getCityList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------getCityList Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1") && message.equalsIgnoreCase("Success")) {

                        cityList.add("CITY");
                        StateBean stateBean1 = new StateBean();
                        stateBean1.set_id("-1");
                        stateBean1.setState_name("CITY");
                        cityBeanList.add(stateBean1);
                        JSONArray stateListArray = object.getJSONArray("list");
                        if(stateListArray.length()>0)
                        {
                            for(int index =0; index<stateListArray.length();index++)
                            {
                                cityList.add(stateListArray.getJSONObject(index).getString("city_name"));
                                StateBean stateBean = new StateBean();
                                stateBean.set_id(stateListArray.getJSONObject(index).getString("_id"));
                                stateBean.setState_name(stateListArray.getJSONObject(index).getString("city_name"));
                                cityBeanList.add(stateBean);
                            }
                        }
                        //loader.dismissLoader();
//                        ArrayAdapter stateAdapter = new ArrayAdapter(getActivity(), R.layout.spinner_item,cityList);
//                        stateAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
//                        spinnerCity.setAdapter(stateAdapter);
//
//                        if(!CompleteProfileData.city.equals(""))
//                        {
//                            spinnerCity.setSelection(cityList.contains(CompleteProfileData.city)?cityList.indexOf(CompleteProfileData.city):0);
//                        }

                    } else if(sStatus.equals("00")){
                        customAlert.singleLoginAlertLogout();
                    }  else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    }else {

                        // loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
//                AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
            }
        });
    }
    private void getStateList()
    {
        stateList.clear();
        stateBeanList.clear();
        //loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String,String> params = new HashMap<>();

        mRequest.makeServiceRequest(IConstant.getStateList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------getStateList Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1") && message.equalsIgnoreCase("Success")) {

                        stateList.add("STATE");
                        StateBean stateBean1 = new StateBean();
                        stateBean1.set_id("-1");
                        stateBean1.setState_name("STATE");
                        stateBeanList.add(stateBean1);
                        JSONArray stateListArray = object.getJSONArray("list");
                        if(stateListArray.length()>0)
                        {
                            for(int index =0; index<stateListArray.length();index++)
                            {
                                stateList.add(stateListArray.getJSONObject(index).getString("state_name"));
                                StateBean stateBean = new StateBean();
                                stateBean.set_id(stateListArray.getJSONObject(index).getString("_id"));
                                stateBean.setState_name(stateListArray.getJSONObject(index).getString("state_name"));
                                stateBeanList.add(stateBean);
                            }
                        }

//                        ArrayAdapter stateEduAdapter = new ArrayAdapter(getActivity(), R.layout.spinner_item,stateList);
//                        stateEduAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
//                        spinnerStateEducation.setAdapter(stateEduAdapter);
//
//                        if(!CompleteProfileData.state2.equals(""))
//                        {
//                            spinnerStateEducation.setSelection(stateList.contains(CompleteProfileData.state2)?stateList.indexOf(CompleteProfileData.state2):0);
//                        }

                    }else if(sStatus.equals("00")){
                        customAlert.singleLoginAlertLogout();
                    }  else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    }else {

                       // loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    //loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
//                AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void getCollegeList(String id)
    {
        collegeList.clear();
        collegeBeanList.clear();
        if(showLoader)
            loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String,String> params = new HashMap<>();
        params.put("state_id",id);

        mRequest.makeServiceRequest(IConstant.getCollegeList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------getCollegeList Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1") && message.equalsIgnoreCase("Success")) {
                        collegeList.add("COLLEGE");
                        CollegeBean collegeBean1 = new CollegeBean();
                        collegeBean1.set_id("-1");
                        collegeBean1.setCollege_name("COLLEGE");
//                                collegeBean.setShortName(stateListArray.getJSONObject(index).getString("short_name"));
//                                collegeBean.setLongName(stateListArray.getJSONObject(index).getString("long_name"));
                        collegeBean1.setState_id("state_id");
                        collegeBeanList.add(collegeBean1);
                        JSONArray stateListArray = object.getJSONArray("list");
                        if(stateListArray.length()>0)
                        {

                            for(int index =0; index<stateListArray.length();index++)
                            {
                                collegeList.add(stateListArray.getJSONObject(index).getString("college_name"));
                                CollegeBean collegeBean = new CollegeBean();
                                collegeBean.set_id(stateListArray.getJSONObject(index).getString("_id"));
                                collegeBean.setCollege_name(stateListArray.getJSONObject(index).getString("college_name"));
//                                collegeBean.setShortName(stateListArray.getJSONObject(index).getString("short_name"));
//                                collegeBean.setLongName(stateListArray.getJSONObject(index).getString("long_name"));
                                collegeBean.setState_id(stateListArray.getJSONObject(index).getString("state_id"));
                                collegeBeanList.add(collegeBean);
                            }
                        }
                        loader.dismissLoader();
//                        ArrayAdapter collegeAdapter = new ArrayAdapter(getActivity(), R.layout.spinner_item,collegeList);
//                        collegeAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
//                        spinnerCollege.setAdapter(collegeAdapter);
//                        if(!CompleteProfileData.college.equals(""))
//                        {
//                            spinnerCollege.setSelection(collegeList.contains(CompleteProfileData.college)?collegeList.indexOf(CompleteProfileData.college):0);
//                        }
                    }  else if(sStatus.equals("00")){
                        loader.dismissLoader();
                        customAlert.singleLoginAlertLogout();
                    } else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    } else {

                        loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
            }
        });
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_complete_profile, container, false);
        if(getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(getActivity()) == 0 || DisplayOrientation.getDisplayOrientation(getActivity()) == 2))
            rootView = inflater.inflate(R.layout.fragment_complete_profile_protrait, container, false);
        else if(getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(getActivity()) == 1 || DisplayOrientation.getDisplayOrientation(getActivity()) == 3))
            rootView = inflater.inflate(R.layout.fragment_complete_profile, container, false);
        else if(!getResources().getBoolean(R.bool.isTablet))
            rootView = inflater.inflate(R.layout.fragment_complete_profile, container, false);
        return  rootView;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle != null && bundle.containsKey("sideMenu")) {
            final TabSideMenu tabSideMenu = (TabSideMenu) bundle.getSerializable("sideMenu");
        }
        TextView txtCompleteProfile =  view.findViewById(R.id.txtCompleteProfile);
        txtMail =  view.findViewById(R.id.txtMail);
        txtMobileNumber = view.findViewById(R.id.txtMobileNumber);
        etFirstName = view.findViewById(R.id.etFirstName);
        etLastName = view.findViewById(R.id.etLastName);
        etAddress1 = view.findViewById(R.id.etAddress1);
        etAddress2 = view.findViewById(R.id.etAddress2);
        etPincode = view.findViewById(R.id.etPincode);
        spinnerCountry = view.findViewById(R.id.spinnerCountry);
        spinnerCity = view.findViewById(R.id.spinnerCity);
        spinnerState = view.findViewById(R.id.spinnerState);
        spinnerStateEducation = view.findViewById(R.id.spinnerStateEducation);
        spinnerCollege = view.findViewById(R.id.spinnerCollege);
        txtResetCompleteProfile = view.findViewById(R.id.txtResetCompleteProfile);
        rlCompleteProfileSubmit = view.findViewById(R.id.rlCompleteProfileSubmit);
        txtCountryHint = view.findViewById(R.id.txtCountryHint);
        txtStateHint = view.findViewById(R.id.txtStateHint);
        txtCityHint = view.findViewById(R.id.txtCityHint);
        txtEduStateHint = view.findViewById(R.id.txtEduStateHint);
        txtEduCollegeHint = view.findViewById(R.id.txtEduCollegeHint);
        rlCountryCompleteProfile = view.findViewById(R.id.rlCountryCompleteProfile);
        rlStateCompleteProfile = view.findViewById(R.id.rlStateCompleteProfile);
        rlCityCompleteProfile = view.findViewById(R.id.rlCityCompleteProfile);
        rlEduStateCompleteProfile = view.findViewById(R.id.rlEduStateCompleteProfile);
        rlEduCollegeCompleteProfile = view.findViewById(R.id.rlEduCollegeCompleteProfile);


//        rlCountrySpinner = view.findViewById(R.id.rlCountrySpinner);
//        rlCountryStateSpinner = view.findViewById(R.id.rlCountryStateSpinner);
//        rlCitySpinner = view.findViewById(R.id.rlCitySpinner);
//        rlEduStateSpinner = view.findViewById(R.id.rlEduStateSpinner);
//        rlCollegeSpinner = view.findViewById(R.id.rlCollegeSpinner);



        setEnableDisableSubmit();
        prepareSpinners();


    }

    private void setNameandmobile() {
        txtMail.setText(sessionManager.getUserDetails().get(SessionManager.KEY_EMAIL));
        txtMobileNumber.setText(sessionManager.getUserDetails().get(SessionManager.KEY_MOBILE));
        if(getResources().getBoolean(R.bool.isTablet)) {
            if(CompleteProfileData.countryId!= null && !CompleteProfileData.countryId.equals(""))
            {

                if(cd.isConnectingToInternet()) {
                    getCountryStateList(CompleteProfileData.countryId);
                }else{
                    startActivity(new Intent(getContext(), NoInterNetActivity.class));
                    DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                }
            }

            if(CompleteProfileData.state1Id!= null && !CompleteProfileData.state1Id.equals(""))
            {
                if(cd.isConnectingToInternet()) {
                    getCityList(CompleteProfileData.state1Id);
                }else{
                    startActivity(new Intent(getContext(), NoInterNetActivity.class));
                    DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                }
            }
            if(CompleteProfileData.state2Id!= null && !CompleteProfileData.state2Id.equals(""))
            {
                if(cd.isConnectingToInternet()) {
                    getCollegeList(CompleteProfileData.state2Id);
                }else{
                    startActivity(new Intent(getContext(), NoInterNetActivity.class));
                    DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                }

            }
            etFirstName.setText(CompleteProfileData.firstName);
            etLastName.setText(CompleteProfileData.lastName);
            etAddress1.setText(CompleteProfileData.address1);
            etAddress2.setText(CompleteProfileData.address2);
            etPincode.setText(CompleteProfileData.pinCode);
            txtCountryHint.setTextColor(CompleteProfileData.country.equalsIgnoreCase("Country")?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
            txtStateHint.setTextColor(CompleteProfileData.state1.equalsIgnoreCase("State")?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
            txtCityHint.setTextColor(CompleteProfileData.city.equalsIgnoreCase("City")?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
            txtEduStateHint.setTextColor(CompleteProfileData.state2.equalsIgnoreCase("State")?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
            txtEduCollegeHint.setTextColor(CompleteProfileData.college.equalsIgnoreCase("College")?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
            txtEduCollegeHint.setText(CompleteProfileData.college);
            txtEduStateHint.setText(CompleteProfileData.state2);
            txtStateHint.setText(CompleteProfileData.state1);
            txtCityHint.setText(CompleteProfileData.city);
            txtCountryHint.setText(CompleteProfileData.country);
        }
    }


    private void setListeners() {
        rlCompleteProfileSubmit.setOnClickListener(this);
        txtResetCompleteProfile.setOnClickListener(this);



        txtCountryHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCountry.setShowKeyboard(false);
                dialogCountry.showSpinerDialog();
            }
        });
        rlCountryCompleteProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCountry.setShowKeyboard(false);
                dialogCountry.showSpinerDialog();
            }
        });

        dialogCountry.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {

                CompleteProfileData.country = item;
                CompleteProfileData.countryId = countryBeanList.get(position).get_id();
                txtCountryHint.setText(item);
                setEnableDisableSubmit();
                txtCountryHint.setTextColor(position==0?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
                showLoader = true;
                if(position != 0){
                    if(cd.isConnectingToInternet()) {
                        getCountryStateList(countryBeanList.get(position).get_id());
                    }else{
                        startActivity(new Intent(getContext(), NoInterNetActivity.class));
                        DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                    }
                }
                else
                {
                    countryStateList.clear();
                    countryStateBeanList.clear();

                }
                txtStateHint.setText("STATE");
                txtStateHint.setTextColor(getResources().getColor(R.color.hint_color));
                txtCityHint.setText("CITY");
                txtCityHint.setTextColor(getResources().getColor(R.color.hint_color));

            }
        });

        txtEduCollegeHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtEduStateHint.getText().toString().equalsIgnoreCase("State"))
                {
                    AlertBox.showSnackBox(getActivity(),getString(R.string.alert_oops),getString(R.string.collegeStateSelectAlert));
                }else {
                    dialogEduCollege.setShowKeyboard(false);
                    dialogEduCollege.showSpinerDialog();
                }
            }
        });
        rlEduCollegeCompleteProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtEduStateHint.getText().toString().equalsIgnoreCase("State"))
                {
                    AlertBox.showSnackBox(getActivity(),getString(R.string.alert_oops),getString(R.string.collegeStateSelectAlert));
                }else {
                    dialogEduCollege.setShowKeyboard(false);
                    dialogEduCollege.showSpinerDialog();
                }
            }
        });

        dialogEduCollege.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                CompleteProfileData.college = item;
                txtEduCollegeHint.setText(item);
                setEnableDisableSubmit();
                txtEduCollegeHint.setTextColor(position==0?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));

            }
        });

        txtStateHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtCountryHint.getText().toString().equalsIgnoreCase("Country"))
                {
                    AlertBox.showSnackBox(getActivity(),getString(R.string.alert_oops),getString(R.string.countrySelectAlert));
                }else {
                    dialogState.setShowKeyboard(false);
                    dialogState.showSpinerDialog();
                }

            }
        });
        rlStateCompleteProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtCountryHint.getText().toString().equalsIgnoreCase("Country"))
                {
                    AlertBox.showSnackBox(getActivity(),getString(R.string.alert_oops),getString(R.string.countrySelectAlert));
                }else {
                    dialogState.setShowKeyboard(false);
                    dialogState.showSpinerDialog();
                }

            }
        });

        dialogState.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                CompleteProfileData.state1 = item;
                CompleteProfileData.state1Id = countryStateBeanList.get(position).get_id();
                txtStateHint.setText(item);
                setEnableDisableSubmit();
                txtStateHint.setTextColor(position==0?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
                showLoader = true;
                if(position != 0)
                {
                    if(cd.isConnectingToInternet()) {
                        getCityList(countryStateBeanList.get(position).get_id());
                    }else{
                        startActivity(new Intent(getContext(), NoInterNetActivity.class));
                        DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                    }
                }
                else
                {
                    cityList.clear();
                    cityBeanList.clear();

                }
                txtCityHint.setText("CITY");
                txtCityHint.setTextColor(getResources().getColor(R.color.hint_color));
            }
        });

        txtCityHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtStateHint.getText().toString().equalsIgnoreCase("state"))
                {
                    AlertBox.showSnackBox(getActivity(),getString(R.string.alert_oops),getString(R.string.stateSelectAlert));
                }else {
                    dialogCity.setShowKeyboard(false);
                    dialogCity.showSpinerDialog();
                }

            }
        });

        rlCityCompleteProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtStateHint.getText().toString().equalsIgnoreCase("state"))
                {
                    AlertBox.showSnackBox(getActivity(),getString(R.string.alert_oops),getString(R.string.stateSelectAlert));
                }else {
                    dialogCity.setShowKeyboard(false);
                    dialogCity.showSpinerDialog();
                }

            }
        });

        dialogCity.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                CompleteProfileData.city = item;
                CompleteProfileData.cityId = cityBeanList.get(position).get_id();
                txtCityHint.setText(item);
                setEnableDisableSubmit();
                txtCityHint.setTextColor(position==0?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));

            }
        });

        txtEduStateHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogEduState.setShowKeyboard(false);
                dialogEduState.showSpinerDialog();
            }
        });
        rlEduStateCompleteProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogEduState.setShowKeyboard(false);
                dialogEduState.showSpinerDialog();
            }
        });

        dialogEduState.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                CompleteProfileData.state2 = item;
                CompleteProfileData.state2Id = stateBeanList.get(position).get_id();
                txtEduStateHint.setText(item);
                setEnableDisableSubmit();
                txtEduStateHint.setTextColor(position==0?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
                showLoader = true;
                if(position != 0)
                {
                    if(cd.isConnectingToInternet()) {
                        getCollegeList(stateBeanList.get(position).get_id());
                    }else{
                        startActivity(new Intent(getContext(), NoInterNetActivity.class));
                        DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                    }
                }
                else{
                    collegeList.clear();
                    collegeBeanList.clear();

                }
                txtEduCollegeHint.setText("COLLEGE");
                txtEduCollegeHint.setTextColor(getResources().getColor(R.color.hint_color));

            }
        });

        etAddress1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setEnableDisableSubmit();
                if(!s.toString().isEmpty())
                {
                    CompleteProfileData.address1 = s.toString();

                }
            }
        });
        etAddress2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setEnableDisableSubmit();
                if(!s.toString().isEmpty())
                {
                    CompleteProfileData.address2 = s.toString();

                }
            }
        });
        etFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setEnableDisableSubmit();
                if(!s.toString().isEmpty())
                {
                    CompleteProfileData.firstName = s.toString();

                }
            }
        });
        etLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setEnableDisableSubmit();
                if(!s.toString().isEmpty())
                {
                    CompleteProfileData.lastName = s.toString();

                }
            }
        });
        etPincode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setEnableDisableSubmit();
                if(!s.toString().isEmpty())
                {
                    CompleteProfileData.pinCode = s.toString();
                    if(s.toString().length()==6)
                        StatusBarColorChange.hideSoftKeyboard(getActivity());
                }

            }
        });

        spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CompleteProfileData.country = countryList.get(position);
                CompleteProfileData.countryId = countryBeanList.get(position).get_id();
                txtCountryHint.setText(spinnerCountry.getSelectedItem().toString());
                txtCountryHint.setTextColor(position==0?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
                if(position != 0)
                    getCountryStateList(countryBeanList.get(position).get_id());
                else
                {
                    countryStateList.clear();
                    countryStateBeanList.clear();
                    spinnerState.setSelection(0);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CompleteProfileData.state1 = countryStateList.get(position);
                CompleteProfileData.state1Id = countryStateBeanList.get(position).get_id();
                txtStateHint.setText(spinnerState.getSelectedItem().toString());
                txtStateHint.setTextColor(position==0?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
                if(position != 0)
                    getCityList(countryStateBeanList.get(position).get_id());
                else
                {
                    cityList.clear();
                    cityBeanList.clear();
                    spinnerCity.setSelection(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerStateEducation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CompleteProfileData.state2 = stateList.get(position);
                CompleteProfileData.state2Id = stateBeanList.get(position).get_id();
                if(position != 0)
                    getCollegeList(stateBeanList.get(position).get_id());
                else{
                    stateBeanList.clear();
                    stateList.clear();
                    spinnerCollege.setSelection(0);
                }
                txtEduStateHint.setText(spinnerStateEducation.getSelectedItem().toString());
                txtEduStateHint.setTextColor(position==0?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CompleteProfileData.city = cityList.get(position);
                txtCityHint.setText(spinnerCity.getSelectedItem().toString());
                txtCityHint.setTextColor(position==0?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerCollege.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CompleteProfileData.college = collegeList.get(position);
                txtEduCollegeHint.setText(spinnerCollege.getSelectedItem().toString());
                txtEduCollegeHint.setTextColor(position==0?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }




    private void completeProfile() {

        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String,String> params = new HashMap<>();
        params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("first_name",etFirstName.getText().toString());
        params.put("last_name",etLastName.getText().toString());
        params.put("line_1",etAddress1.getText().toString());
        params.put("line_2",etAddress2.getText().toString());
        params.put("city",txtCityHint.getText().toString());
        params.put("city_id",CompleteProfileData.cityId);
        params.put("state",txtStateHint.getText().toString());
        params.put("state_id",CompleteProfileData.state1Id);
        params.put("country",txtCountryHint.getText().toString());
        params.put("country_id",CompleteProfileData.countryId);
        params.put("pincode",etPincode.getText().toString());
        params.put("edu_state",stateBeanList.get(stateList.indexOf(txtEduStateHint.getText().toString())).get_id());
        params.put("college",collegeBeanList.get(collegeList.indexOf(txtEduCollegeHint.getText().toString())).get_id());

        mRequest.makeServiceRequest(IConstant.completeProfile, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------completeProfile Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1")) {
                        CompleteProfileData.clearData();
                        loader.dismissLoader();
                        sessionManager.updateName(etFirstName.getText().toString(),etLastName.getText().toString());
                        customAlert.showAlertSuccessCompleteProfile(getString(R.string.alert_success),message,getResources().getBoolean(R.bool.isTablet)?"":"MobilePAges");
                    } else if(sStatus.equals("00")){
                        loader.dismissLoader();
                        customAlert.singleLoginAlertLogout();
                    } else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    }else {

                        loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
            }
        });


    }


    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.txtResetCompleteProfile:
                resetFields();
                break;


            case R.id.rlCompleteProfileSubmit:
                if(etFirstName.getText().toString().isEmpty())
                    AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),getString(R.string.firstNameAlert));
                else if(etLastName.getText().toString().isEmpty())
                    AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),getString(R.string.lastNameAlert));
                else if(etAddress1.getText().toString().isEmpty())
                    AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),getString(R.string.address1Alert));
                else if(etAddress2.getText().toString().isEmpty())
                    AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),getString(R.string.address1Alert));
                else if(countryList.size() == 0 || txtCountryHint.getText().toString().equalsIgnoreCase("Country"))
                    AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),getString(R.string.countrySelectAlert));
                else if(countryStateList.size() == 0 || txtStateHint.getText().toString().equalsIgnoreCase("State"))
                    AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),getString(R.string.stateSelectAlert));
                else if(cityList.size() == 0 || txtCityHint.getText().toString().equalsIgnoreCase("City"))
                    AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),getString(R.string.citySelectAlert));
                else if(etPincode.getText().toString().isEmpty())
                    AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),getString(R.string.pinAlert));
                else if(etPincode.getText().toString().length()<4)
                    AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),getString(R.string.ValidPinAlert));
                else if(stateList.size() == 0 || txtEduStateHint.getText().toString().equalsIgnoreCase("State"))
                    AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),"Please select your educated state.");
                else if(collegeList.size() == 0 || txtEduCollegeHint.getText().toString().equalsIgnoreCase("College"))
                    AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),"Please select your college.");
                else
                {
                    if(cd.isConnectingToInternet()) {
                        completeProfile();
                    }else{
                        startActivity(new Intent(getContext(), NoInterNetActivity.class));
                        DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                    }
                }

                break;
        }
    }

    private void resetFields() {

        etLastName.setText("");
        etFirstName.setText("");
        etAddress1.setText("");
        etAddress2.setText("");
        etPincode.setText("");
        spinnerCountry.setSelection(0);
        spinnerState.setSelection(0);
        spinnerCity.setSelection(0);
        spinnerStateEducation.setSelection(0);
        spinnerCollege.setSelection(0);
        CompleteProfileData.clearData();
        txtCountryHint.setText(CompleteProfileData.country);
        txtStateHint.setText(CompleteProfileData.state1);
        txtCityHint.setText(CompleteProfileData.city);
        txtEduStateHint.setText(CompleteProfileData.state2);
        txtEduCollegeHint.setText(CompleteProfileData.college);
        txtCountryHint.setTextColor(getResources().getColor(R.color.hint_color));
        txtStateHint.setTextColor(getResources().getColor(R.color.hint_color));
        txtCityHint.setTextColor(getResources().getColor(R.color.hint_color));
        txtEduStateHint.setTextColor(getResources().getColor(R.color.hint_color));
        txtEduCollegeHint.setTextColor(getResources().getColor(R.color.hint_color));

    }
    private void setEnableDisableSubmit()
    {
        if(!etFirstName.getText().toString().isEmpty() && !etLastName.getText().toString().isEmpty() && !etAddress1.getText().toString().isEmpty() && !etAddress2.getText().toString().isEmpty() && !etPincode.getText().toString().isEmpty() && !txtCountryHint.getText().toString().equalsIgnoreCase("Country") && !txtStateHint.getText().toString().equalsIgnoreCase("State") && !txtCityHint.getText().toString().equalsIgnoreCase("City") && !txtEduStateHint.getText().toString().equalsIgnoreCase("State") && !txtEduCollegeHint.getText().toString().equalsIgnoreCase("College")&& etPincode.getText().toString().length()>3 )
        {
            rlCompleteProfileSubmit.setBackground(getResources().getDrawable(R.drawable.login_gradient));
        }else
        {
            rlCompleteProfileSubmit.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
        }

    }
}
