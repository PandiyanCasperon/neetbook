package com.app.neetbook.View.SideMenu;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.app.TabPojo.EventbusPojo;
import com.app.neetbook.Adapter.HomeSearchAdapter;
import com.app.neetbook.Adapter.NewTestSectionAdapter;
import com.app.neetbook.Adapter.TestSectionAdapter;
import com.app.neetbook.Adapter.sideMenuContent.SubscriptionCategoryTestAdapter;
import com.app.neetbook.Interfaces.APMTSearchListioner;
import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.Interfaces.OnHeadingClickListener;
import com.app.neetbook.Interfaces.OnSearchItemClickListener;
import com.app.neetbook.Interfaces.TabArtcileChapterClickListener;
import com.app.neetbook.Interfaces.TestSection;
import com.app.neetbook.Model.EventBusTestRefreshPojo;
import com.app.neetbook.Model.SearchBean;
import com.app.neetbook.Model.TestChildModel;
import com.app.neetbook.Model.TestHeadetModel;
import com.app.neetbook.Model.TestList;
import com.app.neetbook.Model.sidemenuFromContent.CategoryTest;
import com.app.neetbook.R;
import com.app.neetbook.TAbAdapter.TabTestChildAdapter;
import com.app.neetbook.TAbAdapter.TabTestmainAdapter;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.ItemOffsetDecoration;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.SmoothScroll.LinearLayoutManagerWithSmoothScroller;
import com.app.neetbook.View.ActivitySubscriptionListDialog;
import com.app.neetbook.View.CustomDialogActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.shuhart.stickyheader.StickyHeaderItemDecorator;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class TestFragment extends Fragment implements ItemClickListener, OnHeadingClickListener, APMTSearchListioner, View.OnClickListener, TestSectionAdapter.SetOnClickInterface, NewTestSectionAdapter.SetOnChildClickListener {
    /////////////////////
    private NewTestSectionAdapter newTestSectionAdapter;
    private ActivitySubscriptionListDialog activitySubscriptionListDialog;

    ///////////////////

    private ArrayList<CategoryTest> mList = new ArrayList<>();
    private ArrayList<TestSection> sectionHeaderTestListList = new ArrayList<>();
    private SubscriptionCategoryTestAdapter mAdapter;
    private TestSectionAdapter testSectionAdapter;
    private RecyclerView mRecyclerView;
    private Context context;
    Loader loader;
    // Dialog progressDialog;
    SessionManager sessionManager;
    private CustomAlert customAlert;
    private int page = 1;
    int loadMoreEnable = 0;
    Dialog alertDialog;
    Dialog progressDialog;
    private int totalSubsCount = 0;
    String strSubjectId = "", strSubjectName = "", strSubjectShortName = "", strisTrial = "";
    //  private TextView textView11;
//  ImageView imageView7;
    ItemOffsetDecoration itemDecoration;
    SwipeRefreshLayout mSwipeRefreshLayout;

    //  TextView textView12,textView14,textView13,textView15;
    ImageView imageView8, imageView10;

    LinearSnapHelper snapHelper;
    Typeface tfBold, tfMedium, tfRegular;
    LinearLayoutManager linearLayoutManager;
    private int indexCount = 0;
    private int index = 0;
    private ImageView imageView14;
    private ConstraintLayout parentView;
    int currentSelectedPosition = 0;
    int firstVisibleInListview = 0;
    int actualPosition = 0;
    int fakePosition = 0;
    int childPosition = 0;
    /*TabView*/
    ArrayList<CategoryTest> resultList;
    private RecyclerView fragment_child_recyclerview;

    /*tabVersion*/
    TabArtcileChapterClickListener tabArtcileChapterClickListener;
    TabTestmainAdapter tabTestmainAdapter;
    private TabTestChildAdapter tabTestChildAdapter;
    private Boolean item_is_selected = false;
    private TextView txtFt, tvCategoryTitle, tvCategoryFullTestTitle, textView17;
    private String strHeadId = "", strChapterId = "";
    private int currentChapterSelectedPosition = -1, currentHeadSelectedPosition = -1, currentSuperHeadSelectedPosition = -1;
    ConstraintLayout llConstrainSearch;
    EditText etSearch;
    private ImageView imgSearchClose, imgSearchGo;
    private HomeSearchAdapter homeSearchAdapter;
    private RecyclerView homeSearchRecyclerView;
    private int totalCountSearch = 0;
    private ArrayList<SearchBean> searchBeanArrayList = new ArrayList<>();
    private String strPending = "-1";
    private String strPendingTestId = "";

    public TestFragment(String subjectId, String subjectName, String strisTrial, String strSubjectShortName, String strHeadId, String strChapterId) {
        this.strSubjectId = subjectId;
        this.strSubjectName = subjectName;
        this.strSubjectShortName = strSubjectShortName;
        this.strisTrial = strisTrial;
        this.strHeadId = strHeadId;
        this.strChapterId = strChapterId;
    }
    public TestFragment(){

    }

    private void CreateLoader() {
        progressDialog = new Dialog(getActivity());
        progressDialog.setContentView(R.layout.custom_progress);
        if (progressDialog.getWindow() != null)
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // startAnimation();
        progressDialog.setCancelable(false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupStatusBarColor();
        EventBus.getDefault().register(this);
        linearLayoutManager = new LinearLayoutManager(getActivity(),
                RecyclerView.VERTICAL, false);
        sessionManager = new SessionManager(getActivity());
        customAlert = new CustomAlert(getActivity());

        alertDialog = new Dialog(getActivity());
        createSnap();
        tfBold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaRegular.otf");
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_test, container, false);
      //  CreateLoader();
        if (getActivity().getResources().getBoolean(R.bool.isTablet))
            initTab(root);
        else
            init(root);
        return root;
    }

    private void initTab(View root) {
        mRecyclerView = root.findViewById(R.id.parent_recyclerview);

        // mSwipeRefreshLayout = root.findViewById(R.id.mSwipeRefreshLayout);


        fragment_child_recyclerview = root.findViewById(R.id.fragment_child_recyclerview);


        page = 1;
        populateList();
        //new GetTestIndex().execute();
        //swipeRefresh();

        /*LoadMoreListener();*/
        //onScrollRecyclerview();

    }

    private void init(View rootView) {
        mRecyclerView = rootView.findViewById(R.id.recyclerViewTest);

        imageView8 = rootView.findViewById(R.id.imageView8);
        tvCategoryFullTestTitle = rootView.findViewById(R.id.tvCategoryFullTestTitle);
        tvCategoryTitle = rootView.findViewById(R.id.tvCategoryTitle);
        textView17 = rootView.findViewById(R.id.textView17);
        imageView14 = rootView.findViewById(R.id.imageView14);
        txtFt = rootView.findViewById(R.id.txtFt);
        parentView = rootView.findViewById(R.id.parentView);

        loader = new Loader(getActivity());
        mSwipeRefreshLayout = rootView.findViewById(R.id.mSwipeRefreshLayout);
        mSwipeRefreshLayout.setEnabled(false);
        mSwipeRefreshLayout.setRefreshing(false);

        imageView8 = rootView.findViewById(R.id.imageView8);
        llConstrainSearch = rootView.findViewById(R.id.llConstrainSearch);
        etSearch = rootView.findViewById(R.id.etSearch);
        imgSearchClose = rootView.findViewById(R.id.imgSearchClose);
        imgSearchGo = rootView.findViewById(R.id.imgSearchGo);


        imgSearchClose.setOnClickListener(this);
        imgSearchGo.setOnClickListener(this);
        itemDecoration = new ItemOffsetDecoration(context, R.dimen._4sdp);
        imageView8.setOnClickListener(this);


        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!etSearch.getText().toString().isEmpty() && etSearch.getText().toString().length() > 3) {
                    imgSearchClose.setVisibility(View.GONE);
                    imgSearchGo.setVisibility(View.VISIBLE);
                }
            }
        });

        page = 1;
        populateList();
        swipeRefresh();
        imageView8.setVisibility(View.VISIBLE);

      //  CreateLoader();
        LoadMoreListener();
    }


    private void swipeRefresh() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                loadMoreEnable = 0;
                mList = new ArrayList<>();
                sectionHeaderTestListList = new ArrayList<>();
                populateList();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void populateList() {
        mList.clear();

        loadMoreEnable = 0;
        indexCount = 0;
        index = 0;
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("subject_id", strSubjectId);
        params.put("page", "" + page);
        params.put("skip", "0");
        params.put("limit", "10");
        mRequest.makeServiceRequest(IConstant.testIndex, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                sectionHeaderTestListList.clear();
                System.out.println("------------testIndex Response----------------" + response);
                String sStatus = "";
                String message = "";

                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalSubsCount = object.getInt("ctotal");
                        JSONArray jsonArray = object.getJSONArray("chapters");
                        JSONArray ftestsArray = object.getJSONArray("ftests");
                        Log.e("ftestarray", ftestsArray.toString());
                        if (ftestsArray.length() > 0) {
                            //llEmpty.setVisibility(View.GONE);
                            for (int i = 0; i < ftestsArray.length(); i++) {
                                indexCount = indexCount + i;
                                CategoryTest listItem = new CategoryTest();
                                JSONObject jsonObject = ftestsArray.getJSONObject(i);

                                listItem._id = jsonObject.getString("_id");
                                listItem.long_name = jsonObject.getString("long_name");
                                listItem.short_name = jsonObject.getString("short_name");
                                //listItem.images = jsonObject.getString("img");
                                listItem.setIsFullTest("Yes");

                                TestHeadetModel testHeadetModel = new TestHeadetModel(indexCount);
                                testHeadetModel.setChapterId(jsonObject.getString("_id"));
                                testHeadetModel.setShortName(jsonObject.getString("short_name"));
                                testHeadetModel.setLongName(jsonObject.getString("long_name"));
                                testHeadetModel.setChapterName(jsonObject.getString("fulltest_name"));
                                testHeadetModel.setFullTestCount(String.valueOf(i + 1));
                                testHeadetModel.setIsFullTest("Yes");
                                testHeadetModel.setIsFinished("No");

                                listItem.setFulltest_name(jsonObject.getString("fulltest_name"));
                                ArrayList<TestList> testListArrayList = new ArrayList<>();
                                TestList testList = new TestList();
                                JSONObject testObject = ftestsArray.getJSONObject(i);
                                testList._id = testObject.getString("_id");
                                testList.crt_ans_mark = testObject.getString("crt_ans_mark");
                                testList.qstn_type = testObject.getString("qstn_type");
                                testList.wrng_ans_mark = testObject.getString("wrng_ans_mark");
                                testList.test_name = testObject.getString("fulltest_name");
                                testList.long_name = testObject.getString("long_name");
                                testList.short_name = testObject.getString("short_name");
                                testList.no_of_qstn = testObject.getString("no_of_qstn");
                                testList.no_of_choice = testObject.getString("no_of_choice");
                                testList.instruction = testObject.getString("instruction");
                                testList.type = testObject.getString("type");
             /*   if(strPending.equals("-1"))
                  strPending = jsonObject.getString("pending");*/
                                testList.is_finished = "No";
                                testListArrayList.add(testList);
                                listItem.testArrayList = testListArrayList;

                                testHeadetModel.setTestArrayList(testListArrayList);
                                sectionHeaderTestListList.add(testHeadetModel);
                                mList.add(listItem);

                            }
                        }
                        if (jsonArray.length() > 0) {
                            //llEmpty.setVisibility(View.GONE);

                            for (int i = 0; i < jsonArray.length(); i++) {
                                index = index + 1;
                                indexCount = indexCount + 1;
                                CategoryTest listItem = new CategoryTest();
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                listItem._id = jsonObject.getString("_id");
                                listItem.chapt_name = jsonObject.getString("chapt_name");
                                listItem.long_name = jsonObject.getString("long_name");
                                listItem.short_name = jsonObject.getString("short_name");
                                listItem.images = jsonObject.getString("img");
                                listItem.setChapterCount(String.valueOf(i + 1));

                                listItem.setIsFullTest("No");

                                TestHeadetModel testHeadetModel = new TestHeadetModel(indexCount);
                                testHeadetModel.setChapterId(jsonObject.getString("_id"));
                                testHeadetModel.setChapterName(jsonObject.getString("chapt_name"));
                                testHeadetModel.setShortName(jsonObject.getString("short_name"));
                                testHeadetModel.setLongName(jsonObject.getString("long_name"));
                                testHeadetModel.setImage(jsonObject.getString("img"));
                                testHeadetModel.setIsFullTest("No");
                                testHeadetModel.setIsFinished("No");
                                testHeadetModel.setCount(String.valueOf(index));
                                sectionHeaderTestListList.add(testHeadetModel);
                                if (jsonObject.getString("_id").equalsIgnoreCase(strChapterId))
                                    currentChapterSelectedPosition = indexCount;
                                TestChildModel testChildModel = new TestChildModel(indexCount);

                                testChildModel.setChapterId(jsonObject.getString("_id"));
                                testChildModel.setChapterName(jsonObject.getString("chapt_name"));
                                testChildModel.setShortName(jsonObject.getString("short_name"));
                                testChildModel.setLongName(jsonObject.getString("long_name"));
                                testChildModel.setImage(jsonObject.getString("img"));
                                testChildModel.setIsFullTest("No");
                                testChildModel.setIsFinished("No");
                                testChildModel.setCount(String.valueOf(index));
                                ArrayList<TestList> testListArrayList = new ArrayList<>();
                                if (jsonObject.getJSONArray("testlist").length() > 0) {
                                    JSONArray testlistArr = jsonObject.getJSONArray("testlist");

                                    for (int j = 0; j < testlistArr.length(); j++) {
                                        TestList testList = new TestList();
                                        JSONObject testObject = testlistArr.getJSONObject(j);
                                        testList._id = testObject.getString("_id");
                                        testList.crt_ans_mark = testObject.getString("crt_ans_mark");
                                        testList.qstn_type = testObject.getString("qstn_type");
                                        testList.wrng_ans_mark = testObject.getString("wrng_ans_mark");
                                        testList.test_name = testObject.getString("test_name");
                                        testList.long_name = testObject.getString("long_name");
                                        testList.short_name = testObject.getString("short_name");
                                        testList.no_of_qstn = testObject.getString("no_of_qstn");
                                        testList.no_of_choice = testObject.getString("no_of_choice");
                                        testList.instruction = testObject.getString("instruction");
                                        testList.type = testObject.getString("type");
                                        if (!jsonObject.getString("index").equals("-1") && Integer.parseInt(jsonObject.getString("index")) == j) {
                                            strPendingTestId = testObject.getString("_id");
                                            strPending = "1";
                                            SubscriptionTestFragment.isPending = "1";
                                            SubscriptionTestFragment.isTestIdSaved = testObject.getString("_id");
                                            SubscriptionTestFragment.testChapterId = jsonObject.getString("_id");
                                            SubscriptionTestFragment.testCMark = testObject.getString("crt_ans_mark");
                                            SubscriptionTestFragment.testEMark = testObject.getString("wrng_ans_mark");
                                            SubscriptionTestFragment.testInstruction = testObject.getString("instruction");
                                            SubscriptionTestFragment.testType = testObject.getString("qstn_type");
                                            SubscriptionTestFragment.testNumQ = testObject.getString("no_of_qstn");
                                        }
                                        testList.is_finished = "No";
                                        testListArrayList.add(testList);


                                    }
                                }
                                listItem.testArrayList = testListArrayList;
                                mList.add(listItem);

                                testChildModel.setTestArrayList(testListArrayList);
                                sectionHeaderTestListList.add(testChildModel);
                            }
                            mRecyclerView.setVisibility(View.VISIBLE);
                            loadMoreEnable = 0;
                            if (getActivity().getResources().getBoolean(R.bool.isTablet))
                                setAdapter(0, mList);
                            else
                                setupRecyclerView(0);
                        }
                        loadMoreEnable = 0;
//            else{
//              llEmpty.setVisibility(View.VISIBLE);}

                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else if (sStatus.equalsIgnoreCase("2")) {
                        packageExpiredAlert(message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {

                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void setupRecyclerView(int position) {

        // mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(context));


        //snapHelper.attachToRecyclerView(mRecyclerView);
    /*mRecyclerView.removeItemDecoration(itemDecoration);
    mRecyclerView.addItemDecoration(itemDecoration);
    mAdapter = new SubscriptionCategoryTestAdapter(getActivity(), mList,strSubjectId,strSubjectShortName);
    mRecyclerView.setAdapter(mAdapter);
    mRecyclerView.scrollToPosition(position);
    performClick();*/

        if (position == 0) {
            testSectionAdapter = new TestSectionAdapter(getActivity(), this::onParentClickListener, sectionHeaderTestListList, strSubjectId, strSubjectShortName, currentChapterSelectedPosition, currentHeadSelectedPosition, strPending, strPendingTestId);
            mRecyclerView.setAdapter(testSectionAdapter);
            StickyHeaderItemDecorator decorator = new StickyHeaderItemDecorator(testSectionAdapter);
            decorator.attachToRecyclerView(mRecyclerView);
            performClickAction(linearLayoutManager);
        } else
            testSectionAdapter.notifyDataSetChanged();

        if (currentChapterSelectedPosition > 0) {
            // mRecyclerView.scrollToPosition(currentChapterSelectedPosition);
            mRecyclerView.smoothScrollToPosition(currentChapterSelectedPosition);
        }

        findCategoryList(strChapterId);
    }

    private void performClickAction(final LinearLayoutManager linearLayoutManager) {
        testSectionAdapter.setOnItemClickListener(new ItemClickListener() {
            @Override
            public void onParentItemClick(int position) {
                currentSelectedPosition = position;
                int offset = position - linearLayoutManager.findFirstVisibleItemPosition();
                if (linearLayoutManager.findFirstVisibleItemPosition() > 0) offset -= 1;
                //linearLayoutManager.scrollToPositionWithOffset(position, offset);
                //  linearLayoutManager.smoothScrollToPosition(mRecyclerView,State(),position);
                smoothScroll(mRecyclerView, position, 280);
            }
        });
    }


    private static void smoothScroll(RecyclerView rv, int toPos, final int duration) throws IllegalArgumentException {
        final int TARGET_SEEK_SCROLL_DISTANCE_PX = 10000;     // See androidx.recyclerview.widget.LinearSmoothScroller
        int itemHeight = rv.getChildAt(0).getHeight();  // Height of first visible view! NB: ViewGroup method!
        itemHeight = itemHeight + 33;                   // Example pixel Adjustment for decoration?
        int fvPos = ((LinearLayoutManager) rv.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        int i = Math.abs((fvPos - toPos) * itemHeight);
        if (i == 0) {
            i = (int) Math.abs(rv.getChildAt(0).getY());
        }
        final int totalPix = i;                         // Best guess: Total number of pixels to scroll
        RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(rv.getContext()) {
            @Override
            protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }

            @Override
            protected int calculateTimeForScrolling(int dx) {
                int ms = (int) (duration * dx / (float) totalPix);
                // Now double the interval for the last fling.
                if (dx < TARGET_SEEK_SCROLL_DISTANCE_PX) {
                    ms = ms * 2;
                } // Crude deceleration!
                //lg(format("For dx=%d we allot %dms", dx, ms));
                return ms;
            }
        };
        //lg(format("Total pixels from = %d to %d = %d [ itemHeight=%dpix ]", fvPos, toPos, totalPix, itemHeight));
        smoothScroller.setTargetPosition(toPos);
        rv.getLayoutManager().startSmoothScroll(smoothScroller);
    }

    private void performClick() {
        mAdapter.setOnSelectedListener(new SubscriptionCategoryTestAdapter.onSelectedListener() {
            @Override
            public void onSelected(int position, boolean isFullTest, String Title, String count) {
                tvCategoryFullTestTitle.setVisibility(View.GONE);
                txtFt.setVisibility(View.INVISIBLE);
                textView17.setVisibility(View.VISIBLE);
                tvCategoryTitle.setVisibility(View.VISIBLE);
                textView17.setText(count);
                tvCategoryTitle.setText(Title);
                imageView14.setVisibility(View.VISIBLE);
                Picasso.with(context).load(IConstant.BaseUrl + sectionHeaderTestListList.get(position).getImage()).into(imageView14);
            }
        });
    }

    private void LoadMoreListener() {
        final int[] location = new int[2];
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                try {

                    LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                    int totalItemCount = layoutManager.getItemCount();
                    int lastVisible = layoutManager.findLastVisibleItemPosition();
                    if (layoutManager.findFirstVisibleItemPosition() >= 0 && !getActivity().getResources().getBoolean(R.bool.isTablet)) {
                        testSectionAdapter.setTopItemView(sectionHeaderTestListList.get(layoutManager.findFirstVisibleItemPosition()).getLongName(), (sectionHeaderTestListList.get(layoutManager.findFirstVisibleItemPosition()).getIsFullTest() != null && sectionHeaderTestListList.get(layoutManager.findFirstVisibleItemPosition()).getIsFullTest().equalsIgnoreCase("Yes")) ? sectionHeaderTestListList.get(layoutManager.findFirstVisibleItemPosition()).getFullTestCount() : sectionHeaderTestListList.get(layoutManager.findFirstVisibleItemPosition()).getCount(), layoutManager.findFirstVisibleItemPosition());
                    }

                    if (dy > 0) {
                        {
                            if (currentSelectedPosition < layoutManager.findFirstVisibleItemPosition())
                                testSectionAdapter.CollapseExpandedView(-1);
                        }
                    } else {
                        if (currentSelectedPosition > lastVisible)
                            testSectionAdapter.CollapseExpandedView(-1);
                    }
       /*if(layoutManager.findFirstVisibleItemPosition()>=0 && mAdapter.getCurrentSelectedPosition() == layoutManager.findFirstVisibleItemPosition())
       {
         parentView.setVisibility(View.VISIBLE);

       }else {
         parentView.setVisibility(View.GONE);
       }*/
                    boolean endHasBeenReached = lastVisible + 5 >= totalItemCount;

                    if ((lastVisible == totalItemCount - 1 || lastVisible == totalItemCount - 2) && totalSubsCount > indexCount) {
                        page = page + 1;

                        loadMoreEnable = 1;
                        loadMoreArticle(indexCount);
                    }

                } catch (Exception e) {

                }

            }
        });
    }

    private void loadMoreArticle(final int position) {

        loadMoreEnable = 0;
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("subject_id", strSubjectId);
        params.put("page", "" + page);
        params.put("skip", "0");
        params.put("limit", "10");
        mRequest.makeServiceRequest(IConstant.testIndex, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------testIndex Response----------------" + response);
                String sStatus = "";
                String message = "";

                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalSubsCount = object.getInt("ctotal");
                        JSONArray jsonArray = object.getJSONArray("chapters");

                        if (jsonArray.length() > 0) {
                            //llEmpty.setVisibility(View.GONE);

                            for (int i = 0; i < jsonArray.length(); i++) {
                                index = index + 1;
                                indexCount = indexCount + 1;
                                CategoryTest listItem = new CategoryTest();
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                listItem._id = jsonObject.getString("_id");
                                listItem.chapt_name = jsonObject.getString("chapt_name");
                                listItem.long_name = jsonObject.getString("long_name");
                                listItem.short_name = jsonObject.getString("short_name");
                                listItem.images = jsonObject.getString("img");
                                listItem.setChapterCount(String.valueOf(index));

                                listItem.setIsFullTest("No");

                                TestHeadetModel testHeadetModel = new TestHeadetModel(indexCount);
                                testHeadetModel.setChapterId(jsonObject.getString("_id"));
                                testHeadetModel.setChapterName(jsonObject.getString("chapt_name"));
                                testHeadetModel.setChapterName(jsonObject.getString("chapt_name"));
                                testHeadetModel.setShortName(jsonObject.getString("short_name"));
                                testHeadetModel.setLongName(jsonObject.getString("long_name"));
                                testHeadetModel.setImage(jsonObject.getString("img"));
                                testHeadetModel.setIsFullTest("No");
                                testHeadetModel.setCount(String.valueOf(index));
                                sectionHeaderTestListList.add(testHeadetModel);

                                TestChildModel testChildModel = new TestChildModel(indexCount);


                                testChildModel.setChapterId(jsonObject.getString("_id"));
                                testChildModel.setChapterName(jsonObject.getString("chapt_name"));
                                testChildModel.setShortName(jsonObject.getString("short_name"));
                                testChildModel.setLongName(jsonObject.getString("long_name"));
                                testChildModel.setImage(jsonObject.getString("img"));
                                testChildModel.setIsFullTest("No");
                                testChildModel.setCount(String.valueOf(index));
                                ArrayList<TestList> testListArrayList = new ArrayList<>();
                                if (jsonObject.getJSONArray("testlist").length() > 0) {
                                    JSONArray testlistArr = jsonObject.getJSONArray("testlist");

                                    for (int j = 0; j < testlistArr.length(); j++) {
                                        TestList testList = new TestList();
                                        JSONObject testObject = testlistArr.getJSONObject(j);
                                        testList._id = testObject.getString("_id");
                                        testList.crt_ans_mark = testObject.getString("crt_ans_mark");
                                        testList.qstn_type = testObject.getString("qstn_type");
                                        testList.wrng_ans_mark = testObject.getString("wrng_ans_mark");
                                        testList.test_name = testObject.getString("test_name");
                                        testList.long_name = testObject.getString("long_name");
                                        testList.short_name = testObject.getString("short_name");
                                        testList.no_of_qstn = testObject.getString("no_of_qstn");
                                        testList.no_of_choice = testObject.getString("no_of_choice");
                                        testList.instruction = testObject.getString("instruction");
                                        testList.type = testObject.getString("type");
                                        testList.is_finished = "No";
                                        testListArrayList.add(testList);

                                    }
                                }
                                listItem.testArrayList = testListArrayList;
                                mList.add(listItem);

                                testChildModel.setTestArrayList(testListArrayList);
                                sectionHeaderTestListList.add(testChildModel);
                            }
                            mRecyclerView.setVisibility(View.VISIBLE);
                            loadMoreEnable = 0;
                            if (getActivity().getResources().getBoolean(R.bool.isTablet))
                                setAdapter(position, mList);
                            else
                                setupRecyclerView(position);
                        }
                        loadMoreEnable = 0;

                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else if (sStatus.equalsIgnoreCase("2")) {
                        packageExpiredAlert(message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {

                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void setupStatusBarColor() {
        ((TabMainActivity) context).updateStatusBarColor(context.getResources()
                .getColor(R.color.mcq_yellow_color));
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onResume() {
        super.onResume();
  /*  TabMainActivity.isReadPage = true;
    setTimer();*/
        linearLayoutManager = new LinearLayoutManager(getActivity(),
                RecyclerView.VERTICAL, false);


    }

    @Override
    public void onPause() {
    /*if(cTimer != null && !TabMainActivity.isReadPage) {
      cTimer.cancel();
      cTimer = null;
    }*/

        super.onPause();
    }

    @Override
    public void onStop() {

        super.onStop();
        EventBus.getDefault().unregister(this);
    /*if(cTimer != null && !TabMainActivity.isReadPage) {
      cTimer.cancel();
      cTimer = null;
    }*/
    }

    private void packageExpiredAlert(String message) {

        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(getString(R.string.alert_oops));
        textViewDesc.setText(message);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                new AsyncUpdateSessionRunner("1").execute();
                TabMainActivity.tabMainActivity.finish();
            }
        });
        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity;
        if (activity instanceof TabArtcileChapterClickListener) {
            tabArtcileChapterClickListener = (TabArtcileChapterClickListener) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        tabArtcileChapterClickListener = null;
    }

    @Override
    public void onParentItemClick(int position) {
        ChildAdapterSet(position);
        tabArtcileChapterClickListener.onChapterClicked(mList.get(position).long_name, mList.get(position)._id, "", "");
    }

    @Override
    public void onHeadingClicked(int parentPosition, int childPosition, String headId) {
        EventbusPojo pojo = new EventbusPojo();
        pojo.setChapterId(mList.get(parentPosition)._id);
        pojo.setHeadId(headId);
        pojo.setChapterName(mList.get(parentPosition).long_name);
        pojo.setFragment_name("Test");
        pojo.setType(mList.get(parentPosition).testArrayList.get(childPosition).type);
        pojo.setQuestionType(mList.get(parentPosition).testArrayList.get(childPosition).qstn_type);
        pojo.setNumOfquestion(mList.get(parentPosition).testArrayList.get(childPosition).no_of_qstn);
        pojo.setIsFinished(mList.get(parentPosition).testArrayList.get(childPosition).is_finished);
        pojo.setInstruction(mList.get(parentPosition).testArrayList.get(childPosition).instruction);
        pojo.setLongname(mList.get(parentPosition).testArrayList.get(childPosition).long_name);
        pojo.setC_a_m(mList.get(parentPosition).testArrayList.get(childPosition).crt_ans_mark);
        pojo.setW_a_m(mList.get(parentPosition).testArrayList.get(childPosition).wrng_ans_mark);
        EventBus.getDefault().post(pojo);
    }

    private void setAdapter(int loadMore, ArrayList<CategoryTest> resultList) {
        if (loadMore == 0) {

            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                    RecyclerView.VERTICAL, false));

            mRecyclerView.setNestedScrollingEnabled(false);

            tabTestmainAdapter = new TabTestmainAdapter(getActivity(), resultList, this);
            mRecyclerView.setAdapter(tabTestmainAdapter);
            /*set Child View*/
            ChildAdapterSet(0);
            tabArtcileChapterClickListener.onChapterClicked(mList.get(0).long_name, mList.get(0)._id, "", "");
        } else {
            tabTestmainAdapter.notifyDataSetChanged();
        }

        item_is_selected = true;

   /* int viewHeight = articleList.size() * adapter.getItemCount();
    mRecyclerView.getLayoutParams().height = viewHeight;*/

    }

    private void ChildAdapterSet(int SelectedPosition) {

        // fragment_child_recyclerview.setHasFixedSize(true);

        fragment_child_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(),
                RecyclerView.VERTICAL, false));

 /*       LinearLayout.LayoutParams lp =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 1000);
        fragment_child_recyclerview.setLayoutParams(lp);*/

        fragment_child_recyclerview.setNestedScrollingEnabled(false);

        tabTestChildAdapter = new TabTestChildAdapter(getActivity(), mList, this, SelectedPosition, strSubjectId, strSubjectName);
        fragment_child_recyclerview.setAdapter(tabTestChildAdapter);

    /*int viewHeight = articleList.size() * child_adapter.getItemCount();
    fragment_child_recyclerview.getLayoutParams().height = 4000;*/
    }

    @Override
    public void onSearch(String headId, String ChapterId) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageView8:
                openSearch();
                break;

            case R.id.imgSearchClose:
                closeSearch();
                break;

            case R.id.imgSearchGo:
                searchAPMT();
                break;
        }
    }

    private void openSearch() {
        InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
        etSearch.requestFocus();
        llConstrainSearch.setVisibility(View.VISIBLE);


    }

    private void closeSearch() {
        etSearch.setText(null);
        InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(requireView().getWindowToken(), 0);
        llConstrainSearch.setVisibility(View.GONE);
    }

    private void searchAPMT() {
        llConstrainSearch.setVisibility(View.GONE);
        openSearchList(4, etSearch.getText().toString());
    }

    private void openSearchList(final int type, String searchKey) {
        etSearch.setText("");
        llConstrainSearch.setVisibility(View.GONE);
        alertDialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        alertDialog.setContentView(R.layout.homesearchlistdialog);
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(false);
           /* if(alertDialog.getWindow() != null) {
                alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }*/
        homeSearchRecyclerView = alertDialog.findViewById(R.id.homeSearchRecyclerView);
        final RelativeLayout rlEmptySearch = alertDialog.findViewById(R.id.rlEmptySearch);
        final ImageView imgSearchResultClose = alertDialog.findViewById(R.id.imgSearchResultClose);
        imgSearchResultClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog.isShowing())
                    alertDialog.dismiss();
            }
        });
        searchBeanArrayList.clear();
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("page", "" + page);
        params.put("search", "" + type);
        params.put("keyword", searchKey);
        mRequest.makeServiceRequest(IConstant.homeSearch, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------homeSearch Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalCountSearch = Integer.parseInt(object.getString("total"));

                        JSONArray testArray = object.getJSONArray("content");
                        if (testArray.length() > 0) {
                            rlEmptySearch.setVisibility(View.GONE);
                            for (int i = 0; i < testArray.length(); i++) {
                                SearchBean searchBean = new SearchBean();
                                JSONObject jsonObject = testArray.getJSONObject(i);
                                searchBean.setStrSubjectId(jsonObject.getString("subject_id"));
                                searchBean.setStrSubjectShortName(jsonObject.getString("subject_short_name"));
                                searchBean.setStrSubjectLongName(jsonObject.getString("subject_long_name"));
                                searchBean.setStrMImage(jsonObject.getString("mob_img"));
                                searchBean.setStrTImage(jsonObject.getString("tab_img"));
                                searchBean.setStrChaptId(jsonObject.getString("chapter_id"));
                                searchBean.setStrChaptLongName(jsonObject.getString("chapter_long_name"));
                                searchBean.setStrChaptShortName(jsonObject.getString("chapter_short_name"));
                                searchBean.setStrHeadId(jsonObject.has("heading_id") ? jsonObject.getString("heading_id") : "");
                                searchBean.setStrHeadLongName(jsonObject.has("heading_long_name") ? jsonObject.getString("heading_long_name") : "");
                                searchBean.setStrHeadShortName(jsonObject.has("heading_short_name") ? jsonObject.getString("heading_short_name") : "");
                                searchBean.setStrIsSubscribed(jsonObject.getString("subscribed_user"));
                                searchBeanArrayList.add(searchBean);

                            }

                            setupSearchAdapter(type);
                        } else {
                            rlEmptySearch.setVisibility(View.VISIBLE);
                        }

                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);

            }
        });
        if (!alertDialog.isShowing())
            alertDialog.show();

    }

    private void setupSearchAdapter(int menuType) {

        homeSearchAdapter = new HomeSearchAdapter(searchBeanArrayList, getActivity(), menuType);
        homeSearchRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        //snapHelper.attachToRecyclerView(homeSearchRecyclerView);
        homeSearchRecyclerView.setAdapter(homeSearchAdapter);
        homeSearchRecyclerView.setVisibility(View.VISIBLE);
        performSearchClick();
    }

    private void performSearchClick() {
        homeSearchAdapter.setOnSearchItemClickListener(new OnSearchItemClickListener() {
            @Override
            public void onSearchItemClicked(int position, int menuType) {
                if (alertDialog != null && alertDialog.isShowing())
                    alertDialog.dismiss();
                strHeadId = searchBeanArrayList.get(position).getStrHeadId();
                strChapterId = searchBeanArrayList.get(position).getStrChaptId();
                findCategoryList(searchBeanArrayList.get(position).getStrChaptId());
                // populateList();
            }
        });
    }

    @Override
    public void onParentClickListener(String rootId,int Position) {
        ArrayList<String> id = new ArrayList<>();
        actualPosition = 0;
        fakePosition = 0;
        childPosition = 0;

        for (int i = 0; i < mList.size(); i++) {
            if (rootId.equalsIgnoreCase(mList.get(i)._id)) {
                actualPosition = i;
                break;
            }
        }
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i).getIsFullTest().equalsIgnoreCase("No"))
                id.add(mList.get(i)._id);
        }
        for (int i = 0; i < id.size(); i++) {
            if (rootId.equalsIgnoreCase(id.get(i))) {
                fakePosition = i;
                break;
            }
        }
        for (int i = 0; i < sectionHeaderTestListList.size(); i++) {
            if (rootId.equalsIgnoreCase(sectionHeaderTestListList.get(i).getChapteId())) {
                childPosition = i;
            }
        }
        try {
            if (mList.get(actualPosition).getIsFullTest().equals("No")){
                CustomDialogActivity.mList=mList;
                CustomDialogActivity.sectionHeaderTestListList=sectionHeaderTestListList;
                CustomDialogActivity.strSubjectId=strSubjectId;
                CustomDialogActivity.strSubjectShortName=strSubjectShortName;
                CustomDialogActivity.strPendingTestId=strPendingTestId;
                CustomDialogActivity.MainParentPosition=actualPosition;
                CustomDialogActivity.childPosition=childPosition;
                CustomDialogActivity.ParentLongName=mList.get(actualPosition).long_name;
                CustomDialogActivity.ParentPosition=fakePosition;
                CustomDialogActivity.ImageUrl=mList.get(actualPosition).images;
                CustomDialogActivity.currentPage=getResources().getString(R.string.test);
                startActivity(new Intent(getActivity(), CustomDialogActivity.class));
                //activitySubscriptionListDialog = new ActivitySubscriptionListDialog(getActivity(), newTestSectionAdapter, mList.get(actualPosition).long_name, fakePosition, mList.get(actualPosition).images, getResources().getString(R.string.test));
                // activitySubscriptionListDialog.show();
            } else{


                 if (sessionManager.getBookmarkDetails().get(SessionManager.BOOKMARK_TITLE)!=null && sessionManager.getBookmarkDetails().get(SessionManager.BOOKMARK_TITLE).equalsIgnoreCase("Test")){


                     Dialog alertDialog;
                     alertDialog = new Dialog(getActivity());
                     //  alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                     alertDialog.setContentView(R.layout.custom_alert);
                     alertDialog.setCancelable(false);
//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                     TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
                     TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
                     textViewTitle.setText(getString(R.string.alert_incomplete));
                     textViewDesc.setText(getString(R.string.alert_incomplete_message));
                     Button btnOk = alertDialog.findViewById(R.id.btnOk);
                     Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
                     btnOk.setText("Continue");
                     btnCancel.setText("New Test");
                     btnOk.setOnClickListener(new View.OnClickListener() {
                         @Override
                         public void onClick(View v) {
                             alertDialog.dismiss();
                             startActivity(new Intent(getActivity(), collMainActivity.class)

                                     .putExtra("subject_id",sessionManager.getIncompleteTest().get(SessionManager.SUBJECT_ID))
                                     .putExtra("chapter_id", sessionManager.getIncompleteTest().get(SessionManager.CHAPTER_ID))
                                     .putExtra("chapter_name",sessionManager.getIncompleteTest().get(SessionManager.CHAPTER_NAME1))
                                     .putExtra("subject_name", sessionManager.getIncompleteTest().get(SessionManager.SUBJECT_NAME1))
                                     .putExtra("pending", "1")
                                     .putExtra("pendingTestId", sessionManager.getIncompleteTest().get(SessionManager.PENDINGTESTID))
                                     .putExtra("testId",sessionManager.getIncompleteTest().get(SessionManager.TESTID))
                                     .putExtra("type",sessionManager.getIncompleteTest().get(SessionManager.TESTTYPE))
                                     .putExtra("no_of_question", sessionManager.getIncompleteTest().get(SessionManager.NO_OF_QUESTION))
                                     .putExtra("isFinished", sessionManager.getIncompleteTest().get(SessionManager.IS_FINISHED))
                                     .putExtra("questionType", sessionManager.getIncompleteTest().get(SessionManager.QUESTIONTYPE))
                                     .putExtra("strInstruction",sessionManager.getIncompleteTest().get(SessionManager.STRINGSTRUCTION))
                                     .putExtra("strLongName",sessionManager.getIncompleteTest().get(SessionManager.STRLONGNAME))
                                     .putExtra("crt_ans_mark", sessionManager.getIncompleteTest().get(SessionManager.CRT_ANS_MARK))
                                     .putExtra("wrng_ans_mark", sessionManager.getIncompleteTest().get(SessionManager.WRNG_ANS_MARK))
                             );


                         }
                     });
                     btnCancel.setOnClickListener(new View.OnClickListener() {
                         @Override
                         public void onClick(View v) {


                             alertDialog.dismiss();
                             TestSection testSection = sectionHeaderTestListList.get(childPosition);
                             TabMainActivity.isReadPage = true;
                             context.startActivity(new Intent(context, collMainActivity.class)
                                     .putExtra("subject_id", strSubjectId)
                                     .putExtra("chapter_id", testSection.getChapteId())
                                     .putExtra("chapter_name", testSection.getShortName())
                                     .putExtra("subject_name", strSubjectShortName)
                                     .putExtra("pending", "0")
                                     .putExtra("pendingTestId", "")
                                     .putExtra("testId", testSection.getTestArrayList().get(0)._id)
                                     .putExtra("type", testSection.getTestArrayList().get(0).type)
                                     .putExtra("no_of_question", testSection.getTestArrayList().get(0).no_of_qstn)
                                     .putExtra("isFinished", testSection.getTestArrayList().get(0).is_finished)
                                     .putExtra("questionType", testSection.getTestArrayList().get(0).qstn_type)
                                     .putExtra("strInstruction", testSection.getTestArrayList().get(0).instruction)
                                     .putExtra("strLongName", testSection.getTestArrayList().get(0).long_name)
                                     .putExtra("crt_ans_mark", testSection.getTestArrayList().get(0).crt_ans_mark)
                                     .putExtra("wrng_ans_mark", testSection.getTestArrayList().get(0).wrng_ans_mark));

                         }
                     });

                     alertDialog.show();

                 }else {
                     TestSection testSection = sectionHeaderTestListList.get(childPosition);
                     TabMainActivity.isReadPage = true;
                     context.startActivity(new Intent(context, collMainActivity.class)
                             .putExtra("subject_id", strSubjectId)
                             .putExtra("chapter_id", testSection.getChapteId())
                             .putExtra("chapter_name", testSection.getShortName())
                             .putExtra("subject_name", strSubjectShortName)
                             .putExtra("pending", "0")
                             .putExtra("pendingTestId", "")
                             .putExtra("testId", testSection.getTestArrayList().get(0)._id)
                             .putExtra("type", testSection.getTestArrayList().get(0).type)
                             .putExtra("no_of_question", testSection.getTestArrayList().get(0).no_of_qstn)
                             .putExtra("isFinished", testSection.getTestArrayList().get(0).is_finished)
                             .putExtra("questionType", testSection.getTestArrayList().get(0).qstn_type)
                             .putExtra("strInstruction", testSection.getTestArrayList().get(0).instruction)
                             .putExtra("strLongName", testSection.getTestArrayList().get(0).long_name)
                             .putExtra("crt_ans_mark", testSection.getTestArrayList().get(0).crt_ans_mark)
                             .putExtra("wrng_ans_mark", testSection.getTestArrayList().get(0).wrng_ans_mark));
                 }


            }




        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public  void findCategoryList(String rootId){
        strChapterId="";
       if (rootId!=null && !rootId.equals("")){
           ArrayList<String> id = new ArrayList<>();
           actualPosition = 0;
           fakePosition = 0;
           childPosition = 0;

           for (int i = 0; i < mList.size(); i++) {
               if (rootId.equalsIgnoreCase(mList.get(i)._id)) {
                   actualPosition = i;
                   break;
               }
           }
           for (int i = 0; i < mList.size(); i++) {
               if (mList.get(i).getIsFullTest().equalsIgnoreCase("No"))
                   id.add(mList.get(i)._id);
           }
           for (int i = 0; i < id.size(); i++) {
               if (rootId.equalsIgnoreCase(id.get(i))) {
                   fakePosition = i;
                   break;
               }
           }
           for (int i = 0; i < sectionHeaderTestListList.size(); i++) {
               if (rootId.equalsIgnoreCase(sectionHeaderTestListList.get(i).getChapteId())) {
                   childPosition = i;
               }
           }


           if (mList.get(actualPosition).getIsFullTest().equals("No")){
               CustomDialogActivity.mList=mList;
               CustomDialogActivity.sectionHeaderTestListList=sectionHeaderTestListList;
               CustomDialogActivity.strSubjectId=strSubjectId;
               CustomDialogActivity.strSubjectShortName=strSubjectShortName;
               CustomDialogActivity.strPendingTestId=strPendingTestId;
               CustomDialogActivity.MainParentPosition=actualPosition;
               CustomDialogActivity.childPosition=childPosition;
               CustomDialogActivity.ParentLongName=mList.get(actualPosition).long_name;
               CustomDialogActivity.ParentPosition=fakePosition;
               CustomDialogActivity.ImageUrl=mList.get(actualPosition).images;
               CustomDialogActivity.currentPage=getResources().getString(R.string.test);
               startActivity(new Intent(getActivity(), CustomDialogActivity.class));
               //activitySubscriptionListDialog = new ActivitySubscriptionListDialog(getActivity(), newTestSectionAdapter, mList.get(actualPosition).long_name, fakePosition, mList.get(actualPosition).images, getResources().getString(R.string.test));
               // activitySubscriptionListDialog.show();
           }
       }
    }

    @Override
    public void OnChildItemClickListener(String message) {
        if (activitySubscriptionListDialog != null)
            activitySubscriptionListDialog.dismiss();
    }

    private class AsyncUpdateSessionRunner extends AsyncTask<String, String, String> {
        String result = "";
        String callingType = "";

        public AsyncUpdateSessionRunner(String s) {
            callingType = s;
        }

        @Override
        protected String doInBackground(String... strings) {
            ServiceRequest mRequest = new ServiceRequest(getActivity());
            HashMap<String, String> params = new HashMap<>();
            params.put("session_id", TabMainActivity.strSessionId);

            params.put("time", String.valueOf(TabMainActivity.startingLevelTime - TabMainActivity.packageTime));
            params.put("close", callingType);

            Log.e("startingLevelTime", "" + TabMainActivity.startingLevelTime + " , packageTime : " + TabMainActivity.packageTime);
            TabMainActivity.startingLevelTime = TabMainActivity.packageTime;
            mRequest.makeServiceRequest(IConstant.updateSession, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("updateSession" + "-------- Response----------------" + response);
                    String sStatus = "";
                    String message = "";
                    try {
                        JSONObject obj = new JSONObject(response);
                        sStatus = obj.getString("status");
                        message = obj.has("message") ? obj.getString("message") : "";
                        result = sStatus;

                        if (sStatus.equalsIgnoreCase("1")) {
                            //JSONObject object = obj.getJSONObject("response");


                        } else if (sStatus.equals("00")) {
                            //customAlert.singleLoginAlertLogout();
                        } else if (sStatus.equalsIgnoreCase("01")) {
                            //customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                        } else {
                            //customAlert.showAlertOk(getString(R.string.alert_oops),message);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onErrorListener(String errorMessage) {
                    // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
                }
            });
            return result;


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            // new AsyncSubjectsRunner(s).execute();
        }
    }

    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
    }

    private void onScrollRecyclerview() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            mRecyclerView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                    if (item_is_selected) {


                    }

                }
            });

        } else {

            mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    if (item_is_selected) {

                       /* item_is_selected=false;

                        ChildViewGone();*/
                    }
                }
            });
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onResultReceived(EventBusTestRefreshPojo eventBusTestRefreshPojo) {
        if (eventBusTestRefreshPojo.getStrRefresh().equals("RefreshList")) {
            strChapterId = eventBusTestRefreshPojo.getStrChapterId();
            populateList();
        }
    }
}