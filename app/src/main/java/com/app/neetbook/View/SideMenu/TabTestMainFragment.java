package com.app.neetbook.View.SideMenu;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.neetbook.Interfaces.OnSubmitListioner;
import com.app.neetbook.Interfaces.OnTestAttemp;
import com.app.neetbook.Interfaces.RefreshInterface;
import com.app.neetbook.R;
import com.app.neetbook.Utils.OnSwipeTouchListener;
import com.app.neetbook.Utils.SessionManager;
import com.google.android.material.tabs.TabLayout;


public class TabTestMainFragment extends Fragment implements View.OnClickListener, OnTestAttemp, RefreshInterface {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Context context;
    SessionManager sessionManager;
    private String subject_id = "",chapter_id = "",chapter_name = "",subject_name = "",testId = "";
    private String type = "",no_of_question = "",isFinished = "",questionType = "",strInstruction = "",strLongName = "",crt_ans_mark = "",wrng_ans_mark = "";
    /*public static ArrayList<TestList> testArrayList;*/
    private TextView textView,txtSurgical;
    Typeface tfBold,tfMedium,tfRegular;
    LinearLayout llParent,llResult;
    private  TextView txtStopwatch;
    private TextView txtQACount;
    RelativeLayout rlSubmit,rlInstruction,rlTimer;
    private int selectedIndex = 0;
    ImageView imageView2;
    RelativeLayout rlParent;
    private ImageView imgTimer,imgCloseInstruction;
    RefreshInterface refreshInterface;
    private TextView textView3,textView4,textView5,textView6,textView7,textView8,textView9,textView10,textViewInstruction,txtEmoji;
    private  TextView txtTestTitle;
    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L ;
    Handler handler,handlerShowTimer;
    private ConstraintLayout constrainInstruction;
    int Seconds, Minutes, MilliSeconds ;
    private OnSubmitListioner onSubmitListioner;
    public TabTestMainFragment(String subject_id,String chapter_id,String chapter_name,
                               String subject_name,String testId,String type,
                               String no_of_question,String isFinished,String questionType,
                               String strInstruction,String strLongName,String crt_ans_mark,String wrng_ans_mark) {
    
    this.subject_id = subject_id;
    this.chapter_id = chapter_id;
    this.chapter_name = chapter_name;
    this.subject_name = subject_name;
    this.testId = testId;
    this.type = type;
    this.no_of_question = no_of_question;
    this.isFinished = isFinished;
    this.questionType = questionType;
    this.strInstruction = strInstruction;
    this.strLongName = strLongName;
    this.crt_ans_mark = crt_ans_mark;
    this.wrng_ans_mark = wrng_ans_mark;
    }

   
    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab_test_main, container, false);
        init(view);
        return view;
    }

    private void init(View view) {

        viewPager = view.findViewById(R.id.viewpager_subscription_category);
        tabLayout = view.findViewById(R.id.tabs_subscription_category);
        textView = view.findViewById(R.id.textView);

        llResult = view.findViewById(R.id.llResult);
        imageView2 = view.findViewById(R.id.imageView2);
        txtTestTitle = view.findViewById(R.id.txtTestTitle);
        txtSurgical = view.findViewById(R.id.textView2);
        txtQACount = view.findViewById(R.id.txtQACount);
        txtStopwatch = view.findViewById(R.id.txtStopwatch);
        llParent = view.findViewById(R.id.llParent);
        rlSubmit = view.findViewById(R.id.rlSubmit);
        rlTimer = view.findViewById(R.id.rlTimer);
        imgTimer = view.findViewById(R.id.imgTimer);
        textViewInstruction = view.findViewById(R.id.textViewInstruction);
        rlInstruction = view.findViewById(R.id.rlInstruction);
        imgCloseInstruction = view.findViewById(R.id.imgCloseInstruction);
        constrainInstruction = view.findViewById(R.id.constrainInstruction);


        textView3 = view.findViewById(R.id.textView3);
        textView4 = view.findViewById(R.id.textView4);
        textView5 = view.findViewById(R.id.textView5);
        textView6 = view.findViewById(R.id.textView6);
        textView7 = view.findViewById(R.id.textView7);
        textView8 = view.findViewById(R.id.textView8);
        textView10 = view.findViewById(R.id.textView10);
        txtEmoji = view.findViewById(R.id.txtEmoji);
        textView9 = view.findViewById(R.id.textView9);

        

        txtTestTitle.setText(strLongName);
        textViewInstruction.setText(strInstruction);
        refreshInterface = this;
        sessionManager = new SessionManager(getActivity());
        listener();
        setFontSize();
        setFragment();
        startStopwatch();
        
        
    }

    private void setFragment() {

        Fragment fragment = new SubscriptionTestFragment(testId, type, chapter_id, subject_id, chapter_name, no_of_question, isFinished, questionType, strInstruction,strLongName, crt_ans_mark,wrng_ans_mark,"","");
        onSubmitListioner = (OnSubmitListioner) fragment;
        ((SubscriptionTestFragment) fragment).intialiseRefreshInterface(refreshInterface);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameContainer, fragment);
        fragmentTransaction.commit();

    }
    private void listener() {
        imageView2.setOnClickListener(this);
        imgCloseInstruction.setOnClickListener(this);
        rlSubmit.setOnClickListener(this);
        rlInstruction.setOnClickListener(this);
        rlTimer.setOnClickListener(this);

    }

    private void setFontSize() {
        tfBold  = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium  = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular  = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaRegular.otf");
        textView.setText(subject_name);
        txtSurgical.setText(chapter_name);
        if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
            textView.setTextAppearance(getActivity(),R.style.textViewSmallSubjectName);
            txtSurgical.setTextAppearance(getActivity(),R.style.textViewSmallChaptName);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
            textView.setTextAppearance(getActivity(),R.style.textViewMediumSubjectName);
            txtSurgical.setTextAppearance(getActivity(),R.style.textViewMediumChaptName);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
            textView.setTextAppearance(getActivity(),R.style.textViewLargeSubjectName);
            txtSurgical.setTextAppearance(getActivity(),R.style.textViewLargeChaptName);
        }

        textView.setTypeface(tfMedium);
        txtSurgical.setTypeface(tfMedium);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.imageView2:
                //finish();
                break;

            case R.id.imgCloseInstruction:
                constrainInstruction.setVisibility(View.GONE);
                break;
            case R.id.rlSubmit:
                onSubmitListioner.onSubmit();
                constrainInstruction.setVisibility(View.GONE);
                break;
            case R.id.rlInstruction:
                constrainInstruction.setVisibility(constrainInstruction.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                break;

            case R.id.rlTimer:
                txtStopwatch.setVisibility(View.VISIBLE);
                imgTimer.setVisibility(View.GONE);
                handlerShowTimer.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        txtStopwatch.setVisibility(View.GONE);
                        imgTimer.setVisibility(View.VISIBLE);
                    }
                },5000);
                break;
        }
    }


 /* private void setupViewPager(final ViewPager viewPager) {
    final ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(),
        this);

    if(testArrayList.size()>0)
    {
      for (int i=0;i<testArrayList.size();i++)
      {
        if(testId.equalsIgnoreCase(testArrayList.get(i)._id)) {
          selectedIndex = i;
          viewPagerAdapter.addFragment(new SubscriptionTestFragment(testArrayList.get(i)._id, testArrayList.get(i).type, chapter_id, subject_id, chapter_name, testArrayList.get(i).no_of_qstn, testArrayList.get(i).is_finished, testArrayList.get(i).qstn_type, testArrayList.get(i).instruction,testArrayList.get(i).long_name), testArrayList.get(i).short_name);
          break;
        }
      }
    }
*//*Test Complete Fragment*//*
     *//*viewPagerAdapter.addFragment(new SubscriptionTestCompleteFragment(), "Test 2 ");*//*

    viewPager.setAdapter(viewPagerAdapter);

    viewPager.setCurrentItem(selectedIndex);
    tabLayout.setupWithViewPager(viewPager);
    for (int i = 0; i < tabLayout.getTabCount(); i++) {
      TabLayout.Tab tab = tabLayout.getTabAt(i);
      if (tab != null) {
        tab.setCustomView(viewPagerAdapter.getTabView(i));
      }
    }
  }*/

    /* private class ViewPagerAdapter extends FragmentPagerAdapter {
   
       List<Fragment> fragmentList = new ArrayList<>();
       List<String> fragmentTitles = new ArrayList<>();
       Context mContext;
       TextView tabTitles;
   
   
       ViewPagerAdapter(FragmentManager fragmentManager,
                        FragmentActivity activity) {
         super(fragmentManager);
         mContext = activity;
       }
   
       @Override
       public Fragment getItem(int i) {
         *//*switch (i) {
        case 0: {*//*
          return new SubscriptionTestFragment(testArrayList.get(i)._id,testArrayList.get(i).type,chapter_id,subject_id,chapter_name,testArrayList.get(i).no_of_qstn,testArrayList.get(i).is_finished,testArrayList.get(i).qstn_type,testArrayList.get(i).instruction,testArrayList.get(i).long_name);
        *//*}
        case 1: {
          return SubscriptionTestCompleteFragment(testArrayList.get(i)._id,testArrayList.get(i).type,chapter_id,subject_id,chapter_name,testArrayList.get(i).no_of_qstn);
        }
        default:
          return new SubscriptionTestFragment(testArrayList.get(i)._id,testArrayList.get(i).type,chapter_id,subject_id,chapter_name,testArrayList.get(i).no_of_qstn);
      }*//*
    }

    @Override
    public int getCount() {
      return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
      return fragmentTitles.get(position);
    }

    void addFragment(Fragment fragment, String name) {
      fragmentList.add(fragment);
      fragmentTitles.add(name);
    }

    View getTabView(int position) {
      // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
      View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_items, null);
      tabTitles = v.findViewById(R.id.txtTabTitle);
      tabTitles.setText(fragmentTitles.get(position));
      tabTitles.setTextColor(getResources().getColor(R.color.white));
      return v;
    }
  }*/
    public Runnable runnable = new Runnable() {

        public void run() {

            MillisecondTime = SystemClock.uptimeMillis() - StartTime;

            UpdateTime = TimeBuff + MillisecondTime;

            Seconds = (int) (UpdateTime / 1000);

            Minutes = Seconds / 60;

            Seconds = Seconds % 60;

            MilliSeconds = (int) (UpdateTime % 1000);

            txtStopwatch.setText("" + Minutes + ":"
                    + String.format("%02d", Seconds));
            /* + String.format("%03d", MilliSeconds));*/

            handler.postDelayed(this, 0);
        }

    };
    private void startStopwatch() {
        txtStopwatch.setVisibility(View.VISIBLE);
        imgTimer.setVisibility(View.GONE);
        StartTime = SystemClock.uptimeMillis();
        handler =  new Handler();
        handler.postDelayed(runnable, 0);

        handlerShowTimer = new Handler();
        handlerShowTimer.postDelayed(new Runnable() {
            @Override
            public void run() {
                txtStopwatch.setVisibility(View.GONE);
                imgTimer.setVisibility(View.VISIBLE);
            }
        },5000);

    }

    @Override
    public void onAnswered(String strQAndA, int numAnswered) {
        txtQACount.setText(strQAndA);
    }

    public void onAnsweredTest(String strQAndA) {
        txtQACount.setText(strQAndA);
        Log.e("strAndA",strQAndA);
    }



    @Override
    public void onSubmitted(String correctAnswer, String wrongAnswer, String questionAttented, String toHundred, double percentage) {
        textView3.setText(correctAnswer);
        textView5.setText(questionAttented);
        textView7.setText(wrongAnswer);
        textView9.setText(toHundred);
        textView10.setText("Score "+(percentage>0?percentage : 0)+"%");
        txtEmoji.setText(percentage == 0 ? (new String(Character.toChars(0x1F625))) : percentage>0 && percentage< 25 ? (new String(Character.toChars(0x1F625)))
                : percentage>24 && percentage< 50 ? (new String(Character.toChars(0x1F614)))
                : percentage>49 && percentage< 75 ? (new String(Character.toChars(0x1F604)))
                : (new String(Character.toChars(0x1F606))));
        llResult.setVisibility(View.VISIBLE);
        TimeBuff += MillisecondTime;
        handler.removeCallbacks(runnable);
    }

    @Override
    public void refresh_fragment(String qAnda) {
        txtQACount.setText(qAnda);
        Log.e("strAndA",qAnda);
    }
}
