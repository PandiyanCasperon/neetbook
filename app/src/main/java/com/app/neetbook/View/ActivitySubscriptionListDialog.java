package com.app.neetbook.View;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.neetbook.Adapter.ArticleSectionAdapter;
import com.app.neetbook.R;
import com.app.neetbook.Utils.widgets.CustomTextView;
import com.app.neetbook.Utils.widgets.CustomTextViewMedium;
import com.app.neetbook.serviceRequest.IConstant;
import com.squareup.picasso.Picasso;

import java.util.Objects;

public class ActivitySubscriptionListDialog extends Dialog {

    public Activity activity;
    public Dialog dialog;
    ConstraintLayout rootLayout,titleHeadLayout;
    private View squareCircleView;
    ImageView dialogCancel,catagorIcon;
    CustomTextViewMedium subscriptionHeaderTitle,catePosition,special;
    private RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    private String headerTitle;
    private String catagoryImageUrl;
    private int catagoryPosition;
    private String currentFragmentPage;
    private String specialheading;

    public ActivitySubscriptionListDialog(Activity a, RecyclerView.Adapter adapter, String headerTitle,int catagoryPosition,String imageUrl,String currentFragmentPage) {
        super(a);
        this.activity = a;
        this.adapter = adapter;
        this.headerTitle = headerTitle;
        this.catagoryPosition=catagoryPosition;
        this.catagoryImageUrl=imageUrl;
        this.currentFragmentPage=currentFragmentPage;
    }
    public ActivitySubscriptionListDialog(Activity a, RecyclerView.Adapter adapter, String headerTitle,String currentFragmentPage,String specialTitle) {
        super(a);
        this.activity = a;
        this.adapter = adapter;
        this.headerTitle = headerTitle;
        this.currentFragmentPage=currentFragmentPage;
        this.specialheading=specialTitle;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_subscription_dialog_layout);
        getWindow().setWindowAnimations(R.style.full_screen_dialog);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
       // Objects.requireNonNull(getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        /*WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
        layoutParams.x = 100; // right margin
        layoutParams.y = 170; // top margin
        dialog.getWindow().setAttributes(layoutParams);*/
        // declaration part
        dialogCancel = findViewById(R.id.dialogCancel);
        subscriptionHeaderTitle = findViewById(R.id.subscriptionHeaderTitle);
        special=findViewById(R.id.specialTitle);
        catePosition = findViewById(R.id.tvCategoryposition);
        recyclerView = findViewById(R.id.subscriptionList);
        rootLayout=findViewById(R.id.rootLayout);
        titleHeadLayout=findViewById(R.id.titleHeadLayout);
        squareCircleView=findViewById(R.id.view);
        catagorIcon=findViewById(R.id.titleImage);

        //declaration end
        if (currentFragmentPage.equals(activity.getResources().getString(R.string.artical))){
            rootLayout.setBackground(ContextCompat.getDrawable(activity,R.drawable.article_custom_dialog_background));
            titleHeadLayout.setBackground(ContextCompat.getDrawable(activity,R.drawable.article_dialog_head_layout_background));
            squareCircleView.setBackground(ContextCompat.getDrawable(activity,R.drawable.article_dialog_head_square));
            catePosition.setBackground(ContextCompat.getDrawable(activity,R.drawable.bg_curve_articles));
            catagorIcon.setColorFilter(ContextCompat.getColor(activity, R.color.artical_pink));
            dialogCancel.setColorFilter(ContextCompat.getColor(activity, R.color.primary_meroon));
            catePosition.setText(String.valueOf(catagoryPosition+1));


        }else if(currentFragmentPage.equals(activity.getResources().getString(R.string.points))){
            rootLayout.setBackground(ContextCompat.getDrawable(activity,R.drawable.points_custom_dialog_background));
            titleHeadLayout.setBackground(ContextCompat.getDrawable(activity,R.drawable.points_dialog_head_layout_background));
            squareCircleView.setBackground(ContextCompat.getDrawable(activity,R.drawable.points_dialog_head_square));
            catePosition.setBackground(ContextCompat.getDrawable(activity,R.drawable.bg_curve_points));
            catagorIcon.setColorFilter(ContextCompat.getColor(activity, R.color.new_points_green_dark));
            dialogCancel.setColorFilter(ContextCompat.getColor(activity, R.color.points_green));
            catePosition.setText(String.valueOf(catagoryPosition+1));
        }
        else if(currentFragmentPage.equals(activity.getResources().getString(R.string.mcq))){
            rootLayout.setBackground(ContextCompat.getDrawable(activity,R.drawable.mcq_custom_dialog_background));
            titleHeadLayout.setBackground(ContextCompat.getDrawable(activity,R.drawable.mcq_dialog_head_layout_background));
            squareCircleView.setBackground(ContextCompat.getDrawable(activity,R.drawable.mcq_dialog_head_square));
            catePosition.setBackground(ContextCompat.getDrawable(activity,R.drawable.bg_curve_mcq));
            catagorIcon.setColorFilter(ContextCompat.getColor(activity, R.color.mcq_blue));
            dialogCancel.setColorFilter(ContextCompat.getColor(activity, R.color.mcq_blue_dark));
            catePosition.setText(String.valueOf(catagoryPosition+1));
        }
        else if(currentFragmentPage.equals(activity.getResources().getString(R.string.test))) {
            if (specialheading!=null) {
                catagorIcon.setBackgroundResource(0);
                catePosition.setText("");
                special.setVisibility(View.VISIBLE);
                special.setText(specialheading);

            }
            else {
                special.setVisibility(View.GONE);
                catePosition.setText(String.valueOf(catagoryPosition+1));
                catePosition.setBackground(ContextCompat.getDrawable(activity,R.drawable.bg_curve_test));
            }
            rootLayout.setBackground(ContextCompat.getDrawable(activity,R.drawable.test_custom_dialog_background));
            titleHeadLayout.setBackground(ContextCompat.getDrawable(activity,R.drawable.test_dialog_head_layout_background));
            squareCircleView.setBackground(ContextCompat.getDrawable(activity,R.drawable.test_dialog_head_square));
            catagorIcon.setColorFilter(ContextCompat.getColor(activity, R.color.test_yellow));
            dialogCancel.setColorFilter(ContextCompat.getColor(activity, R.color.test_yellow));
           // recyclerView.setVerticalScrollbarThumbDrawable(ContextCompat.getDrawable(activity,R.drawable.test_dialog_root_background));
        }
        subscriptionHeaderTitle.setText(headerTitle);
        Picasso.with(activity).load(IConstant.BaseUrl +catagoryImageUrl).into(catagorIcon);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);
        dialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }
    public boolean onTouchEvent(MotionEvent event)
    {

        if(event.getAction() == MotionEvent.ACTION_OUTSIDE){
            System.out.println("TOuch outside the dialog ******************** ");
            this.dismiss();
        }
        return false;
    }
}
