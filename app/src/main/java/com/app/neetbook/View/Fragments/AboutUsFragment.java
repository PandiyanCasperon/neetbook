package com.app.neetbook.View.Fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.app.neetbook.R;
import com.app.neetbook.Utils.AppWebViewClients;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.widgets.DisplayOrientation;
import com.app.neetbook.Utils.widgets.HTMLTextConverter;
import com.app.neetbook.View.MobilePagesActivity;
import com.app.neetbook.View.TermsConditions;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.jar.JarInputStream;


public class AboutUsFragment extends Fragment {

    WebView webView;
    Loader loader;
    SessionManager sessionManager;
    TextView txtTermsAndConditions;
    ImageView ImgTC;
    CustomAlert customAlert;
    public AboutUsFragment() {
        // Required empty public constructor
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about_us, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        sessionManager = new SessionManager(getActivity());
        customAlert = new CustomAlert(getActivity());
        loader = new Loader(getActivity());
        webView = (WebView) view.findViewById(R.id.webView);
        ImgTC = view.findViewById(R.id.ImgTC);
        txtTermsAndConditions = view.findViewById(R.id.txtTermsAndConditions);
        webView.requestFocus();
        webView.getSettings().setLightTouchEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setGeolocationEnabled(true);
        webView.setWebViewClient(new AppWebViewClients(loader, getActivity()));
        webView.setSoundEffectsEnabled(true);
        if(getResources().getBoolean(R.bool.isTablet)) {
            if (DisplayOrientation.getDisplayOrientation(getActivity()) == 1 || DisplayOrientation.getDisplayOrientation(getActivity()) == 3)
                Picasso.with(getActivity()).load(R.drawable.splash_logo).into(ImgTC);
            else
                Picasso.with(getActivity()).load(R.drawable.about_us_text).into(ImgTC);
        }
        prepareTermsAndConditions();

    }
    private void prepareTermsAndConditions() {



        ServiceRequest mRequest = new ServiceRequest(getActivity());

        mRequest.makeServiceRequest(IConstant.about_us, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------about_us Response----------------" + response);
                String sStatus = "";
//
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    if(jsonObject.getString("status").equals("1")) {
                        jsonObject = new JSONObject(response);
                        webView.loadData(jsonObject.getString("description") , "text/html; charset=UTF-8", null);

                    } else if(sStatus.equals("00")){

                        customAlert.singleLoginAlertLogout();
                    }else if(sStatus.equalsIgnoreCase("01")){

                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),jsonObject.getString("message"));
                    } else {

                        customAlert.showAlertOk(getString(R.string.alert_oops),jsonObject.getString("message"));
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {

                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

}
