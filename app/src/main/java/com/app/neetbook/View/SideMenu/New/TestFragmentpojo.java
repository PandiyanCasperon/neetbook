package com.app.neetbook.View.SideMenu.New;

import java.util.ArrayList;

public class TestFragmentpojo implements Section{

    private int section;

    public TestFragmentpojo(int section) {
        this.section = section;
    }

    private String full_test_id="";
    private String full_test_qstn_type="";
    private String full_test_crt_ans_mark="";
    private String full_test_wrng_ans_mark="";
    private String full_test_fulltest_name="";
    private String full_test_short_name="";
    private String full_test_long_name="";
    private String full_test_no_of_qstn="";
    private String full_test_no_of_choice="";
    private String full_test_instruction="";
    private String full_test_type="";
    private String full_test_pending="";
    private Boolean is_full_test=false;

    private String chapter_id="";
    private String chapter_name="";
    private String chapter_long_name="";
    private String chapter_short_name="";
    private String chapter_img="";
    private int section_header;
    private boolean is_expanded=false;

    public boolean isIs_expanded() {
        return is_expanded;
    }

    public void setIs_expanded(boolean is_expanded) {
        this.is_expanded = is_expanded;
    }

    private ArrayList<TestlistPojo> testlist_array;

    public Boolean getIs_full_test() {
        return is_full_test;
    }

    public void setIs_full_test(Boolean is_full_test) {
        this.is_full_test = is_full_test;
    }

    public String getFull_test_id() {
        return full_test_id;
    }

    public void setFull_test_id(String full_test_id) {
        this.full_test_id = full_test_id;
    }

    public String getFull_test_qstn_type() {
        return full_test_qstn_type;
    }

    public void setFull_test_qstn_type(String full_test_qstn_type) {
        this.full_test_qstn_type = full_test_qstn_type;
    }

    public String getFull_test_crt_ans_mark() {
        return full_test_crt_ans_mark;
    }

    public void setFull_test_crt_ans_mark(String full_test_crt_ans_mark) {
        this.full_test_crt_ans_mark = full_test_crt_ans_mark;
    }

    public String getFull_test_wrng_ans_mark() {
        return full_test_wrng_ans_mark;
    }

    public void setFull_test_wrng_ans_mark(String full_test_wrng_ans_mark) {
        this.full_test_wrng_ans_mark = full_test_wrng_ans_mark;
    }

    public String getFull_test_fulltest_name() {
        return full_test_fulltest_name;
    }

    public void setFull_test_fulltest_name(String full_test_fulltest_name) {
        this.full_test_fulltest_name = full_test_fulltest_name;
    }

    public String getFull_test_short_name() {
        return full_test_short_name;
    }

    public void setFull_test_short_name(String full_test_short_name) {
        this.full_test_short_name = full_test_short_name;
    }

    public String getFull_test_long_name() {
        return full_test_long_name;
    }

    public void setFull_test_long_name(String full_test_long_name) {
        this.full_test_long_name = full_test_long_name;
    }

    public String getFull_test_no_of_qstn() {
        return full_test_no_of_qstn;
    }

    public void setFull_test_no_of_qstn(String full_test_no_of_qstn) {
        this.full_test_no_of_qstn = full_test_no_of_qstn;
    }

    public String getFull_test_no_of_choice() {
        return full_test_no_of_choice;
    }

    public void setFull_test_no_of_choice(String full_test_no_of_choice) {
        this.full_test_no_of_choice = full_test_no_of_choice;
    }

    public String getFull_test_instruction() {
        return full_test_instruction;
    }

    public void setFull_test_instruction(String full_test_instruction) {
        this.full_test_instruction = full_test_instruction;
    }

    public String getFull_test_type() {
        return full_test_type;
    }

    public void setFull_test_type(String full_test_type) {
        this.full_test_type = full_test_type;
    }

    public String getFull_test_pending() {
        return full_test_pending;
    }

    public void setFull_test_pending(String full_test_pending) {
        this.full_test_pending = full_test_pending;
    }

    public String getChapter_id() {
        return chapter_id;
    }

    public void setChapter_id(String chapter_id) {
        this.chapter_id = chapter_id;
    }

    public String getChapter_name() {
        return chapter_name;
    }

    public void setChapter_name(String chapter_name) {
        this.chapter_name = chapter_name;
    }

    public String getChapter_long_name() {
        return chapter_long_name;
    }

    public void setChapter_long_name(String chapter_long_name) {
        this.chapter_long_name = chapter_long_name;
    }

    public String getChapter_short_name() {
        return chapter_short_name;
    }

    public void setChapter_short_name(String chapter_short_name) {
        this.chapter_short_name = chapter_short_name;
    }

    public String getChapter_img() {
        return chapter_img;
    }

    public void setChapter_img(String chapter_img) {
        this.chapter_img = chapter_img;
    }

    public ArrayList<TestlistPojo> getTestlist_array() {
        return testlist_array;
    }

    public void setTestlist_array(ArrayList<TestlistPojo> testlist_array) {
        this.testlist_array = testlist_array;
    }

    @Override
    public int type() {
        return HEADER;
    }

    @Override
    public int sectionPosition() {
        return section;
    }
}
