package com.app.neetbook.View;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.neetbook.R;

import butterknife.ButterKnife;

public class McqHeaderView extends RelativeLayout {

    TextView content_title_tv;

    public McqHeaderView(Context context) {
        super(context);
    }

    public McqHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public McqHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public McqHeaderView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    public void setTextSize(float size) {
        content_title_tv = (TextView) findViewById(R.id.content_title_tv);
        content_title_tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
    }

}
