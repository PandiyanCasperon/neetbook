package com.app.neetbook.View.SideMenu;

import android.annotation.TargetApi;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.neetbook.Adapter.PointsStickyHeaderAdapter;
import com.app.neetbook.Adapter.sideMenuContent.SubscriptionPointsAdapter;
import com.app.neetbook.Interfaces.ArticleContentSection;
import com.app.neetbook.Model.PointsList;
import com.app.neetbook.R;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.Thread.AppExecutors;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.app.neetbook.serviceRequest.IConstant.BaseUrl;


public class SubscriptionPointsFragment extends Fragment {

    private ArrayList<Object> subscriptionPointList = new ArrayList<>();
    private SubscriptionPointsAdapter categoryAdapter;
    private RecyclerView pointList;
    private String strPointId = "", strSubjectId = "", strChapterId = "", strPointName = "", strContent = "", Url = "", CurrentPageWebUrl = "";
    Loader loader;
    private int page = 1;
    int loadMoreEnable = 0,count=0;
    private int totalSubsCount;
    SessionManager sessionManager;
    private CustomAlert customAlert;
    private TextView txtPointsContent, txtPointsTitle;
    Typeface tfBold, tfMedium, tfRegular;
    ArrayList<PointsList> pointsTitleContentList;
    List<ArticleContentSection> articleSectionArrayList = new ArrayList<>();
    private RecyclerView recyclerViewPoints;
    PointsStickyHeaderAdapter pointsStickyHeaderAdapter;
    List<Object> items = new ArrayList<>();
    AppExecutors appExecutors;
    private WebView webView;
    HashMap<String, String> header = new HashMap<String, String>();

    public SubscriptionPointsFragment(String strSubjectId, String strChapterId, String strPointId, String strPointName, String strContent, ArrayList<PointsList> pointsTitleContentList, String url,int count) {
        // Required empty public constructor
        this.strPointId = strPointId;
        this.strSubjectId = strSubjectId;
        this.strChapterId = strChapterId;
        this.strPointName = strPointName;
        this.strContent = strContent;
        this.pointsTitleContentList = pointsTitleContentList;
        this.Url = url;
        this.count=count;
    }


/*  public static SubscriptionPointsFragment newInstance() {
    SubscriptionPointsFragment fragment = new SubscriptionPointsFragment("","","","","",ArrayList<PointsList> pointsTitleContentList);
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }*/

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appExecutors = new AppExecutors();
        sessionManager = new SessionManager(getActivity());
        customAlert = new CustomAlert(getActivity());
        loader = new Loader(getActivity());
        tfBold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaRegular.otf");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_subscription_points, container, false);
        init(rootView);
        return rootView;
    }

    private void init(View rootView) {

        try {

            //  txtPointsTitle = rootView.findViewById(R.id.txtPointsTitle);
            // txtPointsContent = rootView.findViewById(R.id.txtPointsContent);
            //  recyclerViewPoints = rootView.findViewById(R.id.recyclerViewPoints);
            webView = rootView.findViewById(R.id.webView);
            webView.getSettings().setJavaScriptEnabled(true); // enable javascript

            if(count == 0)
            {
                webView.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                webView.requestLayout();
                webView.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent event) {
                        return(event.getAction() == MotionEvent.ACTION_MOVE);
                    }
                });

            }
            else
            {
                webView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                webView.requestLayout();
                webView.setOnTouchListener(null);
            }

            webView.setWebViewClient(new WebViewClient() {
                @SuppressWarnings("deprecation")
                @Override
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

                }

                @TargetApi(Build.VERSION_CODES.M)
                @Override
                public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                    // Redirect to deprecated method, so you can use it in all SDK versions
                    onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
                }
            });

            CurrentPageWebUrl = BaseUrl + Url + "?mode=";
            if (sessionManager.isNightModeEnabled())
                CurrentPageWebUrl += "dark" + "&size=" + sessionManager.getFontSize();
            else

                CurrentPageWebUrl += "" + "&size=" + sessionManager.getFontSize();
            webView.setHorizontalScrollBarEnabled(true);
            webView.loadUrl(CurrentPageWebUrl);

            System.out.println("url loading start");

          /*  webView.setWebViewClient(new WebViewClient() {

                // Handle API until level 21
                @SuppressWarnings("deprecation")
                @Override
                public WebResourceResponse shouldInterceptRequest(WebView view, String url) {

                    return getNewResponse(url);
                }

                // Handle API 21+
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {

                    String url = request.getUrl().toString();

                    return getNewResponse(url);
                }

                private WebResourceResponse getNewResponse(String url) {

                    try {
                        OkHttpClient httpClient = new OkHttpClient();

                        Request request = new Request.Builder()
                                .url(url.trim())
                                .addHeader("userId", "5f7f099957f5ea75437e021d") // Example header
                                .addHeader("authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIwMTAwODYwIDogM2E3MmY2YjE2ZGJhMjY2MyIsImlhdCI6MTYwNDQ5MDAzNX0.g-Sh-t53f7YJxhIM1nzT6pxHcrEaGixkOMVFWewyDj4") // Example header
                                .build();

                        Response response = httpClient.newCall(request).execute();

                        assert response.body() != null;
                        System.out.println("url loading midle");
                        return new WebResourceResponse(
                                null,
                                response.header("content-encoding", "utf-8"),
                                response.body().byteStream()
                        );



                    } catch (Exception e) {
                        return null;
                    }

                }
            });*/


            System.out.println("url loading end");

            //webView.setWebViewClient(wvc1);
          //  webView.loadUrl(CurrentPageWebUrl);
            //  webView.loadDataWithBaseURL(CurrentPageWebUrl, null, "text/html", "utf-8", null);



            /*txtPointsTitle.setVisibility(strChapterId.equals("nill")?View.GONE:View.VISIBLE);*/
     /* txtPointsTitle.setText(strPointName);
      if (pointsTitleContentList == null || pointsTitleContentList.size() == 0) {

        if(strContent!=null&&!strContent.equals("")){

          DataLoadWebview(strContent);
         // txtPointsContent.setText(Html.fromHtml(strContent), TextView.BufferType.SPANNABLE);

        }

        txtPointsContent.setVisibility(View.GONE);
        webView.setVisibility(View.VISIBLE);
        recyclerViewPoints.setVisibility(View.GONE);
      }else
      {
        txtPointsContent.setVisibility(View.GONE);
        webView.setVisibility(View.GONE);
        recyclerViewPoints.setVisibility(View.VISIBLE);
        setPointsList();
      }

      setFontSize();*/

        } catch (Exception e) {

        }

    }


    WebViewClient wvc1 = new WebViewClient() {
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {

            try {
                DefaultHttpClient client = new DefaultHttpClient();

                HttpGet httpGet = new HttpGet(url);
                httpGet.setHeader("userId", "5f7f099957f5ea75437e021d");
                httpGet.setHeader("authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIwMTAwODYwIDogM2E3MmY2YjE2ZGJhMjY2MyIsImlhdCI6MTYwNDQ5MDAzNX0.g-Sh-t53f7YJxhIM1nzT6pxHcrEaGixkOMVFWewyDj4");

                HttpResponse httpReponse = client.execute(httpGet);

                Header contentType = httpReponse.getEntity().getContentType();
                Header encoding = httpReponse.getEntity().getContentEncoding();
                InputStream responseInputStream = httpReponse.getEntity().getContent();

                String contentTypeValue = null;
                String encodingValue = null;
                if (contentType != null) {
                    contentTypeValue = contentType.getValue();
                }
                if (encoding != null) {
                    encodingValue = encoding.getValue();
                }
                return new WebResourceResponse(contentTypeValue, encodingValue, responseInputStream);
            } catch (ClientProtocolException e) {

                return null;
            } catch (IOException e) {
                return null;
            }
        }
    };


    public WebViewClient wvc = new WebViewClient() {

        @SuppressWarnings("deprecation")
        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
            try {


                OkHttpClient okHttpClient = new OkHttpClient();
                Request request = new Request.Builder().url(url)
                        .addHeader("userId", "5f7f099957f5ea75437e021d")
                        .addHeader("authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIwMTAwODYwIDogM2E3MmY2YjE2ZGJhMjY2MyIsImlhdCI6MTYwNDQ5MDAzNX0.g-Sh-t53f7YJxhIM1nzT6pxHcrEaGixkOMVFWewyDj4")
                        .build();

                Response response = okHttpClient.newCall(request).execute();
                return new WebResourceResponse(response.header("content-encoding", response.body().contentType().type()), // You can set something other as default content-type
                        response.header("content-encoding", "utf-8"),  // Again, you can set another encoding as default
                        response.body().byteStream());


            } catch (Exception e) {
                //return null to tell WebView we failed to fetch it WebView should try again.
                return null;
            }
        }
    };



/*  private void DataLoadWebview(final String strContent){

    try{

      if(sessionManager.isNightModeEnabled()){

        appExecutors.mainThread().execute(new Runnable() {
          @RequiresApi(api = Build.VERSION_CODES.M)
          @Override
          public void run() {

            String font_size= "14";

            if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {

              font_size= "14";

            }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {

              font_size= "15";

            }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {

              font_size= "16";

            }

            String finalHtml = "<html><head>"
                    + "<style type=\"text/css\">li{color: #fff} span {color: #000}"
                    + "</style></head>"
                    +"<body style='font-size:"+font_size+"px"+"'>"+"<font color='white'>"
                    + strContent+"</font>"
                    + "</body></html>";

           // webView.loadData(finalHtml,"text/html", "UTF-8");
            webView.loadDataWithBaseURL(null,finalHtml,"text/html", "UTF-8",null);
            webView.setBackgroundColor(getActivity().getColor(R.color.night_color_new));
          }
        });

      } else {

        appExecutors.mainThread().execute(new Runnable() {
          @RequiresApi(api = Build.VERSION_CODES.M)
          @Override
          public void run() {

            String font_size= "14";

            if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {

              font_size= "14";

            }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {

              font_size= "15";

            }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {

              font_size= "16";

            }

            String finalHtml = "<html><head>"
                    + "<style type=\"text/css\">li{color: #000} span {color: #000}"
                    + "</style></head>"
                    + "<body style='font-size:"+font_size+"px"+"'>"+"<font color='black'>"
                    + strContent+ "</font>"
                    + "</body></html>";

           // webView.loadData(finalHtml,"text/html", "UTF-8");
            webView.loadDataWithBaseURL(null,finalHtml,"text/html", "UTF-8",null);
            webView.setBackgroundColor(getActivity().getColor(R.color.white_color_new));

          }
        });

      }

    }catch(Exception e){

      Log.e("Exception",e.toString());
    }
  }

  private void setPointsList() {
    items.clear();
    for(int i = 0; i<pointsTitleContentList.size();i++)
    {
      items.add(new ArticleItem(pointsTitleContentList.get(i).getStrTitile(),pointsTitleContentList.get(i).getStrBody()));
      items.add(new ArticleChildItem(pointsTitleContentList.get(i).getStrTitile(),"No",pointsTitleContentList.get(i).getStrBody(),"",i,null,false));

    }
    pointsStickyHeaderAdapter = new PointsStickyHeaderAdapter();
    pointsStickyHeaderAdapter.setDataList(items,getActivity());
    StickyLinearLayoutManager layoutManager = new StickyLinearLayoutManager(getActivity(), pointsStickyHeaderAdapter) {
      @Override
      public boolean isAutoMeasureEnabled() {
        return true;
      }

      @Override
      public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
        RecyclerView.SmoothScroller smoothScroller = new TopSmoothScroller(recyclerView.getContext());
        smoothScroller.setTargetPosition(position);
        startSmoothScroll(smoothScroller);
      }

      class TopSmoothScroller extends LinearSmoothScroller {

        TopSmoothScroller(Context context) {
          super(context);
        }

        @Override
        public int calculateDtToFit(int viewStart, int viewEnd, int boxStart, int boxEnd, int snapPreference) {
          return boxStart - viewStart;
        }
      }
    };
    layoutManager.elevateHeaders(5);
    recyclerViewPoints.setLayoutManager(layoutManager);
    recyclerViewPoints.setAdapter(pointsStickyHeaderAdapter);
    layoutManager.setStickyHeaderListener(new StickyLinearLayoutManager.StickyHeaderListener() {
      @Override
      public void headerAttached(View headerView, int adapterPosition) {
        Log.d("StickyHeader", "Header Attached : " + adapterPosition);
      }

      @Override
      public void headerDetached(View headerView, int adapterPosition) {
        Log.d("StickyHeader", "Header Detached : " + adapterPosition);
      }
    });
  }

  private void setFontSize() {

    if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
      txtPointsTitle.setTextAppearance(getActivity(),R.style.textViewSmallContentTitle);
      txtPointsContent.setTextAppearance(getActivity(),R.style.textViewSmallArticleContent);
    }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
      txtPointsTitle.setTextAppearance(getActivity(),R.style.textViewMediumContentTitle);
      txtPointsContent.setTextAppearance(getActivity(),R.style.textViewMediumArticleContent);
    }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
      txtPointsTitle.setTextAppearance(getActivity(),R.style.textViewLargeContentTitle);
      txtPointsContent.setTextAppearance(getActivity(),R.style.textViewLargeArticleContent);
    }

    txtPointsTitle.setTypeface(tfMedium);
    txtPointsContent.setTypeface(tfRegular);
  }


  public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {

    private int mItemOffset;

    public ItemOffsetDecoration(int itemOffset) {
      mItemOffset = itemOffset;
    }

    public ItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
      this(context.getResources().getDimensionPixelSize(itemOffsetId));
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
        RecyclerView.State state) {
      super.getItemOffsets(outRect, view, parent, state);
      if (parent.getChildAdapterPosition(view) == 0) {
        outRect.top = mItemOffset;
      }
      outRect.left = getContext().getResources().getDimensionPixelSize(R.dimen._16sdp);
      outRect.right = mItemOffset;
      outRect.bottom = mItemOffset;
    }
  }*/



    private WebViewClient getWebViewClient() {

        return new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(request.getMethod(), sessionManager.getApiHeader());
                return true;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url, sessionManager.getApiHeader());
                return true;
            }
        };
    }


/*    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String upordown) {
     if (upordown.equals("hideme"))
        {
            //Toast.makeText(getActivity(),"grtg",Toast.LENGTH_SHORT).show();
            webView.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    return(event.getAction() == MotionEvent.ACTION_MOVE);
                }
            });

        }
        else if (upordown.equals("showme"))
        {

            webView.setOnTouchListener(null);
        }

    }*/


    @Override
    public void onResume() {
        super.onResume();
       /* if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }*/
    }


}
