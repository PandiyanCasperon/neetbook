package com.app.neetbook.View;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.DatabaseErrorHandler;
import android.os.AsyncTask;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.textclassifier.TextLinks;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.app.neetbook.CartRoom.CartDataBase;
import com.app.neetbook.Model.Groups;
import com.app.neetbook.Model.Groupslist;
import com.app.neetbook.Model.TabSideMenu;
import com.app.neetbook.Model.subscription.SubscriptionCart;
import com.app.neetbook.R;
import com.app.neetbook.Subscriptionroom.SubscriptionGroupDatabase;
import com.app.neetbook.Subscriptionroom.SubscriptionGroups;
import com.app.neetbook.Utils.ActionBarActivityNeetBook;
import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Data.DrawerActivityData;
import com.app.neetbook.Utils.Data.EnterPinData;
import com.app.neetbook.Utils.LogOutTimerUtil;
import com.app.neetbook.Utils.ResizeAnimation;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.Utils.ThreadExecutor.AppExecutors;
import com.app.neetbook.Utils.alerter.AlertBox;
import com.app.neetbook.View.Fragments.TabletHomeSideGroupMenu;
import com.app.neetbook.homepageRoom.GroupslistDatabase;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class EnterPin extends AppCompatActivity {

    private int launch_request_code=100;

    ConstraintLayout constrainLeft, constrainRight;
    private TextView img2, img1, img3, img5, img4, img6, img8, img7, img9, img0, img_hash;
    int[] imageViews = {R.id.img1, R.id.img2, R.id.img3, R.id.img4, R.id.img5, R.id.img6, R.id.img7, R.id.img8, R.id.img9, R.id.img0};
    private String number = "", edt_email = "";
    ImageView imgLogoLand, imgLogoPortrait;
    TextView txtNameLand, txtPortrait, imgforgot;
    TextView txtPin1, txtPin2, txtPin3, txtPin4;
    SessionManager sessionManager;
    public static EnterPin enterPin;
    public static String IsShowing = "false";
    public static String currentView = "No";
    ConnectionDetector cd;
    private AppExecutors exector;
    String result1 = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (getResources().getBoolean(R.bool.isTablet)) {
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }  else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.neetpincode);
        IsShowing = "false";//Set false for background and foreground check
        currentView = "Yes";
        ActionBarActivityNeetBook.isSessionOut = "No";
        cd = new ConnectionDetector(EnterPin.this);
        sessionManager = new SessionManager(this);
        if(cd.isConnectingToInternet())
            initView();
        else{
            startActivity(new Intent(EnterPin.this,NoInterNetActivity.class));
            overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
            finish();
        }
    }

    private void initView() {

        exector=new AppExecutors();

        new AsyncGroupRunner().execute();
        new AsyncSubscriptionRunner().execute();
        if(sessionManager.getMilliseconds() != null && !sessionManager.getMilliseconds().equals("0"))
            new AsyncUpdateSessionRunner("1").execute();

        //new AsyncCartRunner().execute();
        enterPin = this;
        txtPin1 = findViewById(R.id.txtPin1);
        txtPin2 = findViewById(R.id.txtPin2);
        txtPin3 = findViewById(R.id.txtPin3);
        txtPin4 = findViewById(R.id.txtPin4);
        imgforgot = findViewById(R.id.imgforgot);
        txtPortrait = findViewById(R.id.txtName);
        txtNameLand = findViewById(R.id.txtNameLand);
        imgLogoLand = findViewById(R.id.imgLogoLand);
        imgLogoPortrait = findViewById(R.id.imgLogoPortrait);
        constrainLeft = findViewById(R.id.constrainLeft);
        constrainRight = findViewById(R.id.constrainRight);

        if (getResources().getBoolean(R.bool.isTablet))
            setOrientationView();
        else
            txtPortrait.setText((sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) != null && !sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("nill")&& !sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equalsIgnoreCase("")) ? getString(R.string.hi) + ", " + sessionManager.getUserDetails().get(SessionManager.KEY_FNAME)+" "+sessionManager.getUserDetails().get(SessionManager.KEY_LNAME) :  getString(R.string.hi_comma) +" "+sessionManager.getUserDetails().get(SessionManager.KEY_EMAIL));
        img2 = findViewById(R.id.img2);
        img1 = findViewById(R.id.img1);
        img3 = findViewById(R.id.img3);
        img5 = findViewById(R.id.img5);
        img4 = findViewById(R.id.img4);
        img6 = findViewById(R.id.img6);
        img8 = findViewById(R.id.img8);
        img7 = findViewById(R.id.img7);
        img9 = findViewById(R.id.img9);
        img0 = findViewById(R.id.img0);
        img_hash = findViewById(R.id.img_hash);

        img1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setbackground(img1.getId());
                return false;
            }
        });
        img2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setbackground(img2.getId());
                return false;
            }
        });
        img3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setbackground(img3.getId());
                return false;
            }
        });
        img4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setbackground(img4.getId());
                return false;
            }
        });
        img5.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setbackground(img5.getId());
                return false;
            }
        });
        img6.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setbackground(img6.getId());
                return false;
            }
        });
        img7.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setbackground(img7.getId());
                return false;
            }
        });
        img8.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setbackground(img8.getId());
                return false;
            }
        });
        img9.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setbackground(img9.getId());
                return false;
            }
        });
        img0.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setbackground(img0.getId());
                return false;
            }
        });

        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_email.length() < 5) {
                    number = number + "1";
                    edt_email = number;
                    setPin("add");
                }

            }
        });
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_email.length() < 5) {
                    number = number + "2";
                    edt_email = number;
                    setbackground(img2.getId());
                    setPin("add");
                }
            }
        });
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_email.length() < 5) {
                    number = number + "3";
                    edt_email = number;
                    setbackground(img3.getId());
                    setPin("add");
                }

            }
        });
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_email.length() < 5) {
                    number = number + "4";
                    edt_email = number;
                    setbackground(img4.getId());
                    setPin("add");
                }


            }
        });
        img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_email.length() < 5) {
                    number = number + "5";
                    edt_email = number;
                    setbackground(img5.getId());
                    setPin("add");
                }

            }
        });

        img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_email.length() < 5) {
                    number = number + "6";
                    edt_email = number;
                    setbackground(img6.getId());
                    setPin("add");
                }
            }
        });
        img7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_email.length() < 5) {
                    number = number + "7";
                    edt_email = number;
                    setbackground(img7.getId());
                    setPin("add");
                }
            }
        });
        img8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_email.length() < 5) {
                    number = number + "8";
                    edt_email = number;
                    setbackground(img8.getId());
                    setPin("add");
                }

            }
        });
        img9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_email.length() < 5) {
                    number = number + "9";
                    edt_email = number;
                    setbackground(img9.getId());
                    setPin("add");
                }

            }
        });

        img0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_email.length() < 5) {
                    number = number + "0";
                    edt_email = number;
                    setbackground(img0.getId());
                    setPin("add");
                }

            }
        });

        img_hash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPin("del");
                String SMobileNo = edt_email;
                if (SMobileNo != null && SMobileNo.length() > 0) {
                    edt_email = SMobileNo.substring(0, SMobileNo.length() - 1);
                    number = edt_email;
                }

                // img_hash.setBackground(getResources().getDrawable(R.drawable.roundshadowneetcolor));
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        // img_hash.setBackground(getResources().getDrawable(R.drawable.roundshadow));
                    }
                }, 100);
            }
        });

        imgforgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signout();
            }
        });

    }

    private void signout() {
        EnterPinData.clearData();
        new CustomAlert(EnterPin.this).showAlertLogout(getString(R.string.alert_alert), getString(R.string.logout_alert));

    }


    private void setOrientationView() {
        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int rotation = display.getRotation();
        Log.e("rotation", rotation + "");
        if (rotation == 1 || rotation == 3) {
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    0,
                    ConstraintLayout.LayoutParams.MATCH_PARENT,
                    1.0f
            );
            constrainLeft.setLayoutParams(param);
            constrainRight.setLayoutParams(param);
            txtPortrait.setVisibility(View.GONE);
            txtNameLand.setVisibility(View.VISIBLE);
            imgLogoPortrait.setVisibility(View.GONE);
            imgLogoLand.setVisibility(View.VISIBLE);
            txtNameLand.setText((sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) != null && !sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("nill") &&!sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equalsIgnoreCase("")) ? getString(R.string.hi) + ", " + sessionManager.getUserDetails().get(SessionManager.KEY_FNAME)+" "+sessionManager.getUserDetails().get(SessionManager.KEY_LNAME) : getString(R.string.hi_comma)+" " +sessionManager.getUserDetails().get(SessionManager.KEY_EMAIL));
            constrainLeft.setBackground(getResources().getDrawable(R.drawable.enter_pin_landscape_gradient));

            if (!EnterPinData.enteredPin.equals("") && EnterPinData.enteredPin.length() > 0) {
                setPinValue();
            }
        } else if (getResources().getBoolean(R.bool.isTablet)) {
            LinearLayout.LayoutParams paramleft = new LinearLayout.LayoutParams(
                    0,
                    ConstraintLayout.LayoutParams.MATCH_PARENT,
                    0.17f
            );
            LinearLayout.LayoutParams paramRight = new LinearLayout.LayoutParams(
                    0,
                    ConstraintLayout.LayoutParams.MATCH_PARENT,
                    0.83f
            );
            constrainLeft.setLayoutParams(paramleft);
            constrainRight.setLayoutParams(paramRight);
            txtPortrait.setVisibility(View.VISIBLE);
            txtPortrait.setText((sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) != null && !sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("nill") &&!sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equalsIgnoreCase("") )? getString(R.string.hi) + ", " + sessionManager.getUserDetails().get(SessionManager.KEY_FNAME)+" "+sessionManager.getUserDetails().get(SessionManager.KEY_LNAME): getString(R.string.hi_comma)+" " +sessionManager.getUserDetails().get(SessionManager.KEY_EMAIL));
            txtNameLand.setVisibility(View.GONE);
            imgLogoPortrait.setVisibility(View.VISIBLE);
            imgLogoLand.setVisibility(View.GONE);
            constrainLeft.setBackground(getResources().getDrawable(R.drawable.enter_pin_protrait_gratient));
            if (!EnterPinData.enteredPin.equals("") && EnterPinData.enteredPin.length() > 0) {
                setPinValue();
            }
        }

    }

    private void setPinValue() {
        number = EnterPinData.enteredPin;
        edt_email = EnterPinData.enteredPin;
        for (int i = 0; i < EnterPinData.enteredPin.length(); i++) {
            switch (i) {
                case 0:
                    expandOrCollapse(txtPin1, "add");
                    break;

                case 1:
                    expandOrCollapse(txtPin2, "add");
                    break;

                case 2:
                    expandOrCollapse(txtPin3, "add");
                    break;

            }
        }
    }

    private void checkPin() {
        SessionManager sessionManager = new SessionManager(EnterPin.this);
        Log.e("saved Pin", sessionManager.getPin() + ", entered Pin : " + edt_email);
        if (sessionManager.getPin() != null && sessionManager.getPin().equals(edt_email)) {
            EnterPinData.enteredPin = "";
            if(getIntent().getExtras() != null && getIntent().getExtras().containsKey("Background") && getIntent().getStringExtra("Background").equalsIgnoreCase("Yes"))
                finish();
            else{
                DrawerActivityData.isFirstTimeLoading = true;
                startActivity(new Intent(EnterPin.this, DrawerContentSlideActivity.class));
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                }
            finish();
        } else
            AlertBox.showSnackBoxPrimaryBa(EnterPin.this, getString(R.string.alert_alert), getString(R.string.INVALID_PIN));
        refreshPin();
    }

    private void refreshPin() {
        edt_email = "";
        number = "";
        expandOrCollapse(txtPin1, "del");
        expandOrCollapse(txtPin2, "del");
        expandOrCollapse(txtPin3, "del");
        expandOrCollapse(txtPin4, "del");
    }

    private void setPin(String action) {
        if (edt_email != null && !edt_email.equals("")) {
            switch (edt_email.length()) {
                case 1: {
                    expandOrCollapse(txtPin1, action);


                }
                break;

                case 2: {
                    expandOrCollapse(txtPin2, action);

                }
                break;

                case 3: {
                    expandOrCollapse(txtPin3, action);

                }
                break;

                case 4: {
                    expandOrCollapse(txtPin4, action);
                    checkPin();

                }
                break;

                default:
                    if (edt_email.length() > 4) {

                    }

            }
        }
    }


    private void setbackground(int image) {
        for (int i = 0; i <= imageViews.length - 1; i++) {
            final TextView img = findViewById(imageViews[i]);
            if (image == imageViews[i]) {
                img.setBackground(getResources().getDrawable(R.drawable.roundshadowneetcolor));
                img.setTextColor(getResources().getColor(R.color.white));
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        img.setTextColor(getResources().getColor(R.color.textneetcolor));
                        img.setBackground(getResources().getDrawable(R.drawable.roundshadow));
                    }
                }, 100);
            } else {
                img.setTextColor(getResources().getColor(R.color.textneetcolor));
                img.setBackground(getResources().getDrawable(R.drawable.roundshadow));
            }
        }


    }

    public void expandOrCollapse(final View v, String exp_or_colpse) {

        if (exp_or_colpse.equals("add")) {
            if (getResources().getBoolean(R.bool.isTablet))
                ResizeAnimation.expand(v, 10, 10, 60, 10);
            else
                ResizeAnimation.expand(v, 10, 10, 80, 10);
        } else {
            if (getResources().getBoolean(R.bool.isTablet))
                ResizeAnimation.expand(v, v.getLayoutParams().width, v.getLayoutParams().height, 13, 13);
            else
                ResizeAnimation.expand(v, v.getLayoutParams().width, v.getLayoutParams().height, 18, 18);
        }

    }

    public void onDestroy() {
        super.onDestroy();
        EnterPinData.enteredPin = edt_email;
        currentView = "No";
        Log.e("Entered Pin", EnterPinData.enteredPin);
    }

    private class AsyncGroupRunner extends AsyncTask<String, String, String> {
        GroupslistDatabase groupslistDatabase = GroupslistDatabase.getAppDatabase(getApplicationContext());
        String result = "";
        List<Groupslist> groupslists = new ArrayList<>();

        @Override
        protected String doInBackground(String... strings) {
            ServiceRequest mRequest = new ServiceRequest(EnterPin.this);
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
            params.put("page_type","1");
            params.put("page","1");
            mRequest.makeServiceRequest(IConstant.groupList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("------------groupList Response----------------" + response);
                    String sStatus = "";
                    try {

                        JSONObject object = new JSONObject(response);
                        sStatus = object.getString("status");

                        groupslistDatabase.groupslistDao().nukeTable();
                        if (sStatus.equalsIgnoreCase("1")) {
                            groupslists.clear();
                            JSONObject jsonObject = new JSONObject(response);
                            final JSONArray result = jsonObject.getJSONObject("response").getJSONArray("groupslist");
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    TabSideMenu tabSideMenu = new TabSideMenu();
                                    tabSideMenu.setName(result.getJSONObject(i).getString("g_name"));
                                    tabSideMenu.setShortName(result.getJSONObject(i).getString("short_name"));
                                    tabSideMenu.setLongName(result.getJSONObject(i).getString("long_name"));
                                    tabSideMenu.setId(result.getJSONObject(i).getString("_id"));

                                    Groupslist groupslist = new Groupslist();
                                    groupslist.setHomegroupid(result.getJSONObject(i).getString("_id"));
                                    groupslist.setHomegroupname(result.getJSONObject(i).getString("g_name"));
                                    groupslist.setHomeshortName(result.getJSONObject(i).getString("short_name"));
                                    groupslist.setHomelongName(result.getJSONObject(i).getString("long_name"));

                                    Groupslist subscriptionCart = groupslistDatabase.groupslistDao().getGroupItem(result.getJSONObject(i).getString("_id"));
                                    if(subscriptionCart != null && subscriptionCart.getHomegroupid() != null && !subscriptionCart.getHomegroupid().isEmpty() && subscriptionCart.getHomegroupid().equalsIgnoreCase(result.getJSONObject(i).getString("_id")))
                                        groupslistDatabase.groupslistDao().update(groupslist);
                                    else {
                                        groupslistDatabase.groupslistDao().insertAll(groupslist);
                                    }
//                                    Groupslist groupslist = new Groupslist(result.getJSONObject(i).getString("_id"),
//                                            result.getJSONObject(i).getString("g_name"),
//                                            result.getJSONObject(i).getString("short_name"),
//                                            result.getJSONObject(i).getString("long_name"));
//                                    groupslists.add(groupslist);
                                }

                                try {

//                                    exector.diskIO().execute(new Runnable() {
//                                        @Override
//                                        public void run() {
//
//                                            Groups groupslist = new Groups(groupslists);
//                                            groupslistDatabase.groupslistDao().insertGroupsList(groupslist);
//                                        }
//                                    });

                                } catch (Exception e) {

                                    System.out.println("Exception---" + e.toString());

                                }
                            }




                        } else {
                        }
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                }

                @Override
                public void onErrorListener(String errorMessage) {
                    // AlertBox.showSnackBoxPrimaryBa(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
                }
            });
            return result;


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            // new AsyncSubjectsRunner(s).execute();
        }
    }
    private class AsyncSubscriptionRunner extends AsyncTask<String, String, String> {
        SubscriptionGroupDatabase groupslistDatabase = SubscriptionGroupDatabase.getAppDatabase(getApplicationContext());

        List<Groupslist> groupslists = new ArrayList<>();

        @Override
        protected String doInBackground(String... strings) {
            ServiceRequest mRequest = new ServiceRequest(EnterPin.this);
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
            params.put("page_type","2");
            params.put("page","1");
            mRequest.makeServiceRequest(IConstant.groupList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("------------AsyncSubscriptionRunner Response----------------" + response);
                    String sStatus = "";
                    try {

                        JSONObject object = new JSONObject(response);
                        sStatus = object.getString("status");
                        result1 = sStatus;
                        groupslistDatabase.subscriptionGroupDao().nukeTable();

                        if (sStatus.equalsIgnoreCase("1")) {
                            groupslists.clear();
                            JSONObject jsonObject = new JSONObject(response);
                            final JSONArray result = jsonObject.getJSONObject("response").getJSONArray("groupslist");
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    SubscriptionGroups tabSideMenu = new SubscriptionGroups();
                                    tabSideMenu.setGroupname(result.getJSONObject(i).getString("g_name"));
                                    tabSideMenu.setShortName(result.getJSONObject(i).getString("short_name"));
                                    tabSideMenu.setLongName(result.getJSONObject(i).getString("long_name"));
                                    tabSideMenu.setGroupid(result.getJSONObject(i).getString("_id"));

                                    SubscriptionGroups subscriptionCart = groupslistDatabase.subscriptionGroupDao().getGroupItem(result.getJSONObject(i).getString("_id"));
                                    if(subscriptionCart != null && subscriptionCart.getGroupid() != null && !subscriptionCart.getGroupid().isEmpty() && subscriptionCart.getGroupid().equalsIgnoreCase(result.getJSONObject(i).getString("_id")))
                                        groupslistDatabase.subscriptionGroupDao().update(tabSideMenu);
                                    else {
                                        groupslistDatabase.subscriptionGroupDao().insertAll(tabSideMenu);
                                    }
                                }

                                try {



                                } catch (Exception e) {

                                    System.out.println("Exception---" + e.toString());

                                }
                            }




                        } else {
                        }
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                }

                @Override
                public void onErrorListener(String errorMessage) {
                    // AlertBox.showSnackBoxPrimaryBa(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
                }
            });
            return result1;


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            // new AsyncSubjectsRunner(s).execute();
        }
    }
    private void getUserProfileData() {

        ServiceRequest mRequest = new ServiceRequest(EnterPin.this);
        HashMap<String,String> params = new HashMap<>();
        params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));

        mRequest.makeServiceRequest(IConstant.getUserProfileData, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------getUserProfileData Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1")) {


                    }  else if(sStatus.equals("00")){

                        new CustomAlert(EnterPin.this).singleLoginAlertLogout();
                    } else if(sStatus.equalsIgnoreCase("01")){
                        new CustomAlert(EnterPin.this).UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    }else {
                        new CustomAlert(EnterPin.this).showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {

                try {
                   // AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), errorMessage);
                }catch ( Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

    }
    private class AsyncUpdateSessionRunner extends AsyncTask<String, String, String> {
        String result = "";
        String callingType = "";

        public AsyncUpdateSessionRunner(String s) {
            callingType = s;
        }

        @Override
        protected String doInBackground(String... strings) {
            ServiceRequest mRequest = new ServiceRequest(getApplicationContext());
            HashMap<String, String> params = new HashMap<>();
            params.put("session_id", sessionManager.getKeyPackageId());

            params.put("time", sessionManager.getMilliseconds());
            params.put("close","1");

            //Log.e("startingLevelTime",""+sessionManager.getMilliseconds()+" , packageTime : "+packageTime);
            //startingLevelTime = packageTime;
            mRequest.makeServiceRequest(IConstant.updateSession, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("updateSession"+"-------- Response----------------" + response);
                    String sStatus = "";
                    String message = "";
                    try {
                        JSONObject obj = new JSONObject(response);
                        sStatus = obj.getString("status");
                        message = obj.has("message")?obj.getString("message"):"";
                        result = sStatus;

                        if (sStatus.equalsIgnoreCase("1")) {
                            //JSONObject object = obj.getJSONObject("response");
                            sessionManager.updateTimer("0","0");

                        } else if(sStatus.equals("00")){
                            //customAlert.singleLoginAlertLogout();
                        }else if(sStatus.equalsIgnoreCase("01")){
                            //customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                        } else {
                            //customAlert.showAlertOk(getString(R.string.alert_oops),message);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onErrorListener(String errorMessage) {
                    // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
                }
            });
            return result;


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            // new AsyncSubjectsRunner(s).execute();
        }
    }
    @Override
    public void onBackPressed() {
        EnterPinData.clearData();
        if(getIntent().getExtras() != null && getIntent().getExtras().containsKey("Background") && getIntent().getStringExtra("Background").equalsIgnoreCase("Yes"))
        {
            Log.e("OnBackPressed","Blocked");
        } else {
            super.onBackPressed();
        }
    }

    public void onStart()
    {
        super.onStart();
        LogOutTimerUtil.stopLogoutTimer();
        currentView = "Yes";
    }
    public void onPause()
    {
        super.onPause();
        currentView = "No";
    }
    public void onStop()
    {
        super.onStop();

        currentView = "No";
    }
    public void onResume()
    {
        super.onResume();

        getUserProfileData();
    }
}
