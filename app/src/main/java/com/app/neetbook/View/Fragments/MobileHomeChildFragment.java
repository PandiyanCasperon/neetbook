package com.app.neetbook.View.Fragments;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.constraintlayout.widget.Guideline;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.neetbook.Adapter.HomeGroupReferNowAdapter;
import com.app.neetbook.Adapter.MobilehomeGroupViewAdapter;
import com.app.neetbook.Adapter.sideMenuContent.SubscriptionCategoryArticlesAdapter;
import com.app.neetbook.Interfaces.OnSubjectsClickListener;
import com.app.neetbook.Interfaces.Updateable;
import com.app.neetbook.Model.GroupChildBean;
import com.app.neetbook.Model.Groups;
import com.app.neetbook.Model.Groupslist;
import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.ReferNowBean;
import com.app.neetbook.Model.TabSideMenu;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ApplicationLifecycleHandler;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Data.DrawerActivityData;
import com.app.neetbook.Utils.Data.GroupMenuData;
import com.app.neetbook.Utils.Data.SubscriptionFragData;
import com.app.neetbook.Utils.EventBusPojo.ActionEvent;
import com.app.neetbook.Utils.ItemOffsetDecoration;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.SnapToBlock;
import com.app.neetbook.Utils.ThreadExecutor.AppExecutors;
import com.app.neetbook.Utils.widgets.DisplayOrientation;
import com.app.neetbook.View.DrawerContentSlideActivity;
import com.app.neetbook.View.EnterPin;
import com.app.neetbook.View.MobilePagesActivity;
import com.app.neetbook.View.SideMenu.ArticleFragment;
import com.app.neetbook.View.SideMenu.SubscriptionArticleActivity;
import com.app.neetbook.View.SideMenu.SubscriptionCategoryActivity;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.app.neetbook.View.TabletHome;
import com.app.neetbook.homepageRoom.GroupslistDatabase;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MobileHomeChildFragment extends Fragment implements Updateable {

    private int sectionNumber;
    String ARG_PARAM = "section";
    RecyclerView mobileHomeRecyclerViewVertical;
    ItemOffsetDecoration itemDecoration;
    ArrayList<GroupChildBean> groupList;
    MobilehomeGroupViewAdapter mobilehomeGroupViewAdapter;
    Guideline guide1;
    boolean isFirstTimeLoading = true;
    Groupslist groupslist;
    SessionManager sessionManager;
    private AppExecutors appexecutor;
    private int skip = 0;
    private int page = 1;
    private int limit = 10;
    String strGroupId = "";
    private int totalSubsCount = 0;
    int l = 0;
    LinearLayout llEmpty;
    ShimmerFrameLayout mShimmerViewContainer ;
    private LinearLayout place_holder_layout;
    private CustomAlert customAlert;
    SwipeRefreshLayout mSwipeRefreshLayout;
    int loadMoreEnable = 0;
    Loader loader;
    LinearSnapHelper snapHelper;
    private int populatedList = 0;

    public MobileHomeChildFragment(String id,Context context) {
        strGroupId = id;
    }

     public MobileHomeChildFragment() {

        }


//    public static MobileHomeChildFragment newInstance(int position) {
//
//        MobileHomeChildFragment fragment = new MobileHomeChildFragment();
//        Bundle args = new Bundle();
//        args.putInt("section", position);
//        //args.putString("", param2);
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // groupslist = (Groupslist) getArguments().getSerializable("sideMenu");
        //Log.e("Group_id",groupslist.getGroupid());
        appexecutor=new AppExecutors();
        sessionManager = new SessionManager(getActivity());
        customAlert = new CustomAlert(getActivity());
        loader = new Loader(getActivity());
        createSnap();
        itemDecoration = new ItemOffsetDecoration(getActivity(), R.dimen._10sdp);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_mobile_home_child, container, false);
        mobileHomeRecyclerViewVertical = (RecyclerView) rootView.findViewById(R.id.mobileHomeRecyclerViewVertical);
        mSwipeRefreshLayout = rootView.findViewById(R.id.mSwipeRefreshLayout);
        mSwipeRefreshLayout.setEnabled(false);
        mSwipeRefreshLayout.setRefreshing(false);
        guide1 = (Guideline) rootView.findViewById(R.id.guide1);
        mShimmerViewContainer  = rootView.findViewById(R.id.mShimmerViewContainer );
        place_holder_layout=rootView.findViewById(R.id.place_holder_layout);
        mShimmerViewContainer.startShimmer();
        llEmpty  = rootView.findViewById(R.id.llEmpty );
        mobileHomeRecyclerViewVertical.setVisibility(View.GONE);
        Log.e("groupId",strGroupId);

        swipeRefresh();

        if(guide1 != null)
        {
            if(DisplayOrientation.getDisplayOrientation(getActivity()) == 1 ||DisplayOrientation.getDisplayOrientation(getActivity()) == 3 )
                guide1.setGuidelinePercent((float) .80);
            else
                guide1.setGuidelinePercent((float) .85);
        }
        //textView.setText("TAB " + sectionNumber);
        init();
        listtener();
        return rootView;
    }

    private void listtener() {


        mobileHomeRecyclerViewVertical.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager=LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();

                boolean endHasBeenReached = lastVisible + 5 >= totalItemCount;
                Log.e("totalItemCount",""+totalItemCount);
                Log.e("lastVisible",""+lastVisible);
                Log.e("endHasBeenReached",""+endHasBeenReached);
                if(lastVisible == totalItemCount-1 && totalSubsCount>groupList.size() && loadMoreEnable == 0)
                {
                    page = page+1;
                    Log.e("loadMoreSubjects()","Called");
                    loadMoreEnable = 1;
                    loadMoreSubjects(totalItemCount);
                }
                int pos = layoutManager.findFirstVisibleItemPosition();
                int lastPos = layoutManager.findLastCompletelyVisibleItemPosition();

                if(layoutManager.findViewByPosition(pos).getTop()==0 && pos==0){
                    mobileHomeRecyclerViewVertical.scrollToPosition(0);
                }

                if(lastVisible==layoutManager.getItemCount() -1  && lastPos==layoutManager.getItemCount()-1){
                    mobileHomeRecyclerViewVertical.scrollToPosition(layoutManager.getItemCount()-1);
                }

            }
        });
    }

    private void loadMoreSubjects(final int lastItemPosition) {
        ///loader.showLoader();

        appexecutor.networkIO().execute(new Runnable() {
            @Override
            public void run() {

                ServiceRequest mRequest = new ServiceRequest(getActivity());
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
                params.put("group_id",strGroupId);
                params.put("page_type","1");
                params.put("skip",""+skip);
                params.put("limit",""+limit);
                params.put("page",""+page);
                mRequest.makeServiceRequest(IConstant.subjectList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                    @Override
                    public void onCompleteListener(String response) {
                        System.out.println("------------loadMoreSubjects Response----------------" + response);
                        String sStatus = "";
                        String message = "";
                        try {

                            JSONObject obj = new JSONObject(response);
                            sStatus = obj.getString("status");
                            message = obj.has("message")?obj.getString("message"):"";

                            //loader.showLoader();
                            l=1;
                            if (sStatus.equalsIgnoreCase("1")) {
                                JSONObject object = obj.getJSONObject("response");
                                JSONArray jsonArray = object.getJSONArray("subject_list");
                                if(jsonArray.length()>0)
                                {
                                    //llEmpty.setVisibility(View.GONE);
                                    for(int i=0;i<jsonArray.length();i++)
                                    {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        GroupChildBean groupChildBean = new GroupChildBean();
                                        groupChildBean.setStrId(jsonObject.has("_id")?jsonObject.getString("_id"):"");
                                        groupChildBean.setStrTrial(jsonObject.getString("trial"));
                                        groupChildBean.setStrTitle(jsonObject.getString("subj_name"));
                                        groupChildBean.setStrGroupId(jsonObject.getString("group_id"));
                                        //groupChildBean.setStrTabImages(jsonObject.getString("tab_img"));
                                        groupChildBean.setStrImages(getResources().getBoolean(R.bool.isTablet)?jsonObject.getString("tab_img"):jsonObject.getString("mob_img"));
                                        groupChildBean.setStrLongName(jsonObject.getString("long_name"));
                                        groupChildBean.setStrShortName(jsonObject.getString("short_name"));
                                        groupChildBean.setStrChapter(jsonObject.getString("c_count"));
                                        groupChildBean.setStrTopics(jsonObject.getString("h_count"));
                                        groupChildBean.setStrMCQ(jsonObject.getString("mcq_count"));
                                        groupChildBean.setStrIsSubcribed(jsonObject.getInt("subscribed_user") == 1 ?"Yes" : "No");
                                        if(jsonObject.getJSONArray("packages").length()>0)
                                        {
                                            JSONArray jsonArray1 = jsonObject.getJSONArray("packages");
                                            for(int j=0;j<jsonArray1.length();j++)
                                            {
                                                JSONObject jsonObject1 = jsonArray1.getJSONObject(j);
                                                if(j == 0) {
                                                    groupChildBean.setStrPackageOneId(jsonObject1.getString("_id"));
                                                    groupChildBean.setStrWeekOne(jsonObject1.getString("duration_days"));
                                                    groupChildBean.setStrWeekontAmount(jsonObject1.getString("price"));
                                                }
                                                if(j == 1) {
                                                    groupChildBean.setStrPackegeTwoId(jsonObject1.getString("_id"));
                                                    groupChildBean.setStrWeekTwo(jsonObject1.getString("duration_days"));
                                                    groupChildBean.setStrWeekTwoAmount(jsonObject1.getString("price"));
                                                }
                                            }
                                        }
                                        if(jsonObject.getInt("trial") == 0 && jsonObject.getInt("subscribed_user") == 1)
                                        {
                                            JSONObject subscriptionsObj = jsonObject.getJSONObject("subscriptions");
                                            groupChildBean.setStrRemainingDays(subscriptionsObj.getString("remaining_days"));
                                            groupChildBean.setStrRemainingHours(subscriptionsObj.getString("remaining_hours"));
                                        }
                                        groupList.add(groupChildBean);
                                    }


                                    loadMoreEnable = 0;
                                    setupRecyclerView(lastItemPosition);
                                    performAdapterClick();
                                }
                            } else if(sStatus.equals("00")){
                                customAlert.singleLoginAlertLogout();
                            }else if(sStatus.equalsIgnoreCase("01")){
                                customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                            } else {
                                customAlert.showAlertOk(getString(R.string.alert_oops),message);
                            }
                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onErrorListener(String errorMessage) {
                        //loader.showLoader();
                        // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
                    }
                });

            }
        });


    }

    private void swipeRefresh() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                populateSubs();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void init() {
        if(groupList == null)
            populateSubs();

    }
    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);

        if(isFragmentVisible_ ){

        }
    }
    private void populateSubs() {
        Log.e("populateSubs","Called");

        mobileHomeRecyclerViewVertical.setVisibility(View.GONE);
        place_holder_layout.setVisibility(View.VISIBLE);

        if(strGroupId.length()>0){

            appexecutor.networkIO().execute(new Runnable() {
                @Override
                public void run() {

                    groupList = new ArrayList<GroupChildBean>();
                    ServiceRequest mRequest = new ServiceRequest(getActivity());
                    HashMap<String, String> params = new HashMap<>();
                    params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
                    params.put("group_id",strGroupId);
                    params.put("page_type","1");
                    params.put("skip",""+skip);
                    params.put("limit",""+limit);
                    params.put("page",""+page);
                    mRequest.makeServiceRequest(IConstant.subjectList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                        @Override
                        public void onCompleteListener(String response) {
                            System.out.println("------------subjectList Response----------------" + response);
                            String sStatus = "";
                            String message = "";
                            try {

                                JSONObject obj = new JSONObject(response);
                                sStatus = obj.getString("status");
                                message = obj.has("message")?obj.getString("message"):"";

                                if (sStatus.equalsIgnoreCase("1")) {
                                    JSONObject object = obj.getJSONObject("response");
                                    totalSubsCount = object.getInt("subject_count");
                                    JSONArray jsonArray = object.getJSONArray("subject_list");
                                    if(jsonArray.length()>0)
                                    {
                                        llEmpty.setVisibility(View.GONE);
                                        for(int i=0;i<jsonArray.length();i++)
                                        {
                                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                                            GroupChildBean groupChildBean = new GroupChildBean();
                                            groupChildBean.setStrId(jsonObject.has("_id")?jsonObject.getString("_id"):"");
                                            groupChildBean.setStrTrial(jsonObject.getString("trial"));
                                            groupChildBean.setStrTitle(jsonObject.getString("subj_name"));
                                            groupChildBean.setStrGroupId(jsonObject.getString("group_id"));
                                            //groupChildBean.setStrTabImages(jsonObject.getString("tab_img"));
                                            groupChildBean.setStrImages(jsonObject.getString("mob_img"));
                                            groupChildBean.setStrLongName(jsonObject.getString("long_name"));
                                            groupChildBean.setStrShortName(jsonObject.getString("short_name"));
                                            groupChildBean.setStrChapter(jsonObject.getString("c_count"));
                                            groupChildBean.setStrTopics(jsonObject.getString("h_count"));
                                            groupChildBean.setStrMCQ(jsonObject.getString("mcq_count"));
                                            groupChildBean.setStrIsSubcribed(jsonObject.getInt("subscribed_user") == 1 ?"Yes" : "No");
                                            if(jsonObject.getJSONArray("packages").length()>0)
                                            {
                                                JSONArray jsonArray1 = jsonObject.getJSONArray("packages");
                                                for(int j=0;j<jsonArray1.length();j++)
                                                {
                                                    JSONObject jsonObject1 = jsonArray1.getJSONObject(j);
                                                    if(j == 0) {
                                                        groupChildBean.setStrPackageOneId(jsonObject1.getString("_id"));
                                                        groupChildBean.setStrWeekOne(jsonObject1.getString("duration_days"));
                                                        groupChildBean.setStrWeekontAmount(jsonObject1.getString("price"));
                                                    }
                                                    if(j == 1) {
                                                        groupChildBean.setStrPackegeTwoId(jsonObject1.getString("_id"));
                                                        groupChildBean.setStrWeekTwo(jsonObject1.getString("duration_days"));
                                                        groupChildBean.setStrWeekTwoAmount(jsonObject1.getString("price"));
                                                    }
                                                }
                                            }
                                            if(jsonObject.getInt("trial") == 0 && jsonObject.getInt("subscribed_user") == 1)
                                            {
                                                JSONObject subscriptionsObj = jsonObject.getJSONObject("subscriptions");
                                                groupChildBean.setStrRemainingDays(subscriptionsObj.getString("remaining_days"));
                                                groupChildBean.setStrRemainingHours(subscriptionsObj.getString("remaining_hours"));
                                            }
                                            groupList.add(groupChildBean);
                                        }


                                        loadMoreEnable = 0;
                                        setupRecyclerView(0);

                                    }else
                                        llEmpty.setVisibility(View.VISIBLE);

                                } else if(sStatus.equals("00")){
                                    customAlert.singleLoginAlertLogout();
                                }else if(sStatus.equalsIgnoreCase("01")){
                                    customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                                } else {
                                    customAlert.showAlertOk(getString(R.string.alert_oops),message);
                                }
                            } catch (JSONException e) {

                                e.printStackTrace();
                            }

                            place_holder_layout.setVisibility(View.GONE);
                            mobileHomeRecyclerViewVertical.setVisibility(View.VISIBLE);

                        }

                        @Override
                        public void onErrorListener(String errorMessage) {
                            place_holder_layout.setVisibility(View.GONE);
                            mobileHomeRecyclerViewVertical.setVisibility(View.VISIBLE);
                            // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
                        }
                    });

                }
            });

        }




    }


    public void showAlertOOps(String title, String desc)
    {
        final Dialog alertDialog = new Dialog(getActivity());
        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(title);
        textViewDesc.setText(desc);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if(getResources().getBoolean(R.bool.isTablet)) {
                    TabletHome.PagesActivity.finish();
                    DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                    DrawerActivityData.lastSelectedMenuId = "2";
                    DrawerActivityData.currentIndex = 1;
                    Bundle bundle1 = new Bundle();
                    TabSideMenu tabSideMenu = new TabSideMenu();
                    tabSideMenu.setId("2");
                    tabSideMenu.setImage_id_acative(R.drawable.complete_proile_inactive);
                    tabSideMenu.setImage_id_inactive(R.drawable.complete_profile_active);
                    tabSideMenu.setName(getString(R.string.complete_profile));
                    bundle1.putSerializable("sideMenu", tabSideMenu);
                    DrawerActivityData.bundle = bundle1;
                    startActivity(new Intent(getActivity(), DrawerContentSlideActivity.class).putExtra("sideMenu", tabSideMenu));
                }else
                {
                    TabSideMenu tabSideMenu2 = new TabSideMenu();
                    tabSideMenu2.setId("2");
                    tabSideMenu2.setName(sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) != null && sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("")?getString(R.string.complete_profile):getString(R.string.my_profile));
                    tabSideMenu2.setImage_id_inactive(R.drawable.complete_proile_inactive);
                    tabSideMenu2.setImage_id_acative(R.drawable.complete_profile_active);
                    startActivity(new Intent(getActivity(), MobilePagesActivity.class).putExtra("sideMenu", tabSideMenu2));
                    getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                }
            }
        });
        if(!alertDialog.isShowing())
            alertDialog.show();
    }
    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }

    @Override
    public void onStop() {
        if(!getResources().getBoolean(R.bool.isTablet))
            mobileHomeRecyclerViewVertical.setVisibility(View.GONE);
        super.onStop();
    }


    private void setupRecyclerView(int lastPosition) {


        if(lastPosition>0)
        {
            mobilehomeGroupViewAdapter.notifyDataSetChanged();
        }else {
            mobilehomeGroupViewAdapter = new MobilehomeGroupViewAdapter(groupList, getActivity());
            if (getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(getActivity()) == 0 || DisplayOrientation.getDisplayOrientation(getActivity()) == 2))
                mobileHomeRecyclerViewVertical.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            else if (getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(getActivity()) == 1 || DisplayOrientation.getDisplayOrientation(getActivity()) == 3))
                mobileHomeRecyclerViewVertical.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            else if (!getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(getActivity()) == 0 || DisplayOrientation.getDisplayOrientation(getActivity()) == 2))
                mobileHomeRecyclerViewVertical.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            snapHelper.attachToRecyclerView(mobileHomeRecyclerViewVertical);
            mobileHomeRecyclerViewVertical.setAdapter(mobilehomeGroupViewAdapter);
            mobileHomeRecyclerViewVertical.scrollToPosition(lastPosition);
            mobileHomeRecyclerViewVertical.setVisibility(View.VISIBLE);
            performAdapterClick();

        }

        ActionEvent event=new ActionEvent();
        event.setAction("viewpagerload");
        EventBus.getDefault().post(event);

    }

    private void performAdapterClick() {

        mobilehomeGroupViewAdapter.setOnChildClick(new OnSubjectsClickListener() {
            @Override
            public void onSubscribed(String id, int position) {
                if(sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) != null && sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals(""))
                {
                    showAlertOOps(getString(R.string.alert_oops),"Complete your profile");

                }else {
                    Log.e("onSubscribed", groupList.get(position).getStrTitle());
                    SubscriptionFragData.currentIndex = GroupMenuData.currentSelectedMenu;
                    SubscriptionFragData.currentGroupId = groupList.get(position).getStrGroupId();

                    if (getActivity().getResources().getBoolean(R.bool.isTablet)){

                        startActivity(new Intent(getActivity(), MainSubscribeActivity.class));
                        getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                    }

                    else {
                        TabSideMenu tabSideMenu = new TabSideMenu();
                        tabSideMenu.setId("11");
                        tabSideMenu.setName(getString(R.string.subscription));
                        tabSideMenu.setImage_id_inactive(R.drawable.subscriptoin_inactive);
                        tabSideMenu.setImage_id_acative(R.drawable.subscriptoin_inactive);
                        startActivity(new Intent(getActivity(), MobilePagesActivity.class).putExtra("sideMenu", tabSideMenu));
                        getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                    }
                }
//                startActivity(new Intent(getActivity(), SubscriptionCategoryActivity.class));
            }

            @Override
            public void onRead(String id, int position) {

           //     Toast.makeText(getActivity(),"toast",Toast.LENGTH_SHORT).show();

                //getPackageDuration(groupList.get(position).getStrRemainingHours(),groupList.get(position).getStrRemainingDays());
                if(groupList.size()>0)
                     sessionManager.ReadSubjectBookmark(groupList.get(position).getStrLongName(),
                            groupList.get(position).getStrShortName(),
                           groupList.get(position).getStrTrial());
                    startActivity(new Intent(getActivity(), TabMainActivity.class)
                            .putExtra("subjectId",id)
                            .putExtra("subjectName",groupList.get(position).getStrLongName())
                            .putExtra("isTrial",groupList.get(position).getStrTrial())
                            .putExtra("subjectShortName",groupList.get(position).getStrShortName()));
                    getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
            }
            @Override
            public void onScrollEnd( int position) {
                Log.e("onScrollEnd",""+position);
            }
        });

    }

    private void getPackageDuration(String strRemainingHours, String strRemainingDays) {

        String[] times = strRemainingHours.split("Hours");
        long hour = Long.parseLong(drawDigitsFromString(times[0])) * 60 * 60 * 1000;
        long minutes = Long.parseLong(drawDigitsFromString(times[1]))* 60 * 1000;
        TabMainActivity.packageTime = hour+minutes;
        Log.e("hour",""+hour);
        Log.e("minutes",""+minutes);
        Log.e("packageTime",""+TabMainActivity.packageTime);


    }


    @Override
    public void update() {
        //populateSubs();
    }


        public String drawDigitsFromString(String strValue){
            String str = strValue.trim();
            String digits="";
            for (int i = 0; i < str.length(); i++) {
                char chrs = str.charAt(i);
                if (Character.isDigit(chrs))
                    digits = digits+chrs;
            }
            return digits;
        }
    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);
                    Log.e("position",""+position+", getItemCount : "+(layoutManager.getItemCount()-1));
                    if(position == 1 || position == layoutManager.getItemCount()-1)
                        return RecyclerView.NO_POSITION;
                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                    Log.e("targetPosition",""+targetPosition);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                return targetPosition ;
            }
        };
    }
}
