package com.app.neetbook.View.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.neetbook.R;
import com.app.neetbook.View.WalkthroughActivity;

import org.json.JSONException;

public class WalkthroughTest extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View view;
    ViewGroup container;
    LayoutInflater inflater;
    ImageView imgTable,imgGirl,imgTableLight,imgTop;
    TextView txtTabletArticleDesc,txtTabletArticle;

    public WalkthroughTest() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Display display = ((WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int rotation = display.getRotation();
        if(rotation == 1)
            view = inflater.inflate(R.layout.walkthrough_test, container, false);
        else if(getResources().getBoolean(R.bool.isTablet))
            view = inflater.inflate(R.layout.walkthrough_test_portrait, container, false);
        else
            view = inflater.inflate(R.layout.fragment_walkthrough_test, container, false);

        imgTop = view.findViewById(R.id.imgTop);
        imgGirl = view.findViewById(R.id.imgGirl);
        imgTable = view.findViewById(R.id.imgTable);
        imgTableLight = view.findViewById(R.id.imgTableLight);
        txtTabletArticle = view.findViewById(R.id.txtTabletArticle);
        txtTabletArticleDesc = view.findViewById(R.id.txtTabletArticleDesc);
        setTitleAndDesc();
        return view;
    }
    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);

        if(isFragmentVisible_){
//            img_man.setVisibility(View.INVISIBLE);
            Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
           // imgTop.startAnimation(RightSwipe);
            imgGirl.setVisibility(View.VISIBLE);
            imgTable.setVisibility(View.GONE);
            imgTableLight.setVisibility(View.GONE);
            txtTabletArticle.setVisibility(View.GONE);
            txtTabletArticleDesc.setVisibility(View.GONE);
            imgGirl.startAnimation(RightSwipe);
            imgGirl.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                    imgTable.setVisibility(View.VISIBLE);
                    imgTable.startAnimation(RightSwipe);;
                }
            },200);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                    imgTableLight.setVisibility(View.VISIBLE);
                    imgTableLight.startAnimation(RightSwipe);
                }
            },300);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                    txtTabletArticle.setVisibility(View.VISIBLE);
                    txtTabletArticle.startAnimation(RightSwipe);
                }
            },400);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                    txtTabletArticleDesc.setVisibility(View.VISIBLE);
                    txtTabletArticleDesc.startAnimation(RightSwipe);
                }
            },500);
        }else {

        }
    }
    private void setTitleAndDesc() {

        try {
            if(WalkthroughActivity.jsonWalkThrough!= null) {
                txtTabletArticle.setText(WalkthroughActivity.jsonWalkThrough.getString("heading4"));
                txtTabletArticleDesc.setText(WalkthroughActivity.jsonWalkThrough.getString("content4"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
