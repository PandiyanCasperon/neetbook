package com.app.neetbook.View.SideMenu.New.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.R;
import com.app.neetbook.View.SideMenu.New.TestFragmentpojo;
import com.app.neetbook.View.SideMenu.New.TestlistPojo;

import java.util.ArrayList;

public class ChildTestAdapter extends RecyclerView.Adapter<ChildTestAdapter.ViewHolder>{

    private ArrayList<TestlistPojo> test_child_array_list;
    private static final int LAYOUT_HEADER = 0;
    private static final int LAYOUT_CHILD = 1;
    private int currentSelectedPosition = -1;
    private int currentChileHeadSelectedPosition = -1;
    private int superChileSeletedPosition = -1;
    Context context;
    private String subjectId = "";
    private String subjectName = "";
    private String currentSelectedHeader, indexCount = "1";
    private int chileSeletedPosition = -1;
    private int index = 0;
    Typeface tfBold, tfMedium, tfRegular;
    private ItemClickListener itemClickListener;
    private String strPending, strPendingTestId;
    Dialog alertDialog;

    public ChildTestAdapter(Context context, ArrayList<TestlistPojo> test_child_array_list) {

        // inflater = LayoutInflater.from(context);
        this.context = context;
        this.test_child_array_list = test_child_array_list;
       /* this.subjectId = subjectId;
        this.subjectName = subjectName;
        this.strPending = strPending;
        this.strPendingTestId = strPendingTestId;
        this.currentSelectedPosition = currentSelectedPosition;*/
        //this.chileSeletedPosition = chileSeletedPosition;
        //currentSelectedHeader = test_array_list.get(0).getLongName();
        tfBold = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaRegular.otf");
        alertDialog = new Dialog(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.child_test_list_item, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        String name=test_child_array_list.get(position).getTest_long_name();

        holder.txt_article_child_head.setText(name);
    }

    @Override
    public int getItemCount() {
        return test_child_array_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_article_child_head;
        ImageView imgArticleChildHead;
        LinearLayout llParent;


        ViewHolder(View itemView) {
            super(itemView);

            llParent = itemView.findViewById(R.id.llParent);
            txt_article_child_head = itemView.findViewById(R.id.txt_article_child_head);
            imgArticleChildHead = itemView.findViewById(R.id.imgArticleChildHead);


        }
    }
}

