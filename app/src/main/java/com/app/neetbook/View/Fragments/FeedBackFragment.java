package com.app.neetbook.View.Fragments;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.app.neetbook.Interfaces.OnMobileNumberChanged;
import com.app.neetbook.Interfaces.OnSpinerItemClick;
import com.app.neetbook.Model.StateBean;
import com.app.neetbook.R;
import com.app.neetbook.Utils.AppController;
import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Data.ContactUsData;
import com.app.neetbook.Utils.Data.FeedBackData;
import com.app.neetbook.Utils.Data.MyProfileData;
import com.app.neetbook.Utils.FilePath;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.alerter.AlertBox;
import com.app.neetbook.Utils.widgets.DisplayOrientation;
import com.app.neetbook.Utils.widgets.SpinnerDialog;
import com.app.neetbook.View.ContactUs;
import com.app.neetbook.View.MobilePagesActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.app.neetbook.serviceRequest.VolleyMultipartRequest;
import com.squareup.picasso.Picasso;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.security.auth.Subject;

import static android.app.Activity.RESULT_OK;


public class FeedBackFragment extends Fragment implements View.OnClickListener {


    //Pdf request code
    private int PICK_PDF_REQUEST = 1;

    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 123;


    //Uri to store the image uri
    private Uri filePath;
    File  file = null;
    Button btnChooseFile;
    TextView txtFeedBackSelectedFile,txtFeedBackSpinHint2,txtFeedBackSpinHint1;
    EditText etContactUsDesc;
    RelativeLayout RlContactUsSubmit,rlFeedBackSpin2,rltFeedBackSpin1,rlSubjects,rlTopics,rlRemoveDoc;
    SessionManager sessionManager;
    private byte[] byteArray;
    ConnectionDetector connectionDetector;
    Loader loader;
    CustomAlert customAlert;
    ArrayList<String> subjectList = new ArrayList<>();
    ArrayList<String> contentTypeList = new ArrayList<>();
    ArrayList<StateBean> subjectListBeanList = new ArrayList<>();
    SpinnerDialog dialogContentTypes,dialogSubjects;
    OnMobileNumberChanged onMobileNumberChanged;
    String strTopic = "0";
    String isFromSideMenu = "No";
    String subjectId = "";
    String strSubjectName = "";
    int subjectSelectposition = 0;
    public FeedBackFragment(String strTopic,String isFromSideMenu,String subjectId) {
        this.strTopic = strTopic;
        this.isFromSideMenu = isFromSideMenu;
        this.subjectId = subjectId;
        // Required empty public constructor
    }

   
    public static FeedBackFragment newInstance(String param1, String param2) {
        FeedBackFragment fragment = new FeedBackFragment("","","");
        Bundle args = new Bundle();
    
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
           
        }
        sessionManager = new SessionManager(getActivity());
        connectionDetector = new ConnectionDetector(getActivity());
        loader = new Loader(getActivity());
        customAlert = new CustomAlert(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(getActivity().getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(getActivity()) == 0 || DisplayOrientation.getDisplayOrientation(getActivity()) == 2) ? R.layout.fragment_feed_back_portrait : R.layout.fragment_feed_back, container, false);
        init(rootView);
        return rootView;
    }

    private void init(View rootView) {
        btnChooseFile = rootView.findViewById(R.id.btnChooseFile);
        txtFeedBackSpinHint1 = rootView.findViewById(R.id.txtFeedBackSpinHint1);
        txtFeedBackSpinHint2 = rootView.findViewById(R.id.txtFeedBackSpinHint2);
        txtFeedBackSelectedFile = rootView.findViewById(R.id.txtFeedBackSelectedFile);
        etContactUsDesc = rootView.findViewById(R.id.etContactUsDesc);
        RlContactUsSubmit = rootView.findViewById(R.id.RlContactUsSubmit);
        rlSubjects = rootView.findViewById(R.id.rlSubjects);
        rlTopics = rootView.findViewById(R.id.rlTopics);
        rlRemoveDoc = rootView.findViewById(R.id.rlRemoveDoc);
        rlSubjects.setOnClickListener(this);
        rlTopics.setOnClickListener(this);
        btnChooseFile.setOnClickListener(this);
        RlContactUsSubmit.setOnClickListener(this);
        rlRemoveDoc.setOnClickListener(this);
        listener();
        requestStoragePermission();

        if(getActivity().getResources().getBoolean(R.bool.isTablet))
            restoreData();
        getSubjects();

    }

    private void listener() {
        etContactUsDesc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty())
                    FeedBackData.strDesc = "";
                else
                    FeedBackData.strDesc = s.toString();
                setSubmitEnable();
            }
        });
    }


    private void restoreData() {
        if(!FeedBackData.strSubjects.isEmpty()) {
            txtFeedBackSpinHint1.setText(FeedBackData.strSubjects);
            txtFeedBackSpinHint1.setTextColor(txtFeedBackSpinHint1.getText().toString().equalsIgnoreCase("")?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
        }
        if(!FeedBackData.strTopics.isEmpty()) {
            txtFeedBackSpinHint2.setText(FeedBackData.strTopics);
            txtFeedBackSpinHint2.setTextColor(txtFeedBackSpinHint1.getText().toString().equalsIgnoreCase("")?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
        }
        if(!FeedBackData.strSelectedFile.isEmpty()) {
            txtFeedBackSelectedFile.setText(FeedBackData.strSelectedFile);
            rlRemoveDoc.setVisibility(View.VISIBLE);
        }
        if(!FeedBackData.strDesc.isEmpty()) {
            etContactUsDesc.setText(FeedBackData.strDesc);
        }
    }


    private void getSubjects() {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(getActivity());

        HashMap<String,String> params = new HashMap<>();
        params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        mRequest.makeServiceRequest(IConstant.content_feedbackGetSubjectList, Request.Method.POST, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------content_feedbackGetSubjectList Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1")) {
                        subjectList.add("Select Subject");
                        StateBean stateBean1 = new StateBean();
                        stateBean1.setState_name("Select Subject");
                        stateBean1.set_id("_id");
                        subjectListBeanList.add(stateBean1);
                        JSONArray listArray = object.getJSONArray("list");
                        if(listArray.length()>0)
                        {
                            for(int i=0;i<listArray.length();i++) {
                                JSONObject jsonObject = listArray.getJSONObject(i);
                                StateBean stateBean = new StateBean();
                                subjectList.add(jsonObject.getString("subj_name"));
                                stateBean.setState_name(jsonObject.getString("subj_name"));
                                stateBean.set_id(jsonObject.getString("_id"));
                                if(subjectId.equalsIgnoreCase(jsonObject.getString("_id")))
                                {
                                    subjectSelectposition = i+1;
                                    strSubjectName = jsonObject.getString("subj_name");
                                }

                                subjectListBeanList.add(stateBean);
                            }

                        }
                        setSpinners();
                    } else if(sStatus.equals("00")){
                        loader.dismissLoader();
                        customAlert.singleLoginAlertLogout();
                    }else if(sStatus.equalsIgnoreCase("01")){
                        loader.dismissLoader();
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    } else {
                        loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void setSpinners() {
        contentTypeList.add("Select Topic");
        contentTypeList.add("Article");
        contentTypeList.add("Points");
        contentTypeList.add("MCQ");
        contentTypeList.add("Test");
        contentTypeList.add("Others");
        txtFeedBackSpinHint1.setText(strTopic.equalsIgnoreCase("0")?contentTypeList.get(1):strTopic.equalsIgnoreCase("1")?contentTypeList.get(2):strTopic.equalsIgnoreCase("2")?contentTypeList.get(3):contentTypeList.get(4));

        if(!strSubjectName.equalsIgnoreCase("")) {
            txtFeedBackSpinHint2.setText(strSubjectName);
            FeedBackData.strSubjects = strSubjectName;
            FeedBackData.strSubjectsId = subjectId;
            txtFeedBackSpinHint2.setTextColor(getResources().getColor(R.color.black));
            setSubmitEnable();
        }
        dialogSubjects = new SpinnerDialog(getActivity(), subjectList,
                "Select Subject");
        dialogContentTypes = new SpinnerDialog(getActivity(), contentTypeList,
                "Select Topic");

        txtFeedBackSpinHint1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogContentTypes.showSpinerDialog();
            }
        });
        rlSubjects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSubjects.showSpinerDialog();
            }
        });
        rlTopics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogContentTypes.showSpinerDialog();
            }
        });
        txtFeedBackSpinHint2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSubjects.showSpinerDialog();
            }
        });

            FeedBackData.strTopics = strTopic.equalsIgnoreCase("0")?contentTypeList.get(1):strTopic.equalsIgnoreCase("1")?contentTypeList.get(2):strTopic.equalsIgnoreCase("2")?contentTypeList.get(3):contentTypeList.get(3);
            FeedBackData.strTopicsId = String.valueOf(contentTypeList.indexOf(txtFeedBackSpinHint1.getText().toString()));
            txtFeedBackSpinHint1.setTextColor(getResources().getColor(R.color.black));

        dialogContentTypes.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {

                txtFeedBackSpinHint1.setText(item);
                FeedBackData.strTopics = item;
                FeedBackData.strTopicsId = ""+position;
                txtFeedBackSpinHint1.setTextColor(position==0?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
                setSubmitEnable();

            }
        });
        dialogSubjects.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {

                txtFeedBackSpinHint2.setText(item);
                FeedBackData.strSubjects = item;
                FeedBackData.strSubjectsId = subjectListBeanList.get(position).get_id();
                txtFeedBackSpinHint2.setTextColor(position==0?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
                setSubmitEnable();

            }
        });
    }

    //Requesting permission
    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }


    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            showFileChooser();
        } else {



        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnChooseFile:
                if (Build.VERSION.SDK_INT >= 23) {
                    if(!checkWriteExternalStoragePermission() || !checkReadExternalStoragePermission())
                        requestStoragePermission();
                    else
                        showFileChooser();
                }else
                    showFileChooser();
                break;

            case R.id.RlContactUsSubmit:
                if(txtFeedBackSpinHint1.getText().toString().equalsIgnoreCase("Select Topic"))
                    AlertBox.showSnackBox(getActivity(),getString(R.string.alert_oops),getString(R.string.alert_select_topic));
                else if(txtFeedBackSpinHint2.getText().toString().equalsIgnoreCase("Select Subject"))
                    AlertBox.showSnackBox(getActivity(),getString(R.string.alert_oops),getString(R.string.alert_select_subjects));
                else if(etContactUsDesc.getText().toString().isEmpty())
                    AlertBox.showSnackBox(getActivity(),getString(R.string.alert_oops),getString(R.string.alert_select_desc));
                else
                    uploadMultipart();
                break;

            case R.id.rlRemoveDoc:
                file = null;
                txtFeedBackSelectedFile.setText(getString(R.string.nofileSelected));
                rlRemoveDoc.setVisibility(View.GONE);
                break;

        }
    }

    private void setSubmitEnable() {
        if(!etContactUsDesc.getText().toString().isEmpty() && !txtFeedBackSpinHint1.getText().toString().equalsIgnoreCase(getString(R.string.selectTopics))&& !txtFeedBackSpinHint2.getText().toString().equalsIgnoreCase(getString(R.string.selectSubjects)))
            RlContactUsSubmit.setBackground(getResources().getDrawable(R.drawable.login_gradient));
        else
            RlContactUsSubmit.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
    }
    private void showFileChooser() {
//        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        // for uploading only images and vedios
//        intent.addCategory(Intent.CATEGORY_OPENABLE);
//        intent.setType("*/*");
//        String[] mimetypes = {"image/*", "video/*"};
//        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
//        String[] extraMimeTypes = {"application/docx", "application/pdf"};
        String[] extraMimeTypes = {"application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword"};

        intent.putExtra(Intent.EXTRA_MIME_TYPES, extraMimeTypes);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(Intent.createChooser(intent, "Select Doc"), PICK_PDF_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_PDF_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                InputStream inputStream = getActivity().getContentResolver().openInputStream(filePath);
                byte[] buffer = new byte[inputStream.available()];
                inputStream.read(buffer);
                 file = new File(getActivity().getFilesDir(), System.currentTimeMillis()+".docx");
                 FeedBackData.file = file;
                OutputStream outStream = new FileOutputStream(file);
                outStream.write(buffer);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (file != null && file.exists()) {
                Log.d("", "onActivityResult exists1 : " + file.exists());
//                myFileTypeStr = "doc";
//                uploadFile(file);
                byteArray = new byte[(int) file.length()];
                FeedBackData.byteArray = byteArray;
                rlRemoveDoc.setVisibility(View.VISIBLE);
                txtFeedBackSelectedFile.setText(file.getName());
                FeedBackData.strSelectedFile = file.getName();
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(file.getAbsolutePath());
                    fis.read(byteArray);
                    fis.close();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


        }
    }

    public void uploadMultipart() {
        loader.showLoader();
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, IConstant.content_feedback, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                System.out.println("UploadUserFeedback response" + resultResponse);
                String sStatus = "", sResponse = "", Surl = "", Smsg = "";
                try {
                    loader.dismissLoader();
                    JSONObject jsonObject = new JSONObject(resultResponse);
                    sStatus = jsonObject.getString("status");
                    Smsg = jsonObject.getString("message");
                    FeedBackData.clearData();
                    if(sStatus.equalsIgnoreCase("1"))
                        showAlertSuccess(getString(R.string.alert_success),Smsg);
                    else if(sStatus.equals("00")){
                        loader.dismissLoader();
                        customAlert.singleLoginAlertLogout();
                    } else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),Smsg);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops),Smsg);
                    }

                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loader.dismissLoader();
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");
                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Log.e("Header",sessionManager.getApiHeader().toString());
                return sessionManager.getApiHeader();
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("subject_id",FeedBackData.strSubjectsId);
                params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));
                params.put("user_uid",sessionManager.getUserDetails().get(SessionManager.KEY_UID));
                params.put("content_type",FeedBackData.strTopicsId);
                params.put("comment",etContactUsDesc.getText().toString().isEmpty()?"":etContactUsDesc.getText().toString());
                Log.e("Post Param",params.toString());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                //  params.put("file[" + 0 + "]", new DataPart("text.jpg", byteImage));

                if(file!= null)
                    params.put("fbfile", new DataPart(file.getName(), byteArray));

                //Log.e("fbfile", "file" + params.toString());
                return params;
            }
        };
        //to avoid repeat request Multiple Time
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        multipartRequest.setShouldCache(false);
        RequestQueue mRequestQueue = Volley.newRequestQueue(getActivity());
        mRequestQueue.add(multipartRequest);
    }

    private void showAlertSuccess(String title, String desc)
    {
        final Dialog alertDialog = new Dialog(getActivity());
        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        ImageView imgAlert = alertDialog.findViewById(R.id.imgAlert);
        Picasso.with(getActivity()).load(R.drawable.right_mark).into(imgAlert);
        textViewTitle.setText(title);
        textViewDesc.setText(desc);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if(isFromSideMenu.equalsIgnoreCase("Yes"))
                    MobilePagesActivity.mobilePagesActivity.finish();
                else {
                    if (onMobileNumberChanged != null)
                        onMobileNumberChanged.onMobileNumberChanged();
                }
            }
        });
        alertDialog.show();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof OnMobileNumberChanged){
            onMobileNumberChanged = (OnMobileNumberChanged) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onMobileNumberChanged = null;
    }
}
