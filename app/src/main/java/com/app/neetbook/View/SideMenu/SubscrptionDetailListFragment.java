package com.app.neetbook.View.SideMenu;


import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.neetbook.Adapter.subscription.SubscriptionDetailAdapter;
import com.app.neetbook.Adapter.subscription.SubscriptionDetailAdapter.OnItemClickListener;
import com.app.neetbook.Model.MCQExamIds;
import com.app.neetbook.Model.sidemenuFromContent.SubscriptionArticleBody;
import com.app.neetbook.Model.sidemenuFromContent.SubscriptionTest;
import com.app.neetbook.Model.subscription.SubscriptionDetail;
import com.app.neetbook.R;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.widgets.CustomTextViewBold;
import com.app.neetbook.Utils.widgets.DisplayOrientation;
import com.app.neetbook.View.ImagesViewActivity;
import com.app.neetbook.View.ReviewOrder;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.content.Context.WINDOW_SERVICE;


public class SubscrptionDetailListFragment extends Fragment {

  private ArrayList<SubscriptionDetail> mSubscriptionDetailList = new ArrayList<>();
  private ArrayList<SubscriptionDetail> mAnsweredList = new ArrayList<>();
  private SubscriptionDetailAdapter categoryAdapter;
  private RecyclerView categoryList;

  private String strsubjectId = "",strChapterId = "",strHeadId = "",strHeadName = "",strLoadingType = "";
  Loader loader;
  private int page = 1;
  int loadMoreEnable = 0;
  private int totalSubsCount;
  SessionManager sessionManager;
  private CustomAlert customAlert;
  TextView txt_subscription_header,noNontentAvailable;
  Typeface tfBold,tfMedium,tfRegular;
  String currentRightAnswer = "";
  ConstraintLayout removeLayout;

  private  RelativeLayout rlLeft,rlRight;
  public SubscrptionDetailListFragment(String strsubjectId ,String strChapterId, String strHeadId,String strHeadName,String strLoadingType) {
    // Required empty public constructor
    this.strsubjectId =strsubjectId;
    this.strChapterId =strChapterId;
    this.strHeadId =strHeadId;
    this.strHeadName =strHeadName;
    this.strLoadingType =strLoadingType;
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    sessionManager = new SessionManager(getActivity());
    customAlert = new CustomAlert(getActivity());
    loader = new Loader(getActivity());
    tfBold  = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaBold.otf");
    tfMedium  = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaSemibold.otf");
    tfRegular  = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaRegular.otf");

  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_subscrption_detail_list, container, false);
    init(rootView);
    return rootView;
  }

  private void init(View rootView) {
    txt_subscription_header = rootView.findViewById(R.id.txt_subscription_header);
    rlLeft = rootView.findViewById(R.id.rlLeft);
    rlRight = rootView.findViewById(R.id.rlLRight);
    noNontentAvailable = rootView.findViewById(R.id.no_content_available);
    categoryList = rootView.findViewById(R.id.rv_category_list);
    removeLayout= rootView.findViewById(R.id.removeLayout);
    noNontentAvailable.setTypeface(tfMedium);
    categoryList.setOnTouchListener(new View.OnTouchListener() {
      public boolean onTouch(View view, MotionEvent event) {
        if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {

        } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
          EventBus.getDefault().post("UPTOUCH");
        }
        return false;
      }
    });

    setFontSize();
    populateCategoryList();
    setLoasMore();
  }
  private  int getScreenHeightInDPs(Context context){
    DisplayMetrics dm = new DisplayMetrics();
    WindowManager windowManager = (WindowManager) context.getSystemService(WINDOW_SERVICE);
    windowManager.getDefaultDisplay().getMetrics(dm);
        /*
            In this example code we converted the float value
            to nearest whole integer number. But, you can get the actual height in dp
            by removing the Math.round method. Then, it will return a float value, you should
            also make the necessary changes.
        */

        /*
            public int heightPixels
                The absolute height of the display in pixels.

            public float density
             The logical density of the display.
        */
    int heightInDP = Math.round(dm.heightPixels / dm.density);
    return heightInDP;
  }
  private void setLoasMore() {
    categoryList.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        LinearLayoutManager layoutManager=LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
        int totalItemCount = layoutManager.getItemCount();
        int lastVisible = layoutManager.findLastVisibleItemPosition();

        if(dy>0){

          System.out.println("SCrollUP");

          if(totalItemCount-15==lastVisible){

            Log.e("Scroll","reached");

            if(totalSubsCount>totalItemCount&&loadMoreEnable == 0){

              page = page+1;
              loadMoreEnable = 1;
              loadMoreMCQ(lastVisible);
            }
          }

        } else{

          System.out.println("SCrollDown");
        }

       /* if (dy > 0) {
          {
            if(currentSelectedPosition < layoutManager.findFirstVisibleItemPosition())
              articleSectionAdapter.CollapseExpandedView(-1);
          }
        } else {
          if(currentSelectedPosition > lastVisible)
            articleSectionAdapter.CollapseExpandedView(-1);
        }*/

      /*  if((lastVisible == totalItemCount-1 || lastVisible == totalItemCount-2) && totalSubsCount>mSubscriptionDetailList.size() && loadMoreEnable == 0)
        {
          page = page+1;
          loadMoreEnable = 1;
          loadMoreMCQ(lastVisible);
        }*/
      }
    });
  }



  private void setFontSize() {

    if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
      txt_subscription_header.setTextAppearance(getActivity(),R.style.textViewSmallContentTitle);
    }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
      txt_subscription_header.setTextAppearance(getActivity(),R.style.textViewMediumContentTitle);
    }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
      txt_subscription_header.setTextAppearance(getActivity(),R.style.textViewLargeContentTitle);
    }

    txt_subscription_header.setTypeface(tfBold);
  }
  private void populateCategoryList() {
    mSubscriptionDetailList.clear();
    ServiceRequest mRequest = new ServiceRequest(getActivity());
    HashMap<String, String> params = new HashMap<>();
    params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
    params.put("subject_id",strsubjectId);
    if(!strLoadingType.equals("") && strLoadingType.equals("1"))
      params.put("head_id",strHeadId);
    else if(!strLoadingType.equals("") && strLoadingType.equals("2"))
    {
      params.put("cat_id",""+strHeadId);
      params.put("chapter_id",""+strChapterId);
    }else {
      params.put("ecat_id",""+strChapterId);
      params.put("ycat_id",""+strHeadId);
    }
    params.put("page",""+page);
    params.put("skip","0");
    params.put("limit","10");
    mRequest.makeServiceRequest(strLoadingType.equals("1")?IConstant.mcqHeading:strLoadingType.equals("2")?IConstant.mcqExam:IConstant.examyear, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
      @Override
      public void onCompleteListener(String response) {
        System.out.println((strLoadingType.equals("1")?"mcqHeading":strLoadingType.equals("2")?"mcqExam":"examyear")+"------------Response----------------" + response);
        String sStatus = "";
        String message = "";
        try {

          JSONObject obj = new JSONObject(response);
          sStatus = obj.getString("status");
          message = obj.has("message")?obj.getString("message"):"";


          if (sStatus.equalsIgnoreCase("1")) {
            JSONObject object = obj.getJSONObject("response");
            totalSubsCount = object.getInt("mcqtotal");
            JSONArray mcqArray = object.getJSONArray("mcq");
            if (mcqArray.length()==0){
              removeLayout.setVisibility(View.GONE);
              noNontentAvailable.setVisibility(View.VISIBLE);
            }
            else
            {
              removeLayout.setVisibility(View.VISIBLE);
              noNontentAvailable.setVisibility(View.GONE);
            }
            if(mcqArray.length()>0) {
              //llEmpty.setVisibility(View.GONE);
              for(int i=0;i<mcqArray.length();i++)
              {
                JSONObject jsonObject = mcqArray.getJSONObject(i);
                SubscriptionDetail categoryItem = new SubscriptionDetail();
                categoryItem.setId(jsonObject.getString("_id"));
                categoryItem.setQuestionCount(String.valueOf(i+1));
                categoryItem.setQuestionTitle(jsonObject.getString("question"));
                categoryItem.setQuestionOptionFirst(jsonObject.getString("optionA"));
                categoryItem.setQuestionOptionSecond(jsonObject.getString("optionB"));
                categoryItem.setQuestionOptionThird(jsonObject.getString("optionC"));
                categoryItem.setQuestionOptionFourth(jsonObject.getString("optionD"));
                categoryItem.setQuestionOptionFifth(jsonObject.getString("optionE"));
                categoryItem.setAnswers(jsonObject.getString("answer"));
                categoryItem.setExplanation(jsonObject.getString("explanation"));
                categoryItem.setReference(jsonObject.getString("reference"));
                ArrayList<String> imagesUrl = new ArrayList<>();
                ArrayList<MCQExamIds> mcqExamIdsArrayList = new ArrayList<>();
                if(jsonObject.getJSONArray("imageurl").length()>0)
                {for(int j=0;j<jsonObject.getJSONArray("imageurl").length();j++) {
                  imagesUrl.add(jsonObject.getJSONArray("imageurl").getJSONObject(j).getString("url"));
                }
                }
                if(jsonObject.getJSONArray("exmyr_ids").length()>0)
                {
                  for(int j=0;j<jsonObject.getJSONArray("exmyr_ids").length();j++) {
                  MCQExamIds mcqExamIds = new MCQExamIds();
                  mcqExamIds.setExam_id(jsonObject.getJSONArray("exmyr_ids").getJSONObject(j).getJSONObject("exam").getString("_id"));
                  mcqExamIds.setExam_name(jsonObject.getJSONArray("exmyr_ids").getJSONObject(j).getJSONObject("exam").getString("exam_name"));
                  mcqExamIds.setYear_id(jsonObject.getJSONArray("exmyr_ids").getJSONObject(j).getJSONObject("year").getString("_id"));
                  mcqExamIds.setYear_name(jsonObject.getJSONArray("exmyr_ids").getJSONObject(j).getJSONObject("year").getString("year_name"));
                  mcqExamIdsArrayList.add(mcqExamIds);
                }
                }
                categoryItem.setImageUrls(imagesUrl);
                categoryItem.setExmyr_ids(mcqExamIdsArrayList);
                mSubscriptionDetailList.add(categoryItem);
              }
              loadMoreEnable = 0;

              setupRecyclerView();
            }

          } else if(sStatus.equals("00")){
            customAlert.singleLoginAlertLogout();
          }else if(sStatus.equalsIgnoreCase("01")){
            customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
          } else {
            customAlert.showAlertOk(getString(R.string.alert_oops),message);
          }

        } catch (JSONException e) {

          e.printStackTrace();
        }

      }

      @Override
      public void onErrorListener(String errorMessage) {
        // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
      }
    });

  }
  private void loadMoreMCQ(int lastVisible) {
    ServiceRequest mRequest = new ServiceRequest(getActivity());
    HashMap<String, String> params = new HashMap<>();
    params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
    params.put("subject_id",strsubjectId);
    if(!strLoadingType.equals("") && strLoadingType.equals("1"))
      params.put("head_id",strHeadId);
    else if(!strLoadingType.equals("") && strLoadingType.equals("2"))
    {
      params.put("cat_id",""+strHeadId);
      params.put("chapter_id",""+strChapterId);
    }else {
      params.put("ecat_id",""+strChapterId);
      params.put("ycat_id",""+strHeadId);
    }
    params.put("page",""+page);
    params.put("skip","0");
    params.put("limit","10");
    mRequest.makeServiceRequest(strLoadingType.equals("1")?IConstant.mcqHeading:strLoadingType.equals("2")?IConstant.mcqExam:IConstant.examyear, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
      @Override
      public void onCompleteListener(String response) {
        System.out.println((strLoadingType.equals("1")?"mcqHeading":strLoadingType.equals("2")?"mcqExam":"examyear")+"------------Response----------------" + response);
        String sStatus = "";
        String message = "";
        try {

          JSONObject obj = new JSONObject(response);
          sStatus = obj.getString("status");
          message = obj.has("message")?obj.getString("message"):"";


          if (sStatus.equalsIgnoreCase("1")) {
            JSONObject object = obj.getJSONObject("response");
            totalSubsCount = object.getInt("mcqtotal");
            JSONArray mcqArray = object.getJSONArray("mcq");
            if(mcqArray.length()>0) {
              //llEmpty.setVisibility(View.GONE);
              for(int i=0;i<mcqArray.length();i++)
              {
                JSONObject jsonObject = mcqArray.getJSONObject(i);
                SubscriptionDetail categoryItem = new SubscriptionDetail();
                categoryItem.setId(jsonObject.getString("_id"));
                categoryItem.setQuestionCount(String.valueOf(i+1));
                categoryItem.setQuestionTitle(jsonObject.getString("question"));
                categoryItem.setQuestionOptionFirst(jsonObject.getString("optionA"));
                categoryItem.setQuestionOptionSecond(jsonObject.getString("optionB"));
                categoryItem.setQuestionOptionThird(jsonObject.getString("optionC"));
                categoryItem.setQuestionOptionFourth(jsonObject.getString("optionD"));
                categoryItem.setQuestionOptionFifth(jsonObject.getString("optionE"));
                categoryItem.setAnswers(jsonObject.getString("answer"));
                categoryItem.setExplanation(jsonObject.getString("explanation"));
                categoryItem.setReference(jsonObject.getString("reference"));
                ArrayList<String> imagesUrl = new ArrayList<>();
                ArrayList<MCQExamIds> mcqExamIdsArrayList = new ArrayList<>();
                if(jsonObject.getJSONArray("imageurl").length()>0)
                {for(int j=0;j<jsonObject.getJSONArray("imageurl").length();j++) {
                  imagesUrl.add(jsonObject.getJSONArray("imageurl").getJSONObject(j).getString("url"));
                }
                }
                if(jsonObject.getJSONArray("exmyr_ids").length()>0)
                {for(int j=0;j<jsonObject.getJSONArray("exmyr_ids").length();j++) {
                  MCQExamIds mcqExamIds = new MCQExamIds();
                  mcqExamIds.setExam_id(jsonObject.getJSONArray("exmyr_ids").getJSONObject(j).getString("exam_id"));
                  mcqExamIds.setExam_name(jsonObject.getJSONArray("exmyr_ids").getJSONObject(j).getString("exam_name"));
                  mcqExamIds.setYear_id(jsonObject.getJSONArray("exmyr_ids").getJSONObject(j).getString("year_id"));
                  mcqExamIds.setYear_name(jsonObject.getJSONArray("exmyr_ids").getJSONObject(j).getString("year_name"));
                  mcqExamIdsArrayList.add(mcqExamIds);
                }
                }
                categoryItem.setImageUrls(imagesUrl);
                categoryItem.setExmyr_ids(mcqExamIdsArrayList);
                mSubscriptionDetailList.add(categoryItem);
              }
              loadMoreEnable = 0;

             categoryAdapter.notifyDataSetChanged();
            }

          } else if(sStatus.equals("00")){
            customAlert.singleLoginAlertLogout();
          }else if(sStatus.equalsIgnoreCase("01")){
            customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
          } else {
            customAlert.showAlertOk(getString(R.string.alert_oops),message);
          }

        } catch (JSONException e) {

          e.printStackTrace();
        }

      }

      @Override
      public void onErrorListener(String errorMessage) {
        // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
      }
    });
  }
  @Override
  public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
    if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
      rlLeft.setVisibility(View.VISIBLE);
      rlRight.setVisibility(View.VISIBLE);
    } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){

      rlLeft.setVisibility(View.GONE);
      rlRight.setVisibility(View.GONE);
    }
  }
  private void setupRecyclerView() {

    categoryList.setLayoutManager(new LinearLayoutManager(getActivity(),
        RecyclerView.VERTICAL, false));
    categoryAdapter = new SubscriptionDetailAdapter(getActivity(), mSubscriptionDetailList);
    categoryList.setAdapter(categoryAdapter);
    categoryAdapter.setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(View itemView, int position) {
        ArrayList<String> answers = new ArrayList<>();
        String strAnswers = "";
        String strComma = "";
        String[] arrayanswer = mSubscriptionDetailList.get(position).getAnswers().split(",");
        if(arrayanswer.length>0)
        {
          for(int i=0;i<arrayanswer.length;i++)
          {
            switch (arrayanswer[i])
            {
              case "A":
                strAnswers = strAnswers + arrayanswer[i]+". "+mSubscriptionDetailList.get(position).getQuestionOptionFirst()+"\n";
                answers.add(arrayanswer[i]+" "+mSubscriptionDetailList.get(position).getQuestionOptionFirst());
                strComma = "\n";
                break;
              case "B":
                strAnswers = strAnswers + arrayanswer[i]+". "+mSubscriptionDetailList.get(position).getQuestionOptionSecond()+"\n";
                answers.add(arrayanswer[i]+" "+mSubscriptionDetailList.get(position).getQuestionOptionSecond());
                strComma = "\n";
                break;
              case "C":
                strAnswers = strAnswers + arrayanswer[i]+". "+mSubscriptionDetailList.get(position).getQuestionOptionThird()+"\n";
                answers.add(arrayanswer[i]+" "+mSubscriptionDetailList.get(position).getQuestionOptionThird());
                strComma = "\n";
                break;
              case "D":
                strAnswers = strAnswers + arrayanswer[i]+". "+mSubscriptionDetailList.get(position).getQuestionOptionFourth()+"\n";
                answers.add(arrayanswer[i]+" "+mSubscriptionDetailList.get(position).getQuestionOptionFourth());
                strComma = "\n";
                break;
              case "E":
                strAnswers = strAnswers + arrayanswer[i]+". "+mSubscriptionDetailList.get(position).getQuestionOptionFifth();
                answers.add(arrayanswer[i]+" "+mSubscriptionDetailList.get(position).getQuestionOptionFifth());
                strComma = "\n";
                break;




            }
          }
        }
        clearOldAnswer(position);
        startSubscriptionCategoryInfoDialogFragment(strAnswers,mSubscriptionDetailList.get(position).getExplanation(),mSubscriptionDetailList.get(position).getImageUrls(),mSubscriptionDetailList.get(position).getExmyr_ids());
      }

      @Override
      public void onAnswerSelected(int position, String Answer) {

        currentRightAnswer =  mSubscriptionDetailList.get(position).getUserAnswers();
        if(currentRightAnswer == null )
          mSubscriptionDetailList.get(position).setUserAnswers(Answer);
        else if(currentRightAnswer.isEmpty())
          mSubscriptionDetailList.get(position).setUserAnswers(Answer);
        else if(!currentRightAnswer.equalsIgnoreCase(mSubscriptionDetailList.get(position).getAnswers()))
          mSubscriptionDetailList.get(position).setUserAnswers(Answer);

        clearOldAnswer(position);

        Log.e("bufferAnswer",Answer);
        Log.e("getUserAnswers",mAnsweredList.size()+"");
      }

      @Override
      public void onMultiAnswerSelected(int position, String Answer) {

          String bufferAnswer = null;
          ArrayList<Character> chars = new ArrayList<>();

          if( mSubscriptionDetailList.get(position).getUserAnswers() != null) {
            for (char ch : mSubscriptionDetailList.get(position).getUserAnswers().toCharArray()) {

              String ans = String.valueOf(ch);
              if(mSubscriptionDetailList.get(position).getAnswers().contains(ans))
              {
                chars.add(ch);
              }else
                mSubscriptionDetailList.get(position).setUserAnswers(mSubscriptionDetailList.get(position).getUserAnswers().replace(ans,""));
            }
            if (mAnsweredList.size() > 0 && chars.size() > 0) {
              for (int i = 0; i < mAnsweredList.size(); i++) {
                if (mAnsweredList.get(i).getId().equalsIgnoreCase(mSubscriptionDetailList.get(position).getId())) {
                  String ans = mSubscriptionDetailList.get(position).getAnswers().replace(",", "");
                  // System.out.println(chars);
                  if (ans.length() > chars.size()) {
                    for (int k = 0; k < chars.size(); k++) {
                      if (Answer.charAt(0) == (chars.get(k)))
                        mSubscriptionDetailList.get(position).setUserAnswers(mSubscriptionDetailList.get(position).getUserAnswers().replace(Answer, ""));
                      else
                        mSubscriptionDetailList.get(position).setUserAnswers(mSubscriptionDetailList.get(position).getUserAnswers().contains(Answer) ? mSubscriptionDetailList.get(position).getUserAnswers() + "" : mSubscriptionDetailList.get(position).getUserAnswers() + Answer);
                    }
                  } else {
                    for (int k = 0; k < chars.size(); k++) {
                      if (Answer.charAt(0) == (chars.get(k)))
                        mSubscriptionDetailList.get(position).setUserAnswers(mSubscriptionDetailList.get(position).getUserAnswers().replace(Answer, ""));
                      else
                        mSubscriptionDetailList.get(position).setUserAnswers(mSubscriptionDetailList.get(position).getUserAnswers());
                    }

                  }
                }
              }
              // mSubscriptionDetailList.get(position).setUserAnswers(bufferAnswer);
            } else {
              mSubscriptionDetailList.get(position).setUserAnswers(Answer);
            }
          }

          if(isAdded(mSubscriptionDetailList.get(position))>-1) {
            Log.e("isAdded",isAdded(mSubscriptionDetailList.get(position))+"");
            if(mAnsweredList.size()>0) {
              mAnsweredList.remove(isAdded(mSubscriptionDetailList.get(position)));
              if( mSubscriptionDetailList.get(position).getUserAnswers() != null && !mSubscriptionDetailList.get(position).getUserAnswers().isEmpty() && mSubscriptionDetailList.get(position).getUserAnswers().length()>0) {
                mSubscriptionDetailList.get(position).setUserAnswers(mSubscriptionDetailList.get(position).getUserAnswers()+Answer);
                mAnsweredList.add(mSubscriptionDetailList.get(position));
              }
            }else
            {
              mSubscriptionDetailList.get(position).setUserAnswers(mSubscriptionDetailList.get(position).getUserAnswers()+Answer);
              mAnsweredList.add(mSubscriptionDetailList.get(position));
            }
          }
          else {
            mSubscriptionDetailList.get(position).setUserAnswers(mSubscriptionDetailList.get(position).getUserAnswers()+Answer);
            mAnsweredList.add(mSubscriptionDetailList.get(position));
            Log.e("isAdded", isAdded(mSubscriptionDetailList.get(position)) + "");
          }

        /*HashSet<SubscriptionDetail> hashSet = new HashSet(mAnsweredList);
        mAnsweredList.clear();
        for (SubscriptionDetail employee : hashSet) {
          System.out.println(employee.getId());
          mAnsweredList.add(employee);
        }*/
        clearOldAnswer(position);
        Log.e("after Filter",mAnsweredList.size()+"");
       /* categoryAdapter.notifyDataSetChanged();*/
      }

      @Override
      public void onImageClicked(int position) {
        clearOldAnswer(position);
        startActivity(new Intent(getActivity(), ImagesViewActivity.class).putExtra("title",strHeadName).putExtra("image",mSubscriptionDetailList.get(position).getImageUrls()).putExtra("from","MCQ"));
        getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
      }
    });
  }

  private void clearOldAnswer(int position) {
    if(mSubscriptionDetailList.size()>0)
    {
      for(int i=0;i<mSubscriptionDetailList.size();i++) {
        if (!mSubscriptionDetailList.get(position).getId().equalsIgnoreCase(mSubscriptionDetailList.get(i).getId()))
        {
          mSubscriptionDetailList.get(i).setUserAnswers("");
        }
      }
    }

    categoryAdapter.notifyDataSetChanged();
  }

  private void startSubscriptionCategoryInfoDialogFragment( String strAnswers,String explanations,ArrayList<String> images,ArrayList<MCQExamIds> mcqExamIds) {
    SubscriptionCategoryItemInfoFragment dialog = new SubscriptionCategoryItemInfoFragment(strAnswers,explanations,images,mcqExamIds,strHeadName);
    FragmentTransaction ft = getFragmentManager().beginTransaction();
    dialog.show(ft, SubscriptionCategoryItemInfoFragment.TAG);
  }
  private int isAdded(SubscriptionDetail priceList)
  {
    if(mAnsweredList.size()>0)
    {
      for(int i=0;i<mAnsweredList.size();i++)
      {
        if(mAnsweredList.get(i).getId().equals(priceList.getId()))
          return i;
        break;
      }

    }else
    {
      return  -1;
    }
    return  -1;
  }
}
