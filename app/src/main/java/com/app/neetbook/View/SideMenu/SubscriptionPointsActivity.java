package com.app.neetbook.View.SideMenu;


import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.app.neetbook.Adapter.ArticleTabsAdapter;
import com.app.neetbook.Interfaces.OnMobileHomeMenuSelectedListioner;
import com.app.neetbook.Model.HeadData;
import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.PointsContent;
import com.app.neetbook.Model.TabSideMenu;
import com.app.neetbook.Model.sidemenuFromContent.CategoryPoints;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ActionBarActivityNeetBook;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.OnSwipeTouchListener;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.View.DynamicFragmentActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.Fade;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;
import androidx.viewpager.widget.ViewPager;

public class SubscriptionPointsActivity extends ActionBarActivityNeetBook implements View.OnClickListener {

  private TabLayout tabLayout;
  private ViewPager viewPager;
  private Context context;
  Loader loader;
  SessionManager sessionManager;
  private CustomAlert customAlert;
  private int page = 1;
  int loadMoreEnable = 0;
  private int totalSubsCount;
  String strSubjectId= "",strChapterId= "";
  private ImageView imageView2,imageView3;
  private TextView textView,txtSurgical,textView2Pin;
  ArrayList<CategoryPoints> pointsArrayList;
  public static ArrayList<PointsContent> pointsContentArrayList;
  ArticleTabsAdapter homeGroupNameAdapter;
  LinearLayoutManager layoutManager;
  LinearSnapHelper snapHelper;
  RecyclerView recyclerViewGroups;
  private ArrayList<HeadData> headDataList;
  Typeface tfBold,tfMedium,tfRegular;
  private int currentSelectedPosition = 0;
  CoordinatorLayout llParent;
  private String strPoint_id;
  private RelativeLayout rlParentRight,rlParent;
  ImageView imgClose;
  Typeface tf;
  AppBarLayout appBarLayout;
  CollapsingToolbarLayout collapsingToolbarLayout;
  ConstraintLayout cLConstrain;

  private RelativeLayout point_title_layout;
  private TextView point_title_tv;
  private RelativeLayout main_point_title_layout;
  private LinearLayout top_reclerview_layout;
  private RelativeLayout content_title_layout;
  private TextView content_title_tv;
  private String is_coming_page="";

  public SubscriptionPointsActivity() {
    // Required empty public constructor
  }

  //
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_subscription_points);
   /* getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
            WindowManager.LayoutParams.FLAG_SECURE);*/
    context = getApplicationContext();
    StatusBarColorChange.updateStatusBarColor(SubscriptionPointsActivity.this,getResources().getColor(R.color.subscription_green));
    init();

  }

  @Override
  protected void attachBaseContext(Context newBase) {
    super.attachBaseContext(newBase);
    final Configuration override = new Configuration(newBase.getResources().getConfiguration());
    override.fontScale = 1.0f;
    applyOverrideConfiguration(override);
  }

  private void init() {
    tfBold  = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaBold.otf");
    tfMedium  = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaSemibold.otf");
    tfRegular  = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaRegular.otf");
    sessionManager = new SessionManager(SubscriptionPointsActivity.this);
    customAlert = new CustomAlert(SubscriptionPointsActivity.this);
    loader = new Loader(SubscriptionPointsActivity.this);
    headDataList = new ArrayList<>();
    viewPager = findViewById(R.id.viewpager_subscription_category);
    tabLayout = findViewById(R.id.tabs_subscription_category);
    imageView2 = findViewById(R.id.imageView2);
    imageView3 = findViewById(R.id.imageView3);
    textView = findViewById(R.id.textView);
    txtSurgical = findViewById(R.id.textView2);
    textView2Pin = findViewById(R.id.textView2Pin);
    llParent = findViewById(R.id.llParent);
    rlParent = findViewById(R.id.rlParent);
    recyclerViewGroups = findViewById(R.id.recyclerViewGroups);
    collapsingToolbarLayout =
            (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_points);

    point_title_layout=findViewById(R.id.point_title_layout);
    point_title_tv=findViewById(R.id.point_title_tv);
    main_point_title_layout=findViewById(R.id.main_point_title_layout);
    top_reclerview_layout=findViewById(R.id.top_reclerview_layout);
    content_title_layout=findViewById(R.id.content_title_layout);
    content_title_tv=findViewById(R.id.content_title_tv);


    if(getIntent().getExtras() != null)
    {
      strPoint_id = getIntent().getStringExtra("point_id");
      strChapterId = getIntent().getStringExtra("chapter_id");
      strSubjectId = getIntent().getStringExtra("subject_id");
      textView.setText(getIntent().getStringExtra("subject_name"));
      txtSurgical.setText(getIntent().getStringExtra("chapter_name"));
      textView2Pin.setText(getIntent().getStringExtra("chapter_name"));
      if(sessionManager.getViewToBookMark() != null && !sessionManager.getViewToBookMark().isEmpty() && sessionManager.getViewToBookMark().equalsIgnoreCase("Yes"))
      {
        pointsContentArrayList =
                new Gson().fromJson(sessionManager.getBookmarkDetails().get(SessionManager.HEAD_LIST), new TypeToken<List<PointsContent>>() {
                }.getType());
      }
      //getPointsContent();

      if(getIntent().getExtras().containsKey("coming_page")){

        is_coming_page=getIntent().getStringExtra("coming_page");
      }
    }
    createSnap();
    setupViewPager(viewPager);

    listener();
    setFontSize();
  }
  private void setCollapseingToolBar() {

    imgClose = findViewById(R.id.imgClose);
    appBarLayout = findViewById(R.id.appBarLayout);
    imgClose.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        if(is_coming_page.equals("homepage")){

          Intent s=new Intent(getApplicationContext(),TabMainActivity.class);
          s.putExtra("subjectId",strSubjectId);
          s.putExtra("subjectName",sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_NAME));
          s.putExtra("isTrial",sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_TRIAL_STATUS));
          s.putExtra("subjectShortName",sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_SHORT_NAME));
          s.putExtra("showing_page_position","1");
          startActivity(s);
          finish();
          overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);

        } else{

          finish();
        }
      }
    });
    if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
      collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbarSmall);
      collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbarSmall);
      content_title_tv.setTextAppearance(context,R.style.expandedappbarSmall);
      point_title_tv.setTextAppearance(context,R.style.collapsedappbarMedium);
    }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
      collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbarMedium);
      collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbarMedium);
      content_title_tv.setTextAppearance(context,R.style.collapsedappbarMedium);
      point_title_tv.setTextAppearance(context,R.style.collapsedappbarMedium);
    }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
      collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbarLarge);
      collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbarLarge);
      content_title_tv.setTextAppearance(context,R.style.collapsedappbarLarge);
      point_title_tv.setTextAppearance(context,R.style.collapsedappbarLarge);
    }
    tf = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaBold.otf");
    collapsingToolbarLayout.setCollapsedTitleTypeface(tf);
    collapsingToolbarLayout.setExpandedTitleTypeface(tf);
    content_title_tv.setTypeface(tf);
    point_title_tv.setTypeface(tf);


    appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
      @Override
      public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

        if (verticalOffset < -30) {

          TitleVisibleGoneAnimation(false);
        }

        if (verticalOffset < -100) {

           if(point_title_tv.getText().toString().length()==0){

             point_title_tv.setText(content_title_tv.getText().toString());
           }

           content_title_tv.setTextColor(ContextCompat.getColor(context,R.color.pointsTitle));

          if(top_reclerview_layout.getVisibility()==View.VISIBLE){

            VisibleGoneAnimation(false);

          }

        } else {

          content_title_tv.setTextColor(ContextCompat.getColor(context,R.color.pointsTitle));

          if(top_reclerview_layout.getVisibility()==View.INVISIBLE || top_reclerview_layout.getVisibility()==View.GONE){

            VisibleGoneAnimation(true);

          }

          TitleVisibleGoneAnimation(true);

        }


     /*   if(verticalOffset<-190) {
          recyclerViewGroups.setVisibility(View.GONE);
          collapsingToolbarLayout.setCollapsedTitleTextColor(getResources().getColor(R.color.white));
        }
        else {
          recyclerViewGroups.setVisibility(View.VISIBLE);
          collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(R.color.pointsTitle));

        }*/
      }
    });
  }


  private void VisibleGoneAnimation(Boolean show){

    try{

      Transition transition = new Fade();
      transition.setDuration(400);
      transition.addTarget(R.id.top_reclerview_layout);
      TransitionManager.beginDelayedTransition(top_reclerview_layout, transition);
      top_reclerview_layout.setVisibility(show ? View.VISIBLE : View.INVISIBLE);

      if(top_reclerview_layout.getVisibility()==View.INVISIBLE
              || top_reclerview_layout.getVisibility()==View.GONE){

        collapse(top_reclerview_layout);

        if(main_point_title_layout.getVisibility()==View.VISIBLE){

          MainTitleSetAnimation(false);

        }



      } else if(top_reclerview_layout.getVisibility()==View.VISIBLE){

        expand(top_reclerview_layout);


        if(main_point_title_layout.getVisibility()==View.GONE){

          MainTitleSetAnimation(true);
        }


      }

    }catch(Exception e){

    }
  }


  private void TitleVisibleGoneAnimation(Boolean show){

    try{

      Transition transition = new Fade();
      transition.setDuration(300);
      transition.addTarget(R.id.content_title_layout);
      TransitionManager.beginDelayedTransition(content_title_layout, transition);
      content_title_layout.setVisibility(show ? View.VISIBLE : View.INVISIBLE);

    }catch(Exception e){

    }
  }


  private void MainTitleSetAnimation(Boolean show){

    try{

      Transition transition = new Fade();
      transition.setDuration(500);
      transition.addTarget(R.id.main_point_title_layout);
      TransitionManager.beginDelayedTransition(main_point_title_layout, transition);
      main_point_title_layout.setVisibility(show ? View.VISIBLE : View.GONE);

    }catch(Exception e){

      Log.e("Exception",e.toString());
    }
  }


  private void setFontSize() {

    /*if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
      textView.setTextAppearance(SubscriptionPointsActivity.this,R.style.textViewSmallSubjectName);
      txtSurgical.setTextAppearance(SubscriptionPointsActivity.this,R.style.textViewSmallChaptName);
      textView2Pin.setTextAppearance(SubscriptionPointsActivity.this,R.style.textViewSmallChaptName);
    }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {*/
      textView.setTextAppearance(SubscriptionPointsActivity.this,R.style.textViewMediumSubjectName);
      txtSurgical.setTextAppearance(SubscriptionPointsActivity.this,R.style.textViewMediumChaptName);
      textView2Pin.setTextAppearance(SubscriptionPointsActivity.this,R.style.textViewMediumChaptName);
    /*}else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
      textView.setTextAppearance(SubscriptionPointsActivity.this,R.style.textViewLargeSubjectName);
      txtSurgical.setTextAppearance(SubscriptionPointsActivity.this,R.style.textViewLargeChaptName);
      textView2Pin.setTextAppearance(SubscriptionPointsActivity.this,R.style.textViewLargeChaptName);
    }*/

    textView.setTypeface(tfMedium);
    txtSurgical.setTypeface(tfMedium);
    textView2Pin.setTypeface(tfMedium);
  }
  private void listener() {

    imageView3.setOnClickListener(this);
    imageView2.setOnClickListener(this);

    rlParent.setOnTouchListener(new OnSwipeTouchListener(SubscriptionPointsActivity.this) {
      public void onSwipeTop() {
        //Toast.makeText(SubscriptionArticleActivity.this, "top", Toast.LENGTH_SHORT).show();
      }
      public void onSwipeRight() {
        //Toast.makeText(SubscriptionArticleActivity.this, "right", Toast.LENGTH_SHORT).show();
        finish();
      }
      public void onSwipeLeft() {

      }
      public void onSwipeBottom() {
        //Toast.makeText(SubscriptionArticleActivity.this, "bottom", Toast.LENGTH_SHORT).show();
      }

    });
  }
  @Override
  public void onClick(View v) {
    switch (v.getId()){
      case R.id.imageView2:
        finish();
        break;

      case R.id.imageView3:
        //finish();
        break;
    }
  }
  
  private void setupViewPager(final ViewPager viewPager) {
    final ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(),
        this);

    if(pointsContentArrayList.size()>0)
    {
      for (int i = 0; i < pointsContentArrayList.size(); i++) {
        HeadData data = new HeadData();
        data._id = pointsContentArrayList.get(i).get_id();
        data.short_name = pointsContentArrayList.get(i).getShort_name();
        data.long_name = pointsContentArrayList.get(i).getLong_name();
        headDataList.add(data);
        viewPagerAdapter.addFragment(new SubscriptionPointsFragment(strSubjectId,strChapterId,pointsContentArrayList.get(i).get_id(),pointsContentArrayList.get(i).getLong_name(), pointsContentArrayList.get(i).getContent(),pointsContentArrayList.get(i).getPointsTitleContentList(),pointsContentArrayList.get(i).getUrl(),0), pointsContentArrayList.get(i).getShort_name());
        if(strPoint_id.equalsIgnoreCase(pointsContentArrayList.get(i).get_id())) {
          currentSelectedPosition = i;
          collapsingToolbarLayout.setTitle("");
          content_title_tv.setText(pointsContentArrayList.get(i).getLong_name());
          sessionManager.updateBookMark("Points",pointsContentArrayList.get(i).get_id(),strSubjectId,getIntent().getStringExtra("subject_name"),new Gson().toJson(pointsContentArrayList),strChapterId,getIntent().getStringExtra("chapter_name"),"",strPoint_id,"");

          sessionManager.LastReadSubjectBookmark(sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                  sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                  sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME));
        }

      }

    }


    setCollapseingToolBar();
    setGroupAdapter(currentSelectedPosition);
    layoutManager = ((LinearLayoutManager)recyclerViewGroups.getLayoutManager());
    int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
    int centeredItemPosition = totalVisibleItems / 2;
    recyclerViewGroups.smoothScrollToPosition(currentSelectedPosition != 0 ?currentSelectedPosition-1 : currentSelectedPosition);
    viewPager.setAdapter(viewPagerAdapter);

    tabLayout.setupWithViewPager(viewPager);
    for (int i = 0; i < tabLayout.getTabCount(); i++) {
      TabLayout.Tab tab = tabLayout.getTabAt(i);
      if (tab != null) {
        tab.setCustomView(viewPagerAdapter.getTabView(i));
      }
    }

    viewPager.setCurrentItem(currentSelectedPosition);
    viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override
      public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

      }

      @Override
      public void onPageSelected(int position) {
        setGroupAdapter(position);
        layoutManager = ((LinearLayoutManager)recyclerViewGroups.getLayoutManager());
        int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
        int centeredItemPosition = totalVisibleItems / 2;
        recyclerViewGroups.smoothScrollToPosition(position != 0 ?position-1 : position);
        collapsingToolbarLayout.setTitle("");
        content_title_tv.setText(pointsContentArrayList.get(position).getLong_name());
        point_title_tv.setText(pointsContentArrayList.get(position).getLong_name());
        sessionManager.updateBookMark("Points",pointsContentArrayList.get(position).get_id(),strSubjectId,getIntent().getStringExtra("subject_name"),new Gson().toJson(pointsContentArrayList),strChapterId,getIntent().getStringExtra("chapter_name"),"",pointsContentArrayList.get(position).get_id(),"");
        sessionManager.LastReadSubjectBookmark(sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME));
      }

      @Override
      public void onPageScrollStateChanged(int state) {

      }
    });

    viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
      @Override
      public void transformPage(@NonNull View page, float position) {
        if (position != 0 && position != pointsContentArrayList.size() - 1) {
          page.setAlpha(0f);
          page.setVisibility(View.VISIBLE);

          // Start Animation for a short period of time
          page.animate()
                  .alpha(1f)
                  .setDuration(page.getResources().getInteger(android.R.integer.config_shortAnimTime));
        }
      }
    });
  }

  @Override
  public void onPointerCaptureChanged(boolean hasCapture) {

  }
  private void setGroupAdapter(int position) {

    if(homeGroupNameAdapter==null ) {

      homeGroupNameAdapter = new ArticleTabsAdapter(headDataList, SubscriptionPointsActivity.this,position);
      layoutManager=new LinearLayoutManager(SubscriptionPointsActivity.this, RecyclerView.HORIZONTAL, false);
      layoutManager.setStackFromEnd(true);
      recyclerViewGroups.setLayoutManager(layoutManager);

     // snapHelper.attachToRecyclerView(recyclerViewGroups);
      recyclerViewGroups.setAdapter(homeGroupNameAdapter);

      recyclerViewGroups.scrollToPosition(position);

    } else{

      homeGroupNameAdapter.notifyPosition(position);
      scrollToCenter(layoutManager,recyclerViewGroups,position);
    }


    homeGroupNameAdapter.setOnGroupMenuSelectedListener(new OnMobileHomeMenuSelectedListioner() {
      @Override
      public void onGroupSelected(String groupId, int position) {
        viewPager.setCurrentItem(position);
        layoutManager = ((LinearLayoutManager)recyclerViewGroups.getLayoutManager());
        int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
        int centeredItemPosition = totalVisibleItems / 2;
        //recyclerViewGroups.smoothScrollToPosition(position != 0 ?position-1 : position);
       // recyclerViewGroups.setScrollY(centeredItemPosition);

        scrollToCenter(layoutManager,recyclerViewGroups,position);
      }
    });

  }


  public void scrollToCenter(LinearLayoutManager layoutManager, RecyclerView recyclerList, int clickPosition) {
    RecyclerView.SmoothScroller smoothScroller = createSnapScroller(recyclerList, layoutManager);

    if (smoothScroller != null) {
      smoothScroller.setTargetPosition(clickPosition);
      layoutManager.startSmoothScroll(smoothScroller);
    }
  }

  // This number controls the speed of smooth scroll
  private static final float MILLISECONDS_PER_INCH = 200f;

  private final static int DIMENSION = 2;
  private final static int HORIZONTAL = 0;
  private final static int VERTICAL = 1;

  @Nullable
  private LinearSmoothScroller createSnapScroller(RecyclerView mRecyclerView, final RecyclerView.LayoutManager layoutManager) {
    if (!(layoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider)) {
      return null;
    }
    return new LinearSmoothScroller(mRecyclerView.getContext()) {
      @Override
      protected void onTargetFound(View targetView, RecyclerView.State state, Action action) {
        int[] snapDistances = calculateDistanceToFinalSnap(layoutManager, targetView);
        final int dx = snapDistances[HORIZONTAL];
        final int dy = snapDistances[VERTICAL];
        final int time = calculateTimeForDeceleration(Math.max(Math.abs(dx), Math.abs(dy)));
        if (time > 0) {
          action.update(dx, dy, time, mDecelerateInterpolator);
        }
      }


      @Override
      protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
        return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
      }
    };
  }


  private int[] calculateDistanceToFinalSnap(@NonNull RecyclerView.LayoutManager layoutManager, @NonNull View targetView) {
    int[] out = new int[DIMENSION];
    if (layoutManager.canScrollHorizontally()) {
      out[HORIZONTAL] = distanceToCenter(layoutManager, targetView,
              OrientationHelper.createHorizontalHelper(layoutManager));
    }

    if (layoutManager.canScrollVertically()) {
      out[VERTICAL] = distanceToCenter(layoutManager, targetView,
              OrientationHelper.createHorizontalHelper(layoutManager));
    }
    return out;
  }


  private int distanceToCenter(@NonNull RecyclerView.LayoutManager layoutManager,
                               @NonNull View targetView, OrientationHelper helper) {
    final int childCenter = helper.getDecoratedStart(targetView)
            + (helper.getDecoratedMeasurement(targetView) / 2);
    final int containerCenter;
    if (layoutManager.getClipToPadding()) {
      containerCenter = helper.getStartAfterPadding() + helper.getTotalSpace() / 2;
    } else {
      containerCenter = helper.getEnd() / 2;
    }
    return childCenter - containerCenter;
  }



  private class ViewPagerAdapter extends FragmentPagerAdapter {

    List<Fragment> fragmentList = new ArrayList<>();
    List<String> fragmentTitles = new ArrayList<>();
    Context mContext;
    TextView tabTitles;


    ViewPagerAdapter(FragmentManager fragmentManager,
                     FragmentActivity activity) {
      super(fragmentManager);
      mContext = activity;
    }

    @Override
    public Fragment getItem(int i) {
      return new SubscriptionPointsFragment(strSubjectId,strChapterId,pointsContentArrayList.get(i).get_id(),pointsContentArrayList.get(i).getLong_name(), pointsContentArrayList.get(i).getContent(),pointsContentArrayList.get(i).getPointsTitleContentList(),pointsContentArrayList.get(i).getUrl(),0);
    }

    @Override
    public int getCount() {
      return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
      return fragmentTitles.get(position);
    }

    void addFragment(Fragment fragment, String name) {
      fragmentList.add(fragment);
      fragmentTitles.add(name);
    }

    View getTabView(int position) {
      // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
      View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_items, null);
      tabTitles = v.findViewById(R.id.txtTabTitle);
      tabTitles.setText(fragmentTitles.get(position));
      tabTitles.setTextColor(getResources().getColor(R.color.white));
      return v;
    }
  }

  private void createSnap() {
    snapHelper = new LinearSnapHelper() {
      @Override
      public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
        int targetPosition = -1;
        try {
          View centerView = findSnapView(layoutManager);
          if (centerView == null)
            return RecyclerView.NO_POSITION;

          int position = layoutManager.getPosition(centerView);

          if (layoutManager.canScrollHorizontally()) {
            if (velocityX < 0) {
              targetPosition = position - 1;
            } else {
              targetPosition = position + 1;
            }
          }

          if (layoutManager.canScrollVertically()) {
            if (velocityY < 0) {
              targetPosition = position - 1;
            } else {
              targetPosition = position + 1;
            }
          }

          final int firstItem = 0;
          final int lastItem = layoutManager.getItemCount() - 1;
          targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

        }catch (Exception e)
        {
          e.printStackTrace();
        }
        return targetPosition;
      }
    };
  }



  public static void expand(final View v) {
    int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
    int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
    v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
    final int targetHeight = v.getMeasuredHeight();

    // Older versions of android (pre API 21) cancel animations for views with a height of 0.
    v.getLayoutParams().height = 1;
    v.setVisibility(View.VISIBLE);
    Animation a = new Animation()
    {
      @Override
      protected void applyTransformation(float interpolatedTime, Transformation t) {
        v.getLayoutParams().height = interpolatedTime == 1
                ? WindowManager.LayoutParams.WRAP_CONTENT
                : (int)(targetHeight * interpolatedTime);
        v.requestLayout();
      }

      @Override
      public boolean willChangeBounds() {
        return true;
      }
    };

    int duration=(int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density);

    Log.e("collapseduration", String.valueOf(duration));

    // Expansion speed of 1dp/ms
    a.setDuration(150);
    v.startAnimation(a);
  }

  public static void collapse(final View v) {
    final int initialHeight = v.getMeasuredHeight();

    Animation a = new Animation()
    {
      @Override
      protected void applyTransformation(float interpolatedTime, Transformation t) {
        if(interpolatedTime == 1){
          v.setVisibility(View.GONE);
        }else{
          v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
          v.requestLayout();
        }
      }

      @Override
      public boolean willChangeBounds() {
        return true;
      }
    };

    int duration=(int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density);

    Log.e("collapseduration", String.valueOf(duration));

    // Collapse speed of 1dp/ms
    a.setDuration(150);
    v.startAnimation(a);
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();

    if(is_coming_page.equals("homepage")){

      Intent s=new Intent(getApplicationContext(),TabMainActivity.class);
      s.putExtra("subjectId",strSubjectId);
      s.putExtra("subjectName",sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_NAME));
      s.putExtra("isTrial",sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_TRIAL_STATUS));
      s.putExtra("subjectShortName",sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_SHORT_NAME));
      s.putExtra("showing_page_position","0");
      startActivity(s);
      finish();
      overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);

    } else{

      finish();
    }
  }

}
