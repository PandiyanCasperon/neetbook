package com.app.neetbook.View.SideMenu;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.database.ContentObserver;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.app.TabPojo.EventbusPojo;
import com.app.TabPojo.SlideTapChildFragmentRecyclerPojo;
import com.app.neetbook.Adapter.ArticleTabsAdapter;
import com.app.neetbook.Adapter.HomeSearchAdapter;
import com.app.neetbook.Interfaces.APMTSearchListioner;
import com.app.neetbook.Interfaces.FragmentRefreshListener;
import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.Interfaces.NetworkStateReceiverListener;
import com.app.neetbook.Interfaces.OnArtickeHeadingChanged;
import com.app.neetbook.Interfaces.OnMobileHomeMenuSelectedListioner;
import com.app.neetbook.Interfaces.OnSearchItemClickListener;
import com.app.neetbook.Interfaces.TabArtcileChapterClickListener;
import com.app.neetbook.Interfaces.TestSection;
import com.app.neetbook.Model.HeadData;
import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.PointsContent;
import com.app.neetbook.Model.SearchBean;
import com.app.neetbook.Model.TabSideMenu;
import com.app.neetbook.Model.TestActivityLastReadDetails;
import com.app.neetbook.R;
import com.app.neetbook.TAbAdapter.SlideTabChildFragmentRecycleAdapter;
import com.app.neetbook.Utils.ActionBarActivityNeetBook;
import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.CustomDialog.ConfirmationDialog;
import com.app.neetbook.Utils.CustomDialog.FeedBackDialog;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.NetworkStateReceiver;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.Utils.SwipeDetector;
import com.app.neetbook.Utils.Thread.AppExecutors;
import com.app.neetbook.View.CustomDialogActivity;
import com.app.neetbook.View.MobilePagesActivity;
import com.app.neetbook.View.ScrollingActivity;
import com.app.neetbook.View.SideMenu.New.NewTestFragment;
import com.app.neetbook.View.SplashActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.cleveroad.sy.cyclemenuwidget.CycleMenuWidget;
import com.cleveroad.sy.cyclemenuwidget.OnMenuItemClickListener;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import eu.long1.spacetablayout.SpaceTabLayout;


public class TabMainActivity extends FragmentActivity implements NetworkStateReceiverListener, View.OnClickListener, DrawerLayout.DrawerListener, ItemClickListener, TabArtcileChapterClickListener, OnMenuItemClickListener, OnArtickeHeadingChanged {

    private Context mContext;
    private CoordinatorLayout coordinatorLayout;
    private SpaceTabLayout tabLayout;
    private ViewPager viewPager;
    public static TabMainActivity tabMainActivity;
    public static long startingLevelTime;
    public static long packageTime = 0;
    public static long packageDays = 0;
    public static boolean isReadPage;
    public static boolean isStarted;
    public static String strSessionId;
    //  private Loader loader;
    private ConnectionDetector cd;
    private SessionManager sessionManager;
    private CustomAlert customAlert;
    private ArrayList<PointsContent> mPointsList = new ArrayList<>();
    private String strRemainingHours = "";
    private String strRemainingDays = "";
    private NetworkStateReceiver networkStateReceiver;
    private FragmentRefreshListener fragmentRefreshListener;
    private TextView textView12, textView14, textView13, textView15, textView11;
    private TextView smallA, mediumA, LargeA;
    private ImageView imageView8, imageView9, imageView10, imageView11, imageView12, imageView13, imageView7;
    private CoordinatorLayout constrainTop;
    private ConstraintLayout constrainTop_tab;
    private CountDownTimer cTimer;
    public static boolean isDestroyed = false;
    public static boolean isLoaded = false;
    private ArticleFragment articleFragment;
    private PointsFragment pointsFragment;
    private McqFragment mcqFragment;
    private RelativeLayout imageView7R, rlSearch, rlFeedback, rlNightMode;
    private TestFragment testFragment;
    private NewTestFragment newtestFragment;
    private String result = "";
    private Typeface tfBold, tfMedium, tfRegular;
    private int currentSelectedPoisition = 0;
    private LinearLayout llChangeFont, constrainFont;
    private View viewMediumA, viewLargeA, viewSmallA;


    /*TabView Resourse*/
    private DrawerLayout navigationdrawer;
    private RelativeLayout right_tab_layout;
    private ImageView imageViewBookmark, imageExitMenu;
    private TextView textView3, textView2, textView;


    private RelativeLayout slide_tab_main_layout;
    private RelativeLayout tab_layout_side_view_layout;
    private ImageView slide_open_imageview;

    //--Navigation Drawer
    private TabLayout tab_layout;
    private ViewPager side_menu_viewpager;
    private View slide_menu_left_view;
    private TextView txt_subscription_header, txtSuperHeading;
    private ImageView slider_layout_drawer_icon;
    private LinearLayout slider_fragment_drawer;
    private RelativeLayout swip_right_slienr_layout;
    private LinearLayout close_child_fragment_drawer;
    private RecyclerView slide_tap_fragment_recycler_view;
    private LinearLayout close_slide_tap_layout;

    private TextView side_header_title_tv, txtChapterName;
    private View slide_menu_right_view;

    private LinearLayout main_view;

    private int Page_Position = 0;

    private SlideTabChildFragmentRecycleAdapter adapter;
    private ArrayList<SlideTapChildFragmentRecyclerPojo> slide_tap_list = new ArrayList<>();

    private ConfirmationDialog dialog;
    private FeedBackDialog feedback_dialog;

    /*ArticleFragment*/
    private CycleMenuWidget mCycleMenuWidget;
    private ArrayList<HeadData> headDataList;
    private ViewPagerAdapter viewPagerAdapter;
    private int currentSelectedPosition = 0;
    private String strHeadId;
    Fragment fragment, articleFromMCQFragment;
    private int isLayoutOpened = 0;
    FragmentManager fragmentManager = getSupportFragmentManager();
    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
    String strLoadingType = "1", strChapterId = "", strSubjectId = "", strLongName = "", strHeadName = "", strChapterName;
    ArticleTabsAdapter homeGroupNameAdapter;
    LinearLayoutManager layoutManager;
    LinearSnapHelper snapHelper;
    RecyclerView recyclerViewGroups;
    private AppExecutors appExecutors;
    ConstraintLayout llConstrainSearch;
    EditText etSearch;
    private ImageView imgSearchClose, imgSearchGo;
    private Dialog alertDialog;
    private int page = 1;
    private int limit = 10;
    private int loadMoreEnable = 0;
    private ArrayList<SearchBean> searchBeanArrayList = new ArrayList<>();
    private HomeSearchAdapter homeSearchAdapter;
    private RecyclerView homeSearchRecyclerView;
    private int totalCountSearch = 0;
    private APMTSearchListioner apmtSearchListioner;
    private String searchHeadId, searchChapterId;
    int modeCheck = 0;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.tab_main_layout);

        sessionManager = new SessionManager(TabMainActivity.this);
        mContext = TabMainActivity.this;




        /*if (getResources().getBoolean(R.bool.isTablet)) {
            initTab();

        } else {
            init();
        }*/

        int nightModeFlags = this.getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;





        if (getResources().getBoolean(R.bool.isTablet)) {
            initTab();

        } else {
            init();
        }
    }


    // Make a listener
    ContentObserver observer = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            Toast.makeText(getApplicationContext(), "er", Toast.LENGTH_SHORT).show();
            super.onChange(selfChange);
        }

        @Override
        public boolean deliverSelfNotifications() {
            Toast.makeText(getApplicationContext(), "er", Toast.LENGTH_SHORT).show();
            return true;
        }
    };

   // Start listening




    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }

    private void initCommonData() {

        tfBold = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaRegular.otf");
        sessionManager = new SessionManager(TabMainActivity.this);
        customAlert = new CustomAlert(TabMainActivity.this);
        // loader = new Loader(TabMainActivity.this);
        cd = new ConnectionDetector(TabMainActivity.this);
        textView11 = findViewById(R.id.textView11);
        imageView7 = findViewById(R.id.imageView7);
        imageView7R = findViewById(R.id.imageView7R);
        textView12 = findViewById(R.id.textView12);
        smallA = findViewById(R.id.smallA);
        mediumA = findViewById(R.id.mediumA);
        LargeA = findViewById(R.id.LargeA);
        textView13 = findViewById(R.id.textView13);
        textView15 = findViewById(R.id.textView15);
        textView14 = findViewById(R.id.textView14);
        imageView8 = findViewById(R.id.imageView8);
        imageView9 = findViewById(R.id.imageView9);
        imageView10 = findViewById(R.id.imageView10);
        imageView11 = findViewById(R.id.imageView11);
        imageView12 = findViewById(R.id.imageView12);
        imageView13 = findViewById(R.id.imageView13);
        viewLargeA = findViewById(R.id.viewLargeA);
        llChangeFont = findViewById(R.id.llChangeFont);
        constrainTop = findViewById(R.id.activity_main);
        viewSmallA = findViewById(R.id.viewSmallA);
        viewMediumA = findViewById(R.id.viewMediumA);
        if (textView15 != null)
            textView15.setVisibility(View.GONE);

        textView11.setText(getIntent().getStringExtra("subjectName"));
        searchHeadId = getIntent().hasExtra("headId") ? getIntent().getStringExtra("headId") : "";
        searchChapterId = getIntent().hasExtra("chapterId") ? getIntent().getStringExtra("chapterId") : "";
        isDestroyed = false;
        setShowOrHide(getIntent().getStringExtra("isTrial").equals("1"));
        imageView7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cTimer != null) {
                    cTimer.cancel();
                    cTimer = null;
                }
                finish();
            }
        });

//        imageView11.setOnClickListener(this);
        imageView12.setOnClickListener(this);
        imageView13.setOnClickListener(this);
        imageView8.setOnClickListener(this);
//        llChangeFont.setOnClickListener(this);

        setDefaultView();
        currentSelectedPoisition = 0;
        tabMainActivity = this;
        isReadPage = false;
        isStarted = true;
        setMenu();
        startTimerSession();
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));


    }

    private void initTab() {

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        dialog = new ConfirmationDialog(mContext);
        dialog.CustomDialoginitialize();

        llChangeFont = findViewById(R.id.llChangeFont);
        txtSuperHeading = findViewById(R.id.txtSuperHeading);
        rlNightMode = findViewById(R.id.rlNightMode);
        rlFeedback = findViewById(R.id.rlFeedback);
        rlSearch = findViewById(R.id.rlSearch);
        tab_layout = (TabLayout) findViewById(R.id.tab_layout);

        imageExitMenu = findViewById(R.id.imageExitMenu);
        imageViewBookmark = findViewById(R.id.imageViewBookmark);
        textView = findViewById(R.id.textView);
        textView3 = findViewById(R.id.textView3);
        textView2 = findViewById(R.id.textView2);
        recyclerViewGroups = findViewById(R.id.recyclerViewGroups);
        slide_menu_left_view = (View) findViewById(R.id.slide_menu_left_view);
        txt_subscription_header = (TextView) findViewById(R.id.txt_subscription_header);
        slider_layout_drawer_icon = (ImageView) findViewById(R.id.slider_layout_drawer_icon);
        navigationdrawer = (DrawerLayout) findViewById(R.id.navigationdrawer);
        slider_fragment_drawer = (LinearLayout) findViewById(R.id.slider_fragment_drawer);
        slide_menu_right_view = (View) findViewById(R.id.slide_menu_right_view);
        swip_right_slienr_layout = (RelativeLayout) findViewById(R.id.swip_right_slienr_layout);

        close_child_fragment_drawer = (LinearLayout) findViewById(R.id.close_child_fragment_drawer);
        /*slide_tap_fragment_recycler_view = (RecyclerView) findViewById(R.id.slide_tap_fragment_recycler_view);*/

        close_slide_tap_layout = (LinearLayout) findViewById(R.id.close_slide_tap_layout);


        constrainTop_tab = findViewById(R.id.constrainTop);

        main_view = (LinearLayout) findViewById(R.id.main_view);

        navigationdrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        navigationdrawer.setDrawerListener(this);
        mCycleMenuWidget = (CycleMenuWidget) findViewById(R.id.itemCycleMenuWidget);
        mCycleMenuWidget.setVisibility(View.GONE);
        mCycleMenuWidget.setCorner(CycleMenuWidget.CORNER.RIGHT_TOP);
        //  mCycleMenuWidget.setMode(sessionManager.isNightModeEnabled()?1:0);
        mCycleMenuWidget.setAlpha(0.9f);
        mCycleMenuWidget.setOnMenuItemClickListener(this);
        DisplayMetrics metric = getResources().getDisplayMetrics();
        int screenwidth = (int) (metric.widthPixels * 0.70);
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) slider_fragment_drawer.getLayoutParams();
        params.width = screenwidth;
        slider_fragment_drawer.setLayoutParams(params);
        appExecutors = new AppExecutors();
        createSnap();
        EventBus.getDefault().register(this);
        if (sessionManager.getViewToBookMark() != null && !sessionManager.getViewToBookMark().isEmpty() && sessionManager.getViewToBookMark().equalsIgnoreCase("Yes")) {
            ScrollingActivity.headDataArrayList =
                    new Gson().fromJson(sessionManager.getBookmarkDetails().get(SessionManager.HEAD_LIST), new TypeToken<List<HeadDetails>>() {
                    }.getType());
        }

        slide_tab_main_layout = (RelativeLayout) findViewById(R.id.slide_tab_main_layout);


        slide_open_imageview = (ImageView) findViewById(R.id.slide_open_imageview);
        right_tab_layout = (RelativeLayout) findViewById(R.id.right_tab_layout);
        tab_layout_side_view_layout = (RelativeLayout) findViewById(R.id.tab_layout_side_view_layout);
        navigationdrawer.setDrawerListener(this);
        side_header_title_tv = (TextView) findViewById(R.id.side_header_title_tv);
        txtChapterName = (TextView) findViewById(R.id.txtChapterName);

        side_header_title_tv.setText(getIntent().getStringExtra("subjectName"));
        feedback_dialog = new FeedBackDialog(mContext);

        headDataList = new ArrayList<>();
        feedback_dialog.FeedBackDialoginitialize();
        initCommonData();
        ButtonClickListener();
        OnclickListener();


        //SlideTapItemLoadAdapterClass();
        llChangeFont.setOnClickListener(this);
        txtSuperHeading.setOnClickListener(this);
        rlNightMode.setOnClickListener(this);
        rlSearch.setOnClickListener(this);
        rlFeedback.setOnClickListener(this);
        imageExitMenu.setOnClickListener(this);
        imageViewBookmark.setOnClickListener(this);


    }

    private void init() {
        // to check this activity is opened
        constrainFont = findViewById(R.id.constrainFont);
        llConstrainSearch = findViewById(R.id.llConstrainSearch);
        etSearch = findViewById(R.id.etSearch);
        imgSearchClose = findViewById(R.id.imgSearchClose);
        imgSearchGo = findViewById(R.id.imgSearchGo);

        constrainFont.setOnClickListener(this);
        imgSearchClose.setOnClickListener(this);
        imgSearchGo.setOnClickListener(this);


        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!etSearch.getText().toString().isEmpty() && etSearch.getText().toString().length() > 3) {
                    imgSearchClose.setVisibility(View.GONE);
                    imgSearchGo.setVisibility(View.VISIBLE);
                }
            }
        });

        initCommonData();

    }

    private void openSearchList(final int type, String searchKey) {
        llConstrainSearch.setVisibility(View.GONE);
        alertDialog = new Dialog(TabMainActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        alertDialog.setContentView(R.layout.homesearchlistdialog);
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(false);
           /* if(alertDialog.getWindow() != null) {
                alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }*/
        homeSearchRecyclerView = alertDialog.findViewById(R.id.homeSearchRecyclerView);
        final RelativeLayout rlEmptySearch = alertDialog.findViewById(R.id.rlEmptySearch);
        final ImageView imgSearchResultClose = alertDialog.findViewById(R.id.imgSearchResultClose);
        imgSearchResultClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog.isShowing())
                    alertDialog.dismiss();
            }
        });
        searchBeanArrayList.clear();
        ServiceRequest mRequest = new ServiceRequest(TabMainActivity.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("page", "" + page);
        params.put("search", "" + type);
        params.put("keyword", searchKey);
        mRequest.makeServiceRequest(IConstant.homeSearch, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------homeSearch Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalCountSearch = Integer.parseInt(object.getString("total"));

                        JSONArray testArray = object.getJSONArray("content");
                        if (testArray.length() > 0) {
                            rlEmptySearch.setVisibility(View.GONE);
                            for (int i = 0; i < testArray.length(); i++) {
                                SearchBean searchBean = new SearchBean();
                                JSONObject jsonObject = testArray.getJSONObject(i);
                                searchBean.setStrSubjectId(jsonObject.getString("subject_id"));
                                searchBean.setStrSubjectShortName(jsonObject.getString("subject_short_name"));
                                searchBean.setStrSubjectLongName(jsonObject.getString("subject_long_name"));
                                searchBean.setStrMImage(jsonObject.getString("mob_img"));
                                searchBean.setStrTImage(jsonObject.getString("tab_img"));
                                searchBean.setStrChaptId(jsonObject.getString("chapter_id"));
                                searchBean.setStrChaptLongName(jsonObject.getString("chapter_long_name"));
                                searchBean.setStrChaptShortName(jsonObject.getString("chapter_short_name"));
                                searchBean.setStrHeadId(jsonObject.has("heading_id") ? jsonObject.getString("heading_id") : "");
                                searchBean.setStrHeadLongName(jsonObject.has("heading_long_name") ? jsonObject.getString("heading_long_name") : "");
                                searchBean.setStrHeadShortName(jsonObject.has("heading_short_name") ? jsonObject.getString("heading_short_name") : "");
                                searchBean.setStrIsSubscribed(jsonObject.getString("subscribed_user"));
                                searchBeanArrayList.add(searchBean);

                            }

                            setupSearchAdapter(type);
                        } else {
                            rlEmptySearch.setVisibility(View.VISIBLE);
                        }

                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);

            }
        });
        if (!alertDialog.isShowing())
            alertDialog.show();

    }

    private void setupSearchAdapter(int menuType) {

        homeSearchAdapter = new HomeSearchAdapter(searchBeanArrayList, TabMainActivity.this, menuType);
        homeSearchRecyclerView.setLayoutManager(new LinearLayoutManager(TabMainActivity.this, LinearLayoutManager.VERTICAL, false));
        //snapHelper.attachToRecyclerView(homeSearchRecyclerView);
        homeSearchRecyclerView.setAdapter(homeSearchAdapter);
        homeSearchRecyclerView.setVisibility(View.VISIBLE);
        performSearchClick();
    }

    private void performSearchClick() {
        homeSearchAdapter.setOnSearchItemClickListener(new OnSearchItemClickListener() {
            @Override
            public void onSearchItemClicked(int position, int menuType) {
                if (alertDialog != null && alertDialog.isShowing())
                    alertDialog.dismiss();
                apmtSearchListioner.onSearch(searchBeanArrayList.get(position).getStrHeadId(), searchBeanArrayList.get(position).getStrChaptId());

            }
        });
    }

    private void setDefaultView() {

        if (viewSmallA != null) {
            viewSmallA.setVisibility(sessionManager.getFontSize().equalsIgnoreCase("small") ? View.VISIBLE : View.INVISIBLE);
            viewMediumA.setVisibility(sessionManager.getFontSize().equalsIgnoreCase("medium") ? View.VISIBLE : View.INVISIBLE);
            viewLargeA.setVisibility(sessionManager.getFontSize().equalsIgnoreCase("large") ? View.VISIBLE : View.INVISIBLE);
        }
        StatusBarColorChange.updateStatusBarColorSMFC(TabMainActivity.this, getResources().getColor(R.color.article_page_background));
        if (getResources().getBoolean(R.bool.isTablet))
            slide_tab_main_layout.setBackgroundColor(getResources().getColor(R.color.article_page_background));
        else
            constrainTop.setBackgroundColor(getResources().getColor(R.color.article_page_background));
        textView11.setTextColor(getResources().getColor(R.color.artical_pink));
        textView12.setTextColor(getResources().getColor(R.color.artical_pink));
        textView14.setTextColor(getResources().getColor(R.color.artical_pink));
        if (viewSmallA != null) {
            LargeA.setTextColor(getResources().getColor(R.color.artical_pink));
            mediumA.setTextColor(getResources().getColor(R.color.artical_pink));
            smallA.setTextColor(getResources().getColor(R.color.artical_pink));

            viewSmallA.setBackgroundColor(getResources().getColor(R.color.artical_pink));
            viewMediumA.setBackgroundColor(getResources().getColor(R.color.artical_pink));
            viewLargeA.setBackgroundColor(getResources().getColor(R.color.artical_pink));

            imageView7R.setBackgroundDrawable(getResources().getDrawable(R.drawable.article_home_circle));
        }
        Picasso.with(TabMainActivity.this).load(R.drawable.home_article_red).into(imageView7);
        Picasso.with(TabMainActivity.this).load(R.drawable.search_article).into(imageView8);
        Picasso.with(TabMainActivity.this).load(R.drawable.clock_article).into(imageView9);
        Picasso.with(TabMainActivity.this).load(R.drawable.calander_article).into(imageView10);
        //Picasso.with(TabMainActivity.this).load(R.drawable.resize_font_article).into(imageView11);
        Picasso.with(TabMainActivity.this).load(sessionManager.isNightModeEnabled() ? R.drawable.moon_night : R.drawable.moon_article).into(imageView12);
        Picasso.with(TabMainActivity.this).load(R.drawable.msg_article).into(imageView13);
    }

    private void setTimer() {

        cTimer = new CountDownTimer(packageTime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                packageTime = millisUntilFinished;
                long Hours = ((millisUntilFinished / 1000) / 60) / 60;
                long Minutes = millisUntilFinished / (60 * 1000) % 60;
                long Seconds = millisUntilFinished / 1000 % 60;
                textView12.setText(Hours + " Hours\n" + Minutes + " Min " + Seconds + " Sec");

            }

            @Override
            public void onFinish() {

                cTimer.cancel();

                if (isLoaded)
                    updateOnTimeOver(getString(R.string.alert), getString(R.string.timeOver));

            }
        };
        cTimer.start();
    }

    private void setShowOrHide(boolean equals) {
        if (textView13 != null)
            textView13.setVisibility(equals ? View.GONE : View.VISIBLE);
        /*textView15.setVisibility(equals?View.GONE:View.VISIBLE);*/
        textView12.setVisibility(equals ? View.GONE : View.VISIBLE);
        textView14.setVisibility(equals ? View.GONE : View.VISIBLE);
        imageView9.setVisibility(equals ? View.GONE : View.VISIBLE);
        imageView10.setVisibility(equals ? View.GONE : View.VISIBLE);
    }

    private void startTimerSession() {

        //   loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(getApplicationContext());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("subject_id", getIntent().getStringExtra("subjectId"));

        mRequest.makeServiceRequest(IConstant.startSession, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("startSession" + "-------- Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    //      loader.dismissLoader();
                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");

                       /* if(Long.parseLong(object.getString("remaining_hours"))<1)
                        {
                            packageExpiredAlert("Insufficient Remaining Hours");
                        }else {*/
                        packageTime = Long.parseLong(object.getString("remaining_hours"));
                        //packageTime = 1*60*1000;
                        startingLevelTime = packageTime;
                        packageDays = Long.parseLong(object.getString("remaining_days"));
                        textView14.setText(object.getString("remaining_days") + " " + getString(R.string.days) + "\n More");
                        strSessionId = object.getString("session_id");
                        if (cTimer != null) {
                            cTimer.cancel();
                            cTimer = null;
                        }
                        isLoaded = true;
                        setTimer();

                        startTimerThread();
                        //showTab();
                        /*}*/

                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else if (sStatus.equalsIgnoreCase("2")) {
                        updateOnTimeOver(getString(R.string.alert_oops), message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }

                } catch (JSONException e) {
                    //    loader.dismissLoader();
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                //  loader.dismissLoader();
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void showTab() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                tabLayout.setVisibility(View.VISIBLE);
            }
        }, 300);

    }

    private void startTimerThread() {
        Thread t = new Thread() {
            @Override
            public void run() {
                while (!isInterrupted()) {
                    try {
                        Thread.sleep(180000);  //1000ms = 1 sec
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (cd.isConnectingToInternet())
                                    new AsyncUpdateSessionRunner("0", "update").execute();

                            }
                        });

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        t.start();
    }

    private void setMenu() {
        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.clear();

        //sessionManager.setHeaderDetails(getIntent().getStringExtra("subjectId"), getIntent().getStringExtra("subjectName"), getIntent().getStringExtra("isTrial"), getIntent().getStringExtra("subjectShortName"), getIntent().hasExtra("menuType") && getIntent().getStringExtra("menuType").equals("1") ? searchHeadId : "", getIntent().hasExtra("menuType") && getIntent().getStringExtra("menuType").equals("1") ? searchChapterId : "", getIntent().getStringExtra("heading_long_name"));
        articleFragment = new ArticleFragment(getIntent().getStringExtra("subjectId"), getIntent().getStringExtra("subjectName"), getIntent().getStringExtra("isTrial"), getIntent().getStringExtra("subjectShortName"), getIntent().hasExtra("menuType") && getIntent().getStringExtra("menuType").equals("1") ? searchHeadId : "", getIntent().hasExtra("menuType") && getIntent().getStringExtra("menuType").equals("1") ? searchChapterId : "", getIntent().getStringExtra("heading_long_name"));
        apmtSearchListioner = (APMTSearchListioner) articleFragment;
        if (!fragmentList.contains(articleFragment))
            fragmentList.add(articleFragment);

        pointsFragment = new PointsFragment(getIntent().getStringExtra("subjectId"), getIntent().getStringExtra("subjectName"), getIntent().getStringExtra("isTrial"), getIntent().getStringExtra("subjectShortName"), getIntent().hasExtra("menuType") && getIntent().getStringExtra("menuType").equals("2") ? searchHeadId : "", getIntent().hasExtra("menuType") && getIntent().getStringExtra("menuType").equals("2") ? searchChapterId : "");
        apmtSearchListioner = (APMTSearchListioner) pointsFragment;
        if (!fragmentList.contains(pointsFragment))
            fragmentList.add(pointsFragment);


        mcqFragment = new McqFragment(getIntent().getStringExtra("subjectId"), getIntent().getStringExtra("subjectName"), getIntent().getStringExtra("isTrial"), getIntent().getStringExtra("subjectShortName"), getIntent().hasExtra("menuType") && getIntent().getStringExtra("menuType").equals("3") ? searchHeadId : "", getIntent().hasExtra("menuType") && getIntent().getStringExtra("menuType").equals("3") ? searchChapterId : "", getIntent().getStringExtra("heading_long_name"));
        apmtSearchListioner = (APMTSearchListioner) mcqFragment;
        if (!fragmentList.contains(mcqFragment))
            fragmentList.add(mcqFragment);

        testFragment = new TestFragment(getIntent().getStringExtra("subjectId"), getIntent().getStringExtra("subjectName"), getIntent().getStringExtra("isTrial"), getIntent().getStringExtra("subjectShortName"), getIntent().hasExtra("menuType") && getIntent().getStringExtra("menuType").equals("4") ? searchHeadId : "", getIntent().hasExtra("menuType") && getIntent().getStringExtra("menuType").equals("4") ? searchChapterId : "");
        apmtSearchListioner = (APMTSearchListioner) testFragment;
        if (!fragmentList.contains(testFragment))
            fragmentList.add(testFragment);

     /*   newtestFragment = new NewTestFragment(getIntent().getStringExtra("subjectId"), getIntent().getStringExtra("subjectName"), getIntent().getStringExtra("isTrial"), getIntent().getStringExtra("subjectShortName"), getIntent().hasExtra("menuType") && getIntent().getStringExtra("menuType").equals("4")?searchHeadId:"",getIntent().hasExtra("menuType") && getIntent().getStringExtra("menuType").equals("4")?searchChapterId:"");
        apmtSearchListioner = (APMTSearchListioner) newtestFragment;
        if (!fragmentList.contains(newtestFragment))
            fragmentList.add(newtestFragment);*/

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.activity_main);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (SpaceTabLayout) findViewById(R.id.spaceTabLayout);


        FragmentManager fm = getSupportFragmentManager();


        tabLayout.initialize(viewPager, fm, fragmentList);
        if (getIntent().hasExtra("menuType")) {
            switch (getIntent().getStringExtra("menuType")) {
                case "1":
                    viewPager.setCurrentItem(0);
                    notifyChanges(0);
                    break;
                case "2":
                    viewPager.setCurrentItem(1);
                    notifyChanges(1);
                    break;
                case "3":
                    viewPager.setCurrentItem(2);
                    notifyChanges(2);
                    break;
                case "4":
                    viewPager.setCurrentItem(3);
                    notifyChanges(3);
                    break;


            }
        }
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                currentSelectedPoisition = position;
                notifyChanges(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                page.setAlpha(0f);
                page.setVisibility(View.VISIBLE);

                // Start Animation for a short period of time
                page.animate()
                        .alpha(1f)
                        .setDuration(page.getResources().getInteger(android.R.integer.config_shortAnimTime));
            }
        });

        if (getIntent().getExtras().containsKey("showing_page_position")) {

            String showing_page = getIntent().getStringExtra("showing_page_position");

            viewPager.setCurrentItem(Integer.parseInt(showing_page));
            notifyChanges(Integer.parseInt(showing_page));
        }
    }

    private void notifyChanges(int position) {

        if (position == 0) {
            StatusBarColorChange.updateStatusBarColorSMFC(TabMainActivity.this, getResources().getColor(R.color.article_page_background));
            if (getResources().getBoolean(R.bool.isTablet))
                slide_tab_main_layout.setBackgroundColor(getResources().getColor(R.color.article_page_background));
            else
                constrainTop.setBackgroundColor(getResources().getColor(R.color.article_page_background));
            textView11.setTextColor(getResources().getColor(R.color.artical_pink));
            textView12.setTextColor(getResources().getColor(R.color.artical_pink));
            textView14.setTextColor(getResources().getColor(R.color.artical_pink));
            if (viewSmallA != null) {
                LargeA.setTextColor(getResources().getColor(R.color.artical_pink));
                mediumA.setTextColor(getResources().getColor(R.color.artical_pink));
                smallA.setTextColor(getResources().getColor(R.color.artical_pink));
                viewSmallA.setBackgroundColor(getResources().getColor(R.color.artical_pink));
                viewMediumA.setBackgroundColor(getResources().getColor(R.color.artical_pink));
                viewLargeA.setBackgroundColor(getResources().getColor(R.color.artical_pink));

                imageView7R.setBackgroundDrawable(getResources().getDrawable(R.drawable.article_home_circle));
            }
            Picasso.with(TabMainActivity.this).load(R.drawable.home_article_red).into(imageView7);
            Picasso.with(TabMainActivity.this).load(R.drawable.search_article).into(imageView8);
            Picasso.with(TabMainActivity.this).load(R.drawable.clock_article).into(imageView9);
            Picasso.with(TabMainActivity.this).load(R.drawable.calander_article).into(imageView10);
            //Picasso.with(TabMainActivity.this).load(R.drawable.resize_font_article).into(imageView11);
            Picasso.with(TabMainActivity.this).load(sessionManager.isNightModeEnabled() ? R.drawable.moon_night : R.drawable.moon_article).into(imageView12);
            Picasso.with(TabMainActivity.this).load(R.drawable.msg_article).into(imageView13);
        } else if (position == 1) {
            StatusBarColorChange.updateStatusBarColorSMFC(TabMainActivity.this, getResources().getColor(R.color.points_page_background));
            if (getResources().getBoolean(R.bool.isTablet))
                slide_tab_main_layout.setBackgroundColor(getResources().getColor(R.color.points_page_background));
            else
                constrainTop.setBackgroundColor(getResources().getColor(R.color.points_page_background));
            textView11.setTextColor(getResources().getColor(R.color.points_top_text));
            textView12.setTextColor(getResources().getColor(R.color.points_top_text));
            textView14.setTextColor(getResources().getColor(R.color.points_top_text));
            if (viewSmallA != null) {
                LargeA.setTextColor(getResources().getColor(R.color.points_top_text));
                mediumA.setTextColor(getResources().getColor(R.color.points_top_text));
                smallA.setTextColor(getResources().getColor(R.color.points_top_text));
                viewSmallA.setBackgroundColor(getResources().getColor(R.color.points_top_text));
                viewMediumA.setBackgroundColor(getResources().getColor(R.color.points_top_text));
                viewLargeA.setBackgroundColor(getResources().getColor(R.color.points_top_text));

                imageView7R.setBackgroundDrawable(getResources().getDrawable(R.drawable.points_home_circle));
            }
            Picasso.with(TabMainActivity.this).load(R.drawable.home_points).into(imageView7);
            Picasso.with(TabMainActivity.this).load(R.drawable.icn_point_search).into(imageView8);
            Picasso.with(TabMainActivity.this).load(R.drawable.clock_points).into(imageView9);
            Picasso.with(TabMainActivity.this).load(R.drawable.cal_points).into(imageView10);
            //Picasso.with(TabMainActivity.this).load(R.drawable.resize_font_points).into(imageView11);
            Picasso.with(TabMainActivity.this).load(sessionManager.isNightModeEnabled() ? R.drawable.moon_night : R.drawable.moon_points).into(imageView12);
            Picasso.with(TabMainActivity.this).load(R.drawable.message_points).into(imageView13);
        } else if (position == 2) {
            StatusBarColorChange.updateStatusBarColorSMFC(TabMainActivity.this, getResources().getColor(R.color.mcq_page_background));
            if (getResources().getBoolean(R.bool.isTablet))
                slide_tab_main_layout.setBackgroundColor(getResources().getColor(R.color.mcq_page_background));
            else
                constrainTop.setBackgroundColor(getResources().getColor(R.color.mcq_page_background));
            textView11.setTextColor(getResources().getColor(R.color.mcq_top_text));
            textView12.setTextColor(getResources().getColor(R.color.mcq_top_text));
            textView14.setTextColor(getResources().getColor(R.color.mcq_top_text));
            if (viewSmallA != null) {
                LargeA.setTextColor(getResources().getColor(R.color.mcq_top_text));
                mediumA.setTextColor(getResources().getColor(R.color.mcq_top_text));
                smallA.setTextColor(getResources().getColor(R.color.mcq_top_text));
                viewSmallA.setBackgroundColor(getResources().getColor(R.color.mcq_top_text));
                viewMediumA.setBackgroundColor(getResources().getColor(R.color.mcq_top_text));
                viewLargeA.setBackgroundColor(getResources().getColor(R.color.mcq_top_text));

                imageView7R.setBackgroundDrawable(getResources().getDrawable(R.drawable.mcq_home_circle));
            }
            Picasso.with(TabMainActivity.this).load(R.drawable.home_article).into(imageView7);
            Picasso.with(TabMainActivity.this).load(R.drawable.search_blue).into(imageView8);
            Picasso.with(TabMainActivity.this).load(R.drawable.clock_mcq).into(imageView9);
            Picasso.with(TabMainActivity.this).load(R.drawable.cal_mcq).into(imageView10);
            // Picasso.with(TabMainActivity.this).load(R.drawable.resize_font_mcq).into(imageView11);
            Picasso.with(TabMainActivity.this).load(sessionManager.isNightModeEnabled() ? R.drawable.moon_night : R.drawable.moon_mcq).into(imageView12);
            Picasso.with(TabMainActivity.this).load(R.drawable.message_article).into(imageView13);
        } else if (position == 3) {
            StatusBarColorChange.updateStatusBarColorSMFC(TabMainActivity.this, getResources().getColor(R.color.test_page_background));
            if (getResources().getBoolean(R.bool.isTablet))
                slide_tab_main_layout.setBackgroundColor(getResources().getColor(R.color.test_page_background));
            else
                constrainTop.setBackgroundColor(getResources().getColor(R.color.test_page_background));
            textView11.setTextColor(getResources().getColor(R.color.test_top_text));
            textView12.setTextColor(getResources().getColor(R.color.test_top_text));
            textView14.setTextColor(getResources().getColor(R.color.test_top_text));
            if (viewSmallA != null) {
                LargeA.setTextColor(getResources().getColor(R.color.test_top_text));
                mediumA.setTextColor(getResources().getColor(R.color.test_top_text));
                smallA.setTextColor(getResources().getColor(R.color.test_top_text));
                viewSmallA.setBackgroundColor(getResources().getColor(R.color.test_top_text));
                viewMediumA.setBackgroundColor(getResources().getColor(R.color.test_top_text));
                viewLargeA.setBackgroundColor(getResources().getColor(R.color.test_top_text));

                imageView7R.setBackgroundDrawable(getResources().getDrawable(R.drawable.test_home_circle));
            }
            Picasso.with(TabMainActivity.this).load(R.drawable.home_test).into(imageView7);
            Picasso.with(TabMainActivity.this).load(R.drawable.search_yellow).into(imageView8);
            Picasso.with(TabMainActivity.this).load(R.drawable.clock_test).into(imageView9);
            Picasso.with(TabMainActivity.this).load(R.drawable.cal_test).into(imageView10);
            // Picasso.with(TabMainActivity.this).load(R.drawable.resize_font__test).into(imageView11);
            Picasso.with(TabMainActivity.this).load(sessionManager.isNightModeEnabled() ? R.drawable.moon_night : R.drawable.moon_test).into(imageView12);
            Picasso.with(TabMainActivity.this).load(R.drawable.message_test).into(imageView13);
        }
    }


    public void updateStatusBarColor(int color) {// Color must be in hexadecimal fromat
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            //window.setStatusBarColor(color);
        }
    }


    @Override
    public void onResume() {

        TabMainActivity.isReadPage = false;
        sessionManager.closeViewToBookMark();
        if (cTimer != null) {
            cTimer.cancel();
            cTimer = null;
        }
        setTimer();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        ConnectionDetector cd = new ConnectionDetector(TabMainActivity.this);

        if (cTimer != null && !TabMainActivity.isReadPage) {

            if (cd.isConnectingToInternet())
                new AsyncUpdateSessionRunner("0", "update").execute();
            cTimer.cancel();
            cTimer = null;
        }
        super.onPause();
    }


    @Override
    public void onStop() {
        super.onStop();
        if (cTimer != null && !TabMainActivity.isReadPage) {
            cTimer.cancel();
            cTimer = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
           // this.getApplicationContext().getContentResolver().unregisterContentObserver(mSettingsContentObserver);

            ConnectionDetector cd = new ConnectionDetector(TabMainActivity.this);
            EventBus.getDefault().unregister(this);
            isDestroyed = true;
            sessionManager.updateTimer(String.valueOf(startingLevelTime - packageTime), strSessionId);
            if (cd.isConnectingToInternet())
                new AsyncUpdateSessionRunner("1", "update").execute();
            if (cTimer != null) {
                cTimer.cancel();
                cTimer = null;
            }
            if (EventBus.getDefault().isRegistered(this)) {
                EventBus.getDefault().unregister(this);
            }

            networkStateReceiver.removeListener(this);
            this.unregisterReceiver(networkStateReceiver);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void networkAvailable() {
        if (sessionManager.getMilliseconds() != null && !sessionManager.getMilliseconds().equals("")) {

            packageDays = startingLevelTime - Long.parseLong(sessionManager.getMilliseconds());

            if (cTimer != null) {
                cTimer.cancel();
                cTimer = null;
            }
            if (!isDestroyed) {
                setTimer();
            } else {
                if (cd.isConnectingToInternet())
                    new AsyncUpdateSessionRunner("1", "update").execute();
            }
            if (getFragmentRefreshListener() != null) {
                getFragmentRefreshListener().onRefreshResume();
            }
        }
    }

    @Override
    public void networkUnavailable() {

        sessionManager.updateTimer(String.valueOf(startingLevelTime - packageTime), strSessionId);
        if (cTimer != null) {
            cTimer.cancel();
            cTimer = null;
        }
        if (getFragmentRefreshListener() != null) {
            getFragmentRefreshListener().onRefresh();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.constrainFont:
                showFontSize();
                break;

            case R.id.rlFeedback:
                FeedbackDialogShow();
                break;

            case R.id.imageView12:
                swapAppMode();
                break;

            case R.id.rlNightMode:
                swapAppMode();
                break;

            case R.id.rlSearch:

                break;
            case R.id.imageView13:
                if (!getResources().getBoolean(R.bool.isTablet))
                    showFeedBack();
                break;


            case R.id.slider_layout_drawer_icon:

                SliderFragmentDrwerOpen();

                break;

            case R.id.close_child_fragment_drawer:

                SliderFragmentDrawerClose();

                break;

            case R.id.close_slide_tap_layout:

                CloseSlideTabLayout();

                break;

            case R.id.slide_open_imageview:

                SlideOpenCloseFunction();

                break;

            case R.id.imageView8:
                openSearch();
                break;

            case R.id.imgSearchClose:
                closeSearch();
                break;

            case R.id.imgSearchGo:
                searchAPMT();
                break;
        }
    }

    private void closeSearch() {
        llConstrainSearch.setVisibility(View.GONE);
    }

    private void searchAPMT() {
        llConstrainSearch.setVisibility(View.GONE);
        openSearchList(viewPager.getCurrentItem() + 1, etSearch.getText().toString());
    }

    private void openSearch() {
        // keyboard auto open
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        llConstrainSearch.setVisibility(View.VISIBLE);
    }

    public void showFontSize() {

        if (viewSmallA.getVisibility() == View.VISIBLE) {
            sessionManager.setFontSize("medium");
        } else if (viewMediumA.getVisibility() == View.VISIBLE) {
            sessionManager.setFontSize("large");
        } else if (viewLargeA.getVisibility() == View.VISIBLE) {
            sessionManager.setFontSize("small");
        }

        viewSmallA.setVisibility(sessionManager.getFontSize().equalsIgnoreCase("small") ? View.VISIBLE : View.INVISIBLE);
        viewMediumA.setVisibility(sessionManager.getFontSize().equalsIgnoreCase("medium") ? View.VISIBLE : View.INVISIBLE);
        viewLargeA.setVisibility(sessionManager.getFontSize().equalsIgnoreCase("large") ? View.VISIBLE : View.INVISIBLE);

    }


    private void showFeedBack() {

        TabSideMenu tabSideMenu2 = new TabSideMenu();
        tabSideMenu2.setId("9");
        tabSideMenu2.setName(getString(R.string.feedback));
        tabSideMenu2.setImage_id_inactive(R.drawable.feedback_inactive);
        tabSideMenu2.setImage_id_acative(R.drawable.feedback_active);

        Log.e("subjectId", ">" + getIntent().getStringExtra("subjectId"));
        startActivity(new Intent(TabMainActivity.this, MobilePagesActivity.class).putExtra("sideMenu", tabSideMenu2).putExtra("topic", String.valueOf(viewPager.getCurrentItem())).putExtra("subjectId", getIntent().getStringExtra("subjectId")));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    private void swapAppMode() {

        if (sessionManager.isNightModeEnabled()) {
            sessionManager.setNightMode(false);
        } else {
            sessionManager.setNightMode(true);
        }
        if (sessionManager.isNightModeEnabled()) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        notifyChanges(currentSelectedPoisition);
//        Intent intent = getIntent();
//        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//        finish();
//        startActivity(intent);
    }

    @Override
    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(@NonNull View drawerView) {

        navigationdrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);

    }

    @Override
    public void onDrawerClosed(@NonNull View drawerView) {

        navigationdrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    @Override
    public void onChapterClicked(String ChapterName, String ChapterId, String strSuperHeadingName, String strHeadingName) {
        txtChapterName.setText(ChapterName);
        getPoints(ChapterId);
    }

    @Override
    public void onMenuItemClick(View view, int itemPosition) {

    }

    @Override
    public void onMenuItemLongClick(View view, int itemPosition) {

    }

    @Override
    public void onArticleHeadingChanged(String chapterId, String chapterName, String HeadId, String loadingType, String CatId, String eCatId, String YCatId, String HeadName, String subId) {
        this.strChapterId = chapterId;
        this.strChapterName = chapterName;
        this.strSubjectId = subId;
        this.strHeadId = HeadId;
        this.strHeadName = HeadName;
    }

    private class AsyncUpdateSessionRunner extends AsyncTask<String, String, String> {

        String callingType = "";
        String timeOver = "";

        public AsyncUpdateSessionRunner(String s, String timeOver) {
            callingType = s;
            this.timeOver = timeOver;
        }

        @Override
        protected String doInBackground(String... strings) {
            ServiceRequest mRequest = new ServiceRequest(getApplicationContext());
            HashMap<String, String> params = new HashMap<>();
            params.put("session_id", strSessionId);

            if (timeOver.equalsIgnoreCase("TimeOver"))
                params.put("time", String.valueOf(packageTime));
            else
                params.put("time", String.valueOf(startingLevelTime - packageTime));
            params.put("close", callingType);

            Log.e("startingLevelTime", "" + startingLevelTime + " , packageTime : " + packageTime);
            startingLevelTime = packageTime;
            mRequest.makeServiceRequest(IConstant.updateSession, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("updateSession" + "-------- Response----------------" + response);
                    String sStatus = "";
                    String message = "";
                    try {
                        JSONObject obj = new JSONObject(response);
                        sStatus = obj.getString("status");
                        message = obj.has("message") ? obj.getString("message") : "";
                        result = sStatus;

                        if (sStatus.equalsIgnoreCase("1")) {
                            //JSONObject object = obj.getJSONObject("response");

                            sessionManager.updateTimer("0", "0");
                        } else if (sStatus.equals("00")) {
                            //customAlert.singleLoginAlertLogout();
                        } else if (sStatus.equalsIgnoreCase("01")) {
                            //customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                        } else {
                            //customAlert.showAlertOk(getString(R.string.alert_oops),message);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onErrorListener(String errorMessage) {
                    // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
                }
            });
            return result;


        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("status", result);
            if (result.equals("2")) {
                packageExpiredAlertSession(getString(R.string.packageExpired));
            }
            // new AsyncSubjectsRunner(s).execute();
        }
    }

    public FragmentRefreshListener getFragmentRefreshListener() {
        return fragmentRefreshListener;
    }

    public void setFragmentRefreshListener(FragmentRefreshListener fragmentRefreshListener) {
        this.fragmentRefreshListener = fragmentRefreshListener;
    }

    public void onBackPressed() {
        if (cTimer != null) {
            cTimer.cancel();
            cTimer = null;
        }
        isDestroyed = true;
        if (getResources().getBoolean(R.bool.isTablet)) {
            if (isLayoutOpened == 0)
                super.onBackPressed();
            else {
                new AppExecutors().mainThread().execute(new Runnable() {
                    @Override
                    public void run() {

                        Animation slidegoneAnimation = AnimationUtils.loadAnimation(TabMainActivity.this, R.anim.new_right_to_left);
                        right_tab_layout.startAnimation(slidegoneAnimation);

                        isLayoutOpened = 0;
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tab_layout_side_view_layout.getLayoutParams();
                        params.setMargins(0, 0, -800, 0); //substitute parameters for left, top, right, bottom
                        tab_layout_side_view_layout.setLayoutParams(params);

                        slide_tab_main_layout.setVisibility(View.VISIBLE);
                    }
                });
            }
        } else {
            super.onBackPressed();
        }
    }

    private void packageExpiredAlertSession(String message) {
        final Dialog alertDialog = new Dialog(getApplicationContext());
        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(getString(R.string.alert_oops));
        textViewDesc.setText(message);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (cd.isConnectingToInternet())
                    new AsyncUpdateSessionRunner("1", "update").execute();
                TabMainActivity.tabMainActivity.finish();
            }
        });
        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    private void packageExpiredAlert(String message) {
        final Dialog alertDialog = new Dialog(TabMainActivity.this);
        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(getString(R.string.alert_oops));
        textViewDesc.setText(message);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    private void updateOnTimeOver(String title, String desc) {
        final Dialog alertDialog = new Dialog(TabMainActivity.this);
        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(title);
        textViewDesc.setText(desc);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (cd.isConnectingToInternet())
                    new AsyncUpdateSessionRunner("1", "TimeOver").execute();
                finish();
            }
        });
        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    /*TabView*/

    private void setupViewPager(final ViewPager viewPager, final EventbusPojo pojo) {
        viewPagerAdapter = null;
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this);
        headDataList.clear();
        if (ScrollingActivity.headDataArrayList.size() > 0) {
            Log.e("headDataArrayList", new Gson().toJson(ScrollingActivity.headDataArrayList));
            for (int i = 0; i < ScrollingActivity.headDataArrayList.size(); i++) {
                if (ScrollingActivity.headDataArrayList.get(i).isSH.equals("Yes")) {
                    HeadData data = new HeadData();
                    data._id = ScrollingActivity.headDataArrayList.get(i).headData.get(0)._id;
                    data.short_name = ScrollingActivity.headDataArrayList.get(i).headData.get(0).short_name;
                    data.long_name = ScrollingActivity.headDataArrayList.get(i).headData.get(0).long_name;
                    data.long_name = ScrollingActivity.headDataArrayList.get(i).headData.get(0).long_name;
                    data.url = ScrollingActivity.headDataArrayList.get(i).headData.get(0).url;
                    headDataList.add(data);
                    viewPagerAdapter.addFragment(new SubscriptionArticleFragment(ScrollingActivity.headDataArrayList.get(i).headData.get(0)._id, getIntent().getStringExtra("subjectId"), ScrollingActivity.headDataArrayList.get(i).headData.get(0).long_name, i, ScrollingActivity.headDataArrayList.get(i).headData.get(0).url,Integer.parseInt(headDataList.get(0).count)), ScrollingActivity.headDataArrayList.get(i).headData.get(0).short_name);
                } else {
                    if (ScrollingActivity.headDataArrayList.get(i).headData.size() > 0) {
                        for (int j = 0; j < ScrollingActivity.headDataArrayList.get(i).headData.size(); j++) {
                            HeadData data = new HeadData();
                            data._id = ScrollingActivity.headDataArrayList.get(i).headData.get(j)._id;
                            data.short_name = ScrollingActivity.headDataArrayList.get(i).headData.get(j).short_name;
                            data.long_name = ScrollingActivity.headDataArrayList.get(i).headData.get(j).long_name;
                            data.url = ScrollingActivity.headDataArrayList.get(i).headData.get(j).url;
                            headDataList.add(data);
                            viewPagerAdapter.addFragment(new SubscriptionArticleFragment(ScrollingActivity.headDataArrayList.get(i).headData.get(j)._id, getIntent().getStringExtra("subjectId"), ScrollingActivity.headDataArrayList.get(i).headData.get(0).long_name, i, ScrollingActivity.headDataArrayList.get(i).headData.get(0).url,Integer.parseInt(headDataList.get(0).count)), ScrollingActivity.headDataArrayList.get(i).headData.get(j).short_name);
                        }
                    }
                }
            }

        }

        if (headDataList.size() > 0) {
            for (int i = 0; i < headDataList.size(); i++) {
                if (strHeadId.equals(headDataList.get(i)._id))
                    currentSelectedPosition = i;
            }
            sessionManager.updateBookMark("Article", strHeadId, getIntent().getStringExtra("subjectId"), getIntent().getStringExtra("subjectName"), new Gson().toJson(ScrollingActivity.headDataArrayList), getIntent().getStringExtra("chapter_id"), getIntent().getStringExtra("chapter_name"), getIntent().hasExtra("superHeading") ? getIntent().getStringExtra("superHeading") : "No", "", "");
        }
        viewPager.setAdapter(viewPagerAdapter);

        tab_layout.setupWithViewPager(viewPager);
        for (int i = 0; i < tab_layout.getTabCount(); i++) {
            TabLayout.Tab tab = tab_layout.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(viewPagerAdapter.getTabView(i));
            }
        }
        setGroupAdapter(currentSelectedPosition);
        viewPager.setCurrentItem(currentSelectedPosition);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setGroupAdapter(position);
                layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
                int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                int centeredItemPosition = totalVisibleItems / 2;
                recyclerViewGroups.smoothScrollToPosition(position != 0 ? position - 1 : position);
                strHeadId = headDataList.get(position)._id;
                strHeadName = headDataList.get(position).short_name;
                strLongName = headDataList.get(position).long_name;
                sessionManager.updateBookMark("Article", strHeadId, getIntent().getStringExtra("subjectId"), getIntent().getStringExtra("subjectName"), new Gson().toJson(ScrollingActivity.headDataArrayList), getIntent().getStringExtra("chapter_id"), getIntent().getStringExtra("chapter_name"), getIntent().hasExtra("superHeading") ? getIntent().getStringExtra("superHeading") : "No", "", "");
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setupPointsViewPager(final ViewPager viewPager, final EventbusPojo pojo) {
        viewPagerAdapter = null;
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(),
                this);
        headDataList.clear();
        if (SubscriptionPointsActivity.pointsContentArrayList.size() > 0) {
            for (int i = 0; i < SubscriptionPointsActivity.pointsContentArrayList.size(); i++) {
                HeadData data = new HeadData();
                data._id = SubscriptionPointsActivity.pointsContentArrayList.get(i).get_id();
                data.short_name = SubscriptionPointsActivity.pointsContentArrayList.get(i).getShort_name();
                data.long_name = SubscriptionPointsActivity.pointsContentArrayList.get(i).getLong_name();
                data.count = SubscriptionPointsActivity.pointsContentArrayList.get(i).getCount();
                headDataList.add(data);
                viewPagerAdapter.addFragment(new SubscriptionPointsFragment(strSubjectId, strChapterId, SubscriptionPointsActivity.pointsContentArrayList.get(i).get_id(), SubscriptionPointsActivity.pointsContentArrayList.get(i).getLong_name(), SubscriptionPointsActivity.pointsContentArrayList.get(i).getContent(), SubscriptionPointsActivity.pointsContentArrayList.get(i).getPointsTitleContentList(), SubscriptionPointsActivity.pointsContentArrayList.get(i).getUrl(),0), SubscriptionPointsActivity.pointsContentArrayList.get(i).getShort_name());
                if (pojo.getHeadId().equalsIgnoreCase(SubscriptionPointsActivity.pointsContentArrayList.get(i).get_id()))
                    currentSelectedPosition = i;

            }
        }
        setGroupAdapter(currentSelectedPosition);
        layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
        int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
        int centeredItemPosition = totalVisibleItems / 2;
        recyclerViewGroups.smoothScrollToPosition(currentSelectedPosition != 0 ? currentSelectedPosition - 1 : currentSelectedPosition);
        viewPager.setAdapter(viewPagerAdapter);

        tab_layout.setupWithViewPager(viewPager);
        for (int i = 0; i < tab_layout.getTabCount(); i++) {
            TabLayout.Tab tab = tab_layout.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(viewPagerAdapter.getTabView(i));
            }
        }

        viewPager.setCurrentItem(currentSelectedPosition);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setGroupAdapter(position);
                layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
                int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                int centeredItemPosition = totalVisibleItems / 2;
                recyclerViewGroups.smoothScrollToPosition(position != 0 ? position - 1 : position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setupMCQViewPager(final ViewPager viewPager, final EventbusPojo pojo) {
        viewPagerAdapter = null;
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this);
        headDataList.clear();
        if (pojo.getMcqLoadingType().equals("1")) {
            if (SubscriptionDetailActivity.mcqHeadList.size() > 0) {
                for (int i = 0; i < SubscriptionDetailActivity.mcqHeadList.size(); i++) {
                    if (SubscriptionDetailActivity.mcqHeadList.get(i).isSH.equals("Yes")) {

                        HeadData data = new HeadData();
                        data._id = SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(0)._id;
                        data.short_name = SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(0).short_name;
                        data.long_name = SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(0).long_name;
                        headDataList.add(data);
                        viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(strSubjectId, strChapterId, SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(0)._id, SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(0).long_name, pojo.getMcqLoadingType()), SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(0).short_name);


                    } else {
                        if (SubscriptionDetailActivity.mcqHeadList.get(i).headData.size() > 0) {
                            for (int j = 0; j < SubscriptionDetailActivity.mcqHeadList.get(i).headData.size(); j++) {

                                HeadData data = new HeadData();
                                data._id = SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(j)._id;
                                data.short_name = SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(j).short_name;
                                data.long_name = SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(j).long_name;
                                headDataList.add(data);
                                viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(strSubjectId, strChapterId, SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(j)._id, SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(j).long_name, pojo.getMcqLoadingType()), SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(j).short_name);
                            }
                        }
                    }
                }
            }
        } else if (pojo.getMcqLoadingType().equals("2") || pojo.getMcqLoadingType().equals("3")) {
            if (SubscriptionDetailActivity.mcqExamArrayList.size() > 0) {
                for (int i = 0; i < SubscriptionDetailActivity.mcqExamArrayList.size(); i++) {
                    HeadData data = new HeadData();
                    data._id = SubscriptionDetailActivity.mcqExamArrayList.get(i).get_id();
                    data.short_name = SubscriptionDetailActivity.mcqExamArrayList.get(i).getShort_name();
                    data.long_name = SubscriptionDetailActivity.mcqExamArrayList.get(i).getLong_name();
                    headDataList.add(data);
                    viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(strSubjectId, strChapterId, SubscriptionDetailActivity.mcqExamArrayList.get(i).get_id(), SubscriptionDetailActivity.mcqExamArrayList.get(i).getLong_name(), pojo.getMcqLoadingType()), SubscriptionDetailActivity.mcqExamArrayList.get(i).getLong_name());

                }

            }
            Log.e("HeadData", headDataList.toString());
        }


        if (headDataList.size() > 0) {
            for (int i = 0; i < headDataList.size(); i++) {
                if (strHeadId.equalsIgnoreCase(headDataList.get(i)._id))
                    currentSelectedPosition = i;
            }
        }
        setGroupAdapter(currentSelectedPosition);
        layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
        int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
        int centeredItemPosition = totalVisibleItems / 2;
        recyclerViewGroups.smoothScrollToPosition(currentSelectedPosition != 0 ? currentSelectedPosition - 1 : currentSelectedPosition);
        recyclerViewGroups.setScrollY(centeredItemPosition);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(currentSelectedPosition);
        tab_layout.setupWithViewPager(viewPager);
        for (int i = 0; i < tab_layout.getTabCount(); i++) {
            TabLayout.Tab tab = tab_layout.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(viewPagerAdapter.getTabView(i));
            }
        }

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setGroupAdapter(position);
                layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
                int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                int centeredItemPosition = totalVisibleItems / 2;
                recyclerViewGroups.smoothScrollToPosition(position != 0 ? position - 1 : position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    /*private void setPointsGroupAdapter(int position) {
        homeGroupNameAdapter = new ArticleTabsAdapter(headDataList, getActivity(),position);
        recyclerViewGroups.setLayoutManager(new LinearLayoutManager(getActivity(),RecyclerView.HORIZONTAL,false));
        snapHelper.attachToRecyclerView(recyclerViewGroups);
        recyclerViewGroups.setAdapter(homeGroupNameAdapter);

        recyclerViewGroups.scrollToPosition(position);
        homeGroupNameAdapter.setOnGroupMenuSelectedListener(new OnMobileHomeMenuSelectedListioner() {
            @Override
            public void onGroupSelected(String groupId, int position) {
                viewPager.setCurrentItem(position);
                layoutManager = ((LinearLayoutManager)recyclerViewGroups.getLayoutManager());
                int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                int centeredItemPosition = totalVisibleItems / 2;
                recyclerViewGroups.smoothScrollToPosition(position != 0 ?position-1 : position);
                recyclerViewGroups.setScrollY(centeredItemPosition);
            }
        });

    }*/
    private void setGroupAdapter(final int position) {

        homeGroupNameAdapter = new ArticleTabsAdapter(headDataList, TabMainActivity.this, position);
        recyclerViewGroups.setLayoutManager(new LinearLayoutManager(TabMainActivity.this, RecyclerView.HORIZONTAL, false));
        snapHelper.attachToRecyclerView(recyclerViewGroups);
        recyclerViewGroups.setAdapter(homeGroupNameAdapter);

        recyclerViewGroups.scrollToPosition(position);
        homeGroupNameAdapter.setOnGroupMenuSelectedListener(new OnMobileHomeMenuSelectedListioner() {
            @Override
            public void onGroupSelected(String groupId, int position) {
                viewPager.setCurrentItem(position);
                layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
                int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                int centeredItemPosition = totalVisibleItems / 2;
                recyclerViewGroups.smoothScrollToPosition(position != 0 ? position - 1 : position);
                recyclerViewGroups.setScrollY(centeredItemPosition);
            }
        });

    }

    private void ButtonClickListener() {

        slider_layout_drawer_icon.setOnClickListener(this);
        close_child_fragment_drawer.setOnClickListener(this);
        close_slide_tap_layout.setOnClickListener(this);
        slide_open_imageview.setOnClickListener(this);
    }

    private void OnclickListener() {


        new SwipeDetector(slide_menu_left_view).setOnSwipeListener(new SwipeDetector.onSwipeEvent() {
            @Override
            public void SwipeEventDetected(View v, SwipeDetector.SwipeTypeEnum swipeType) {

                if (swipeType == SwipeDetector.SwipeTypeEnum.LEFT_TO_RIGHT) {

                    new AppExecutors().mainThread().execute(new Runnable() {
                        @Override
                        public void run() {

                            Animation slidegoneAnimation = AnimationUtils.loadAnimation(TabMainActivity.this, R.anim.new_right_to_left);
                            right_tab_layout.startAnimation(slidegoneAnimation);

                            isLayoutOpened = 0;
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tab_layout_side_view_layout.getLayoutParams();
                            params.setMargins(0, 0, -800, 0); //substitute parameters for left, top, right, bottom
                            tab_layout_side_view_layout.setLayoutParams(params);

                            slide_tab_main_layout.setVisibility(View.VISIBLE);
                        }
                    });


                } else if (swipeType == SwipeDetector.SwipeTypeEnum.RIGHT_TO_LEFT) {


                    new AppExecutors().mainThread().execute(new Runnable() {
                        @Override
                        public void run() {
                            isLayoutOpened = 1;
                            slide_tab_main_layout.setVisibility(View.GONE);

                            Animation slidegoneAnimation = AnimationUtils.loadAnimation(TabMainActivity.this, R.anim.new_left_to_right);
                            right_tab_layout.startAnimation(slidegoneAnimation);

                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tab_layout_side_view_layout.getLayoutParams();
                            params.setMargins(0, 0, 0, 0); //substitute parameters for left, top, right, bottom
                            tab_layout_side_view_layout.setLayoutParams(params);

                        }
                    });

                }
            }
        });


        new SwipeDetector(slide_menu_right_view).setOnSwipeListener(new SwipeDetector.onSwipeEvent() {
            @Override
            public void SwipeEventDetected(View v, final SwipeDetector.SwipeTypeEnum swipeType) {

                new AppExecutors().mainThread().execute(new Runnable() {
                    @Override
                    public void run() {

                        if (swipeType == SwipeDetector.SwipeTypeEnum.LEFT_TO_RIGHT) {

                            swip_right_slienr_layout.setVisibility(View.GONE);
                            close_slide_tap_layout.setVisibility(View.GONE);

                        } else if (swipeType == SwipeDetector.SwipeTypeEnum.RIGHT_TO_LEFT) {
                            mcqFromArticle();
                            swip_right_slienr_layout.setVisibility(View.VISIBLE);

                            Animation slideAnimation = AnimationUtils.loadAnimation(TabMainActivity.this, R.anim.new_left_to_right);
                            swip_right_slienr_layout.startAnimation(slideAnimation);

                            close_slide_tap_layout.setVisibility(View.VISIBLE);

                        }

                    }
                });

            }
        });

    }

    private void mcqFromArticle() {

        articleFromMCQFragment = new SubscrptionDetailListFragment(strSubjectId, strChapterId, strHeadId, strHeadName, "1");
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameContainerMCQ, articleFromMCQFragment);
        fragmentTransaction.commit();

    }

    private void SlideOpenCloseFunction() {

        new AppExecutors().mainThread().execute(new Runnable() {
            @Override
            public void run() {

                if (slide_tab_main_layout.getVisibility() == View.VISIBLE) {

                    slide_tab_main_layout.setVisibility(View.GONE);

                    Animation slidegoneAnimation = AnimationUtils.loadAnimation(TabMainActivity.this, R.anim.new_left_to_right);
                    right_tab_layout.startAnimation(slidegoneAnimation);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tab_layout_side_view_layout.getLayoutParams();
                    params.setMargins(0, 0, 0, 0); //substitute parameters for left, top, right, bottom
                    tab_layout_side_view_layout.setLayoutParams(params);

                } else {

                    Animation slidegoneAnimation = AnimationUtils.loadAnimation(TabMainActivity.this, R.anim.new_right_to_left);
                    right_tab_layout.startAnimation(slidegoneAnimation);

                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tab_layout_side_view_layout.getLayoutParams();
                    params.setMargins(0, 0, -800, 0); //substitute parameters for left, top, right, bottom
                    tab_layout_side_view_layout.setLayoutParams(params);

                    slide_tab_main_layout.setVisibility(View.VISIBLE);
                }
            }
        });


    }

    private void CloseSlideTabLayout() {

        swip_right_slienr_layout.setVisibility(View.GONE);
        close_slide_tap_layout.setVisibility(View.GONE);
    }

    private void SliderFragmentDrwerOpen() {

        navigationdrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        navigationdrawer.openDrawer(slider_fragment_drawer);
    }

    private void SliderFragmentDrawerClose() {

        navigationdrawer.closeDrawer(slider_fragment_drawer);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        List<Fragment> fragmentList = new ArrayList<>();
        List<String> fragmentTitles = new ArrayList<>();
        Context mContext;
        TextView tabTitile;


        ViewPagerAdapter(FragmentManager fragmentManager,
                         FragmentActivity activity) {
            super(fragmentManager);
            mContext = activity;
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitles.get(position);
        }

        void addFragment(Fragment fragment, String name) {
            fragmentList.add(fragment);
            fragmentTitles.add(name);
        }

        View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_items, null);
            tabTitile = v.findViewById(R.id.txtTabTitle);
            tabTitile.setText(fragmentTitles.get(position));
            tabTitile.setTextColor(getResources().getColor(R.color.white));
            return v;
        }
    }

    private void SlideTapItemLoadAdapterClass() {

        try {

            List s = Arrays.asList(getResources().getStringArray(R.array.question_title));

            for (int i = 0; i < s.size(); i++) {

                SlideTapChildFragmentRecyclerPojo pojo = new SlideTapChildFragmentRecyclerPojo();

                pojo.setQuestion_Title(s.get(i).toString());
                slide_tap_list.add(pojo);
            }


            slide_tap_fragment_recycler_view.setLayoutManager(new LinearLayoutManager(mContext,
                    RecyclerView.VERTICAL, false));

            adapter = new SlideTabChildFragmentRecycleAdapter(mContext, slide_tap_list, TabMainActivity.this);
            slide_tap_fragment_recycler_view.setAdapter(adapter);


        } catch (Exception e) {

            System.out.println("Exception--" + e.toString());
        }

    }

    private void FeedbackDialogShow() {

        feedback_dialog.dialogshow();
    }

    @Override
    public void onParentItemClick(int position) {

        dialog.dialogshow();
        Log.e("onParentItemClick", "" + position);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(EventbusPojo pojo) {

        String name = pojo.getFragment_name();

        strHeadId = pojo.getHeadId();

        textView.setText(getIntent().getStringExtra("subjectName"));
        textView2.setText(pojo.getChapterName());
        textView3.setText(pojo.getSuperHeadingName());
        /*if (name.equalsIgnoreCase("Article")) {
            fragment = new TabContentMainFragment(strHeadId,pojo.getChapterId(),pojo.getChapterName(),getIntent().getStringExtra("subjectName"),getIntent().getStringExtra("subjectId"),pojo.getIsSuperHeading() != null && pojo.getIsSuperHeading().equalsIgnoreCase("Yes")?pojo.getSuperHeadingName():"No");
        }else if(name.equalsIgnoreCase("Points"))
        {
            fragment = new TabPointsMainFragment(strHeadId,pojo.getChapterId(),getIntent().getStringExtra("subjectId"),pojo.getChapterName(),getIntent().getStringExtra("subjectName"));
        }else if(name.equalsIgnoreCase("MCQ"))
        {
            fragment = new TabMcqMainFragment(strHeadId,pojo.getChapterId(),getIntent().getStringExtra("subjectId"),pojo.getChapterName(),getIntent().getStringExtra("subjectName"),pojo.getMcqLoadingType(),pojo.getIsSuperHeading() != null && pojo.getIsSuperHeading().equalsIgnoreCase("Yes")?pojo.getSuperHeadingName():"No");
        }else if(name.equalsIgnoreCase("Test"))
        {
            fragment = new TabTestMainFragment(getIntent().getStringExtra("subjectId"),pojo.getChapterId(),pojo.getChapterName(),getIntent().getStringExtra("subjectName"),strHeadId,pojo.getType(),pojo.getNumOfquestion(),pojo.getIsFinished(),pojo.getQuestionType(),pojo.getInstruction(),pojo.getLongname(),pojo.getC_a_m(),pojo.getW_a_m());
        }

        fragmentManager.getFragments().clear();
        fragmentTransaction.add(R.id.frameContainer, fragment);
        fragmentTransaction.commit();*/
        setArticlePage(pojo);
    }

    private void setArticlePage(EventbusPojo pojo) {
        side_menu_viewpager = (ViewPager) findViewById(R.id.viewpager_subscription_category);
        currentSelectedPosition = 0;
        if (pojo.getFragment_name().equalsIgnoreCase("Article"))
            setupViewPager(side_menu_viewpager, pojo);
        else if (pojo.getFragment_name().equalsIgnoreCase("Points"))
            setupPointsViewPager(side_menu_viewpager, pojo);
        else if (pojo.getFragment_name().equalsIgnoreCase("MCQ"))
            setupMCQViewPager(side_menu_viewpager, pojo);
        else if (pojo.getFragment_name().equalsIgnoreCase("Test"))
            setupViewPager(side_menu_viewpager, pojo);
    }

    private void getPoints(String chapterId) {

        mPointsList.clear();
        ServiceRequest mRequest = new ServiceRequest(TabMainActivity.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("subject_id", getIntent().getStringExtra("subjectId"));
        params.put("chapter_id", chapterId);

        mRequest.makeServiceRequest(IConstant.pointsContent, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------pointsContent Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";

                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        JSONArray jsonArray = object.getJSONArray("points");

                        if (jsonArray.length() > 0) {
                            for (int j = jsonArray.length() - 1; j >= 0; j--) {
                                PointsContent pointsContent = new PointsContent();
                                pointsContent.set_id(jsonArray.getJSONObject(j).getString("_id"));
                                pointsContent.setShort_name(jsonArray.getJSONObject(j).getString("short_name"));
                                pointsContent.setLong_name(jsonArray.getJSONObject(j).getString("long_name"));
                                pointsContent.setContent(jsonArray.getJSONObject(j).getString("content"));
//                    pointsContent.setStatus(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("status"));
//                    pointsContent.setCstatus(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("cstatus"));
                                pointsContent.setPoint_name(jsonArray.getJSONObject(j).getString("point_name"));
                                mPointsList.add(pointsContent);
                            }


                            // mCycleMenuWidget.setMenuRes(R.menu.points_menu,mPointsList.size());
                            //mCycleMenuWidget.open(true);
                        }

                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });

    }


    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(TestActivityLastReadDetails event) {


        Dialog alertDialog;
        alertDialog = new Dialog(TabMainActivity.this);
        //  alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);
//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(getString(R.string.alert_incomplete));
        textViewDesc.setText(getString(R.string.alert_incomplete_message));
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnOk.setText("Continue");
        btnCancel.setText("New Test");
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                System.out.println("ok type-->" + sessionManager.getIncompleteTest().get(SessionManager.TESTTYPE));
                startActivity(new Intent(TabMainActivity.this, collMainActivity.class)

                        .putExtra("subject_id", sessionManager.getIncompleteTest().get(SessionManager.SUBJECT_ID))
                        .putExtra("chapter_id", sessionManager.getIncompleteTest().get(SessionManager.CHAPTER_ID))
                        .putExtra("chapter_name", sessionManager.getIncompleteTest().get(SessionManager.CHAPTER_NAME1))
                        .putExtra("subject_name", sessionManager.getIncompleteTest().get(SessionManager.SUBJECT_NAME1))
                        .putExtra("pending", "1")
                        .putExtra("pendingTestId", sessionManager.getIncompleteTest().get(SessionManager.PENDINGTESTID))
                        .putExtra("testId", sessionManager.getIncompleteTest().get(SessionManager.TESTID))
                        .putExtra("type", sessionManager.getIncompleteTest().get(SessionManager.TESTTYPE))
                        .putExtra("no_of_question", sessionManager.getIncompleteTest().get(SessionManager.NO_OF_QUESTION))
                        .putExtra("isFinished", sessionManager.getIncompleteTest().get(SessionManager.IS_FINISHED))
                        .putExtra("questionType", sessionManager.getIncompleteTest().get(SessionManager.QUESTIONTYPE))
                        .putExtra("strInstruction", sessionManager.getIncompleteTest().get(SessionManager.STRINGSTRUCTION))
                        .putExtra("strLongName", sessionManager.getIncompleteTest().get(SessionManager.STRLONGNAME))
                        .putExtra("crt_ans_mark", sessionManager.getIncompleteTest().get(SessionManager.CRT_ANS_MARK))
                        .putExtra("wrng_ans_mark", sessionManager.getIncompleteTest().get(SessionManager.WRNG_ANS_MARK))
                );


            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("cancel type-->" + event.getType());
                alertDialog.dismiss();
                SubscriptionTestFragment.isPending = "-1";
                startActivity(new Intent(TabMainActivity.this, collMainActivity.class)
                        .putExtra("subject_id", event.getSubject_id())
                        .putExtra("chapter_id", event.getChapter_id())
                        .putExtra("chapter_name", event.getChapter_name())
                        .putExtra("subject_name", event.getSubject_name())
                        .putExtra("pending", "0")
                        .putExtra("pendingTestId", "")
                        .putExtra("testId", event.getTestId())
                        .putExtra("type", event.getType())
                        .putExtra("no_of_question", event.getNo_of_question())
                        .putExtra("isFinished", event.getIsFinished())
                        .putExtra("questionType", event.getQuestionType())
                        .putExtra("strInstruction", event.getStrInstruction())
                        .putExtra("strLongName", event.getStrLongName())
                        .putExtra("crt_ans_mark", event.getCrt_ans_mark())
                        .putExtra("wrng_ans_mark", event.getWrng_ans_mark())
                );

            }
        });

        alertDialog.show();
    }





}
