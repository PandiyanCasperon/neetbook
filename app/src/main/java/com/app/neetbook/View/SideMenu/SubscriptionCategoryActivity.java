package com.app.neetbook.View.SideMenu;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.app.neetbook.R;
import com.app.neetbook.Utils.ActionBarActivityNeetBook;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

public class SubscriptionCategoryActivity extends ActionBarActivityNeetBook implements View.OnClickListener {

  LinearLayout llArticleActive,llPointsActive,llMCQActive,llTest,llMCQ,llPoints,llArticle,llTestActive;
  String strSubjectId;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_subscription_category);
//    BottomNavigationView navView = findViewById(R.id.nav_view);
//
//    NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
//    NavigationUI.setupWithNavController(navView, navController);

    init();

  }

  private void init() {
    llArticle = findViewById(R.id.llArticle);
    llArticleActive = findViewById(R.id.llArticleActive);
    llPoints = findViewById(R.id.llPoints);
    llPointsActive = findViewById(R.id.llPointsActive);
    llMCQ = findViewById(R.id.llMCQ);
    llMCQActive = findViewById(R.id.llMCQActive);
    llTest = findViewById(R.id.llTest);
    llTestActive = findViewById(R.id.llTestActive);

    strSubjectId = getIntent().getStringExtra("subjectId");
    llArticle.setOnClickListener(this);
    llArticleActive.setOnClickListener(this);
    llPoints.setOnClickListener(this);
    llPointsActive.setOnClickListener(this);
    llMCQ.setOnClickListener(this);
    llMCQActive.setOnClickListener(this);
    llTest.setOnClickListener(this);
    llTestActive.setOnClickListener(this);
    initialView();
  }

  private void initialView() {
    llArticle.setVisibility(View.VISIBLE);
    llArticleActive.setVisibility(View.GONE);
    llPoints.setVisibility(View.VISIBLE);
    llPointsActive.setVisibility(View.GONE);
    llMCQ.setVisibility(View.VISIBLE);
    llMCQActive.setVisibility(View.GONE);
    llTest.setVisibility(View.VISIBLE);
    llTestActive.setVisibility(View.GONE);
    //loadFragment(new ArticleFragment(getIntent().getStringExtra("subjectId"),getIntent().getStringExtra("subjectName")));
  }

  //  public void updateStatusBarColor(String color) {// Color must be in hexadecimal fromat
  public void updateStatusBarColor(int color) {// Color must be in hexadecimal fromat
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      Window window = getWindow();
      window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
      window.setStatusBarColor(color);
    }
  }

  @Override
  public void onClick(View v) {
    switch (v.getId())
    {
      case R.id.llArticle:
        setSelection("1");
        break;

      case R.id.llArticleActive:
        setSelection("1A");
        break;
      case R.id.llPoints:
        setSelection("2");
        break;
      case R.id.llPointsActive:
        setSelection("2A");
        break;
      case R.id.llMCQ:
        setSelection("3");
        break;
      case R.id.llMCQActive:
        setSelection("3A");
        break;
      case R.id.llTest:
        setSelection("4");
        break;
      case R.id.llTestActive:
        setSelection("4A");
        break;
    }
  }

  private void setSelection(String i) {
    Fragment fragment = null;

    switch (i)
    {
      case "1":
        llArticle.setVisibility(View.VISIBLE);
        llArticleActive.setVisibility(View.GONE);
        llPoints.setVisibility(View.VISIBLE);
        llPointsActive.setVisibility(View.GONE);
        llMCQ.setVisibility(View.VISIBLE);
        llMCQActive.setVisibility(View.GONE);
        llTest.setVisibility(View.VISIBLE);
        llTestActive.setVisibility(View.GONE);
       // fragment = new ArticleFragment(getIntent().getStringExtra("subjectId"),getIntent().getStringExtra("subjectName"));
        break;
      case "1A":

        break;
      case "2":
        llArticle.setVisibility(View.VISIBLE);
        llArticleActive.setVisibility(View.GONE);
        llPoints.setVisibility(View.VISIBLE);
        llPointsActive.setVisibility(View.GONE);
        llMCQ.setVisibility(View.VISIBLE);
        llMCQActive.setVisibility(View.GONE);
        llTest.setVisibility(View.VISIBLE);
        llTestActive.setVisibility(View.GONE);
        //fragment = new PointsFragment(getIntent().getStringExtra("subjectId"),getIntent().getStringExtra("subjectName"));
        break;
      case "2A":

        break;
      case "3":
        llArticle.setVisibility(View.VISIBLE);
        llArticleActive.setVisibility(View.GONE);
        llPoints.setVisibility(View.VISIBLE);
        llPointsActive.setVisibility(View.GONE);
        llMCQ.setVisibility(View.VISIBLE);
        llMCQActive.setVisibility(View.GONE);
        llTest.setVisibility(View.VISIBLE);
        llTestActive.setVisibility(View.GONE);
        //fragment = new McqFragment(getIntent().getStringExtra("subjectId"),getIntent().getStringExtra("subjectName"));
        break;
      case "3A":

        break;
      case "4":
        llArticle.setVisibility(View.VISIBLE);
        llArticleActive.setVisibility(View.GONE);
        llPoints.setVisibility(View.VISIBLE);
        llPointsActive.setVisibility(View.GONE);
        llMCQ.setVisibility(View.VISIBLE);
        llMCQActive.setVisibility(View.GONE);
        llTest.setVisibility(View.VISIBLE);
        llTestActive.setVisibility(View.GONE);
        //fragment = new TestFragment(getIntent().getStringExtra("subjectId"),getIntent().getStringExtra("subjectName"));
        break;

      case "4A":

        break;
    }

    loadFragment(fragment);
  }

  private boolean loadFragment(Fragment fragment) {
    //switching fragment
    if (fragment != null) {
      getSupportFragmentManager()
              .beginTransaction()
              .replace(R.id.fragment_container, fragment)
              .commit();
      return true;
    }
    return false;
  }
}
