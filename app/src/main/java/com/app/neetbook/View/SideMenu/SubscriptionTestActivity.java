package com.app.neetbook.View.SideMenu;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.neetbook.Interfaces.OnSubmitListioner;
import com.app.neetbook.Interfaces.OnTestAttemp;
import com.app.neetbook.Model.TestList;
import com.app.neetbook.Model.sidemenuFromContent.SubscriptionTest;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ActionBarActivityNeetBook;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Data.ChangeMobileData;
import com.app.neetbook.Utils.Data.ChangePinData;
import com.app.neetbook.Utils.Data.CompleteProfileData;
import com.app.neetbook.Utils.Data.ContactUsData;
import com.app.neetbook.Utils.Data.DrawerActivityData;
import com.app.neetbook.Utils.Data.LoginData;
import com.app.neetbook.Utils.Data.MyProfileData;
import com.app.neetbook.Utils.OnSwipeTouchListener;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ImageViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class SubscriptionTestActivity extends ActionBarActivityNeetBook implements View.OnClickListener, OnTestAttemp {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Context context;
    SessionManager sessionManager;
    private String subject_id = "", chapter_id = "", chapter_name = "", subject_name = "", testId = "";
    private String type = "", no_of_question = "", isFinished = "", questionType = "", strInstruction = "", strLongName = "", crt_ans_mark = "", wrng_ans_mark = "";
    /*public static ArrayList<TestList> testArrayList;*/
    private TextView textView, textView2Pin;
    Typeface tfBold, tfMedium, tfRegular;
    LinearLayout llParent, llResult, llSecondRowRightHalf;
    private TextView txtStopwatch;
    private TextView txtQACount;
    RelativeLayout rlSubmit, rlInstruction, rlTimer;
    private int selectedIndex = 0;
    ImageView imgSubmit, imgSubmitInActive, imageView2, imgInstruction;
    RelativeLayout rlParent;
    private ImageView imgTimer, imgCloseInstruction;
    private TextView textView3, textView4, textView5, textView6, textView7, textView8, textView9, textView10, textViewInstruction, txtEmoji;
    private TextView txtTestTitle;
    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L;
    Handler handler, handlerShowTimer;
    private ConstraintLayout constrainInstruction;
    int Seconds, Minutes, MilliSeconds;
    private OnSubmitListioner onSubmitListioner;
    ImageView imgClose;
    Typeface tf;
    int increment = 0;
    int madechanges = 0;

    Toolbar toolbar;
    AppBarLayout appBarLayout;
    CollapsingToolbarLayout collapsing_toolbar_test;
    private Dialog alertDialog;
    private String pending, pendingTestId;
    private String is_coming_page = "";

    public SubscriptionTestActivity() {
        // Required empty public constructor
    }

    //
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_test);
        StatusBarColorChange.updateStatusBarColor(SubscriptionTestActivity.this, getResources().getColor(R.color.test_completed_brown));
    /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
            WindowManager.LayoutParams.FLAG_SECURE);*/
        viewPager = findViewById(R.id.viewpager_subscription_category);
        llSecondRowRightHalf = findViewById(R.id.llSecondRowRightHalf);
        tabLayout = findViewById(R.id.tabs_subscription_category);
        textView = findViewById(R.id.textView);
        rlParent = findViewById(R.id.rlParent);
        imgInstruction = findViewById(R.id.imgInstruction);
        llResult = findViewById(R.id.llResult);
        imageView2 = findViewById(R.id.imageView2);
        imgSubmit = findViewById(R.id.imgSubmit);
        imgSubmitInActive = findViewById(R.id.imgSubmitInActive);
        txtTestTitle = findViewById(R.id.txtTestTitle);

        textView2Pin = findViewById(R.id.textView2Pin);
        txtQACount = findViewById(R.id.txtQACount);
        txtStopwatch = findViewById(R.id.txtStopwatch);
        llParent = findViewById(R.id.llParent);
        rlSubmit = findViewById(R.id.rlSubmit);
        rlTimer = findViewById(R.id.rlTimer);
        imgTimer = findViewById(R.id.imgTimer);
        textViewInstruction = findViewById(R.id.textViewInstruction);
        rlInstruction = findViewById(R.id.rlInstruction);
        imgCloseInstruction = findViewById(R.id.imgCloseInstruction);
        constrainInstruction = findViewById(R.id.constrainInstruction);
        textView3 = findViewById(R.id.textView3);
        textView4 = findViewById(R.id.textView4);
        textView5 = findViewById(R.id.textView5);
        textView6 = findViewById(R.id.textView6);
        textView7 = findViewById(R.id.textView7);
        textView8 = findViewById(R.id.textView8);
        textView10 = findViewById(R.id.textView10);
        txtEmoji = findViewById(R.id.txtEmoji);
        textView9 = findViewById(R.id.textView9);
        collapsing_toolbar_test =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_test);
        if (getIntent().getExtras() != null) {
            subject_id = getIntent().getStringExtra("subject_id");
            chapter_id = getIntent().getStringExtra("chapter_id");
            chapter_name = getIntent().getStringExtra("chapter_name");
            subject_name = getIntent().getStringExtra("subject_name");
            testId = getIntent().getStringExtra("testId");
            type = getIntent().getStringExtra("type");
            no_of_question = getIntent().getStringExtra("no_of_question");
            isFinished = getIntent().getStringExtra("isFinished");
            questionType = getIntent().getStringExtra("questionType");
            strInstruction = getIntent().getStringExtra("strInstruction");
            strLongName = getIntent().getStringExtra("strLongName");
            crt_ans_mark = getIntent().getStringExtra("crt_ans_mark");
            wrng_ans_mark = getIntent().getStringExtra("wrng_ans_mark");
            pending = getIntent().getStringExtra("pending");
            pendingTestId = getIntent().getStringExtra("pendingTestId");

            if (getIntent().getExtras().containsKey("coming_page")) {

                is_coming_page = getIntent().getStringExtra("coming_page");
            }
        }
        sessionManager = new SessionManager(SubscriptionTestActivity.this);
        sessionManager.updateBookMark("Test", chapter_id, subject_id, subject_name, "", chapter_id, chapter_name, "", "", "");
        sessionManager.updateBookMarkTest(testId, type, no_of_question, isFinished, questionType, strInstruction, strLongName, crt_ans_mark, wrng_ans_mark);


        sessionManager.IncompleteTest(subject_id, chapter_id, chapter_name, subject_name, pending,
                pendingTestId, testId, type, no_of_question, isFinished, questionType, strInstruction, strLongName, crt_ans_mark, wrng_ans_mark);


        sessionManager.LastReadSubjectBookmark(sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME));

        txtTestTitle.setText(strLongName);
        textViewInstruction.setText(strInstruction);
        visibleHideSubmit();
        setColapsingToolBar();
        listener();
        setFontSize();
        setFragment();
        startStopwatch();
        showHideInstructio();
        //setupViewPager(viewPager);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setting the title
        toolbar.setTitle("My Toolbar");

        //placing toolbar in place of actionbar
        setSupportActionBar(toolbar);


    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }

    private void visibleHideSubmit() {
        imgSubmitInActive.setVisibility(pending != null && pending.equals("0") ? View.VISIBLE : View.GONE);
        imgSubmit.setVisibility(pending != null && pending.equals("1") ? View.VISIBLE : View.GONE);
    }

    private void setColapsingToolBar() {
        imgClose = findViewById(R.id.imgClose);
        appBarLayout = findViewById(R.id.appBarLayout);
        collapsing_toolbar_test.setTitle(strLongName);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_coming_page.equals("homepage")) {

                    Intent s = new Intent(getApplicationContext(), TabMainActivity.class);
                    s.putExtra("subjectId", subject_id);
                    s.putExtra("subjectName", sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_NAME));
                    s.putExtra("isTrial", sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_TRIAL_STATUS));
                    s.putExtra("subjectShortName", sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_SHORT_NAME));
                    s.putExtra("showing_page_position", "3");
                    startActivity(s);
                    finish();
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                } else {
                    EventBus.getDefault().post("close");

                }
            }
        });

        if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
            collapsing_toolbar_test.setExpandedTitleTextAppearance(R.style.expandedappbarSmall);
            collapsing_toolbar_test.setCollapsedTitleTextAppearance(R.style.collapsedappbarSmall);
        } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
            collapsing_toolbar_test.setExpandedTitleTextAppearance(R.style.expandedappbarMedium);
            collapsing_toolbar_test.setCollapsedTitleTextAppearance(R.style.collapsedappbarMedium);
        } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
            collapsing_toolbar_test.setExpandedTitleTextAppearance(R.style.expandedappbarLarge);
            collapsing_toolbar_test.setCollapsedTitleTextAppearance(R.style.collapsedappbarLarge);
        }
        tf = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaBold.otf");
        collapsing_toolbar_test.setCollapsedTitleTypeface(tf);
        collapsing_toolbar_test.setExpandedTitleTypeface(tf);


        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                textView.setAlpha(1.0f - Math.abs(verticalOffset / (float)
                        appBarLayout.getTotalScrollRange()));
                System.out.println("--b===" + Math.abs(verticalOffset / (float)
                        appBarLayout.getTotalScrollRange()));

                imgInstruction.setAlpha(1.0f - Math.abs(verticalOffset / (float)
                        appBarLayout.getTotalScrollRange()));
        /*textView2Pin.setAlpha(1.0f - Math.abs(verticalOffset / (float)
                appBarLayout.getTotalScrollRange()));*/

                ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) rlSubmit.getLayoutParams();


                if (Math.abs(verticalOffset) - appBarLayout.getTotalScrollRange() == 0) {
                    //  Collapsed
                    llSecondRowRightHalf.setVisibility(View.VISIBLE);
                    //  textView2Pin.setVisibility(View.GONE);
                    imgInstruction.setVisibility(View.GONE);


                    System.out.println("---->All" + (Math.abs(verticalOffset) - appBarLayout.getTotalScrollRange()));
                    collapsing_toolbar_test.setExpandedTitleColor(getResources().getColor(R.color.white));
                    params.rightMargin = 90;
                } else {
                    double percentage = (double) Math.abs(verticalOffset) / collapsing_toolbar_test.getHeight() - collapsing_toolbar_test.getHeight();
                    System.out.println("percentage--->" + percentage);
                    if (percentage > 0.225) {
                        System.out.println("changeme--->" + 25);
                        params.rightMargin = 10;
                    } else {
                        params.rightMargin = 15;
                        System.out.println("changeme--->" + 15);
                    }
                    //Expanded
                    llSecondRowRightHalf.setVisibility(View.VISIBLE);
                    // textView2Pin.setVisibility(View.VISIBLE);
                    imgInstruction.setVisibility(View.VISIBLE);
                    collapsing_toolbar_test.setExpandedTitleColor(getResources().getColor(R.color.white));


                }
            }
        });
    }


    private void setFragment() {

        Fragment fragment = new SubscriptionTestFragment(testId, type, chapter_id, subject_id, chapter_name, no_of_question, isFinished, questionType, strInstruction, strLongName, crt_ans_mark, wrng_ans_mark, pending, pendingTestId);
        onSubmitListioner = (OnSubmitListioner) fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameContainer, fragment);
        fragmentTransaction.commit();

    }

    private void listener() {
        imageView2.setOnClickListener(this);
        imgCloseInstruction.setOnClickListener(this);
        imgSubmit.setOnClickListener(this);
        rlInstruction.setOnClickListener(this);
        rlTimer.setOnClickListener(this);
        rlParent.setOnTouchListener(new OnSwipeTouchListener(SubscriptionTestActivity.this) {
            public void onSwipeTop() {
            }

            public void onSwipeRight() {
                EventBus.getDefault().post("close");
            }

            public void onSwipeLeft() {

            }

            public void onSwipeBottom() {
            }

        });
    }

    private void setFontSize() {
        tfBold = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaRegular.otf");
        textView.setText(subject_name);

        textView2Pin.setText(chapter_name);
   /* if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
      textView.setTextAppearance(SubscriptionTestActivity.this,R.style.textViewSmallSubjectName);

      textView2Pin.setTextAppearance(SubscriptionTestActivity.this,R.style.textViewSmallChaptName);
    }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {*/
        textView.setTextAppearance(SubscriptionTestActivity.this, R.style.textViewMediumSubjectName);

        textView2Pin.setTextAppearance(SubscriptionTestActivity.this, R.style.textViewMediumChaptName);
    /*}else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
      textView.setTextAppearance(SubscriptionTestActivity.this,R.style.textViewLargeSubjectName);

      textView2Pin.setTextAppearance(SubscriptionTestActivity.this,R.style.textViewLargeChaptName);
    }*/

        textView.setTypeface(tfMedium);

        textView2Pin.setTypeface(tfMedium);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageView2:
                // onSubmitListioner.onBackPressed();
                EventBus.getDefault().post("close");
                break;

            case R.id.imgCloseInstruction:
                constrainInstruction.setVisibility(View.GONE);
                break;
            case R.id.imgSubmit:
                TimeBuff += MillisecondTime;
                handler.removeCallbacks(runnable);
                onSubmitListioner.onSubmit();
                constrainInstruction.setVisibility(View.GONE);
                break;
            case R.id.rlInstruction:
                showHideInstructio();
                //constrainInstruction.setVisibility(constrainInstruction.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                break;

            case R.id.rlTimer:
                txtStopwatch.setVisibility(View.VISIBLE);
                imgTimer.setVisibility(View.GONE);
                handlerShowTimer.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        txtStopwatch.setVisibility(View.GONE);
                        imgTimer.setVisibility(View.VISIBLE);
                    }
                }, 5000);
                break;
        }
    }

    private void showHideInstructio() {
        if (alertDialog != null && alertDialog.isShowing())
            alertDialog.dismiss();
        else {
            alertDialog = new Dialog(SubscriptionTestActivity.this);
            alertDialog.setContentView(R.layout.custom_instruction_alert);
            alertDialog.setCancelable(true);
            alertDialog.setCanceledOnTouchOutside(true);

            if (alertDialog.getWindow() != null) {
                alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }

            TextView txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
            txtInstructionDialog.setText(strInstruction);
    /*  LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) txtInstructionDialog.getLayoutParams();
      params.height = (int) (height1*(0.26));
      txt_description.setLayoutParams(params);*/
            txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
            RelativeLayout rlClose = alertDialog.findViewById(R.id.rlClose);
            rlClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();

                }
            });
            if (!alertDialog.isShowing())
                alertDialog.show();
        }

    }


 /* private void setupViewPager(final ViewPager viewPager) {
    final ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(),
        this);

    if(testArrayList.size()>0)
    {
      for (int i=0;i<testArrayList.size();i++)
      {
        if(testId.equalsIgnoreCase(testArrayList.get(i)._id)) {
          selectedIndex = i;
          viewPagerAdapter.addFragment(new SubscriptionTestFragment(testArrayList.get(i)._id, testArrayList.get(i).type, chapter_id, subject_id, chapter_name, testArrayList.get(i).no_of_qstn, testArrayList.get(i).is_finished, testArrayList.get(i).qstn_type, testArrayList.get(i).instruction,testArrayList.get(i).long_name), testArrayList.get(i).short_name);
          break;
        }
      }
    }
*//*Test Complete Fragment*//*
     *//*viewPagerAdapter.addFragment(new SubscriptionTestCompleteFragment(), "Test 2 ");*//*

    viewPager.setAdapter(viewPagerAdapter);

    viewPager.setCurrentItem(selectedIndex);
    tabLayout.setupWithViewPager(viewPager);
    for (int i = 0; i < tabLayout.getTabCount(); i++) {
      TabLayout.Tab tab = tabLayout.getTabAt(i);
      if (tab != null) {
        tab.setCustomView(viewPagerAdapter.getTabView(i));
      }
    }
  }*/

    /* private class ViewPagerAdapter extends FragmentPagerAdapter {

       List<Fragment> fragmentList = new ArrayList<>();
       List<String> fragmentTitles = new ArrayList<>();
       Context mContext;
       TextView tabTitles;


       ViewPagerAdapter(FragmentManager fragmentManager,
                        FragmentActivity activity) {
         super(fragmentManager);
         mContext = activity;
       }

       @Override
       public Fragment getItem(int i) {
         *//*switch (i) {
        case 0: {*//*
          return new SubscriptionTestFragment(testArrayList.get(i)._id,testArrayList.get(i).type,chapter_id,subject_id,chapter_name,testArrayList.get(i).no_of_qstn,testArrayList.get(i).is_finished,testArrayList.get(i).qstn_type,testArrayList.get(i).instruction,testArrayList.get(i).long_name);
        *//*}
        case 1: {
          return SubscriptionTestCompleteFragment(testArrayList.get(i)._id,testArrayList.get(i).type,chapter_id,subject_id,chapter_name,testArrayList.get(i).no_of_qstn);
        }
        default:
          return new SubscriptionTestFragment(testArrayList.get(i)._id,testArrayList.get(i).type,chapter_id,subject_id,chapter_name,testArrayList.get(i).no_of_qstn);
      }*//*
    }

    @Override
    public int getCount() {
      return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
      return fragmentTitles.get(position);
    }

    void addFragment(Fragment fragment, String name) {
      fragmentList.add(fragment);
      fragmentTitles.add(name);
    }

    View getTabView(int position) {
      // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
      View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_items, null);
      tabTitles = v.findViewById(R.id.txtTabTitle);
      tabTitles.setText(fragmentTitles.get(position));
      tabTitles.setTextColor(getResources().getColor(R.color.white));
      return v;
    }
  }*/
    public Runnable runnable = new Runnable() {

        public void run() {

            MillisecondTime = SystemClock.uptimeMillis() - StartTime;

            UpdateTime = TimeBuff + MillisecondTime;

            Seconds = (int) (UpdateTime / 1000);

            Minutes = Seconds / 60;

            Seconds = Seconds % 60;

            MilliSeconds = (int) (UpdateTime % 1000);

            txtStopwatch.setText("" + Minutes + ":"
                    + String.format("%02d", Seconds));
            /* + String.format("%03d", MilliSeconds));*/

            handler.postDelayed(this, 0);
        }

    };

    private void startStopwatch() {
        txtStopwatch.setVisibility(View.VISIBLE);
        imgTimer.setVisibility(View.GONE);
        StartTime = SystemClock.uptimeMillis();
        handler = new Handler();
        handler.postDelayed(runnable, 0);

        handlerShowTimer = new Handler();
        handlerShowTimer.postDelayed(new Runnable() {
            @Override
            public void run() {
                txtStopwatch.setVisibility(View.GONE);
                imgTimer.setVisibility(View.VISIBLE);
            }
        }, 5000);

    }

    @Override
    public void onAnswered(String strQAndA, int numAnswered) {
        imgSubmitInActive.setVisibility(numAnswered == 0 ? View.VISIBLE : View.GONE);
        imgSubmit.setVisibility(numAnswered != 0 ? View.VISIBLE : View.GONE);
        txtQACount.setText(strQAndA);
    }

    @Override
    public void onSubmitted(String correctAnswer, String wrongAnswer, String questionAttented, String toHundred, double percentage) {
        TimeBuff += MillisecondTime;
        handler.removeCallbacks(runnable);
   /*textView3.setText(correctAnswer);
      textView5.setText(questionAttented);
      textView7.setText(wrongAnswer);
       textView9.setText(toHundred);
       double mark = percentage;
      textView10.setText("Score "+(percentage>0? Math.round(percentage) : 0)+"%");
      txtEmoji.setText(mark < 0 ? (new String(Character.toChars(0x1F620)))*//*angry*//* : mark>0 && mark< 40 ? (new String(Character.toChars(0x1F622)))*//*crying*//*
              : mark>41 && mark< 60 ? (new String(Character.toChars(0x1F616)))*//*Confounded Face*//*
              : mark>61&& mark< 80 ? (new String(Character.toChars(0x263A)))*//*smiling*//*
              : (new String(Character.toChars(0x1F603))));*//*angry*//*
    llResult.setVisibility(View.VISIBLE);*/

    }

    @Override
    public void onBackPressed() {
        onSubmitListioner.onBackPressed();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String event) {
        if (event.equals("answaerd")) {
            imgSubmit.setVisibility(View.VISIBLE);
            imgSubmitInActive.setVisibility(View.GONE);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_article, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.edit) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
