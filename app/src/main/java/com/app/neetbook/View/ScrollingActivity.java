package com.app.neetbook.View;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.app.neetbook.Adapter.ArticleTabsAdapter;
import com.app.neetbook.Interfaces.ArticleContentSummaryGet;
import com.app.neetbook.Interfaces.OnMobileHomeMenuSelectedListioner;
import com.app.neetbook.Model.HeadData;
import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.PointsContent;
import com.app.neetbook.R;
import com.app.neetbook.Utils.OnSwipeTouchListener;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.SmoothScroll.LinearLayoutManagerWithSmoothScroller;
import com.app.neetbook.Utils.Thread.AppExecutors;
import com.app.neetbook.View.SideMenu.ArticleDetailActivity;
import com.app.neetbook.View.SideMenu.NewPointsActivity;
import com.app.neetbook.View.SideMenu.SubscriptionArticleFragment;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.linroid.filtermenu.library.FilterMenu;
import com.linroid.filtermenu.library.FilterMenuLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import hugo.weaving.DebugLog;

public class ScrollingActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener, ArticleContentSummaryGet {

    private String strSummary = "";
    private RelativeLayout rlParentRight, rlParent;
    String fullupordown = "0";
    protected saHeaderView toolbarHeaderView;
    LinearSnapHelper snapHelper;
    private boolean isTouch = false;
    int change = 0;
    int marginmove = 10;
    AppExecutors appExecutors;
    protected saHeaderView floatHeaderView;
    public static ArrayList<HeadDetails> headDataArrayList;
    private SessionManager sessionManager;
    ArticleTabsAdapter homeGroupNameAdapter;
    protected AppBarLayout appBarLayout;
    private int totalSubsCount;
    TextView content_title_tv, content_title_tv1, chapternames, headnames, superTitle,NoContentAvailable;
    LinearLayout lin;
    private ArrayList<HeadData> headDataList;
    ImageView imageViewArticle, imageViewArticle1;
    Float expandpercentage = 0f;
    private Context context;
    RelativeLayout headinglin;
    LinearLayoutManager layoutManager;
    LinearLayoutManagerWithSmoothScroller layoutManagersmoothscroll;
    RelativeLayout dimscreen;
    private String strArticleId = "";
    String strSubjectId = "", strHeadId = "", strHeadName = "", strLongName = "", strsuperHeading = "", chapter_name = "", chapter_short_name;
    private ArrayList<PointsContent> mPointsList = new ArrayList<>();
    protected Toolbar toolbar;
    LinearLayout recylerview;
    private FilterMenuLayout filter_menu1;
    protected CollapsingToolbarLayout collapsingToolbarLayout;
    RecyclerView recyclerViewGroups;
    private boolean isHideToolbarView = false;
    private String Subject_name = "", Summary = "";
    private String is_coming_page = "";
    private int currentSelectedPosition = 0;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    RelativeLayout disabletouch;
    private ImageView iv_filter;
    ConstraintLayout overallmove;

    Window window;
    saHeaderBehaviour saHeaderBehaviour;
    String pointTitles[] = {"POINTS 1", "POINTS 2", "POINTS 3", "POINTS 4", "POINTS 5"};

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.saactivity_main);


       /* window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.article_page_background));*/
        saHeaderBehaviour = new saHeaderBehaviour(this);
        headinglin = findViewById(R.id.headinglin);
        rlParent = findViewById(R.id.rlParent);
        rlParentRight = findViewById(R.id.rlParentRight);
        disabletouch= findViewById(R.id.disabletouch);
        overallmove= findViewById(R.id.overallmove);
        tabLayout = findViewById(R.id.tabs_subscription_category);
        viewPager = findViewById(R.id.viewpager_subscription_category);
        recyclerViewGroups = findViewById(R.id.recyclerViewGroups);
        toolbarHeaderView = (saHeaderView) findViewById(R.id.toolbar_header_view);
        appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imageViewArticle = (ImageView) findViewById(R.id.imageViewArticles);
        imageViewArticle1 = (ImageView) findViewById(R.id.imageViewArticles1);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        floatHeaderView = (saHeaderView) findViewById(R.id.float_header_view);
        dimscreen = (RelativeLayout) findViewById(R.id.dimscreen);
        recylerview = (LinearLayout) findViewById(R.id.recylerview);
        content_title_tv = (TextView) findViewById(R.id.content_title_tv);
        content_title_tv1 = (TextView) findViewById(R.id.content_title_tv1);
        chapternames = (TextView) findViewById(R.id.chapternames);
        NoContentAvailable= (TextView) findViewById(R.id.no_content_available);
     //   webViewLayout=(LinearLayout) findViewById(R.id.webViewLayout);

        headnames = (TextView) findViewById(R.id.headnames);
        iv_filter = (ImageView) findViewById(R.id.iv_filter);

        superTitle = (TextView) findViewById(R.id.superTitleTxt);
        lin = (LinearLayout) findViewById(R.id.lin);
        filter_menu1 = (FilterMenuLayout) findViewById(R.id.filter_menu1);
        attachMenu1(filter_menu1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationIcon(R.drawable.menu_32);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (is_coming_page.equals("homepage")) {

                    Intent s = new Intent(getApplicationContext(), TabMainActivity.class);
                    s.putExtra("subjectId", strSubjectId);
                    s.putExtra("subjectName", sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_NAME));
                    s.putExtra("isTrial", sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_TRIAL_STATUS));
                    s.putExtra("subjectShortName", sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_SHORT_NAME));
                    s.putExtra("showing_page_position", "0");
                    startActivity(s);
                    finish();
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                } else {

                    finish();
                }

            }
        });

        initUi();

        /*Replace with own custom image */
        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.photo);
        int color = getDominantColor(icon);
        collapsingToolbarLayout.setContentScrimColor(color);
        collapsingToolbarLayout.setStatusBarScrimColor(color);
        if (Build.VERSION.SDK_INT >= 21) {
            //   getWindow().setStatusBarColor(darker(color, 0.8f));
        }
        /*Replace with own custom image */

        context = getApplicationContext();
        appExecutors = new AppExecutors();

        sessionManager = new SessionManager(ScrollingActivity.this);
        headDataList = new ArrayList<>();//superHeading
        if (getIntent().getExtras() != null) {
            strArticleId = getIntent().getStringExtra("chapter_id");
            strHeadId = getIntent().getStringExtra("head_id");
            strSubjectId = getIntent().getStringExtra("subject_id");

            strLongName = getIntent().getStringExtra("subject_name");
            chapter_name = getIntent().getStringExtra("chapter_name");
            chapter_short_name = getIntent().getStringExtra("chapter_short_name");
            Subject_name = getIntent().getStringExtra("subject_name");

            if (getIntent().getExtras().containsKey("coming_page")) {

                is_coming_page = getIntent().getStringExtra("coming_page");
            }

            chapternames.setText(chapter_short_name);
            headnames.setText(getIntent().getStringExtra("subject_name")); //main title

            currentSelectedPosition = 0;
            if (sessionManager.getViewToBookMark() != null && !sessionManager.getViewToBookMark().isEmpty() && sessionManager.getViewToBookMark().equalsIgnoreCase("Yes")) {
                headDataArrayList =
                        new Gson().fromJson(sessionManager.getBookmarkDetails().get(SessionManager.HEAD_LIST), new TypeToken<List<HeadDetails>>() {
                        }.getType());
            }
        }

        getPoints();
        createSnap();
        setupViewPager(viewPager);

        rlParent.setOnTouchListener(new OnSwipeTouchListener(ScrollingActivity.this) {
            public void onSwipeTop() {
                //Toast.makeText(SubscriptionArticleActivity.this, "top", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeRight() {
                //Toast.makeText(SubscriptionArticleActivity.this, "right", Toast.LENGTH_SHORT).show();
                finish();
            }

            public void onSwipeLeft() {
                startActivity(new Intent(ScrollingActivity.this, DynamicFragmentActivity.class).putExtra("subject_id", strSubjectId).putExtra("head_id", strHeadId).putExtra("title", strHeadName).putExtra("long_name", strLongName).putExtra("isPoints", "No").putExtra("chapterName", chapter_short_name  ));
            }

            public void onSwipeBottom() {
                //Toast.makeText(SubscriptionArticleActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });

        disabletouch.setOnTouchListener(new OnSwipeTouchListener(ScrollingActivity.this) {
            public void onSwipeTop() {
                //Toast.makeText(SubscriptionArticleActivity.this, "top", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeRight() {
                //Toast.makeText(SubscriptionArticleActivity.this, "right", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeLeft() {
            }

            public void onSwipeBottom() {
                //Toast.makeText(SubscriptionArticleActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });

      /*  NoContentAvailable.setOnTouchListener(new OnSwipeTouchListener(ScrollingActivity.this) {
            public void onSwipeTop() {
                //Toast.makeText(SubscriptionArticleActivity.this, "top", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeRight() {
                viewPager.setVisibility(View.VISIBLE);
                NoContentAvailable.setVisibility(View.VISIBLE);
            }

            public void onSwipeLeft() {
                viewPager.setVisibility(View.VISIBLE);
                NoContentAvailable.setVisibility(View.VISIBLE);
            }

            public void onSwipeBottom() {
                //Toast.makeText(SubscriptionArticleActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });*/


        rlParentRight.setOnTouchListener(new OnSwipeTouchListener(ScrollingActivity.this) {
            public void onSwipeTop() {
                //Toast.makeText(SubscriptionArticleActivity.this, "top", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeRight() {

            }

            public void onSwipeLeft() {
                startActivity(new Intent(ScrollingActivity.this, DynamicFragmentActivity.class).putExtra("subject_id", strSubjectId).putExtra("head_id", strHeadId).putExtra("title", strHeadName).putExtra("long_name", strLongName).putExtra("isPoints", "No").putExtra("chapterName", chapter_short_name));
            }

            public void onSwipeBottom() {
                //Toast.makeText(SubscriptionArticleActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });

        iv_filter.setBackgroundResource(R.drawable.filter);

        iv_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (change == 0) {
                    iv_filter.setBackgroundResource(R.drawable.ic_close);
                    change = 1;
                    filter_menu1.setVisibility(View.VISIBLE);
                    filter_menu1.toggle(true);


                } else {
                    change = 0;
                    iv_filter.setBackgroundResource(R.drawable.filter);
                    filter_menu1.setVisibility(View.VISIBLE);
                    filter_menu1.toggle(false);
                }
            }
        });


        imageViewArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ScrollingActivity.this, ArticleDetailActivity.class).putExtra("title", strHeadName).putExtra("summary", Summary).putExtra("chapter_short_name", chapter_short_name));
                overridePendingTransition(R.anim.right_to_left_anim, R.anim.left_to_right_anim);

            }
        });

        imageViewArticle1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ScrollingActivity.this, ArticleDetailActivity.class).putExtra("title", strHeadName).putExtra("summary", Summary).putExtra("chapter_short_name", chapter_short_name));
                overridePendingTransition(R.anim.right_to_left_anim, R.anim.left_to_right_anim);
            }
        });
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int X = (int) event.getX();
        int Y = (int) event.getY();
        int eventaction = event.getAction();

        switch (eventaction) {
            case MotionEvent.ACTION_DOWN:

                isTouch = true;
                break;

            case MotionEvent.ACTION_MOVE:

                break;

            case MotionEvent.ACTION_UP:

                break;
        }
        return true;
    }


    @Override
    public void onArticleContentChanged(String summary, int CurrentPosition) {
        if (currentSelectedPosition == CurrentPosition)
            strSummary = summary;
    }

    private void setGroupAdapter(final int position) {

        if (homeGroupNameAdapter == null) {

            layoutManagersmoothscroll = new LinearLayoutManagerWithSmoothScroller(ScrollingActivity.this, RecyclerView.HORIZONTAL, false);

            layoutManager = new LinearLayoutManager(ScrollingActivity.this, RecyclerView.HORIZONTAL, false);
            layoutManager.setStackFromEnd(true);
            homeGroupNameAdapter = new ArticleTabsAdapter(headDataList, ScrollingActivity.this, position);
            recyclerViewGroups.setLayoutManager(layoutManager);
            //snapHelper.attachToRecyclerView(recyclerViewGroups);
            recyclerViewGroups.setAdapter(homeGroupNameAdapter);

            // scrollToCenter(layoutManager,recyclerViewGroups,position);
            recyclerViewGroups.scrollToPosition(position);

        } else {

            homeGroupNameAdapter.notifyPosition(position);
            scrollToCenter(layoutManager, recyclerViewGroups, position);
        }

        // recyclerViewGroups.smoothScrollToPosition(position);
        // recyclerViewGroups.scrollToPosition(position);


        homeGroupNameAdapter.setOnGroupMenuSelectedListener(new OnMobileHomeMenuSelectedListioner() {
            @Override
            public void onGroupSelected(String groupId, int position) {
                viewPager.setVisibility(View.GONE);
                try {

//                    viewPager.setCurrentItem(position);
//                    layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
//                    int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
//                    int centeredItemPosition = totalVisibleItems / 2;
//                    recyclerViewGroups.smoothScrollToPosition(position != 0 ? position - 1 : position);
//                    recyclerViewGroups.setScrollY(centeredItemPosition);
                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            viewPager.setVisibility(View.VISIBLE);
                        }
                    }, 500);

                    viewPager.setCurrentItem(position);
                    scrollToCenter(layoutManager, recyclerViewGroups, position);
                    //  scrollToCenter(layoutManager,recyclerViewGroups,position);

                           /* layoutManagersmoothscroll = ((LinearLayoutManagerWithSmoothScroller) recyclerViewGroups.getLayoutManager());
                            int totalVisibleItems = layoutManagersmoothscroll.findLastVisibleItemPosition() - layoutManagersmoothscroll.findFirstVisibleItemPosition();
                            int centeredItemPosition = totalVisibleItems / 2;
                            recyclerViewGroups.smoothScrollToPosition(position != 0 ? position - 1 : position);
                            //  recyclerViewGroups.setScrollY(centeredItemPosition);*/


                } catch (Exception e) {

                    Log.e("Exception", e.toString());

                }

            }
        });


    }


    public void scrollToCenter(LinearLayoutManager layoutManager, RecyclerView recyclerList, int clickPosition) {
        RecyclerView.SmoothScroller smoothScroller = createSnapScroller(recyclerList, layoutManager);

        if (smoothScroller != null) {
            smoothScroller.setTargetPosition(clickPosition);
            layoutManager.startSmoothScroll(smoothScroller);
        }
    }

    // This number controls the speed of smooth scroll
    private static final float MILLISECONDS_PER_INCH = 200f;

    private final static int DIMENSION = 2;
    private final static int HORIZONTAL = 0;
    private final static int VERTICAL = 1;

    @Nullable
    private LinearSmoothScroller createSnapScroller(RecyclerView mRecyclerView, final RecyclerView.LayoutManager layoutManager) {
        if (!(layoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider)) {
            return null;
        }
        return new LinearSmoothScroller(mRecyclerView.getContext()) {
            @Override
            protected void onTargetFound(View targetView, RecyclerView.State state, Action action) {
                int[] snapDistances = calculateDistanceToFinalSnap(layoutManager, targetView);
                final int dx = snapDistances[HORIZONTAL];
                final int dy = snapDistances[VERTICAL];
                final int time = calculateTimeForDeceleration(Math.max(Math.abs(dx), Math.abs(dy)));
                if (time > 0) {
                    action.update(dx, dy, time, mDecelerateInterpolator);
                }
            }


            @Override
            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
            }
        };
    }


    private int[] calculateDistanceToFinalSnap(@NonNull RecyclerView.LayoutManager layoutManager, @NonNull View targetView) {
        int[] out = new int[DIMENSION];
        if (layoutManager.canScrollHorizontally()) {
            out[HORIZONTAL] = distanceToCenter(layoutManager, targetView,
                    OrientationHelper.createHorizontalHelper(layoutManager));
        }

        if (layoutManager.canScrollVertically()) {
            out[VERTICAL] = distanceToCenter(layoutManager, targetView,
                    OrientationHelper.createHorizontalHelper(layoutManager));
        }
        return out;
    }


    private int distanceToCenter(@NonNull RecyclerView.LayoutManager layoutManager,
                                 @NonNull View targetView, OrientationHelper helper) {
        final int childCenter = helper.getDecoratedStart(targetView)
                + (helper.getDecoratedMeasurement(targetView) / 2);
        final int containerCenter;
        if (layoutManager.getClipToPadding()) {
            containerCenter = helper.getStartAfterPadding() + helper.getTotalSpace() / 2;
        } else {
            containerCenter = helper.getEnd() / 2;
        }
        return childCenter - containerCenter;
    }

    private void setupViewPager(final ViewPager viewPager) {
        final ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this);
        try {
            if (headDataArrayList.size() > 0) {
                Summary = headDataArrayList.get(0).headData.get(0).summary;
                Log.e("headDataArrayList", new Gson().toJson(headDataArrayList));
                for (int i = 0; i < headDataArrayList.size(); i++) {
                    if (headDataArrayList.get(i).isSH.equals("Yes")) {
                        HeadData data = new HeadData();
                        data._id = headDataArrayList.get(i).headData.get(0)._id;
                        data.short_name = headDataArrayList.get(i).headData.get(0).short_name;
                        data.long_name = headDataArrayList.get(i).headData.get(0).long_name;
                        data.superHeadingName = headDataArrayList.get(i).isSH.equals("1") ? headDataArrayList.get(i).superDetails.short_name : "";
                        data.url = headDataArrayList.get(i).headData.get(0).url;
                        data.count = headDataArrayList.get(i).headData.get(0).count;
                        data.firsthead = headDataArrayList.get(i).headData.get(0).firsthead;
                        headDataList.add(data);
                        viewPagerAdapter.addFragment(new SubscriptionArticleFragment(headDataArrayList.get(i).headData.get(0)._id, strSubjectId, headDataArrayList.get(i).headData.get(0).long_name, i, headDataArrayList.get(i).headData.get(0).url,Integer.parseInt(headDataArrayList.get(i).headData.get(0).count)), headDataArrayList.get(i).headData.get(0).short_name);
                    } else {
                        if (headDataArrayList.get(i).headData.size() > 0) {
                            for (int j = 0; j < headDataArrayList.get(i).headData.size(); j++) {
                                HeadData data = new HeadData();
                                data._id = headDataArrayList.get(i).headData.get(j)._id;
                                data.short_name = headDataArrayList.get(i).headData.get(j).short_name;
                                data.long_name = headDataArrayList.get(i).headData.get(j).long_name;
                                data.url = headDataArrayList.get(i).headData.get(j).url;
                                data.count = headDataArrayList.get(i).headData.get(j).count;
                                data.firsthead = headDataArrayList.get(i).headData.get(j).firsthead;
                                data.superHeadingName = headDataArrayList.get(i).isSH.equals("1") ? headDataArrayList.get(i).superDetails.short_name : "";
                                headDataList.add(data);
                                viewPagerAdapter.addFragment(new SubscriptionArticleFragment(headDataArrayList.get(i).headData.get(j)._id, strSubjectId, headDataArrayList.get(i).headData.get(0).long_name, i, headDataArrayList.get(i).headData.get(j).url,Integer.parseInt(headDataArrayList.get(i).headData.get(j).count)), headDataArrayList.get(i).headData.get(j).short_name);
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {

        }


        if (headDataList.size() > 0) {
            for (int i = 0; i < headDataList.size(); i++) {
                if (strHeadId.equals(headDataList.get(i)._id)) {
                    currentSelectedPosition = i;
                    strsuperHeading = headDataList.get(i).superHeadingName.equals("") ? "" : headDataList.get(i).superHeadingName;
                    strHeadName = headDataList.get(i).long_name;
                    sessionManager.updateBookMark("Article", headDataList.get(i)._id, strSubjectId, headDataList.get(i).long_name, new Gson().toJson(headDataArrayList), strArticleId, chapter_name, strsuperHeading, "", "");
                    sessionManager.LastReadSubjectBookmark(sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                            sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                            sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME));
                }
            }

        }
        content_title_tv.setText(strHeadName);
        content_title_tv1.setText(strHeadName);

        viewPager.setAdapter(viewPagerAdapter);

        tabLayout.setupWithViewPager(viewPager);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(viewPagerAdapter.getTabView(i));
            }
        }
        setGroupAdapter(currentSelectedPosition);
        viewPager.setCurrentItem(currentSelectedPosition);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {


                try {
                    setGroupAdapter(position);

                    /*if (headDataList.get(position).count.equals("0")){
                        NoContentAvailable.setVisibility(View.VISIBLE);
                        viewPager.setVisibility(View.GONE);
                    }else {
                        NoContentAvailable.setVisibility(View.GONE);
                        viewPager.setVisibility(View.VISIBLE);
                    }*/



/*
                    if(position == 3)
                    {
                        EventBus.getDefault().post("hideme");
                    }
                    else if(headDataList.get(position).count.equals("0"))
                    {
                        EventBus.getDefault().post("hideme");
                    }
                    else
                    {
                        EventBus.getDefault().post("showme");
                    }*/

                    strHeadId = headDataList.get(position)._id;
                    strHeadName = headDataList.get(position).long_name;
                    content_title_tv.setText(strHeadName);
                    content_title_tv1.setText(strHeadName);
                    superTitle.setVisibility(View.VISIBLE);
                    sessionManager.setLastReadDetails(headDataList.get(position).firsthead);
                    if (headDataList.get(position).superHeadingName != null && !headDataList.get(position).superHeadingName.equals(""))
                        superTitle.setText(headDataList.get(position).superHeadingName);

                    else
                        superTitle.setText("");
                    //  chapternames.setText(headDataList.get(position).long_name);
                    sessionManager.updateBookMark("Article", strHeadId, strSubjectId, headDataList.get(position).long_name, new Gson().toJson(headDataArrayList), strArticleId, chapter_name, headDataList.get(position).superHeadingName.equals("") ? "" : headDataList.get(position).superHeadingName, "", "");
                    sessionManager.LastReadSubjectBookmark(sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                            sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                            sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME));
                    // appBarLayout.setExpanded(true);
                    currentSelectedPosition = position;
                    appBarLayout.setExpanded(true);

                } catch (Exception e) {

                    Log.e("Exception", e.toString());
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
    }

    private void getPoints() {

        mPointsList.clear();
        ServiceRequest mRequest = new ServiceRequest(ScrollingActivity.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("subject_id", strSubjectId);
        params.put("chapter_id", strArticleId);

        mRequest.makeServiceRequest(IConstant.pointsContent, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------pointsContent Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";

                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalSubsCount = object.getInt("atotal");
                        JSONArray jsonArray = object.getJSONArray("points");

                        if (jsonArray.length() > 0) {
                            for (int j = jsonArray.length() - 1; j >= 0; j--) {
                                JSONObject point_object = jsonArray.getJSONObject(j);
                                PointsContent pointsContent = new PointsContent();
                                pointsContent.set_id(jsonArray.getJSONObject(j).getString("_id"));
                                pointsContent.setShort_name(jsonArray.getJSONObject(j).getString("short_name"));
                                pointsContent.setLong_name(jsonArray.getJSONObject(j).getString("long_name"));
                                pointsContent.setContent(jsonArray.getJSONObject(j).getString("content"));


                                pointsContent.setPoint_name(jsonArray.getJSONObject(j).getString("point_name"));
                                mPointsList.add(pointsContent);
                            }

                        }

                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });

    }

    public static int darker(int color, float factor) {
        int a = Color.alpha(color);
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);

        return Color.argb(a,
                Math.max((int) (r * factor), 0),
                Math.max((int) (g * factor), 0),
                Math.max((int) (b * factor), 0));
    }

    public static int getDominantColor(Bitmap bitmap) {
        Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, 1, 1, true);
        final int color = newBitmap.getPixel(0, 0);
        newBitmap.recycle();
        return color;
    }

    private void initUi() {
        appBarLayout.addOnOffsetChangedListener(this);

    }

    private FilterMenu attachMenu1(FilterMenuLayout layout) {
        return new FilterMenu.Builder(this)

                .addItem(R.drawable.point_icon_5)
                .addItem(R.drawable.point_icon_4)
                .addItem(R.drawable.point_icon_3)
                .addItem(R.drawable.point_icon_2)
                .addItem(R.drawable.point_icon_1)
                .attach(layout)
                .withListener(listener)
                .build();
    }


    FilterMenu.OnMenuChangeListener listener = new FilterMenu.OnMenuChangeListener() {
        @DebugLog
        @Override
        public void onMenuItemClick(View view, int position) {
            startActivity(new Intent(ScrollingActivity.this, NewPointsActivity.class).putExtra("SUBJECT_ID", strSubjectId).putExtra("CHAPTER_NAME", strArticleId).putExtra("POINTS_NAME", pointTitles[4 - position]).putExtra("CHAPTER_SHORT_NAME", chapter_short_name).putExtra("POSITION", String.valueOf(4 - position)));
            overridePendingTransition(R.anim.right_to_left_anim, R.anim.left_to_right_anim);
        }

        @DebugLog
        @Override
        public void onMenuCollapse() {
            dimscreen.setVisibility(View.GONE);
            iv_filter.setBackgroundResource(R.drawable.filter);
            chapternames.setTextColor(Color.parseColor("#ffffff"));
            headnames.setTextColor(Color.parseColor("#ffffff"));
            change = 0;
            Animation rotation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_refresh);
            //rotation.setRepeatCount(Animation.INFINITE);
            iv_filter.startAnimation(rotation);

        }

        @DebugLog
        @Override
        public void onMenuExpand() {
            dimscreen.setVisibility(View.VISIBLE);
            iv_filter.setBackgroundResource(R.drawable.ic_close);
            Animation rotation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_refresh);
            //rotation.setRepeatCount(Animation.INFINITE);
            iv_filter.startAnimation(rotation);
            chapternames.setTextColor(Color.parseColor("#80ffffff"));
            headnames.setTextColor(Color.parseColor("#80ffffff"));

        }
    };

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(verticalOffset) / (float) maxScroll;
        System.out.println("percentage--->" + percentage);
        expandpercentage = percentage;
        if(percentage == 1)
        {
            fullupordown = "1";
        }
        else  if(percentage == 0)
        {
            fullupordown = "0";
        }
        if (percentage > 0.7) {
            ViewGroup.MarginLayoutParams params1 = (ViewGroup.MarginLayoutParams) lin.getLayoutParams();
            params1.rightMargin = this.getResources().getDimensionPixelOffset(R.dimen._30sdp);
            ViewGroup.MarginLayoutParams params2 = (ViewGroup.MarginLayoutParams) content_title_tv.getLayoutParams();
            //   params2.rightMargin = 100;
            headinglin.setVisibility(View.INVISIBLE);
            chapternames.setVisibility(View.GONE);
            if (content_title_tv.length() > 40) {
                content_title_tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header_view_end_size_minimum));
                content_title_tv1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header_view_end_size_small));
            } else {
                content_title_tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header_view_end_size_minimum));
                content_title_tv1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header_view_end_size_minimum));
            }
            ViewGroup.MarginLayoutParams paramsd = (ViewGroup.MarginLayoutParams) content_title_tv.getLayoutParams();
            //paramsd.rightMargin = 320;
        } else {
            if (content_title_tv.length() > 40) {
                content_title_tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header_view_end_size_minimum));
                content_title_tv1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header_view_end_size_small));
            } else {
                content_title_tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header_view_end_size_minimum));
                content_title_tv1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header_view_end_size_minimum));
            }
            ViewGroup.MarginLayoutParams paramsd = (ViewGroup.MarginLayoutParams) content_title_tv.getLayoutParams();
            //paramsd.rightMargin = 100;
//            headinglin.setVisibility(View.VISIBLE);
//            chapternames.setVisibility(View.VISIBLE);
            ViewGroup.MarginLayoutParams params1 = (ViewGroup.MarginLayoutParams) lin.getLayoutParams();
            params1.rightMargin = 0;
            params1.leftMargin = 70;
            ViewGroup.MarginLayoutParams params2 = (ViewGroup.MarginLayoutParams) content_title_tv.getLayoutParams();
            //  params2.rightMargin = 0;
        }
        System.out.println("marginmove--->" + marginmove);
        if (percentage > 0.35f) {
            chapternames.setVisibility(View.GONE);
            headinglin.setVisibility(View.INVISIBLE);

        }else {
            headinglin.setVisibility(View.VISIBLE);
            chapternames.setVisibility(View.VISIBLE);
        }

        if (percentage <= 0.35f) {
            recylerview.setVisibility(View.VISIBLE);
        } else {
            recylerview.setVisibility(View.GONE);
        }
        content_title_tv.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.article_title_color));
        imageViewArticle.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.article_title_color));
        if (percentage > 0.35f) {

            content_title_tv.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            imageViewArticle.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.white));
        } else {
            content_title_tv.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.article_title_color));
            imageViewArticle.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.article_title_color));
        }
        if (percentage == 1f && isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.VISIBLE);
            isHideToolbarView = !isHideToolbarView;
        } else if (percentage < 1f && !isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.GONE);
            isHideToolbarView = !isHideToolbarView;
        }
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        List<Fragment> fragmentList = new ArrayList<>();
        List<String> fragmentTitles = new ArrayList<>();
        Context mContext;
        TextView tabTitles;


        ViewPagerAdapter(FragmentManager fragmentManager,
                         FragmentActivity activity) {
            super(fragmentManager);
            mContext = activity;
        }

        @Override
        public Fragment getItem(int position) {
            return new SubscriptionArticleFragment(headDataList.get(position)._id, strSubjectId, headDataList.get(position).long_name, position, headDataList.get(position).url,Integer.parseInt(headDataList.get(position).count));
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitles.get(position);
        }

        void addFragment(Fragment fragment, String name) {
            fragmentList.add(fragment);
            fragmentTitles.add(name);
        }

        View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_items, null);
            tabTitles = v.findViewById(R.id.txtTabTitle);
            tabTitles.setText(fragmentTitles.get(position));
            tabTitles.setTextColor(getResources().getColor(R.color.white));
            return v;
        }
    }

    @Override
    public void onBackPressed() {
    }

    public static void adjustFontScale(Context context, Configuration configuration) {
        if (configuration.fontScale != 1) {
            configuration.fontScale = 1;
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            wm.getDefaultDisplay().getMetrics(metrics);
            metrics.scaledDensity = configuration.fontScale * metrics.density;
            context.getResources().updateConfiguration(configuration, metrics);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String event) {
        if(event.equals("UPTOUCH") && fullupordown.equals("0"))
        {
            if (expandpercentage > 0.1) {
                appBarLayout.setExpanded(false);
            }


        }
        else  if(event.equals("UPTOUCH") && fullupordown.equals("1"))
        {
            if (expandpercentage != 1f) {
                appBarLayout.setExpanded(true);
            }
        }
    };


    @Override

    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }
}
