package com.app.neetbook.View;

import android.animation.Animator;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.app.neetbook.Model.TabSideMenu;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ApplicationLifecycleHandler;
import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.Utils.Data.EnterPinData;
import com.app.neetbook.Utils.Data.LoginData;
import com.app.neetbook.Utils.PrefManager;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.Thread.AppExecutors;
import com.app.neetbook.Utils.alerter.AlertBox;

import com.app.neetbook.Utils.widgets.DisplayOrientation;
import com.app.neetbook.View.Fragments.NoInternetAvailFragment;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.app.neetbook.socket.SocketCheckService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SplashActivity extends AppCompatActivity {
    ConnectionDetector cd;
    SessionManager sessionManager;
    PrefManager prefManager;
    String TAG = "Splash";
    AppExecutors appexecutor;
    int modeCheck=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cd = new ConnectionDetector(SplashActivity.this);
        if (!getResources().getBoolean(R.bool.isTablet))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
         setContentView(R.layout.activity_splash);

        loadAnimatedLogo();
        sessionManager = new SessionManager(SplashActivity.this);
    }

    private void init() {

        prefManager = new PrefManager(this);

        //Check Socket is connected

        if (sessionManager.isLoggedIn()) {

            // Intent service = new Intent(SplashActivity.this, SocketCheckService.class);
            //  startService(service);
        }




        if (prefManager.isFirstTimeLaunch())
            new AsyntaskRunner().execute();
        LaunchHome();
    }


    private void loadAnimatedLogo() {
        LottieAnimationView lottieAnimationView = (LottieAnimationView) findViewById(R.id.lottieAnimationView);
        lottieAnimationView.setImageAssetsFolder("images/");
        lottieAnimationView.setAnimation("data.json");
        lottieAnimationView.loop(false);
        lottieAnimationView.playAnimation();
        lottieAnimationView.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (cd.isConnectingToInternet())
                    init();
                else {
                    startActivity(new Intent(SplashActivity.this,NoInterNetActivity.class));
                    overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                    finish();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });


    }

    private void LaunchHome() {


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (prefManager.isFirstTimeLaunch()){

                    startActivity(new Intent(SplashActivity.this, WalkthroughActivity.class));
                    overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                }

                else {
                    LoginData.clearData();
                    EnterPinData.clearData();
                    if (sessionManager.isLoggedIn()) {
                        if (sessionManager.getPin() != null && !sessionManager.getPin().equals("") && sessionManager.getPin().length() == 4)
                        {startActivity(new Intent(SplashActivity.this, EnterPin.class));
                            overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);}
                        else{
                            startActivity(new Intent(SplashActivity.this, SignInSighUp.class));
                            overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                        }
                    } else {
                        startActivity(new Intent(SplashActivity.this, SignInSighUp.class));
                        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                    }
                }
                finish();
            }
        }, 3000);

    }

    private class AsyntaskRunner extends AsyncTask<String, String, String> {

        String result = "";

        @Override
        protected String doInBackground(String... strings) {
            ServiceRequest mRequest = new ServiceRequest(SplashActivity.this);
            mRequest.makeServiceRequest(IConstant.walkThrough, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("------------walkThrough Response----------------" + response);
                    String sStatus = "";
                    try {
                        JSONObject object = new JSONObject(response);
                        sStatus = object.getString("status");
                        if (sStatus.equalsIgnoreCase("1")) {

                            WalkthroughActivity.jsonWalkThrough = object.getJSONObject("response");


                        } else {
                        }
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                }

                @Override
                public void onErrorListener(String errorMessage) {
                    AlertBox.showSnackBoxPrimaryBa(SplashActivity.this, getString(R.string.alert_oops), errorMessage);
                }
            });
            return result;
        }
    }

    private class AsyntaskGroupRunner extends AsyncTask<String, String, String> {
        String result = "";

        @Override
        protected String doInBackground(String... strings) {
            ServiceRequest mRequest = new ServiceRequest(SplashActivity.this);
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
            params.put("page_type","1");
            params.put("page","1");
            mRequest.makeServiceRequest(IConstant.groupList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("------------groupList Response----------------" + response);
                    String sStatus = "";
                    try {

                        JSONObject object = new JSONObject(response);
                        sStatus = object.getString("status");
                        if (sStatus.equalsIgnoreCase("1")) {

                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray result = jsonObject.getJSONObject("response").getJSONArray("groupslist");
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    TabSideMenu tabSideMenu = new TabSideMenu();
                                    tabSideMenu.setName(result.getJSONObject(i).getString("g_name"));
                                    tabSideMenu.setShortName(result.getJSONObject(i).getString("short_name"));
                                    tabSideMenu.setLongName(result.getJSONObject(i).getString("long_name"));
                                    tabSideMenu.setId(result.getJSONObject(i).getString("_id"));
                                }
                            }

//                            for(int i=0;i<result.length();i++ )
//                            {
//                                String id=result.getJSONObject(i).getString("_id");
//                                String g_name=result.getJSONObject(i).getString("g_name");
//                                com.app.neetbook.Utils.alerter.DemoUtils.getTime(getApplicationContext(),id,g_name,(i+1));
//                            }
//
//                            com.app.neetbook.Utils.alerter.DemoUtils.getretrieve(getApplicationContext());
                        } else {
                        }
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                }
                @Override
                public void onErrorListener(String errorMessage) {
                    // AlertBox.showSnackBoxPrimaryBa(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
                }
            });
            return result;


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            // new AsyncSubjectsRunner(s).execute();
        }
    }

    private class AsyncSubjectsRunner extends AsyncTask<String, String, String> {
        String result = "";
        String id = "";

        public AsyncSubjectsRunner(String id) {
            this.id = id;
        }

        @Override
        protected String doInBackground(String... strings) {
            ServiceRequest mRequest = new ServiceRequest(SplashActivity.this);
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
            params.put("page_type","1");
            params.put("page","1");
            mRequest.makeServiceRequest(IConstant.groupList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("------------groupList Response----------------" + response);
                    String sStatus = "";
                    try {
                        result = response;
                        JSONObject object = new JSONObject(response);
                        sStatus = object.getString("status");
                        if (sStatus.equalsIgnoreCase("1")) {


                        } else {
                        }
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                }

                @Override
                public void onErrorListener(String errorMessage) {
                    AlertBox.showSnackBoxPrimaryBa(SplashActivity.this, getString(R.string.alert_oops), errorMessage);
                }
            });
            return result;


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
    }

    public void onResume()
    {
        super.onResume();


    }
}
