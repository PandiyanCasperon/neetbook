package com.app.neetbook.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.app.neetbook.Adapter.HomeGroupReferNowAdapter;
import com.app.neetbook.Interfaces.OnFragmentInteractionListener;
import com.app.neetbook.Interfaces.OnGroupHomeMenuClickListener;
import com.app.neetbook.Interfaces.OnGroupMenuSelectedListener;
import com.app.neetbook.Model.GroupChildBean;
import com.app.neetbook.Model.Groups;
import com.app.neetbook.Model.Groupslist;
import com.app.neetbook.Model.ReferNowBean;
import com.app.neetbook.Model.TabSideMenu;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ActionBarActivityNeetBook;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Data.DrawerActivityData;
import com.app.neetbook.Utils.Data.GroupMenuData;
import com.app.neetbook.Utils.Data.SubscriptionFragData;
import com.app.neetbook.Utils.EventBusPojo.ActionEvent;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.Utils.widgets.DisplayOrientation;
import com.app.neetbook.View.Fragments.MainSubscribeActivity;
import com.app.neetbook.View.Fragments.MobileHomeChildFragment;
import com.app.neetbook.View.Fragments.TabletHomeSideGroupMenu;
import com.app.neetbook.View.Fragments.TabletSideMenuFrament;
import com.app.neetbook.homepageRoom.GroupslistDatabase;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TabletHome extends ActionBarActivityNeetBook implements OnFragmentInteractionListener, OnGroupHomeMenuClickListener, OnGroupMenuSelectedListener {
    Fragment fragment = null;

    RecyclerView mobileHomeRecyclerViewHorizontal;
    ArrayList<ReferNowBean> referNowList;
    HomeGroupReferNowAdapter homeGroupReferNowAdapter;
    private int totalCount = 0;
    private int skip = 0;
    private int page = 1;
    private int limit = 10;
    private int loadMoreEnable = 0;
    SessionManager sessionManager;
    RelativeLayout RlSubscribe;
    private CustomAlert customAlert;
    public static TabletHome PagesActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablet_home);
        sessionManager = new SessionManager(this);
        customAlert = new CustomAlert(this);
        PagesActivity = this;
        if(!getResources().getBoolean(R.bool.isTablet))
        {
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        RlSubscribe = findViewById(R.id.RlSubscribe);

        RlSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubscriptionFragData.clearData();
                if(sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) != null && sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals(""))
                {
                    showAlertOOps(getString(R.string.alert_oops),"Complete your profile");
                }else
                    startActivity(new Intent(getApplicationContext(), MainSubscribeActivity.class));
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
            }
        });

        prepareReferNowList();
        setLeftSideMenu();

        setRightFragment();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }

    private void promoListListener() {
        mobileHomeRecyclerViewHorizontal.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager=LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();

                boolean endHasBeenReached = lastVisible + 5 >= totalItemCount;
                Log.e("totalItemCount",""+totalItemCount);
                Log.e("totalCount",""+totalCount);
                Log.e("lastVisible",""+lastVisible);
                Log.e("endHasBeenReached",""+endHasBeenReached);
                if(lastVisible == totalItemCount-1 && totalCount>referNowList.size() && loadMoreEnable == 0)
                {
                    page = page+1;
                    Log.e("loadMoreSubjects()","Called");
                    loadMoreEnable = 1;
                    loadMorePromo(totalItemCount);
                }
            }
        });
    }
    private void setLeftSideMenu() {
        TabletHomeSideGroupMenu sideMenuFrament = new TabletHomeSideGroupMenu();

        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.add(R.id.frameLayoutLeft,sideMenuFrament);
        fragmentTransaction.commit();
    }

    private void setRightFragment() {
        MobileHomeChildFragment mobileHomeChildFragment = new MobileHomeChildFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.frameLayoutRight,mobileHomeChildFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onSideMenuSelected(TabSideMenu tabSideMenu,int position) {


    }

    @Override
    public void onGroupHomeMenuClicked() {

        finish();

    }
    private void loadMorePromo(final int lastLoadedPosition) {
        ServiceRequest mRequest = new ServiceRequest(TabletHome.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("skip",""+skip);
        params.put("limit",""+limit);
        params.put("page",""+page);
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));

        mRequest.makeServiceRequest(IConstant.promoList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------loadMorepromoList Response----------------" + response);
                String sStatus = "";

                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");




                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalCount = object.getInt("total");
                        JSONArray jsonArray = object.getJSONArray("content");
                        if(jsonArray.length()>0)
                        {
                            for(int i=0;i<jsonArray.length();i++)
                            {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                ReferNowBean referNowBean = new ReferNowBean();
                                referNowBean.setStrTitle(jsonObject.getString("disp_name"));
                                referNowBean.setStrId(jsonObject.getString("_id"));
                                referNowBean.setStrPromoName(jsonObject.getString("promo_name"));
                                referNowBean.setStrCouponCode(jsonObject.getString("coupon_code"));
                                referNowBean.setStrDesc("");

                                referNowList.add(referNowBean);

                            }
                            homeGroupReferNowAdapter.notifyDataSetChanged();
                            loadMoreEnable = 0;
                            mobileHomeRecyclerViewHorizontal.scrollToPosition(lastLoadedPosition);
                        }
                    }else if(sStatus.equals("00")){

                        customAlert.singleLoginAlertLogout();
                    }else if(sStatus.equalsIgnoreCase("01")){

                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),obj.getString("message"));
                    } else {

                        customAlert.showAlertOk(getString(R.string.alert_oops),obj.getString("message"));
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }
    private void prepareReferNowList() {
        mobileHomeRecyclerViewHorizontal = findViewById(R.id.mobileHomeRecyclerViewHorizontal);
        referNowList = new ArrayList<ReferNowBean>();
        referNowList.clear();
            ServiceRequest mRequest = new ServiceRequest(TabletHome.this);
            HashMap<String, String> params = new HashMap<>();
            params.put("skip",""+skip);
            params.put("limit",""+limit);
        params.put("page",""+page);
            params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));

            mRequest.makeServiceRequest(IConstant.promoList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("------------promoList Response----------------" + response);
                    String sStatus = "";

                    try {

                        JSONObject obj = new JSONObject(response);
                        sStatus = obj.getString("status");


                        if (sStatus.equalsIgnoreCase("1")) {
                            JSONObject object = obj.getJSONObject("response");
                            totalCount = object.getInt("total");
                            JSONArray jsonArray = object.getJSONArray("content");
                            if(jsonArray.length()>0)
                            {
                                mobileHomeRecyclerViewHorizontal.setVisibility(View.VISIBLE);
                                for(int i=0;i<jsonArray.length();i++)
                                {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    ReferNowBean referNowBean = new ReferNowBean();
                                    referNowBean.setStrTitle(jsonObject.getString("disp_name"));
                                    referNowBean.setStrId(jsonObject.getString("_id"));
                                    referNowBean.setStrPromoName(jsonObject.getString("promo_name"));
                                    referNowBean.setStrCouponCode(jsonObject.getString("coupon_code"));
                                    referNowBean.setStrDesc("");

                                    referNowList.add(referNowBean);

                                }
                               setReferNoewListAdapter();
                            }else
                            {
                                mobileHomeRecyclerViewHorizontal.setVisibility(View.GONE);
                            }
                        } else if(sStatus.equals("00")){

                            customAlert.singleLoginAlertLogout();
                        }else if(sStatus.equalsIgnoreCase("01")){

                            customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),obj.getString("message"));
                        } else {

                            customAlert.showAlertOk(getString(R.string.alert_oops),obj.getString("message"));
                        }
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                }

                @Override
                public void onErrorListener(String errorMessage) {
                    // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
                }
            });


    }
    private void setReferNoewListAdapter() {

        homeGroupReferNowAdapter = new HomeGroupReferNowAdapter(referNowList,TabletHome.this);
        mobileHomeRecyclerViewHorizontal.setLayoutManager(new LinearLayoutManager(TabletHome.this,RecyclerView.HORIZONTAL,false));
        mobileHomeRecyclerViewHorizontal.setAdapter(homeGroupReferNowAdapter);
        if(DisplayOrientation.getDisplayOrientation(TabletHome.this) == 0 || DisplayOrientation.getDisplayOrientation(TabletHome.this) == 2 ) {
            SnapHelper snapHelper = new PagerSnapHelper();
            snapHelper.attachToRecyclerView(mobileHomeRecyclerViewHorizontal);
        }
        homeGroupReferNowAdapter.notifyDataSetChanged();
        promoListListener();
        //performAdapterClick();


    }

    private void showAlertOOps(String title, String desc) {
        final Dialog alertDialog = new Dialog(TabletHome.this);
        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(title);
        textViewDesc.setText(desc);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                DrawerActivityData.lastSelectedMenuId = "2";
                DrawerActivityData.currentIndex = 1;
                Bundle bundle1 = new Bundle();
                TabSideMenu tabSideMenu = new TabSideMenu();
                tabSideMenu.setId("2");
                tabSideMenu.setImage_id_acative(R.drawable.complete_proile_inactive);
                tabSideMenu.setImage_id_inactive(R.drawable.complete_profile_active);
                tabSideMenu.setName(getString(R.string.my_profile));
                bundle1.putSerializable("sideMenu", tabSideMenu);
                DrawerActivityData.bundle = bundle1;
                startActivity(new Intent(TabletHome.this, DrawerContentSlideActivity.class).putExtra("sideMenu",tabSideMenu));
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                TabletHome.PagesActivity.finish();
            }
        });
        if(!alertDialog.isShowing())
            alertDialog.show();
    }

    @Override
    public void onGroupMenuSelected(Groupslist tabSideMenu, int position) {
        MobileHomeChildFragment mobileHomeChildFragment = new MobileHomeChildFragment(tabSideMenu.getHomegroupid(),TabletHome.this);
        Bundle bundle = new Bundle();
        bundle.putSerializable("sideMenu",tabSideMenu);
        Log.e("selected menu",tabSideMenu.getHomegroupname());
        GroupMenuData.currentSelectedMenu = position;
        mobileHomeChildFragment.setArguments(bundle);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.frameLayoutRight,mobileHomeChildFragment);
        fragmentTransaction.commit();
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        new AsyncGroupRunner().execute();
    }
    private class AsyncGroupRunner extends AsyncTask<String, String, String> {
        GroupslistDatabase groupslistDatabase = GroupslistDatabase.getAppDatabase(getApplicationContext());
        String result = "";
        List<Groupslist> groupslists = new ArrayList<>();

        @Override
        protected String doInBackground(String... strings) {
            ServiceRequest mRequest = new ServiceRequest(TabletHome.this);
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
            params.put("page_type","1");
            params.put("page","1");
            mRequest.makeServiceRequest(IConstant.groupList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("------------groupList Response----------------" + response);
                    String sStatus = "";
                    try {

                        JSONObject object = new JSONObject(response);
                        sStatus = object.getString("status");

                        groupslistDatabase.groupslistDao().nukeTable();
                        if (sStatus.equalsIgnoreCase("1")) {
                            groupslists.clear();
                            JSONObject jsonObject = new JSONObject(response);
                            final JSONArray result = jsonObject.getJSONObject("response").getJSONArray("groupslist");
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    TabSideMenu tabSideMenu = new TabSideMenu();
                                    tabSideMenu.setName(result.getJSONObject(i).getString("g_name"));
                                    tabSideMenu.setShortName(result.getJSONObject(i).getString("short_name"));
                                    tabSideMenu.setLongName(result.getJSONObject(i).getString("long_name"));
                                    tabSideMenu.setId(result.getJSONObject(i).getString("_id"));

                                    Groupslist groupslist = new Groupslist();
                                    groupslist.setHomegroupid(result.getJSONObject(i).getString("_id"));
                                    groupslist.setHomegroupname(result.getJSONObject(i).getString("g_name"));
                                    groupslist.setHomeshortName(result.getJSONObject(i).getString("short_name"));
                                    groupslist.setHomelongName(result.getJSONObject(i).getString("long_name"));

                                    Groupslist subscriptionCart = groupslistDatabase.groupslistDao().getGroupItem(result.getJSONObject(i).getString("_id"));
                                    if(subscriptionCart != null && subscriptionCart.getHomegroupid() != null && !subscriptionCart.getHomegroupid().isEmpty() && subscriptionCart.getHomegroupid().equalsIgnoreCase(result.getJSONObject(i).getString("_id")))
                                        groupslistDatabase.groupslistDao().update(groupslist);
                                    else {
                                        groupslistDatabase.groupslistDao().insertAll(groupslist);
                                    }

                                }


                            }
                        } else {
                        }
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                }

                @Override
                public void onErrorListener(String errorMessage) {
                    // AlertBox.showSnackBoxPrimaryBa(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
                }
            });
            return result;


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            // new AsyncSubjectsRunner(s).execute();
        }
    }


    @Override
    public void onBackPressed()
    {
        DrawerContentSlideActivity.drawerContentSlideActivity.finish();
        DrawerActivityData.lastSelectedMenuId = "2";
        DrawerActivityData.currentIndex = 1;
        Bundle bundle1 = new Bundle();
        TabSideMenu tabSideMenu = new TabSideMenu();
        tabSideMenu.setId("2");
        tabSideMenu.setImage_id_acative(R.drawable.complete_proile_inactive);
        tabSideMenu.setImage_id_inactive(R.drawable.complete_profile_active);
        tabSideMenu.setName(getString(R.string.my_profile));
        bundle1.putSerializable("sideMenu", tabSideMenu);
        DrawerActivityData.bundle = bundle1;
        startActivity(new Intent(TabletHome.this, DrawerContentSlideActivity.class).putExtra("sideMenu",tabSideMenu));
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
        finish();
    }
}
