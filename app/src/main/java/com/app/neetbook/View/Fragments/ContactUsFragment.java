package com.app.neetbook.View.Fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.app.neetbook.Model.StateBean;
import com.app.neetbook.Model.TabSideMenu;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Data.ContactUsData;
import com.app.neetbook.Utils.Data.DrawerActivityData;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.alerter.AlertBox;
import com.app.neetbook.Utils.widgets.DisplayOrientation;
import com.app.neetbook.View.DrawerContentSlideActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class ContactUsFragment extends Fragment implements View.OnClickListener {
    Spinner spinnerContactUs;
    EditText etContactUsDesc;
    TextView txtContactUsSpinHint;
    RelativeLayout RlContactUsSubmit;
    ArrayList<String> contactUsList = new ArrayList<>();
    ArrayList<StateBean> contactUsBeanList = new ArrayList<>();
    Loader loader;
    CustomAlert customAlert;
    SessionManager sessionManager;
    RelativeLayout rlSelectQueryContactUs;
    ConnectionDetector cd;
    ImageView ImgTC;
    private boolean isSelected = false;

    public ContactUsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getActivity());
        loader = new Loader(getActivity());
        customAlert = new CustomAlert(getActivity());
        cd = new ConnectionDetector(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        spinnerContactUs = view.findViewById(R.id.spinnerContactUs);
        txtContactUsSpinHint = view.findViewById(R.id.txtContactUsSpinHint);
        etContactUsDesc = view.findViewById(R.id.etContactUsDesc);
        rlSelectQueryContactUs = view.findViewById(R.id.rlSelectQueryContactUs);
        RlContactUsSubmit = view.findViewById(R.id.RlContactUsSubmit);
        ImgTC = view.findViewById(R.id.ImgTC);


        if(getResources().getBoolean(R.bool.isTablet)) {
            if(DisplayOrientation.getDisplayOrientation(getActivity()) == 1 || DisplayOrientation.getDisplayOrientation(getActivity()) == 3)
                Picasso.with(getActivity()).load(R.drawable.splash_logo).into(ImgTC);
            else
                Picasso.with(getActivity()).load(R.drawable.contact_us_text).into(ImgTC);
            manageOrientationChanges();
        }
        generateDroptown();

        listeners();
    }

    private void manageOrientationChanges() {

        if(ContactUsData.contactUsDesc != null && !ContactUsData.contactUsDesc.equals(""))
            etContactUsDesc.setText(ContactUsData.contactUsDesc);
        spinnerContactUs.setSelection(ContactUsData.contactUsDroptownSelectedPosition);
        txtContactUsSpinHint.setText(ContactUsData.contactUsSelectedItem);
        txtContactUsSpinHint.setTextColor(ContactUsData.contactUsSelectedItem.equalsIgnoreCase("Select Query")?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
    }

    private void listeners() {
        RlContactUsSubmit.setOnClickListener(this);
        rlSelectQueryContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerContactUs.performClick();
            }
        });
        spinnerContactUs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//                if(isSelected) {
                    txtContactUsSpinHint.setText(spinnerContactUs.getSelectedItem().toString());
                    if(getResources().getBoolean(R.bool.isTablet)) {
                        ContactUsData.contactUsDroptownSelectedPosition = spinnerContactUs.getSelectedItemPosition();
                        ContactUsData.contactUsSelectedItem = txtContactUsSpinHint.getText().toString();
                    }
                    txtContactUsSpinHint.setText(spinnerContactUs.getSelectedItem().toString());
                    if(position>0) {
                        txtContactUsSpinHint.setTextColor(getResources().getColor(R.color.black));
                    }
                    else {
                        txtContactUsSpinHint.setTextColor(getResources().getColor(R.color.hint_color));
                    }
                setButtonEnable();
//                }else
//                {
////                    ContactUsData.contactUsDroptownSelectedPosition = spinnerContactUs.getSelectedItemPosition();
////                    ContactUsData.contactUsSelectedItem = txtContactUsSpinHint.getText().toString();
//                    isSelected= true;
//                }
//                if(!etContactUsDesc.getText().toString().isEmpty() && !txtContactUsSpinHint.getText().toString().equalsIgnoreCase("Select"))
//                    RlContactUsSubmit.setBackground(getResources().getDrawable(R.drawable.login_btn_gratient));
//                else
//                    RlContactUsSubmit.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etContactUsDesc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setButtonEnable();
                if(!etContactUsDesc.getText().toString().isEmpty() && !txtContactUsSpinHint.getText().toString().equalsIgnoreCase("Select Query"))
                {
                    ContactUsData.contactUsDesc = etContactUsDesc.getText().toString();
                    //RlContactUsSubmit.setBackground(getResources().getDrawable(R.drawable.login_btn_gratient));
                }
//                else
                   // RlContactUsSubmit.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
            }
        });
    }

    private void generateDroptown() {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(getActivity());

        mRequest.makeServiceRequest(IConstant.contactUsQuery, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------contactUsQuery Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1")) {
                        contactUsList.add("SELECT QUERY");
                        StateBean stateBean1 = new StateBean();
                        stateBean1.setState_name("name");
                        stateBean1.set_id("id");
                        contactUsBeanList.add(stateBean1);
                        JSONArray listArray = object.getJSONArray("list");
                        if(listArray.length()>0)
                        {
                            for(int i=0;i<listArray.length();i++) {
                                JSONObject jsonObject = listArray.getJSONObject(i);
                                StateBean stateBean = new StateBean();
                                contactUsList.add(jsonObject.getString("description"));
                                stateBean.setState_name(jsonObject.getString("description"));
                                stateBean.set_id(jsonObject.getString("_id"));
                                contactUsBeanList.add(stateBean);
                            }
                            ArrayAdapter countryAdapter = new ArrayAdapter(getActivity(), R.layout.spinner_item,contactUsList);
                            countryAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                            spinnerContactUs.setAdapter(countryAdapter);
                            spinnerContactUs.setSelection(contactUsList.contains(ContactUsData.contactUsSelectedItem)?contactUsList.indexOf(ContactUsData.contactUsSelectedItem):0);
                        }
                    } else if(sStatus.equals("00")){
                        loader.dismissLoader();
                        customAlert.singleLoginAlertLogout();
                    }else if(sStatus.equalsIgnoreCase("01")){
                        loader.dismissLoader();
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    } else {
                        loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.RlContactUsSubmit:
                    if (txtContactUsSpinHint.getText().toString().equalsIgnoreCase("Select Query"))
                        AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.contactUsSelectTopicAlert));
                    else if (etContactUsDesc.getText().toString().isEmpty())
                        AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.contactUsEnterDescAlert));
                    else
                        submitContact();

                break;
        }
    }

    private void submitContact() {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String,String> param = new HashMap<>();
       param.put("query_id", contactUsBeanList.get(contactUsList.indexOf(txtContactUsSpinHint.getText().toString())).get_id());
       param.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        param.put("comment",etContactUsDesc.getText().toString());
        mRequest.makeServiceRequest(IConstant.contactUsSubmit, Request.Method.POST, param, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------contactUsSubmit Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1")) {
                        ContactUsData.clearData();
                        alertSubmitted(getString(R.string.alert_success),message);
                    }else if(sStatus.equals("00")){
                        customAlert.singleLoginAlertLogout();
                    }else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
            }
        });
    }



    private void alertSubmitted(String title, String desc)
    {
        final Dialog alertDialog = new Dialog(getActivity());
        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        ImageView imgAlert = alertDialog.findViewById(R.id.imgAlert);
        Picasso.with(getActivity()).load(R.drawable.right_mark).into(imgAlert);
        textViewTitle.setText(title);
        textViewDesc.setText(desc);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                TabSideMenu tabSideMenu2 = new TabSideMenu();
                tabSideMenu2.setId("2");
                tabSideMenu2.setName((sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) == null || sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals(""))?getActivity().getString(R.string.complete_profile):sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("nill")?getActivity().getString(R.string.complete_profile):getString(R.string.my_profile));
                tabSideMenu2.setImage_id_inactive(R.drawable.complete_proile_inactive);
                tabSideMenu2.setImage_id_acative(R.drawable.complete_profile_active);

                Bundle bundle = new Bundle();
                bundle.putSerializable("sideMenu",tabSideMenu2);
                if(getActivity().getResources().getBoolean(R.bool.isTablet)) {

                    // selectDrawerMenu(tabSideMenu.getId(),bundle);
                    DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                    DrawerActivityData.lastSelectedMenuId = "2";
                    DrawerActivityData.currentIndex = 1;
                    Bundle bundle1 = new Bundle();
                    TabSideMenu tabSideMenu = new TabSideMenu();
                    tabSideMenu.setId("2");
                    tabSideMenu.setImage_id_acative(R.drawable.complete_profile_active);
                    tabSideMenu.setImage_id_inactive(R.drawable.complete_proile_inactive);
                    tabSideMenu.setName(getString(R.string.home));

                    bundle1.putSerializable("sideMenu", tabSideMenu);
                    DrawerActivityData.bundle = bundle;
                    DrawerActivityData.isFirstTimeLoading = true;
                    startActivity(new Intent(getActivity(), DrawerContentSlideActivity.class).putExtra("sideMenu", tabSideMenu2));
                    getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                }else
                {
                        DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                        DrawerActivityData.lastSelectedMenuId = "2";
                        DrawerActivityData.currentIndex = 1;
                        Bundle bundle1 = new Bundle();
                        TabSideMenu tabSideMenu = new TabSideMenu();
                        tabSideMenu.setId("2");
                        tabSideMenu.setImage_id_acative(R.drawable.complete_profile_active);
                        tabSideMenu.setImage_id_inactive(R.drawable.complete_proile_inactive);
                        tabSideMenu.setName(getString(R.string.home));

                        bundle1.putSerializable("sideMenu", tabSideMenu);
                        DrawerActivityData.bundle = bundle;
                    DrawerActivityData.isFirstTimeLoading = true;
                        startActivity(new Intent(getActivity(), DrawerContentSlideActivity.class).putExtra("sideMenu", tabSideMenu));
                        getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);

                }
                }
            });

        alertDialog.show();
    }

    private void setButtonEnable() {
        if(!etContactUsDesc.getText().toString().isEmpty()&& !txtContactUsSpinHint.getText().toString().equalsIgnoreCase("Select Query"))
            RlContactUsSubmit.setBackground(getResources().getDrawable(R.drawable.login_btn_gratient));
        else
            RlContactUsSubmit.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
    }
}
