package com.app.neetbook.View.Fragments.subscription;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.app.neetbook.Adapter.HomeGroupNameAdapter;
import com.app.neetbook.Adapter.subscription.SubscriptionGroupNameAdapter;
import com.app.neetbook.Interfaces.OnMobileHomeMenuSelectedListioner;
import com.app.neetbook.Model.Groups;
import com.app.neetbook.Model.Groupslist;
import com.app.neetbook.R;
import com.app.neetbook.Subscriptionroom.SubscriptionGroupDatabase;
import com.app.neetbook.Subscriptionroom.SubscriptionGroups;
import com.app.neetbook.Utils.ActionBarActivityNeetBook;
import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Data.SubscriptionFragData;
import com.app.neetbook.Utils.EventBusPojo.ActionEvent;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.ThreadExecutor.AppExecutors;
import com.app.neetbook.View.DrawerContentSlideActivity;
import com.app.neetbook.View.Fragments.MobileHomeChildFragment;
import com.app.neetbook.View.NoInterNetActivity;
import com.app.neetbook.View.SideMenu.SubscriptionDetailActivity;
import com.app.neetbook.homepageRoom.GroupslistDatabase;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

public class SubscriptionFragment extends Fragment implements View.OnClickListener {

  private TabLayout tabLayout;
  private ViewPager viewPager;
  AppExecutors exector;
  SubscriptionGroupDatabase groupslistDatabase;
  private ArrayList<Groupslist> homeMenus = new ArrayList<>();
  private List<SubscriptionGroups> subscriptionGroups = new ArrayList<>();
  private CustomAlert customAlert;
  ConnectionDetector cd;
  SessionManager sessionManager;
  RecyclerView recyclerViewGroups;
  int lastPosition = 0;
  int positionToSelect = 0;
  LinearLayoutManager layoutManager;
  SubscriptionGroupNameAdapter homeGroupNameAdapter;
  private LinearSnapHelper snapHelper;
  RelativeLayout rlBack;
  LinearLayout llTitleContainer;
  TextView txtPageTitle;
  ShimmerFrameLayout mShimmerViewContainer ;
  public SubscriptionFragment() {
    // Required empty public constructor
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    EventBus.getDefault().register(this);
    exector=new AppExecutors();
    sessionManager = new SessionManager(getActivity());
    cd = new ConnectionDetector(getActivity());
    customAlert = new CustomAlert(getActivity());
    createSnap();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    View view = inflater.inflate(R.layout.fragment_subscription, container, false);
    mShimmerViewContainer  = view.findViewById(R.id.mShimmerViewContainer );
    mShimmerViewContainer.startShimmer();
    mShimmerViewContainer.setVisibility(View.VISIBLE);
    viewPager = view.findViewById(R.id.viewpager_subscription);
    recyclerViewGroups = view.findViewById(R.id.recyclerViewGroups);
    tabLayout = view.findViewById(R.id.tabs_subscription);
    llTitleContainer = view.findViewById(R.id.llTitleContainer);
    rlBack = view.findViewById(R.id.rlBack);
      txtPageTitle = view.findViewById(R.id.txtPageTitle);

    init();



    return view;
  }

  private void init() {
    if (cd.isConnectingToInternet())
      new AsyncSubscriptionRunner().execute();
    else {
      startActivity(new Intent(getActivity(), NoInterNetActivity.class));
      DrawerContentSlideActivity.drawerContentSlideActivity.finish();
    }
    rlBack.setOnClickListener(this);
      llTitleContainer.setVisibility(getActivity().getResources().getBoolean(R.bool.isTablet)?View.VISIBLE:View.GONE);
      txtPageTitle.setText(getString(R.string.subscription));
  }

  private void setupViewPager(final ViewPager viewPager) {
    ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager(),
        getActivity());


    for (int i=0; i<subscriptionGroups.size(); i++){

      if(SubscriptionFragData.currentGroupId.equalsIgnoreCase(subscriptionGroups.get(i).getGroupid()))
        positionToSelect = i;
      SubscriptionListFragment fView = new SubscriptionListFragment(subscriptionGroups.get(i).getGroupid(),getActivity());
      Bundle bundle = new Bundle();
      bundle.putString("groupId",subscriptionGroups.get(i).getGroupid());
      fView.setArguments(bundle);
      viewPagerAdapter.addFragment(fView,subscriptionGroups.get(i).getShortName());
    }

  /*  if(subscriptionGroups.size()==0){

      SubscriptionListFragment fView = new SubscriptionListFragment();
      Bundle bundle = new Bundle();
      bundle.putString("groupId","");
      fView.setArguments(bundle);
      viewPagerAdapter.addFragment(fView,"");
      viewPager.setAdapter(viewPagerAdapter);
    }*/


      setGroupAdapter(positionToSelect);
      viewPager.setAdapter(viewPagerAdapter);
      tabLayout.setTabTextColors(
              ContextCompat.getColor(getActivity(), R.color.white),
              ContextCompat.getColor(getActivity(), R.color.white)
      );
      tabLayout.setupWithViewPager(viewPager);
      for (int i = 0; i < tabLayout.getTabCount(); i++) {
        TabLayout.Tab tab = tabLayout.getTabAt(i);
        if (tab != null) {
          tab.setCustomView(viewPagerAdapter.getTabView(i));
        }
      }
      viewPager.setCurrentItem(positionToSelect);
      viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
          setGroupAdapter(position);
          viewPager.getAdapter().notifyDataSetChanged();
          layoutManager = ((LinearLayoutManager)recyclerViewGroups.getLayoutManager());
          int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
          int centeredItemPosition = totalVisibleItems / 2;
         /* recyclerViewGroups.smoothScrollToPosition(position != 0 ?position-1 : position);
          recyclerViewGroups.setScrollY(centeredItemPosition);*/
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
      });
      viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
        @Override
        public void transformPage(@NonNull View page, float position) {
          page.setAlpha(0f);
          page.setVisibility(View.VISIBLE);

          // Start Animation for a short period of time
          page.animate()
                  .alpha(1f)
                  .setDuration(page.getResources().getInteger(android.R.integer.config_shortAnimTime));
        }
      });
      //tabLayout.getTabAt(SubscriptionFragData.currentIndex).select();



    mShimmerViewContainer.setVisibility(View.GONE);


  }
  private void setGroupAdapter(int position) {

    if(homeGroupNameAdapter==null ) {

      homeGroupNameAdapter = new SubscriptionGroupNameAdapter(subscriptionGroups,getActivity(),position);
      layoutManager=new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
      layoutManager.setStackFromEnd(true);
      recyclerViewGroups.setLayoutManager(layoutManager);
     // snapHelper.attachToRecyclerView(recyclerViewGroups);
      recyclerViewGroups.setAdapter(homeGroupNameAdapter);
      recyclerViewGroups.scrollToPosition(position);

    } else{

      homeGroupNameAdapter.notifyPosition(position);
      scrollToCenter(layoutManager,recyclerViewGroups,position);
    }

    homeGroupNameAdapter.setOnGroupMenuSelectedListener(new OnMobileHomeMenuSelectedListioner() {
      @Override
      public void onGroupSelected(String groupId, int position) {
        viewPager.setCurrentItem(position);
        layoutManager = ((LinearLayoutManager)recyclerViewGroups.getLayoutManager());
        int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
        int centeredItemPosition = totalVisibleItems / 2;
      /*  recyclerViewGroups.smoothScrollToPosition(position != 0 ?position-1 : position);
        recyclerViewGroups.setScrollY(centeredItemPosition);*/
        scrollToCenter(layoutManager,recyclerViewGroups,position);
      }
    });
//        final LinearSnapHelper snapHelper = new LinearSnapHelper();
//
//        snapHelper.attachToRecyclerView(recyclerViewGroups);
//
//        recyclerViewGroups.setOnFlingListener(snapHelper);

  }


  public void scrollToCenter(LinearLayoutManager layoutManager, RecyclerView recyclerList, int clickPosition) {
    RecyclerView.SmoothScroller smoothScroller = createSnapScroller(recyclerList, layoutManager);

    if (smoothScroller != null) {
      smoothScroller.setTargetPosition(clickPosition);
      layoutManager.startSmoothScroll(smoothScroller);
    }
  }

  // This number controls the speed of smooth scroll
  private static final float MILLISECONDS_PER_INCH = 200f;

  private final static int DIMENSION = 2;
  private final static int HORIZONTAL = 0;
  private final static int VERTICAL = 1;

  @Nullable
  private LinearSmoothScroller createSnapScroller(RecyclerView mRecyclerView, final RecyclerView.LayoutManager layoutManager) {
    if (!(layoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider)) {
      return null;
    }
    return new LinearSmoothScroller(mRecyclerView.getContext()) {
      @Override
      protected void onTargetFound(View targetView, RecyclerView.State state, Action action) {
        int[] snapDistances = calculateDistanceToFinalSnap(layoutManager, targetView);
        final int dx = snapDistances[HORIZONTAL];
        final int dy = snapDistances[VERTICAL];
        final int time = calculateTimeForDeceleration(Math.max(Math.abs(dx), Math.abs(dy)));
        if (time > 0) {
          action.update(dx, dy, time, mDecelerateInterpolator);
        }
      }


      @Override
      protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
        return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
      }
    };
  }


  private int[] calculateDistanceToFinalSnap(@NonNull RecyclerView.LayoutManager layoutManager, @NonNull View targetView) {
    int[] out = new int[DIMENSION];
    if (layoutManager.canScrollHorizontally()) {
      out[HORIZONTAL] = distanceToCenter(layoutManager, targetView,
              OrientationHelper.createHorizontalHelper(layoutManager));
    }

    if (layoutManager.canScrollVertically()) {
      out[VERTICAL] = distanceToCenter(layoutManager, targetView,
              OrientationHelper.createHorizontalHelper(layoutManager));
    }
    return out;
  }


  private int distanceToCenter(@NonNull RecyclerView.LayoutManager layoutManager,
                               @NonNull View targetView, OrientationHelper helper) {
    final int childCenter = helper.getDecoratedStart(targetView)
            + (helper.getDecoratedMeasurement(targetView) / 2);
    final int containerCenter;
    if (layoutManager.getClipToPadding()) {
      containerCenter = helper.getStartAfterPadding() + helper.getTotalSpace() / 2;
    } else {
      containerCenter = helper.getEnd() / 2;
    }
    return childCenter - containerCenter;
  }






  private void getGroupList() {
    try {

      exector.diskIO().execute(new Runnable() {
        @Override
        public void run() {

          try{

            subscriptionGroups.clear();
            groupslistDatabase = SubscriptionGroupDatabase.getAppDatabase(getActivity());

            Log.e("GroupList", new Gson().toJson(groupslistDatabase.subscriptionGroupDao().getAll().toString()));

            subscriptionGroups =groupslistDatabase.subscriptionGroupDao().getAll();

            ActionEvent event=new ActionEvent();
            event.setAction("success2");
            EventBus.getDefault().post(event);

          }catch(Exception e){

            System.out.println("Exception---"+e.toString());
          }

        }
      });

    } catch (Exception e) {

      System.out.println("Exception---" + e.toString());

    }
  }

  @Override
  public void onClick(View v) {
    switch (v.getId())
    {
      case R.id.rlBack:
        getActivity().finish();
        break;
    }
  }

  private class ViewPagerAdapter extends FragmentPagerAdapter {

    List<Fragment> fragmentList = new ArrayList<>();
    List<String> fragmentTitles = new ArrayList<>();
    Context mContext;

    ViewPagerAdapter(FragmentManager fragmentManager, FragmentActivity activity) {
      super(fragmentManager);
      mContext = activity;
    }

    @Override
    public Fragment getItem(int position) {

      if(subscriptionGroups.size()==0){

        return new SubscriptionListFragment();
      }

      return new SubscriptionListFragment(subscriptionGroups.get(position).getGroupid(),getActivity());
    }
    @Override
    public int getItemPosition(Object object) {
      SubscriptionListFragment f = (SubscriptionListFragment ) object;
      if (f != null) {
        f.update();
      }
      return super.getItemPosition(object);
    }
    @Override
    public int getCount() {
      return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
      return fragmentTitles.get(position);
    }

    void addFragment(SubscriptionListFragment fragment, String name) {
      fragmentList.add(fragment);
      fragmentTitles.add(name);
    }

    View getTabView(int position) {
      // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
      View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_items, null);
      TextView tv = v.findViewById(R.id.txtTabTitle);
      tv.setText(fragmentTitles.get(position));
      tv.setTextColor(getResources().getColor(R.color.white));
      return v;
    }
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    EventBus.getDefault().unregister(this);

  }

  @Subscribe(threadMode = ThreadMode.MAIN)
  public void onMessageEvent(ActionEvent event) {

    if(event.getAction().equals("success2")){
        setupViewPager(viewPager);
    }
  }

  private class AsyncSubscriptionRunner extends AsyncTask<String, String, String> {
    SubscriptionGroupDatabase groupslistDatabase = SubscriptionGroupDatabase.getAppDatabase(getActivity());
    String result = "";
    List<Groupslist> groupslists = new ArrayList<>();

    @Override
    protected void onPreExecute() {
      super.onPreExecute();

    }

    @Override
    protected String doInBackground(String... strings) {
      ServiceRequest mRequest = new ServiceRequest(getActivity());
      HashMap<String, String> params = new HashMap<>();
      params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
      params.put("page_type","2");
      params.put("page","1");
      mRequest.makeServiceRequest(IConstant.groupList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
        @Override
        public void onCompleteListener(String response) {
          System.out.println("------------AsyncSubscriptionRunner Response----------------" + response);
          String sStatus = "";
          try {

            JSONObject object = new JSONObject(response);
            sStatus = object.getString("status");
            groupslistDatabase.subscriptionGroupDao().nukeTable();

            if (sStatus.equalsIgnoreCase("1")) {
              groupslists.clear();
              JSONObject jsonObject = new JSONObject(response);
              final JSONArray result = jsonObject.getJSONObject("response").getJSONArray("groupslist");
              if (result.length() > 0) {
                for (int i = 0; i < result.length(); i++) {
                  SubscriptionGroups tabSideMenu = new SubscriptionGroups();
                  tabSideMenu.setGroupname(result.getJSONObject(i).getString("g_name"));
                  tabSideMenu.setShortName(result.getJSONObject(i).getString("short_name"));
                  tabSideMenu.setLongName(result.getJSONObject(i).getString("long_name"));
                  tabSideMenu.setGroupid(result.getJSONObject(i).getString("_id"));

                  SubscriptionGroups subscriptionCart = groupslistDatabase.subscriptionGroupDao().getGroupItem(result.getJSONObject(i).getString("_id"));
                  if(subscriptionCart != null && subscriptionCart.getGroupid() != null && !subscriptionCart.getGroupid().isEmpty() && subscriptionCart.getGroupid().equalsIgnoreCase(result.getJSONObject(i).getString("_id")))
                    groupslistDatabase.subscriptionGroupDao().update(tabSideMenu);
                  else {
                    groupslistDatabase.subscriptionGroupDao().insertAll(tabSideMenu);
                  }
                }

                try {



                } catch (Exception e) {

                  System.out.println("Exception---" + e.toString());

                }
              }

              getGroupList();


            } else {
            }
          } catch (JSONException e) {

            e.printStackTrace();
          }

        }

        @Override
        public void onErrorListener(String errorMessage) {
          // AlertBox.showSnackBoxPrimaryBa(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
        }
      });
      return result;


    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);


      // new AsyncSubjectsRunner(s).execute();
    }
  }

  private void createSnap() {
    snapHelper = new LinearSnapHelper() {
      @Override
      public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
        int targetPosition = -1;
        try {
          View centerView = findSnapView(layoutManager);
          if (centerView == null)
            return RecyclerView.NO_POSITION;

          int position = layoutManager.getPosition(centerView);

          if (layoutManager.canScrollHorizontally()) {
            if (velocityX < 0) {
              targetPosition = position - 1;
            } else {
              targetPosition = position + 1;
            }
          }

          if (layoutManager.canScrollVertically()) {
            if (velocityY < 0) {
              targetPosition = position - 1;
            } else {
              targetPosition = position + 1;
            }
          }

          final int firstItem = 0;
          final int lastItem = layoutManager.getItemCount() - 1;
          targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

        }catch (Exception e)
        {
          e.printStackTrace();
        }
        return targetPosition;
      }
    };
  }
}
