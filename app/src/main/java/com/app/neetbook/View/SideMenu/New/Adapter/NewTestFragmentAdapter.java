package com.app.neetbook.View.SideMenu.New.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.app.neetbook.Adapter.TestSectionAdapter;
import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.Interfaces.TestSection;
import com.app.neetbook.R;
import com.app.neetbook.View.SideMenu.New.Section;
import com.app.neetbook.View.SideMenu.New.TestFragmentpojo;
import com.shuhart.stickyheader.StickyAdapter;

import java.util.ArrayList;

public class NewTestFragmentAdapter extends StickyAdapter<RecyclerView.ViewHolder, RecyclerView.ViewHolder> {

    private ArrayList<TestFragmentpojo> test_array_list;
    private static final int LAYOUT_HEADER = 0;
    private static final int LAYOUT_CHILD = 1;
    private int currentSelectedPosition = -1;
    private int currentChileHeadSelectedPosition = -1;
    private int superChileSeletedPosition = -1;
    Context context;
    private String subjectId = "";
    private String subjectName = "";
    private String currentSelectedHeader, indexCount = "1";
    private int chileSeletedPosition = -1;
    private int index = 0;
    Typeface tfBold, tfMedium, tfRegular;
    private ItemClickListener itemClickListener;
    private String strPending, strPendingTestId;
    Dialog alertDialog;

    public NewTestFragmentAdapter(Context context, ArrayList<TestFragmentpojo> test_array_list, String subjectId, String subjectName, int currentSelectedPosition, int chileSeletedPosition, String strPending, String strPendingTestId) {

        // inflater = LayoutInflater.from(context);
        this.context = context;
        this.test_array_list = test_array_list;
        this.subjectId = subjectId;
        this.subjectName = subjectName;
        this.strPending = strPending;
        this.strPendingTestId = strPendingTestId;
        this.currentSelectedPosition = currentSelectedPosition;
        //this.chileSeletedPosition = chileSeletedPosition;
        //currentSelectedHeader = test_array_list.get(0).getLongName();
        tfBold = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaRegular.otf");
        alertDialog = new Dialog(context);
    }

    @Override
    public int getItemViewType(int position) {

            return test_array_list.get(position).type();
    }


    @Override
    public int getHeaderPositionForItem(int itemPosition) {
        return test_array_list.get(itemPosition).sectionPosition();
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(test_array_list.get(position).getIs_full_test()){

            ((HeaderViewholder) holder).txtFt.setVisibility(View.VISIBLE);
            ((HeaderViewholder) holder).textView17.setVisibility(View.GONE);
            ((HeaderViewholder) holder).ivExpand.setVisibility(View.GONE);
            ((HeaderViewholder) holder).book_image.setVisibility(View.GONE);

             String name=test_array_list.get(position).getFull_test_long_name();

            ((HeaderViewholder) holder).title.setText(name);
            ((HeaderViewholder) holder).txtFt.setText("FT" + (position + 1) + " ");

        } else {

            ((HeaderViewholder) holder).txtFt.setVisibility(View.GONE);
            ((HeaderViewholder) holder).textView17.setVisibility(View.VISIBLE);
            ((HeaderViewholder) holder).ivExpand.setVisibility(View.VISIBLE);
            ((HeaderViewholder) holder).book_image.setVisibility(View.VISIBLE);

            ((HeaderViewholder) holder).title.setText(test_array_list.get(position).getChapter_long_name());
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return createViewHolder(parent, LAYOUT_HEADER);


    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        //return new ItemViewholder(inflater.inflate(R.layout.new_test_fragment_layout, parent, false));

        if (viewType == Section.HEADER || viewType == Section.CUSTOM_HEADER) {
            return new HeaderViewholder(inflater.inflate(R.layout.new_test_fragment_layout, parent, false));
        }
        return new ItemViewHolder(inflater.inflate(R.layout.test_parent_child_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        HeaderViewholder headerViewholder = ((HeaderViewholder) holder);

        if(test_array_list.get(position).getIs_full_test()){

            ((HeaderViewholder) holder).txtFt.setVisibility(View.VISIBLE);
            ((HeaderViewholder) holder).textView17.setVisibility(View.GONE);
            ((HeaderViewholder) holder).ivExpand.setVisibility(View.GONE);
            ((HeaderViewholder) holder).book_image.setVisibility(View.GONE);

            String name=test_array_list.get(position).getFull_test_long_name();

            ((HeaderViewholder) holder).title.setText(name);
            ((HeaderViewholder) holder).txtFt.setText("FT" + (position + 1) + " ");

        } else {

            ((HeaderViewholder) holder).txtFt.setVisibility(View.GONE);
            ((HeaderViewholder) holder).textView17.setVisibility(View.VISIBLE);
            ((HeaderViewholder) holder).ivExpand.setVisibility(View.VISIBLE);
            ((HeaderViewholder) holder).book_image.setVisibility(View.VISIBLE);

            ((HeaderViewholder) holder).title.setText(test_array_list.get(position).getChapter_long_name());
        }

    }

    @Override
    public int getItemCount() {
        return test_array_list.size();
    }


    public static class HeaderViewholder extends RecyclerView.ViewHolder {
        TextView textView17;
        ImageView ivExpand;
        private TextView title, txtFt;
        ImageView book_image;
        LinearLayout parent_view;
        RecyclerView child_recyclerview;

        HeaderViewholder(View itemView) {
            super(itemView);
            ivExpand = itemView.findViewById(R.id.ivExpand);
            parent_view = itemView.findViewById(R.id.parentView);
            title = itemView.findViewById(R.id.tvCategoryTitle);

            textView17 = itemView.findViewById(R.id.textView17);
            book_image = itemView.findViewById(R.id.book_image);
            txtFt = itemView.findViewById(R.id.txtFt);
            child_recyclerview=itemView.findViewById(R.id.child_recyclerview);

        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {


        private LinearLayoutCompat llChildHead;
        private RelativeLayout rlParent;

        ItemViewHolder(View itemView) {
            super(itemView);
            llChildHead = itemView.findViewById(R.id.llChildHead);

        }
    }
}
