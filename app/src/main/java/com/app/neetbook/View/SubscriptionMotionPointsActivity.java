package com.app.neetbook.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.neetbook.Adapter.ArticleTabsAdapter;
import com.app.neetbook.Interfaces.OnMobileHomeMenuSelectedListioner;
import com.app.neetbook.Model.HeadData;
import com.app.neetbook.Model.PointsContent;
import com.app.neetbook.R;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.View.SideMenu.SubscriptionPointsActivity;
import com.app.neetbook.View.SideMenu.SubscriptionPointsFragment;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class SubscriptionMotionPointsActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private Context context;
    Typeface tfBold,tfMedium,tfRegular;
    SessionManager sessionManager;
    private CustomAlert customAlert;
    Loader loader;
    private ArrayList<HeadData> headDataList;
    private TextView textView,content_title_tv,textView2Pin;
    RecyclerView recyclerViewGroups;
    private String strPoint_id="",strSubjectId= "",strChapterId= "",is_coming_page="";
    public static ArrayList<PointsContent> pointsContentArrayList;
    LinearSnapHelper snapHelper;
    private int currentSelectedPosition = 0;
    Typeface tf;
    ArticleTabsAdapter homeGroupNameAdapter;
    LinearLayoutManager layoutManager;
    private static final float MILLISECONDS_PER_INCH = 200f;
    private final static int DIMENSION = 2;
    private final static int HORIZONTAL = 0;
    private final static int VERTICAL = 1;
    ImageView homeBack;
    MotionLayout pointsMotionActivity;
    private int Position = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_motion_points);

        context = getApplicationContext();
        StatusBarColorChange.updateStatusBarColor(SubscriptionMotionPointsActivity.this,getResources().getColor(R.color.subscription_green));
        init();

    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }
    private void init() {
        tfBold  = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium  = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular  = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaRegular.otf");
        sessionManager = new SessionManager(SubscriptionMotionPointsActivity.this);
        customAlert = new CustomAlert(SubscriptionMotionPointsActivity.this);
   //     loader = new Loader(SubscriptionMotionPointsActivity.this);
        headDataList = new ArrayList<>();
        viewPager = findViewById(R.id.recyclerview);

        textView = findViewById(R.id.chapterTitleConstraint);
        recyclerViewGroups = findViewById(R.id.ChapterListRecyclerView);
        content_title_tv=findViewById(R.id.subjectTitleConstraint);
        textView2Pin = findViewById(R.id.subjectTitleConstraint);
        homeBack = findViewById(R.id.home);
        pointsMotionActivity=(MotionLayout)findViewById(R.id.pointsLayout);



        if(getIntent().getExtras() != null)
        {
            strPoint_id = getIntent().getStringExtra("point_id");
            strChapterId = getIntent().getStringExtra("chapter_id");
            strSubjectId = getIntent().getStringExtra("subject_id");
            textView.setText(getIntent().getStringExtra("subject_name"));
            textView2Pin.setText(getIntent().getStringExtra("chapter_name"));

            if(sessionManager.getViewToBookMark() != null && !sessionManager.getViewToBookMark().isEmpty() && sessionManager.getViewToBookMark().equalsIgnoreCase("Yes"))
            {
                pointsContentArrayList = new Gson().fromJson(sessionManager.getBookmarkDetails().get(SessionManager.HEAD_LIST), new TypeToken<List<PointsContent>>() {}.getType());
            }
            //getPointsContent();

            if(getIntent().getExtras().containsKey("coming_page")){

                is_coming_page=getIntent().getStringExtra("coming_page");
            }
        }
        createSnap();
        setupViewPager(viewPager);

        setFontSize();

        homeBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        pointsMotionActivity.addTransitionListener(new MotionLayout.TransitionListener() {
            @Override
            public void onTransitionStarted(MotionLayout motionLayout, int i, int i1) {

            }

            @Override
            public void onTransitionChange(MotionLayout motionLayout, int i, int i1, float v) {
                setGroupAdapter(Position);
            }

            @Override
            public void onTransitionCompleted(MotionLayout motionLayout, int i) {

            }

            @Override
            public void onTransitionTrigger(MotionLayout motionLayout, int i, boolean b, float v) {

            }
        });
    }

    private void setFontSize() {
        textView.setTextAppearance(SubscriptionMotionPointsActivity.this,R.style.textViewMediumSubjectName);
        textView2Pin.setTextAppearance(SubscriptionMotionPointsActivity.this,R.style.textViewMediumChaptName);

        textView.setTypeface(tfMedium);
        textView2Pin.setTypeface(tfMedium);
    }

    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
    }
    private void setupViewPager(final ViewPager viewPager) {
        final ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this);

        if(pointsContentArrayList.size()>0)
        {
            for (int i = 0; i < pointsContentArrayList.size(); i++) {
                HeadData data = new HeadData();
                data._id = pointsContentArrayList.get(i).get_id();
                data.short_name = pointsContentArrayList.get(i).getShort_name();
                data.long_name = pointsContentArrayList.get(i).getLong_name();
                headDataList.add(data);
                viewPagerAdapter.addFragment(new SubscriptionPointsFragment(strSubjectId,strChapterId,pointsContentArrayList.get(i).get_id(),pointsContentArrayList.get(i).getLong_name(), pointsContentArrayList.get(i).getContent(),pointsContentArrayList.get(i).getPointsTitleContentList(),pointsContentArrayList.get(i).getUrl(),0), pointsContentArrayList.get(i).getShort_name());
                if(strPoint_id.equalsIgnoreCase(pointsContentArrayList.get(i).get_id())) {
                    currentSelectedPosition = i;
                    content_title_tv.setText(pointsContentArrayList.get(i).getLong_name());

                    sessionManager.updateBookMark("Points",pointsContentArrayList.get(i).get_id(),strSubjectId,getIntent().getStringExtra("subject_name"),new Gson().toJson(pointsContentArrayList),strChapterId,getIntent().getStringExtra("chapter_name"),"",strPoint_id,"");

                    sessionManager.LastReadSubjectBookmark(sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME), sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME), sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME));
                }

            }

        }


        setCollapseingToolBar();
        setGroupAdapter(currentSelectedPosition);
        layoutManager = ((LinearLayoutManager)recyclerViewGroups.getLayoutManager());
        int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
        int centeredItemPosition = totalVisibleItems / 2;
        recyclerViewGroups.smoothScrollToPosition(currentSelectedPosition != 0 ?currentSelectedPosition-1 : currentSelectedPosition);
        viewPager.setAdapter(viewPagerAdapter);

        viewPager.setCurrentItem(currentSelectedPosition);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setGroupAdapter(position);
                Position=position;
                layoutManager = ((LinearLayoutManager)recyclerViewGroups.getLayoutManager());
                int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                int centeredItemPosition = totalVisibleItems / 2;
                recyclerViewGroups.smoothScrollToPosition(position != 0 ?position-1 : position);

                content_title_tv.setText(pointsContentArrayList.get(position).getLong_name());
                //point_title_tv.setText(pointsContentArrayList.get(position).getLong_name());
                sessionManager.updateBookMark("Points",pointsContentArrayList.get(position).get_id(),strSubjectId,getIntent().getStringExtra("subject_name"),new Gson().toJson(pointsContentArrayList),strChapterId,getIntent().getStringExtra("chapter_name"),"",pointsContentArrayList.get(position).get_id(),"");
                sessionManager.LastReadSubjectBookmark(sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                        sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                        sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME));
                scrollToCenter(layoutManager,recyclerViewGroups,position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                if (position != 0 && position != pointsContentArrayList.size() - 1) {
                    page.setAlpha(0f);
                    page.setVisibility(View.VISIBLE);

                    // Start Animation for a short period of time
                    page.animate()
                            .alpha(1f)
                            .setDuration(page.getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }
        });
    }
    private class ViewPagerAdapter extends FragmentPagerAdapter {

        List<Fragment> fragmentList = new ArrayList<>();
        List<String> fragmentTitles = new ArrayList<>();
        Context mContext;
        TextView tabTitles;


        ViewPagerAdapter(FragmentManager fragmentManager,
                         FragmentActivity activity) {
            super(fragmentManager);
            mContext = activity;
        }

        @Override
        public Fragment getItem(int i) {
            return new SubscriptionPointsFragment(strSubjectId,strChapterId,pointsContentArrayList.get(i).get_id(),pointsContentArrayList.get(i).getLong_name(), pointsContentArrayList.get(i).getContent(),pointsContentArrayList.get(i).getPointsTitleContentList(),pointsContentArrayList.get(i).getUrl(),0);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitles.get(position);
        }

        void addFragment(Fragment fragment, String name) {
            fragmentList.add(fragment);
            fragmentTitles.add(name);
        }

        View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_items, null);
            tabTitles = v.findViewById(R.id.txtTabTitle);
            tabTitles.setText(fragmentTitles.get(position));
            tabTitles.setTextColor(getResources().getColor(R.color.white));
            return v;
        }
    }

    private void setCollapseingToolBar() {


        if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
            content_title_tv.setTextAppearance(context,R.style.expandedappbarSmall);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
            content_title_tv.setTextAppearance(context,R.style.collapsedappbarMedium);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
            content_title_tv.setTextAppearance(context,R.style.collapsedappbarLarge);
        }
        tf = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaBold.otf");
        content_title_tv.setTypeface(tf);
    }

    private void setGroupAdapter(int position) {

        if(homeGroupNameAdapter==null ) {

            homeGroupNameAdapter = new ArticleTabsAdapter(headDataList, SubscriptionMotionPointsActivity.this,position);
            layoutManager=new LinearLayoutManager(SubscriptionMotionPointsActivity.this, RecyclerView.HORIZONTAL, false);
           // layoutManager.setStackFromEnd(true);
            recyclerViewGroups.setLayoutManager(layoutManager);
            // snapHelper.attachToRecyclerView(recyclerViewGroups);
            recyclerViewGroups.setAdapter(homeGroupNameAdapter);
            recyclerViewGroups.scrollToPosition(position);

        } else{

            homeGroupNameAdapter.notifyPosition(position);
            scrollToCenter(layoutManager,recyclerViewGroups,position);
        }


        homeGroupNameAdapter.setOnGroupMenuSelectedListener(new OnMobileHomeMenuSelectedListioner() {
            @Override
            public void onGroupSelected(String groupId, int position) {
                Position=position;
                viewPager.setCurrentItem(position);
                layoutManager = ((LinearLayoutManager)recyclerViewGroups.getLayoutManager());
                int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                int centeredItemPosition = totalVisibleItems / 2;
                //recyclerViewGroups.smoothScrollToPosition(position != 0 ?position-1 : position);
                // recyclerViewGroups.setScrollY(centeredItemPosition);

                scrollToCenter(layoutManager,recyclerViewGroups,position);
            }
        });

    }

    public void scrollToCenter(LinearLayoutManager layoutManager, RecyclerView recyclerList, int clickPosition) {
        RecyclerView.SmoothScroller smoothScroller = createSnapScroller(recyclerList, layoutManager);

        if (smoothScroller != null) {
            smoothScroller.setTargetPosition(clickPosition);
            layoutManager.startSmoothScroll(smoothScroller);
        }
    }

    @Nullable
    private LinearSmoothScroller createSnapScroller(RecyclerView mRecyclerView, final RecyclerView.LayoutManager layoutManager) {
        if (!(layoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider)) {
            return null;
        }
        return new LinearSmoothScroller(mRecyclerView.getContext()) {
            @Override
            protected void onTargetFound(View targetView, RecyclerView.State state, Action action) {
                int[] snapDistances = calculateDistanceToFinalSnap(layoutManager, targetView);
                final int dx = snapDistances[HORIZONTAL];
                final int dy = snapDistances[VERTICAL];
                final int time = calculateTimeForDeceleration(Math.max(Math.abs(dx), Math.abs(dy)));
                if (time > 0) {
                    action.update(dx, dy, time, mDecelerateInterpolator);
                }
            }


            @Override
            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
            }
        };
    }

    private int[] calculateDistanceToFinalSnap(@NonNull RecyclerView.LayoutManager layoutManager, @NonNull View targetView) {
        int[] out = new int[DIMENSION];
        if (layoutManager.canScrollHorizontally()) {
            out[HORIZONTAL] = distanceToCenter(layoutManager, targetView,
                    OrientationHelper.createHorizontalHelper(layoutManager));
        }

        if (layoutManager.canScrollVertically()) {
            out[VERTICAL] = distanceToCenter(layoutManager, targetView,
                    OrientationHelper.createHorizontalHelper(layoutManager));
        }
        return out;
    }

    private int distanceToCenter(@NonNull RecyclerView.LayoutManager layoutManager,
                                 @NonNull View targetView, OrientationHelper helper) {
        final int childCenter = helper.getDecoratedStart(targetView)
                + (helper.getDecoratedMeasurement(targetView) / 2);
        final int containerCenter;
        if (layoutManager.getClipToPadding()) {
            containerCenter = helper.getStartAfterPadding() + helper.getTotalSpace() / 2;
        } else {
            containerCenter = helper.getEnd() / 2;
        }
        return childCenter - containerCenter;
    }


}