package com.app.neetbook.View.SideMenu;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.app.TabPojo.EventbusPojo;
import com.app.neetbook.Adapter.HomeSearchAdapter;
import com.app.neetbook.Adapter.MCQExamsSectionAdapter;
import com.app.neetbook.Adapter.MCQSectionAdapter;
import com.app.neetbook.Adapter.NewMCQExamsSectionAdapter;
import com.app.neetbook.Adapter.NewMCQSectionAdapter;
import com.app.neetbook.Adapter.SubscriptionCategoryMcqExamAdapter;
import com.app.neetbook.Adapter.sideMenuContent.SubscriptionCategoryMcqAdapter;
import com.app.neetbook.Interfaces.APMTSearchListioner;
import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.Interfaces.MCQChapterSection;
import com.app.neetbook.Interfaces.OnHeadingClickListener;
import com.app.neetbook.Interfaces.OnSearchItemClickListener;
import com.app.neetbook.Interfaces.TabArtcileChapterClickListener;
import com.app.neetbook.Model.HeadData;
import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.MCQHeaderModel;
import com.app.neetbook.Model.MCQTestChildModel;
import com.app.neetbook.Model.McqExam;
import com.app.neetbook.Model.NewMcqSuperListPojo;
import com.app.neetbook.Model.SearchBean;
import com.app.neetbook.Model.SuperDetails;
import com.app.neetbook.Model.sidemenuFromContent.CategoryArticle;
import com.app.neetbook.R;
import com.app.neetbook.TAbAdapter.TAbMCQExamYearAdapter;
import com.app.neetbook.TAbAdapter.TabMCQChildRecyclerAdapter;
import com.app.neetbook.TAbAdapter.TabMCQExamYearMainAdapter;
import com.app.neetbook.TAbAdapter.TabMCQMainAdapter;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.ItemOffsetDecoration;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.SmoothScroll.LinearLayoutManagerWithSmoothScroller;
import com.app.neetbook.View.ActivitySubscriptionListDialog;
import com.app.neetbook.View.CustomDialogActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.shuhart.stickyheader.StickyHeaderItemDecorator;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class McqFragment extends Fragment implements View.OnClickListener, ItemClickListener, OnHeadingClickListener, APMTSearchListioner, MCQSectionAdapter.OnItemClickInterface,
        NewMCQSectionAdapter.setOnItemClick, MCQExamsSectionAdapter.McqExamInterface, NewMCQExamsSectionAdapter.setOnItemClick {

    private ArrayList<NewMcqSuperListPojo> temArrayList;
    private ArrayList<CategoryArticle> mListChapter = new ArrayList<>();
    private ArrayList<MCQChapterSection> mcqChapterSectionArrayList = new ArrayList<>();
    private ArrayList<MCQChapterSection> mcqExamSectionArrayList = new ArrayList<>();
    private ArrayList<MCQChapterSection> mcqYearSectionArrayList = new ArrayList<>();
    private ArrayList<CategoryArticle> mExamList = new ArrayList<>();
    private ArrayList<CategoryArticle> mYearList = new ArrayList<>();
    private MCQSectionAdapter mcqSectionAdapter;
    private SubscriptionCategoryMcqAdapter mAdapter;
    private SubscriptionCategoryMcqExamAdapter mExamAdapter;
    MCQExamsSectionAdapter mcqExamsSectionAdapter;
    private RecyclerView mRecyclerView;
    StickyHeaderItemDecorator decorator;
    private Context context;
    Loader loader;
    SessionManager sessionManager;
    private CustomAlert customAlert;
    ConstraintLayout constrainChapterExam, constrainChapterHeading, constrainChapterYear;
    TextView txtExams, txtYear, txtChapter2, txtChapter1, txtHeadings, txtChapter3;
    View viewHeading, viewYear, viewExam;
    private int page = 1;
    int loadMoreEnable = 0;
    private int currentView = 1;
    private int totalSubsCount = 0;
    private int indexCount = 0;
    int currentSelectedPosition = 0;
    int firstVisibleInListview = 0;
    ImageView imageView8;

    ItemOffsetDecoration itemDecoration;
    String strSubjectId = "", strSubjectName = "", strisTrial = "", strSubjectShortName = "";

    SwipeRefreshLayout mSwipeRefreshLayout;

    CountDownTimer cTimer;
    Dialog alertDialog;
    LinearSnapHelper snapHelper;
    LinearLayoutManager linearLayoutManager;
    Dialog progressDialog;
    /*TabView*/
    private RecyclerView fragment_child_recyclerview;
    private Boolean item_is_selected = false;
    TabArtcileChapterClickListener tabArtcileChapterClickListener;
    private TabMCQMainAdapter adapter;
    private TabMCQExamYearMainAdapter tabMCQExamYearMainAdapter;
    private TabMCQChildRecyclerAdapter tabMCQChildRecyclerAdapter;
    private TAbMCQExamYearAdapter tAbMCQExamYearAdapter;
    Display display;
    int height1, width1;
    boolean isLoading;
    private String strHeadId = "", strChapterId = "",headLongName;
    private int currentChapterSelectedPosition = -1, currentHeadSelectedPosition = -1, currentSuperHeadSelectedPosition = -1;
    ConstraintLayout llConstrainSearch;
    EditText etSearch;
    private ImageView imgSearchClose, imgSearchGo;
    private HomeSearchAdapter homeSearchAdapter;
    private RecyclerView homeSearchRecyclerView;
    private int totalCountSearch = 0;
    private ArrayList<SearchBean> searchBeanArrayList = new ArrayList<>();
    //////////////////////
    private NewMCQSectionAdapter newMCQSectionAdapter;
    private NewMCQExamsSectionAdapter newMCQExamsSectionAdapter;
    private ActivitySubscriptionListDialog activitySubscriptionListDialog;
    private int indexPosition=0;
    ShimmerFrameLayout mShimmerViewContainer;
    ////////////////////

    public McqFragment(String subjectId, String subjectName, String strisTrial, String strSubjectShortName, String strHeadId, String strChapterId,String headLongName) {
        this.strSubjectId = subjectId;
        this.strSubjectName = subjectName;
        this.strSubjectShortName = strSubjectShortName;
        this.strisTrial = strisTrial;
        this.strHeadId = strHeadId;
        this.strChapterId = strChapterId;
        this.headLongName=headLongName;
    }

    public McqFragment(){

    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupStatusBarColor();
        sessionManager = new SessionManager(getActivity());
        customAlert = new CustomAlert(getActivity());
        loader = new Loader(getActivity());
        alertDialog = new Dialog(getActivity());
        linearLayoutManager = new LinearLayoutManager(getActivity(),
                RecyclerView.VERTICAL, false);
        createSnap();
        display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width1 = size.x;
        height1 = size.y;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_mcq, container, false);
      //  CreateLoader();
        if (getActivity().getResources().getBoolean(R.bool.isTablet))
            initTab(root);
        else
            init(root);
        return root;
    }

    private void initTab(View root) {
        mRecyclerView = root.findViewById(R.id.parent_recyclerview);

        // mSwipeRefreshLayout = root.findViewById(R.id.mSwipeRefreshLayout);


        constrainChapterYear = root.findViewById(R.id.constrainChapterYear);
        fragment_child_recyclerview = root.findViewById(R.id.fragment_child_recyclerview);
        constrainChapterHeading = root.findViewById(R.id.constrainChapterHeading);
        txtChapter1 = root.findViewById(R.id.txtChapter1);
        txtHeadings = root.findViewById(R.id.txtHeadings);
        viewHeading = root.findViewById(R.id.viewHeading);
        constrainChapterExam = root.findViewById(R.id.constrainChapterExam);
        txtChapter2 = root.findViewById(R.id.txtChapter2);
        txtChapter3 = root.findViewById(R.id.txtChapter3);
        txtExams = root.findViewById(R.id.txtExams);
        txtYear = root.findViewById(R.id.txtYear);
        viewExam = root.findViewById(R.id.viewExam);
        viewYear = root.findViewById(R.id.viewYear);
        imageView8 = root.findViewById(R.id.imageView8);


        constrainChapterHeading.setOnClickListener(this);
        constrainChapterYear.setOnClickListener(this);
        constrainChapterExam.setOnClickListener(this);

        page = 1;

        prepareChapters(1);
        //swipeRefresh();
        LoadMoreListener();
        imageView8.setVisibility(View.VISIBLE);
        onScrollRecyclerview();

    }

    private void init(View rootView) {

        temArrayList=new ArrayList<>();

        mSwipeRefreshLayout = rootView.findViewById(R.id.mSwipeRefreshLayout);
        mSwipeRefreshLayout.setEnabled(false);
        mSwipeRefreshLayout.setRefreshing(false);

        mRecyclerView = rootView.findViewById(R.id.recyclerViewMcq);
        constrainChapterYear = rootView.findViewById(R.id.constrainChapterYear);
        constrainChapterHeading = rootView.findViewById(R.id.constrainChapterHeading);
        txtChapter1 = rootView.findViewById(R.id.txtChapter1);
        txtHeadings = rootView.findViewById(R.id.txtHeadings);
        viewHeading = rootView.findViewById(R.id.viewHeading);
        constrainChapterExam = rootView.findViewById(R.id.constrainChapterExam);
        txtChapter2 = rootView.findViewById(R.id.txtChapter2);
        txtChapter3 = rootView.findViewById(R.id.txtChapter3);
        txtExams = rootView.findViewById(R.id.txtExams);
        txtYear = rootView.findViewById(R.id.txtYear);
        viewExam = rootView.findViewById(R.id.viewExam);
        viewYear = rootView.findViewById(R.id.viewYear);


        constrainChapterHeading.setOnClickListener(this);
        constrainChapterYear.setOnClickListener(this);
        constrainChapterExam.setOnClickListener(this);

        imageView8 = rootView.findViewById(R.id.imageView8);
        llConstrainSearch = rootView.findViewById(R.id.llConstrainSearch);
        etSearch = rootView.findViewById(R.id.etSearch);
        imgSearchClose = rootView.findViewById(R.id.imgSearchClose);
        imgSearchGo = rootView.findViewById(R.id.imgSearchGo);


        imgSearchClose.setOnClickListener(this);
        imgSearchGo.setOnClickListener(this);
        itemDecoration = new ItemOffsetDecoration(context, R.dimen._4sdp);
        imageView8.setOnClickListener(this);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!etSearch.getText().toString().isEmpty() && etSearch.getText().toString().length() > 3) {
                    imgSearchClose.setVisibility(View.GONE);
                    imgSearchGo.setVisibility(View.VISIBLE);
                }
            }
        });

       // CreateLoader();
        prepareChapters(1);
        swipeRefresh();
        LoadMoreListener();
        imageView8.setVisibility(View.VISIBLE);
    }

    private void CreateLoader() {
        progressDialog = new Dialog(getActivity());
        progressDialog.setContentView(R.layout.custom_progress);


        if (progressDialog.getWindow() != null)
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // startAnimation();
        progressDialog.setCancelable(false);
    }

    private void LoadMoreListener() {

        try {

            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                    int totalItemCount = layoutManager.getItemCount();
                    int lastVisible = layoutManager.findLastVisibleItemPosition();

                    boolean endHasBeenReached = lastVisible + 5 >= totalItemCount;
                    if (dy > 0) {
                        {
                            if (currentSelectedPosition < layoutManager.findFirstVisibleItemPosition()) {
                                if (currentView == 1)
                                    mcqSectionAdapter.CollapseExpandedView(-1);
                                else
                                    mcqExamsSectionAdapter.CollapseExpandedView(-1);
                            }
                        }
                    } else {
                        if (currentSelectedPosition > lastVisible) {
                            if (currentView == 1)
                                mcqSectionAdapter.CollapseExpandedView(-1);
                            else
                                mcqExamsSectionAdapter.CollapseExpandedView(-1);
                        }
                    }
                    if (layoutManager.findFirstVisibleItemPosition() >= 0 && !getActivity().getResources().getBoolean(R.bool.isTablet)) {
                        if (currentView == 1 && mcqChapterSectionArrayList.size() > 0)
                            mcqSectionAdapter.setTopItemView(mcqChapterSectionArrayList.get(layoutManager.findFirstVisibleItemPosition()).getMCQLongrName(), mcqChapterSectionArrayList.get(layoutManager.findFirstVisibleItemPosition()).getMCQCount(), layoutManager.findFirstVisibleItemPosition());
                        else if (currentView == 2 && mcqExamSectionArrayList.size() > 0)
                            mcqExamsSectionAdapter.setTopItemView(mcqExamSectionArrayList.get(layoutManager.findFirstVisibleItemPosition()).getMCQLongrName(), mcqExamSectionArrayList.get(layoutManager.findFirstVisibleItemPosition()).getMCQCount(), layoutManager.findFirstVisibleItemPosition());
                        else if (currentView == 3 && mcqYearSectionArrayList.size() > 0)
                            mcqExamsSectionAdapter.setTopItemView(mcqYearSectionArrayList.get(layoutManager.findFirstVisibleItemPosition()).getMCQLongrName(), mcqYearSectionArrayList.get(layoutManager.findFirstVisibleItemPosition()).getMCQCount(), layoutManager.findFirstVisibleItemPosition());

                    }
                    if ((lastVisible == totalItemCount - 1 || lastVisible == totalItemCount - 2)) {

                        Log.e("loadMoreSubjects()", "Called");

                        if (currentView == 1 && totalSubsCount > indexCount)
                            loadMoreprepareChapters(indexCount);
                        else if (currentView == 2 && totalSubsCount > indexCount)
                            loadMoreprepareChaptersExams(indexCount);
                        else if (currentView == 3 && totalSubsCount > indexCount)
                            loadMoreprepareChaptersYear(indexCount);
                        loadMoreEnable = 1;
                    }
                }
            });

        } catch (Exception e) {

        }

    }

    private void swipeRefresh() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;

                indexCount = 0;
                loadMoreEnable = 0;
                isLoading = true;
                if (currentView == 1) {
                    mListChapter = new ArrayList<>();
                    mcqChapterSectionArrayList = new ArrayList<>();
                    prepareChapters(1);
                } else if (currentView == 2) {
                    mExamList = new ArrayList<>();
                    mcqExamSectionArrayList = new ArrayList<>();
                    prepareChaptersExams(2);
                } else if (currentView == 3) {
                    mYearList = new ArrayList<>();
                    mcqYearSectionArrayList = new ArrayList<>();
                    prepareChaptersYear(3);
                }

                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    private void setupStatusBarColor() {
        ((TabMainActivity) context).updateStatusBarColor(context.getResources()
                .getColor(R.color.category_mcq_header_blue));
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.constrainChapterHeading:
                indexPosition=0;
                if (!isLoading) {
                    constrainChapterYear.setEnabled(false);
                    constrainChapterExam.setEnabled(false);
                    //AdapterClear();
                    mRecyclerView.setVisibility(View.GONE);
                    currentView = 1;
                    page = 1;
                    mListChapter.clear();
                    prepareChapters(1);
                    constrainChapterHeading.setBackground(getActivity().getResources().getDrawable(R.drawable.mcq_chapter_selected));
                    constrainChapterYear.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_curve_buttons_mcq));
                    constrainChapterExam.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_curve_buttons_mcq));
                    txtChapter1.setTextColor(Color.WHITE);
                    viewHeading.setBackgroundColor(Color.WHITE);
                    txtHeadings.setTextColor(Color.WHITE);
                    txtChapter2.setTextColor(Color.BLACK);
                    viewExam.setBackgroundColor(Color.BLACK);
                    txtExams.setTextColor(Color.BLACK);
                    txtChapter3.setTextColor(Color.BLACK);
                    viewYear.setBackgroundColor(Color.BLACK);
                    txtYear.setTextColor(Color.BLACK);
                }
                break;

            case R.id.constrainChapterYear:
                indexPosition=1;
                if (!isLoading) {
                    constrainChapterHeading.setEnabled(false);
                    constrainChapterExam.setEnabled(false);
                    mRecyclerView.setVisibility(View.GONE);
                    mYearList.clear();
                    mcqYearSectionArrayList.clear();
                    currentView = 3;
                    page = 1;
                    prepareChaptersYear(3);
                    constrainChapterHeading.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_curve_buttons_mcq));
                    constrainChapterYear.setBackground(getActivity().getResources().getDrawable(R.drawable.mcq_chapter_selected));
                    constrainChapterExam.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_curve_buttons_mcq));
                    txtChapter1.setTextColor(Color.BLACK);
                    viewHeading.setBackgroundColor(Color.BLACK);
                    txtHeadings.setTextColor(Color.BLACK);
                    txtChapter2.setTextColor(Color.BLACK);
                    viewExam.setBackgroundColor(Color.BLACK);
                    txtExams.setTextColor(Color.BLACK);
                    txtChapter3.setTextColor(Color.WHITE);
                    viewYear.setBackgroundColor(Color.WHITE);
                    txtYear.setTextColor(Color.WHITE);
                }
                break;

            case R.id.constrainChapterExam:
                indexPosition=2;
                if (!isLoading) {
                    constrainChapterHeading.setEnabled(false);
                    constrainChapterYear.setEnabled(false);
                    mRecyclerView.setVisibility(View.GONE);
                    mExamList.clear();
                    mcqExamSectionArrayList.clear();
                    currentView = 2;
                    page = 1;
                    prepareChaptersExams(2);
                    constrainChapterHeading.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_curve_buttons_mcq));
                    constrainChapterYear.setBackground(getActivity().getResources().getDrawable(R.drawable.bg_curve_buttons_mcq));
                    constrainChapterExam.setBackground(getActivity().getResources().getDrawable(R.drawable.mcq_chapter_selected));
                    txtChapter1.setTextColor(Color.BLACK);
                    viewHeading.setBackgroundColor(Color.BLACK);
                    txtHeadings.setTextColor(Color.BLACK);
                    txtChapter2.setTextColor(Color.WHITE);
                    viewExam.setBackgroundColor(Color.WHITE);
                    txtExams.setTextColor(Color.WHITE);
                    txtChapter3.setTextColor(Color.BLACK);
                    viewYear.setBackgroundColor(Color.BLACK);
                    txtYear.setTextColor(Color.BLACK);
                }
                break;


            case R.id.imageView8:
                openSearch();
                break;

            case R.id.imgSearchClose:
                closeSearch();
                break;

            case R.id.imgSearchGo:
                searchAPMT();
                break;

        }
    }

    private void AdapterClear() {

        try {

            mcqChapterSectionArrayList.clear();

            mRecyclerView.setLayoutManager(linearLayoutManager);
            mcqSectionAdapter = new MCQSectionAdapter(getActivity(), this::onItemClick, mcqChapterSectionArrayList, strSubjectId, strSubjectShortName, width1, currentChapterSelectedPosition, currentHeadSelectedPosition, currentSuperHeadSelectedPosition);
            mRecyclerView.setAdapter(mcqSectionAdapter);

        } catch (Exception e) {

            Log.e("Exception_error", e.toString());
        }
    }

    private void openSearch() {

        InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
        etSearch.requestFocus();
        this.strChapterId="";
        llConstrainSearch.setVisibility(View.VISIBLE);

    }

    private void closeSearch() {
        etSearch.setText(null);
        InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(requireView().getWindowToken(), 0);
        llConstrainSearch.setVisibility(View.GONE);
    }

    private void searchAPMT() {
        llConstrainSearch.setVisibility(View.GONE);
        openSearchList(3, etSearch.getText().toString());
    }

    private void openSearchList(final int type, String searchKey) {
        etSearch.setText("");
        llConstrainSearch.setVisibility(View.GONE);
        alertDialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        alertDialog.setContentView(R.layout.homesearchlistdialog);
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(false);
           /* if(alertDialog.getWindow() != null) {
                alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }*/
        homeSearchRecyclerView = alertDialog.findViewById(R.id.homeSearchRecyclerView);
        final RelativeLayout rlEmptySearch = alertDialog.findViewById(R.id.rlEmptySearch);
        final ImageView imgSearchResultClose = alertDialog.findViewById(R.id.imgSearchResultClose);
        imgSearchResultClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog.isShowing())
                    alertDialog.dismiss();
            }
        });
        searchBeanArrayList.clear();
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("page", "" + page);
        params.put("search", "" + type);
        params.put("keyword", searchKey);
        mRequest.makeServiceRequest(IConstant.homeSearch, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------homeSearch Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalCountSearch = Integer.parseInt(object.getString("total"));

                        JSONArray testArray = object.getJSONArray("content");
                        if (testArray.length() > 0) {
                            rlEmptySearch.setVisibility(View.GONE);
                            for (int i = 0; i < testArray.length(); i++) {
                                SearchBean searchBean = new SearchBean();
                                JSONObject jsonObject = testArray.getJSONObject(i);
                                searchBean.setStrSubjectId(jsonObject.getString("subject_id"));
                                searchBean.setStrSubjectShortName(jsonObject.getString("subject_short_name"));
                                searchBean.setStrSubjectLongName(jsonObject.getString("subject_long_name"));
                                searchBean.setStrMImage(jsonObject.getString("mob_img"));
                                searchBean.setStrTImage(jsonObject.getString("tab_img"));
                                searchBean.setStrChaptId(jsonObject.getString("chapter_id"));
                                searchBean.setStrChaptLongName(jsonObject.getString("chapter_long_name"));
                                searchBean.setStrChaptShortName(jsonObject.getString("chapter_short_name"));
                                searchBean.setStrHeadId(jsonObject.has("heading_id") ? jsonObject.getString("heading_id") : "");
                                searchBean.setStrHeadLongName(jsonObject.has("heading_long_name") ? jsonObject.getString("heading_long_name") : "");
                                searchBean.setStrHeadShortName(jsonObject.has("heading_short_name") ? jsonObject.getString("heading_short_name") : "");
                                searchBean.setStrIsSubscribed(jsonObject.getString("subscribed_user"));
                                searchBeanArrayList.add(searchBean);
                            }
                            setupSearchAdapter(type);
                        } else {
                            rlEmptySearch.setVisibility(View.VISIBLE);
                        }

                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);

            }
        });
        if (!alertDialog.isShowing())
            alertDialog.show();

    }

    private void setupSearchAdapter(int menuType) {

        homeSearchAdapter = new HomeSearchAdapter(searchBeanArrayList, getActivity(), menuType);
        homeSearchRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        //snapHelper.attachToRecyclerView(homeSearchRecyclerView);
        homeSearchRecyclerView.setAdapter(homeSearchAdapter);
        homeSearchRecyclerView.setVisibility(View.VISIBLE);
        performSearchClick();
    }

    private void performSearchClick() {
        homeSearchAdapter.setOnSearchItemClickListener(new OnSearchItemClickListener() {
            @Override
            public void onSearchItemClicked(int position, int menuType) {
                if (alertDialog != null && alertDialog.isShowing())
                    alertDialog.dismiss();
                strHeadId = searchBeanArrayList.get(position).getStrHeadId();
                strChapterId = searchBeanArrayList.get(position).getStrChaptId();
                headLongName=searchBeanArrayList.get(position).getStrHeadLongName();
                if (indexPosition==0)
                    findCategoryListTitle(searchBeanArrayList.get(position).getStrChaptId(),searchBeanArrayList.get(position).getStrHeadId());
                else {
                    constrainChapterHeading.performClick();
                }



                  //  constrainChapterHeading.performClick();
                   // findCategoryListTitle(searchBeanArrayList.get(position).getStrChaptId(),searchBeanArrayList.get(position).getStrHeadLongName());
                   // findCategoryListInExam(searchBeanArrayList.get(position).getStrChaptId());


             //   prepareChapters(1);
            }
        });
    }

    private void prepareChapters(final int index) {
        indexPosition=0;
        mListChapter.clear();
        mcqChapterSectionArrayList.clear();
        indexCount = 0;
        page = 1;

     //   progressDialog.show();

        if (mRecyclerView != null)
            mRecyclerView.setVisibility(View.GONE);
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("subject_id", strSubjectId);
        params.put("page", "" + page);
        params.put("skip", "0");
        params.put("limit", "10");
        mRequest.makeServiceRequest(IConstant.headingIndex, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("Heading" + "-------- Response----------------" + response);
                String sStatus = "";
                String message = "";

                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalSubsCount = object.getInt("ctotal");
                        JSONArray jsonArray = object.getJSONArray("chapters");
                        if (jsonArray.length() > 0) {
                            //llEmpty.setVisibility(View.GONE);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                indexCount = indexCount + 1;
                                JSONObject chaptersObject = jsonArray.getJSONObject(i);
                                CategoryArticle listItem = new CategoryArticle();

                                MCQHeaderModel mcqHeaderModel = new MCQHeaderModel(indexCount);
                                mcqHeaderModel.setChapterId(chaptersObject.getString("_id"));
                                mcqHeaderModel.setChapterName(chaptersObject.getString("chapt_name"));
                                mcqHeaderModel.setChapterShortName(chaptersObject.getString("short_name"));
                                mcqHeaderModel.setChapterShorLongname(chaptersObject.getString("long_name"));
                                mcqHeaderModel.setChapterImage(chaptersObject.getString("img"));
                                mcqHeaderModel.setMcqCount(String.valueOf(indexCount));
                                if (chaptersObject.getString("_id").equalsIgnoreCase(strChapterId))
                                    currentChapterSelectedPosition = indexCount;
                                mcqChapterSectionArrayList.add(mcqHeaderModel);
                                listItem._id = chaptersObject.getString("_id");
                                listItem.chapt_name = chaptersObject.getString("chapt_name");
                                listItem.short_name = chaptersObject.getString("short_name");
                                listItem.long_name = chaptersObject.getString("long_name");
                                listItem.images = chaptersObject.getString("img");
                                ArrayList<HeadDetails> headDetailsArrayList = new ArrayList<HeadDetails>();
                                JSONArray headDetailsArray = chaptersObject.getJSONArray("headDetails");
                                MCQTestChildModel mcqTestChildModel = new MCQTestChildModel(indexCount);
                                mcqTestChildModel.setChapterId(chaptersObject.getString("_id"));
                                mcqTestChildModel.setChapterName(chaptersObject.getString("chapt_name"));
                                mcqTestChildModel.setChapterShortName(chaptersObject.getString("short_name"));
                                mcqTestChildModel.setChapterShorLongname(chaptersObject.getString("long_name"));
                                mcqTestChildModel.setChapterImage(chaptersObject.getString("img"));
                                mcqTestChildModel.setMcqCount(String.valueOf(indexCount));
                                if (headDetailsArray.length() > 0) {
                                    for (int j = 0; j < headDetailsArray.length(); j++) {
                                        HeadDetails headDetails = new HeadDetails();
                                        JSONObject headDetailsObject = headDetailsArray.getJSONObject(j);
                                        headDetails.isSH = headDetailsObject.getString("isSH");
                                        SuperDetails superDetails = new SuperDetails();
                                        if (headDetailsObject.getString("isSH").equals("1")) {
                                            JSONObject superDetailsObject = headDetailsObject.getJSONObject("superDetails");
                                            superDetails._id = superDetailsObject.getString("_id");
                                            superDetails.chapter = superDetailsObject.getString("chapter");
                                            superDetails.supr_head_name = superDetailsObject.getString("supr_head_name");
                                            superDetails.short_name = superDetailsObject.getString("short_name");
                                            superDetails.long_name = superDetailsObject.getString("long_name");
                                            superDetails.firsthead = superDetailsObject.getString("firsthead");
                                            superDetails.head = superDetailsObject.getString("head");
                                        }
                                        headDetails.superDetails = superDetails;
                                        JSONArray headDataArray = headDetailsObject.getJSONArray("headData");
                                        ArrayList<HeadData> headDataArrayList = new ArrayList<HeadData>();
                                        if (headDataArray.length() > 0) {
                                            for (int k = 0; k < headDataArray.length(); k++) {
                                                HeadData headData = new HeadData();
                                                JSONObject headDataObject = headDataArray.getJSONObject(k);
                                                headData._id = headDataObject.getString("_id");
                                                headData.head_name = headDataObject.getString("head_name");
                                                headData.long_name = headDataObject.getString("long_name");
                                                headData.short_name = headDataObject.getString("short_name");
                                                headData.chapter_id = headDataObject.getString("chapter_id");
                                                headData.position = headDataObject.getString("position");
                                                headData.isSH = headDataObject.getString("isSH");
                                                headData.firsthead = headDataObject.getString("firsthead");
                                                headDataArrayList.add(headData);
                                                if (headDataObject.getString("isSH").equalsIgnoreCase("1")) {

                                                    if (headDataObject.getString("_id").equalsIgnoreCase(strHeadId)) {
                                                        currentChapterSelectedPosition = indexCount;
                                                        currentHeadSelectedPosition = j;
                                                        currentSuperHeadSelectedPosition = k;
                                                    }


                                                } else {
                                                    if (headDataObject.getString("_id").equalsIgnoreCase(strHeadId)) {
                                                        currentChapterSelectedPosition = indexCount;
                                                        currentHeadSelectedPosition = j;
                                                        currentSuperHeadSelectedPosition = -1;
                                                    }
                                                }
                                            }
                                        }
                                        headDetails.headData = headDataArrayList;
                                        headDetailsArrayList.add(headDetails);
                                    }
                                }
                                mcqTestChildModel.setHeadDetailsArrayList(headDetailsArrayList);
                                mcqChapterSectionArrayList.add(mcqTestChildModel);
                                listItem.headDetailsArrayList = headDetailsArrayList;
                                mListChapter.add(listItem);
                            }
                            loadMoreEnable = 0;
                            mRecyclerView.setVisibility(View.VISIBLE);
                            if (getActivity().getResources().getBoolean(R.bool.isTablet))
                                setAdapter(0);
                            else{
                                setupRecyclerView(0);
                            }

                        }
//            else{
//              llEmpty.setVisibility(View.VISIBLE);}

                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else if (sStatus.equalsIgnoreCase("2")) {
                        packageExpiredAlert(message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }
                    progressDialog.dismiss();

                } catch (JSONException e) {

                    e.printStackTrace();
                }

                constrainChapterYear.setEnabled(true);
                constrainChapterExam.setEnabled(true);
                constrainChapterHeading.setEnabled(true);


            }

            @Override
            public void onErrorListener(String errorMessage) {

                constrainChapterYear.setEnabled(true);
                constrainChapterExam.setEnabled(true);
                constrainChapterHeading.setEnabled(true);
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void loadMoreprepareChapters(final int index) {

        page = page + 1;
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("subject_id", strSubjectId);
        params.put("page", "" + page);
        params.put("skip", "0");
        params.put("limit", "10");
        mRequest.makeServiceRequest(IConstant.headingIndex, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("Heading" + "-------- Response----------------" + response);
                String sStatus = "";
                String message = "";

                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";

                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        JSONArray jsonArray = object.getJSONArray("chapters");
                        if (jsonArray.length() > 0) {
                            //llEmpty.setVisibility(View.GONE);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                indexCount = indexCount + 1;
                                JSONObject chaptersObject = jsonArray.getJSONObject(i);
                                CategoryArticle listItem = new CategoryArticle();

                                MCQHeaderModel mcqHeaderModel = new MCQHeaderModel(indexCount);
                                mcqHeaderModel.setChapterId(chaptersObject.getString("_id"));
                                mcqHeaderModel.setChapterName(chaptersObject.getString("chapt_name"));
                                mcqHeaderModel.setChapterShortName(chaptersObject.getString("short_name"));
                                mcqHeaderModel.setChapterShorLongname(chaptersObject.getString("long_name"));
                                mcqHeaderModel.setChapterImage(chaptersObject.getString("img"));
                                mcqHeaderModel.setMcqCount(String.valueOf(indexCount));

                                mcqChapterSectionArrayList.add(mcqHeaderModel);

                                listItem._id = chaptersObject.getString("_id");
                                listItem.chapt_name = chaptersObject.getString("chapt_name");
                                listItem.short_name = chaptersObject.getString("short_name");
                                listItem.long_name = chaptersObject.getString("long_name");
                                listItem.images = chaptersObject.getString("img");
                                ArrayList<HeadDetails> headDetailsArrayList = new ArrayList<HeadDetails>();
                                JSONArray headDetailsArray = chaptersObject.getJSONArray("headDetails");
                                MCQTestChildModel mcqTestChildModel = new MCQTestChildModel(indexCount);
                                mcqTestChildModel.setChapterId(chaptersObject.getString("_id"));
                                mcqTestChildModel.setChapterName(chaptersObject.getString("chapt_name"));
                                mcqTestChildModel.setChapterShortName(chaptersObject.getString("short_name"));
                                mcqTestChildModel.setChapterShorLongname(chaptersObject.getString("long_name"));
                                mcqTestChildModel.setChapterImage(chaptersObject.getString("img"));
                                mcqTestChildModel.setMcqCount(String.valueOf(indexCount));
                                if (headDetailsArray.length() > 0) {
                                    for (int j = 0; j < headDetailsArray.length(); j++) {
                                        HeadDetails headDetails = new HeadDetails();


                                        JSONObject headDetailsObject = headDetailsArray.getJSONObject(j);
                                        headDetails.isSH = headDetailsObject.getString("isSH");
                                        SuperDetails superDetails = new SuperDetails();
                                        if (headDetailsObject.getString("isSH").equals("1")) {
                                            JSONObject superDetailsObject = headDetailsObject.getJSONObject("superDetails");
                                            superDetails._id = superDetailsObject.getString("_id");
                                            superDetails.chapter = superDetailsObject.getString("chapter");
                                            superDetails.supr_head_name = superDetailsObject.getString("supr_head_name");
                                            superDetails.short_name = superDetailsObject.getString("short_name");
                                            superDetails.long_name = superDetailsObject.getString("long_name");
                                            superDetails.firsthead = superDetailsObject.getString("firsthead");
                                            superDetails.head = superDetailsObject.getString("head");

                                        }
                                        headDetails.superDetails = superDetails;

                                        JSONArray headDataArray = headDetailsObject.getJSONArray("headData");
                                        ArrayList<HeadData> headDataArrayList = new ArrayList<HeadData>();
                                        if (headDataArray.length() > 0) {
                                            for (int k = 0; k < headDataArray.length(); k++) {
                                                HeadData headData = new HeadData();
                                                JSONObject headDataObject = headDataArray.getJSONObject(k);
                                                headData._id = headDataObject.getString("_id");
                                                headData.head_name = headDataObject.getString("head_name");
                                                headData.long_name = headDataObject.getString("long_name");
                                                headData.short_name = headDataObject.getString("short_name");
                                                headData.chapter_id = headDataObject.getString("chapter_id");
                                                headData.position = headDataObject.getString("position");
                                                headData.isSH = headDataObject.getString("isSH");
                                                headData.firsthead = headDataObject.getString("firsthead");
                                                headDataArrayList.add(headData);
                                            }
                                        }
                                        headDetails.headData = headDataArrayList;
                                        headDetailsArrayList.add(headDetails);


                                    }
                                }
                                mcqTestChildModel.setHeadDetailsArrayList(headDetailsArrayList);
                                mcqChapterSectionArrayList.add(mcqTestChildModel);
                                listItem.headDetailsArrayList = headDetailsArrayList;
                                mListChapter.add(listItem);
                            }
                            loadMoreEnable = 0;
                            mRecyclerView.setVisibility(View.VISIBLE);
                            if (getActivity().getResources().getBoolean(R.bool.isTablet))
                                setAdapter(index);
                            else{
                                //setupRecyclerView(index);
                            }

                        }
//            else{
//              llEmpty.setVisibility(View.VISIBLE);}
//            mShimmerViewContainer.stopShimmerAnimation();
//            mShimmerViewContainer.setVisibility(View.GONE);

                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else if (sStatus.equalsIgnoreCase("2")) {
                        packageExpiredAlert(message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {

                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void prepareChaptersExams(final int index) {
        indexPosition=1;
       // progressDialog.show();
        mExamList.clear();
        mcqExamSectionArrayList.clear();
        indexCount = 0;

        mRecyclerView.setVisibility(View.GONE);
        page = 1;
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("subject_id", strSubjectId);
        params.put("page", "" + page);
        params.put("skip", "0");
        params.put("limit", "10");
        mRequest.makeServiceRequest(IConstant.examIndex, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("examIndex-------- Response----------------" + response);
                String sStatus = "";
                String message = "";

                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalSubsCount = object.getInt("ctotal");
                        JSONArray jsonArray = object.getJSONArray("chapters");
                        if (jsonArray.length() > 0) {
                            //llEmpty.setVisibility(View.GONE);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                indexCount = indexCount + 1;
                                JSONObject chaptersObject = jsonArray.getJSONObject(i);
                                CategoryArticle listItem = new CategoryArticle();
                                listItem._id = chaptersObject.getString("_id");
                                listItem.chapt_name = chaptersObject.getString("chapt_name");
                                listItem.short_name = chaptersObject.getString("short_name");
                                listItem.long_name = chaptersObject.getString("long_name");
                                listItem.images = chaptersObject.getString("img");


                                MCQHeaderModel mcqHeaderModel = new MCQHeaderModel(indexCount);
                                mcqHeaderModel.setChapterId(chaptersObject.getString("_id"));
                                mcqHeaderModel.setChapterName(chaptersObject.getString("chapt_name"));
                                mcqHeaderModel.setChapterShortName(chaptersObject.getString("short_name"));
                                mcqHeaderModel.setChapterShorLongname(chaptersObject.getString("long_name"));
                                mcqHeaderModel.setChapterImage(chaptersObject.getString("img"));
                                mcqHeaderModel.setMcqCount(String.valueOf(indexCount));
                                mcqExamSectionArrayList.add(mcqHeaderModel);

                                JSONArray examsArray = chaptersObject.getJSONArray("exams");
                                ArrayList<McqExam> mcqExamArrayList = new ArrayList<McqExam>();
                                MCQTestChildModel mcqTestChildModel = new MCQTestChildModel(indexCount);
                                mcqTestChildModel.setChapterId(chaptersObject.getString("_id"));
                                mcqTestChildModel.setChapterName(chaptersObject.getString("chapt_name"));
                                mcqTestChildModel.setChapterShortName(chaptersObject.getString("short_name"));
                                mcqTestChildModel.setChapterShorLongname(chaptersObject.getString("long_name"));
                                mcqTestChildModel.setChapterImage(chaptersObject.getString("img"));
                                mcqTestChildModel.setMcqCount(String.valueOf(indexCount));
                                if (examsArray.length() > 0) {
                                    for (int j = 0; j < examsArray.length(); j++) {
                                        McqExam headData = new McqExam();
                                        JSONObject headDataObject = examsArray.getJSONObject(j);
                                        headData.set_id(headDataObject.getString("_id"));
                                        headData.setExam_cat_name(headDataObject.getString("exam_cat_name"));
                                        headData.setLong_name(headDataObject.getString("long_name"));
                                        headData.setShort_name(headDataObject.getString("short_name"));
                                        mcqExamArrayList.add(headData);
                                    }
                                }
                                listItem.mcqExamArrayList = mcqExamArrayList;
                                mcqTestChildModel.setMcqExamArrayLis(mcqExamArrayList);
                                mcqExamSectionArrayList.add(mcqTestChildModel);
                                mExamList.add(listItem);
                            }
                            loadMoreEnable = 0;
                            mRecyclerView.setVisibility(View.VISIBLE);
                            if (getActivity().getResources().getBoolean(R.bool.isTablet))
                                setExamAdapterTab(0);
                            else
                                setupExamRecyclerView(0);
                        }
//            else{
//              llEmpty.setVisibility(View.VISIBLE);}

                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else if (sStatus.equalsIgnoreCase("2")) {
                        packageExpiredAlert(message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

                constrainChapterHeading.setEnabled(true);
                constrainChapterExam.setEnabled(true);
                constrainChapterYear.setEnabled(true);

                progressDialog.dismiss();


            }

            @Override
            public void onErrorListener(String errorMessage) {

                try {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    constrainChapterHeading.setEnabled(true);
                    constrainChapterExam.setEnabled(true);
                    constrainChapterYear.setEnabled(true);
                }catch (Exception e){
                    e.printStackTrace();
                }

                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void loadMoreprepareChaptersExams(final int index) {
        indexPosition=1;
    /*if(!progressDialog.isShowing())
      progressDialog.show();*/
        page = page + 1;
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("subject_id", strSubjectId);
        params.put("page", "" + page);
        params.put("skip", "0");
        params.put("limit", "10");
        mRequest.makeServiceRequest(IConstant.examIndex, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("examIndex-------- Response----------------" + response);
                String sStatus = "";
                String message = "";
                /*progressDialog.dismiss();*/
                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        JSONArray jsonArray = object.getJSONArray("chapters");
                        if (jsonArray.length() > 0) {
                            //llEmpty.setVisibility(View.GONE);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                indexCount = indexCount + 1;
                                JSONObject chaptersObject = jsonArray.getJSONObject(i);
                                CategoryArticle listItem = new CategoryArticle();
                                listItem._id = chaptersObject.getString("_id");
                                listItem.chapt_name = chaptersObject.getString("chapt_name");
                                listItem.short_name = chaptersObject.getString("short_name");
                                listItem.long_name = chaptersObject.getString("long_name");
                                listItem.images = chaptersObject.getString("img");


                                MCQHeaderModel mcqHeaderModel = new MCQHeaderModel(indexCount);
                                mcqHeaderModel.setChapterId(chaptersObject.getString("_id"));
                                mcqHeaderModel.setChapterName(chaptersObject.getString("chapt_name"));
                                mcqHeaderModel.setChapterShortName(chaptersObject.getString("short_name"));
                                mcqHeaderModel.setChapterShorLongname(chaptersObject.getString("long_name"));
                                mcqHeaderModel.setChapterImage(chaptersObject.getString("img"));
                                mcqHeaderModel.setMcqCount(String.valueOf(indexCount));
                                mcqExamSectionArrayList.add(mcqHeaderModel);

                                JSONArray examsArray = chaptersObject.getJSONArray("exams");
                                ArrayList<McqExam> mcqExamArrayList = new ArrayList<McqExam>();
                                MCQTestChildModel mcqTestChildModel = new MCQTestChildModel(indexCount);
                                mcqTestChildModel.setChapterId(chaptersObject.getString("_id"));
                                mcqTestChildModel.setChapterName(chaptersObject.getString("chapt_name"));
                                mcqTestChildModel.setChapterShortName(chaptersObject.getString("short_name"));
                                mcqTestChildModel.setChapterShorLongname(chaptersObject.getString("long_name"));
                                mcqTestChildModel.setChapterImage(chaptersObject.getString("img"));
                                mcqTestChildModel.setMcqCount(String.valueOf(indexCount));
                                if (examsArray.length() > 0) {
                                    for (int j = 0; j < examsArray.length(); j++) {
                                        McqExam headData = new McqExam();
                                        JSONObject headDataObject = examsArray.getJSONObject(j);
                                        headData.set_id(headDataObject.getString("_id"));
                                        headData.setExam_cat_name(headDataObject.getString("exam_cat_name"));
                                        headData.setLong_name(headDataObject.getString("long_name"));
                                        headData.setShort_name(headDataObject.getString("short_name"));
                                        mcqExamArrayList.add(headData);
                                    }
                                }
                                listItem.mcqExamArrayList = mcqExamArrayList;
                                mcqTestChildModel.setMcqExamArrayLis(mcqExamArrayList);
                                mcqExamSectionArrayList.add(mcqTestChildModel);
                                mExamList.add(listItem);
                            }
                            loadMoreEnable = 0;
                            mRecyclerView.setVisibility(View.VISIBLE);
                            if (getActivity().getResources().getBoolean(R.bool.isTablet))
                                setExamAdapterTab(index);
                            else
                                setupExamRecyclerView(index);
                        }
//            else{
//              llEmpty.setVisibility(View.VISIBLE);}
//            mShimmerViewContainer.stopShimmerAnimation();
//            mShimmerViewContainer.setVisibility(View.GONE);
                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else if (sStatus.equalsIgnoreCase("2")) {
                        packageExpiredAlert(message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                /*progressDialog.dismiss();*/
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void prepareChaptersYear(final int index) {

        indexPosition = 2;
        indexCount = index;
     //   progressDialog.show();
        mYearList.clear();
        mcqYearSectionArrayList.clear();
        indexCount = 0;
        page = 1;

        mRecyclerView.setVisibility(View.GONE);
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("subject_id", strSubjectId);
        params.put("page", "" + page);
        params.put("skip", "0");
        params.put("limit", "10");
        mRequest.makeServiceRequest(IConstant.examyearindex, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("examyearindex-------- Response----------------" + response);
                String sStatus = "";
                String message = "";

                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalSubsCount = object.getInt("etotal");
                        JSONArray jsonArray = object.getJSONArray("exams");
                        if (jsonArray.length() > 0) {
                            //llEmpty.setVisibility(View.GONE);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                indexCount = indexCount + 1;
                                JSONObject chaptersObject = jsonArray.getJSONObject(i);
                                CategoryArticle listItem = new CategoryArticle();
                                listItem._id = chaptersObject.getString("_id");
                                listItem.chapt_name = chaptersObject.getString("exam_cat_name");
                                listItem.short_name = chaptersObject.getString("short_name");
                                listItem.long_name = chaptersObject.getString("long_name");


                                MCQHeaderModel mcqHeaderModel = new MCQHeaderModel(indexCount);
                                mcqHeaderModel.setChapterId(chaptersObject.getString("_id"));
                                mcqHeaderModel.setChapterName(chaptersObject.getString("exam_cat_name"));
                                mcqHeaderModel.setChapterShortName(chaptersObject.getString("short_name"));
                                mcqHeaderModel.setChapterShorLongname(chaptersObject.getString("long_name"));
                                mcqHeaderModel.setMcqCount(String.valueOf(indexCount));
                                mcqHeaderModel.setChapterImage(chaptersObject.getString("img"));

                                mcqYearSectionArrayList.add(mcqHeaderModel);

                                // listItem.images = chaptersObject.getString("img");
                                MCQTestChildModel mcqTestChildModel = new MCQTestChildModel(indexCount);
                                mcqTestChildModel.setChapterId(chaptersObject.getString("_id"));
                                mcqTestChildModel.setChapterName(chaptersObject.getString("exam_cat_name"));
                                mcqTestChildModel.setChapterShortName(chaptersObject.getString("short_name"));
                                mcqTestChildModel.setChapterShorLongname(chaptersObject.getString("long_name"));
                                mcqTestChildModel.setMcqCount(String.valueOf(indexCount));
                                mcqTestChildModel.setChapterImage(chaptersObject.getString("img"));
                                JSONArray examsArray = chaptersObject.getJSONArray("years");
                                ArrayList<McqExam> mcqExamArrayList = new ArrayList<McqExam>();

                                if (examsArray.length() > 0) {
                                    for (int j = 0; j < examsArray.length(); j++) {
                                        McqExam headData = new McqExam();
                                        JSONObject headDataObject = examsArray.getJSONObject(j);
                                        headData.set_id(headDataObject.getString("_id"));
                                        headData.setExam_cat_name(headDataObject.getString("year_cat_name"));
                                        headData.setLong_name(headDataObject.getString("long_name"));
                                        headData.setShort_name(headDataObject.getString("short_name"));
                                        mcqExamArrayList.add(headData);
                                    }
                                }

                                mcqTestChildModel.setMcqExamArrayLis(mcqExamArrayList);
                                mcqYearSectionArrayList.add(mcqTestChildModel);
                                listItem.mcqExamArrayList = mcqExamArrayList;
                                mYearList.add(listItem);
                            }

                            mRecyclerView.setVisibility(View.VISIBLE);
                            if (getActivity().getResources().getBoolean(R.bool.isTablet))
                                setYearAdapterTab(0);
                            else
                                setupYearRecyclerView(0);
                        }
                        loadMoreEnable = 0;
//            else{
//              llEmpty.setVisibility(View.VISIBLE);}

                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

                constrainChapterHeading.setEnabled(true);
                constrainChapterExam.setEnabled(true);
                constrainChapterYear.setEnabled(true);

                progressDialog.dismiss();

            }

            @Override
            public void onErrorListener(String errorMessage) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                constrainChapterHeading.setEnabled(true);
                constrainChapterExam.setEnabled(true);
                constrainChapterYear.setEnabled(true);
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void loadMoreprepareChaptersYear(final int index) {
        indexPosition=2;
    /*if(!progressDialog.isShowing())
      progressDialog.show();*/
        loader.showLoader();
        page = page + 1;
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("subject_id", strSubjectId);
        params.put("page", "" + page);
        params.put("skip", "0");
        params.put("limit", "10");
        mRequest.makeServiceRequest(IConstant.examyearindex, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("examyearindex-------- Response----------------" + response);
                String sStatus = "";
                String message = "";
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        JSONArray jsonArray = object.getJSONArray("exams");
                        if (jsonArray.length() > 0) {
                            //llEmpty.setVisibility(View.GONE);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                indexCount = indexCount + 1;
                                JSONObject chaptersObject = jsonArray.getJSONObject(i);
                                CategoryArticle listItem = new CategoryArticle();
                                listItem._id = chaptersObject.getString("_id");
                                listItem.chapt_name = chaptersObject.getString("exam_cat_name");
                                listItem.short_name = chaptersObject.getString("short_name");
                                listItem.long_name = chaptersObject.getString("long_name");


                                MCQHeaderModel mcqHeaderModel = new MCQHeaderModel(indexCount);
                                mcqHeaderModel.setChapterId(chaptersObject.getString("_id"));
                                mcqHeaderModel.setChapterName(chaptersObject.getString("exam_cat_name"));
                                mcqHeaderModel.setChapterShortName(chaptersObject.getString("short_name"));
                                mcqHeaderModel.setChapterShorLongname(chaptersObject.getString("long_name"));
                                mcqHeaderModel.setMcqCount(String.valueOf(indexCount));
                                mcqHeaderModel.setChapterImage(chaptersObject.getString("img"));

                                mcqYearSectionArrayList.add(mcqHeaderModel);

                                // listItem.images = chaptersObject.getString("img");
                                MCQTestChildModel mcqTestChildModel = new MCQTestChildModel(indexCount);
                                mcqTestChildModel.setChapterId(chaptersObject.getString("_id"));
                                mcqTestChildModel.setChapterName(chaptersObject.getString("exam_cat_name"));
                                mcqTestChildModel.setChapterShortName(chaptersObject.getString("short_name"));
                                mcqTestChildModel.setChapterShorLongname(chaptersObject.getString("long_name"));
                                mcqTestChildModel.setMcqCount(String.valueOf(indexCount));
                                mcqTestChildModel.setChapterImage(chaptersObject.getString("img"));
                                JSONArray examsArray = chaptersObject.getJSONArray("years");
                                ArrayList<McqExam> mcqExamArrayList = new ArrayList<McqExam>();

                                if (examsArray.length() > 0) {
                                    for (int j = 0; j < examsArray.length(); j++) {
                                        McqExam headData = new McqExam();
                                        JSONObject headDataObject = examsArray.getJSONObject(j);
                                        headData.set_id(headDataObject.getString("_id"));
                                        headData.setExam_cat_name(headDataObject.getString("year_cat_name"));
                                        headData.setLong_name(headDataObject.getString("long_name"));
                                        headData.setShort_name(headDataObject.getString("short_name"));
                                        mcqExamArrayList.add(headData);
                                    }
                                }

                                mcqTestChildModel.setMcqExamArrayLis(mcqExamArrayList);
                                mcqYearSectionArrayList.add(mcqTestChildModel);
                                listItem.mcqExamArrayList = mcqExamArrayList;
                                mYearList.add(listItem);
                            }

                            mRecyclerView.setVisibility(View.VISIBLE);
                            if (getActivity().getResources().getBoolean(R.bool.isTablet))
                                setYearAdapterTab(index);
                            else
                                setupYearRecyclerView(index);
                        }
                        loadMoreEnable = 0;
//            else{
//              llEmpty.setVisibility(View.VISIBLE);}
//            mShimmerViewContainer.stopShimmerAnimation();
//            mShimmerViewContainer.setVisibility(View.GONE);
                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else if (sStatus.equalsIgnoreCase("2")) {
                        packageExpiredAlert(message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                /*progressDialog.dismiss();*/
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void setupRecyclerView(int position) {

    /*snapHelper.attachToRecyclerView(mRecyclerView);
    mRecyclerView.removeItemDecoration(itemDecoration);
    mRecyclerView.addItemDecoration(itemDecoration);
    mAdapter = new SubscriptionCategoryMcqAdapter(context, mList,strSubjectId,strSubjectShortName);
    mRecyclerView.setAdapter(mAdapter);
    mRecyclerView.scrollToPosition(position);*/
        isLoading = false;
        if (position == 0) {
            // mRecyclerView.setLayoutManager(linearLayoutManager);
            mRecyclerView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(context));
            mcqSectionAdapter = new MCQSectionAdapter(getActivity(), this::onItemClick, mcqChapterSectionArrayList, strSubjectId, strSubjectShortName, width1, currentChapterSelectedPosition, currentHeadSelectedPosition, currentSuperHeadSelectedPosition);
            mRecyclerView.setAdapter(mcqSectionAdapter);
            mRecyclerView.removeItemDecoration(decorator);
            decorator = new StickyHeaderItemDecorator(mcqSectionAdapter);
            decorator.attachToRecyclerView(mRecyclerView);
            performClickAction(linearLayoutManager);
            findCategoryListTitle(strChapterId,headLongName);
        } else
            mcqSectionAdapter.notifyDataSetChanged();

        if (currentChapterSelectedPosition > 0) {
            // mRecyclerView.scrollToPosition(currentChapterSelectedPosition);
             mRecyclerView.smoothScrollToPosition(currentChapterSelectedPosition);
        }
    }

    private void performClickAction(final LinearLayoutManager linearLayoutManager) {
        mcqSectionAdapter.setOnItemClickListener(new ItemClickListener() {
            @Override
            public void onParentItemClick(int position) {
                currentSelectedPosition = position;
                int offset = position - linearLayoutManager.findFirstVisibleItemPosition();
                if (linearLayoutManager.findFirstVisibleItemPosition() > 0) offset -= 1;
                //   linearLayoutManager.scrollToPositionWithOffset(position, offset);

                smoothScroll(mRecyclerView, position, 280);
            }
        });
    }

    private static void smoothScroll(RecyclerView rv, int toPos, final int duration) throws IllegalArgumentException {
        final int TARGET_SEEK_SCROLL_DISTANCE_PX = 10000;     // See androidx.recyclerview.widget.LinearSmoothScroller
        int itemHeight = rv.getChildAt(0).getHeight();  // Height of first visible view! NB: ViewGroup method!
        itemHeight = itemHeight + 33;                   // Example pixel Adjustment for decoration?
        int fvPos = ((LinearLayoutManager) rv.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        int i = Math.abs((fvPos - toPos) * itemHeight);
        if (i == 0) {
            i = (int) Math.abs(rv.getChildAt(0).getY());
        }
        final int totalPix = i;                         // Best guess: Total number of pixels to scroll
        RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(rv.getContext()) {
            @Override
            protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }

            @Override
            protected int calculateTimeForScrolling(int dx) {
                int ms = (int) (duration * dx / (float) totalPix);
                // Now double the interval for the last fling.
                if (dx < TARGET_SEEK_SCROLL_DISTANCE_PX) {
                    ms = ms * 2;
                } // Crude deceleration!
                //lg(format("For dx=%d we allot %dms", dx, ms));
                return ms;
            }
        };
        //lg(format("Total pixels from = %d to %d = %d [ itemHeight=%dpix ]", fvPos, toPos, totalPix, itemHeight));
        smoothScroller.setTargetPosition(toPos);
        rv.getLayoutManager().startSmoothScroll(smoothScroller);
    }

    private void setupExamRecyclerView(int position) {
    /*mRecyclerView.setLayoutManager(linearLayoutManager);
    snapHelper.attachToRecyclerView(mRecyclerView);
    mRecyclerView.removeItemDecoration(itemDecoration);
    mRecyclerView.addItemDecoration(itemDecoration);
    mExamAdapter = new SubscriptionCategoryMcqExamAdapter(context, mExamList,strSubjectId,strSubjectShortName,"2");
    mRecyclerView.setAdapter(mExamAdapter);
    mRecyclerView.scrollToPosition(position);*/
        isLoading = false;
        if (position == 0) {
            //mRecyclerView.setLayoutManager(linearLayoutManager);
            mRecyclerView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(context));
            mcqExamsSectionAdapter = new MCQExamsSectionAdapter(context, mcqExamSectionArrayList, strSubjectId, strSubjectShortName, "2",this::parentClickListener);
            mRecyclerView.setAdapter(mcqExamsSectionAdapter);
            mRecyclerView.removeItemDecoration(decorator);
            decorator = new StickyHeaderItemDecorator(mcqExamsSectionAdapter);
            decorator.attachToRecyclerView(mRecyclerView);
            performExamClickAction(linearLayoutManager);
        } else
            mcqExamsSectionAdapter.notifyDataSetChanged();
    }

    private void performExamClickAction(final LinearLayoutManager linearLayoutManager) {
        mcqExamsSectionAdapter.setOnItemClickListener(new ItemClickListener() {
            @Override
            public void onParentItemClick(int position) {
                currentSelectedPosition = position;
                int offset = position - linearLayoutManager.findFirstVisibleItemPosition();
                if (linearLayoutManager.findFirstVisibleItemPosition() > 0) offset -= 1;
                //  linearLayoutManager.scrollToPositionWithOffset(position, offset);

                smoothScroll(mRecyclerView, position, 280);
            }
        });
    }

    private void setupYearRecyclerView(int position) {
    /*snapHelper.attachToRecyclerView(mRecyclerView);
    mRecyclerView.removeItemDecoration(itemDecoration);
    mRecyclerView.addItemDecoration(itemDecoration);
    mExamAdapter = new SubscriptionCategoryMcqExamAdapter(context, mYearList,strSubjectId,strSubjectShortName,"3");
    mRecyclerView.setAdapter(mExamAdapter);
    mRecyclerView.scrollToPosition(position);*/
        isLoading = false;
        if (position == 0) {
            //   mRecyclerView.setLayoutManager(linearLayoutManager);
            mRecyclerView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(context));
            mcqExamsSectionAdapter = new MCQExamsSectionAdapter(context, mcqYearSectionArrayList, strSubjectId, strSubjectShortName, "3",this::parentClickListener);
            mRecyclerView.setAdapter(mcqExamsSectionAdapter);
            mRecyclerView.removeItemDecoration(decorator);
            decorator = new StickyHeaderItemDecorator(mcqExamsSectionAdapter);
            decorator.attachToRecyclerView(mRecyclerView);
            performYearClickAction(linearLayoutManager);
        } else {
            mcqExamsSectionAdapter.notifyDataSetChanged();
        }
    }

    private void performYearClickAction(final LinearLayoutManager linearLayoutManager) {
        mcqExamsSectionAdapter.setOnItemClickListener(new ItemClickListener() {
            @Override
            public void onParentItemClick(int position) {
                currentSelectedPosition = position;
                int offset = position - linearLayoutManager.findFirstVisibleItemPosition();
                if (linearLayoutManager.findFirstVisibleItemPosition() > 0) offset -= 1;
                //  linearLayoutManager.scrollToPositionWithOffset(position, offset);

                smoothScroll(mRecyclerView, position, 280);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        linearLayoutManager = new LinearLayoutManager(getActivity(),
                RecyclerView.VERTICAL, false);
        /* prepareChapters(1);*/
        //page = 1;

    }

    @Override
    public void onPause() {
   /* if(cTimer != null && !TabMainActivity.isReadPage) {
      cTimer.cancel();
      cTimer = null;
    }*/

        super.onPause();
    }

    @Override
    public void onStop() {

        super.onStop();

    /*if(cTimer != null && !TabMainActivity.isReadPage) {
      cTimer.cancel();
      cTimer = null;
    }*/
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity;
        if (activity instanceof TabArtcileChapterClickListener) {
            tabArtcileChapterClickListener = (TabArtcileChapterClickListener) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        tabArtcileChapterClickListener = null;
    }

    private void packageExpiredAlert(String message) {

        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(getString(R.string.alert_oops));
        textViewDesc.setText(message);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                new AsyncUpdateSessionRunner("1").execute();
                TabMainActivity.tabMainActivity.finish();
            }
        });
        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    @Override
    public void onParentItemClick(int position) {
        ChildAdapterSet(position);
        tabArtcileChapterClickListener.onChapterClicked(mListChapter.get(position).long_name, mListChapter.get(position)._id, "", "");
        Log.e("MCQFragment", "onParentItemClick : " + position);
    }

    @Override
    public void onHeadingClicked(int parentPosition, int childPosition, String headId) {
        EventbusPojo pojo = new EventbusPojo();
        pojo.setChapterId(mListChapter.get(parentPosition)._id);
        pojo.setHeadId(headId);
        pojo.setMcqLoadingType("" + currentView);
        pojo.setChapterName(mListChapter.get(parentPosition).long_name);
        pojo.setIsSuperHeading(mListChapter.get(parentPosition).headDetailsArrayList.get(childPosition).isSH);
        pojo.setSuperHeadingName(mListChapter.get(parentPosition).headDetailsArrayList.get(childPosition).superDetails.long_name);
        pojo.setFragment_name("MCQ");
        EventBus.getDefault().post(pojo);
    }

    @Override
    public void onSearch(String headId, String ChapterId) {

    }


    @Override
    public void onItemClick(String rootId) {
        int parentPosition = 0,parentMainPosition=0;
        temArrayList.clear();

        for (int i = 0; i < mListChapter.size(); i++) {
            if (rootId.equalsIgnoreCase(mListChapter.get(i)._id)) {
                parentPosition = i;
            }
        }
        for (int i = 0; i < mcqChapterSectionArrayList.size(); i++) {
            if (rootId.equalsIgnoreCase(mcqChapterSectionArrayList.get(i).getMCQChapterId())) {
                parentMainPosition = i;
            }
        }

        /*for (int i=0;i<sectionHeaderArticleList.get(parentPosition).getHeadList().size();i++){
            if (sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).isSH.equalsIgnoreCase("0"))
                temArrayList.add(new NewSuperListPojo("0",sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(0).long_name,sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(0)._id,sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(0).firsthead,sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(0).short_name,parentId,sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(0)._id));
            else {
                String chapterId=sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).superDetails._id;
                temArrayList.add(new NewSuperListPojo("1",sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).superDetails.long_name,sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).superDetails._id,sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).superDetails.firsthead,"",chapterId,sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).superDetails._id));
                for (int j=0;j<sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.size();j++)
                    temArrayList.add(new NewSuperListPojo("11",sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(j).long_name,sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(j)._id,sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(j).firsthead,sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(j).short_name,chapterId,sectionHeaderArticleList.get(parentPosition).getHeadList().get(i).headData.get(j)._id));
            }
        }*/


        for (int i = 0; i < mListChapter.get(parentPosition).headDetailsArrayList.size(); i++) {
            if (mListChapter.get(parentPosition).headDetailsArrayList.get(i).isSH.equals("0")) {
                temArrayList.add(new NewMcqSuperListPojo("0", mListChapter.get(parentPosition).headDetailsArrayList.get(i).headData.get(0).long_name, mListChapter.get(parentPosition).headDetailsArrayList.get(i).headData.get(0)._id, mListChapter.get(parentPosition).headDetailsArrayList.get(i).headData.get(0)._id));
            } else if (mListChapter.get(parentPosition).headDetailsArrayList.get(i).isSH.equals("1")) {
                temArrayList.add(new NewMcqSuperListPojo("1", mListChapter.get(parentPosition).headDetailsArrayList.get(i).superDetails.long_name, mListChapter.get(parentPosition).headDetailsArrayList.get(i).superDetails._id, mListChapter.get(parentPosition).headDetailsArrayList.get(i).superDetails.firsthead));
                for (int s = 0; s < mListChapter.get(parentPosition).headDetailsArrayList.get(i).headData.size(); s++) {
                    if (mListChapter.get(parentPosition).headDetailsArrayList.get(i).headData.get(s).firsthead.equalsIgnoreCase(mListChapter.get(parentPosition).headDetailsArrayList.get(i).superDetails.firsthead)) {
                        temArrayList.add(new NewMcqSuperListPojo("11", mListChapter.get(parentPosition).headDetailsArrayList.get(i).headData.get(s).long_name, mListChapter.get(parentPosition).headDetailsArrayList.get(i).headData.get(s)._id, mListChapter.get(parentPosition).headDetailsArrayList.get(i).headData.get(s).firsthead));
                    }
                }
            }
        }

        CustomDialogActivity.mcqTempArrayList=temArrayList;
        CustomDialogActivity.mcqChapterSectionArrayList=mcqChapterSectionArrayList;
        CustomDialogActivity.strSubjectId=strSubjectId;
        CustomDialogActivity.strSubjectShortName=strSubjectShortName;
        CustomDialogActivity.ParentPosition=parentPosition;
        CustomDialogActivity.MainParentPosition=parentMainPosition;
        CustomDialogActivity.ParentLongName=mListChapter.get(parentPosition).long_name;
        CustomDialogActivity.ImageUrl=mListChapter.get(parentPosition).images;
        CustomDialogActivity.currentPage=getResources().getString(R.string.mcq1);
        CustomDialogActivity.SearchHeadingLongName="";
        startActivity(new Intent(getActivity(), CustomDialogActivity.class));
      /*  newMCQSectionAdapter = new NewMCQSectionAdapter(getActivity(), this::newOnItemClick, temArrayList,mcqChapterSectionArrayList, strSubjectId, strSubjectShortName,parentMainPosition);
        activitySubscriptionListDialog=new ActivitySubscriptionListDialog(getActivity(),newMCQSectionAdapter,mListChapter.get(parentPosition).long_name,parentPosition,mListChapter.get(parentPosition).images,getResources().getString(R.string.mcq));
        activitySubscriptionListDialog.setCanceledOnTouchOutside(true);
        activitySubscriptionListDialog.show();*/

    }

    public void findCategoryListTitle(String rootId,String SubjectName){

      if (rootId!=null && !rootId.equals("") ){
          int parentPosition = 0,parentMainPosition=0;
          temArrayList.clear();

          for (int i = 0; i < mListChapter.size(); i++) {
              if (rootId.equalsIgnoreCase(mListChapter.get(i)._id)) {
                  parentPosition = i;
              }
          }
          for (int i = 0; i < mcqChapterSectionArrayList.size(); i++) {
              if (rootId.equalsIgnoreCase(mcqChapterSectionArrayList.get(i).getMCQChapterId())) {
                  parentMainPosition = i;
              }
          }

          for (int i = 0; i < mListChapter.get(parentPosition).headDetailsArrayList.size(); i++) {
              if (mListChapter.get(parentPosition).headDetailsArrayList.get(i).isSH.equals("0")) {
                  temArrayList.add(new NewMcqSuperListPojo("0", mListChapter.get(parentPosition).headDetailsArrayList.get(i).headData.get(0).long_name, mListChapter.get(parentPosition).headDetailsArrayList.get(i).headData.get(0)._id, mListChapter.get(parentPosition).headDetailsArrayList.get(i).headData.get(0).firsthead));
              } else if (mListChapter.get(parentPosition).headDetailsArrayList.get(i).isSH.equals("1")) {
                  temArrayList.add(new NewMcqSuperListPojo("1", mListChapter.get(parentPosition).headDetailsArrayList.get(i).superDetails.long_name, mListChapter.get(parentPosition).headDetailsArrayList.get(i).superDetails._id, mListChapter.get(parentPosition).headDetailsArrayList.get(i).superDetails.firsthead));
                  for (int s = 0; s < mListChapter.get(parentPosition).headDetailsArrayList.get(i).headData.size(); s++) {
                      if (mListChapter.get(parentPosition).headDetailsArrayList.get(i).headData.get(s).firsthead.equalsIgnoreCase(mListChapter.get(parentPosition).headDetailsArrayList.get(i).superDetails.firsthead)) {
                          temArrayList.add(new NewMcqSuperListPojo("11", mListChapter.get(parentPosition).headDetailsArrayList.get(i).headData.get(s).long_name, mListChapter.get(parentPosition).headDetailsArrayList.get(i).headData.get(s)._id, mListChapter.get(parentPosition).headDetailsArrayList.get(i).headData.get(s).firsthead));
                      }
                  }
              }
          }
          InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
          assert imm != null;
          imm.hideSoftInputFromWindow(requireView().getWindowToken(), 0);

          CustomDialogActivity.mcqTempArrayList=temArrayList;
          CustomDialogActivity.mcqChapterSectionArrayList=mcqChapterSectionArrayList;
          CustomDialogActivity.strSubjectId=strSubjectId;
          CustomDialogActivity.strSubjectShortName=strSubjectShortName;
          CustomDialogActivity.ParentPosition=parentPosition;
          CustomDialogActivity.MainParentPosition=parentMainPosition;
          CustomDialogActivity.ParentLongName=mListChapter.get(parentPosition).long_name;
          CustomDialogActivity.ImageUrl=mListChapter.get(parentPosition).images;
          CustomDialogActivity.currentPage=getResources().getString(R.string.mcq1);
          CustomDialogActivity.SearchHeadingLongName=SubjectName;
          this.headLongName="";
          this.strChapterId="";
          startActivity(new Intent(getActivity(), CustomDialogActivity.class));

      }

    }

    @Override
    public void newOnItemClick(String message) {
        if (message.equalsIgnoreCase(getResources().getString(R.string.closedialog))) {
            if (activitySubscriptionListDialog != null)
                activitySubscriptionListDialog.dismiss();
        }
    }

    // MCQExamsSectionAdapter adapter listener
    @Override
    public void parentClickListener(String rootId) {
        int actualPosition=0;
       if (indexPosition==1){
           for (int i=0;i<mcqExamSectionArrayList.size();i++){
               if (rootId.equals(mcqExamSectionArrayList.get(i).getMCQChapterId()))
                   actualPosition=i;
           }

           CustomDialogActivity.mcqExamSectionArrayList=mcqExamSectionArrayList;
           CustomDialogActivity.strSubjectId=strSubjectId;
           CustomDialogActivity.strSubjectShortName=strSubjectShortName;
           CustomDialogActivity.MainParentPosition=actualPosition;
           CustomDialogActivity.ParentPosition=actualPosition/2;
           CustomDialogActivity.ParentLongName=mcqExamSectionArrayList.get(actualPosition).getMCQLongrName();
           CustomDialogActivity.ImageUrl=mcqExamSectionArrayList.get(actualPosition).getMCQImage();
           CustomDialogActivity.currentPage=getResources().getString(R.string.mcq2);
           CustomDialogActivity.strLoading_type=String.valueOf(indexPosition);
           startActivity(new Intent(getActivity(), CustomDialogActivity.class));
           /*newMCQExamsSectionAdapter=new NewMCQExamsSectionAdapter(getActivity(),this::onMcqChildItemClick,mcqExamSectionArrayList.get(actualPosition).getMcqExamsList().size(),mcqExamSectionArrayList,actualPosition,strSubjectId, strSubjectShortName, String.valueOf(indexPosition));
           activitySubscriptionListDialog=new ActivitySubscriptionListDialog(getActivity(),newMCQExamsSectionAdapter,mcqExamSectionArrayList.get(actualPosition).getMCQLongrName(),actualPosition/2,mcqExamSectionArrayList.get(actualPosition).getMCQImage(),getResources().getString(R.string.mcq));
           activitySubscriptionListDialog.setCanceledOnTouchOutside(true);
           activitySubscriptionListDialog.show();*/
       }else if (indexPosition==2){
           for (int i=0;i<mcqYearSectionArrayList.size();i++){
               if (rootId.equals(mcqYearSectionArrayList.get(i).getMCQChapterId()))
                   actualPosition=i;
           }

           CustomDialogActivity.mcqYearSectionArrayList=mcqYearSectionArrayList;
           CustomDialogActivity.strSubjectId=strSubjectId;
           CustomDialogActivity.strSubjectShortName=strSubjectShortName;
           CustomDialogActivity.MainParentPosition=actualPosition;
           CustomDialogActivity.ParentPosition=actualPosition/2;
          // CustomDialogActivity.length=mcqYearSectionArrayList.get(actualPosition).getMcqExamsList().size();
           CustomDialogActivity.ParentLongName=mcqYearSectionArrayList.get(actualPosition).getMCQLongrName();
           CustomDialogActivity.ImageUrl=mcqYearSectionArrayList.get(actualPosition).getMCQImage();
           CustomDialogActivity.currentPage=getResources().getString(R.string.mcq3);
           CustomDialogActivity.strLoading_type=String.valueOf(indexPosition);
           startActivity(new Intent(getActivity(), CustomDialogActivity.class));
          /* newMCQExamsSectionAdapter=new NewMCQExamsSectionAdapter(getActivity(),this::onMcqChildItemClick,mcqYearSectionArrayList.get(actualPosition).getMcqExamsList().size(),mcqYearSectionArrayList,actualPosition,strSubjectId, strSubjectShortName, String.valueOf(indexPosition));
           activitySubscriptionListDialog=new ActivitySubscriptionListDialog(getActivity(),newMCQExamsSectionAdapter,mcqYearSectionArrayList.get(actualPosition).getMCQLongrName(),actualPosition/2,mcqYearSectionArrayList.get(actualPosition).getMCQImage(),getResources().getString(R.string.mcq));
           activitySubscriptionListDialog.setCanceledOnTouchOutside(true);
           activitySubscriptionListDialog.show();*/
       }
    }

    public void findCategoryListInExam(String rootId){


        int actualPosition=0;
        if (indexPosition==2){
            for (int i=0;i<mcqExamSectionArrayList.size();i++){
                if (rootId.equals(mcqExamSectionArrayList.get(i).getMCQChapterId()))
                    actualPosition=i;
            }

            CustomDialogActivity.mcqExamSectionArrayList=mcqExamSectionArrayList;
            CustomDialogActivity.strSubjectId=strSubjectId;
            CustomDialogActivity.strSubjectShortName=strSubjectShortName;
            CustomDialogActivity.MainParentPosition=actualPosition;
            CustomDialogActivity.ParentPosition=actualPosition/2;
            CustomDialogActivity.ParentLongName=mcqExamSectionArrayList.get(actualPosition).getMCQLongrName();
            CustomDialogActivity.ImageUrl=mcqExamSectionArrayList.get(actualPosition).getMCQImage();
            CustomDialogActivity.currentPage=getResources().getString(R.string.mcq2);
            CustomDialogActivity.strLoading_type=String.valueOf(indexPosition);
            startActivity(new Intent(getActivity(), CustomDialogActivity.class));
           /*newMCQExamsSectionAdapter=new NewMCQExamsSectionAdapter(getActivity(),this::onMcqChildItemClick,mcqExamSectionArrayList.get(actualPosition).getMcqExamsList().size(),mcqExamSectionArrayList,actualPosition,strSubjectId, strSubjectShortName, String.valueOf(indexPosition));
           activitySubscriptionListDialog=new ActivitySubscriptionListDialog(getActivity(),newMCQExamsSectionAdapter,mcqExamSectionArrayList.get(actualPosition).getMCQLongrName(),actualPosition/2,mcqExamSectionArrayList.get(actualPosition).getMCQImage(),getResources().getString(R.string.mcq));
           activitySubscriptionListDialog.setCanceledOnTouchOutside(true);
           activitySubscriptionListDialog.show();*/
        }else if (indexPosition==3){
            for (int i=0;i<mcqYearSectionArrayList.size();i++){
                if (rootId.equals(mcqYearSectionArrayList.get(i).getMCQChapterId()))
                    actualPosition=i;
            }

            CustomDialogActivity.mcqYearSectionArrayList=mcqYearSectionArrayList;
            CustomDialogActivity.strSubjectId=strSubjectId;
            CustomDialogActivity.strSubjectShortName=strSubjectShortName;
            CustomDialogActivity.MainParentPosition=actualPosition;
            CustomDialogActivity.ParentPosition=actualPosition/2;
            // CustomDialogActivity.length=mcqYearSectionArrayList.get(actualPosition).getMcqExamsList().size();
            CustomDialogActivity.ParentLongName=mcqYearSectionArrayList.get(actualPosition).getMCQLongrName();
            CustomDialogActivity.ImageUrl=mcqYearSectionArrayList.get(actualPosition).getMCQImage();
            CustomDialogActivity.currentPage=getResources().getString(R.string.mcq3);
            CustomDialogActivity.strLoading_type=String.valueOf(indexPosition);
            startActivity(new Intent(getActivity(), CustomDialogActivity.class));
          /* newMCQExamsSectionAdapter=new NewMCQExamsSectionAdapter(getActivity(),this::onMcqChildItemClick,mcqYearSectionArrayList.get(actualPosition).getMcqExamsList().size(),mcqYearSectionArrayList,actualPosition,strSubjectId, strSubjectShortName, String.valueOf(indexPosition));
           activitySubscriptionListDialog=new ActivitySubscriptionListDialog(getActivity(),newMCQExamsSectionAdapter,mcqYearSectionArrayList.get(actualPosition).getMCQLongrName(),actualPosition/2,mcqYearSectionArrayList.get(actualPosition).getMCQImage(),getResources().getString(R.string.mcq));
           activitySubscriptionListDialog.setCanceledOnTouchOutside(true);
           activitySubscriptionListDialog.show();*/
        }

    }

    @Override
    public void onMcqChildItemClick(String message) {
        if (message.equalsIgnoreCase(getResources().getString(R.string.closedialog))) {
            if (activitySubscriptionListDialog != null)
                activitySubscriptionListDialog.dismiss();
        }

    }

    private class AsyncUpdateSessionRunner extends AsyncTask<String, String, String> {
        String result = "";
        String callingType = "";

        public AsyncUpdateSessionRunner(String s) {
            callingType = s;
        }

        @Override
        protected String doInBackground(String... strings) {
            ServiceRequest mRequest = new ServiceRequest(getActivity());
            HashMap<String, String> params = new HashMap<>();
            params.put("session_id", TabMainActivity.strSessionId);

            params.put("time", String.valueOf(TabMainActivity.startingLevelTime - TabMainActivity.packageTime));
            params.put("close", callingType);

            Log.e("startingLevelTime", "" + TabMainActivity.startingLevelTime + " , packageTime : " + TabMainActivity.packageTime);
            TabMainActivity.startingLevelTime = TabMainActivity.packageTime;
            mRequest.makeServiceRequest(IConstant.updateSession, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("updateSession" + "-------- Response----------------" + response);
                    String sStatus = "";
                    String message = "";
                    try {
                        JSONObject obj = new JSONObject(response);
                        sStatus = obj.getString("status");
                        message = obj.has("message") ? obj.getString("message") : "";
                        result = sStatus;

                        if (sStatus.equalsIgnoreCase("1")) {
                            //JSONObject object = obj.getJSONObject("response");


                        } else if (sStatus.equals("00")) {
                            //customAlert.singleLoginAlertLogout();
                        } else if (sStatus.equalsIgnoreCase("01")) {
                            //customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                        } else {
                            //customAlert.showAlertOk(getString(R.string.alert_oops),message);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onErrorListener(String errorMessage) {
                    // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
                }
            });
            return result;


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            // new AsyncSubjectsRunner(s).execute();
        }
    }

    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
    }

    private void onScrollRecyclerview() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            mRecyclerView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                    if (item_is_selected) {


                    }

                }
            });

        } else {

            mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    if (item_is_selected) {

                       /* item_is_selected=false;

                        ChildViewGone();*/
                    }
                }
            });
        }
    }

    private void setAdapter(int loadMore) {
        if (loadMore == 0) {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                    RecyclerView.VERTICAL, false));


            mRecyclerView.setNestedScrollingEnabled(false);


            adapter = new TabMCQMainAdapter(getActivity(), mListChapter, this);
            mRecyclerView.setAdapter(adapter);
            /*set Child View*/
            ChildAdapterSet(0);
            tabArtcileChapterClickListener.onChapterClicked(mListChapter.get(0).long_name, mListChapter.get(0)._id, "", "");
        } else {
            adapter.notifyDataSetChanged();
        }

        item_is_selected = true;


   /* int viewHeight = articleList.size() * adapter.getItemCount();
    mRecyclerView.getLayoutParams().height = viewHeight;*/


    }

    private void setExamAdapterTab(int loadMore) {
        if (loadMore == 0) {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                    RecyclerView.VERTICAL, false));

            mRecyclerView.setNestedScrollingEnabled(false);

            tabMCQExamYearMainAdapter = new TabMCQExamYearMainAdapter(getActivity(), mExamList, this);
            mRecyclerView.setAdapter(tabMCQExamYearMainAdapter);
            /*set Child View*/
            ChildAdapterSetExam(0);
            tabArtcileChapterClickListener.onChapterClicked(mExamList.get(0).long_name, mExamList.get(0)._id, "", "");
        } else {
            tabMCQExamYearMainAdapter.notifyDataSetChanged();
        }
        item_is_selected = true;

   /* int viewHeight = articleList.size() * adapter.getItemCount();
    mRecyclerView.getLayoutParams().height = viewHeight;*/


    }

    private void setYearAdapterTab(int loadMore) {

        if (loadMore == 0) {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                    RecyclerView.VERTICAL, false));
            mRecyclerView.setNestedScrollingEnabled(false);
            tabMCQExamYearMainAdapter = new TabMCQExamYearMainAdapter(getActivity(), mYearList, this);
            mRecyclerView.setAdapter(tabMCQExamYearMainAdapter);
            /*set Child View*/
            ChildAdapterSetYear(0);
            tabArtcileChapterClickListener.onChapterClicked(mYearList.get(0).long_name, mYearList.get(0)._id, "", "");
        } else {
            tabMCQExamYearMainAdapter.notifyDataSetChanged();
        }

        item_is_selected = true;


   /* int viewHeight = articleList.size() * adapter.getItemCount();
    mRecyclerView.getLayoutParams().height = viewHeight;*/


    }

    private void ChildAdapterSet(int SelectedPosition) {

        // fragment_child_recyclerview.setHasFixedSize(true);

        fragment_child_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(),
                RecyclerView.VERTICAL, false));

 /*       LinearLayout.LayoutParams lp =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 1000);
        fragment_child_recyclerview.setLayoutParams(lp);*/


        fragment_child_recyclerview.setNestedScrollingEnabled(false);

        tabMCQChildRecyclerAdapter = new TabMCQChildRecyclerAdapter(getActivity(), mListChapter, this, SelectedPosition, strSubjectId, strSubjectName);
        fragment_child_recyclerview.setAdapter(tabMCQChildRecyclerAdapter);

    /*int viewHeight = articleList.size() * child_adapter.getItemCount();
    fragment_child_recyclerview.getLayoutParams().height = 4000;*/
    }

    private void ChildAdapterSetExam(int SelectedPosition) {

        // fragment_child_recyclerview.setHasFixedSize(true);

        fragment_child_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(),
                RecyclerView.VERTICAL, false));

 /*       LinearLayout.LayoutParams lp =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 1000);
        fragment_child_recyclerview.setLayoutParams(lp);*/


        fragment_child_recyclerview.setNestedScrollingEnabled(false);

        tAbMCQExamYearAdapter = new TAbMCQExamYearAdapter(getActivity(), mExamList, this, SelectedPosition, strSubjectId, strSubjectName);
        fragment_child_recyclerview.setAdapter(tAbMCQExamYearAdapter);

    /*int viewHeight = articleList.size() * child_adapter.getItemCount();
    fragment_child_recyclerview.getLayoutParams().height = 4000;*/
    }

    private void ChildAdapterSetYear(int SelectedPosition) {

        // fragment_child_recyclerview.setHasFixedSize(true);

        fragment_child_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(),
                RecyclerView.VERTICAL, false));

 /*       LinearLayout.LayoutParams lp =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 1000);
        fragment_child_recyclerview.setLayoutParams(lp);*/


        fragment_child_recyclerview.setNestedScrollingEnabled(false);

        tAbMCQExamYearAdapter = new TAbMCQExamYearAdapter(getActivity(), mYearList, this, SelectedPosition, strSubjectId, strSubjectName);
        fragment_child_recyclerview.setAdapter(tAbMCQExamYearAdapter);

    /*int viewHeight = articleList.size() * child_adapter.getItemCount();
    fragment_child_recyclerview.getLayoutParams().height = 4000;*/
    }
}