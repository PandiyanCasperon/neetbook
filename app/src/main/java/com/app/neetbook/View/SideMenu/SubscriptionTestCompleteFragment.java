package com.app.neetbook.View.SideMenu;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.neetbook.Adapter.sideMenuContent.SubscriptionTestCompleteAdapter;
import com.app.neetbook.Model.sidemenuFromContent.SubscriptionTestComplete;
import com.app.neetbook.R;
import com.app.neetbook.Utils.widgets.CustomTextViewBold;

import java.util.ArrayList;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubscriptionTestCompleteFragment extends Fragment {

  private ArrayList<SubscriptionTestComplete> mSubscriptionDetailList = new ArrayList<>();
  private SubscriptionTestCompleteAdapter categoryAdapter;
  private RecyclerView testList;
  private CustomTextViewBold subscriptionTestTitle;


  public SubscriptionTestCompleteFragment() {
    // Required empty public constructor
  }

  public static SubscriptionTestCompleteFragment newInstance(int param1) {
    SubscriptionTestCompleteFragment fragment = new SubscriptionTestCompleteFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View rootView = inflater
        .inflate(R.layout.fragment_subscription_test_complete, container, false);
    init(rootView);
    return rootView;
  }

  private void init(View rootView) {
    subscriptionTestTitle = rootView.findViewById(R.id.txt_subscription_test_header);
    testList = rootView.findViewById(R.id.rv_test_list);

    populateTestList();
  }

  private void populateTestList() {

    SubscriptionTestComplete testItem = new SubscriptionTestComplete();
    testItem.setTitle("Test 1");
    subscriptionTestTitle.setText(testItem.getTitle());

    for (int i = 1; i < 6; i++) {
      testItem.setQuestionCount(1 + ".");
      testItem.setQuestionTitle("Best method of diagnosing acute appendicitis is");
      testItem.setQuestionOptionFirst("Barium meal");
      testItem.setQuestionOptionSecond("History");
      testItem.setQuestionOptionThird("X-ray abdomen");
      testItem.setQuestionOptionFourth("Physical examination");
      mSubscriptionDetailList.add(testItem);
    }

    setupRecyclerView();
  }

  private void setupRecyclerView() {
    testList.setLayoutManager(new LinearLayoutManager(getActivity(),
        RecyclerView.VERTICAL, false));
    categoryAdapter = new SubscriptionTestCompleteAdapter(getActivity(), mSubscriptionDetailList);
    testList.setAdapter(categoryAdapter);

  }

}
