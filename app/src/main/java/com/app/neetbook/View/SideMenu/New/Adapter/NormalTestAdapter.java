package com.app.neetbook.View.SideMenu.New.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.R;
import com.app.neetbook.Utils.SmoothScroll.LinearLayoutManagerWithSmoothScroller;
import com.app.neetbook.View.SideMenu.New.TestFragmentpojo;
import com.app.neetbook.View.SideMenu.New.TestlistPojo;

import java.util.ArrayList;

public class NormalTestAdapter extends RecyclerView.Adapter<NormalTestAdapter.ViewHolder>{

    private ArrayList<TestFragmentpojo> test_array_list;
    private static final int LAYOUT_HEADER = 0;
    private static final int LAYOUT_CHILD = 1;
    private int currentSelectedPosition = -1;
    private int currentChileHeadSelectedPosition = -1;
    private int superChileSeletedPosition = -1;
    Context context;
    private String subjectId = "";
    private String subjectName = "";
    private String currentSelectedHeader, indexCount = "1";
    private int chileSeletedPosition = -1;
    private int index = 0;
    Typeface tfBold, tfMedium, tfRegular;
    private ItemClickListener itemClickListener;
    private String strPending, strPendingTestId;
    Dialog alertDialog;
    private ChildTestAdapter child_adapter;

    public NormalTestAdapter(Context context, ArrayList<TestFragmentpojo> test_array_list, String subjectId, String subjectName, int currentSelectedPosition, int chileSeletedPosition, String strPending, String strPendingTestId) {

        // inflater = LayoutInflater.from(context);
        this.context = context;
        this.test_array_list = test_array_list;
        this.subjectId = subjectId;
        this.subjectName = subjectName;
        this.strPending = strPending;
        this.strPendingTestId = strPendingTestId;
        this.currentSelectedPosition = currentSelectedPosition;
        //this.chileSeletedPosition = chileSeletedPosition;
        //currentSelectedHeader = test_array_list.get(0).getLongName();
        tfBold = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular = Typeface.createFromAsset(context.getAssets(), "fonts/ProximaNovaRegular.otf");
        alertDialog = new Dialog(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.new_test_fragment_layout, parent, false);
        return new ViewHolder(listItem);
    }

    public void setOnItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

            if(test_array_list.get(position).getIs_full_test()){

                holder.txtFt.setVisibility(View.VISIBLE);
                holder.textView17.setVisibility(View.GONE);
                holder.ivExpand.setVisibility(View.GONE);
                holder.book_image.setVisibility(View.GONE);

                String name=test_array_list.get(position).getFull_test_long_name();

                holder.title.setText(name);
                holder.txtFt.setText("FT" + (position + 1) + " ");

        } else {

                holder.txtFt.setVisibility(View.GONE);
                holder.textView17.setVisibility(View.VISIBLE);
                holder.ivExpand.setVisibility(View.VISIBLE);
                holder.book_image.setVisibility(View.VISIBLE);

                holder.title.setText(test_array_list.get(position).getChapter_long_name());

                AdapterSet(holder.child_recyclerview,test_array_list.get(position).getTestlist_array());

                if(test_array_list.get(position).isIs_expanded()){

                    expand(holder.child_recyclerview);

                } else{

                    collapse(holder.child_recyclerview);
                }
        }

          holder.parent_view.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {

                  if(test_array_list.get(position).getIs_full_test()){


                  } else{


                      itemClickListener.onParentItemClick(position);
                  }
          }
          });
    }

    private void AdapterSet(RecyclerView child_recyclerview, ArrayList<TestlistPojo> testlist_array){

        child_recyclerview.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(context));
        child_adapter = new ChildTestAdapter(context,testlist_array);
        child_recyclerview.setAdapter(child_adapter);
    }

    @Override
    public int getItemCount() {
        return test_array_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView17;
        ImageView ivExpand;
        private TextView title, txtFt;
        ImageView book_image;
        LinearLayout parent_view;
        RecyclerView child_recyclerview;

        public ViewHolder(View itemView) {
            super(itemView);
            ivExpand = itemView.findViewById(R.id.ivExpand);
            parent_view = itemView.findViewById(R.id.parent_view);
            title = itemView.findViewById(R.id.tvCategoryTitle);

            textView17 = itemView.findViewById(R.id.textView17);
            book_image = itemView.findViewById(R.id.book_image);
            txtFt = itemView.findViewById(R.id.txtFt);
            child_recyclerview=itemView.findViewById(R.id.child_recyclerview);

        }
    }


    public void expand(final View v) {


        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? WindowManager.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Expansion speed of 1dp/ms
        //  a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        int s= (int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density);
        System.out.println("value"+s);
        a.setDuration(500);
        v.startAnimation(a);
    }


    public void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Collapse speed of 1dp/ms
      /*  a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);*/

        int s= (int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density);
        System.out.println("value"+s);
        a.setDuration(100);
        v.startAnimation(a);

    }
}

