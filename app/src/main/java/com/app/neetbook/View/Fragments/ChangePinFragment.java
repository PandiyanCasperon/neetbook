package com.app.neetbook.View.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.constraintlayout.widget.Guideline;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.app.neetbook.Interfaces.OnFragmentInteractionListener;
import com.app.neetbook.Interfaces.OnGroupHomeMenuClickListener;
import com.app.neetbook.Interfaces.OnPinChanged;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Data.ChangePinData;
import com.app.neetbook.Utils.Data.MyProfileData;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.Utils.alerter.AlertBox;
import com.app.neetbook.Utils.widgets.DisplayOrientation;
import com.app.neetbook.Utils.widgets.Pinview;
import com.app.neetbook.View.SignInSighUp;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class ChangePinFragment extends Fragment implements View.OnClickListener {

    ConstraintLayout mConstraintLayout;
    Pinview oldPin,newPin;
    LinearLayout llTop;
    Guideline guide1,guide3,guide4;
    FrameLayout card_view;
    TextView txtOldPinHint,txtNewPinHint;
    RelativeLayout changePinSubmit;
    SessionManager sessionManager;
    CustomAlert customAlert;
    OnPinChanged onPinChanged;
    ConnectionDetector cd;

    public ChangePinFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getActivity());
        customAlert = new CustomAlert(getActivity());
        cd = new ConnectionDetector(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_change_pin, container, false);
        mConstraintLayout = rootView.findViewById(R.id.changePinFrame);
        llTop = rootView.findViewById(R.id.llTop);
        guide1 = rootView.findViewById(R.id.guide1);
        guide3 = rootView.findViewById(R.id.guide3);
        guide4 = rootView.findViewById(R.id.guide4);
        oldPin = rootView.findViewById(R.id.pinViewOld);
        newPin = rootView.findViewById(R.id.pinViewNew);
        card_view = rootView.findViewById(R.id.card_view);
        txtNewPinHint = rootView.findViewById(R.id.txtNewPinHint);
        txtOldPinHint = rootView.findViewById(R.id.txtOldPinHint);
        changePinSubmit = rootView.findViewById(R.id.changePinSubmit);
        changePinSubmit.setOnClickListener(this);
        getUserProfileData();

        setListener();
        if(getResources().getBoolean(R.bool.isTablet))
            setViewForTab();
        return rootView;
    }

    private void setListener() {
        newPin.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                setSubmitEnableDisable();
                StatusBarColorChange.hideSoftKeyboard(getActivity());

            }

            @Override
            public void onDataEntering(Pinview pinview, boolean fromUser) {
                setSubmitEnableDisable();
                if(pinview.getValue() != null && !pinview.getValue().equals("") && pinview.getValue().length()>0) {
                    ChangePinData.newPin = pinview.getValue();
                    txtNewPinHint.setVisibility(View.GONE);
                }
                else
                    txtNewPinHint.setVisibility(View.VISIBLE);
                changePinSubmit.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
            }
        });
        oldPin.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                setSubmitEnableDisable();
                StatusBarColorChange.hideSoftKeyboard(getActivity());
            }

            @Override
            public void onDataEntering(Pinview pinview, boolean fromUser) {
                setSubmitEnableDisable();
                if(pinview.getValue() != null && !pinview.getValue().equals("") && pinview.getValue().length()>0) {
                    txtOldPinHint.setVisibility(View.GONE);
                    ChangePinData.oldPin = pinview.getValue();
                }
                else
                    txtOldPinHint.setVisibility(View.VISIBLE);

            }
        });
    }

    private void setViewForTab() {
        if(getResources().getBoolean(R.bool.isTablet)) {
            double wisth = StatusBarColorChange.getViewWidth(card_view);
            if(DisplayOrientation.getDisplayOrientation(getActivity()) == 0 || DisplayOrientation.getDisplayOrientation(getActivity()) == 2) {
                oldPin.setPinWidth((int) Math.round(wisth / 30.0));
                newPin.setPinWidth((int) Math.round(wisth / 30.0));
                oldPin.setSplitWidth((int) Math.round(wisth / 20.0));
                newPin.setSplitWidth((int) Math.round(wisth / 20.0));
                txtNewPinHint.setPadding((int) Math.round(wisth / 35.0),0,0,0);
                txtOldPinHint.setPadding((int) Math.round(wisth / 35.0),0,0,0);
            }else
            {
                oldPin.setPinWidth((int) Math.round(wisth / 28.0));
                newPin.setPinWidth((int) Math.round(wisth / 28.0));
                oldPin.setSplitWidth((int) Math.round(wisth / 24.0));
                newPin.setSplitWidth((int) Math.round(wisth / 24.0));
                txtNewPinHint.setPadding((int) Math.round(wisth / 40.0),0,0,0);
                txtOldPinHint.setPadding((int) Math.round(wisth / 40.0),0,0,0);
                guide3.setGuidelinePercent((float) .20);
                guide4.setGuidelinePercent((float) .80);
            }


            if(ChangePinData.oldPin != null && !ChangePinData.oldPin.equals(""))
            {
                oldPin.setValue(ChangePinData.oldPin);
                txtOldPinHint.setVisibility(View.GONE);
            }
            if(ChangePinData.newPin!= null && !ChangePinData.newPin.equals(""))
            {
                newPin.setValue(ChangePinData.newPin);
                txtNewPinHint.setVisibility(View.GONE);
            }
        }


    }



    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.changePinSubmit:

                if(oldPin.getValue()!=  null && !oldPin.getValue().equals(""))
                {
                    if(oldPin.getValue().length() == 4) {
                        if (newPin.getValue() == null || newPin.getValue().equals("") || newPin.getValue().length() == 0) {
                            if (!oldPin.getValue().equals(sessionManager.getPin())) {
                                AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.incorrectExistingPin));
                            }else if (newPin.getValue() != null && !newPin.getValue().equals("")) {
                                if (newPin.getValue().length() == 4) {
                                    if(!oldPin.getValue().equals(newPin.getValue())) {
                                        sessionManager.updatePin(newPin.getValue());
                                        showAlertSuccess(getString(R.string.alert_success), getString(R.string.alert_change_pin_success));
                                    }else
                                    {
                                        AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.samePinError));
                                    }
                                } else {
                                    AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.ALERT_ENTER_Valid_PIN));
                                }
                            } else {
                                AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.alert_enter_ur_new_pin));
                            }
                        }else {
                            if (newPin.getValue() != null && !newPin.getValue().equals("")) {
                                if (!oldPin.getValue().equals(sessionManager.getPin())) {
                                    AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.incorrectExistingPin));
                                }else if (newPin.getValue().length() == 4) {
                                    if(!oldPin.getValue().equals(newPin.getValue())) {
                                        sessionManager.updatePin(newPin.getValue());
                                        showAlertSuccess(getString(R.string.alert_success), getString(R.string.alert_change_pin_success));
                                    }else
                                    {
                                        AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.samePinError));
                                    }
                                } else {
                                    AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.ALERT_ENTER_Valid_PIN));
                                }
                            } else {
                                AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.alert_enter_ur_new_pin));
                            }
                        }
                    }else
                    {
                        AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.ALERT_ENTER_Valid_PIN));
                    }
                }
                else {
                    AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),getString(R.string.alert_enter_ur_old_pin));
                }

                break;
        }
    }
    private void showAlertSuccess(String title, String desc)
    {
        final Dialog alertDialog = new Dialog(getActivity());
        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        ImageView imgAlert = alertDialog.findViewById(R.id.imgAlert);
        Picasso.with(getActivity()).load(R.drawable.right_mark).into(imgAlert);
        textViewTitle.setText(title);
        textViewDesc.setText(desc);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                onPinChanged.onPinChanged();
            }
        });
        alertDialog.show();
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof OnPinChanged){
            onPinChanged = (OnPinChanged) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onPinChanged = null;
    }

    private void setSubmitEnableDisable()
    {
        if(oldPin.getValue() != null && !oldPin.getValue().equals("") && oldPin.getValue().length()==4 ) {
            if (newPin.getValue() != null && !newPin.getValue().equals("") && newPin.getValue().length() == 4)
                changePinSubmit.setBackground(getResources().getDrawable(R.drawable.login_gradient));
        }
        else {
            changePinSubmit.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
        }
    }


    private void getUserProfileData() {

        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String,String> params = new HashMap<>();
        params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));

        mRequest.makeServiceRequest(IConstant.getUserProfileData, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------getUserProfileData Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1")) {

                    }  else if(sStatus.equals("00")){
                        customAlert.singleLoginAlertLogout();
                    } else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    }else {
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {

                try {
                    AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), errorMessage);
                }catch ( Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

    }
}
