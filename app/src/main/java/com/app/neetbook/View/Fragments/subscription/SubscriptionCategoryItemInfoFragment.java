package com.app.neetbook.View.Fragments.subscription;


import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.neetbook.R;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.DialogFragment;

public class SubscriptionCategoryItemInfoFragment extends DialogFragment {


  public static String TAG = "FullScreenDialog";
  private ImageView buttonClose;
  private Dialog dialog;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View view = inflater.inflate(R.layout.fragment_subscription_category_item_info,
        container, false);

    buttonClose = view.findViewById(R.id.btn_close);
    buttonClose.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.dismiss();
      }
    });
    return view;
  }

  @Override
  public void onStart() {
    super.onStart();
    dialog = getDialog();
    if (dialog != null) {
      int width = ViewGroup.LayoutParams.MATCH_PARENT;
      int height = ViewGroup.LayoutParams.MATCH_PARENT;
      dialog.getWindow().setLayout(width, height);
    }
  }

}
