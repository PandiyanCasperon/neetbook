package com.app.neetbook.View.SideMenu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import eu.long1.spacetablayout.SpaceTabLayout;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.app.neetbook.R;
import com.app.neetbook.View.Fragments.subscription.Tab1Fragment;

import java.util.ArrayList;
import java.util.List;

public class TabNewMainActivity extends AppCompatActivity implements View.OnClickListener {

    private ArticleFragment articleFragment;
    private PointsFragment pointsFragment;
    private McqFragment mcqFragment;
    private TestFragment testFragment;

    private AppCompatImageView imageView12;

    private Context mContext;
    private CoordinatorLayout coordinatorLayout;
    private SpaceTabLayout tabLayout;
    private ViewPager viewPager;

    private int currentSelectedPoisition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_new_main);


        init();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }

    private void init(){


    }


    private void setMenu() {

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imageView12:

                Themmodeclick();

            break;
        }

    }

    private void Themmodeclick(){

        Toast.makeText(getApplicationContext(), "Mode Clicked", Toast.LENGTH_SHORT).show();
    }
}
