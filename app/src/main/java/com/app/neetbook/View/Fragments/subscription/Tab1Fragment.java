package com.app.neetbook.View.Fragments.subscription;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.TabPojo.EventbusPojo;
import com.app.TabPojo.TabItemPojo;
import com.app.neetbook.Interfaces.ItemClickListener;
import com.app.neetbook.R;
import com.app.neetbook.TAbAdapter.TabFragmentChildRecyclerAdapter;
import com.app.neetbook.TAbAdapter.TabItemLoadAdapter;

import com.app.neetbook.Utils.CustomDialog.FeedBackDialog;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class Tab1Fragment extends Fragment implements ItemClickListener, View.OnClickListener {

    private Context context;
    private RecyclerView parent_recyclerview;
    private ImageView imageView13;

    private RecyclerView fragment_child_recyclerview;



    private TabItemLoadAdapter adapter;
    private ArrayList<TabItemPojo> item_list=new ArrayList<>();

    private TabFragmentChildRecyclerAdapter child_adapter;
    private List<String> child_item_list;

    private Boolean item_is_selected=false;
    private int selected_parent_position=0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.tab1fragmentlayout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        context=getActivity();

        init(view);
    }

    private void init(View view){



        parent_recyclerview=(RecyclerView)view.findViewById(R.id.parent_recyclerview);
        imageView13=(ImageView)view.findViewById(R.id.imageView13);

        fragment_child_recyclerview=(RecyclerView)view.findViewById(R.id.fragment_child_recyclerview);



        OnlcickListener();
        onScrollRecyclerview();
        DataItemLoad();

    }

    private void OnlcickListener(){

        imageView13.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imageView13:



                break;
        }
    }



    private void DataItemLoad(){

        try{

             List s= Arrays.asList(getResources().getStringArray(R.array.tab_list));

            for(int i=0;i<s.size();i++){

                TabItemPojo pojo=new TabItemPojo();

                if(i==0){

                    pojo.setIs_selection(true);

                } else {

                    pojo.setIs_selection(false);
                }

                List child_list= Arrays.asList(getResources().getStringArray(R.array.child_list));

                pojo.setChild_list(child_list);

                pojo.setQuestion(s.get(i).toString());

                item_list.add(pojo);
            }

            SetAdapter();

        }catch(Exception e){

            System.out.println("Exception--"+e.toString());
        }
    }

    private void SetAdapter(){

       // parent_recyclerview.setHasFixedSize(true);

        parent_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(),
                RecyclerView.VERTICAL, false));

   /*     LinearLayout.LayoutParams lp =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 1000);
        parent_recyclerview.setLayoutParams(lp);*/


        parent_recyclerview.setNestedScrollingEnabled(false);


        /*adapter=new TabItemLoadAdapter(getActivity(),item_list,this);*/
        parent_recyclerview.setAdapter(adapter);

        item_is_selected=true;


        int viewHeight = item_list.size() * adapter.getItemCount();
        parent_recyclerview.getLayoutParams().height = 4000;

        ChildAdapterSet();
    }

    private void ChildAdapterSet(){

       // fragment_child_recyclerview.setHasFixedSize(true);

        fragment_child_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(),
                RecyclerView.VERTICAL, false));

 /*       LinearLayout.LayoutParams lp =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 1000);
        fragment_child_recyclerview.setLayoutParams(lp);*/


        fragment_child_recyclerview.setNestedScrollingEnabled(false);

        /*child_adapter=new TabFragmentChildRecyclerAdapter(getActivity(),item_list,this,selected_parent_position);*/
        fragment_child_recyclerview.setAdapter(child_adapter);

        int viewHeight = item_list.size() * child_adapter.getItemCount();
        fragment_child_recyclerview.getLayoutParams().height = 4000;
    }

    @Override
    public void onParentItemClick(int position) {

        try{

            item_is_selected=true;



            for(int i=0;i<item_list.size();i++){

                item_list.get(i).setIs_selection(false);
            }

            item_list.get(position).setIs_selection(true);

            adapter.notifyDataSetChanged();

            EventbusPojo pojo=new EventbusPojo();
            pojo.setFragment_name("Test_id");
            EventBus.getDefault().post(pojo);


            selected_parent_position=position;
            ChildAdapterSet();

        }catch(Exception e){

            System.out.println("Exception--"+e.toString());
        }


    }

    private void onScrollRecyclerview(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            parent_recyclerview.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                    if(item_is_selected){

                       /* item_is_selected=false;

                        ChildViewGone();*/
                    }

                }
            });

        } else{

            parent_recyclerview.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    if(item_is_selected){

                       /* item_is_selected=false;

                        ChildViewGone();*/
                    }
                }
            });
        }
    }

    private void ChildViewGone(){

        try{

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    for(int i=0;i<item_list.size();i++){

                        item_list.get(i).setIs_selection(false);
                    }

                    adapter.notifyDataSetChanged();

                }
            });

        }catch(Exception e){

            System.out.println("Exception--"+e.toString());
        }

    }




}

