package com.app.neetbook.View.SideMenu;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.app.neetbook.R;
import com.app.neetbook.Utils.ActionBarActivityNeetBook;
import com.app.neetbook.Utils.OnSwipeTouchListener;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.Utils.Thread.AppExecutors;

public class ArticleDetailActivity extends ActionBarActivityNeetBook {
    String strTitle = "", strSummary = "";
    TextView txtSummary, txtPageTitle,chapterShortName;
    RelativeLayout rlBack;
    ConstraintLayout llParent;
    Typeface tfBold, tfMedium, tfRegular;
    private SessionManager sessionManager;
    private RelativeLayout rlParent;
    private WebView webView;
    private AppExecutors appexector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_detail);
        StatusBarColorChange.updateStatusBarColor(ArticleDetailActivity.this, getResources().getColor(R.color.article_content_page_pink));
        init();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }

    private void init() {
        appexector = new AppExecutors();
        sessionManager = new SessionManager(ArticleDetailActivity.this);
        rlBack = findViewById(R.id.rlBack);
        txtSummary = findViewById(R.id.txtSummary);
        llParent = findViewById(R.id.llParent);
        txtPageTitle = findViewById(R.id.txtPageTitle);
        chapterShortName= findViewById(R.id.chapterShortName);
        rlParent = findViewById(R.id.rlParent);
        webView = findViewById(R.id.webView);
        //webView.getSettings().setTextZoom(200);
        strTitle = getIntent().getStringExtra("title");
        strSummary = getIntent().getStringExtra("summary");
        chapterShortName.setText(getIntent().getStringExtra("chapter_short_name"));
//    Objects.requireNonNull(getSupportActionBar()).setTitle(strTitle);
        if (strSummary != null)
            txtSummary.setText(Html.fromHtml(strSummary));


        if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
            webView.getSettings().setTextZoom(100);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
            webView.getSettings().setTextZoom(120);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
            webView.getSettings().setTextZoom(130);
        }

        StartLoad();
        txtPageTitle.setText(strTitle);


        setFont();
        rlBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overridePendingTransition(R.anim.alerter_slide_in_from_left, R.anim.alerter_slide_out_to_right);
                onBackPressed();
                //finish();
            }
        });

        rlParent.setOnTouchListener(new OnSwipeTouchListener(ArticleDetailActivity.this) {
            public void onSwipeTop() {
                //Toast.makeText(SubscriptionArticleActivity.this, "top", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeRight() {
                //Toast.makeText(SubscriptionArticleActivity.this, "right", Toast.LENGTH_SHORT).show();
                overridePendingTransition(R.anim.alerter_slide_in_from_left, R.anim.alerter_slide_out_to_right);
                onBackPressed();
            }

            public void onSwipeLeft() {

            }

            public void onSwipeBottom() {
                //Toast.makeText(SubscriptionArticleActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });
        txtSummary.setOnTouchListener(new OnSwipeTouchListener(ArticleDetailActivity.this) {
            public void onSwipeTop() {
                //Toast.makeText(SubscriptionArticleActivity.this, "top", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeRight() {
                //Toast.makeText(SubscriptionArticleActivity.this, "right", Toast.LENGTH_SHORT).show();
                finish();
            }

            public void onSwipeLeft() {

            }

            public void onSwipeBottom() {
                //Toast.makeText(SubscriptionArticleActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });
    }

    private void StartLoad() {

        try {

            if (sessionManager.isNightModeEnabled()) {

                String finalHtml = "<html><head>"
                        + "<style type=\"text/css\">li{color: #fff} span {color: #000}"
                        + "</style></head>"
                        + "<body>" + "<font color='white'>"
                        + strSummary + "</font>"
                        + "</body></html>";

                // webView.loadData(finalHtml,"text/html", "UTF-8");
                webView.loadDataWithBaseURL(null, finalHtml, "text/html", "UTF-8", null);
                webView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.night_color_new));

            } else {

                String finalHtml = "<html><head>"
                        + "<style type=\"text/css\">li{color: #000} span {color: #000}"
                        + "</style></head>"
                        + "<body>" + "<font color='black'>"
                        + strSummary + "</font>"
                        + "</body></html>";

                webView.loadDataWithBaseURL(null, finalHtml, "text/html", "UTF-8", null);

                // webView.loadData(finalHtml,"text/html", "UTF-8");

                webView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white_color_new));
            }

        } catch (Exception e) {

            Log.e("Exception", e.toString());
        }
    }

    private void setFont() {
        tfBold = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaRegular.otf");

  /*  if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
      txtPageTitle.setTextAppearance(ArticleDetailActivity.this,R.style.textViewSmallSummaryTitle);
      txtSummary.setTextAppearance(ArticleDetailActivity.this,R.style.textViewSmallArticleInfo);
    }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
      txtPageTitle.setTextAppearance(ArticleDetailActivity.this,R.style.textViewMediumSummaryTitle);
      txtSummary.setTextAppearance(ArticleDetailActivity.this,R.style.textViewMediumArticleInfo);
    }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
      txtPageTitle.setTextAppearance(ArticleDetailActivity.this,R.style.textViewLargeSummaryTitle);
      txtSummary.setTextAppearance(ArticleDetailActivity.this,R.style.textViewLargeArticleInfo);
    }
*/
        txtPageTitle.setTypeface(tfMedium);
        txtSummary.setTypeface(tfRegular);
        chapterShortName.setTypeface(tfMedium);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.alerter_slide_in_from_left, R.anim.alerter_slide_out_to_right);
    }
}
