package com.app.neetbook.View.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;

import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.neetbook.R;
import com.app.neetbook.View.WalkthroughActivity;

import org.json.JSONException;

/**
 * A simple {@link Fragment} subclass.
 */
public class WalkFragment2 extends Fragment {
    ImageView img_man,img_girl,imgMessageGirl,imgMessageMen,imgBg;
    View view;
    ViewGroup container;
    LayoutInflater inflater;
    TextView txtPointsDisc,txtPoints;

    public WalkFragment2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Display display = ((WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int rotation = display.getRotation();
        if(rotation == 1)
            view = inflater.inflate(R.layout.fragment_walk_fragment2, container, false);
        else if(getResources().getBoolean(R.bool.isTablet))
            view = inflater.inflate(R.layout.fragment_walk_fragment2_portrait, container, false);
        else
            view = inflater.inflate(R.layout.fragment_walk_fragment2, container, false);
        imgBg = view.findViewById(R.id.imgBg);
        img_man = view.findViewById(R.id.img_man);
        img_girl = view.findViewById(R.id.img_girl);
        imgMessageGirl = view.findViewById(R.id.imgMessageGirl);
        imgMessageMen = view.findViewById(R.id.imgMessageMen);
        txtPoints = view.findViewById(R.id.txtPoints);
        txtPointsDisc = view.findViewById(R.id.txtPointsDesc);
        setTitleAndDesc();
        return view;
    }
    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);

        if(isFragmentVisible_){
            img_man.setVisibility(View.VISIBLE);
            img_girl.setVisibility(View.GONE);
            imgMessageGirl.setVisibility(View.GONE);
            imgMessageMen.setVisibility(View.GONE);
            txtPointsDisc.setVisibility(View.GONE);
            txtPoints.setVisibility(View.GONE);
            ////////.setVisibility(View.INVISIBLE);
            Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
            //imgBg.startAnimation(RightSwipe);
            img_man.startAnimation(RightSwipe);
            img_man.setVisibility(View.VISIBLE);



            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                    img_girl.setVisibility(View.VISIBLE);
                    img_girl.startAnimation(RightSwipe);;
                }
            },200);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                    imgMessageMen.setVisibility(View.VISIBLE);
                    imgMessageMen.startAnimation(RightSwipe);
                }
            },300);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                    imgMessageGirl.setVisibility(View.VISIBLE);
                    imgMessageGirl.startAnimation(RightSwipe);
                }
            },400);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                    txtPointsDisc.setVisibility(View.VISIBLE);
                    txtPointsDisc.startAnimation(RightSwipe);
                }
            },600);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.right_to_left);
                    txtPoints.setVisibility(View.VISIBLE);
                    txtPoints.startAnimation(RightSwipe);
                }
            },500);
        }
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putBundle("newBundy", newBundy);
    }

    private void setTitleAndDesc() {

        try {
            if(WalkthroughActivity.jsonWalkThrough!= null) {
                txtPoints.setText(WalkthroughActivity.jsonWalkThrough.getString("heading2"));
                txtPointsDisc.setText(WalkthroughActivity.jsonWalkThrough.getString("content2"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
