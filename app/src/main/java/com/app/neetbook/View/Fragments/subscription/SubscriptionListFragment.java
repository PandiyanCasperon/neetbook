package com.app.neetbook.View.Fragments.subscription;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.neetbook.Adapter.MobilehomeGroupViewAdapter;
import com.app.neetbook.Adapter.subscription.SubscriptionAdapter;
import com.app.neetbook.Adapter.subscription.SubscriptionAdapter.OnItemClickListener;
import com.app.neetbook.Adapter.subscription.SubscriptionBottomSheetAdapter;
import com.app.neetbook.CartRoom.CartDataBase;
import com.app.neetbook.Interfaces.OnDeleteItemFromCart;
import com.app.neetbook.Interfaces.Updateable;
import com.app.neetbook.Interfaces.onSubscriptionChecked;
import com.app.neetbook.Model.CartList;
import com.app.neetbook.Model.GroupChildBean;
import com.app.neetbook.Model.TabSideMenu;
import com.app.neetbook.Model.subscription.Subscription;
import com.app.neetbook.Model.subscription.SubscriptionCart;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ActionBarActivityNeetBook;
import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Data.ChangeMobileData;
import com.app.neetbook.Utils.Data.DrawerActivityData;
import com.app.neetbook.Utils.EventBusPojo.ActionEvent;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.OnSwipeTouchListener;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.ThreadExecutor.AppExecutors;
import com.app.neetbook.Utils.alerter.AlertBox;
import com.app.neetbook.Utils.widgets.CustomTextViewMedium;
import com.app.neetbook.Utils.widgets.DisplayOrientation;
import com.app.neetbook.View.DrawerContentSlideActivity;
import com.app.neetbook.View.DynamicFragmentActivity;
import com.app.neetbook.View.EnterPin;
import com.app.neetbook.View.MobilePagesActivity;
import com.app.neetbook.View.ReviewOrder;
import com.app.neetbook.View.SideMenu.SubscriptionArticleActivity;
import com.app.neetbook.View.TabletHome;
import com.app.neetbook.homepageRoom.GroupslistDatabase;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import static android.content.Context.WINDOW_SERVICE;


public class SubscriptionListFragment extends Fragment implements Updateable, View.OnClickListener {

  private ArrayList<Subscription> subscriptionList = new ArrayList<>();
  private static List<SubscriptionCart> mSubscriptionCartList = new ArrayList<>();

  private SubscriptionAdapter adapter;
  private SubscriptionBottomSheetAdapter priceAdapter;

  private RecyclerView subscriptionRecyclerView;
  private RecyclerView subscriptionBottomSheetList;
  TextView txt_subscription_cart,txt_subscription_cart1;

  private ConstraintLayout mConstraintLayout,constrainSubContainer;
  private BottomSheetBehavior bottomSheetBehavior;

  private CustomTextViewMedium subscriptionCount;
  private CustomTextViewMedium subscriptionSubjects;
  private CustomTextViewMedium subscriptionFinalAmount;
  private ImageView bottomSheetButton;
  private LinearLayout llEmpty;
  String strGroupId = "";
  FragmentActivity activity;
  private int skip = 0;
  private int page = 1;
  private int limit = 10;
  int loadMoreEnable = 0;
  private int totalSubsCount = 0;
  int l = 0;
  SessionManager sessionManager;
  private CustomAlert customAlert;
  ConnectionDetector cd;
  private AppExecutors exector;
  CartDataBase cartlistDatabase;
  Loader loader;
  ShimmerFrameLayout mShimmerViewContainer ;
  SwipeRefreshLayout mSwipeRefreshLayout;
  LinearSnapHelper snapHelper;
  private boolean isExpanded = false;
  public SubscriptionListFragment() {
    // Required empty public constructor
  }

    public SubscriptionListFragment(String groupid, FragmentActivity activity) {
    this.strGroupId = groupid;
    this.activity = activity;
    }




    public static SubscriptionListFragment newInstance(int param1) {
    SubscriptionListFragment fragment = new SubscriptionListFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {

    }
    sessionManager = new SessionManager(getActivity());
    customAlert = new CustomAlert(getActivity());
    cd = new ConnectionDetector(getActivity());
    loader = new Loader(getActivity());
    EventBus.getDefault().register(this);
    exector=new AppExecutors();
    cartlistDatabase = CartDataBase.getAppDatabase(getActivity());
    createSnap();

  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_subscription_list, container, false);
    init(rootView);
    return rootView;
  }

  private void init(View rootView) {
    subscriptionRecyclerView = rootView.findViewById(R.id.rv_subscription_list);
    constrainSubContainer = rootView.findViewById(R.id.constrainSubContainer);
    subscriptionBottomSheetList = rootView.findViewById(R.id.rv_price);

    subscriptionCount = rootView.findViewById(R.id.txt_subscription_count);
    subscriptionSubjects = rootView.findViewById(R.id.txt_subscription_subjects);
    subscriptionFinalAmount = rootView.findViewById(R.id.txt_subcription_total_price);
    bottomSheetButton = rootView.findViewById(R.id.btn_bottomsheet);
    mConstraintLayout = rootView.findViewById(R.id.bottom_sheet);
    //txt_subcription_total_price = rootView.findViewById(R.id.txt_subcription_total_price);
    txt_subscription_cart = rootView.findViewById(R.id.txt_subscription_cart);
    txt_subscription_cart1 = rootView.findViewById(R.id.txt_subscription_cart);
    mShimmerViewContainer  = rootView.findViewById(R.id.mShimmerViewContainer );
    mShimmerViewContainer.startShimmer();
    llEmpty  = rootView.findViewById(R.id.llEmpty );
    mSwipeRefreshLayout = rootView.findViewById(R.id.mSwipeRefreshLayout);
    txt_subscription_cart.setOnClickListener(this);
    txt_subscription_cart1.setOnClickListener(this);
    listtener();
    swipeRefresh();
    setupBottomSheet();
    populateSubscriptionList();

  }

  private void swipeRefresh() {
    mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
      @Override
      public void onRefresh() {
        page = 1;
        subscriptionList = null;
        subscriptionList = new ArrayList<>();
        adapter.notifyDataSetChanged();
        populateSubscriptionList();
        mSwipeRefreshLayout.setRefreshing(false);
      }
    });
  }
  private void listtener() {
    subscriptionRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        LinearLayoutManager layoutManager=LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
        int totalItemCount = layoutManager.getItemCount();
        int lastVisible = layoutManager.findLastVisibleItemPosition();

        boolean endHasBeenReached = lastVisible + 5 >= totalItemCount;
        Log.e("totalItemCount",""+totalItemCount);
        Log.e("lastVisible",""+lastVisible);

        if(lastVisible == totalItemCount-1 && totalSubsCount>subscriptionList.size() && loadMoreEnable == 0)
        {
          page = page+1;
          Log.e("loadMoreSubjects()","Called");
          loadMoreEnable = 1;
          loadMoreSubjects(totalItemCount);
        }
      }
    });

    bottomSheetButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (subscriptionBottomSheetList.getVisibility() == View.VISIBLE) {
          isExpanded = false;
          bottomSheetCollapseView();
        } else {
          isExpanded = true;
          bottomSheetExpandedView();
        }
      }
    });
  }
  private void setupBottomSheet() {
    CollectCartItems();
   // bottomSheetBehavior = BottomSheetBehavior.from(mConstraintLayout);


    bottomSheetCollapseView();

    constrainSubContainer.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
      public void onSwipeTop() {
        //bottomSheetExpandedView();
      }
      public void onSwipeRight() {

      }
      public void onSwipeLeft() {

      }
      public void onSwipeBottom() {
        //bottomSheetCollapseView();
      }

    });

    /*bottomSheetBehavior.setBottomSheetCallback(new BottomSheetCallback() {
      @Override
      public void onStateChanged(@NonNull View view, int i) {
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
          bottomSheetButton.setImageDrawable(getResources().getDrawable(R.drawable.icn_arrow_up_svg));
          subscriptionBottomSheetList.setVisibility(View.GONE);
        } else if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED && isExpanded) {
          bottomSheetButton.setImageDrawable(getResources().getDrawable(R.drawable.acn_arrow_down));
          //subscriptionBottomSheetList.setVisibility(View.VISIBLE);
        }
      }

      @Override
      public void onSlide(@NonNull View view, float v) {

      }
    });*/
  }

  private void bottomSheetExpandedView() {
    subscriptionBottomSheetList.setVisibility(View.VISIBLE);
    txt_subscription_cart.setVisibility(View.VISIBLE);
    subscriptionFinalAmount.setVisibility(View.VISIBLE);
    subscriptionCount.setVisibility(View.VISIBLE);
    subscriptionSubjects.setVisibility(View.VISIBLE);
    bottomSheetButton.setImageDrawable(getResources().getDrawable(R.drawable.acn_arrow_down));
  }

  private void bottomSheetCollapseView() {
    bottomSheetButton.setImageDrawable(getResources().getDrawable(R.drawable.icn_arrow_up_svg));
    subscriptionBottomSheetList.setVisibility(View.GONE);
    txt_subscription_cart.setVisibility(View.VISIBLE);
    subscriptionFinalAmount.setVisibility(View.VISIBLE);
    subscriptionCount.setVisibility(View.VISIBLE);
    subscriptionSubjects.setVisibility(View.VISIBLE);
  }

  private void populateSubscriptionList() {

   /* mShimmerViewContainer.setVisibility(View.VISIBLE);*/

    if(strGroupId.length()>0){

      subscriptionList = new ArrayList<>();
      ServiceRequest mRequest = new ServiceRequest(getActivity());
      HashMap<String, String> params = new HashMap<>();
      params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
      params.put("group_id",strGroupId);
      params.put("page_type","2");
      params.put("skip",""+skip);
      params.put("limit",""+limit);
      params.put("page",""+page);
      mRequest.makeServiceRequest(IConstant.subjectList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
        @Override
        public void onCompleteListener(String response) {
          System.out.println("------------subjectList Response----------------" + response);
          String sStatus = "";
          String message = "";
          try {

            JSONObject obj = new JSONObject(response);
            sStatus = obj.getString("status");
            message = obj.has("message")?obj.getString("message"):"";



            l=1;
            if (sStatus.equalsIgnoreCase("1")) {
              JSONObject object = obj.getJSONObject("response");
              totalSubsCount = object.getInt("subject_count");
              JSONArray jsonArray = object.getJSONArray("subject_list");
              if(jsonArray.length()>0)
              {
                llEmpty.setVisibility(View.GONE);
                for(int i=0;i<jsonArray.length();i++)
                {
                  JSONObject jsonObject = jsonArray.getJSONObject(i);
                  Subscription itemList = new Subscription();
                  itemList.setStrId(jsonObject.has("_id")?jsonObject.getString("_id"):"");
                  itemList.setStrTrial(jsonObject.getString("trial"));
                  itemList.setTitle(jsonObject.getString("subj_name"));
                  itemList.setStrGroupId(jsonObject.getString("group_id"));
                  itemList.setStrImage(/*getResources().getBoolean(R.bool.isTablet)?*/jsonObject.getString("tab_img")/*:jsonObject.getString("mob_img")*/);
                  itemList.setStrLongName(jsonObject.getString("long_name"));
                  itemList.setStrShortName(jsonObject.getString("short_name"));
                  itemList.setChapter(jsonObject.getString("c_count"));
                  itemList.setTopics(jsonObject.getString("h_count"));
                  itemList.setQuestions(jsonObject.getString("mcq_count"));
                  itemList.setStrIsSubcribed(jsonObject.getInt("subscribed_user") == 1 ?"Yes" : "No");
                  if(jsonObject.getJSONArray("packages").length()>0)
                  {
                    JSONArray jsonArray1 = jsonObject.getJSONArray("packages");
                    for(int j=0;j<jsonArray1.length();j++)
                    {
                      JSONObject jsonObject1 = jsonArray1.getJSONObject(j);
                      if(j == 0) {
                        itemList.setStrPackageOneId(jsonObject1.getString("_id"));
                        itemList.setMiniSubscription(jsonObject1.getString("duration_days"));
                        itemList.setMiniSubscriptionPrice(jsonObject1.getString("price"));
                        itemList.setStrMiniHours(jsonObject1.getString("duration_hours"));
                        SubscriptionCart priceList = new SubscriptionCart();
                        priceList.setTitle(jsonObject.getString("subj_name"));
                        priceList.setStrId(jsonObject1.getString("_id"));
                        SubscriptionCart subscriptionCart = cartlistDatabase.cartListDao().getCartItem(priceList.getStrId());
                        if(subscriptionCart != null && subscriptionCart.getStrId() != null && !subscriptionCart.getStrId().isEmpty() && subscriptionCart.getStrId().equalsIgnoreCase(priceList.getStrId()))
                          itemList.setMiniSubscriptionChecked(true);
                        else {
                          itemList.setMiniSubscriptionChecked(false);
                        }

                      }
                      if(j == 1) {
                        itemList.setStrPackageTwoId(jsonObject1.getString("_id"));
                        itemList.setLargeSubscription(jsonObject1.getString("duration_days"));
                        itemList.setLargeSubscriptionPrice(jsonObject1.getString("price"));
                        itemList.setStrLargeHours(jsonObject1.getString("duration_hours"));
                        SubscriptionCart priceList = new SubscriptionCart();
                        priceList.setTitle(jsonObject.getString("subj_name"));
                        priceList.setStrId(jsonObject1.getString("_id"));
                        SubscriptionCart subscriptionCart = cartlistDatabase.cartListDao().getCartItem(priceList.getStrId());
                        if(subscriptionCart != null && subscriptionCart.getStrId() != null && !subscriptionCart.getStrId().isEmpty() && subscriptionCart.getStrId().equalsIgnoreCase(priceList.getStrId()))
                          itemList.setLargeSubscriptionChecked(true);
                        else {
                          itemList.setLargeSubscriptionChecked(false);
                        }

                      }
                    }
                  }
                  if(jsonObject.getInt("trial") == 0 && jsonObject.getInt("subscribed_user") == 1)
                  {
                    JSONObject subscriptionsObj = jsonObject.getJSONObject("subscriptions");
                    itemList.setStrRemainingDays(subscriptionsObj.getString("remaining_days"));
                    itemList.setStrRemainingHours(subscriptionsObj.getString("remaining_hours"));
                  }
                  subscriptionList.add(itemList);
                  loadMoreEnable = 0;
                }
                setupRecyclerView(0);
              }else
              llEmpty.setVisibility(View.VISIBLE);
              mShimmerViewContainer.setVisibility(View.GONE);
            } else if(sStatus.equals("00")){
              customAlert.singleLoginAlertLogout();
            }else if(sStatus.equalsIgnoreCase("01")){
              customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
            } else {
              customAlert.showAlertOk(getString(R.string.alert_oops),message);
            }

          } catch (JSONException e) {

            e.printStackTrace();
          }

        }

        @Override
        public void onErrorListener(String errorMessage) {
          // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
        }
      });
    }


  }

  private void loadMoreSubjects(final int lastItemPosition) {
    ServiceRequest mRequest = new ServiceRequest(getActivity());
    HashMap<String, String> params = new HashMap<>();
    params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
    params.put("group_id",strGroupId);
    params.put("page_type","2");
    params.put("skip",""+skip);
    params.put("limit",""+limit);
    params.put("page",""+page);
    mRequest.makeServiceRequest(IConstant.subjectList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
      @Override
      public void onCompleteListener(String response) {
        System.out.println("------------subjectList Response----------------" + response);
        String sStatus = "";
        String message = "";
        try {

          JSONObject obj = new JSONObject(response);
          sStatus = obj.getString("status");
          message = obj.has("message")?obj.getString("message"):"";



          l=1;
          if (sStatus.equalsIgnoreCase("1")) {
            JSONObject object = obj.getJSONObject("response");
            JSONArray jsonArray = object.getJSONArray("subject_list");
            if(jsonArray.length()>0)
            {
              for(int i=0;i<jsonArray.length();i++)
              {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Subscription itemList = new Subscription();
                itemList.setStrId(jsonObject.has("_id")?jsonObject.getString("_id"):"");
                itemList.setStrTrial(jsonObject.getString("trial"));
                itemList.setTitle(jsonObject.getString("subj_name"));
                itemList.setStrGroupId(jsonObject.getString("group_id"));
                itemList.setStrImage(/*getResources().getBoolean(R.bool.isTablet)?*/jsonObject.getString("tab_img")/*:jsonObject.getString("mob_img")*/);
                itemList.setStrLongName(jsonObject.getString("long_name"));
                itemList.setStrShortName(jsonObject.getString("short_name"));
                itemList.setChapter(jsonObject.getString("c_count"));
                itemList.setTopics(jsonObject.getString("h_count"));
                itemList.setQuestions(jsonObject.getString("mcq_count"));
                itemList.setStrIsSubcribed(jsonObject.getInt("subscribed_user") == 1 ?"Yes" : "No");
                if(jsonObject.getJSONArray("packages").length()>0)
                {
                  JSONArray jsonArray1 = jsonObject.getJSONArray("packages");
                  for(int j=0;j<jsonArray1.length();j++)
                  {
                    JSONObject jsonObject1 = jsonArray1.getJSONObject(j);
                    if(j == 0) {
                      itemList.setStrPackageOneId(jsonObject1.getString("_id"));
                      itemList.setMiniSubscription(jsonObject1.getString("duration_days"));
                      itemList.setMiniSubscriptionPrice(jsonObject1.getString("price"));
                      itemList.setStrMiniHours(jsonObject1.getString("duration_hours"));
                      SubscriptionCart priceList = new SubscriptionCart();
                      priceList.setTitle(jsonObject.getString("subj_name"));
                      priceList.setStrId(jsonObject1.getString("_id"));
                      SubscriptionCart subscriptionCart = cartlistDatabase.cartListDao().getCartItem(priceList.getStrId());
                      if(subscriptionCart != null && subscriptionCart.getStrId() != null && !subscriptionCart.getStrId().isEmpty() && subscriptionCart.getStrId().equalsIgnoreCase(priceList.getStrId()))
                        itemList.setMiniSubscriptionChecked(true);
                      else {
                        itemList.setMiniSubscriptionChecked(false);
                      }

                    }
                    if(j == 1) {
                      itemList.setStrPackageTwoId(jsonObject1.getString("_id"));
                      itemList.setLargeSubscription(jsonObject1.getString("duration_days"));
                      itemList.setLargeSubscriptionPrice(jsonObject1.getString("price"));
                      itemList.setStrLargeHours(jsonObject1.getString("duration_hours"));
                      SubscriptionCart priceList = new SubscriptionCart();
                      priceList.setTitle(jsonObject.getString("subj_name"));
                      priceList.setStrId(jsonObject1.getString("_id"));
                      SubscriptionCart subscriptionCart = cartlistDatabase.cartListDao().getCartItem(priceList.getStrId());
                      if(subscriptionCart != null && subscriptionCart.getStrId() != null && !subscriptionCart.getStrId().isEmpty() && subscriptionCart.getStrId().equalsIgnoreCase(priceList.getStrId()))
                        itemList.setLargeSubscriptionChecked(true);
                      else {
                        itemList.setLargeSubscriptionChecked(false);
                      }

                    }
                  }
                }
                if(jsonObject.getInt("trial") == 0 && jsonObject.getInt("subscribed_user") == 1)
                {
                  JSONObject subscriptionsObj = jsonObject.getJSONObject("subscriptions");
                  itemList.setStrRemainingDays(subscriptionsObj.getString("remaining_days"));
                  itemList.setStrRemainingHours(subscriptionsObj.getString("remaining_hours"));
                }
                subscriptionList.add(itemList);
              }
              setupRecyclerView(lastItemPosition);
            }
          } else if(sStatus.equals("00")){
            customAlert.singleLoginAlertLogout();
          }else if(sStatus.equalsIgnoreCase("01")){
            customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
          } else {
            customAlert.showAlertOk(getString(R.string.alert_oops),message);
          }

        } catch (JSONException e) {

          e.printStackTrace();
        }

      }

      @Override
      public void onErrorListener(String errorMessage) {
        // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
      }
    });
  }


  private void populateBottomSheet() {
    //new AsyncCartRunner().execute();


    if(mSubscriptionCartList.size()>0) {
      double total = 0;
      for(int i=0;i<mSubscriptionCartList.size();i++)
      {
        total = Math.round(total + Math.round(Double.parseDouble(mSubscriptionCartList.get(i).getPrice())));
      }

      subscriptionFinalAmount.setVisibility(View.VISIBLE);
      subscriptionCount.setVisibility(View.VISIBLE);
      subscriptionSubjects.setVisibility(View.VISIBLE);
      subscriptionFinalAmount.setText(getString(R.string.currency_symbol)+" "+ Math.round(total));
      subscriptionCount.setText(""+mSubscriptionCartList.size());
      subscriptionSubjects.setText(getString(R.string.subjects));

    }else
    {
      subscriptionFinalAmount.setVisibility(View.VISIBLE);
      subscriptionCount.setVisibility(View.VISIBLE);
      subscriptionSubjects.setVisibility(View.VISIBLE);
      subscriptionFinalAmount.setText(getString(R.string.currency_symbol)+" 0");
      subscriptionCount.setText("0");
      subscriptionSubjects.setText(getString(R.string.subjects));
    }
    setupPriceRecyclerView();
  }

  private void setupRecyclerView(int lastItemPosition) {
    if(lastItemPosition == 0) {
      GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), (getActivity().getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(getActivity()) == 0 || DisplayOrientation.getDisplayOrientation(getActivity()) == 2))?3 : getActivity().getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(getActivity()) == 1 || DisplayOrientation.getDisplayOrientation(getActivity()) == 3) ?4 : 2);
      subscriptionRecyclerView.setLayoutManager(layoutManager);
      //snapHelper.attachToRecyclerView(subscriptionRecyclerView);
      adapter = new SubscriptionAdapter(getActivity(), subscriptionList);
      subscriptionRecyclerView.setAdapter(adapter);
      performclickAction();
    }else
    {
      adapter.notifyDataSetChanged();
    }

  }

  private void performclickAction() {
    adapter.setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(View itemView, int position) {
        //startActivity(new Intent(getActivity(), SubscriptionDetailFragment.class));
//        addFragment();
      }
    });
    adapter.setOnPackageSelectListioner(new onSubscriptionChecked() {
      @Override
      public void onItemChecked(boolean checked, String packageId, final Subscription subscription, int selectedPackage, final int position) {

        final SubscriptionCart priceList = new SubscriptionCart();
        priceList.setTitle(subscription.getTitle());
        priceList.setStrId(packageId);
        if(selectedPackage == 1)
        {
          priceList.setSubscription(subscription.getMiniSubscription()+" Days "+subscription.getStrMiniHours()+" Hours");
          priceList.setPrice(subscription.getMiniSubscriptionPrice());
        }else
        {
          priceList.setSubscription(subscription.getLargeSubscription()+" Days "+subscription.getStrLargeHours()+" Hours");
          priceList.setPrice(subscription.getLargeSubscriptionPrice());
        }

        if(checked) {
          try {
            exector.diskIO().execute(new Runnable() {
              @Override
              public void run() {
                SubscriptionCart subscriptionCart = cartlistDatabase.cartListDao().getCartItem(priceList.getStrId());
                if(subscriptionCart != null && subscriptionCart.getStrId() != null && !subscriptionCart.getStrId().isEmpty() && subscriptionCart.getStrId().equalsIgnoreCase(priceList.getStrId()))
                  cartlistDatabase.cartListDao().update(priceList);
                else {
                  cartlistDatabase.cartListDao().insertAll(priceList);
                }
                mSubscriptionCartList.clear();
                Log.e("InsertedCartItems",cartlistDatabase.cartListDao().getAll().toString());
                mSubscriptionCartList = cartlistDatabase.cartListDao().getAll();
                ActionEvent event=new ActionEvent();
                event.setAction("successCart");
                EventBus.getDefault().post(event);

              }
            });
            //addPackageToCart(priceList,packageId,position);

          } catch (Exception e) {

            System.out.println("Exception---" + e.toString());

          }

        }
        else
        {
          if(mSubscriptionCartList.size()>0) {

            cartlistDatabase.cartListDao().deleteByCartId(priceList.getStrId());
            Log.e("DeletedCartItems", "deleted");
            if(mSubscriptionCartList.size()>0){
              for(int i=0;i<mSubscriptionCartList.size();i++)
              {
                if(priceList.getStrId().equalsIgnoreCase(mSubscriptionCartList.get(i).getStrId()))
                  mSubscriptionCartList.remove(i);
              }
            }

          }

          removePackageFromCart(priceList,packageId,position);
          populateBottomSheet();
        }
      }
    });
  }

  private int isAdded(SubscriptionCart priceList)
{
  Collections.binarySearch(mSubscriptionCartList, priceList, new Comparator<SubscriptionCart>() {

    @Override
    public int compare(SubscriptionCart o1, SubscriptionCart o2) {
      return o1.getStrId().compareTo(o2.getStrId());
    }
  });
  return  -1;
}
//  private void addFragment() {
//    getChildFragmentManager()
//        .beginTransaction()
//        .add(R.id.sample_container, new SubscriptionDetailFragment(), "SOMETAG").
//        commit();
//
//  }

  private void setupPriceRecyclerView() {

    priceAdapter = null;
      subscriptionBottomSheetList
              .setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
      priceAdapter = new SubscriptionBottomSheetAdapter(getActivity(), mSubscriptionCartList);
    snapHelper.attachToRecyclerView(subscriptionBottomSheetList);
      subscriptionBottomSheetList.setAdapter(priceAdapter);
      snapHelper.attachToRecyclerView(subscriptionBottomSheetList);

    if(mSubscriptionCartList.size()>3)
    {
     /* ViewGroup.LayoutParams params=subscriptionBottomSheetList.getLayoutParams();
      params.height=getActivity().getResources().getBoolean(R.bool.isTablet)?(int) (getScreenHeightInDPs(getActivity())*(0.32)) : (int) (getScreenHeightInDPs(getActivity())*(0.95));
      Log.e("Height",""+params.height);
      subscriptionBottomSheetList.setLayoutParams(params);*/

      ViewGroup.LayoutParams params=subscriptionBottomSheetList.getLayoutParams();
      params.height=getActivity().getResources().getBoolean(R.bool.isTablet)?(int) (getScreenHeightInDPs(getActivity())) : (int) (getScreenHeightInDPs(getActivity()));
      Log.e("Height",""+params.height);
      subscriptionBottomSheetList.setLayoutParams(params);

    }else{
      ViewGroup.LayoutParams params=subscriptionBottomSheetList.getLayoutParams();
      params.height= ViewGroup.LayoutParams.WRAP_CONTENT;
      subscriptionBottomSheetList.setLayoutParams(params);
    }
    priceAdapter.setOnDeleteItemFromCart(new OnDeleteItemFromCart() {
      @Override
      public void onDelete(int position, SubscriptionCart priceList) {
        mSubscriptionCartList.remove(position);
        cartlistDatabase.cartListDao().delete(priceList);

        populateBottomSheet();
                    if(subscriptionList.size()>0)
            {
              for(int j=0;j<subscriptionList.size();j++)
              {
                if(subscriptionList.get(j).getStrPackageOneId() != null && subscriptionList.get(j).getStrPackageOneId().equalsIgnoreCase(priceList.getStrId())) {
                  subscriptionList.get(j).setMiniSubscriptionChecked(false);
                  adapter.notifyItemChanged(j);
                }
                else if(subscriptionList.get(j).getStrPackageTwoId() != null && subscriptionList.get(j).getStrPackageTwoId().equalsIgnoreCase(priceList.getStrId())) {
                  subscriptionList.get(j).setLargeSubscriptionChecked(false);
                  adapter.notifyItemChanged(j);
                }
              }

            }
        //deletePackage(priceList,position);

      }
    });
  }

  @Override
  public void update() {
    populateSubscriptionList();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    EventBus.getDefault().unregister(this);
  }

  @Subscribe(threadMode = ThreadMode.MAIN)
  public void onMessageEvent(ActionEvent event) {

    if(event.getAction().equals("successCart")){
      populateBottomSheet();
    }
    else if(event.getAction().equals("deleteCart")){
      priceAdapter.notifyDataSetChanged();
    }
  }

  @Override
  public void onClick(View v) {
    switch (v.getId())
    {
      case R.id.txt_subscription_cart:
        if(mSubscriptionCartList.size()>0) {
          try {
          JSONArray jsonArray = new JSONArray();
          JSONObject jsonObject = new JSONObject();
          jsonObject.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));

            jsonObject.put("user_uid",sessionManager.getUserDetails().get(SessionManager.KEY_UID));

          for(int i=0;i<mSubscriptionCartList.size();i++)
          {
            jsonArray.put(mSubscriptionCartList.get(i).getStrId());
          }
          jsonObject.put("package_ids",jsonArray);
          addCartPackages(jsonObject);
          } catch (JSONException e) {
            e.printStackTrace();
          }
        }
    }
  }

  private void addCartPackages(JSONObject jsonObject) {
    loader.showLoader();
    Log.e("jsonObject",jsonObject.toString());
    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, IConstant.add_packages, jsonObject,
            new Response.Listener<JSONObject>(){
              @Override
              public void onResponse(JSONObject response) {
                Log.e("Response", response.toString());
                loader.dismissLoader();
                try {
                  if(response.getString("status").equals("1"))
                    startActivity(new Intent(getActivity(),ReviewOrder.class));
                  getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                } catch (JSONException e) {
                  e.printStackTrace();
                }
              }
            },
            new Response.ErrorListener(){
              @Override
              public void onErrorResponse(VolleyError error) {
                Log.e("Error.Response", error.toString());
                loader.dismissLoader();
                String json = null;
                NetworkResponse response = error.networkResponse;
                if(response != null && response.data != null){
                  switch(response.statusCode){
                    case 400:

                      json = new String(response.data);
                      System.out.println(json);
                      break;
                  }
                }
              }
            })
    {


      @Override
      public Map<String, String> getHeaders() throws AuthFailureError {
        Log.e("Header",sessionManager.getApiHeader().toString());
        return sessionManager.getApiHeader();
      }
    };

    RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
    requestQueue.add(jsonObjectRequest);
  }
  private  int getScreenHeightInDPs(Context context){
    DisplayMetrics dm = new DisplayMetrics();
    WindowManager windowManager = (WindowManager) context.getSystemService(WINDOW_SERVICE);
    windowManager.getDefaultDisplay().getMetrics(dm);
        /*
            In this example code we converted the float value
            to nearest whole integer number. But, you can get the actual height in dp
            by removing the Math.round method. Then, it will return a float value, you should
            also make the necessary changes.
        */

        /*
            public int heightPixels
                The absolute height of the display in pixels.

            public float density
             The logical density of the display.
        */
    int heightInDP = Math.round(dm.heightPixels / dm.density);
    return heightInDP;
  }

  private void  addPackageToCart(final SubscriptionCart priceList, String strPackageId, final int position) {
    //loader.showLoader();
    ServiceRequest mRequest = new ServiceRequest(getActivity());
    HashMap<String,String> params = new HashMap<>();
    params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));
    params.put("user_uid",sessionManager.getUserDetails().get(SessionManager.KEY_UID));
    params.put("package_id",strPackageId);
    mRequest.makeServiceRequest(IConstant.add_package, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
      @Override
      public void onCompleteListener(String response) {
        System.out.println("------------add_package Response----------------" + response);
        String sStatus = "";
        String message = "";
        try {
          JSONObject object = new JSONObject(response);
          sStatus = object.getString("status");
          message = object.getString("message");
          //loader.dismissLoader();
          if (sStatus.equalsIgnoreCase("1")) {




          } else if(sStatus.equals("00")){
            customAlert.singleLoginAlertLogout();
          } else if(sStatus.equalsIgnoreCase("01")){
            customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
          } else {
            //showAlertOOps(getString(R.string.alert_oops),message,priceList);
          }
        } catch (JSONException e) {
         // loader.dismissLoader();
          e.printStackTrace();
        }

      }
      @Override
      public void onErrorListener(String errorMessage) {
        //loader.dismissLoader();
       // AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
      }
    });
  }
  public void showAlertOOps(String title, String desc,final SubscriptionCart priceList)
  {
    final Dialog alertDialog = new Dialog(getActivity());
    alertDialog.setContentView(R.layout.custom_alert);
    alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

    TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
    TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
    textViewTitle.setText(title);
    textViewDesc.setText(desc);
    Button btnOk = alertDialog.findViewById(R.id.btnOk);
    Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
    btnCancel.setVisibility(View.GONE);
    btnOk.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        alertDialog.dismiss();
        populateSubscriptionList();
      }
    });
    if(!alertDialog.isShowing())
      alertDialog.show();
  }
  private void CollectCartItems() {
    mSubscriptionCartList.clear();
    Log.e("InsertedCartItems",cartlistDatabase.cartListDao().getAll().toString());
    mSubscriptionCartList = cartlistDatabase.cartListDao().getAll();
    ActionEvent event=new ActionEvent();
    event.setAction("successCart");
    EventBus.getDefault().post(event);
  }

  private void  removePackageFromCart(final SubscriptionCart priceList, String strPackageId, int position) {
    //loader.showLoader();
    ServiceRequest mRequest = new ServiceRequest(getActivity());
    HashMap<String,String> params = new HashMap<>();
    params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));
    params.put("package_id",strPackageId);
    mRequest.makeServiceRequest(IConstant.remove_package, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
      @Override
      public void onCompleteListener(String response) {
        System.out.println("------------remove_package Response----------------" + response);
        String sStatus = "";
        String message = "";
        try {
          JSONObject object = new JSONObject(response);
          sStatus = object.getString("status");
          message = object.getString("message");
          //loader.dismissLoader();
          if (sStatus.equalsIgnoreCase("1")) {


          } else if(sStatus.equals("00")){
            customAlert.singleLoginAlertLogout();
          } else if(sStatus.equalsIgnoreCase("01")){
            customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
          } else {
            //customAlert.showAlertOk(getString(R.string.alert_oops),message);
          }
        } catch (JSONException e) {
          //loader.dismissLoader();
          e.printStackTrace();
        }

      }
      @Override
      public void onErrorListener(String errorMessage) {
//        loader.dismissLoader();
//        AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
      }
    });
  }
  private void  deletePackage(final SubscriptionCart priceList,final int position) {
   // loader.showLoader();
    ServiceRequest mRequest = new ServiceRequest(getActivity());
    HashMap<String,String> params = new HashMap<>();
    params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));
    params.put("package_id",priceList.getStrId());
    mRequest.makeServiceRequest(IConstant.remove_package, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
      @Override
      public void onCompleteListener(String response) {
        System.out.println("------------remove_package Response----------------" + response);
        String sStatus = "";
        String message = "";
        try {
          JSONObject object = new JSONObject(response);
          sStatus = object.getString("status");
          message = object.getString("message");
         // loader.dismissLoader();
          if (sStatus.equalsIgnoreCase("1")) {


//            populateSubscriptionList();
//            if(subscriptionList.size()>0)
//            {
//              for(int j=0;j<subscriptionList.size();j++)
//              {
//                if(subscriptionList.get(j).getStrPackageOneId().equalsIgnoreCase(priceList.getStrId())) {
//                  subscriptionList.get(j).setMiniSubscriptionChecked(false);
//                  adapter.notifyItemChanged(j);
//                }
//                else if(subscriptionList.get(j).getStrPackageTwoId().equalsIgnoreCase(priceList.getStrId())) {
//                  subscriptionList.get(j).setLargeSubscriptionChecked(false);
//                  adapter.notifyItemChanged(j);
//                }
//              }
//
//            }

          } else if(sStatus.equals("00")){
            customAlert.singleLoginAlertLogout();
          } else if(sStatus.equalsIgnoreCase("01")){
            customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
          } else {
            customAlert.showAlertOk(getString(R.string.alert_oops),message);
          }
        } catch (JSONException e) {
         // loader.dismissLoader();
          e.printStackTrace();
        }

      }
      @Override
      public void onErrorListener(String errorMessage) {
        //loader.dismissLoader();
        //AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
      }
    });
  }

  @Override
  public void onResume() {
    super.onResume();
    mShimmerViewContainer.startShimmer();

  }
  @Override
  public void setUserVisibleHint(boolean isVisibleToUser) {
    super.setUserVisibleHint(isVisibleToUser);
    if (isVisibleToUser) {

    }
  }
  @Override
  public void onPause() {
    mShimmerViewContainer.stopShimmer();
    super.onPause();
  }

  private class AsyncCartRunner extends AsyncTask<String, String, String> {
    CartDataBase cartlistDatabase = CartDataBase.getAppDatabase(getActivity());
    String result = "";


    @Override
    protected String doInBackground(String... strings) {
      ServiceRequest mRequest = new ServiceRequest(getActivity());
      HashMap<String, String> params = new HashMap<>();
      params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
      mRequest.makeServiceRequest(IConstant.cartDetails, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
        @Override
        public void onCompleteListener(String response) {
          System.out.println("------------cartDetails Response----------------" + response);
          String sStatus = "";
          try {

            JSONObject object = new JSONObject(response);
            sStatus = object.getString("status");


            if (sStatus.equalsIgnoreCase("1")) {

              JSONObject tax = object.getJSONObject("tax");
              double gst = Double.parseDouble(tax.getString("gst"));
              double sgst = Double.parseDouble(tax.getString("sgst"));
              double service = Double.parseDouble(tax.getString("service"));
              JSONObject cart = object.getJSONObject("cart");
              JSONArray packagesArray = cart.getJSONArray("packages");
              cartlistDatabase.cartListDao().nukeTable();
              if(packagesArray.length()>0)
              {
                for(int i=0;i<packagesArray.length();i++)
                {
                  JSONObject packagesObject = packagesArray.getJSONObject(i);
                  SubscriptionCart subscriptionCart = new SubscriptionCart();
                  subscriptionCart.setStrId(packagesObject.getString("id"));
                  subscriptionCart.setTitle(packagesObject.getString("subj_name"));
                  double packagePrice = Double.parseDouble(packagesObject.getString("price"));
                  double afterGst = packagePrice+packagePrice*gst;
                  double serviceCharge = afterGst*service;
                  double sgstCharge = serviceCharge*sgst;
                  subscriptionCart.setPrice(String.valueOf(Math.round(afterGst+serviceCharge+sgstCharge)));
                  if(packagesObject.getString("status").equalsIgnoreCase("1") &&packagesObject.getString("s_status").equalsIgnoreCase("1") && packagesObject.getString("g_status").equalsIgnoreCase("1") && packagesObject.getString("g_trial").equalsIgnoreCase("0")) {
                    subscriptionCart.setSubscription(packagesObject.getString("duration_days") + getString(R.string.days) + " " + packagesObject.getString("duration_hours") + getString(R.string.hours));
                  }
                  else {
                    subscriptionCart.setSubscription("Not Available");
                    subscriptionCart.setPrice("0");
                  }


                  SubscriptionCart subscriptionCart1 = cartlistDatabase.cartListDao().getCartItem(subscriptionCart.getStrId());
                  if(subscriptionCart1 != null && subscriptionCart1.getStrId() != null && !subscriptionCart1.getStrId().isEmpty() && subscriptionCart1.getStrId().equalsIgnoreCase(subscriptionCart.getStrId()))
                    cartlistDatabase.cartListDao().update(subscriptionCart);
                  else {
                    cartlistDatabase.cartListDao().insertAll(subscriptionCart);
                  }
                }
              }




            } else {
            }
          } catch (JSONException e) {

            e.printStackTrace();
          }

        }

        @Override
        public void onErrorListener(String errorMessage) {
          // AlertBox.showSnackBoxPrimaryBa(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
        }
      });
      return result;


    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);

      // new AsyncSubjectsRunner(s).execute();
    }
  }

  private void createSnap() {
    snapHelper = new LinearSnapHelper() {
      @Override
      public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
        int targetPosition = -1;
        try {
          View centerView = findSnapView(layoutManager);
          if (centerView == null)
            return RecyclerView.NO_POSITION;

          int position = layoutManager.getPosition(centerView);

          if (layoutManager.canScrollHorizontally()) {
            if (velocityX < 0) {
              targetPosition = position - 1;
            } else {
              targetPosition = position + 1;
            }
          }

          if (layoutManager.canScrollVertically()) {
            if (velocityY < 0) {
              targetPosition = position - 1;
            } else {
              targetPosition = position + 1;
            }
          }

          final int firstItem = 0;
          final int lastItem = layoutManager.getItemCount() - 1;
          targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

        }catch (Exception e)
        {
          e.printStackTrace();
        }
        return targetPosition;
      }
    };
  }
}
