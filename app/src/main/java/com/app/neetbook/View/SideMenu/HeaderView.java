package com.app.neetbook.View.SideMenu;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.neetbook.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HeaderView extends RelativeLayout {

    @BindView(R.id.txtTestTitle)
    TextView tvName;

    @BindView(R.id.txtTestTitle)
    TextView tvDescription;

    public HeaderView(Context context) {

        super(context);
        tvName=findViewById(R.id.txtTestTitle);
        tvDescription=findViewById(R.id.txtTestTitle);
    }

    public HeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public HeaderView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    public void bindTo(String name, String lastSeen) {
        tvName=findViewById(R.id.txtTestTitle);
        tvDescription=findViewById(R.id.txtTestTitle);
        this.tvName.setText(name);
        this.tvDescription.setText(lastSeen);
    }

    public void setTextSize(float size) {
        tvName=findViewById(R.id.txtTestTitle);
        tvDescription=findViewById(R.id.txtTestTitle);
//       tvName.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
    }

}
