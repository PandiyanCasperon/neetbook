package com.app.neetbook.View.SideMenu.New;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.app.neetbook.R;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewArticleSubcsriptionActivity extends AppCompatActivity  {

    protected AppBarLayout appBarLayout;
    protected Toolbar toolbar;
    private CollapsingToolbarLayout collapsing_toolbar;
    TextView toolar_title_tv;
    private RecyclerView recyclerViewGroups;

    Menu menu;
    private boolean isHideToolbarView = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_article_subcsription);


        initUi();
    }

    private void initUi() {

        appBarLayout=findViewById(R.id.appbar);
        toolbar=findViewById(R.id.toolbar);
        collapsing_toolbar=findViewById(R.id.collapsing_toolbar);
        toolar_title_tv=findViewById(R.id.toolar_title_tv);
        recyclerViewGroups=findViewById(R.id.recyclerViewGroups);

        collapsing_toolbar.setCollapsedTitleTextColor(getResources().getColor(R.color.white));
        collapsing_toolbar.setExpandedTitleColor(getResources().getColor(R.color.article_title_color));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("Article");
    }

  /*  @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        MenuItem menuItem = menu.findItem(R.id.edit);
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;

        if (percentage == 1f && isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.VISIBLE);
            menuItem.setVisible(true);
            isHideToolbarView = !isHideToolbarView;

        } else if (percentage < 1f && !isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.GONE);
            menuItem.setVisible(false);
            isHideToolbarView = !isHideToolbarView;
        }
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu=menu;
        getMenuInflater().inflate(R.menu.menu_article, menu);
        ShowMenu();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.edit) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void ShowMenu(){

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
                MenuItem menuItem = menu.findItem(R.id.edit);

                int maxScroll = appBarLayout.getTotalScrollRange();
                float percentage = (float) Math.abs(offset) / (float) maxScroll;

                if (percentage == 1f && isHideToolbarView) {

                    menuItem.setVisible(true);

                    isHideToolbarView = !isHideToolbarView;

                } else if (percentage < 1f && !isHideToolbarView) {

                    menuItem.setVisible(false);
                    isHideToolbarView = !isHideToolbarView;
                }
            }
        });
    }
}
