package com.app.neetbook.View.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.neetbook.Adapter.MobilehomeGroupViewAdapter;
import com.app.neetbook.Adapter.ProfileOldSubscriptionAdapter;
import com.app.neetbook.Adapter.ProfileSubscriptionAdapter;
import com.app.neetbook.Interfaces.OnSpinerItemClick;
import com.app.neetbook.Model.GroupChildBean;
import com.app.neetbook.Model.StateBean;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Data.CompleteProfileData;
import com.app.neetbook.Utils.Data.MyProfileData;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.alerter.AlertBox;
import com.app.neetbook.Utils.widgets.DisplayOrientation;
import com.app.neetbook.Utils.widgets.SpinnerDialog;
import com.app.neetbook.View.DrawerContentSlideActivity;
import com.app.neetbook.View.EditProfileActivity;
import com.app.neetbook.View.NoInterNetActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class MyProfileFragment extends Fragment implements View.OnClickListener {

    RecyclerView recyclerOldSubscription, recyclerCurrentSubscription;
    ArrayList<GroupChildBean> currentSubsList;
    ArrayList<GroupChildBean> oldSubsList;
    ProfileSubscriptionAdapter profileSubscriptionAdapter;
    ProfileOldSubscriptionAdapter profileOldSubscriptionAdapter;
    RelativeLayout rlCurrentSubsArrow, rlMyProfileSubmit, rlWallet, rlOldSubscription, imgMyProfileCancel;
    ImageView imgCurrentSUBArrow, imgWalletArrow, imgOldSUBArrow;
    Spinner spinnerCountryState, spinnerCode, spinnerCity, spinnerCountry;
    ArrayList<String> countryList;
    ArrayList<StateBean> countryBeanList;
    ArrayList<StateBean> stateBeanList;
    ArrayList<String> stateList;
    ArrayList<String> cityList;
    ArrayList<StateBean> cityBeanList;
    LinearLayout llAddress, llWalletBalance;
    ConstraintLayout constrainEditView;
    ImageView imgProfileEdit;
    String strCountry = "", strState = "", strCity = "", strEduCState = "", strCollege = "", strAddress1 = "", strAddress2 = "", strPinCode = "";
    Loader loader;
    CustomAlert customAlert;
    SessionManager sessionManager;
    TextView txtPhone, txtName, txtCollege, txtAddress, txtMail;
    EditText etPinCode, etAddressOne, etAddressTwo;
    TextView txtStateHint, txtCityHint, txtCountryHint;
    SpinnerDialog dialogCountry, dialogState, dialogCity;
    TextView txtCityState, txtWalletBalance;
    ConnectionDetector cd;
    private boolean showLoader = false;
    TextView txtOldSubs, txtWallet, txtCurrentSub, txtCurrentNoSubsAvail, txtOldNoSubsAvail;
    ImageView imgCurrentSubsText, imgOldSubsText, imgWalletText;
    EditText etLastName, etFirstName;
    private int page = 1;
    int loadMoreEnable = 0;
    private int totalSubsCount;
    LinearSnapHelper snapHelper, snapHelperCurrent;

    private LinearLayout bottom_llWallet;
    private LinearLayout current_sub_arrow_layout;
    private LinearLayout old_sub_arrow_layout;
    private LinearLayout wallet_arrow_layout;
    private LinearLayout bottom_old_subscription_layout;

    public MyProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getActivity());
        cd = new ConnectionDetector(getActivity());
        loader = new Loader(getActivity());
        customAlert = new CustomAlert(getActivity());
        showLoader = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = null;
        if (getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(getActivity()) == 0 || DisplayOrientation.getDisplayOrientation(getActivity()) == 2))
            rootView = inflater.inflate(R.layout.fragment_my_profile_portrait, container, false);
        else if (getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(getActivity()) == 1 || DisplayOrientation.getDisplayOrientation(getActivity()) == 3))
            rootView = inflater.inflate(R.layout.new_my_profile_layout, container, false);
        else if (!getResources().getBoolean(R.bool.isTablet))
            rootView = inflater.inflate(R.layout.new_my_profile_layout, container, false);
        init(rootView);
        return rootView;
    }

    private void init(View rootView) {
        recyclerCurrentSubscription = rootView.findViewById(R.id.recyclerCurrentSubscription);
        recyclerOldSubscription = rootView.findViewById(R.id.recyclerOldSubscription);
        rlCurrentSubsArrow = rootView.findViewById(R.id.rlCurrentSubsArrow);
        imgCurrentSUBArrow = rootView.findViewById(R.id.imgCurrentSUBArrow);
        imgOldSUBArrow = rootView.findViewById(R.id.imgOldSUBArrow);
        rlOldSubscription = rootView.findViewById(R.id.rlOldSubscription);
        rlWallet = rootView.findViewById(R.id.rlWallet);
        imgWalletArrow = rootView.findViewById(R.id.imgWalletArrow);
        llWalletBalance = rootView.findViewById(R.id.llWalletBalance);
        txtWalletBalance = rootView.findViewById(R.id.txtWalletBalance);
        imgProfileEdit = rootView.findViewById(R.id.imgProfileEdit);
        txtMail = rootView.findViewById(R.id.txtMail);
        txtPhone = rootView.findViewById(R.id.txtPhone);
        txtAddress = rootView.findViewById(R.id.txtAddress);
        txtCollege = rootView.findViewById(R.id.txtCollege);
        txtName = rootView.findViewById(R.id.txtName);
        etPinCode = rootView.findViewById(R.id.etPinCode);
        etAddressOne = rootView.findViewById(R.id.etAddressOne);
        etAddressTwo = rootView.findViewById(R.id.etAddressTwo);
        txtCityState = rootView.findViewById(R.id.txtCityState);
        txtCurrentSub = rootView.findViewById(R.id.txtCurrentSub);
        txtOldNoSubsAvail = rootView.findViewById(R.id.txtOldNoSubsAvail);
        txtCurrentNoSubsAvail = rootView.findViewById(R.id.txtCurrentNoSubsAvail);
        txtOldSubs = rootView.findViewById(R.id.txtOldSubs);
        txtWallet = rootView.findViewById(R.id.txtWallet);
        imgCurrentSubsText = rootView.findViewById(R.id.imgCurrentSubsText);
        imgOldSubsText = rootView.findViewById(R.id.imgOldSubsText);
        imgWalletText = rootView.findViewById(R.id.imgWalletText);
        etFirstName = rootView.findViewById(R.id.etFirstName);
        etLastName = rootView.findViewById(R.id.etLastName);
        bottom_llWallet = rootView.findViewById(R.id.bottom_llWallet);
        bottom_old_subscription_layout=rootView.findViewById(R.id.bottom_old_subscription_layout);
        current_sub_arrow_layout = rootView.findViewById(R.id.current_sub_arrow_layout);
        old_sub_arrow_layout = rootView.findViewById(R.id.old_sub_arrow_layout);
        wallet_arrow_layout = rootView.findViewById(R.id.wallet_arrow_layout);

        imgProfileEdit.setOnClickListener(this);
        createSnap();
        if (getResources().getBoolean(R.bool.isTablet)) {
            initTablet(rootView);
        }
        rlCurrentSubsArrow.setOnClickListener(this);
        rlOldSubscription.setOnClickListener(this);
        rlWallet.setOnClickListener(this);
        bottom_llWallet.setOnClickListener(this);
        bottom_old_subscription_layout.setOnClickListener(this);
        currentSubsList = new ArrayList<GroupChildBean>();
        oldSubsList = new ArrayList<GroupChildBean>();

        txtName.setText(sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) != null && !sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("") && !sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("nill") ? sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) + " " + sessionManager.getUserDetails().get(SessionManager.KEY_LNAME) : sessionManager.getUserDetails().get(SessionManager.KEY_EMAIL));
       // setCurrentSubsVisible();

        if (cd.isConnectingToInternet()) {
            getUserProfileData();
            prepareCurrentSubscription();
            prepareOldSubscription();
        } else {
            startActivity(new Intent(getContext(), NoInterNetActivity.class));
            DrawerContentSlideActivity.drawerContentSlideActivity.finish();
        }
        LoadMoreListener();

    }

    private void Listeners() {


        etFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                enableDisablesubmitButton();
                if (!etFirstName.getText().toString().isEmpty())
                    MyProfileData.strFName = etFirstName.getText().toString();


            }
        });
        etLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                enableDisablesubmitButton();
                if (!etLastName.getText().toString().isEmpty())
                    MyProfileData.strLName = etLastName.getText().toString();


            }
        });
        etAddressOne.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                enableDisablesubmitButton();
                if (!etAddressOne.getText().toString().isEmpty())
                    MyProfileData.strAddress1 = etAddressOne.getText().toString();


            }
        });
        etAddressTwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                enableDisablesubmitButton();
                if (!etAddressTwo.getText().toString().isEmpty())
                    MyProfileData.strAddress2 = etAddressTwo.getText().toString();


            }
        });
        etPinCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                enableDisablesubmitButton();
                if (!etPinCode.getText().toString().isEmpty())
                    MyProfileData.strPost = etPinCode.getText().toString();


            }
        });
        txtCountryHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCountry.showSpinerDialog();
            }
        });
        txtStateHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogState.showSpinerDialog();
            }
        });
        txtCityHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCity.showSpinerDialog();
            }
        });

        dialogCountry.setCancellable(true);
        dialogCountry.setShowKeyboard(false);

        dialogCountry.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {

                MyProfileData.strcountry = item;
                MyProfileData.strCountryId = countryBeanList.get(position).get_id();
                txtCountryHint.setText(item);
                strCountry = item;
                enableDisablesubmitButton();
                txtCountryHint.setTextColor(position == 0 ? getResources().getColor(R.color.hint_color) : getResources().getColor(R.color.black));
                showLoader = true;
                if (position != 0)
                    getStateList(countryBeanList.get(position).get_id());
                txtStateHint.setText("STATE");
                txtStateHint.setTextColor(getResources().getColor(R.color.hint_color));
                txtCityHint.setText("CITY");
                txtCityHint.setTextColor(getResources().getColor(R.color.hint_color));
            }
        });

        dialogState.setCancellable(true);
        dialogState.setShowKeyboard(false);

        dialogState.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                MyProfileData.strState = item;
                MyProfileData.strStateId = stateBeanList.get(position).get_id();
                strState = item;
                txtStateHint.setText(item);
                txtStateHint.setTextColor(position == 0 ? getResources().getColor(R.color.hint_color) : getResources().getColor(R.color.black));
                enableDisablesubmitButton();
                showLoader = true;
                if (position != 0)
                    getCityList(stateBeanList.get(position).get_id());
                txtCityHint.setText("CITY");
                txtCityHint.setTextColor(getResources().getColor(R.color.hint_color));
            }
        });

        dialogCity.setCancellable(true);
        dialogCity.setShowKeyboard(false);

        dialogCity.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                strCity = item;
                txtCityHint.setText(item);
                txtCityHint.setTextColor(position == 0 ? getResources().getColor(R.color.hint_color) : getResources().getColor(R.color.black));
                enableDisablesubmitButton();
                MyProfileData.strCity = item;
                MyProfileData.strCityId = cityBeanList.get(position).get_id();
            }
        });


    }

    private void LoadMoreListener() {
        recyclerCurrentSubscription.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();

                boolean endHasBeenReached = lastVisible + 5 >= totalItemCount;
                Log.e("totalItemCount", "" + totalItemCount);
                Log.e("lastVisible", "" + lastVisible);
                Log.e("totalSubsCount", "" + totalSubsCount);
                Log.e("articleList.size()", "" + currentSubsList);
                Log.e("loadMoreEnable", "" + loadMoreEnable);

                if (lastVisible == totalItemCount - 1 && totalSubsCount > currentSubsList.size() && loadMoreEnable == 0) {
                    page = page + 1;
                    Log.e("loadMoreSubjects()", "Called");
                    loadMoreEnable = 1;
                    //loadMoreprepareCurrentSubscription(lastVisible);
                }
            }
        });

        recyclerOldSubscription.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();

                boolean endHasBeenReached = lastVisible + 5 >= totalItemCount;
                Log.e("totalItemCount", "" + totalItemCount);
                Log.e("lastVisible", "" + lastVisible);
                Log.e("totalSubsCount", "" + totalSubsCount);
                Log.e("articleList.size()", "" + currentSubsList);
                Log.e("loadMoreEnable", "" + loadMoreEnable);

                if (lastVisible == totalItemCount - 1 && totalSubsCount > oldSubsList.size() && loadMoreEnable == 0) {
                    page = page + 1;
                    Log.e("loadMoreSubjects()", "Called");
                    loadMoreEnable = 1;
                    //loadMoreOldSubscription(lastVisible);
                }
            }
        });
    }

    private void setOrientationRestore() {

        if (getResources().getBoolean(R.bool.isTablet)) {
            if (DisplayOrientation.getDisplayOrientation(getActivity()) == 1 || DisplayOrientation.getDisplayOrientation(getActivity()) == 3) {
                if (MyProfileData.currentView == 1) {
                    constrainEditView.setVisibility(View.GONE);
                    llAddress.setVisibility(View.VISIBLE);
                } else {
                    constrainEditView.setVisibility(View.VISIBLE);
                    llAddress.setVisibility(View.GONE);
                    etPinCode.setText(MyProfileData.strPost);
                    etFirstName.setText(MyProfileData.strFName);
                    etLastName.setText(MyProfileData.strLName);
                    etAddressOne.setText(MyProfileData.strAddress1);
                    etAddressTwo.setText(MyProfileData.strAddress2);
                    txtCityHint.setText(MyProfileData.strCity);
                    txtStateHint.setText(MyProfileData.strState);
                    txtCountryHint.setText(MyProfileData.strcountry);
                }


            } else {
                if (MyProfileData.currentView == 1) {
                    constrainEditView.setVisibility(View.GONE);
                    llAddress.setVisibility(View.VISIBLE);
                } else {
                    constrainEditView.setVisibility(View.VISIBLE);
                    llAddress.setVisibility(View.GONE);
                    etPinCode.setText(MyProfileData.strPost);
                    etFirstName.setText(MyProfileData.strFName);
                    etLastName.setText(MyProfileData.strLName);
                    etAddressOne.setText(MyProfileData.strAddress1);
                    etAddressTwo.setText(MyProfileData.strAddress2);
                    txtCityHint.setText(MyProfileData.strCity);
                    txtStateHint.setText(MyProfileData.strState);
                    txtCountryHint.setText(MyProfileData.strcountry);

                }
            }
            strCountry = MyProfileData.strcountry;
            strState = MyProfileData.strState;
            strCity = MyProfileData.strCity;


            if (MyProfileData.strCurrentSubs.equals("open")) {
                recyclerCurrentSubscription.setVisibility(View.VISIBLE);
                Picasso.with(getActivity()).load(R.drawable.arrow_top_gradient).into(imgCurrentSUBArrow);
                txtCurrentSub.setVisibility(View.GONE);
                imgCurrentSubsText.setVisibility(View.VISIBLE);
                if (currentSubsList.size() == 0)
                    txtCurrentNoSubsAvail.setVisibility(View.VISIBLE);

            } else {
                txtCurrentSub.setVisibility(View.VISIBLE);
                imgCurrentSubsText.setVisibility(View.GONE);
                recyclerCurrentSubscription.setVisibility(View.GONE);
                txtCurrentNoSubsAvail.setVisibility(View.GONE);
                Picasso.with(getActivity()).load(R.drawable.down_arrow_gradient).into(imgCurrentSUBArrow);
            }
            if (MyProfileData.strOldSubs.equals("open")) {
                txtOldSubs.setVisibility(View.GONE);
                imgOldSubsText.setVisibility(View.VISIBLE);
                recyclerOldSubscription.setVisibility(View.VISIBLE);
                if (oldSubsList.size() == 0)
                    txtOldNoSubsAvail.setVisibility(View.VISIBLE);
                Picasso.with(getActivity()).load(R.drawable.arrow_top_gradient).into(imgCurrentSUBArrow);
            } else {
                txtOldSubs.setVisibility(View.VISIBLE);
                imgOldSubsText.setVisibility(View.GONE);
                recyclerOldSubscription.setVisibility(View.GONE);
                txtOldNoSubsAvail.setVisibility(View.GONE);
                Picasso.with(getActivity()).load(R.drawable.down_arrow_gradient).into(imgCurrentSUBArrow);
            }
            if (MyProfileData.strWallet.equals("open")) {
                txtWallet.setVisibility(View.GONE);
                imgWalletText.setVisibility(View.VISIBLE);
                llWalletBalance.setVisibility(View.VISIBLE);
                Picasso.with(getActivity()).load(R.drawable.arrow_top_gradient).into(imgCurrentSUBArrow);
            } else {
                txtWallet.setVisibility(View.VISIBLE);
                imgWalletText.setVisibility(View.GONE);
                llWalletBalance.setVisibility(View.GONE);
                Picasso.with(getActivity()).load(R.drawable.down_arrow_gradient).into(imgCurrentSUBArrow);
            }
        }

    }

    private void initTablet(View rootView) {
        llAddress = rootView.findViewById(R.id.llAddress);
        constrainEditView = rootView.findViewById(R.id.constrainEditView);
        spinnerCountry = rootView.findViewById(R.id.spinnerCountry);
        spinnerCountryState = rootView.findViewById(R.id.spinnerCountryState);
        spinnerCity = rootView.findViewById(R.id.spinnerCity);
        txtOldNoSubsAvail = rootView.findViewById(R.id.txtOldNoSubsAvail);
        txtCurrentNoSubsAvail = rootView.findViewById(R.id.txtCurrentNoSubsAvail);
        rlMyProfileSubmit = rootView.findViewById(R.id.rlMyProfileSubmit);
        imgProfileEdit = rootView.findViewById(R.id.imgProfileEdit);
        imgMyProfileCancel = rootView.findViewById(R.id.imgMyProfileCancel);


        txtCountryHint = rootView.findViewById(R.id.txtCountryHint);
        txtStateHint = rootView.findViewById(R.id.txtStateHint);
        txtCityHint = rootView.findViewById(R.id.txtCityHint);


        rlMyProfileSubmit.setOnClickListener(this);
        imgProfileEdit.setOnClickListener(this);
        imgMyProfileCancel.setOnClickListener(this);


    }

    private void getUserProfileData() {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));

        mRequest.makeServiceRequest(IConstant.getUserProfileData, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------getUserProfileData Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1")) {

                        JSONObject respo = object.getJSONObject("respo");
                        JSONObject education = respo.getJSONObject("education");
                        JSONObject residence = respo.getJSONObject("residence");
                        JSONObject wallet_settings = respo.getJSONObject("wallet_settings");
                        Log.e("WalletBalance", wallet_settings.getString("available"));
                        txtWalletBalance.setText(getString(R.string.currency_symbol) + " " + wallet_settings.getLong("available"));
                        txtMail.setText(respo.getString("email"));
                        txtPhone.setText(respo.getString("phone"));

                        if (txtCityState != null)
                            txtCityState.setText(residence.getString("city") + ", " + residence.getString("state") + ", " + residence.getString("country") + ",\n" + residence.getString("pincode"));
                        txtAddress.setText(residence.getString("line_1") + ", " + residence.getString("line_2"));

                        if (etPinCode != null)
                            etPinCode.setText(residence.getString("pincode"));

                        strAddress1 = residence.getString("line_1");
                        strAddress2 = residence.getString("line_2");
                        strPinCode = residence.getString("pincode");

                        strCountry = residence.getString("country");
                        MyProfileData.strcountry = residence.getString("country");
                        MyProfileData.strCountryId = residence.getString("country_id");
                        strState = residence.getString("state");
                        MyProfileData.strState = residence.getString("state");
                        MyProfileData.strStateId = residence.getString("state_id");
                        MyProfileData.strCity = residence.getString("city");
                        MyProfileData.strCityId = residence.getString("city_id");
                        strCity = residence.getString("city");
                        strEduCState = education.getString("state_name");
                        strCollege = education.getString("college_name");
                        txtCollege.setText(strCollege + ", " + strEduCState);
                        if (getResources().getBoolean(R.bool.isTablet)) {
                            txtCountryHint.setText(strCountry);
                            txtStateHint.setText(strState);
                            txtCityHint.setText(strCity);
                            txtStateHint.setTextColor(getResources().getColor(R.color.black));
                            txtCityHint.setTextColor(getResources().getColor(R.color.black));
                            txtCountryHint.setTextColor(getResources().getColor(R.color.black));
                            setSpinner();
                        }
                    } else if (sStatus.equals("00")) {
                        loader.dismissLoader();
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                try {
                    AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), errorMessage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void setSpinner() {

        countryList = new ArrayList<>();
        countryBeanList = new ArrayList<>();
        stateList = new ArrayList<>();
        stateBeanList = new ArrayList<>();
        cityList = new ArrayList<>();
        cityBeanList = new ArrayList<>();
        dialogCountry = new SpinnerDialog(getActivity(), countryList,
                "COUNTRY");
        dialogState = new SpinnerDialog(getActivity(), stateList,
                "STATE");
        dialogCity = new SpinnerDialog(getActivity(), cityList,
                "CITY");
        if (cd.isConnectingToInternet()) {
            getCountryList();
            getStateList(MyProfileData.strCountryId);
            getCityList(MyProfileData.strStateId);
        } else
            AlertBox.showSnackBox(getActivity(), getString(R.string.action_no_internet_title), getString(R.string.action_no_internet_message));

        if (getResources().getBoolean(R.bool.isTablet))
            Listeners();


    }

    private void getCountryList() {
        countryList.clear();
        countryBeanList.clear();
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();

        mRequest.makeServiceRequest(IConstant.getCountryList, Request.Method.POST, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------getCountryList Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1") && message.equalsIgnoreCase("Success")) {

                        countryList.add("COUNTRY");
                        StateBean stateBean1 = new StateBean();
                        stateBean1.set_id("-1");
                        stateBean1.setState_name("COUNTRY");
                        countryBeanList.add(stateBean1);
                        JSONArray stateListArray = object.getJSONArray("list");
                        if (stateListArray.length() > 0) {
                            for (int index = 0; index < stateListArray.length(); index++) {
                                countryList.add(stateListArray.getJSONObject(index).getString("country_name"));
                                StateBean stateBean = new StateBean();
                                stateBean.set_id(stateListArray.getJSONObject(index).getString("_id"));
                                stateBean.setState_name(stateListArray.getJSONObject(index).getString("country_name"));
                                countryBeanList.add(stateBean);
                            }
                        }
                        if (getResources().getBoolean(R.bool.isTablet))
                            setOrientationRestore();

                        //loader.dismissLoader();
//                        ArrayAdapter stateAdapter = new ArrayAdapter(getActivity(), R.layout.spinner_item,countryList);
//                        stateAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
//                        spinnerCountry.setAdapter(stateAdapter);
//
//                        if(!MyProfileData.strcountry.equals(""))
//                        {
//                            spinnerCountry.setSelection(countryList.contains(MyProfileData.strcountry)?countryList.indexOf(MyProfileData.strcountry):0);
//                        }


                    } else {

                        // loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }
                } catch (JSONException e) {
                    //loader.dismissLoader();
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
//                AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void getStateList(String id) {
        stateList.clear();
        stateBeanList.clear();
        if (showLoader)
            loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("country_id", id);

        mRequest.makeServiceRequest(IConstant.getCountrySateList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------getStateList Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1") && message.equalsIgnoreCase("Success")) {

                        stateList.add("STATE");
                        StateBean stateBean1 = new StateBean();
                        stateBean1.set_id("-1");
                        stateBean1.setState_name("STATE");
                        stateBeanList.add(stateBean1);
                        JSONArray stateListArray = object.getJSONArray("list");
                        if (stateListArray.length() > 0) {
                            for (int index = 0; index < stateListArray.length(); index++) {
                                stateList.add(stateListArray.getJSONObject(index).getString("state_name"));
                                StateBean stateBean = new StateBean();
                                stateBean.set_id(stateListArray.getJSONObject(index).getString("_id"));
                                stateBean.setState_name(stateListArray.getJSONObject(index).getString("state_name"));
                                stateBeanList.add(stateBean);
                            }
                        }
//                        getCityList(stateBeanList.get(stateList.indexOf(MyProfileData.strState)).get_id());

//                        ArrayAdapter stateEduAdapter = new ArrayAdapter(getActivity(), R.layout.spinner_item,stateList);
//                        stateEduAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
//                        spinnerCountryState.setAdapter(stateEduAdapter);
//
//                        if(!MyProfileData.strState.equals(""))
//                        {
//                            spinnerCountryState.setSelection(stateList.contains(MyProfileData.strState)?stateList.indexOf(MyProfileData.strState):0);
//                        }
                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else {

                        // loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), errorMessage);
            }
        });
    }

    private void getCityList(String id) {

        cityBeanList.clear();
        cityList.clear();
        if (showLoader)
            loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("state_id", id);

        mRequest.makeServiceRequest(IConstant.getCityList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------getCityList Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1") && message.equalsIgnoreCase("Success")) {

                        cityList.add("CITY");
                        StateBean stateBean1 = new StateBean();
                        stateBean1.set_id("-1");
                        stateBean1.setState_name("CITY");
                        cityBeanList.add(stateBean1);
                        JSONArray stateListArray = object.getJSONArray("list");
                        if (stateListArray.length() > 0) {
                            for (int index = 0; index < stateListArray.length(); index++) {
                                cityList.add(stateListArray.getJSONObject(index).getString("city_name"));
                                StateBean stateBean = new StateBean();
                                stateBean.set_id(stateListArray.getJSONObject(index).getString("_id"));
                                stateBean.setState_name(stateListArray.getJSONObject(index).getString("city_name"));
                                cityBeanList.add(stateBean);
                            }
                        }


                    } else if (sStatus.equals("00")) {

                        customAlert.singleLoginAlertLogout();
                    } else {

                        // loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
//                AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void prepareCurrentSubscription() {

        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("skip", "0");
        params.put("limit", "10");
        params.put("page", "" + page);
        mRequest.makeServiceRequest(IConstant.currentSubscription, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------currentSubscription Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    JSONObject respObject = object.getJSONObject("response");
                    sStatus = object.getString("status");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1")) {
                        totalSubsCount = Integer.parseInt(respObject.getString("total"));
                        JSONArray stateListArray = respObject.getJSONArray("content");
                        if (stateListArray.length() > 0) {
                            txtCurrentNoSubsAvail.setVisibility(View.GONE);
                            for (int index = 0; index < stateListArray.length(); index++) {
                                GroupChildBean groupChildBean = new GroupChildBean();
                                JSONObject content = stateListArray.getJSONObject(index);
                                groupChildBean.setStrId(content.getString("_id"));
                                groupChildBean.setStrTitle(content.getString("subj_name"));
                                groupChildBean.setStrLongName(content.getString("long_name"));
                                groupChildBean.setStrShortName(content.getString("short_name"));
                                groupChildBean.setStrImages(getResources().getBoolean(R.bool.isTablet) ? content.getString("tab_img") : content.getString("mob_img"));
                                groupChildBean.setStrRemainingHours(content.getString("remaining_hours") + " " + getString(R.string.hours) + "\n" + getString(R.string.remaining));
                                groupChildBean.setStrRemainingDays(content.getString("remaining_days") + " " + getString(R.string.days) + "\n" + getString(R.string.remaining));
                                currentSubsList.add(groupChildBean);
                            }
                            setAdapter(0);
                        }


                    } else if (sStatus.equals("00")) {

                        customAlert.singleLoginAlertLogout();
                    } else {

                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
//                AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
            }
        });

    }

    private void loadMoreprepareCurrentSubscription(final int lastVisibleCount) {

        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("skip", "0");
        params.put("limit", "10");
        params.put("page", "" + page);
        mRequest.makeServiceRequest(IConstant.currentSubscription, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------currentSubscriptionMore Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    JSONObject respObject = object.getJSONObject("response");
                    sStatus = object.getString("status");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1")) {

                        JSONArray stateListArray = respObject.getJSONArray("content");
                        if (stateListArray.length() > 0) {
                            txtCurrentNoSubsAvail.setVisibility(View.GONE);
                            for (int index = 0; index < stateListArray.length(); index++) {
                                GroupChildBean groupChildBean = new GroupChildBean();
                                JSONObject content = stateListArray.getJSONObject(index);
                                groupChildBean.setStrId(content.getString("_id"));
                                groupChildBean.setStrTitle(content.getString("subj_name"));
                                groupChildBean.setStrLongName(content.getString("long_name"));
                                groupChildBean.setStrShortName(content.getString("short_name"));
                                groupChildBean.setStrImages(getResources().getBoolean(R.bool.isTablet) ? content.getString("tab_img") : content.getString("mob_img"));
                                groupChildBean.setStrRemainingHours(content.getString("remaining_hours") + " " + getString(R.string.hours) + "\n" + getString(R.string.remaining));
                                groupChildBean.setStrRemainingDays(content.getString("remaining_days") + " " + getString(R.string.days) + "\n" + getString(R.string.remaining));
                                currentSubsList.add(groupChildBean);
                            }
                            setAdapter(lastVisibleCount);
                        }


                    } else if (sStatus.equals("00")) {

                        customAlert.singleLoginAlertLogout();
                    } else {

                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
//                AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
            }
        });

    }

    private void prepareOldSubscription() {

        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("skip", "0");
        params.put("limit", "10");
        params.put("page", "" + page);
        mRequest.makeServiceRequest(IConstant.oldSubscription, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------oldSubscription Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    JSONObject respObject = object.getJSONObject("response");
                    sStatus = object.getString("status");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1")) {
                        totalSubsCount = Integer.parseInt(respObject.getString("total"));
                        JSONArray stateListArray = respObject.getJSONArray("content");
                        if (stateListArray.length() > 0) {
                            txtOldNoSubsAvail.setVisibility(View.GONE);
                            for (int index = 0; index < stateListArray.length(); index++) {
                                GroupChildBean groupChildBean = new GroupChildBean();
                                JSONObject content = stateListArray.getJSONObject(index);
                                groupChildBean.setStrId(content.getString("_id"));
                                groupChildBean.setStrTitle(content.getString("subj_name"));
                                groupChildBean.setStrLongName(content.getString("long_name"));
                                groupChildBean.setStrShortName(content.getString("short_name"));
                                groupChildBean.setStrImages(getResources().getBoolean(R.bool.isTablet) ? content.getString("tab_img") : content.getString("mob_img"));
                                groupChildBean.setStrRemainingHours(content.getString("pack_hours") + " " + getString(R.string.hours));
                                groupChildBean.setStrRemainingDays(content.getString("pack_days") + " " + getString(R.string.days));
                                groupChildBean.setStrPrice(content.getString("price"));
                                oldSubsList.add(groupChildBean);
                            }
                            setOldSubAdapter(0);
                        }


                    } else if (sStatus.equals("00")) {

                        customAlert.singleLoginAlertLogout();
                    } else {

                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
//                AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
            }
        });

    }

    private void loadMoreOldSubscription(final int lastVisiblePosition) {

        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("skip", "0");
        params.put("limit", "10");
        params.put("page", "" + page);
        mRequest.makeServiceRequest(IConstant.oldSubscription, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------LoadMoreOldSubscription Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    JSONObject respObject = object.getJSONObject("response");
                    sStatus = object.getString("status");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1")) {

                        JSONArray stateListArray = respObject.getJSONArray("content");
                        if (stateListArray.length() > 0) {
                            txtOldNoSubsAvail.setVisibility(View.GONE);
                            for (int index = 0; index < stateListArray.length(); index++) {
                                GroupChildBean groupChildBean = new GroupChildBean();
                                JSONObject content = stateListArray.getJSONObject(index);
                                groupChildBean.setStrId(content.getString("_id"));
                                groupChildBean.setStrTitle(content.getString("subj_name"));
                                groupChildBean.setStrLongName(content.getString("long_name"));
                                groupChildBean.setStrShortName(content.getString("short_name"));
                                groupChildBean.setStrImages(getResources().getBoolean(R.bool.isTablet) ? content.getString("tab_img") : content.getString("mob_img"));
                                groupChildBean.setStrRemainingHours(content.getString("pack_hours") + " " + getString(R.string.hours) + "\n" + getString(R.string.remaining));
                                groupChildBean.setStrRemainingDays(content.getString("pack_days") + " " + getString(R.string.days) + "\n" + getString(R.string.remaining));
                                groupChildBean.setStrPrice(content.getString("price"));
                                currentSubsList.add(groupChildBean);
                            }
                            setOldSubAdapter(lastVisiblePosition);
                        }


                    } else if (sStatus.equals("00")) {

                        customAlert.singleLoginAlertLogout();
                    } else {

                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
//                AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
            }
        });

    }

    private void setAdapter(int lastVisiblePosition) {
        profileSubscriptionAdapter = new ProfileSubscriptionAdapter(currentSubsList, getActivity());
        if (getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(getActivity()) == 0 || DisplayOrientation.getDisplayOrientation(getActivity()) == 2))
            recyclerCurrentSubscription.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        else if (getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(getActivity()) == 1 || DisplayOrientation.getDisplayOrientation(getActivity()) == 3))
            recyclerCurrentSubscription.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        else if (!getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(getActivity()) == 0 || DisplayOrientation.getDisplayOrientation(getActivity()) == 2))
            recyclerCurrentSubscription.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        snapHelperCurrent.attachToRecyclerView(recyclerCurrentSubscription);
        recyclerCurrentSubscription.setAdapter(profileSubscriptionAdapter);
        profileSubscriptionAdapter.notifyDataSetChanged();
        if (lastVisiblePosition > 0)
            recyclerCurrentSubscription.scrollToPosition(lastVisiblePosition);

       // ScreenHeightSet();
        expand(recyclerCurrentSubscription);

    }


    private void ScreenHeightSet() {

        try {


            if (currentSubsList.size() > 2) {
                /*DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                ViewGroup.LayoutParams params = recyclerCurrentSubscription.getLayoutParams();
                Log.e("Height",""+(int) Math.round(displayMetrics.heightPixels / displayMetrics.density));
                Log.e("HeightDevided",""+(int) Math.round(displayMetrics.heightPixels / displayMetrics.density/3));
                params.height = getResources().getBoolean(R.bool.isTablet)?(int) Math.round(displayMetrics.heightPixels / displayMetrics.density/3) : (int) Math.round(displayMetrics.heightPixels / displayMetrics.density/1.2);
                recyclerCurrentSubscription.setLayoutParams(params);*/

                ViewGroup.LayoutParams params = recyclerCurrentSubscription.getLayoutParams();
                params.height = (int) getResources().getDimension(R.dimen._210sdp);
                recyclerCurrentSubscription.setLayoutParams(params);
            }

        } catch (Exception e) {

            Log.e("Excpetion", e.toString());
        }
    }

    private void setOldSubAdapter(int lastVisiblePosition) {

        profileOldSubscriptionAdapter = new ProfileOldSubscriptionAdapter(oldSubsList, getActivity());
        if (getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(getActivity()) == 0 || DisplayOrientation.getDisplayOrientation(getActivity()) == 2))
            recyclerOldSubscription.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        else if (getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(getActivity()) == 1 || DisplayOrientation.getDisplayOrientation(getActivity()) == 3))
            recyclerOldSubscription.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        else if (!getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(getActivity()) == 0 || DisplayOrientation.getDisplayOrientation(getActivity()) == 2))
            recyclerOldSubscription.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        snapHelper.attachToRecyclerView(recyclerOldSubscription);
        recyclerOldSubscription.setAdapter(profileOldSubscriptionAdapter);
        profileOldSubscriptionAdapter.notifyDataSetChanged();
        if (lastVisiblePosition > 0)
            recyclerOldSubscription.scrollToPosition(lastVisiblePosition);


        OldSubscriptionScreenHeightSet();

    }

    private void OldSubscriptionScreenHeightSet() {

        try {

            if (oldSubsList.size() > 2) {

            /*    DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                ViewGroup.LayoutParams params = recyclerOldSubscription.getLayoutParams();
                params.height = getResources().getBoolean(R.bool.isTablet)?(int) Math.round(displayMetrics.heightPixels / displayMetrics.density/3) : (int) Math.round(displayMetrics.heightPixels / displayMetrics.density/1.2);
                recyclerOldSubscription.setLayoutParams(params);*/

                ViewGroup.LayoutParams params = recyclerOldSubscription.getLayoutParams();
                params.height = (int) getResources().getDimension(R.dimen._210sdp);
                recyclerOldSubscription.setLayoutParams(params);
            }


        } catch (Exception e) {

            Log.e("Excpetion", e.toString());
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlCurrentSubsArrow:
                if (recyclerCurrentSubscription.getVisibility() == View.VISIBLE) {
                    //  setCurrentSubsHide();

                    collapse(recyclerCurrentSubscription);

                } else {
                    expand(recyclerCurrentSubscription);

                   /* setCurrentSubsVisible();
                    setOldSubsHide();
                    setWalletHide();*/
                }
                break;

            case R.id.current_sub_arrow_layout:

                if (recyclerCurrentSubscription.getVisibility() == View.VISIBLE) {

                    collapse(recyclerCurrentSubscription);

                } else {
                    expand(recyclerCurrentSubscription);
                }

                break;


            case R.id.rlOldSubscription:
                if (recyclerOldSubscription.getVisibility() == View.VISIBLE) {
                    // setOldSubsHide();
                    collapse(recyclerOldSubscription);

                } else {

                    expand(recyclerOldSubscription);
                   /* setOldSubsVisible();
                    setWalletHide();
                    setCurrentSubsHide();*/
                }
                break;

            case R.id.old_sub_arrow_layout:

                if (recyclerOldSubscription.getVisibility() == View.VISIBLE) {

                    collapse(recyclerOldSubscription);

                } else {

                    expand(recyclerOldSubscription);

                }

                break;

            case R.id.rlWallet:

                if (llWalletBalance.getVisibility() == View.VISIBLE) {
                    // setWalletHide();
                    collapse(llWalletBalance);
                } else {
                    expand(llWalletBalance);

                    setWalletVisible();
                    setOldSubsHide();
                    setCurrentSubsHide();
                }
                break;

            case R.id.wallet_arrow_layout:

                if (llWalletBalance.getVisibility() == View.VISIBLE) {
                    // setWalletHide();
                    collapse(llWalletBalance);

                } else {

                    expand(llWalletBalance);
                    setWalletVisible();
                    setOldSubsHide();
                    setCurrentSubsHide();
                }

                break;

            case R.id.bottom_llWallet:

                VisibilityChecking(bottom_llWallet);
                expand(llWalletBalance);

                break;

            case R.id.bottom_old_subscription_layout:

                VisibilityChecking(bottom_old_subscription_layout);
                expand(recyclerOldSubscription);

                break;

            case R.id.imgMyProfileCancel:
                constrainEditView.setVisibility(View.GONE);
                llAddress.setVisibility(View.VISIBLE);
                setSpinnerSelectedPosition();
                MyProfileData.currentView = 1;
                break;


            case R.id.rlMyProfileSubmit:
                MyProfileData.currentView = 1;
                if (etFirstName.getText().toString().isEmpty())
                    AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.firstNameAlert));
                else if (etLastName.getText().toString().isEmpty())
                    AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.lastNameAlert));
                else if (etAddressOne.getText().toString().isEmpty())
                    AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.address1Alert));
                else if (etAddressTwo.getText().toString().isEmpty())
                    AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.address2Alert));
                else if (txtCountryHint.getText().toString().equalsIgnoreCase("Country"))
                    AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.countrySelectAlert));
                else if (txtStateHint.getText().toString().equalsIgnoreCase("State"))
                    AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.stateSelectAlert));
                else if (txtCityHint.getText().toString().equalsIgnoreCase("City"))
                    AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.citySelectAlert));
                else if (etPinCode.getText().toString().isEmpty())
                    AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.pinAlert));
                else if (etPinCode.getText().toString().length() < 4)
                    AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), getString(R.string.ValidPinAlert));
                else
                    updateProfile();

                break;

            case R.id.imgProfileEdit:
                if (getResources().getBoolean(R.bool.isTablet)) {
                    constrainEditView.setVisibility(View.VISIBLE);
                    llAddress.setVisibility(View.GONE);
                    setSpinnerSelectedPosition();
                    MyProfileData.currentView = 2;
                } else
                    startActivity(new Intent(getActivity(), EditProfileActivity.class));
                break;
        }
    }

    private void setWalletVisible() {
        MyProfileData.strWallet = "open";
        llWalletBalance.setVisibility(View.VISIBLE);
        Picasso.with(getActivity()).load(R.drawable.arrow_top_gradient).into(imgWalletArrow);
        imgWalletText.setVisibility(View.VISIBLE);
        txtWallet.setVisibility(View.GONE);
    }

    private void setWalletHide() {
        MyProfileData.strWallet = "close";
        llWalletBalance.setVisibility(View.GONE);
        imgWalletText.setVisibility(View.GONE);
        txtWallet.setVisibility(View.VISIBLE);

        Picasso.with(getActivity()).load(R.drawable.down_arrow_gradient).into(imgWalletArrow);
    }

    private void setOldSubsVisible() {
        MyProfileData.strOldSubs = "open";
        imgOldSubsText.setVisibility(View.VISIBLE);
        txtOldSubs.setVisibility(View.GONE);
        recyclerOldSubscription.setVisibility(View.VISIBLE);
        if (oldSubsList.size() == 0)
            txtOldNoSubsAvail.setVisibility(View.VISIBLE);
        Picasso.with(getActivity()).load(R.drawable.arrow_top_gradient).into(imgOldSUBArrow);
    }

    private void setOldSubsHide() {
        MyProfileData.strOldSubs = "close";
        imgOldSubsText.setVisibility(View.GONE);
        txtOldSubs.setVisibility(View.VISIBLE);
        recyclerOldSubscription.setVisibility(View.GONE);
        txtOldNoSubsAvail.setVisibility(View.GONE);
        Picasso.with(getActivity()).load(R.drawable.down_arrow_gradient).into(imgOldSUBArrow);
    }

    private void setCurrentSubsHide() {
        MyProfileData.strCurrentSubs = "close";
        recyclerCurrentSubscription.setVisibility(View.GONE);
        txtCurrentNoSubsAvail.setVisibility(View.GONE);
        Picasso.with(getActivity()).load(R.drawable.down_arrow_gradient).into(imgCurrentSUBArrow);
        imgCurrentSubsText.setVisibility(View.GONE);
        imgCurrentSubsText.setVisibility(View.GONE);
        txtCurrentSub.setVisibility(View.VISIBLE);
    }

    private void setCurrentSubsVisible() {
        MyProfileData.strCurrentSubs = "open";
        recyclerCurrentSubscription.setVisibility(View.VISIBLE);
        if (currentSubsList.size() == 0)
            txtCurrentNoSubsAvail.setVisibility(View.VISIBLE);
        imgCurrentSubsText.setVisibility(View.VISIBLE);
        txtCurrentSub.setVisibility(View.GONE);
        Picasso.with(getActivity()).load(R.drawable.arrow_top_gradient).into(imgCurrentSUBArrow);
    }

    private void updateProfile() {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("first_name", etFirstName.getText().toString());
        params.put("last_name", etLastName.getText().toString());
        params.put("line_1", etAddressOne.getText().toString());
        params.put("line_2", etAddressTwo.getText().toString());
        params.put("city", txtCityHint.getText().toString());
        params.put("city_id", MyProfileData.strCityId);
        params.put("state", txtStateHint.getText().toString());
        params.put("state_id", MyProfileData.strStateId);
        params.put("country", txtCountryHint.getText().toString());
        params.put("country_id", MyProfileData.strCountryId);
        params.put("pincode", etPinCode.getText().toString());


        mRequest.makeServiceRequest(IConstant.updateProfileAddress, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------updateProfileAddress Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1")) {
                        loader.dismissLoader();
                        MyProfileData.clearData();
                        if (getResources().getBoolean(R.bool.isTablet)) {
                            constrainEditView.setVisibility(View.GONE);
                            llAddress.setVisibility(View.VISIBLE);
                            MyProfileData.currentView = 1;
                        }
                        sessionManager.updateName(etFirstName.getText().toString(), etLastName.getText().toString());
                        getUserProfileData();
                    } else if (sStatus.equals("00")) {
                        loader.dismissLoader();
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else {

                        loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), errorMessage);
            }
        });
    }

    private void setSpinnerSelectedPosition() {
        etAddressOne.setText(strAddress1);
        etAddressTwo.setText(strAddress2);
        etPinCode.setText(strPinCode);
        etLastName.setText(sessionManager.getUserDetails().get(SessionManager.KEY_LNAME));
        etFirstName.setText(sessionManager.getUserDetails().get(SessionManager.KEY_FNAME));

    }

    public void onDestroy() {
        super.onDestroy();
        if (getResources().getBoolean(R.bool.isTablet)) {

        }
    }

    private void enableDisablesubmitButton() {
        if (!etFirstName.getText().toString().isEmpty() && !etLastName.getText().toString().isEmpty() && !etAddressTwo.getText().toString().isEmpty() && !etAddressOne.getText().toString().isEmpty() && !etPinCode.getText().toString().isEmpty() && !txtCountryHint.getText().toString().equalsIgnoreCase("Country") && !txtStateHint.getText().toString().equalsIgnoreCase("State") && !txtCityHint.getText().toString().equalsIgnoreCase("City")) {
            rlMyProfileSubmit.setBackground(getResources().getDrawable(R.drawable.login_gradient));
        } else {
            rlMyProfileSubmit.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
        }
    }

    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
        snapHelperCurrent = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
    }


    public void expand(final View v) {

        VisibilityChecking(v);

        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? WindowManager.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Expansion speed of 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);

        if (v.getId() == R.id.recyclerOldSubscription) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (oldSubsList.size() > 2) {

                        bottom_llWallet.setVisibility(View.VISIBLE);
                    }

                }
            }, 300);

            Picasso.with(getActivity()).load(R.drawable.arrow_top_gradient).into(imgOldSUBArrow);

            txtOldSubs.setVisibility(View.GONE);
            imgOldSubsText.setVisibility(View.VISIBLE);

            if (oldSubsList.size() == 0) {

                txtOldNoSubsAvail.setVisibility(View.VISIBLE);
            }

        } else if (v.getId() == R.id.recyclerCurrentSubscription) {


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (currentSubsList.size() > 3) {

                        bottom_old_subscription_layout.setVisibility(View.VISIBLE);
                        bottom_llWallet.setVisibility(View.VISIBLE);
                    }

                }
            }, 300);

            Picasso.with(getActivity()).load(R.drawable.arrow_top_gradient).into(imgCurrentSUBArrow);

            txtCurrentSub.setVisibility(View.GONE);
            imgCurrentSubsText.setVisibility(View.VISIBLE);

            if (currentSubsList.size() == 0) {

                txtCurrentNoSubsAvail.setVisibility(View.VISIBLE);
            }

        } else if (v.getId() == R.id.llWalletBalance) {

            Picasso.with(getActivity()).load(R.drawable.arrow_top_gradient).into(imgWalletArrow);
            imgWalletText.setVisibility(View.VISIBLE);
            txtWallet.setVisibility(View.GONE);

        }

    }

    public void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Collapse speed of 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);

        if (v.getId() == R.id.recyclerOldSubscription) {

            bottom_llWallet.setVisibility(View.INVISIBLE);
            bottom_old_subscription_layout.setVisibility(View.GONE);
            txtOldSubs.setVisibility(View.VISIBLE);
            imgOldSubsText.setVisibility(View.GONE);
            Picasso.with(getActivity()).load(R.drawable.down_arrow_gradient).into(imgOldSUBArrow);

        } else if (v.getId() == R.id.recyclerCurrentSubscription) {

            bottom_llWallet.setVisibility(View.INVISIBLE);
            bottom_old_subscription_layout.setVisibility(View.GONE);
            txtCurrentSub.setVisibility(View.VISIBLE);
            imgCurrentSubsText.setVisibility(View.GONE);
            Picasso.with(getActivity()).load(R.drawable.down_arrow_gradient).into(imgCurrentSUBArrow);

        } else if (v.getId() == R.id.llWalletBalance) {

            bottom_llWallet.setVisibility(View.INVISIBLE);
            bottom_old_subscription_layout.setVisibility(View.GONE);
            imgWalletText.setVisibility(View.GONE);
            txtWallet.setVisibility(View.VISIBLE);
            Picasso.with(getActivity()).load(R.drawable.down_arrow_gradient).into(imgWalletArrow);
        }

    }


    private void VisibilityChecking(final View v){

        try{

            if (v.getId() == R.id.recyclerCurrentSubscription) {

                if(recyclerOldSubscription.getVisibility()==View.VISIBLE){

                    collapse(recyclerOldSubscription);

                } else if(llWalletBalance.getVisibility()==View.VISIBLE){

                    collapse(llWalletBalance);
                }

            } else if (v.getId() == R.id.recyclerOldSubscription) {

                if(recyclerCurrentSubscription.getVisibility()==View.VISIBLE){

                    collapse(recyclerCurrentSubscription);

                } else if(llWalletBalance.getVisibility()==View.VISIBLE){

                    collapse(llWalletBalance);
                }

            } else if (v.getId() == R.id.llWalletBalance) {

                if(recyclerCurrentSubscription.getVisibility()==View.VISIBLE){

                    collapse(recyclerCurrentSubscription);

                } else if(recyclerOldSubscription.getVisibility()==View.VISIBLE){

                    collapse(recyclerOldSubscription);
                }

            } else if(v.getId()==R.id.bottom_llWallet){

                if(recyclerCurrentSubscription.getVisibility()==View.VISIBLE){

                    collapse(recyclerCurrentSubscription);

                } else if(recyclerOldSubscription.getVisibility()==View.VISIBLE){

                    collapse(recyclerOldSubscription);
                }
            } else if(v.getId()==R.id.bottom_old_subscription_layout){

                if(recyclerCurrentSubscription.getVisibility()==View.VISIBLE){

                    collapse(recyclerCurrentSubscription);

                } else if(llWalletBalance.getVisibility()==View.VISIBLE){

                    collapse(llWalletBalance);
                }
            }

        }catch(Exception e){

            Log.e("Exception",e.toString());
        }
    }

}
