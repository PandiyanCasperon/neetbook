package com.app.neetbook.View.SideMenu;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.app.neetbook.Adapter.ArticleRecyclerAdapter;
import com.app.neetbook.Adapter.sideMenuContent.SubscriptionArticleAdapter;
import com.app.neetbook.Interfaces.ArticleContentSection;
import com.app.neetbook.Interfaces.ArticleContentSummaryGet;
import com.app.neetbook.Model.ArticleContent.ArticleChildItem;
import com.app.neetbook.Model.ArticleContent.ArticleItem;
import com.app.neetbook.Model.ArticleContentChildModel;
import com.app.neetbook.Model.ArticleContentmodel;
import com.app.neetbook.Model.sidemenuFromContent.SubscriptionArticleBody;
import com.app.neetbook.R;
import com.app.neetbook.StickeyHeader.StickyLinearLayoutManager;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.RecyclerSectionItemDecoration;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.Thread.AppExecutors;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.OkHttpClient;
import okhttp3.Response;

import static com.app.neetbook.serviceRequest.IConstant.BaseUrl;

public class SubscriptionArticleFragment extends Fragment {

    private ArrayList<Object> subscriptionPointList = new ArrayList<>();
    private SubscriptionArticleAdapter categoryAdapter;
    //private RecyclerView articleItemList;
    private AppCompatImageView articleImage;
    private String head_id = "", strSubId = "", strLongName = "";
    private int CurrentPosition = 0;
    Loader loader;
    private int page = 1;
    int loadMoreEnable = 0;
    private int totalSubsCount;
    SessionManager sessionManager;
    WebView webView;
    String storeupordown = "", CurrentPage_Url = "", CurrentPageWebUrl = "";
    private CustomAlert customAlert;
    TextView txt_subscription_article_header;
    private String StrSummary = "", StrCurrentTitle = "";
    Typeface tf;
    Drawable mIcon;
    List<Object> items = new ArrayList<>();
    List<Object> itemsLoadMore = new ArrayList<>();
    AppExecutors appExecutors;
    //ArticleContentSectionAdaptewr articleContentSectionAdaptewr;
    private ArrayList<ArticleContentSection> articleSectionArrayList = new ArrayList<>();
    ArticleRecyclerAdapter articleRecyclerAdapter;
    private ArticleContentSummaryGet articleContentSummaryGet;
    int count=0;


    // private LinearLayout place_holder_layout;
    //  private ShimmerFrameLayout shimmer_view_container;
    //  private WebView webView;

    public SubscriptionArticleFragment(String head_id, String strSubId, String strLongName, int CurrentPosition, String url,int count) {
        // Required empty public constructor
        this.head_id = head_id;
        this.strSubId = strSubId;
        this.strLongName = strLongName;
        this.CurrentPosition = CurrentPosition;
        this.CurrentPage_Url = url;
        this.count=count;
    }

    public SubscriptionArticleFragment() {

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            appExecutors = new AppExecutors();
            sessionManager = new SessionManager(getActivity());
            customAlert = new CustomAlert(getActivity());
            loader = new Loader(getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }
//    tf= Typeface.createFromAsset(getContext().getAssets(), "fonts/ProximaNovaBold.otf");

    }

    public static SubscriptionArticleFragment newInstance() {
        SubscriptionArticleFragment fragment = new SubscriptionArticleFragment("", "", "", 0, "",0);
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_subscription_article, container, false);
        init(rootView);
        return rootView;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void init(View rootView) {

        txt_subscription_article_header = rootView.findViewById(R.id.txt_subscription_article_header);
        // articleItemList = rootView.findViewById(R.id.recycler_view_article);
        articleImage = rootView.findViewById(R.id.imageViewArticle);
        webView = rootView.findViewById(R.id.webView);
        webView.getSettings().setTextZoom(100); // 100% is maximum here
        webView.getSettings().setJavaScriptEnabled(true); // enable javascript

        /*if (count==0){
            webView.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    return(event.getAction() == MotionEvent.ACTION_MOVE);
                }
            });
        }else
            webView.setOnTouchListener(null);*/

        webView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
        webView.setLongClickable(false);
        webView.setHapticFeedbackEnabled(false);

           /* webView.setWebViewClient(new WebViewClient() {

            // Handle API until level 21
            @SuppressWarnings("deprecation")
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {

                return getNewResponse(url);
            }

            // Handle API 21+
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {

                String url = request.getUrl().toString();

                return getNewResponse(url);
            }

            private WebResourceResponse getNewResponse(String url) {

                try {
                    OkHttpClient httpClient = new OkHttpClient();

                    okhttp3.Request request = new okhttp3.Request.Builder()
                            .url(url.trim())
                            .addHeader("userId", "5f7f099957f5ea75437e021d") // Example header
                            .addHeader("authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIwMTAwODYwIDogM2E3MmY2YjE2ZGJhMjY2MyIsImlhdCI6MTYwNDQ5MDAzNX0.g-Sh-t53f7YJxhIM1nzT6pxHcrEaGixkOMVFWewyDj4") // Example header
                            .build();

                    Response response = httpClient.newCall(request).execute();
                    System.out.println("url loading midle");
                    assert response.body() != null;
                    return new WebResourceResponse(
                            null,
                            response.header("content-encoding", "utf-8"),
                            response.body().byteStream()
                    );

                } catch (Exception e) {
                    return null;
                }

            }
        });*/
      /*  webView.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

            }

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });*/

        CurrentPageWebUrl = BaseUrl + CurrentPage_Url + "?mode=";
        if (sessionManager.isNightModeEnabled())
            CurrentPageWebUrl += "dark" + "&size=" + sessionManager.getFontSize();
        else

            CurrentPageWebUrl += "" + "&size=" + sessionManager.getFontSize();
        webView.setHorizontalScrollBarEnabled(true);
        //webView .loadUrl("https://en.wikipedia.org/wiki/National_Eligibility_cum_Entrance_Test_(Undergraduate)");
        webView.loadUrl(CurrentPageWebUrl);
        System.out.println("CurrentPageWebUrl--->"+CurrentPageWebUrl);

        if(count == 0)
        {
            webView.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
            webView.requestLayout();
            webView.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    return(event.getAction() == MotionEvent.ACTION_MOVE);
                }
            });

        }
        else
        {
            webView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            webView.requestLayout();
            webView.setOnTouchListener(null);
        }

       // System.out.println("url loading end");
        //    webView = rootView.findViewById(R.id.webView);
        //  place_holder_layout = rootView.findViewById(R.id.place_holder_layout);
        //   shimmer_view_container = rootView.findViewById(R.id.shimmer_view_container);
//        shimmer_view_container.startShimmer();



        txt_subscription_article_header.setText(strLongName);
        //   listener();
        setFontSize();
        articleImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ArticleDetailActivity.class).putExtra("title", strLongName).putExtra("summary", StrSummary));
            }
        });
        page = 1;
        // getArticleContent();
        new AsyncLoad().execute();

    }

    private void listener() {
   /* articleItemList.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
        int totalItemCount = layoutManager.getItemCount();
        int lastVisible = layoutManager.findLastVisibleItemPosition();
        if (layoutManager.findFirstVisibleItemPosition() >= 0 && !getActivity().getResources().getBoolean(R.bool.isTablet)) {
          //articleContentSectionAdaptewr.setTopItemView(articleSectionArrayList.get(layoutManager.findFirstVisibleItemPosition()).titile(), layoutManager.findFirstVisibleItemPosition());
        }

        System.out.println("Collapse---"+totalItemCount);
        Log.e("RecyclerScroll---", String.valueOf(totalItemCount));
        Log.e("RecyclerScroll---", String.valueOf(lastVisible));


        if ((lastVisible == totalItemCount - 1 || lastVisible == totalItemCount - 2) && totalSubsCount > items.size()/2 && loadMoreEnable == 0) {
          page = page + 1;
          Log.e("loadMoreSubjects()", "Called");
          loadMoreEnable = 1;
          loadMoreArticle();
        }
      }
    });*/
    }




    private void setFontSize() {
        if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
            txt_subscription_article_header.setTextAppearance(getActivity(), R.style.textViewSmallContentTitle);
        } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
            txt_subscription_article_header.setTextAppearance(getActivity(), R.style.textViewMediumContentTitle);
        } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
            txt_subscription_article_header.setTextAppearance(getActivity(), R.style.textViewLargeContentTitle);
        }

        txt_subscription_article_header.setTypeface(tf);


    }

    private void setupRecyclerView() {

        try {
            if (CurrentPosition == 0 && articleContentSummaryGet != null)
                articleContentSummaryGet.onArticleContentChanged(StrSummary, CurrentPosition);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(subscriptionPointList.size()>0)
        {
          SubscriptionArticleBody subscriptionArticleBody = (SubscriptionArticleBody) subscriptionPointList.get(0);
          StrSummary = subscriptionArticleBody.summary;
        }
   /*     articleItemList.setLayoutManager(new LinearLayoutManager(getActivity(),
                RecyclerView.VERTICAL, false));
        SubscriptionArticleFragment.ItemOffsetDecoration itemDecoration = new SubscriptionArticleFragment.ItemOffsetDecoration(
                getActivity(),
                R.dimen._1sdp);
        articleItemList.addItemDecoration(itemDecoration);
        categoryAdapter = new SubscriptionArticleAdapter(getActivity(), subscriptionPointList);
        articleItemList.setAdapter(categoryAdapter);*/
        appExecutors.mainThread().execute(new Runnable() {
            @Override
            public void run() {

                articleRecyclerAdapter = new ArticleRecyclerAdapter();
                articleRecyclerAdapter.setDataList(items, getActivity());
                StickyLinearLayoutManager layoutManager = new StickyLinearLayoutManager(getActivity(), articleRecyclerAdapter) {
                    @Override
                    public boolean isAutoMeasureEnabled() {
                        return true;
                    }

                    @Override
                    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                        RecyclerView.SmoothScroller smoothScroller = new TopSmoothScroller(recyclerView.getContext());
                        smoothScroller.setTargetPosition(position);
                        startSmoothScroll(smoothScroller);
                    }

                    class TopSmoothScroller extends LinearSmoothScroller {

                        TopSmoothScroller(Context context) {
                            super(context);
                        }

                        @Override
                        public int calculateDtToFit(int viewStart, int viewEnd, int boxStart, int boxEnd, int snapPreference) {
                            return boxStart - viewStart;
                        }
                    }
                };
                layoutManager.elevateHeaders(5);
                //articleItemList.setLayoutManager(layoutManager);
                //articleItemList.setAdapter(articleRecyclerAdapter);

                layoutManager.setStickyHeaderListener(new StickyLinearLayoutManager.StickyHeaderListener() {
                    @Override
                    public void headerAttached(View headerView, int adapterPosition) {
                        Log.d("StickyHeader", "Header Attached : " + adapterPosition);
                    }

                    @Override
                    public void headerDetached(View headerView, int adapterPosition) {
                        Log.d("StickyHeader", "Header Detached : " + adapterPosition);
                    }
                });
            }
        });


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                //  place_holder_layout.setVisibility(View.GONE);

            }
        }, 2000);


    }

    private RecyclerSectionItemDecoration.SectionCallback getSectionCallback(final ArrayList<Object> people) {
        return new RecyclerSectionItemDecoration.SectionCallback() {
            @Override
            public boolean isSection(int position) {
                SubscriptionArticleBody itemBody = (SubscriptionArticleBody) people.get(position);

                return itemBody.titile.equalsIgnoreCase("Yes");
            }

            @Override
            public String getSectionHeader(int position) {
                SubscriptionArticleBody itemBody = (SubscriptionArticleBody) people.get(position);
                return itemBody.titile;
            }
        };
    }

    public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {

        private int mItemOffset;

        public ItemOffsetDecoration(int itemOffset) {
            mItemOffset = itemOffset;
        }

        public ItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
            this(context.getResources().getDimensionPixelSize(itemOffsetId));
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            if (parent.getChildAdapterPosition(view) == 0) {
                outRect.top = mItemOffset;
            }
            outRect.left = getContext().getResources().getDimensionPixelSize(R.dimen._1sdp);
            outRect.right = getContext().getResources().getDimensionPixelSize(R.dimen._1sdp);
            outRect.bottom = mItemOffset;
        }
    }

    private void getArticleContent() {

        //  place_holder_layout.setVisibility(View.VISIBLE);

        appExecutors.networkIO().execute(new Runnable() {
            @Override
            public void run() {

                subscriptionPointList.clear();
                articleSectionArrayList.clear();
                items.clear();
                ServiceRequest mRequest = new ServiceRequest(getActivity());
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
                params.put("subject_id", strSubId);
                params.put("head_id", head_id);
                params.put("page", "1");
                params.put("fontsize", "15.0px / 17.0px/ 19.0px");
                if (sessionManager.isNightModeEnabled())
                    params.put("themecolor", "303030");
                else
                    params.put("themecolor", "");
                mRequest.makeServiceRequest(IConstant.articleContent, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
                    @Override
                    public void onCompleteListener(String response) {
                        System.out.println("------------articleContent Response----------------" + response);
                        String sStatus = "", template = "";

                        try {
                            //  place_holder_layout.setVisibility(View.GONE);
                            JSONObject obj = new JSONObject(response);
                            sStatus = obj.getString("status");
                            if (sStatus.equals("1")) {
                                template = obj.getString("template");
                                //  webView.loadData(template, "text/html", "UTF-8");
                                //     webView.loadDataWithBaseURL(null, template, "text/html", "UTF-8", null);

                            }



              /*if (sStatus.equalsIgnoreCase("1")) {
                JSONObject object = obj.getJSONObject("response");
                totalSubsCount = object.getInt("atotal");
                JSONArray articlesArray = object.getJSONArray("articles");
                if(articlesArray.length()>0) {
                  //llEmpty.setVisibility(View.GONE);
                  for(int i=0;i<articlesArray.length();i++)
                  {
                    JSONObject jsonObject = articlesArray.getJSONObject(i);
                    ArrayList<String> images = new ArrayList<>();
                    ArticleContentmodel articleContentmodel = new ArticleContentmodel(i);
                    articleContentmodel.setTitile(jsonObject.getString("title"));
                    articleContentmodel.setIsHeader("Yes");
                    articleSectionArrayList.add(articleContentmodel);

                    items.add(new ArticleItem(jsonObject.getString("title"),jsonObject.getString("summary")));

                    if(jsonObject.getJSONArray("image").length()>0) {

                      for (int j = 0; j < jsonObject.getJSONArray("image").length(); j++) {
//                    JSONObject imagejs = ;
                        images.add(jsonObject.getJSONArray("image").getJSONObject(j).getString("url"));
                      }
                    }
                    ArticleContentChildModel articleContentChildModel = new ArticleContentChildModel(i);
                    articleContentChildModel.setPoints(jsonObject.getString("body"));
                    articleContentChildModel.setStrImages(images);
                    articleContentChildModel.setIsHeader("No");
                    articleContentChildModel.setTitile(jsonObject.getString("title"));
                    articleContentChildModel.setSummary(jsonObject.getString("summary"));
                    articleSectionArrayList.add(articleContentChildModel);
                    StrSummary = jsonObject.getString("summary");
                    items.add(new ArticleChildItem(jsonObject.getString("title"),"No",jsonObject.getString("summary"),jsonObject.getString("body"),i,images,false));
                    *//*subscriptionPointList
                            .add(new SubscriptionArticleBody(jsonObject.getString("title"),jsonObject.getString("body"),jsonObject.getString("summary"),images,"Yes"));*//*

                  }
                  loadMoreEnable = 0;


                 // setupRecyclerView();


                } else{

                  place_holder_layout.setVisibility(View.GONE);
                }

              } else if(sStatus.equals("00")){
                customAlert.singleLoginAlertLogout();
              }else if(sStatus.equalsIgnoreCase("01")){
                customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
              } else {
                customAlert.showAlertOk(getString(R.string.alert_oops),message);
              }*/

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onErrorListener(String errorMessage) {
                        // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
                    }
                });


            }
        });

    }

    private void loadMoreArticle() {

        itemsLoadMore = new ArrayList<>();
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("subject_id", strSubId);
        params.put("head_id", head_id);
        params.put("page", "" + page);
        params.put("skip", "0");
        params.put("limit", "10");
        mRequest.makeServiceRequest(IConstant.articleContent, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------Load more articleContent Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalSubsCount = object.getInt("atotal");
                        JSONArray articlesArray = object.getJSONArray("articles");
                        if (articlesArray.length() > 0) {
                            //llEmpty.setVisibility(View.GONE);
                            for (int i = 0; i < articlesArray.length(); i++) {
                                JSONObject jsonObject = articlesArray.getJSONObject(i);
                                ArrayList<String> images = new ArrayList<>();
                                ArticleContentmodel articleContentmodel = new ArticleContentmodel(i);
                                articleContentmodel.setTitile(jsonObject.getString("title"));
                                articleContentmodel.setIsHeader("Yes");
                                articleSectionArrayList.add(articleContentmodel);

                                itemsLoadMore.add(new ArticleItem(jsonObject.getString("title"), jsonObject.getString("summary")));

                                if (jsonObject.getJSONArray("image").length() > 0) {

                                    for (int j = 0; j < jsonObject.getJSONArray("image").length(); j++) {
//                    JSONObject imagejs = ;
                                        images.add(jsonObject.getJSONArray("image").getJSONObject(j).getString("url"));
                                    }
                                }
                                ArticleContentChildModel articleContentChildModel = new ArticleContentChildModel(i);
                                articleContentChildModel.setPoints(jsonObject.getString("body"));
                                articleContentChildModel.setStrImages(images);
                                articleContentChildModel.setIsHeader("No");
                                articleContentChildModel.setTitile(jsonObject.getString("title"));
                                articleContentChildModel.setSummary(jsonObject.getString("summary"));
                                articleSectionArrayList.add(articleContentChildModel);

                                itemsLoadMore.add(new ArticleChildItem(jsonObject.getString("title"), "No", jsonObject.getString("summary"), jsonObject.getString("body"), i, images, false));
                    /*subscriptionPointList
                            .add(new SubscriptionArticleBody(jsonObject.getString("title"),jsonObject.getString("body"),jsonObject.getString("summary"),images,"Yes"));*/

                            }
                            loadMoreEnable = 0;

                            articleRecyclerAdapter.addDataList(itemsLoadMore);


                        }

                    } else if (sStatus.equals("00")) {
                        customAlert.singleLoginAlertLogout();
                    } else if (sStatus.equalsIgnoreCase("01")) {
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops), message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops), message);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ArticleContentSummaryGet) {
            articleContentSummaryGet = (ArticleContentSummaryGet) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        articleContentSummaryGet = null;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            try {
                if (articleContentSummaryGet != null)
                    articleContentSummaryGet.onArticleContentChanged(StrSummary, CurrentPosition);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    class AsyncLoad extends AsyncTask<String, String, String> {

        InputStream inputStream;
        HttpURLConnection urlConnection;
        byte[] outputBytes;
        String query;
        String ResponseData;

        public AsyncLoad() {

        }

        @Override
        protected String doInBackground(String... params) {

            // Send data  params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
            //        params.put("subject_id",strSubId);
            //        params.put("head_id",head_id);
            //        params.put("page",""+page);
            //        params.put("skip","0");
            //        params.put("limit","10");
            try {
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("username", sessionManager.getUserDetails().get(SessionManager.KEY_ID))
                        .appendQueryParameter("subject_id", strSubId)
                        .appendQueryParameter("head_id", head_id)
                        .appendQueryParameter("page", "" + page)
                        .appendQueryParameter("skip", "0")
                        .appendQueryParameter("limit", "10");

                String query = builder.build().getEncodedQuery();
                /* forming th java.net.URL object */
                URL url = new URL(IConstant.articleContent);
                urlConnection = (HttpURLConnection) url.openConnection();


                /* pass post data */
                outputBytes = query.getBytes("UTF-8");

                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("authorization", sessionManager.getApiHeader().get(SessionManager.KEY_JWT));
                urlConnection.setRequestProperty("userId", sessionManager.getApiHeader().get(SessionManager.KEY_ID));
                urlConnection.connect();
                OutputStream os = urlConnection.getOutputStream();
                os.write(outputBytes);
                os.close();

                /* Get Response and execute WebService request*/
                int statusCode = urlConnection.getResponseCode();

                /* 200 represents HTTP OK */
                if (statusCode == HttpsURLConnection.HTTP_OK) {

                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                    ResponseData = convertStreamToString(inputStream);

                } else {

                    ResponseData = null;
                }


            } catch (Exception e) {

                e.printStackTrace();

            }


            return null;
        }
    }

    public static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append((line + "\n"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String upordown) {
        if (upordown.equals("downns") || upordown.equals("upps")) {
            if (upordown.equals("downns")) {
                storeupordown = "downn";

            }
            if (upordown.equals("upps")) {
                storeupordown = "upp";

            }

        }
        else if (upordown.equals("hideme"))
        {
            //Toast.makeText(getActivity(),"grtg",Toast.LENGTH_SHORT).show();
            webView.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    return(event.getAction() == MotionEvent.ACTION_MOVE);
                }
            });

        }
        else if (upordown.equals("showme"))
        {

            webView.setOnTouchListener(null);
        }

    }



    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }




}
