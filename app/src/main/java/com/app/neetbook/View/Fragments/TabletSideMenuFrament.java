package com.app.neetbook.View.Fragments;

import android.app.Activity;
import android.content.Context;
import android.opengl.Visibility;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.app.neetbook.Interfaces.OnFragmentInteractionListener;
import com.app.neetbook.Model.TabSideMenu;
import com.app.neetbook.R;
import com.app.neetbook.Utils.Data.DrawerActivityData;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.widgets.DisplayOrientation;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class TabletSideMenuFrament extends Fragment {

    TextView txtName,txtHi;
    OnFragmentInteractionListener listener;
    public TabletSideMenuFrament() {
        return;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_tablet_side_menu_frament, container, false);


        txtName = rootView.findViewById(R.id.txtName);
        txtHi = rootView.findViewById(R.id.txtHi);
        if(rootView.findViewById(R.id.txtHi) != null && (DisplayOrientation.getDisplayOrientation(getActivity()) == 0 || DisplayOrientation.getDisplayOrientation(getActivity()) == 2))
        {
            rootView.findViewById(R.id.txtHi).setVisibility(View.GONE);
            rootView.findViewById(R.id.txtName).setVisibility(View.GONE);


        }else if(DisplayOrientation.getDisplayOrientation(getActivity()) == 1 || DisplayOrientation.getDisplayOrientation(getActivity()) == 3)
        {
            SessionManager sessionManager = new SessionManager(getActivity());
            if(sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) == null|| sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("")){
                txtName.setVisibility(View.GONE);
                txtHi.setVisibility(View.GONE);
            }else {
                txtName.setText(sessionManager.getUserDetails().get(SessionManager.KEY_FNAME));
            }


        }
        ListView menuListView = (ListView) rootView.findViewById(android.R.id.list);
        SideMenuAdapter sideMenuAdapter = new SideMenuAdapter(getActivity(),generateAnimalList());
        menuListView.setAdapter(sideMenuAdapter);
        sideMenuAdapter.setSelectedIndex(DrawerActivityData.currentIndex);

        menuListView.setOnItemClickListener(onMenuItemClickListener);
        return rootView;
    }
    AdapterView.OnItemClickListener onMenuItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            TabSideMenu sideMenu = (TabSideMenu)adapterView.getItemAtPosition(i);
            if(listener!=null)
                listener.onSideMenuSelected(sideMenu,i);
        }
    };
    private List<TabSideMenu> generateAnimalList(){
        SessionManager sessionManager = new SessionManager(getActivity());
        List<TabSideMenu> sideMenus = new ArrayList<TabSideMenu>(10);

        if(getResources().getBoolean(R.bool.isTablet)) {
            TabSideMenu tabSideMenu = new TabSideMenu();
            tabSideMenu.setId("1");
            tabSideMenu.setName(getString(R.string.home));
            tabSideMenu.setImage_id_inactive(R.drawable.home_inactive);
            tabSideMenu.setImage_id_acative(R.drawable.home_active);
            sideMenus.add(tabSideMenu);
        }

        TabSideMenu tabSideMenu2 = new TabSideMenu();
        tabSideMenu2.setId("2");
        tabSideMenu2.setName(sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) != null && sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("")?getString(R.string.complete_profile):getString(R.string.my_profile));
        tabSideMenu2.setImage_id_inactive(R.drawable.complete_proile_inactive);
        tabSideMenu2.setImage_id_acative(R.drawable.complete_profile_active);
        sideMenus.add(tabSideMenu2);
        if(!getResources().getBoolean(R.bool.isTablet)) {
            TabSideMenu tabSideMenu = new TabSideMenu();
            tabSideMenu.setId("11");
            tabSideMenu.setName(getString(R.string.subscription));
            tabSideMenu.setImage_id_inactive(R.drawable.subscriptoin_inactive);
            tabSideMenu.setImage_id_acative(R.drawable.subscriptoin_inactive);
            sideMenus.add(tabSideMenu);
        }
        TabSideMenu tabSideMenu3 = new TabSideMenu();
        tabSideMenu3.setId("3");
        tabSideMenu3.setName(getString(R.string.change_pin));
        tabSideMenu3.setImage_id_inactive(R.drawable.change_pin_inactive);
        tabSideMenu3.setImage_id_acative(R.drawable.change_pin_active);
        sideMenus.add(tabSideMenu3);

        TabSideMenu tabSideMenu4 = new TabSideMenu();
        tabSideMenu4.setId("4");
        tabSideMenu4.setName(getString(R.string.change_mobile_no));
        tabSideMenu4.setImage_id_inactive(R.drawable.change_mobile_inactive);
        tabSideMenu4.setImage_id_acative(R.drawable.change_mobile_active);
        sideMenus.add(tabSideMenu4);

        TabSideMenu tabSideMenu5 = new TabSideMenu();
        tabSideMenu5.setId("5");
        tabSideMenu5.setName(getString(R.string.about_us));
        tabSideMenu5.setImage_id_inactive(R.drawable.about_us_inactive);
        tabSideMenu5.setImage_id_acative(R.drawable.about_us_active);
        sideMenus.add(tabSideMenu5);

        TabSideMenu tabSideMenu6 = new TabSideMenu();
        tabSideMenu6.setId("6");
        tabSideMenu6.setName(getString(R.string.terms_and_conditions));
        tabSideMenu6.setImage_id_inactive(R.drawable.terms_and_conditions_inactive);
        tabSideMenu6.setImage_id_acative(R.drawable.terms_and_conditions_active);
        sideMenus.add(tabSideMenu6);

        TabSideMenu tabSideMenu7 = new TabSideMenu();
        tabSideMenu7.setId("7");
        tabSideMenu7.setName(getString(R.string.help));
        tabSideMenu7.setImage_id_inactive(R.drawable.help_inactive);
        tabSideMenu7.setImage_id_acative(R.drawable.help_active);
        sideMenus.add(tabSideMenu7);

        TabSideMenu tabSideMenu8 = new TabSideMenu();
        tabSideMenu8.setId("8");
        tabSideMenu8.setName(getString(R.string.contact_us));
        tabSideMenu8.setImage_id_inactive(R.drawable.contact_us_inactive);
        tabSideMenu8.setImage_id_acative(R.drawable.contavt_us_active);
        sideMenus.add(tabSideMenu8);

        TabSideMenu tabSideMenu9 = new TabSideMenu();
        tabSideMenu9.setId("9");
        tabSideMenu9.setName(getString(R.string.feedback));
        tabSideMenu9.setImage_id_inactive(R.drawable.feedback_inactive);
        tabSideMenu9.setImage_id_acative(R.drawable.feedback_active);
        sideMenus.add(tabSideMenu9);




        TabSideMenu tabSideMenu10 = new TabSideMenu();
        tabSideMenu10.setId("10");
        tabSideMenu10.setName(getString(R.string.sign_out));
        tabSideMenu10.setImage_id_inactive(R.drawable.logout_inactive);
        tabSideMenu10.setImage_id_acative(R.drawable.logout_active);
        sideMenus.add(tabSideMenu10);

        return sideMenus;
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof  OnFragmentInteractionListener){
            listener = (OnFragmentInteractionListener) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public class SideMenuAdapter extends BaseAdapter {
        private int selectedIndex;
        List<TabSideMenu> tabSideMenuList;
        Context context;
        SessionManager sessionManager;
        String isCompleted = "";

        public SideMenuAdapter(Context context,List<TabSideMenu> tabSideMenuList){
            this.context = context;
            this.tabSideMenuList = tabSideMenuList;
            selectedIndex = 1;
            sessionManager = new SessionManager(context);
            isCompleted = sessionManager.getUserDetails().get(SessionManager.KEY_FNAME);
        }

        @Override
        public int getCount() {
            return tabSideMenuList==null?0:tabSideMenuList.size();
        }

        @Override
        public TabSideMenu getItem(int i) {
            return tabSideMenuList==null?null:tabSideMenuList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            if(view==null)
                view = LayoutInflater.from(context).inflate(R.layout.sidemenu_list_item,null);
            TabSideMenu sideMenu = getItem(i);
            TextView nameTextView = (TextView)view.findViewById(R.id.txtMenu);
            ImageView imageView = (ImageView)view.findViewById(R.id.imgMenu);//imgMenu
            ImageView imgTitle = (ImageView)view.findViewById(R.id.imgTitle);
            ImageView imgMenuPortrait = (ImageView)view.findViewById(R.id.imgMenuPortrait);
            LinearLayout llMenuPortrait = (LinearLayout)view.findViewById(R.id.llMenuPortrait);
            LinearLayout llMenuLandscape = (LinearLayout)view.findViewById(R.id.llMenuLandscape);
            nameTextView.setText(sideMenu.getName());

            Picasso.with(context).load(sideMenu.getImage_id_inactive()).into(imageView);
            imgTitle.setVisibility(sideMenu.getName().equals(context.getString(R.string.complete_profile))? View.VISIBLE : View.GONE);
            if(getResources().getBoolean(R.bool.isTablet)) {

                if(DisplayOrientation.getDisplayOrientation(context) == 0 || DisplayOrientation.getDisplayOrientation(context) == 2)
                {
                    llMenuPortrait.setVisibility(View.VISIBLE);
                    llMenuLandscape.setVisibility(View.GONE);
                    if (i == selectedIndex && i!=0) {
                        view.setBackground(context.getResources().getDrawable(R.drawable.selected_menu_bg));
                        Picasso.with(context).load(sideMenu.getImage_id_acative()).into(imgMenuPortrait);
                    } else {
                        Picasso.with(context).load(sideMenu.getImage_id_inactive()).into(imgMenuPortrait);
                        view.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
                    }
                }else
                {
                    llMenuPortrait.setVisibility(View.GONE);
                    llMenuLandscape.setVisibility(View.VISIBLE);
                    if (i == selectedIndex && i!=0) {
                        view.setBackground(context.getResources().getDrawable(R.drawable.selected_menu_bg));
                        nameTextView.setTextColor(context.getResources().getColor(R.color.primary_meroon));
                        Picasso.with(context).load(sideMenu.getImage_id_acative()).into(imageView);
                    } else {
                        Picasso.with(context).load(sideMenu.getImage_id_inactive()).into(imageView);
                        view.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
                        nameTextView.setTextColor(context.getResources().getColor(R.color.white));
                    }
                }

            }else
            {
                if(sideMenu.getName().equals(getString(R.string.complete_profile)))
                    nameTextView.setTextColor(context.getResources().getColor(R.color.test_completed_red));
                 else
                     nameTextView.setTextColor(context.getResources().getColor(R.color.dark_gray));
            }
            final int position = i;
            final TabSideMenu sideMenu_new = sideMenu;
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedIndex = position == 0 || position == 9?DrawerActivityData.currentIndex : position;
                    notifyDataSetChanged();
                    listener.onSideMenuSelected(sideMenu_new,position);
                }
            });
            return view;
        }
        public void setSelectedIndex(int ind)
        {
            selectedIndex = ind;
            notifyDataSetChanged();
        }
    }

}
