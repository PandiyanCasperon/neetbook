package com.app.neetbook.View;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.app.neetbook.Adapter.ArticleTabsAdapter;
import com.app.neetbook.Interfaces.OnMobileHomeMenuSelectedListioner;
import com.app.neetbook.Model.HeadData;
import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.McqExam;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ActionBarActivityNeetBook;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.View.SideMenu.SubscrptionDetailListFragment;
import com.app.neetbook.View.SideMenu.TabMainActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class SubscriptionMotionDetailActivity extends ActionBarActivityNeetBook implements View.OnClickListener {
    private Context context;
    Display display;
    int height1, width1;
    SessionManager sessionManager;
    Typeface tfBold, tfMedium, tfRegular;
    private ViewPager viewPager;
    RecyclerView recyclerViewGroups;
    private ArrayList<HeadData> headDataList;
    private TextView content_title_tv, superHeading, textView;
    String strSubjectId = "", strSubjectName = "", strChaptortName = "", strHeadId = "", strChapterId = "", loadingType = "", is_coming_page = "", strHeadName = "";
    public static ArrayList<HeadDetails> mcqHeadList;
    public static ArrayList<McqExam> mcqExamArrayList;
    LinearSnapHelper snapHelper;
    ViewPagerAdapter viewPagerAdapter;
    private int currentSelectedPosition = 0;
    ArticleTabsAdapter homeGroupNameAdapter;
    LinearLayoutManager layoutManager;
    private static final float MILLISECONDS_PER_INCH = 200f;
    ImageView homeBack,ToolBarLayout;

    private final static int DIMENSION = 2;
    private final static int HORIZONTAL = 0;
    private final static int VERTICAL = 1;
    private MotionLayout ArticleMotionLayout;
    private int Position = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_motion_detail);

        context = getApplicationContext();
        StatusBarColorChange.updateStatusBarColor(SubscriptionMotionDetailActivity.this, getResources().getColor(R.color.test_completed_blue));
        init();
    }

    private void init() {
        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width1 = size.x;
        height1 = size.y;
        sessionManager = new SessionManager(SubscriptionMotionDetailActivity.this);
        tfBold = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaRegular.otf");

        ToolBarLayout=(ImageView)findViewById(R.id.toolBarLayout);


        viewPager = findViewById(R.id.recyclerview);
        recyclerViewGroups = findViewById(R.id.ChapterListRecyclerView);

        headDataList = new ArrayList<>();

        textView = findViewById(R.id.chapterTitleConstraint);
        content_title_tv = findViewById(R.id.subjectTitleConstraint);
        superHeading = findViewById(R.id.superTitleConstraint);
        homeBack = findViewById(R.id.home);
        ArticleMotionLayout = (MotionLayout) findViewById(R.id.ArticleMotion);

     /*   if (sessionManager.isNightModeEnabled()){
            content_title_tv.setBackgroundColor(ContextCompat.getColor(this,R.color.point_motion_super_title_bgcolor));
            content_title_tv.setTextColor(ContextCompat.getColor(this,R.color.point_motion_super_title_color));
            textView.setTextColor(ContextCompat.getColor(this,R.color.point_motion_super_title_color));
            ToolBarLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.point_motion_super_title_bgcolor));
        }else {
            content_title_tv.setTextColor(ContextCompat.getColor(this,R.color.point_motion_super_title_color));
            content_title_tv.setBackgroundColor(ContextCompat.getColor(this,R.color.subsDetail_blue));
        }*/


        homeBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        if (getIntent().getExtras() != null) {
            strHeadId = getIntent().getStringExtra("head_id");
            strSubjectId = getIntent().getStringExtra("subject_id");
            strSubjectName = getIntent().getStringExtra("subject_name");
            strChaptortName = getIntent().getStringExtra("chapter_name");
            textView.setText(strSubjectName);
            loadingType = getIntent().getStringExtra("loading_type");

            if (getIntent().getExtras().containsKey("coming_page")) {

                is_coming_page = getIntent().getStringExtra("coming_page");
            }


            if (!getIntent().getStringExtra("loading_type").equals("1")) {
                strChapterId = getIntent().getStringExtra("chapter_id");
            }
            if (sessionManager.getViewToBookMark() != null && !sessionManager.getViewToBookMark().isEmpty() && sessionManager.getViewToBookMark().equalsIgnoreCase("Yes")) {
                if (sessionManager.getBookmarkDetails().get(SessionManager.LOADING_TYPE).equals("1")) {
                    mcqHeadList = new Gson().fromJson(sessionManager.getBookmarkDetails().get(SessionManager.HEAD_LIST), new TypeToken<List<HeadDetails>>() {
                    }.getType());
                } else {
                    mcqExamArrayList = new Gson().fromJson(sessionManager.getBookmarkDetails().get(SessionManager.HEAD_LIST), new TypeToken<List<McqExam>>() {
                    }.getType());
                }
            }
        }
        createSnap();
        setupViewPager(viewPager);
        setFontSize();

        ArticleMotionLayout.addTransitionListener(new MotionLayout.TransitionListener() {
            @Override
            public void onTransitionStarted(MotionLayout motionLayout, int i, int i1) {


            }

            @Override
            public void onTransitionChange(MotionLayout motionLayout, int i, int i1, float v) {
                setGroupAdapter(Position);
            }

            @Override
            public void onTransitionCompleted(MotionLayout motionLayout, int i) {


            }

            @Override
            public void onTransitionTrigger(MotionLayout motionLayout, int i, boolean b, float v) {


            }
        });
    }

    private void setFontSize() {
        textView.setTextAppearance(SubscriptionMotionDetailActivity.this, R.style.textViewMediumSubjectName);
        textView.setTypeface(tfMedium);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }

    private void setupViewPager(final ViewPager viewPager) {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this);

        if (loadingType.equals("1")) {
            if (mcqHeadList.size() > 0) {
                for (int i = 0; i < mcqHeadList.size(); i++) {
                    if (mcqHeadList.get(i).isSH.equals("Yes")) {

                        HeadData data = new HeadData();
                        data._id = mcqHeadList.get(i).headData.get(0)._id;
                        data.short_name = mcqHeadList.get(i).headData.get(0).short_name;
                        data.long_name = mcqHeadList.get(i).headData.get(0).long_name;
                        data.superHeadingName = mcqHeadList.get(i).isSH.equals("1") ? mcqHeadList.get(i).superDetails.short_name : "";
                        headDataList.add(data);
                        viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(strSubjectId, strChapterId, mcqHeadList.get(i).headData.get(0)._id, mcqHeadList.get(i).headData.get(0).long_name, loadingType), mcqHeadList.get(i).headData.get(0).short_name);

                    } else {
                        if (mcqHeadList.get(i).headData.size() > 0) {
                            for (int j = 0; j < mcqHeadList.get(i).headData.size(); j++) {

                                HeadData data = new HeadData();
                                data._id = mcqHeadList.get(i).headData.get(j)._id;
                                data.short_name = mcqHeadList.get(i).headData.get(j).short_name;
                                data.long_name = mcqHeadList.get(i).headData.get(j).long_name;
                                data.superHeadingName = mcqHeadList.get(i).isSH.equals("1") ? mcqHeadList.get(i).superDetails.short_name : "";
                                headDataList.add(data);
                                viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(strSubjectId, strChapterId, mcqHeadList.get(i).headData.get(j)._id, mcqHeadList.get(i).headData.get(j).long_name, loadingType), mcqHeadList.get(i).headData.get(j).short_name);
                            }
                        }
                    }
                }
            }
        } else if (loadingType.equals("2") || loadingType.equals("3")) {
            if (mcqExamArrayList.size() > 0) {
                for (int i = 0; i < mcqExamArrayList.size(); i++) {
                    HeadData data = new HeadData();
                    data._id = mcqExamArrayList.get(i).get_id();
                    data.short_name = mcqExamArrayList.get(i).getShort_name();
                    data.long_name = mcqExamArrayList.get(i).getLong_name();
                    data.superHeadingName = "";
                    headDataList.add(data);
                    viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(strSubjectId, strChapterId, mcqExamArrayList.get(i).get_id(), mcqExamArrayList.get(i).getLong_name(), loadingType), mcqExamArrayList.get(i).getLong_name());

                }

            }
        }


        if (headDataList.size() > 0) {
            for (int i = 0; i < headDataList.size(); i++) {
                if (strHeadId.equalsIgnoreCase(headDataList.get(i)._id)) {
                    currentSelectedPosition = i;
                    content_title_tv.setText(headDataList.get(i).long_name);
                    strHeadName = headDataList.get(i).long_name;
                    sessionManager.updateBookMark("MCQ", strHeadId, strSubjectId, getIntent().getStringExtra("subject_name"), new Gson().toJson(loadingType.equals("1") ? mcqHeadList : mcqExamArrayList), strChapterId, getIntent().getStringExtra("chapter_name"), headDataList.get(i).superHeadingName.equals("") ? "" : headDataList.get(i).superHeadingName, "", loadingType);
                    sessionManager.LastReadSubjectBookmark(sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME), sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME), sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME));
                }
            }
        }

        setGroupAdapter(currentSelectedPosition);
        layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
        int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
        int centeredItemPosition = totalVisibleItems / 2;
        recyclerViewGroups.smoothScrollToPosition(currentSelectedPosition != 0 ? currentSelectedPosition - 1 : currentSelectedPosition);
        recyclerViewGroups.setScrollY(centeredItemPosition);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(currentSelectedPosition);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Position = position;
                setGroupAdapter(position);
                layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
                int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                int centeredItemPosition = totalVisibleItems / 2;
                recyclerViewGroups.smoothScrollToPosition(position != 0 ? position - 1 : position);

                content_title_tv.setText(headDataList.get(position).long_name);
                if (headDataList.get(position).superHeadingName != null && !headDataList.get(position).superHeadingName.equals(""))
                    superHeading.setText(headDataList.get(position).superHeadingName);
                else
                    superHeading.setText("");
                sessionManager.updateBookMark("MCQ", headDataList.get(position)._id, strSubjectId, getIntent().getStringExtra("subject_name"), new Gson().toJson(loadingType.equals("1") ? mcqHeadList : mcqExamArrayList), strChapterId, getIntent().getStringExtra("chapter_name"), headDataList.get(position).superHeadingName.equals("") ? "" : headDataList.get(position).superHeadingName, "", loadingType);

                sessionManager.LastReadSubjectBookmark(sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                        sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                        sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME));

                scrollToCenter(layoutManager, recyclerViewGroups, position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        /*viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                if (position != 0 && position != headDataList.size() - 1) {
                    page.setAlpha(0f);
                    page.setVisibility(View.VISIBLE);

                    // Start Animation for a short period of time
                    page.animate()
                            .alpha(1f)
                            .setDuration(page.getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }
        });*/
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        List<Fragment> fragmentList = new ArrayList<>();
        List<String> fragmentTitles = new ArrayList<>();
        Context mContext;
        TextView tabTitile;


        ViewPagerAdapter(FragmentManager fragmentManager,
                         FragmentActivity activity) {
            super(fragmentManager);
            mContext = activity;
        }

        @Override
        public Fragment getItem(int i) {
            return new SubscrptionDetailListFragment(strSubjectId, strChapterId, loadingType.equals("1") ? headDataList.get(i)._id : mcqExamArrayList.get(i).get_id(), loadingType.equals("1") ? headDataList.get(i).long_name : mcqExamArrayList.get(i).getLong_name(), loadingType);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitles.get(position);
        }

        void addFragment(Fragment fragment, String name) {
            fragmentList.add(fragment);
            fragmentTitles.add(name);
        }

        View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_items, null);
            tabTitile = v.findViewById(R.id.txtTabTitle);
            tabTitile.setText(fragmentTitles.get(position));
            tabTitile.setTextColor(getResources().getColor(R.color.white));
            return v;
        }
    }

    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
    }

    @Override
    public void onClick(View v) {

    }


    private void setGroupAdapter(int position) {

        Position = position;
        if (homeGroupNameAdapter == null) {
            homeGroupNameAdapter = new ArticleTabsAdapter(headDataList, SubscriptionMotionDetailActivity.this, position);
            layoutManager = new LinearLayoutManager(SubscriptionMotionDetailActivity.this, RecyclerView.HORIZONTAL, false);
            //layoutManager.setStackFromEnd(true);
            recyclerViewGroups.setLayoutManager(layoutManager);
            recyclerViewGroups.setAdapter(homeGroupNameAdapter);
            recyclerViewGroups.scrollToPosition(position);

        } else {
            homeGroupNameAdapter.notifyPosition(position);
            homeGroupNameAdapter.notifyDataSetChanged();
            scrollToCenter(layoutManager, recyclerViewGroups, position);
        }


        homeGroupNameAdapter.setOnGroupMenuSelectedListener(new OnMobileHomeMenuSelectedListioner() {
            @Override
            public void onGroupSelected(String groupId, int position) {
                Position = position;
                viewPager.setCurrentItem(position);
                layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
                assert layoutManager != null;
                int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                int centeredItemPosition = totalVisibleItems / 2;
                scrollToCenter(layoutManager, recyclerViewGroups, position);
            }
        });

    }

    public void scrollToCenter(LinearLayoutManager layoutManager, RecyclerView recyclerList, int clickPosition) {
        RecyclerView.SmoothScroller smoothScroller = createSnapScroller(recyclerList, layoutManager);

        if (smoothScroller != null) {
            smoothScroller.setTargetPosition(clickPosition);
            layoutManager.startSmoothScroll(smoothScroller);
        }
    }

    @Nullable
    private LinearSmoothScroller createSnapScroller(RecyclerView mRecyclerView, final RecyclerView.LayoutManager layoutManager) {
        if (!(layoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider)) {
            return null;
        }
        return new LinearSmoothScroller(mRecyclerView.getContext()) {
            @Override
            protected void onTargetFound(View targetView, RecyclerView.State state, Action action) {
                int[] snapDistances = calculateDistanceToFinalSnap(layoutManager, targetView);
                final int dx = snapDistances[HORIZONTAL];
                final int dy = snapDistances[VERTICAL];
                final int time = calculateTimeForDeceleration(Math.max(Math.abs(dx), Math.abs(dy)));
                if (time > 0) {
                    action.update(dx, dy, time, mDecelerateInterpolator);
                }
            }


            @Override
            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
            }
        };
    }

    private int[] calculateDistanceToFinalSnap(@NonNull RecyclerView.LayoutManager layoutManager, @NonNull View targetView) {
        int[] out = new int[DIMENSION];
        if (layoutManager.canScrollHorizontally()) {
            out[HORIZONTAL] = distanceToCenter(layoutManager, targetView,
                    OrientationHelper.createHorizontalHelper(layoutManager));
        }

        if (layoutManager.canScrollVertically()) {
            out[VERTICAL] = distanceToCenter(layoutManager, targetView,
                    OrientationHelper.createHorizontalHelper(layoutManager));
        }
        return out;
    }

    private int distanceToCenter(@NonNull RecyclerView.LayoutManager layoutManager,
                                 @NonNull View targetView, OrientationHelper helper) {
        final int childCenter = helper.getDecoratedStart(targetView)
                + (helper.getDecoratedMeasurement(targetView) / 2);
        final int containerCenter;
        if (layoutManager.getClipToPadding()) {
            containerCenter = helper.getStartAfterPadding() + helper.getTotalSpace() / 2;
        } else {
            containerCenter = helper.getEnd() / 2;
        }
        return childCenter - containerCenter;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (is_coming_page.equals("homepage")) {

            Intent s = new Intent(getApplicationContext(), TabMainActivity.class);
            s.putExtra("subjectId", strSubjectId);
            s.putExtra("subjectName", sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_NAME));
            s.putExtra("isTrial", sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_TRIAL_STATUS));
            s.putExtra("subjectShortName", sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_SHORT_NAME));
            s.putExtra("showing_page_position", "0");
            startActivity(s);
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        } else {

            finish();
        }
    }
}