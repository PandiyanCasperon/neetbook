package com.app.neetbook.View.Fragments.subscription;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.neetbook.Adapter.subscription.SubscriptionDetailAdapter;
import com.app.neetbook.Adapter.subscription.SubscriptionDetailAdapter.OnItemClickListener;
import com.app.neetbook.Model.subscription.SubscriptionDetail;
import com.app.neetbook.R;
import com.app.neetbook.Utils.widgets.CustomTextViewBold;

import java.util.ArrayList;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class SubscrptionDetailListFragment extends Fragment {

  private ArrayList<SubscriptionDetail> mSubscriptionDetailList = new ArrayList<>();
  private SubscriptionDetailAdapter categoryAdapter;
  private RecyclerView categoryList;
  private CustomTextViewBold subscriptionCategoryTitle;


  public SubscrptionDetailListFragment() {
    // Required empty public constructor
  }


  public static SubscrptionDetailListFragment newInstance(int param1) {
    SubscrptionDetailListFragment fragment = new SubscrptionDetailListFragment();
    Bundle args = new Bundle();

    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_subscrption_detail_list, container, false);
    init(rootView);
    return rootView;
  }

  private void init(View rootView) {
    subscriptionCategoryTitle = rootView.findViewById(R.id.txt_subscription_header);
    categoryList = rootView.findViewById(R.id.rv_category_list);

    populateCategoryList();
  }

  private void populateCategoryList() {
    SubscriptionDetail categoryItem = new SubscriptionDetail();
    categoryItem.setTitle("Pathological Findings");
    subscriptionCategoryTitle.setText(categoryItem.getTitle());

    for (int i = 1; i < 6; i++) {
      categoryItem.setQuestionCount(1 + ".");
      categoryItem.setQuestionTitle("Best method of diagnosing acute appendicitis is");
      categoryItem.setQuestionOptionFirst("Barium meal");
      categoryItem.setQuestionOptionSecond("History");
      categoryItem.setQuestionOptionThird("X-ray abdomen");
      categoryItem.setQuestionOptionFourth("Physical examination");
      mSubscriptionDetailList.add(categoryItem);
    }

    setupRecyclerView();
  }

  private void setupRecyclerView() {
    categoryList.setLayoutManager(new LinearLayoutManager(getActivity(),
        RecyclerView.VERTICAL, false));
    categoryAdapter = new SubscriptionDetailAdapter(getActivity(), mSubscriptionDetailList);
    categoryList.setAdapter(categoryAdapter);
    categoryAdapter.setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(View itemView, int position) {
        startSubscriptionCategoryInfoDialogFragment();
      }

      @Override
      public void onAnswerSelected(int position, String Answer) {

      }

      @Override
      public void onMultiAnswerSelected(int position, String Answer) {

      }

      @Override
      public void onImageClicked(int position) {

      }
    });
  }

  private void startSubscriptionCategoryInfoDialogFragment() {
    SubscriptionCategoryItemInfoFragment dialog = new SubscriptionCategoryItemInfoFragment();
    FragmentTransaction ft = getFragmentManager().beginTransaction();
    dialog.show(ft, SubscriptionCategoryItemInfoFragment.TAG);
  }

}
