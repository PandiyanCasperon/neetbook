package com.app.neetbook.View.SideMenu;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.neetbook.Adapter.sideMenuContent.SubscriptionTestAdapter;
import com.app.neetbook.Interfaces.APMTSearchListioner;
import com.app.neetbook.Interfaces.OnPinChanged;
import com.app.neetbook.Interfaces.OnSubmitListioner;
import com.app.neetbook.Interfaces.OnTestAttemp;
import com.app.neetbook.Interfaces.RefreshInterface;
import com.app.neetbook.Interfaces.TestOnBackPressed;
import com.app.neetbook.Interfaces.onAttemptingTestListener;
import com.app.neetbook.Model.EventBusTestRefreshPojo;
import com.app.neetbook.Model.sidemenuFromContent.SubscriptionArticleBody;
import com.app.neetbook.Model.sidemenuFromContent.SubscriptionTest;
import com.app.neetbook.Model.subscription.SubscriptionCart;
import com.app.neetbook.R;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Data.ChangeMobileData;
import com.app.neetbook.Utils.Data.ChangePinData;
import com.app.neetbook.Utils.Data.CompleteProfileData;
import com.app.neetbook.Utils.Data.ContactUsData;
import com.app.neetbook.Utils.Data.DrawerActivityData;
import com.app.neetbook.Utils.Data.LoginData;
import com.app.neetbook.Utils.Data.MyProfileData;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.OnSwipeTouchListener;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.widgets.CustomTextViewBold;
import com.app.neetbook.View.DynamicFragmentActivity;
import com.app.neetbook.View.ImagesViewActivity;
import com.app.neetbook.View.ReviewOrder;

import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

public class SubscriptionTestFragment extends Fragment implements View.OnClickListener, OnSubmitListioner, TestOnBackPressed {

  private ArrayList<SubscriptionTest> mSubscriptionDetailList;
  private ArrayList<SubscriptionTest> mAnsweredList = new ArrayList<>();
  private SubscriptionTestAdapter categoryAdapter;
  private RecyclerView testList;
  private CustomTextViewBold subscriptionTestTitle;
  String test_id = "",type = "",chapter_id = "",subject_id = "",strLongName = "",no_of_question = "",chapter_name = "",isFinished = "",questionType = "",strInstruction = "";
  Loader loader;
  private int page = 1;
  int loadMoreEnable = 0;
  private int totalSubsCount;
  SessionManager sessionManager;
  private CustomAlert customAlert;
  private LinearLayout llResult;
  private Button btnSubmit;
  private ImageView imgTimer,imgCloseInstruction;
  private OnTestAttemp onTestAttemp;
  Typeface tfBold,tfMedium,tfRegular;

  TextView notext;
  RelativeLayout nestedscroll;
  int nochangesmade = 0;
  RefreshInterface refreshInterface;
  private TextView textView3,textView4,textView5,textView6,textView7,textView8,textView9,textView10,textViewInstruction,txtEmoji;

  private String wrng_ans_mark,crt_ans_mark;
    private Dialog alertDialog;
  private double correctAnswerMark = 0;
  private double wrongAnswerMark = 0;
  private  RelativeLayout rlLeft,rlRight;
  private String strPending="",pendingTestId="";
  public static String isPending = "0";
  public static String isTestIdSaved = "";
  public static String testType = "";
  public static String testInstruction = "";
  public static String testCMark = "";
  public static String testEMark = "";
  public static String testChapterId = "";
  public static String testNumQ = "";
  public SubscriptionTestFragment(String test_id,String type,String chapter_id,String subject_id,String chapter_name,String no_of_question,String isFinished,String questionType,String strInstruction,String strLongName,String crt_ans_mark,String wrng_ans_mark,String strPending,String pendingTestId) {
    // Required empty public constructor
    this.test_id = test_id;
    this.type = type;
    this.chapter_id = chapter_id;
    this.subject_id = subject_id;
    this.chapter_name = chapter_name;
    this.no_of_question = no_of_question;
    this.isFinished = isFinished;
    this.questionType = questionType;
    this.strInstruction = strInstruction;
    this.strInstruction = strInstruction;
    this.strLongName = strLongName;
    this.strLongName = strLongName;
    this.crt_ans_mark = crt_ans_mark;
    this.wrng_ans_mark = wrng_ans_mark;
    this.strPending = strPending;
    this.pendingTestId = pendingTestId;
  }


  public static SubscriptionTestFragment newInstance() {
    //SubscriptionTestFragment fragment = new SubscriptionTestFragment();
    //Bundle args = new Bundle();
    //fragment.setArguments(args);
    return null;
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    sessionManager = new SessionManager(getActivity());
    customAlert = new CustomAlert(getActivity());
    loader = new Loader(getActivity());
    tfBold  = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaBold.otf");
    tfMedium  = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaSemibold.otf");
    tfRegular  = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNovaRegular.otf");

  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_subscription_test, container, false);
    init(rootView);
    return rootView;
  }

  private void init(View rootView) {

    testList = rootView.findViewById(R.id.rv_test_list);
    //imgCloseInstruction = rootView.findViewById(R.id.imgCloseInstruction);
    subscriptionTestTitle = rootView.findViewById(R.id.txt_subscription_test_header);
    llResult = rootView.findViewById(R.id.llResult);
    rlLeft = rootView.findViewById(R.id.rlLeft);
    rlRight = rootView.findViewById(R.id.rlLRight);
    textView3 = rootView.findViewById(R.id.textView3);
    textView4 = rootView.findViewById(R.id.textView4);
    textView5 = rootView.findViewById(R.id.textView5);
    textView6 = rootView.findViewById(R.id.textView6);
    textView7 = rootView.findViewById(R.id.textView7);
    textView8 = rootView.findViewById(R.id.textView8);
    textView10 = rootView.findViewById(R.id.textView10);
    txtEmoji = rootView.findViewById(R.id.txtEmoji);
    textView9 = rootView.findViewById(R.id.textView9);
    notext= rootView.findViewById(R.id.notext);
    nestedscroll= rootView.findViewById(R.id.nestedscroll);
    btnSubmit = rootView.findViewById(R.id.btnSubmit);

    // imgTimer = rootView.findViewById(R.id.imgTimer);
    if(strPending != null && strPending.equals("1"))
    {
      btnSubmit.setEnabled(true);
      btnSubmit.setBackground(getResources().getDrawable(R.drawable.test_submit_btn_bg));
    }else
      btnSubmit.setEnabled(false);

    //llResult.setVisibility(isFinished.equals("Yes")?View.VISIBLE:View.GONE);
    //subscriptionTestTitle.setText(chapter_name);

    // textViewInstruction.setText(strInstruction);
    btnSubmit.setOnClickListener(this);

    // imgCloseInstruction.setOnClickListener(this);

    setFontSize();
    populateTestList();
    listener();


  }

  private void listener() {

  }




  private void setFontSize() {

    if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
      subscriptionTestTitle.setTextAppearance(getActivity(),R.style.textViewSmallChaptName);
      /*textView3.setTextAppearance(getActivity(),R.style.textViewSmallTestResultValue);
      textView5.setTextAppearance(getActivity(),R.style.textViewSmallTestResultValue);
      textView7.setTextAppearance(getActivity(),R.style.textViewSmallTestResultValue);
      textView9.setTextAppearance(getActivity(),R.style.textViewSmallTestResultValue);
      textView4.setTextAppearance(getActivity(),R.style.textViewSmallTestResultSentence);
      textView6.setTextAppearance(getActivity(),R.style.textViewSmallTestResultSentence);
      textView8.setTextAppearance(getActivity(),R.style.textViewSmallTestResultSentence);
      textView10.setTextAppearance(getActivity(),R.style.textViewSmallTestResultSentence);
      textViewInstruction.setTextAppearance(getActivity(),R.style.textViewSmallTestInstruction);*/
      btnSubmit.setTextAppearance(getActivity(),R.style.textViewSmallTestSubmitButton);
//      txtQACount.setTextAppearance(getActivity(),R.style.textViewSmallTimer);

    }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
      subscriptionTestTitle.setTextAppearance(getActivity(),R.style.textViewMediumChaptName);
/*      textView3.setTextAppearance(getActivity(),R.style.textViewMediumTestResultValue);
      textView5.setTextAppearance(getActivity(),R.style.textViewMediumTestResultValue);
      textView7.setTextAppearance(getActivity(),R.style.textViewMediumTestResultValue);
      textView9.setTextAppearance(getActivity(),R.style.textViewMediumTestResultValue);
      textView4.setTextAppearance(getActivity(),R.style.textViewMediumTestResultSentence);
      textView6.setTextAppearance(getActivity(),R.style.textViewMediumTestResultSentence);
      textView8.setTextAppearance(getActivity(),R.style.textViewMediumTestResultSentence);
      textView10.setTextAppearance(getActivity(),R.style.textViewMediumTestResultSentence);
      textViewInstruction.setTextAppearance(getActivity(),R.style.textViewMediumTestInstruction);*/
      btnSubmit.setTextAppearance(getActivity(),R.style.textViewMediumTestSubmitButton);

//      txtQACount.setTextAppearance(getActivity(),R.style.textViewMediumTimer);

    }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
      subscriptionTestTitle.setTextAppearance(getActivity(),R.style.textViewLargeChaptName);
  /*    textView3.setTextAppearance(getActivity(),R.style.textViewLargeTestResultValue);
      textView5.setTextAppearance(getActivity(),R.style.textViewLargeTestResultValue);
      textView7.setTextAppearance(getActivity(),R.style.textViewLargeTestResultValue);
      textView9.setTextAppearance(getActivity(),R.style.textViewLargeTestResultValue);
      textView4.setTextAppearance(getActivity(),R.style.textViewLargeTestResultSentence);
      textView6.setTextAppearance(getActivity(),R.style.textViewLargeTestResultSentence);
      textView8.setTextAppearance(getActivity(),R.style.textViewLargeTestResultSentence);
      textView10.setTextAppearance(getActivity(),R.style.textViewLargeTestResultSentence);
      textViewInstruction.setTextAppearance(getActivity(),R.style.textViewLargeTestInstruction);*/
      btnSubmit.setTextAppearance(getActivity(),R.style.textViewLargeTestSubmitButton);

    }
    subscriptionTestTitle.setTypeface(tfBold);
   /* textView3.setTypeface(tfBold);
    textView4.setTypeface(tfMedium);
    textView5.setTypeface(tfBold);
    textView6.setTypeface(tfMedium);
    textView7.setTypeface(tfBold);
    textView8.setTypeface(tfMedium);
    textView9.setTypeface(tfBold);
    textView10.setTypeface(tfMedium);*/

    /* textViewInstruction.setTypeface(tfRegular);*/
//    txtQACount.setTypeface(tfRegular);

    btnSubmit.setTypeface(tfMedium);
    notext.setTypeface(tfMedium);

  }
  private void populateTestList() {

    mSubscriptionDetailList = new ArrayList<>();
    ServiceRequest mRequest = new ServiceRequest(getActivity());
    HashMap<String, String> params = new HashMap<>();
    params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
    params.put("subject_id",subject_id);
    params.put("type",isTestIdSaved.equals("")?type:testType);
    params.put("test_id",isTestIdSaved.equals("")?test_id:isTestIdSaved);
    params.put("chapter_id",isTestIdSaved.equals("")?chapter_id:testChapterId);
    params.put("no_of_qstn",isTestIdSaved.equals("")?no_of_question:testNumQ);
    params.put("page",""+page);
    params.put("incomplete",strPending);
    params.put("skip","0");
    params.put("limit","10");


    System.out.println("------------params Response----------------" + params.toString());


    mRequest.makeServiceRequest(IConstant.testQuestions, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
      @Override
      public void onCompleteListener(String response) {
        System.out.println("------------testQuestions Response----------------" + response);
        String sStatus = "";
        String message = "";
        try {

          JSONObject obj = new JSONObject(response);
          sStatus = obj.getString("status");
          message = obj.has("message")?obj.getString("message"):"";


          if (sStatus.equalsIgnoreCase("1")) {
            JSONObject object = obj.getJSONObject("response");
            btnSubmit.setVisibility(View.VISIBLE);

            JSONArray testArray = object.getJSONArray("questions");
            if(testArray.length() == 0)
            {
              notext.setVisibility(View.VISIBLE);
              nestedscroll.setVisibility(View.GONE);
            }
            else

            {
              notext.setVisibility(View.GONE);
              nestedscroll.setVisibility(View.VISIBLE);
                showHideInstructio();
            }
            JSONArray answeredArray = object.getJSONArray("answered");

            if(answeredArray.length() > 0)
            {
              nochangesmade ++;
            }




            if(testArray.length()>0) {
              //llEmpty.setVisibility(View.GONE);
              for(int i=0;i<testArray.length();i++)
              {
                SubscriptionTest testItem = new SubscriptionTest();
                JSONObject jsonObject = testArray.getJSONObject(i);

                testItem.setTestId(test_id);
                testItem.set_id(jsonObject.getString("_id"));
                testItem.setQuestionTitle(jsonObject.getString("question"));
                testItem.setQuestionCount(String.valueOf(i+1));
                // testItem.setQuestionCount(jsonObject.getString("status"));
                testItem.setQuestionOptionFirst(jsonObject.getString("optionA"));
                testItem.setQuestionOptionSecond(jsonObject.getString("optionB"));
                testItem.setQuestionOptionThird(jsonObject.getString("optionC"));
                testItem.setQuestionOptionFourth(jsonObject.getString("optionD"));
                testItem.setQuestionOptionFifth(!jsonObject.isNull("optionE")?jsonObject.getString("optionE") : "");

                testItem.setAnswer(jsonObject.getString("answer"));


                testItem.setQuestionType(questionType);

                ArrayList<String> images = new ArrayList<>();
                if(jsonObject.has("imageurl")&& jsonObject.getJSONArray("imageurl").length()>0)
                {
                  for(int j=0;j<jsonObject.getJSONArray("imageurl").length();j++)
                  {
                    images.add(jsonObject.getJSONArray("imageurl").getJSONObject(j).getString("url"));
                  }

                }
                testItem.setImageUrls(images);
                if(answeredArray.length()>0)
                {
                  for(int k = 0; k < answeredArray.length();k++)
                  {
                    if(answeredArray.getJSONObject(k).getString("q_id").equals(jsonObject.getString("_id"))) {
                      testItem.setUserAnswer(answeredArray.getJSONObject(k).getString("user_answer"));
                      mAnsweredList.add(testItem);
                    }
                  }
                }
                mSubscriptionDetailList.add(testItem);

              }
              loadMoreEnable = 0;

              setupRecyclerView(false);
            }

            if(answeredArray.length() > 0)
            {
              EventBus.getDefault().post("answaerd");
            }

          } else if(sStatus.equals("00")){
            customAlert.singleLoginAlertLogout();
          }else if(sStatus.equalsIgnoreCase("01")){
            customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
          } else {
            customAlert.showAlertOk(getString(R.string.alert_oops),message);
          }

        } catch (JSONException e) {

          e.printStackTrace();
        }

      }

      @Override
      public void onErrorListener(String errorMessage) {
        // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
      }
    });

  }


    private void showHideInstructio() {
        if(alertDialog != null && alertDialog.isShowing())
            alertDialog.dismiss();
        else {
            alertDialog = new Dialog(getActivity());
            alertDialog.setContentView(R.layout.custom_instruction_alert);
            alertDialog.setCancelable(true);
            alertDialog.setCanceledOnTouchOutside(true);

            if(alertDialog.getWindow() != null) {
                alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }

            TextView txtInstructionDialog = alertDialog.findViewById(R.id.txtInstructionDialog);
            txtInstructionDialog.setText(strInstruction);
    /*  LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) txtInstructionDialog.getLayoutParams();
      params.height = (int) (height1*(0.26));
      txt_description.setLayoutParams(params);*/
            txtInstructionDialog.setMovementMethod(new ScrollingMovementMethod());
            RelativeLayout rlClose = alertDialog.findViewById(R.id.rlClose);
            rlClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();

                }
            });
            if (!alertDialog.isShowing())
                alertDialog.show();
        }

    }

  private void setupRecyclerView(boolean isCompleted) {
    testList.setLayoutManager(new LinearLayoutManager(getActivity(),
            RecyclerView.VERTICAL, false));
    categoryAdapter = new SubscriptionTestAdapter(getActivity(), mSubscriptionDetailList,isCompleted);
    testList.setAdapter(categoryAdapter);


    /*SubscriptionTestActivity.txtQACount.setText(0+"/"+mSubscriptionDetailList.size());*/
    categoryAdapter.setOnAttemptingTestListener(new onAttemptingTestListener() {
      @Override
      public void onSelectedAnswer(int position, String Answer) {
        Log.e("Answer",Answer);
        nochangesmade++;

        if (mAnsweredList.size() > 0) {
          for (int i = 0; i < mAnsweredList.size(); i++) {
            if (mAnsweredList.get(i).get_id().equalsIgnoreCase(mSubscriptionDetailList.get(position).get_id()))
              mAnsweredList.remove(i);
          }
        }
        if(!Answer.isEmpty() && Answer.length()>0)
          mAnsweredList.add(mSubscriptionDetailList.get(position));

        /*SubscriptionTestActivity.txtQACount.setText(mAnsweredList.size()+"/"+mSubscriptionDetailList.size());*/

        if(mAnsweredList.size()>0)
        {
          btnSubmit.setEnabled(true);
          btnSubmit.setBackground(getResources().getDrawable(R.drawable.test_submit_btn_bg));
          Set<SubscriptionTest> filterArray = new HashSet<SubscriptionTest>(mAnsweredList);
          mAnsweredList.clear();
          mAnsweredList.addAll(filterArray);
          if(onTestAttemp!= null)
            onTestAttemp.onAnswered(mAnsweredList.size()+"/"+mSubscriptionDetailList.size(),mAnsweredList.size());
          else  if(refreshInterface!= null)
            refreshInterface.refresh_fragment(mAnsweredList.size()+"/"+mSubscriptionDetailList.size());
        }else {
          btnSubmit.setEnabled(false);
          btnSubmit.setBackground(getResources().getDrawable(R.drawable.test_submit_btn_inactive));
          if(onTestAttemp!= null)
            onTestAttemp.onAnswered(mAnsweredList.size()+"/"+mSubscriptionDetailList.size(),mAnsweredList.size());
          else  if(refreshInterface!= null)
            refreshInterface.refresh_fragment(mAnsweredList.size()+"/"+mSubscriptionDetailList.size());
        }



      }

      @Override
      public void onMultiChoiceSelectedAnswer(int position, String Answer) {
        String bufferAnswer = null;
        ArrayList<Character> chars = new ArrayList<>();

        for (char ch: mSubscriptionDetailList.get(position).getUserAnswer().toCharArray()) {
          chars.add(ch);
        }
        if (mAnsweredList.size() > 0 && chars.size()>0) {
          for (int i = 0; i < mAnsweredList.size(); i++) {
            if (mAnsweredList.get(i).get_id().equalsIgnoreCase(mSubscriptionDetailList.get(position).get_id())) {
              /*String ans = mSubscriptionDetailList.get(position).getAnswer().replace(",", "");*/
              // System.out.println(chars);
              int ans = mSubscriptionDetailList.get(position).getQuestionOptionFifth() != null && !mSubscriptionDetailList.get(position).getQuestionOptionFifth().isEmpty()? 5 :4 ;
              if (ans > chars.size()) {
                for (int k = 0; k < chars.size(); k++) {
                  if (Answer.charAt(0)==(chars.get(k)))
                    mSubscriptionDetailList.get(position).setUserAnswer(mSubscriptionDetailList.get(position).getUserAnswer().replace(Answer,""));
                  else
                    mSubscriptionDetailList.get(position).setUserAnswer(mSubscriptionDetailList.get(position).getUserAnswer().contains(Answer)?mSubscriptionDetailList.get(position).getUserAnswer()+"":mSubscriptionDetailList.get(position).getUserAnswer()+Answer);
                }
              }else {
                for (int k = 0; k < chars.size(); k++) {
                  if (Answer.charAt(0)==(chars.get(k)))
                    mSubscriptionDetailList.get(position).setUserAnswer(mSubscriptionDetailList.get(position).getUserAnswer().replace(Answer,""));
                  else
                    mSubscriptionDetailList.get(position).setUserAnswer(mSubscriptionDetailList.get(position).getUserAnswer());
                }

              }/*else{
                for (int k = 0; k < chars.size(); k++) {
                  if (Answer.equals(chars.get(k)))
                    chars.remove(k);
                  else
                    bufferAnswer = bufferAnswer + chars.get(k);
                }
              }
                bufferAnswer = bufferAnswer + Answer;*/
            }
          }
          // mSubscriptionDetailList.get(position).setUserAnswer(bufferAnswer);
        }else
        {
          mSubscriptionDetailList.get(position).setUserAnswer(Answer);
        }


        if(isAdded(mSubscriptionDetailList.get(position))>-1) {
          Log.e("isAdded",isAdded(mSubscriptionDetailList.get(position))+"");
          if(mAnsweredList.size()>0) {
            mAnsweredList.remove(isAdded(mSubscriptionDetailList.get(position)));
            if( mSubscriptionDetailList.get(position).getUserAnswer() != null && !mSubscriptionDetailList.get(position).getUserAnswer().isEmpty() && mSubscriptionDetailList.get(position).getUserAnswer().length()>0)
              mAnsweredList.add(mSubscriptionDetailList.get(position));
          }else
          {
            mAnsweredList.add(mSubscriptionDetailList.get(position));
          }
        }
        else {
          mAnsweredList.add(mSubscriptionDetailList.get(position));
          Log.e("isAdded", isAdded(mSubscriptionDetailList.get(position)) + "");
        }
        if(mAnsweredList.size()>0)
        {
          btnSubmit.setEnabled(true);
          btnSubmit.setBackground(getResources().getDrawable(R.drawable.test_submit_btn_bg));
          Set<SubscriptionTest> filterArray = new HashSet<SubscriptionTest>(mAnsweredList);
          mAnsweredList.clear();
          mAnsweredList.addAll(filterArray);
          if(onTestAttemp!= null)
            onTestAttemp.onAnswered(mAnsweredList.size()+"/"+mSubscriptionDetailList.size(),mAnsweredList.size());
          else  if(refreshInterface!= null)
            refreshInterface.refresh_fragment(mAnsweredList.size()+"/"+mSubscriptionDetailList.size());
        }else {
          btnSubmit.setEnabled(false);
          btnSubmit.setBackground(getResources().getDrawable(R.drawable.test_submit_btn_inactive));
          if(onTestAttemp!= null)
            onTestAttemp.onAnswered(mAnsweredList.size()+"/"+mSubscriptionDetailList.size(),mAnsweredList.size());
          else  if(refreshInterface!= null)
            refreshInterface.refresh_fragment(mAnsweredList.size()+"/"+mSubscriptionDetailList.size());
        }
        /*SubscriptionTestActivity.txtQACount.setText(mAnsweredList.size()+"/"+mSubscriptionDetailList.size());*/

        Log.e("bufferAnswer",mSubscriptionDetailList.get(position).getUserAnswer());
        Log.e("mAnsweredList",mAnsweredList.size()+"");
        categoryAdapter.notifyDataSetChanged();

      }

      @Override
      public void onImageClicked(int position) {
        startActivity(new Intent(getActivity(), ImagesViewActivity.class).putExtra("title",chapter_name).putExtra("image",mSubscriptionDetailList.get(position).getImageUrls()).putExtra("from","Test"));
        getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
      }
    });
    if(onTestAttemp!= null)
      onTestAttemp.onAnswered(mAnsweredList.size()+"/"+mSubscriptionDetailList.size(),0);
    else if(refreshInterface!= null)
      refreshInterface.refresh_fragment(mAnsweredList.size()+"/"+mSubscriptionDetailList.size());

    if(isCompleted)
    {

      testList.smoothScrollToPosition(0);

      if (testList != null) {
        nochangesmade = 0;
        //  Toast.makeText(getActivity(),"jdd",Toast.LENGTH_LONG).show();
        testList.scrollToPosition(0);
        LinearLayoutManager layoutManager = (LinearLayoutManager) testList.getLayoutManager();
        layoutManager.smoothScrollToPosition(testList, new RecyclerView.State(), 0);

      }
    }

  }



  private int isAdded(SubscriptionTest priceList)
  {
    /*Collections.binarySearch(mAnsweredList, priceList, new Comparator<SubscriptionTest>() {

      @Override
      public int compare(SubscriptionTest o1, SubscriptionTest o2) {
        return o1.get_id().compareTo(o2.get_id());
      }
    });
   return  -1; */

    if(mAnsweredList.size()>0)
    {
      for(int i=0;i<mAnsweredList.size();i++)
      {
        if(mAnsweredList.get(i).get_id().equals(priceList.get_id()))
          return i;
        break;
      }

    }else
    {
      return  -1;
    }
    return  -1;
  }
  private void calculateResult()
  {
    /*TimeBuff += MillisecondTime;*//*getting timer value*//*

    handler.removeCallbacks(runnable);*/ /*Stop timer*/
    isTestIdSaved = "";
    testChapterId = "";
    testCMark = "";
    testEMark = "";
    testInstruction = "";
    testType = "";
    testNumQ = "";
    double RightAnswerCount = 0;
    double totalRightAnswer = 0;
    double WrongAnswerCount = 0;
    double totalWrongAnswer = 0;
    double totalMarkScored = 0;
    double overAllCorrectCount = 0;

    correctAnswerMark = Double.parseDouble(crt_ans_mark);
    wrongAnswerMark = Double.parseDouble(wrng_ans_mark);
    ArrayList<Character> charsA = new ArrayList<>();
    for(int k=0;k<mSubscriptionDetailList.size();k++)
    {
      charsA.clear();
      for (char ch: mSubscriptionDetailList.get(k).getAnswer().replaceAll(",","").replaceAll(" ","").toCharArray()) {
        charsA.add(ch);
      }
      if(charsA.size()>0)
      {
        for(int j=0;j<charsA.size();j++)
        {
          overAllCorrectCount  = overAllCorrectCount+1;
        }
      }
    }
    if(mAnsweredList.size()>0)
    {



      for(int i=0;i<mAnsweredList.size();i++)
      {
        if(mAnsweredList.get(i).getTestId().equalsIgnoreCase(test_id))
        {
          if(mAnsweredList.get(i).getAnswer().equalsIgnoreCase(mAnsweredList.get(i).getUserAnswer()))
            RightAnswerCount = RightAnswerCount+1;
          else
            WrongAnswerCount = WrongAnswerCount+1;
        }
      }
      totalRightAnswer = correctAnswerMark * RightAnswerCount;
      totalWrongAnswer = wrongAnswerMark * WrongAnswerCount;
      totalMarkScored = totalRightAnswer - totalWrongAnswer;


      double totalIn100 = (totalMarkScored) / (overAllCorrectCount * correctAnswerMark);
      long finalPercentage = Math.round((totalIn100*100) /100) > 0 ? Math.round((totalIn100*100) /100) : 0;
      double percentage = (totalMarkScored) / (overAllCorrectCount * correctAnswerMark) *100;
      long toHundred = Math.round((totalIn100*100) /100);
      textView3.setText(""+Math.round(RightAnswerCount));
      textView5.setText(""+mAnsweredList.size()+"/"+mSubscriptionDetailList.size());
      textView7.setText(""+Math.round(WrongAnswerCount));
      textView9.setText(Math.round(totalMarkScored)+"/"+Math.round((overAllCorrectCount * correctAnswerMark)));
      double mark = percentage;
      textView10.setText("Score "+(percentage>0? Math.round(percentage) : 0)+"%");
      txtEmoji.setText(mark < 0 ? (new String(Character.toChars(0x1F620)))/*angry*/ : mark>0 && mark< 40 ? (new String(Character.toChars(0x1F622)))/*crying*/
              : mark>41 && mark< 60 ? (new String(Character.toChars(0x1F616)))/*Confounded Face*/
              : mark>61&& mark< 80 ? (new String(Character.toChars(0x263A)))/*smiling*/
              : (new String(Character.toChars(0x1F603))));/*angry*/
      llResult.setVisibility(View.VISIBLE);
      onTestAttemp.onSubmitted(""+Math.round(RightAnswerCount),
              ""+Math.round(WrongAnswerCount),
              ""+mAnsweredList.size()+"/"+mSubscriptionDetailList.size(),Math.round(totalMarkScored)+"/"+Math.round((overAllCorrectCount * correctAnswerMark)),Math.round(percentage));

      new AsyncRemoveTest().execute();
    }
  }
  private void calculateMultiChoiceResult() {
    isTestIdSaved = "";
    testChapterId = "";
    testCMark = "";
    testEMark = "";
    testInstruction = "";
    testType = "";
    testNumQ = "";
  /*  TimeBuff += MillisecondTime;
    handler.removeCallbacks(runnable);*/

    double RightAnswerCount = 0;
    double totalRightAnswer = 0;
    double WrongAnswerCount = 0;
    double totalWrongAnswer = 0;
    double totalMarkScored = 0;
    double overAllCorrectCount = 0;
    correctAnswerMark = Double.parseDouble(crt_ans_mark);
    wrongAnswerMark = Double.parseDouble(wrng_ans_mark);
    ArrayList<Character> charsA = new ArrayList<>();
    for(int k=0;k<mSubscriptionDetailList.size();k++)
    {
      charsA.clear();
      for (char ch: mSubscriptionDetailList.get(k).getAnswer().replaceAll(",","").replaceAll(" ","").toCharArray()) {
        charsA.add(ch);
      }
      if(charsA.size()>0)
      {
        for(int j=0;j<charsA.size();j++)
        {
          overAllCorrectCount  = overAllCorrectCount+1;
        }
      }
    }
    if(mAnsweredList.size()>0)
    {
      for(int i=0;i<mAnsweredList.size();i++)
      {
        if(mAnsweredList.get(i).getTestId().equalsIgnoreCase(test_id))
        {
          ArrayList<Character> chars = new ArrayList<>();

          for (char ch: mAnsweredList.get(i).getUserAnswer().toCharArray()) {
            chars.add(ch);
          }
          if(chars.size()>0)
          {
            for(int j=0;j<chars.size();j++)
            {
              if(mAnsweredList.get(i).getAnswer().contains(""+chars.get(j)))
                RightAnswerCount = RightAnswerCount+1;
              else
                WrongAnswerCount = WrongAnswerCount+1;
            }
          }

        }
      }
      totalRightAnswer = correctAnswerMark * RightAnswerCount;
      totalWrongAnswer = wrongAnswerMark * WrongAnswerCount;
      totalMarkScored = totalRightAnswer - totalWrongAnswer;


      double totalIn100 = (totalMarkScored) / (overAllCorrectCount * correctAnswerMark);
      double percentage = (totalMarkScored) / (overAllCorrectCount * correctAnswerMark) *100;
      long finalPercentage = Math.round((totalIn100*100) /100) > 0 ? Math.round((totalIn100*100) /100) : 0;

      long toHundred = Math.round((totalIn100*100) /100);
      textView3.setText(""+Math.round(RightAnswerCount));
      textView5.setText(""+mAnsweredList.size()+"/"+mSubscriptionDetailList.size());
      textView7.setText(""+Math.round(WrongAnswerCount));
      textView9.setText(Math.round(totalMarkScored)+"/"+Math.round((overAllCorrectCount * correctAnswerMark)));
      double mark = percentage;
      textView10.setText("Score "+(percentage>0? Math.round(percentage) : 0)+"%");
      txtEmoji.setText(mark < 0 ? (new String(Character.toChars(0x1F620)))/*angry*/ : mark>0 && mark< 40 ? (new String(Character.toChars(0x1F622)))/*crying*/
              : mark>41 && mark< 60 ? (new String(Character.toChars(0x1F616)))/*Confounded Face*/
              : mark>61&& mark< 80 ? (new String(Character.toChars(0x263A)))/*smiling*/
              : (new String(Character.toChars(0x1F603))));/*angry*/
      llResult.setVisibility(View.VISIBLE);
      onTestAttemp.onSubmitted(""+Math.round(RightAnswerCount),
              ""+Math.round(WrongAnswerCount),
              ""+mAnsweredList.size()+"/"+mSubscriptionDetailList.size(),Math.round(totalMarkScored)+"/"+Math.round((overAllCorrectCount * correctAnswerMark)),Math.round(percentage));
      new AsyncRemoveTest().execute();
    }
  }


  private class AsyncRemoveTest extends AsyncTask<String, String, String> {


    @Override
    protected String doInBackground(String... strings) {
      ServiceRequest mRequest = new ServiceRequest(getActivity());
      HashMap<String, String> params = new HashMap<>();
      params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
      mRequest.makeServiceRequest(IConstant.remove_incomplete, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
        @Override
        public void onCompleteListener(String response) {
          SubscriptionTestFragment.isPending = "-1";
          System.out.println("------------remove_incomplete Response----------------" + response);
          String sStatus = "";
          String message = "";


        }

        @Override
        public void onErrorListener(String errorMessage) {
          // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
        }
      });
      return "result";


    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);


      // new AsyncSubjectsRunner(s).execute();
    }
  }
  public String sortString(String inputString)
  {
    // convert input string to char array
    Log.e("Before Short",inputString);
    char tempArray[] = inputString.toCharArray();

    // sort tempArray
    Arrays.sort(tempArray);

    Log.e("After Short",new String(tempArray));
    // return new sorted string
    return new String(tempArray);
  }


  @Override
  public void onClick(View v) {
    switch (v.getId())
    {
      case R.id.btnSubmit:
        if(questionType.equalsIgnoreCase("1") && mAnsweredList.size()>0)
          calculateResult();
        else if(questionType.equalsIgnoreCase("2") && mAnsweredList.size()>0)
          calculateMultiChoiceResult();
        setupRecyclerView(true);
        btnSubmit.setEnabled(false);
        nochangesmade=0;
        btnSubmit.setBackground(getResources().getDrawable(R.drawable.test_submit_btn_inactive));
        testList.smoothScrollToPosition(0);


        //  layoutManager.smoothScrollToPosition(testList, new RecyclerView.State(), 0);


        break;
      /*case R.id.imgCloseInstruction:
        constrainInstruction.setVisibility(View.GONE);
        break;
        case R.id.rlSubmit:
          if(questionType.equalsIgnoreCase("1") && mAnsweredList.size()>0)
            calculateResult();
          else if(questionType.equalsIgnoreCase("2") && mAnsweredList.size()>0)
            calculateMultiChoiceResult();
          setupRecyclerView(true);
          btnSubmit.setVisibility(View.GONE);
          constrainInstruction.setVisibility(View.GONE);
      break;
     case R.id.rlInstruction:
        constrainInstruction.setVisibility(constrainInstruction.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        break;

     case R.id.rlTimer:
       txtStopwatch.setVisibility(View.VISIBLE);
       imgTimer.setVisibility(View.GONE);
       handlerShowTimer.postDelayed(new Runnable() {
         @Override
         public void run() {
           txtStopwatch.setVisibility(View.GONE);
           imgTimer.setVisibility(View.VISIBLE);
         }
       },5000);
        break;*/
    }
  }
  @Override
  public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
    if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
      rlLeft.setVisibility(View.VISIBLE);
      rlRight.setVisibility(View.VISIBLE);
    } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){

      rlLeft.setVisibility(View.GONE);
      rlRight.setVisibility(View.GONE);
    }
  }
  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    if(activity instanceof OnTestAttemp){
      onTestAttemp = (OnTestAttemp) activity;
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    onTestAttemp = null;
  }



  private void saveTest() {
    JSONObject jsonObject = new JSONObject();
    try {
      jsonObject.put("subject_id",subject_id);

      jsonObject.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));
      jsonObject.put("test_id",isTestIdSaved.equals("")?test_id:isTestIdSaved);
      jsonObject.put("type",isTestIdSaved.equals("")?type:testType);
      jsonObject.put("chapter_id",isTestIdSaved.equals("")?chapter_id:testChapterId);
      JSONArray jsonArray = new JSONArray();
      JSONArray jsonArrayAns = new JSONArray();
      if(mAnsweredList.size()>0)
      {
        for(int i=0;i<mAnsweredList.size();i++)
        {
          JSONObject jsonObject1 = new JSONObject();
          jsonObject1.put("q_id",mAnsweredList.get(i).get_id());
          jsonObject1.put("user_answer",mAnsweredList.get(i).getUserAnswer());
          jsonArrayAns.put(i,jsonObject1);
        }
      }
      if(mSubscriptionDetailList.size()>0)
      {
        for(int i=0;i<mSubscriptionDetailList.size();i++)
        {

          jsonArray.put(i,mSubscriptionDetailList.get(i).get_id());
        }
      }
      jsonObject.put("answered",jsonArrayAns);
      jsonObject.put("questions",jsonArray);
      jsonObject.put("duration",sessionManager.udatestarttime());
    } catch (JSONException e) {
      e.printStackTrace();
    }
    loader.showLoader();
    Log.e("jsonObject",jsonObject.toString());
    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, IConstant.incomplete, jsonObject,
            new Response.Listener<JSONObject>(){
              @Override
              public void onResponse(JSONObject response) {

                sessionManager.updateBookMark("Test",chapter_id,subject_id,strLongName,"",chapter_id,chapter_name,"","","");
                //   sessionManager.setLastReadTestId(test_id);
                isTestIdSaved = test_id;
                testChapterId = chapter_id;
                testCMark = crt_ans_mark;
                testEMark = wrng_ans_mark;
                testInstruction = strInstruction;
                testType = type;
                testNumQ = no_of_question;
                Log.e("Response", response.toString());
                loader.dismissLoader();
                try {
                  if(response.getString("status").equals("1")) {
                    EventBusTestRefreshPojo eventBusTestRefreshPojo = new EventBusTestRefreshPojo();
                    eventBusTestRefreshPojo.setStrChapterId(chapter_id);
                    eventBusTestRefreshPojo.setStrLabelId(test_id);
                    eventBusTestRefreshPojo.setStrRefresh("RefreshList");






                    EventBus.getDefault().post(eventBusTestRefreshPojo);
                    EventBus.getDefault().post("closeallboard");





                  }
                } catch (JSONException e) {
                  e.printStackTrace();
                }
              }
            },
            new Response.ErrorListener(){
              @Override
              public void onErrorResponse(VolleyError error) {
                Log.e("Error.Response", error.toString());
                loader.dismissLoader();
                String json = null;
                NetworkResponse response = error.networkResponse;
                if(response != null && response.data != null){
                  switch(response.statusCode){
                    case 400:

                      json = new String(response.data);
                      System.out.println(json);
                      break;
                  }
                }
              }
            })
    {
      @Override
      public Map<String, String> getHeaders() throws AuthFailureError {
        Log.e("Header",sessionManager.getApiHeader().toString());
        return sessionManager.getApiHeader();
      }
    };

    RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
    requestQueue.add(jsonObjectRequest);

  }



  @Override
  public void onSubmit() {
    if(questionType.equalsIgnoreCase("1") && mAnsweredList.size()>0)
      calculateResult();
    else if(questionType.equalsIgnoreCase("2") && mAnsweredList.size()>0)
      calculateMultiChoiceResult();
    setupRecyclerView(true);
    btnSubmit.setEnabled(false);
    btnSubmit.setBackground(getResources().getDrawable(R.drawable.test_submit_btn_inactive));
  }

  @Override
  public void onBackPressed() {
    if(nochangesmade != 0)
    {
      final Dialog alertDialog = new Dialog(getActivity());
      alertDialog.setContentView(R.layout.custom_alert);
      alertDialog.setCancelable(false);
      TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
      TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
      textViewTitle.setText(getActivity().getString(R.string.alert));
      textViewDesc.setText("Do you want to save this test?");
      Button btnOk = alertDialog.findViewById(R.id.btnOk);
      Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
      btnOk.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          alertDialog.dismiss();
          saveTest();
        }
      });
      btnCancel.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          isTestIdSaved = "";
          testChapterId = "";
          testCMark = "";
          testEMark = "";
          testInstruction = "";
          testType = "";
          testNumQ = "";

          new AsyncRemoveTest().execute();
          alertDialog.dismiss();
          getActivity().finish();
        }
      });
      if(!alertDialog.isShowing())
        alertDialog.show();
    }else {
      isTestIdSaved = "";
      testChapterId = "";
      testCMark = "";
      testEMark = "";
      testInstruction = "";
      testType = "";
      testNumQ = "";
      new AsyncRemoveTest().execute();
      getActivity().finish();
    }
  }

  public void intialiseRefreshInterface(RefreshInterface refreshInterface){
    this.refreshInterface = refreshInterface;
  }


  @Override
  public void onBackPressedTest() {

  }

  @Subscribe(threadMode = ThreadMode.MAIN)
  public void onMessageEvent(String  event)
  {
    if(event.equals("close"))
    {
      if(nochangesmade != 0)
      {
        final Dialog alertDialog = new Dialog(getActivity());
        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);
        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(getActivity().getString(R.string.alert));
        textViewDesc.setText("Do you want to save this test?");
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnOk.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            alertDialog.dismiss();
            saveTest();
          }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            isTestIdSaved = "";
            testChapterId = "";
            testCMark = "";
            testEMark = "";
            testInstruction = "";
            testType = "";
            testNumQ = "";

            new AsyncRemoveTest().execute();
            alertDialog.dismiss();
            getActivity().finish();
          }
        });
        if(!alertDialog.isShowing())
          alertDialog.show();
      }else {
        isTestIdSaved = "";
        testChapterId = "";
        testCMark = "";
        testEMark = "";
        testInstruction = "";
        testType = "";
        testNumQ = "";
        new AsyncRemoveTest().execute();
        getActivity().finish();
      }
    }

  };

  @Override
  public void onStart() {
    super.onStart();
    if(!EventBus.getDefault().isRegistered(this))
    {
      EventBus.getDefault().register(this);
    }

  }

  @Override
  public void onStop() {
    super.onStop();
    if(EventBus.getDefault().isRegistered(this))
    {
      EventBus.getDefault().unregister(this);
    }
  }
}
