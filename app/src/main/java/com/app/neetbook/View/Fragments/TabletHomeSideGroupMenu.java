package com.app.neetbook.View.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.app.neetbook.Interfaces.OnFragmentInteractionListener;
import com.app.neetbook.Interfaces.OnGroupHomeMenuClickListener;
import com.app.neetbook.Interfaces.OnGroupMenuSelectedListener;
import com.app.neetbook.Interfaces.OnMobileNumberChanged;
import com.app.neetbook.Model.Groups;
import com.app.neetbook.Model.Groupslist;
import com.app.neetbook.Model.TabSideMenu;
import com.app.neetbook.R;
import com.app.neetbook.Utils.Data.DrawerActivityData;
import com.app.neetbook.Utils.Data.GroupMenuData;
import com.app.neetbook.Utils.EventBusPojo.ActionEvent;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.ThreadExecutor.AppExecutors;
import com.app.neetbook.Utils.widgets.DisplayOrientation;
import com.app.neetbook.View.DrawerContentSlideActivity;
import com.app.neetbook.View.TabletHome;
import com.app.neetbook.homepageRoom.GroupslistDatabase;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;


public class TabletHomeSideGroupMenu extends Fragment implements View.OnClickListener {

   OnGroupMenuSelectedListener listener;
   ImageView imgGroupHome;
   OnGroupHomeMenuClickListener homeMenuClickListener;
   TextView txtName;
   SessionManager sessionManager;
    GroupslistDatabase groupslistDatabase;
    AppExecutors exector;
   private List<Groupslist> homeMenus = new ArrayList<>();
    ListView menuListView;
    GroupMenuAdapter sideMenuAdapter;

    public static TabletHomeSideGroupMenu newInstance(String param1, String param2) {
        TabletHomeSideGroupMenu fragment = new TabletHomeSideGroupMenu();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getActivity());
        exector=new AppExecutors();
        EventBus.getDefault().register(this);
        if (getArguments() != null) {

        }
    }

    private void getGroupList() {
        try {

            exector.diskIO().execute(new Runnable() {
                @Override
                public void run() {

                    try{

                        groupslistDatabase = GroupslistDatabase.getAppDatabase(getActivity());

//                        Log.e("GroupList", new Gson().toJson(groupslistDatabase.groupslistDao().getroupsList().toString()));
//
//                        List<Groups> data=groupslistDatabase.groupslistDao().getroupsList();
//
//                        List list=data.get(0).getGroupslists();

                        // List list = data.

                        homeMenus= groupslistDatabase.groupslistDao().getAll();

                        ActionEvent event=new ActionEvent();
                        event.setAction("successG");
                        EventBus.getDefault().post(event);

                    }catch(Exception e){

                        System.out.println("Exception---"+e.toString());
                    }

                }
            });

        } catch (Exception e) {

            System.out.println("Exception---" + e.toString());

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_tablet_home_side_group_menu, container, false);
        menuListView = (ListView) rootView.findViewById(android.R.id.list);
        imgGroupHome = (ImageView) rootView.findViewById(R.id.imgGroupHome);
        txtName = (TextView) rootView.findViewById(R.id.txtName);
        txtName.setText(sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) != null && !sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("nill") && !sessionManager.getUserDetails().get(SessionManager.KEY_FNAME).equals("") ? sessionManager.getUserDetails().get(SessionManager.KEY_FNAME) : "");
        imgGroupHome.setOnClickListener(this);
        txtName.setOnClickListener(this);


        getGroupList();

        return rootView;
    }

    private void setAdapter(){

        sideMenuAdapter = new GroupMenuAdapter(getActivity(),homeMenus);
        menuListView.setAdapter(sideMenuAdapter);
        sideMenuAdapter.setSelectedIndex(GroupMenuData.currentSelectedMenu);
        menuListView.setOnItemClickListener(onMenuItemClickListener);
    }

    AdapterView.OnItemClickListener onMenuItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Groupslist sideMenu = (Groupslist)adapterView.getItemAtPosition(i);
            if(listener!=null)
                listener.onGroupMenuSelected(sideMenu,i);
        }
    };
    private ArrayList<TabSideMenu> generateGroupList() {
        ArrayList<TabSideMenu> sideMenus = new ArrayList<TabSideMenu>();

        TabSideMenu tabSideMenu = new TabSideMenu();
        tabSideMenu.setId("1");
        tabSideMenu.setName("Group 1");
        sideMenus.add(tabSideMenu);

        TabSideMenu tabSideMenu2 = new TabSideMenu();
        tabSideMenu2.setId("2");
        tabSideMenu2.setName("Group 2");

        sideMenus.add(tabSideMenu2);

        TabSideMenu tabSideMenu3 = new TabSideMenu();
        tabSideMenu3.setId("3");
        tabSideMenu3.setName("Group 3");

        sideMenus.add(tabSideMenu3);

        TabSideMenu tabSideMenu4 = new TabSideMenu();
        tabSideMenu4.setId("4");
        tabSideMenu4.setName("Group 4");

        sideMenus.add(tabSideMenu4);

        TabSideMenu tabSideMenu5 = new TabSideMenu();
        tabSideMenu5.setId("5");
        tabSideMenu5.setName("Group 5");

        sideMenus.add(tabSideMenu5);

        TabSideMenu tabSideMenu6 = new TabSideMenu();
        tabSideMenu6.setId("6");
        tabSideMenu6.setName("Group 6");

        sideMenus.add(tabSideMenu6);



        return sideMenus;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof  OnGroupMenuSelectedListener){
            listener = (OnGroupMenuSelectedListener) activity;
            homeMenuClickListener = (OnGroupHomeMenuClickListener) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
        homeMenuClickListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.imgGroupHome: {
                DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                DrawerActivityData.lastSelectedMenuId = "2";
                DrawerActivityData.currentIndex = 1;
                Bundle bundle1 = new Bundle();
                TabSideMenu tabSideMenu = new TabSideMenu();
                tabSideMenu.setId("2");
                tabSideMenu.setImage_id_acative(R.drawable.complete_proile_inactive);
                tabSideMenu.setImage_id_inactive(R.drawable.complete_profile_active);
                tabSideMenu.setName(getString(R.string.my_profile));
                bundle1.putSerializable("sideMenu", tabSideMenu);
                DrawerActivityData.bundle = bundle1;
                startActivity(new Intent(getActivity(), DrawerContentSlideActivity.class).putExtra("sideMenu", tabSideMenu));
                TabletHome.PagesActivity.finish();
            }
                break;
            case R.id.txtName:
                DrawerContentSlideActivity.drawerContentSlideActivity.finish();
                DrawerActivityData.lastSelectedMenuId = "2";
                DrawerActivityData.currentIndex = 1;
                Bundle bundle1 = new Bundle();
                TabSideMenu tabSideMenu = new TabSideMenu();
                tabSideMenu.setId("2");
                tabSideMenu.setImage_id_acative(R.drawable.complete_proile_inactive);
                tabSideMenu.setImage_id_inactive(R.drawable.complete_profile_active);
                tabSideMenu.setName(getString(R.string.my_profile));
                bundle1.putSerializable("sideMenu", tabSideMenu);
                DrawerActivityData.bundle = bundle1;
                startActivity(new Intent(getActivity(), DrawerContentSlideActivity.class).putExtra("sideMenu",tabSideMenu));
                TabletHome.PagesActivity.finish();
                break;
        }
    }

    public class GroupMenuAdapter extends BaseAdapter {
        private int selectedIndex;
        List<Groupslist> groupMenuList;
        Context context;

        public GroupMenuAdapter(Context context,List<Groupslist> groupMenuList){
            this.context = context;
            this.groupMenuList = groupMenuList;
            selectedIndex = 0;
        }

        @Override
        public int getCount() {
            return groupMenuList==null?0:groupMenuList.size();
        }

        @Override
        public Groupslist getItem(int i) {
            return groupMenuList==null?null:groupMenuList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            if(view==null)
                view = LayoutInflater.from(context).inflate(R.layout.tab_group_menu_item,null);
            Groupslist sideMenu = groupMenuList.get(i);
            final int position = i;
            TextView txtGroupName = (TextView)view.findViewById(R.id.txtGroupName);
            txtGroupName.setText(sideMenu.getHomelongName());

            if (i == selectedIndex) {

                final Groupslist sideMenu_new = sideMenu;
                view.setBackground(context.getResources().getDrawable(R.drawable.selected_menu_bg));
                txtGroupName.setTextColor(context.getResources().getColor(R.color.primary_meroon));
                listener.onGroupMenuSelected(sideMenu_new,position);
            } else {

                view.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
                txtGroupName.setTextColor(context.getResources().getColor(R.color.white));
            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedIndex = position;
                    notifyDataSetChanged();

                }
            });
            return view;
        }
        public void setSelectedIndex(int ind)
        {
            selectedIndex = ind;
            notifyDataSetChanged();

        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ActionEvent event) {

        if(event.getAction().equals("successG")){
            setAdapter();
//            Intent intent = new Intent("STRING_ID_FOR_BRODCAST");
//            intent.putExtra("groupId",homeMenus.get(0).getGroupid());
//            getActivity().sendBroadcast(intent);
//            sideMenuAdapter.notifyDataSetChanged();
        }
    }
}
