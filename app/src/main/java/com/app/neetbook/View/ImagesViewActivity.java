package com.app.neetbook.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.neetbook.Adapter.ImageViewPagerAdapter;
import com.app.neetbook.Adapter.InnerImageListAdapter;
import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.R;
import com.app.neetbook.Utils.OnSwipeTouchListener;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.View.SideMenu.SubscriptionArticleActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class ImagesViewActivity extends AppCompatActivity {
    ViewPager viewPager;
    TextView textView;
    ImageView imageView2,btn_close;
    ConstraintLayout titleConstrain,llParent;
    int selectablePosition = 0;
    private ArrayList<String> itemsList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images_view);
        StatusBarColorChange.updateStatusBarColor(ImagesViewActivity.this,getResources().getColor(R.color.bg_white));
        /*StatusBarColorChange.updateStatusBarColor(ImagesViewActivity.this,getIntent().getStringExtra("from").equalsIgnoreCase("MCQ")?getResources().getColor(R.color.subsDetail_blue) : getIntent().getStringExtra("from").equalsIgnoreCase("Test")?getResources().getColor(R.color.test_completed_brown) : getResources().getColor(R.color.article_content_page_pink));*/
       /* getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);*/
        init();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }

    private void init() {
        btn_close = findViewById(R.id.btn_close);
        imageView2 = findViewById(R.id.imageView2);
        textView = findViewById(R.id.textView);
        viewPager = findViewById(R.id.viewPager);
        titleConstrain = findViewById(R.id.titleConstrain);
        llParent = findViewById(R.id.llParent);
        titleConstrain.setBackgroundColor(getIntent().getStringExtra("from").equalsIgnoreCase("MCQ")?getResources().getColor(R.color.subsDetail_blue) : getIntent().getStringExtra("from").equalsIgnoreCase("Test")?getResources().getColor(R.color.test_completed_brown) : getResources().getColor(R.color.article_content_page_pink));

        textView.setText(getIntent().getStringExtra("title"));
        itemsList = (ArrayList<String>) getIntent().getSerializableExtra("image");
        selectablePosition = getIntent().hasExtra("position")?Integer.parseInt(getIntent().getStringExtra("position")) : 0;
        ImageViewPagerAdapter viewPagerAdapter = new ImageViewPagerAdapter(this, itemsList);

        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(selectablePosition);
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        llParent.setOnTouchListener(new OnSwipeTouchListener(ImagesViewActivity.this) {
            public void onSwipeTop() {
                //Toast.makeText(SubscriptionArticleActivity.this, "top", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeRight() {
                //Toast.makeText(SubscriptionArticleActivity.this, "right", Toast.LENGTH_SHORT).show();
                finish();
            }
            public void onSwipeLeft() {

            }
            public void onSwipeBottom() {
                //Toast.makeText(SubscriptionArticleActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });
    }
}
