package com.app.neetbook.View.SideMenu;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.neetbook.Adapter.ArticleTabsAdapter;
import com.app.neetbook.Interfaces.OnMobileHomeMenuSelectedListioner;
import com.app.neetbook.Model.HeadData;
import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.McqExam;
import com.app.neetbook.R;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.OnSwipeTouchListener;
import com.app.neetbook.Utils.SessionManager;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;


public class TabMcqMainFragment extends Fragment implements View.OnClickListener {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Context context;
    Loader loader;
    SessionManager sessionManager;
    private CustomAlert customAlert;
    private int page = 1;
    int loadMoreEnable = 0;
    private int totalSubsCount;
    String strSubjectId= "",strSubjectName= "",strChaptortName= "",strHeadId= "",strChapterId= "",loadingType= "",strSuperHeading= "";
    ViewPagerAdapter viewPagerAdapter;
    private ImageView imageView2,imageView3;
    private TextView textView,textView3,txtSurgical;

    private ArrayList<HeadData> headDataList;
    private int currentSelectedPosition = 0;
    ArticleTabsAdapter homeGroupNameAdapter;
    LinearLayoutManager layoutManager;
    LinearSnapHelper snapHelper;
    RecyclerView recyclerViewGroups;
    Typeface tfBold,tfMedium,tfRegular;
    //private RelativeLayout rlParent;
    public TabMcqMainFragment(String strHeadId, String strChapterId, String strSubjectId, String strChaptortName, String strSubjectName, String loadingType, String strSuperHeading) {
        this.strHeadId = strHeadId;
        this.strChapterId = strChapterId;
        this.strSubjectId = strSubjectId;
        this.strChaptortName = strChaptortName;
        this.strSubjectName = strSubjectName;
        this.loadingType = loadingType;
        this.strSuperHeading = strSuperHeading;
    }

   

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getActivity());
        customAlert = new CustomAlert(getActivity());
        loader = new Loader(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab_mcq_main, container, false);
        init(view);
        return view;
    }

    private void init(View view) {

        viewPager = view.findViewById(R.id.viewpager_subscription_category);
        tabLayout = view.findViewById(R.id.tabs_subscription_category);
        recyclerViewGroups = view.findViewById(R.id.recyclerViewGroups);
        viewPager = view.findViewById(R.id.viewpager_subscription_category);
        tabLayout = view.findViewById(R.id.tabs_subscription_category);
        imageView2 = view.findViewById(R.id.imageView2);
        imageView3 = view.findViewById(R.id.imageView3);
        textView = view.findViewById(R.id.textView);
        txtSurgical = view.findViewById(R.id.textView2);
        textView3 = view.findViewById(R.id.textView3);
        /*rlParent = view.findViewById(R.id.rlParent);*/
        headDataList = new ArrayList<>();
        /*if(getIntent().getExtras() != null)
        {
            strHeadId = getIntent().getStringExtra("head_id");
            strSubjectId = getIntent().getStringExtra("subject_id");
            strSubjectName = getIntent().getStringExtra("subject_name");
            strChaptortName = getIntent().getStringExtra("chapter_name");
            textView.setText(strSubjectName);
            txtSurgical.setText(strChaptortName);
            loadingType = getIntent().getStringExtra("loading_type");
            textView3.setText(getIntent().getStringExtra("superHeading"));
            textView3.setVisibility(!getIntent().getStringExtra("superHeading").equalsIgnoreCase("No")?View.VISIBLE:View.GONE);
            if(!getIntent().getStringExtra("loading_type").equals("1"))
            {
                strChapterId = getIntent().getStringExtra("chapter_id");
            }
        }*/
        createSnap();
        setupViewPager(viewPager);
        listener();
        setFontSize();
        
    }

    private void setFontSize() {

        if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
            textView.setTextAppearance(getActivity(),R.style.textViewSmallSubjectName);
            txtSurgical.setTextAppearance(getActivity(),R.style.textViewSmallChaptName);
            textView3.setTextAppearance(getActivity(),R.style.textViewSmallChaptName);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
            textView.setTextAppearance(getActivity(),R.style.textViewMediumSubjectName);
            txtSurgical.setTextAppearance(getActivity(),R.style.textViewMediumChaptName);
            textView3.setTextAppearance(getActivity(),R.style.textViewMediumChaptName);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
            textView.setTextAppearance(getActivity(),R.style.textViewLargeSubjectName);
            txtSurgical.setTextAppearance(getActivity(),R.style.textViewLargeChaptName);
            textView3.setTextAppearance(getActivity(),R.style.textViewLargeChaptName);
        }

        textView.setTypeface(tfMedium);
        txtSurgical.setTypeface(tfMedium);
        textView3.setTypeface(tfMedium);
    }
    private void listener() {

        imageView3.setOnClickListener(this);
        imageView2.setOnClickListener(this);


    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageView2:
                //finish();
                break;

            case R.id.imageView3:
                /* finish();*/
                break;
        }
    }

    private void setupViewPager(final ViewPager viewPager) {
        viewPagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager(),getActivity());

        if(loadingType.equals("1")) {
            if (SubscriptionDetailActivity.mcqHeadList.size() > 0) {
                for (int i = 0; i < SubscriptionDetailActivity.mcqHeadList.size(); i++) {
                    if (SubscriptionDetailActivity.mcqHeadList.get(i).isSH.equals("Yes")) {

                        HeadData data = new HeadData();
                        data._id = SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(0)._id;
                        data.short_name = SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(0).short_name;
                        data.long_name = SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(0).long_name;
                        headDataList.add(data);
                        viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(strSubjectId, strChapterId, SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(0)._id, SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(0).long_name,loadingType), SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(0).short_name);


                    } else {
                        if (SubscriptionDetailActivity.mcqHeadList.get(i).headData.size() > 0) {
                            for (int j = 0; j < SubscriptionDetailActivity.mcqHeadList.get(i).headData.size(); j++) {

                                HeadData data = new HeadData();
                                data._id = SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(j)._id;
                                data.short_name = SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(j).short_name;
                                data.long_name = SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(j).long_name;
                                headDataList.add(data);
                                viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(strSubjectId, strChapterId, SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(j)._id, SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(j).long_name,loadingType), SubscriptionDetailActivity.mcqHeadList.get(i).headData.get(j).short_name);
                            }
                        }
                    }
                }
            }
        }else if(loadingType.equals("2") || loadingType.equals("3"))
        {
            if (SubscriptionDetailActivity.mcqExamArrayList.size() > 0) {
                for (int i = 0; i < SubscriptionDetailActivity.mcqExamArrayList.size(); i++) {
                    HeadData data = new HeadData();
                    data._id = SubscriptionDetailActivity.mcqExamArrayList.get(i).get_id();
                    data.short_name = SubscriptionDetailActivity.mcqExamArrayList.get(i).getShort_name();
                    data.long_name = SubscriptionDetailActivity.mcqExamArrayList.get(i).getLong_name();
                    headDataList.add(data);
                    viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(strSubjectId, strChapterId, SubscriptionDetailActivity.mcqExamArrayList.get(i).get_id(), SubscriptionDetailActivity.mcqExamArrayList.get(i).getLong_name(),loadingType), SubscriptionDetailActivity.mcqExamArrayList.get(i).getLong_name());

                }

            }
            Log.e("HeadData",headDataList.toString());
        }


        if(headDataList.size()>0)
        {
            for (int i=0;i<headDataList.size();i++)
            {
                if(strHeadId.equalsIgnoreCase(headDataList.get(i)._id))
                    currentSelectedPosition = i;
            }
        }
        setGroupAdapter(currentSelectedPosition);
        layoutManager = ((LinearLayoutManager)recyclerViewGroups.getLayoutManager());
        int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
        int centeredItemPosition = totalVisibleItems / 2;
        recyclerViewGroups.smoothScrollToPosition(currentSelectedPosition != 0 ?currentSelectedPosition-1 : currentSelectedPosition);
        recyclerViewGroups.setScrollY(centeredItemPosition);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(currentSelectedPosition);
        tabLayout.setupWithViewPager(viewPager);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(viewPagerAdapter.getTabView(i));
            }
        }

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setGroupAdapter(position);
                layoutManager = ((LinearLayoutManager)recyclerViewGroups.getLayoutManager());
                int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                int centeredItemPosition = totalVisibleItems / 2;
                recyclerViewGroups.smoothScrollToPosition(position != 0 ?position-1 : position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    private void setGroupAdapter(int position) {

        homeGroupNameAdapter = new ArticleTabsAdapter(headDataList, getActivity(),position);
        recyclerViewGroups.setLayoutManager(new LinearLayoutManager(getActivity(),RecyclerView.HORIZONTAL,false));
        snapHelper.attachToRecyclerView(recyclerViewGroups);
        recyclerViewGroups.setAdapter(homeGroupNameAdapter);

        recyclerViewGroups.scrollToPosition(position);
        homeGroupNameAdapter.setOnGroupMenuSelectedListener(new OnMobileHomeMenuSelectedListioner() {
            @Override
            public void onGroupSelected(String groupId, int position) {
                viewPager.setCurrentItem(position);
                layoutManager = ((LinearLayoutManager)recyclerViewGroups.getLayoutManager());
                int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                int centeredItemPosition = totalVisibleItems / 2;
                recyclerViewGroups.smoothScrollToPosition(position != 0 ?position-1 : position);
                recyclerViewGroups.setScrollY(centeredItemPosition);
            }
        });

    }
    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
    }
    private class ViewPagerAdapter extends FragmentPagerAdapter {

        List<Fragment> fragmentList = new ArrayList<>();
        List<String> fragmentTitles = new ArrayList<>();
        Context mContext;
        TextView tabTitile;


        ViewPagerAdapter(FragmentManager fragmentManager,
                         FragmentActivity activity) {
            super(fragmentManager);
            mContext = activity;
        }

        @Override
        public Fragment getItem(int i) {
            return new SubscrptionDetailListFragment(strSubjectId,strChapterId,loadingType.equals("1")?headDataList.get(i)._id : SubscriptionDetailActivity.mcqExamArrayList.get(i).get_id(),loadingType.equals("1")?headDataList.get(i).long_name : SubscriptionDetailActivity.mcqExamArrayList.get(i).getLong_name(),loadingType);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitles.get(position);
        }

        void addFragment(Fragment fragment, String name) {
            fragmentList.add(fragment);
            fragmentTitles.add(name);
        }

        View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_items, null);
            tabTitile = v.findViewById(R.id.txtTabTitle);
            tabTitile.setText(fragmentTitles.get(position));
            tabTitile.setTextColor(getResources().getColor(R.color.white));
            return v;
        }
    }

}
