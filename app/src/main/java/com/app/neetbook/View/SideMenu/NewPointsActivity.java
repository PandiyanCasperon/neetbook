package com.app.neetbook.View.SideMenu;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.app.neetbook.Adapter.ArticlePagePointsAdapter;
import com.app.neetbook.Adapter.PointsStickyHeaderAdapter;
import com.app.neetbook.Model.ArticleContent.ArticleChildItem;
import com.app.neetbook.Model.ArticleContent.ArticleItem;
import com.app.neetbook.Model.ArticlePointsList;
import com.app.neetbook.Model.Points;
import com.app.neetbook.R;
import com.app.neetbook.StickeyHeader.StickyLinearLayoutManager;
import com.app.neetbook.Utils.OnSwipeTouchListener;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.Thread.AppExecutors;
import com.app.neetbook.View.DynamicFragmentActivity;
import com.app.neetbook.View.ScrollingActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.facebook.shimmer.ShimmerFrameLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.app.neetbook.serviceRequest.IConstant.BaseUrl;

public class NewPointsActivity extends AppCompatActivity {
    WebView webView;
    ShimmerFrameLayout shimmerFrameLayout;
    TextView PointsTextView, chapterShortNameTextView;
    ImageView back;
    SessionManager sessionManager;
    ServiceRequest mRequest;
    String SubjectId, ChapterId, PointsName, ChapterShortName,Position,CurrentPageWebUrl;
    AppExecutors appExecutors;
    RecyclerView recyclerPoints;
    ArrayList<ArticlePointsList> pointsList;
    ArrayList<Points> pointsArrayList;
    Points points;
    ArticlePagePointsAdapter pointsStickyHeaderAdapter;
    private RecyclerView recyclerViewPoints;
    private RelativeLayout  rlParent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_points);

        sessionManager = new SessionManager(this);
        appExecutors = new AppExecutors();
        pointsList = new ArrayList<>();
        pointsArrayList=new ArrayList<>();
        webView = (WebView) findViewById(R.id.webView);
        shimmerFrameLayout = (ShimmerFrameLayout) findViewById(R.id.mShimmerViewContainer12);
        PointsTextView = (TextView) findViewById(R.id.pointTitle);
        chapterShortNameTextView = (TextView) findViewById(R.id.chapterShortName);
        back = (ImageView) findViewById(R.id.imageView15);
        recyclerPoints = (RecyclerView) findViewById(R.id.recycler);
        rlParent = findViewById(R.id.rlParent);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.alerter_slide_in_from_left, R.anim.alerter_slide_out_to_right);
            }
        });
        rlParent.setOnTouchListener(new OnSwipeTouchListener(NewPointsActivity.this) {
            public void onSwipeTop() {
                //Toast.makeText(SubscriptionArticleActivity.this, "top", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeRight() {
                //Toast.makeText(SubscriptionArticleActivity.this, "right", Toast.LENGTH_SHORT).show();
                onBackPressed();
                overridePendingTransition(R.anim.alerter_slide_in_from_left, R.anim.alerter_slide_out_to_right);
            }

            public void onSwipeLeft() {
             }

            public void onSwipeBottom() {
                //Toast.makeText(SubscriptionArticleActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });



        if (getIntent().getExtras() != null) {
            SubjectId = getIntent().getStringExtra("SUBJECT_ID");
            ChapterId = getIntent().getStringExtra("CHAPTER_NAME");
            PointsName = getIntent().getStringExtra("POINTS_NAME");
            ChapterShortName = getIntent().getStringExtra("CHAPTER_SHORT_NAME");
            Position=getIntent().getStringExtra("POSITION");
            PointsTextView.setText(PointsName);
            chapterShortNameTextView.setText(ChapterShortName);
        }
       // getArticleContent();

        webView.getSettings().setJavaScriptEnabled(true); // enable javascript
        webView.getSettings().setTextZoom(100);
        webView.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

            }

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });
        getArticleContent();
       // webView.loadUrl("https://doctor.casperon.co/user/get/article/5e8a1856ed2f783a7b4bf65a/5e8c5ae6ed2f783a7b4bf7c1?mode=&size=small");
    }

    private void getArticleContent() {
        mRequest = new ServiceRequest(NewPointsActivity.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("subject_id", SubjectId);
        params.put("chapter_id", ChapterId);

        mRequest.makeServiceRequest(IConstant.pointsContent, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String sStatus = "", id, point_name, short_name, long_name, url, title;

                try {
                    //shimmerFrameLayout.setVisibility(View.GONE);\
                    JSONObject obj = new JSONObject(response);
                    System.out.println("print josn obj: "+ obj);
                    sStatus = obj.getString("status");
                    if (sStatus.equals("1")) {
                        JSONObject res = obj.getJSONObject("response");
                        JSONArray jsonarray=res.getJSONArray("points");
                        for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject object = jsonarray.getJSONObject(i);
                            id = object.getString("_id");
                            point_name = object.getString("point_name");
                            short_name = object.getString("short_name");
                            long_name = object.getString("long_name");
                            url=object.getString("url");
                            points = new Points(id, point_name, short_name, long_name,url, pointsList);
                            pointsArrayList.add(points);

                        }
                    }
                    CurrentPageWebUrl = BaseUrl + pointsArrayList.get(Integer.parseInt(Position)).getUrl() + "?mode=";
                    if (sessionManager.isNightModeEnabled())
                        CurrentPageWebUrl += "dark" + "&size=" + sessionManager.getFontSize();
                    else

                        CurrentPageWebUrl += "" + "&size=" + sessionManager.getFontSize();
                    webView.setHorizontalScrollBarEnabled(true);

                    System.out.println("url loading start");

                  //  shimmerFrameLayout.setVisibility(View.GONE);
                   // webView.setVisibility(View.VISIBLE);
                    webView.loadUrl(CurrentPageWebUrl);
                   // setPointsList();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener(String errorMessage) {
                System.out.println(errorMessage);
            }
        });
    }

    private void setPointsList() {


        pointsStickyHeaderAdapter = new ArticlePagePointsAdapter(pointsArrayList,this);
       // recyclerPoints.setHasFixedSize(true);
       // recyclerPoints.setLayoutManager(new LinearLayoutManager(this));
        recyclerPoints.setAdapter(pointsStickyHeaderAdapter);
        StickyLinearLayoutManager layoutManager = new StickyLinearLayoutManager(this, pointsStickyHeaderAdapter) {
            @Override
            public boolean isAutoMeasureEnabled() {
                return true;
            }

            @Override
            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                RecyclerView.SmoothScroller smoothScroller = new TopSmoothScroller(recyclerView.getContext());
                smoothScroller.setTargetPosition(position);
                startSmoothScroll(smoothScroller);
            }

            class TopSmoothScroller extends LinearSmoothScroller {

                TopSmoothScroller(Context context) {
                    super(context);
                }

                @Override
                public int calculateDtToFit(int viewStart, int viewEnd, int boxStart, int boxEnd, int snapPreference) {
                    return boxStart - viewStart;
                }
            }
        };
        layoutManager.elevateHeaders(5);
        recyclerViewPoints.setLayoutManager(layoutManager);
        recyclerViewPoints.setAdapter(pointsStickyHeaderAdapter);
        layoutManager.setStickyHeaderListener(new StickyLinearLayoutManager.StickyHeaderListener() {
            @Override
            public void headerAttached(View headerView, int adapterPosition) {
                Log.d("StickyHeader", "Header Attached : " + adapterPosition);
            }

            @Override
            public void headerDetached(View headerView, int adapterPosition) {
                Log.d("StickyHeader", "Header Detached : " + adapterPosition);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.alerter_slide_in_from_left, R.anim.alerter_slide_out_to_right);
    }
}
