package com.app.neetbook.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.neetbook.Model.PointsList;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ActionBarActivityNeetBook;
import com.app.neetbook.Utils.OnSwipeTouchListener;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.View.Fragments.AboutUsFragment;
import com.app.neetbook.View.Fragments.ChangeMobileFragment;
import com.app.neetbook.View.Fragments.ChangePinFragment;
import com.app.neetbook.View.Fragments.CompleteProfileFragment;
import com.app.neetbook.View.Fragments.ContactUsFragment;
import com.app.neetbook.View.Fragments.FeedBackFragment;
import com.app.neetbook.View.Fragments.HelpFragment;
import com.app.neetbook.View.Fragments.MyProfileFragment;
import com.app.neetbook.View.Fragments.TermsAndConditions;
import com.app.neetbook.View.SideMenu.SubscriptionArticleActivity;
import com.app.neetbook.View.SideMenu.SubscriptionDetailActivity;
import com.app.neetbook.View.SideMenu.SubscriptionPointsActivity;
import com.app.neetbook.View.SideMenu.SubscriptionPointsFragment;
import com.app.neetbook.View.SideMenu.SubscrptionDetailListFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class DynamicFragmentActivity extends ActionBarActivityNeetBook implements View.OnClickListener{
    TextView txtChapter,txtPageTitle;
    SessionManager sessionManager;
    RelativeLayout rlBack;
    String strSubjectId = "",strHead_id = "",strLongName = "",strchapterName = "";
    String strPointsTitle = "",strPointsContent = "",strChapterNmae = "";
    public static DynamicFragmentActivity dynamicFragmentActivity;
    Typeface tfBold,tfMedium,tfRegular;
    RelativeLayout llTitleContainer;
    RelativeLayout llParent;
    Fragment fragment;
    private RelativeLayout rlParent;
    ArrayList<PointsList> pintsTitleContentList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarColorChange.updateStatusBarColor(DynamicFragmentActivity.this,getIntent().getStringExtra("isPoints").equalsIgnoreCase("No")?getResources().getColor(R.color.subsDetail_blue):getResources().getColor(R.color.article_points_green));
        setContentView(R.layout.activity_dynamic_fragment);

        init();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }
    private void init() {
        tfBold  = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium  = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular  = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaRegular.otf");
        dynamicFragmentActivity = this;
        rlBack = findViewById(R.id.rlBack);
        txtPageTitle = findViewById(R.id.txtPageTitle);
        txtChapter = findViewById(R.id.txtChapter);
        rlParent = findViewById(R.id.rlParent);
        llTitleContainer = findViewById(R.id.llTitleContainer);
        sessionManager = new SessionManager(DynamicFragmentActivity.this);

  /*      if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
            txtPageTitle.setTextAppearance(DynamicFragmentActivity.this,R.style.textViewSmallSummaryTitle);
            txtChapter.setTextAppearance(DynamicFragmentActivity.this,R.style.textViewSmallChaptName);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
            txtPageTitle.setTextAppearance(DynamicFragmentActivity.this,R.style.textViewMediumSummaryTitle);
            txtChapter.setTextAppearance(DynamicFragmentActivity.this,R.style.textViewMediumChaptName);
        }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
            txtPageTitle.setTextAppearance(DynamicFragmentActivity.this,R.style.textViewLargeSummaryTitle);
            txtChapter.setTextAppearance(DynamicFragmentActivity.this,R.style.textViewLargeChaptName);
        }*/

        txtPageTitle.setTypeface(tfMedium);
        txtChapter.setTypeface(tfMedium);



        if(getIntent().getExtras()!=null && getIntent().getStringExtra("isPoints").equalsIgnoreCase("No"))
        {
            strSubjectId = getIntent().getStringExtra("subject_id");
            strHead_id = getIntent().getStringExtra("head_id");
            strLongName = getIntent().getStringExtra("long_name");
            strchapterName = getIntent().getStringExtra("chapterName");
            txtPageTitle.setText(getIntent().getStringExtra("title"));
            llTitleContainer.setBackgroundColor(getResources().getColor(R.color.subsDetail_blue));
           // txtChapter.setVisibility(View.GONE);
            txtChapter.setText(strchapterName);
            fragment = new SubscrptionDetailListFragment(strSubjectId,"hide",strHead_id,strLongName,"1");
        }else if(getIntent().getExtras()!=null && getIntent().getStringExtra("isPoints").equalsIgnoreCase("Yes"))
        {
            strPointsTitle = getIntent().getStringExtra("pointsTitle");
            strPointsContent = getIntent().getStringExtra("pointsContent");
            strChapterNmae = getIntent().getStringExtra("chapterName");
            llTitleContainer.setBackgroundColor(getResources().getColor(R.color.article_points_green));
           // txtChapter.setVisibility(View.VISIBLE);
            txtChapter.setText(getIntent().getStringExtra("chapterName"));
            txtPageTitle.setText(strPointsTitle);
            JsonParsing(strPointsContent);
            fragment = new SubscriptionPointsFragment(strSubjectId,"nill",strHead_id,strPointsTitle,strPointsContent,pintsTitleContentList,"",0);
        }


        listener();
        rlBack.setOnClickListener(this);
        switchToFragment();
    }

    private void JsonParsing(String strPointsContent){

        try{

            pintsTitleContentList=new ArrayList<>();

            JSONArray content_array=new JSONArray(strPointsContent);

            if(content_array.length()>0){

                for(int i=0;i<content_array.length();i++){

                    PointsList point=new PointsList();

                    JSONObject body_object=content_array.getJSONObject(i);

                    String Body=body_object.getString("body");
                    String Title=body_object.getString("title");

                    point.setStrBody(Body);
                    point.setStrTitile(Title);

                    pintsTitleContentList.add(point);
                }
            }

        }catch(Exception e){

        }

    }

    private void listener() {
        rlParent.setOnTouchListener(new OnSwipeTouchListener(DynamicFragmentActivity.this) {
            public void onSwipeTop() {
                //Toast.makeText(SubscriptionArticleActivity.this, "top", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeRight() {
                //Toast.makeText(SubscriptionArticleActivity.this, "right", Toast.LENGTH_SHORT).show();
                finish();
            }
            public void onSwipeLeft() {

            }
            public void onSwipeBottom() {
                //Toast.makeText(SubscriptionArticleActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.rlBack:
                finish();
                break;
        }
    }

    private void switchToFragment() {

        setFragment(fragment);

    }

    private void setFragment(Fragment fragment) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameContainer, fragment);
        fragmentTransaction.commit();

    }
}
