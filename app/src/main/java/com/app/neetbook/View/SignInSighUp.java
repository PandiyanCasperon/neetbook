package com.app.neetbook.View;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;

import androidx.annotation.ColorInt;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Data.EnterPinData;
import com.app.neetbook.Utils.Data.LoginData;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.Utils.alerter.AlertBox;
import com.app.neetbook.Utils.widgets.DisplayOrientation;
import com.app.neetbook.Utils.widgets.VerticalTextView;
import com.app.neetbook.Utils.widgets.Pinview;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.HashMap;

public class SignInSighUp extends AppCompatActivity implements View.OnClickListener {

    EditText etsetpin,etenterotp,etemailandmobile;
    View viewentrotp,viewentrpin,viewsigninInActive,viewsignupInActive,viewsigninActive,txtSignInInActive,viewsignupActive,viewemailmobileInActive;
    LinearLayout llsigninTitle,llsignupTitle,llotp,llLoginContainer,ll_enter_mobile_email,llpin,llLSignupContainer;
    TextView txtContactUs,txtContactUsSignUp,txtTermsConditions,txtTermsConditionsSignUp,txtSignInActive,txtSignUpInActive,txtSignUpActive,txtResendOTPSignUp,txtResendOTPSignIn;
    RelativeLayout signUPSubmit,rlReset,rlenter_mobile_email,llmailSignUp,rlSignInPin,rlSignInRight,rlSignUpLeft;
    ConstraintLayout constrain,constrainSignUpRight,constrainSignInLeft,constrainPortraitFieldsContainer;
    LinearLayout llSetPinSignUp,llSignUpMailotp,llmobileSignUp,llSignUpMobileOtp;
    TextView txtReset,txtReset1,txtSignUpPinHint,txtSignInPinHint;
    Pinview pinview,pinViewSignup;

    // SignUp
    View viewMobileSignUp,viewemailSignUp;
    EditText etEnterMailotp,etemailSignUp,etMobileSignUp,etEnterMobileOtp;

    int currentView = 0;

    private boolean doubleBackToExitPressedOnce = false;

    /*landscape*/
    RelativeLayout signInSubmitLand,signUPLandLabel,signUPSubmitLand,signInLandLabel;
    LinearLayout llSignInLand,llSignUpLand;

    /*Portrait*/
    VerticalTextView txtSignUpLabelPortrait,txtSignInLabelPortrait;
    RelativeLayout rlSignUpLabelContainerPortrait,rlSignInLabelContainerPortrait;
    TextView txtHeadingPortrait;
    SessionManager sessionManager;
    HashMap<String, String> signInUpDetails;
    String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
    Animation leftToRoght;
    Animation RightToLeft;
    Loader loader;
    CustomAlert customAlert;
    TextView txtAcceptTerms;
    LinearLayout llAcceptTerms;
    CheckBox signUpCheckBox;
    ConnectionDetector cd;
    ImageView imgBottom;
    int keyboardhide = 0;
    FrameLayout frame_container;
    Guideline guide4,guide1;
    Handler handler;
    Runnable runnable;
    CountDownTimer cTimer;
    SpannableString ss;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(getResources().getBoolean(R.bool.isTablet))
        {
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }else {
            StatusBarColorChange.setLoginStatusBarGradiant(SignInSighUp.this);
        }
        super.onCreate(savedInstanceState);
        leftToRoght = AnimationUtils.loadAnimation(SignInSighUp.this, R.anim.left_to_right);
        RightToLeft = AnimationUtils.loadAnimation(SignInSighUp.this, R.anim.right_to_left);
        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int rotation = display.getRotation();
        Log.e("rotation",rotation+"");
        cd = new ConnectionDetector(SignInSighUp.this);

        if(rotation == 1 || rotation == 3) {
            setContentView(R.layout.activity_sighin_signup);
            initLand();
        }else if(getResources().getBoolean(R.bool.isTablet) && (rotation == 2 || rotation ==0)) {
            setContentView(R.layout.activity_sighin_signup_portrait);
            initPortrait();
        }else{
            setContentView(R.layout.activity_sighin_signup);
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);}

        if(cd.isConnectingToInternet())
            init();
        else{
            startActivity(new Intent(SignInSighUp.this,NoInterNetActivity.class));
            overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
            finish();
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }


    private void cancelTimer() {
        LoginData.countDownTimer = LoginData.countDown;
        if(cTimer!=null)
            cTimer.cancel();
    }
    private void startCountDownTimer() {
        if(currentView == 0)
                {
                    if(llpin.getVisibility() == View.GONE && llotp.getVisibility() == View.VISIBLE) {
                        txtResendOTPSignIn.setVisibility(View.VISIBLE);
                        LoginData.SignInResendVisible = "Yes";
                        LoginData.SignUpResendVisible = "No";
                    }
                }
                else
                {
                    if((llSignUpMailotp.getVisibility() == View.VISIBLE  && llmobileSignUp.getVisibility() == View.GONE) || llSignUpMobileOtp.getVisibility() == View.VISIBLE && llmobileSignUp.getVisibility() == View.VISIBLE && llSignUpMailotp.getVisibility() == View.GONE) {
                        if (txtResendOTPSignUp != null) {
                            txtResendOTPSignUp.setVisibility(View.VISIBLE);
                        }
                        LoginData.SignUpResendVisible = "Yes";
                        LoginData.SignInResendVisible = "No";
                    }
                }

        cTimer = new CountDownTimer(LoginData.countDownTimer, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                LoginData.countDownTimer = millisUntilFinished;
                long Minutes = millisUntilFinished / (60 * 1000) % 60;
                long Seconds = millisUntilFinished / 1000 % 60;
                if(currentView==0){

                    txtResendOTPSignIn.setText(getString(R.string.resend_otp)+" in "+Minutes+":"+(Seconds>9?Seconds:"0"+Seconds));
                    ss = new SpannableString(getString(R.string.resend_otp)+" in "+Minutes+":"+(Seconds>9?Seconds:"0"+Seconds));
                    ss.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.light_gray)), 0, 13, 0);
                    txtResendOTPSignIn.setText(ss);
                }
                else {
                    ss = new SpannableString(getString(R.string.resend_otp)+" in "+Minutes+":"+(Seconds>9?Seconds:"0"+Seconds));
                    ss.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.light_gray)), 0, 13, 0);
                    txtResendOTPSignUp.setText(ss);

                }
            }

            @Override
            public void onFinish() {

                if(currentView==0) {
                    txtResendOTPSignIn.setText(getResources().getString(R.string.resend_otp));
                    txtResendOTPSignIn.setTextColor(getResources().getColor(R.color.mcq_blue));
                }
                else {
                    txtResendOTPSignUp.setText(getResources().getString(R.string.resend_otp));
                    txtResendOTPSignUp.setTextColor(getResources().getColor(R.color.mcq_blue));
                }
                cTimer.cancel();
            }
        };
        cTimer.start();
    }

    private void initPortrait() {
        txtSignUpLabelPortrait = findViewById(R.id.txtSignUpLabelPortrait);
        txtSignInLabelPortrait = findViewById(R.id.txtSignInLabelPortrait);
        rlSignUpLabelContainerPortrait = findViewById(R.id.rlSignUpLabelContainerPortrait);
        rlSignInLabelContainerPortrait = findViewById(R.id.rlSignInLabelContainerPortrait);
        txtHeadingPortrait = findViewById(R.id.txtHeadingPortrait);
        constrainPortraitFieldsContainer = findViewById(R.id.constrainPortraitFieldsContainer);
        pinViewSignup = findViewById(R.id.pinViewSignup);
        pinview = findViewById(R.id.pinview);

        txtSignUpLabelPortrait.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/LatoBold.ttf"));
        txtSignInLabelPortrait.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/LatoBold.ttf"));
        txtSignInLabelPortrait.setOnClickListener(this);
        txtSignUpLabelPortrait.setOnClickListener(this);
    }

    private void setPinwidth() {
        double wisth = StatusBarColorChange.getViewWidth(pinview);
        if(getResources().getBoolean(R.bool.isTablet)) {
            if (DisplayOrientation.getDisplayOrientation(SignInSighUp.this) == 0 || DisplayOrientation.getDisplayOrientation(SignInSighUp.this) == 2) {
            pinview.setPinWidth((int) Math.round(wisth / 14));

            pinview.setSplitWidth((int) Math.round(wisth / 5.5));
            pinViewSignup.setPinWidth((int) Math.round(wisth / 14));

            pinViewSignup.setSplitWidth((int) Math.round(wisth / 5.5));
            Log.e("tab","0,2");
            } else {
                Log.e("tab","1,3");
//                pinview.setPinWidth((int) Math.round(wisth / 16.0));
//
//                pinview.setSplitWidth((int) Math.round(wisth / 6.0));
//                pinViewSignup.setPinWidth((int) Math.round(wisth / 16.0));
//
//                pinViewSignup.setSplitWidth((int) Math.round(wisth / 6.0));
//
            }
        }else
        {
            pinview.setPinWidth((int) Math.round(wisth / 14.5));

            pinview.setSplitWidth((int) Math.round(wisth / 4.8));
            pinViewSignup.setPinWidth((int) Math.round(wisth / 14.5));

            pinViewSignup.setSplitWidth((int) Math.round(wisth / 4.8));
            Log.e("tab","mobile");
        }
    }

    private void initLand() {

        signUPLandLabel = findViewById(R.id.signUPLandLabel);
        signInLandLabel = findViewById(R.id.signInLandLabel);
        signInSubmitLand = findViewById(R.id.signInSubmitLand);
        signUPSubmitLand = findViewById(R.id.signUPSubmitLand);
        txtReset1 = findViewById(R.id.txtReset1);
        txtReset = findViewById(R.id.txtReset);
        txtResendOTPSignIn = findViewById(R.id.txtResendOTPSignIn);
        txtResendOTPSignUp = findViewById(R.id.txtResendOTPSignUp);
        llSignUpLand = findViewById(R.id.llSignUpLand);
        llSignInLand = findViewById(R.id.llSignInLand);
        txtTermsConditionsSignUp = findViewById(R.id.txtTermsConditionsSignUp);
        txtContactUsSignUp = findViewById(R.id.txtContactUsSignUp);
        constrainSignUpRight = findViewById(R.id.constrainSignUpRight);
        constrainSignInLeft = findViewById(R.id.constrainSignInLeft);
        constrain = findViewById(R.id.constrain);
        rlSignUpLeft = findViewById(R.id.rlSignUpLeft);
        rlSignInRight = findViewById(R.id.rlSignInRight);
        pinViewSignup = findViewById(R.id.pinViewSignup);
        pinview = findViewById(R.id.pinview);
        signUPLandLabel.setOnClickListener(this);
        signInSubmitLand.setOnClickListener(this);
        signUPSubmitLand.setOnClickListener(this);
        signInLandLabel.setOnClickListener(this);


    }

    private void init() {

        txtTermsConditions = findViewById(R.id.txtTermsConditions);
        txtContactUs = findViewById(R.id.txtContactUs);
        viewentrpin = findViewById(R.id.viewentrpin);
        viewemailmobileInActive = findViewById(R.id.viewemailmobileInActive);

        viewsignupActive = findViewById(R.id.viewsignupActive);
        viewsignupInActive = findViewById(R.id.viewsignupInActive);
        viewsigninInActive = findViewById(R.id.viewsigninInActive);
        viewsigninActive = findViewById(R.id.viewsigninActive);
        etenterotp = findViewById(R.id.etenterotp);
        etemailandmobile = findViewById(R.id.etemailandmobile);
        llsigninTitle = findViewById(R.id.llsigninTitle);
        llsignupTitle = findViewById(R.id.llsignupTitle);
        llotp = findViewById(R.id.llotp);
        llpin = findViewById(R.id.llpin);
        rlSignInPin = findViewById(R.id.rlSignInPin);
        rlenter_mobile_email = findViewById(R.id.rlenter_mobile_email);
        llLoginContainer = findViewById(R.id.llLoginContainer);
        llLSignupContainer = findViewById(R.id.llLSignupContainer);
        llmailSignUp = findViewById(R.id.llmailSignUp);
        signUPSubmit = findViewById(R.id.signUPSubmit);
        txtSignUpActive = findViewById(R.id.txtSignUpActive);
        txtSignUpInActive = findViewById(R.id.txtSignUpInActive);
        txtSignInActive = findViewById(R.id.txtSignInActive);
        txtSignInInActive = findViewById(R.id.txtSignInInActive);
        rlReset = findViewById(R.id.rlReset);
        txtReset = findViewById(R.id.txtReset);
        txtReset1 = findViewById(R.id.txtReset1);
        pinview = findViewById(R.id.pinview);
        txtSignInPinHint = findViewById(R.id.txtSignInPinHint);
        txtSignUpPinHint = findViewById(R.id.txtSignUpPinHint);
        txtResendOTPSignIn = findViewById(R.id.txtResendOTPSignIn);
        txtResendOTPSignUp = findViewById(R.id.txtResendOTPSignUp);
        imgBottom = findViewById(R.id.imgBottom);
        constrain = findViewById(R.id.constrain);
        frame_container = findViewById(R.id.frame_container);
        guide4 = findViewById(R.id.guide4);
        guide1 = findViewById(R.id.guide1);

        // SignUp
        viewemailSignUp = findViewById(R.id.viewemailSignUp);
        viewMobileSignUp = findViewById(R.id.viewMobileSignUp);
        etemailSignUp = findViewById(R.id.etemailSignUp);
        etEnterMailotp = findViewById(R.id.etEnterMailotp);
        etEnterMobileOtp = findViewById(R.id.etEnterMobileOtp);
        etMobileSignUp = findViewById(R.id.etMobileSignUp);
        llSetPinSignUp = findViewById(R.id.llSetPinSignUp);
        llmobileSignUp = findViewById(R.id.llmobileSignUp);
        llSignUpMobileOtp = findViewById(R.id.llSignUpMobileOtp);
        llSignUpMailotp = findViewById(R.id.llSignUpMailotp);
        pinViewSignup = findViewById(R.id.pinViewSignup);
        txtAcceptTerms = findViewById(R.id.txtAcceptTerms);
        llAcceptTerms = findViewById(R.id.llAcceptTerms);
        signUpCheckBox = findViewById(R.id.signUpCheckBox);
        llAcceptTerms.setVisibility(View.GONE);
//        String TandC = getString(R.string.acceptTerms);
//        TandC = TandC.replace("Terms&Conditions", "<font color='#0081FE'><u>Terms&Conditions</u></font>");
//        txtAcceptTerms.setText(Html.fromHtml(TandC));
        setCursorColor(etemailandmobile,"signin");
        setCheckTermsConditions();
        setPinwidth();
        setUnderLine();
        if(llsigninTitle != null)
            llsigninTitle.setOnClickListener(this);
        if(llsignupTitle != null)
            llsignupTitle.setOnClickListener(this);
        llotp.setOnClickListener(this);
        llpin.setOnClickListener(this);
        if(rlSignInPin != null)
            rlSignInPin.setOnClickListener(this);
        rlenter_mobile_email.setOnClickListener(this);
        if(signUPSubmit != null)
            signUPSubmit.setOnClickListener(this);
        if(txtReset != null)
            txtReset.setOnClickListener(this);
        if(txtReset1 != null)
            txtReset1.setOnClickListener(this);
        txtContactUs.setOnClickListener(this);
        txtTermsConditions.setOnClickListener(this);

        if(txtResendOTPSignUp != null)
            txtResendOTPSignUp.setOnClickListener(this);
        if(txtResendOTPSignIn != null)
            txtResendOTPSignIn.setOnClickListener(this);

        setSubmitEnable(false);
        sessionManager = new SessionManager(this);
        loader = new Loader(SignInSighUp.this);
        customAlert = new CustomAlert(SignInSighUp.this);
        signInUpDetails = sessionManager.getSignInUpPageDetails();
        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int rotation = display.getRotation();
        if(LoginData.currentView.equalsIgnoreCase("SignIn"))
        {
            Log.e("view","Login");
            if(rotation == 1 || rotation == 3) {
                setSignInLandScape();
                if(LoginData.SignInField1 != null && !LoginData.SignInField1.equals("") && LoginData.SignInField1.equalsIgnoreCase("yes")) {
                    etemailandmobile.setText(LoginData.mail != null && !LoginData.mail.equals("") ? LoginData.mail : "");
                    if (!etemailandmobile.getText().toString().isEmpty()) {
                        if (LoginData.mail.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                            setSubmitEnable(true);
                        } else if (!LoginData.mail.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$") && LoginData.mail.length() == 10) {
                            setSubmitEnable(true);
                        }else
                        {
                            setSubmitEnable(false);
                        }

                    }
                }
                if(LoginData.SignInField2 != null && !LoginData.SignInField2.equals("") && LoginData.SignInField2.equalsIgnoreCase("yes"))
                {
                    etemailandmobile.setEnabled(false);
                    llotp.setVisibility(View.VISIBLE);
                    etenterotp.setText(LoginData.mailotp != null && !LoginData.mailotp.equals("") ? LoginData.mailotp : "");
                    if(txtReset != null)
                        txtReset.setVisibility(View.VISIBLE);
                    if(txtResendOTPSignIn != null && LoginData.SignInResendVisible.equalsIgnoreCase("Yes")) {
                        setResendOTP();
                    }
                    if (txtReset1 != null) {
                        txtReset1.setVisibility(View.VISIBLE);
                    }
                    if(!etenterotp.getText().toString().isEmpty() && etenterotp.length() == 6)
                    {
                        setSubmitEnable(true);
                    }
                }
                if(LoginData.SignInField3 != null && !LoginData.SignInField3.equals("") && LoginData.SignInField3.equalsIgnoreCase("yes"))
                {
                    etemailandmobile.setEnabled(false);
                    etenterotp.setEnabled(false);
                    llotp.setVisibility(View.GONE);
                    llpin.setVisibility(View.VISIBLE);
                    if(txtReset != null)
                        txtReset.setVisibility(View.VISIBLE);
                    if (txtReset1 != null) {
                        txtReset1.setVisibility(View.VISIBLE);
                    }
                    cancelTimer();
                    pinview.setValue(LoginData.pin != null && !LoginData.pin.equals("") ? LoginData.pin : "");
                    if(!pinview.getValue().isEmpty() && pinview.getValue().length()>0)
                        txtSignInPinHint.setVisibility(View.GONE);
                    if(!pinview.getValue().isEmpty() && pinview.getValue().length() == 4)
                    {Log.e("Called",pinview+"");
                        setSubmitEnable(true);
                    }

                }
            }else if(getResources().getBoolean(R.bool.isTablet) && (rotation == 2 || rotation ==0)) {
                setSignUpPortrait();
                if(LoginData.SignInField1 != null && !LoginData.SignInField1.equals("") && LoginData.SignInField1.equalsIgnoreCase("yes")) {
                    etemailandmobile.setText(LoginData.mail != null && !LoginData.mail.equals("") ? LoginData.mail : "");
                    if (!etemailandmobile.getText().toString().isEmpty()) {
                        if (LoginData.mail.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                            setSubmitEnable(true);
                        } else if (!LoginData.mail.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$") && LoginData.mail.length() == 10) {
                            setSubmitEnable(true);
                        }else
                        {
                            setSubmitEnable(false);
                        }

                    }
                }
                if(LoginData.SignInField2 != null && !LoginData.SignInField2.equals("") && LoginData.SignInField2.equalsIgnoreCase("yes"))
                {
                    etemailandmobile.setEnabled(false);
                    llotp.setVisibility(View.VISIBLE);
                    etenterotp.setText(LoginData.mailotp != null && !LoginData.mailotp.equals("") ? LoginData.mailotp : "");
                    if(txtReset != null)
                        txtReset.setVisibility(View.VISIBLE);
                    if(txtResendOTPSignIn != null && LoginData.SignInResendVisible.equalsIgnoreCase("Yes")) {
                        setResendOTP();
                    }
                    if (txtReset1 != null) {
                        txtReset1.setVisibility(View.VISIBLE);
                    }
                    if(!etenterotp.getText().toString().isEmpty() && etenterotp.length() == 6)
                    {
                        setSubmitEnable(true);
                    }
                }
                if(LoginData.SignInField3 != null && !LoginData.SignInField3.equals("") && LoginData.SignInField3.equalsIgnoreCase("yes"))
                {
                    etemailandmobile.setEnabled(false);
                    etenterotp.setEnabled(false);
                    llotp.setVisibility(View.GONE);
                    llpin.setVisibility(View.VISIBLE);
                    if(txtReset != null)
                        txtReset.setVisibility(View.VISIBLE);
                    if (txtReset1 != null) {
                        txtReset1.setVisibility(View.VISIBLE);
                    }
                    cancelTimer();
                    pinview.setValue(LoginData.pin != null && !LoginData.pin.equals("") ? LoginData.pin : "");
                    if(!pinview.getValue().isEmpty() && pinview.getValue().length()>0)
                        txtSignInPinHint.setVisibility(View.GONE);
                    if(!pinview.getValue().isEmpty() && pinview.getValue().length() == 4)
                    {Log.e("Called",pinview+"");
                        setSubmitEnable(true);
                    }

                }
            }
        }else
        {
            if(txtTermsConditionsSignUp != null)
                txtTermsConditionsSignUp.setVisibility(View.GONE);
            if(txtContactUsSignUp != null)
                txtContactUsSignUp.setVisibility(View.GONE);
            if(txtTermsConditions != null)
                txtTermsConditions.setVisibility(View.GONE);
            if(txtContactUs != null)
                txtContactUs.setVisibility(View.GONE);
            Log.e("view","SignUp");
            if(rotation == 1 || rotation == 3) {
                setSignUpLandScape();
                if(LoginData.SignUpField1.equalsIgnoreCase("yes"))
                {
                    llmailSignUp.setVisibility(View.VISIBLE);
                    llSignUpMobileOtp.setVisibility(View.GONE);
                    llSignUpMailotp.setVisibility(View.GONE);
                    etemailSignUp.setText(LoginData.mail != null && !LoginData.mail.equals("") ? LoginData.mail : "");
                    setSubmitEnable(true);
                }
                if(LoginData.SignUpField2.equalsIgnoreCase("yes"))
                {
                    txtReset.setVisibility(View.VISIBLE);
//                    txtResendOTPSignIn.setVisibility(View.VISIBLE);
                    if (txtReset1 != null) {
                        txtReset1.setVisibility(View.VISIBLE);

                    }
                    llSignUpMailotp.setVisibility(View.VISIBLE);
                    llSignUpMobileOtp.setVisibility(View.GONE);
                    if(LoginData.SignUpResendVisible.equalsIgnoreCase("Yes"))
                        setResendOTP();

                    etemailSignUp.setEnabled(false);
                    etEnterMailotp.setText(LoginData.mailotp != null && !LoginData.mailotp.equals("") ? LoginData.mailotp : "");
                    if(!etEnterMailotp.getText().toString().isEmpty() && etEnterMailotp.length() == 6)
                    {
                        setSubmitEnable(true);
                    }
                }
                if(LoginData.SignUpField3.equalsIgnoreCase("yes"))
                {
                    txtReset.setVisibility(View.VISIBLE);
                    txtResendOTPSignIn.setVisibility(View.GONE);
//                    LoginData.SignUpResendVisible = "No";
//                    LoginData.SignInResendVisible = "No";
                    if (txtReset1 != null) {
                        txtReset1.setVisibility(View.VISIBLE);
                        txtResendOTPSignUp.setVisibility(View.GONE);
                    }
//                    cancelTimer();
                    etemailSignUp.setEnabled(false);
                    llmobileSignUp.setVisibility(View.VISIBLE);
                    llSignUpMobileOtp.setVisibility(View.GONE);
                    llSignUpMailotp.setVisibility(View.GONE);
                    etMobileSignUp.setText(LoginData.mobile != null && !LoginData.mobile.equals("") ? LoginData.mobile : "");
                    if(!etMobileSignUp.getText().toString().isEmpty() && etMobileSignUp.length() == 10)
                    {
                        setSubmitEnable(true);
                    }
                }
                if(LoginData.SignUpField4.equalsIgnoreCase("yes"))
                {
                    etemailSignUp.setEnabled(false);
                    llmobileSignUp.setVisibility(View.VISIBLE);
                    llSignUpMobileOtp.setVisibility(View.VISIBLE);
                    txtReset.setVisibility(View.VISIBLE);
//                    txtResendOTPSignIn.setVisibility(View.VISIBLE);
                    if (txtReset1 != null) {
                        txtReset1.setVisibility(View.VISIBLE);

                    }

                    llSignUpMailotp.setVisibility(View.GONE);
                    etMobileSignUp.setEnabled(false);
                    if(LoginData.SignUpResendVisible.equalsIgnoreCase("Yes"))
                        setResendOTP();
                    etEnterMobileOtp.setText(LoginData.mobileotp != null && !LoginData.mobileotp.equals("") ? LoginData.mobileotp : "");
                    if(!etEnterMobileOtp.getText().toString().isEmpty() && etEnterMobileOtp.length() == 6)
                    {
                        setSubmitEnable(true);
                    }
                    if( signUpCheckBox != null) {
                        signUpCheckBox.setChecked(LoginData.isChecked);
                    }
                    if(llAcceptTerms != null) {
                        llAcceptTerms.setVisibility(View.VISIBLE);
                            //llAcceptTerms.setVisibility(LoginData.SignUpTermsLink ? View.VISIBLE : View.GONE);

                    }
                }
                if(LoginData.SignUpField5.equalsIgnoreCase("yes"))
                {
                    Log.e("Called",LoginData.SignUpField5);
                    etemailSignUp.setEnabled(false);
                    etMobileSignUp.setEnabled(false);
                    llmailSignUp.setVisibility(View.VISIBLE);
                    llmobileSignUp.setVisibility(View.VISIBLE);
                    llSignUpMobileOtp.setVisibility(View.GONE);
                    llSignUpMailotp.setVisibility(View.GONE);
                    llSetPinSignUp.setVisibility(View.VISIBLE);
                    txtReset.setVisibility(View.VISIBLE);
                    llAcceptTerms.setVisibility(View.GONE);
                    txtResendOTPSignIn.setVisibility(View.GONE);
                    if (txtReset1 != null) {
                        txtReset1.setVisibility(View.VISIBLE);
                        txtResendOTPSignUp.setVisibility(View.GONE);
                    }
                    LoginData.SignUpResendVisible = "No";
                    LoginData.SignInResendVisible = "No";
                    pinViewSignup.setValue(LoginData.pinSignUp != null && !LoginData.pinSignUp.equals("") ? LoginData.pinSignUp : "");
                    if(!pinViewSignup.getValue().isEmpty() && pinViewSignup.getValue().length()>0)
                        txtSignUpPinHint.setVisibility(View.GONE);
                    if(!pinViewSignup.getValue().isEmpty() && pinViewSignup.getValue().length() == 4)
                    {Log.e("Called",pinViewSignup+"");
                        setSubmitEnable(true);
                    }

                }


            }else if(getResources().getBoolean(R.bool.isTablet) && (rotation == 2 || rotation ==0)) {
                setSignInPortrait();
                if(LoginData.SignUpField1.equalsIgnoreCase("yes"))
                {
                    llmailSignUp.setVisibility(View.VISIBLE);
                    llSignUpMobileOtp.setVisibility(View.GONE);
                    llSignUpMailotp.setVisibility(View.GONE);
                    txtReset.setVisibility(View.GONE);
                    txtResendOTPSignIn.setVisibility(View.GONE);
                    if (txtReset1 != null) {
                        txtReset1.setVisibility(View.GONE);
                        txtResendOTPSignUp.setVisibility(View.GONE);
                    }
                    LoginData.SignUpResendVisible = "No";
                    LoginData.SignInResendVisible = "No";
                    etemailSignUp.setText(LoginData.mail != null && !LoginData.mail.equals("") ? LoginData.mail : "");
                    setSubmitEnable(true);
                }
                if(LoginData.SignUpField2.equalsIgnoreCase("yes"))
                {
                    llSignUpMailotp.setVisibility(View.VISIBLE);
                    llSignUpMobileOtp.setVisibility(View.GONE);
                    txtReset.setVisibility(View.VISIBLE);
//                    txtResendOTPSignIn.setVisibility(View.VISIBLE);
                    if (txtReset1 != null) {
                        txtReset1.setVisibility(View.VISIBLE);

                    }
                    if(LoginData.SignUpResendVisible.equalsIgnoreCase("Yes"))
                        setResendOTP();
                    etemailSignUp.setEnabled(false);
                    etEnterMailotp.setText(LoginData.mailotp != null && !LoginData.mailotp.equals("") ? LoginData.mailotp : "");
                    if(!etEnterMailotp.getText().toString().isEmpty() && etEnterMailotp.length() == 6)
                    {
                        setSubmitEnable(true);
                    }
                }
                if(LoginData.SignUpField3.equalsIgnoreCase("yes"))
                {
                    etemailSignUp.setEnabled(false);
                    llmobileSignUp.setVisibility(View.VISIBLE);
                    llSignUpMobileOtp.setVisibility(View.GONE);
                    llSignUpMailotp.setVisibility(View.GONE);
                    txtReset.setVisibility(View.VISIBLE);
                    txtResendOTPSignIn.setVisibility(View.GONE);
                    if (txtReset1 != null) {
                        txtReset1.setVisibility(View.VISIBLE);
                        txtResendOTPSignUp.setVisibility(View.GONE);
                    }
//                    cancelTimer();
//                    LoginData.SignUpResendVisible = "No";
//                    LoginData.SignInResendVisible = "No";
                    etMobileSignUp.setText(LoginData.mobile != null && !LoginData.mobile.equals("") ? LoginData.mobile : "");
                    if(!etMobileSignUp.getText().toString().isEmpty() && etMobileSignUp.length() == 10)
                    {
                        setSubmitEnable(true);
                    }
                }
                if(LoginData.SignUpField4.equalsIgnoreCase("yes"))
                {
                    etemailSignUp.setEnabled(false);
                    llmobileSignUp.setVisibility(View.VISIBLE);
                    llSignUpMobileOtp.setVisibility(View.VISIBLE);
                    llSignUpMailotp.setVisibility(View.GONE);
                    txtReset.setVisibility(View.VISIBLE);
//                    txtResendOTPSignIn.setVisibility(View.VISIBLE);
                    if (txtReset1 != null) {
                        txtReset1.setVisibility(View.VISIBLE);

                    }
                    if(LoginData.SignUpResendVisible.equalsIgnoreCase("Yes"))
                        setResendOTP();
                    etMobileSignUp.setEnabled(false);
                    etEnterMobileOtp.setText(LoginData.mobileotp != null && !LoginData.mobileotp.equals("") ? LoginData.mobileotp : "");
                    if(!etEnterMobileOtp.getText().toString().isEmpty() && etEnterMobileOtp.length() == 6)
                    {
                        setSubmitEnable(true);
                    }
                    if( signUpCheckBox != null) {
                        signUpCheckBox.setChecked(LoginData.isChecked);
                    }
                    if(llAcceptTerms != null) {
                        llAcceptTerms.setVisibility(View.VISIBLE);
                        //llAcceptTerms.setVisibility(LoginData.SignUpTermsLink ? View.VISIBLE : View.GONE);

                    }
                }
                if(LoginData.SignUpField5.equalsIgnoreCase("yes"))
                {
                    etemailSignUp.setEnabled(false);
                    llmobileSignUp.setVisibility(View.VISIBLE);
                    llSignUpMobileOtp.setVisibility(View.GONE);
                    llSignUpMailotp.setVisibility(View.GONE);
                    llSetPinSignUp.setVisibility(View.VISIBLE);
                    txtReset.setVisibility(View.VISIBLE);
                    llAcceptTerms.setVisibility(View.GONE);
                    txtResendOTPSignIn.setVisibility(View.GONE);
                    if (txtReset1 != null) {
                        txtReset1.setVisibility(View.VISIBLE);
                        txtResendOTPSignUp.setVisibility(View.GONE);
                    }
                    cancelTimer();
                    LoginData.SignUpResendVisible = "No";
                    LoginData.SignInResendVisible = "No";
                    pinViewSignup.setValue(LoginData.pinSignUp != null && !LoginData.pinSignUp.equals("") ? LoginData.pinSignUp : "");
                    if(!pinViewSignup.getValue().isEmpty() && pinViewSignup.getValue().length()>0)
                        txtSignUpPinHint.setVisibility(View.GONE);
                    if(!pinViewSignup.getValue().isEmpty() && pinViewSignup.getValue().length() == 4)
                    {
                        setSubmitEnable(true);
                    }

                }
            }
        }

        Listeners();

    }

    private void setCheckTermsConditions() {
        SpannableString myString = new SpannableString(Html.fromHtml("I accept the "+"<font color=\"#0081FE\"><u>"+"Terms & Conditions"+"</u></font>"/* +" or "+ "<font color=\"#F15d36\"><u>"+"sign up"+ "</u></font>"+" to begin your YupIT experience"*/));

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                startActivity(new Intent(SignInSighUp.this,TermsConditions.class));
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
            }
        };

//        ClickableSpan clickableSpan1 = new ClickableSpan() {
//            @Override
//            public void onClick(View textView) {
//            }
//        };

        myString.setSpan(clickableSpan,15,myString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        //myString.setSpan(clickableSpan1,15,23,Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        myString.setSpan(new ForegroundColorSpan(Color.parseColor("#0081FE")),13,myString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        // myString.setSpan(new ForegroundColorSpan(Color.parseColor("#F15d36")),15,23, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        txtAcceptTerms.setMovementMethod(LinkMovementMethod.getInstance());
        txtAcceptTerms.setText(myString);
    }

    private void Listeners() {
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {

                setSubmitEnable(true);
                StatusBarColorChange.hideSoftKeyboard(SignInSighUp.this);
            }

            @Override
            public void onDataEntering(Pinview pinview, boolean fromUser) {
                if(pinview.getValue() != null && !pinview.getValue().equals("") && pinview.getValue().length()>0)
                {
                    LoginData.pin = pinview.getValue();
                    txtSignInPinHint.setVisibility(View.GONE);
                    if(pinview.getValue().length()<4)
                        setSubmitEnable(false);
                    sessionManager.setloginview("login",etemailandmobile.getText().toString(),etEnterMailotp.getText().toString(),"","","",pinview.getValue(),"");
                }
                else
                {txtSignInPinHint.setVisibility(View.VISIBLE);
                    sessionManager.setloginview("login",etemailandmobile.getText().toString(),etEnterMailotp.getText().toString(),"","","","","");
                    setSubmitEnable(false);
                }
            }
        });

        pinViewSignup.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                setSubmitEnable(true);
                StatusBarColorChange.hideSoftKeyboard(SignInSighUp.this);

            }

            @Override
            public void onDataEntering(Pinview pinview, boolean fromUser) {
                if(pinViewSignup.getValue() != null && !pinViewSignup.getValue().equals("") && pinViewSignup.getValue().length()>0)
                {
                    LoginData.pinSignUp = pinViewSignup.getValue();
                    txtSignUpPinHint.setVisibility(View.GONE);
                    if(pinViewSignup.getValue().length()<4)
                        setSubmitEnable(false);
                    sessionManager.setloginview("signUp","","",etemailSignUp.getText().toString(),etMobileSignUp.getText().toString(),etEnterMailotp.getText().toString(),pinview.getValue(),etEnterMobileOtp.getText().toString());
                }
                else
                {txtSignUpPinHint.setVisibility(View.VISIBLE);
                    setSubmitEnable(false);
                    sessionManager.setloginview("signUp","","",etemailSignUp.getText().toString(),etMobileSignUp.getText().toString(),etEnterMailotp.getText().toString(),pinview.getValue(),etEnterMobileOtp.getText().toString());
                }
            }
        });

        etemailandmobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s != null && !s.toString().equals("") && s.toString().length()>9) {
                    if(s.toString().length() == 10 && s.toString().matches("[0-9]+"))
                    {
                        etemailandmobile.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
                        setSubmitEnable(true);
                    }
                    else if(s.toString().length() > 10 && s.toString().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"))
                    {
                        setSubmitEnable(true);
                    }else
                    {
                        etemailandmobile.setFilters(new InputFilter[] {});
                        setSubmitEnable(false);
                    }

                }
                else {
                    etemailandmobile.setFilters(new InputFilter[] {});
                    setSubmitEnable(false);
                }
            }
        });

        etenterotp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s != null && !s.toString().equals("") && s.toString().length()>=6) {
                    setSubmitEnable(true);
                    StatusBarColorChange.hideSoftKeyboard(SignInSighUp.this);
                }
                else{

                    setSubmitEnable(false);
                }



            }
        });

        etemailSignUp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
               // LoginData.SignUpField1 = "yes";
                if(s != null && !s.toString().equals("") && s.toString().length()>0 && s.toString().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")){
                    setSubmitEnable(true);
                    sessionManager.setloginview("signUp","","",etemailSignUp.getText().toString(),"","","","");
                }
                else {
                    setSubmitEnable(false);
                    sessionManager.setloginview("signUp","","","","","","","");
                }

            }
        });

        etMobileSignUp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //LoginData.SignUpField3 = "yes";
                if(s != null && !s.toString().equals("") && s.toString().length()>=10) {
                    StatusBarColorChange.hideSoftKeyboard(SignInSighUp.this);
                    setSubmitEnable(true);
                }
                else {
                    setSubmitEnable(false);
                }

                if(s.toString().isEmpty())
                    sessionManager.setloginview("signUp","","",etemailSignUp.getText().toString(),"","","","");
                else
                    sessionManager.setloginview("signUp","","",etemailSignUp.getText().toString(),etMobileSignUp.getText().toString(),"","","");
            }
        });
        etEnterMailotp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
               // LoginData.SignUpField2 = "yes";
                if(s != null && !s.toString().equals("") && s.toString().length()>=6) {
                    setSubmitEnable(true);
                    StatusBarColorChange.hideSoftKeyboard(SignInSighUp.this);
                }
                else
                    setSubmitEnable(false);


            }
        });

        if(etEnterMobileOtp!=  null) {
            etEnterMobileOtp.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    //LoginData.SignUpField4 = "yes";
                    if (s != null && !s.toString().equals("") && s.toString().length() >= 6) {
                        setSubmitEnable(true);
                        StatusBarColorChange.hideSoftKeyboard(SignInSighUp.this);
                    }else
                        setSubmitEnable(false);


                }
            });
        }
        if(signUpCheckBox!= null)
        {
            signUpCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked) {
                        LoginData.isChecked = true;
                        if(signUPSubmitLand != null)
                            signUPSubmitLand.setBackground(getResources().getDrawable(R.drawable.login_gradient));
                        if(signUPSubmit != null)
                            signUPSubmit.setBackground(getResources().getDrawable(R.drawable.login_gradient));
                    }
                    else {
                        LoginData.isChecked = false;
                        if(signUPSubmitLand != null)
                            signUPSubmitLand.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
                        if(signUPSubmit != null)
                            signUPSubmit.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
                    }
                }
            });
        }
        if(constrain!= null)
        {
            constrain.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    Rect r = new Rect();
                    constrain.getWindowVisibleDisplayFrame(r);
                    int heightDiff = constrain.getRootView().getHeight() - (r.bottom - r.top);
                   // Log.e("heightDiff",""+heightDiff);
                    if (heightDiff > 221) { // if more than 100 pixels, its probably a keyboard...
                        //ok now we know the keyboard is up...
                       Log.e("Keyboard","open");
//                        ConstraintSet set = new ConstraintSet();
//                        set.clone(constrain);
//                        // Comment out line above and uncomment line below to make the connection.
//                         set.connect(frame_container.getId(), ConstraintSet.TOP, guide4.getId(), ConstraintSet.TOP, 0);
//                         set.connect(frame_container.getId(), ConstraintSet.BOTTOM, guide4.getId(), ConstraintSet.BOTTOM, 0);
//                        set.applyTo(constrain);

                        if(txtContactUs!=null)
                            txtContactUs.setVisibility(View.GONE);
                        if(txtTermsConditions!=null)
                            txtTermsConditions.setVisibility(View.GONE);

                        if(imgBottom!=null)
                            imgBottom.setVisibility(View.GONE);
                        //slideUp(llMainContainer);
                        if(txtSignInLabelPortrait != null)
                            txtSignInLabelPortrait.setVisibility(View.GONE);
                        if(txtSignUpLabelPortrait != null)
                            txtSignUpLabelPortrait.setVisibility(View.GONE);

                    }else{
                        //ok now we know the keyboard is down...
                        Log.e("Keyboard","close");
                        if (currentView == 0) {
                            if(txtContactUs!=null)
                                txtContactUs.setVisibility(View.VISIBLE);
                            if(txtTermsConditions!=null)
                                txtTermsConditions.setVisibility(View.VISIBLE);
                        }


                        if(imgBottom!=null)
                            imgBottom.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if(txtSignInLabelPortrait != null)
                                    txtSignInLabelPortrait.setVisibility(View.VISIBLE);
                                if(txtSignUpLabelPortrait != null)
                                    txtSignUpLabelPortrait.setVisibility(View.VISIBLE);
                            }
                        },1000);

                       // slideDown(llMainContainer);
//                        ConstraintSet set = new ConstraintSet();
//                        set.clone(constrain);
//                        // Comment out line above and uncomment line below to make the connection.
//                        set.connect(frame_container.getId(), ConstraintSet.TOP, guide4.getId(), ConstraintSet.TOP, 0);
//                        set.connect(frame_container.getId(), ConstraintSet.END, constrain., ConstraintSet.END, 0);
//                        set.connect(frame_container.getId(), ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START, 0);
//                        set.applyTo(constrain);

                    }
                }
            });
        }
    }

    private void setUnderLine() {
        txtContactUs.setPaintFlags(txtContactUs.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtTermsConditions.setPaintFlags(txtTermsConditions.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        if(txtTermsConditionsSignUp!= null)
        {
            txtTermsConditionsSignUp.setPaintFlags(txtTermsConditionsSignUp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            txtContactUsSignUp.setPaintFlags(txtContactUsSignUp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }
    }

    private void showResetAlert(String title, String desc)
    {
        final Dialog alertDialog = new Dialog(SignInSighUp.this);
        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        textViewTitle.setText(title);
        textViewDesc.setText(desc);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                cancelTimer();
                if(txtReset1!= null)
                    txtReset1.setVisibility(View.GONE);
                if(txtResendOTPSignUp!= null)
                    txtResendOTPSignUp.setVisibility(View.GONE);
                if(txtReset!= null)
                    txtReset.setVisibility(View.GONE);
                if(txtResendOTPSignIn!= null)
                    txtResendOTPSignIn.setVisibility(View.GONE);
                LoginData.SignUpResendVisible = "No";
                LoginData.SignInResendVisible = "No";
                if(currentView == 0)
                {
                    reSetSignIn();
                    LoginData.clearData();
                    LoginData.currentView = "signIn";
                }else
                {
                    if(llAcceptTerms != null)
                        llAcceptTerms.setVisibility(View.GONE);
                    reSetSignUp();
                    LoginData.clearData();
                    LoginData.currentView = "signUp";
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txtResendOTPSignIn:
                if(txtResendOTPSignIn.getText().toString().equalsIgnoreCase(getString(R.string.resend_otp))) {
                    cTimer = null;
                    LoginData.countDownTimer = LoginData.countDown;
                    reSendOTPSignIn();
                }

                break;
            case R.id.txtResendOTPSignUp:
                if(txtResendOTPSignUp.getText().toString().equalsIgnoreCase(getString(R.string.resend_otp))) {
                    cTimer = null;
                    LoginData.countDownTimer = LoginData.countDown;
                    reSendOTPSignUp();
                }
                break;
            case R.id.llsigninTitle:
                setSubmitEnable(false);
                showLoginView();
                setHideContactUs(true);
                setCursorColor(etemailandmobile,"signin");
               // etemailandmobile.setTextCursorDrawable(R.drawable.cursor_color_bg);
                if(txtReset1!= null)
                    txtReset1.setVisibility(View.GONE);
                if(txtResendOTPSignUp!= null)
                    txtResendOTPSignUp.setVisibility(View.GONE);
                if(txtReset!= null)
                    txtReset.setVisibility(View.GONE);
                if(txtResendOTPSignIn!= null)
                    txtResendOTPSignIn.setVisibility(View.GONE);
                LoginData.SignUpResendVisible = "No";
                LoginData.SignInResendVisible = "No";
                cancelTimer();
                break;
            case R.id.llsignupTitle:
                setSubmitEnable(false);
                showSignUpView();
                setCursorColor(etemailSignUp,"signup");

                setHideContactUs(false);
                if(txtReset1!= null)
                    txtReset1.setVisibility(View.GONE);
                if(txtResendOTPSignUp!= null)
                    txtResendOTPSignUp.setVisibility(View.GONE);
                if(txtReset!= null)
                    txtReset.setVisibility(View.GONE);
                if(txtResendOTPSignIn!= null)
                    txtResendOTPSignIn.setVisibility(View.GONE);
                LoginData.SignUpResendVisible = "No";
                LoginData.SignInResendVisible = "No";
                cancelTimer();
                break;
            case R.id.llotp:

                break;

            case R.id.txtReset:
                showResetAlert(getString(R.string.alert_oops),getString(R.string.reset_alert));
                break;

            case R.id.txtReset1:
                showResetAlert(getString(R.string.alert_oops),getString(R.string.reset_alert));
                break;

            case R.id.signUPSubmit:
//                if(txtReset1!= null)
//                    txtReset1.setVisibility(View.GONE);
//                if(txtResendOTPSignUp!= null)
//                    txtResendOTPSignUp.setVisibility(View.GONE);
//                if(txtReset!= null)
//                    txtReset.setVisibility(View.GONE);
//                if(txtResendOTPSignIn!= null)
//                    txtResendOTPSignIn.setVisibility(View.GONE);
                if(cd.isConnectingToInternet())
                {

                    if(currentView == 0)
                    {
                        checkSignIn();
                    }else
                    {
                        checkSignUp();
                    }
                }
                else{
                    startActivity(new Intent(SignInSighUp.this,NoInterNetActivity.class));
                    overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                    finish();
                }

                break;

            case R.id.signInSubmitLand:

                if(cd.isConnectingToInternet())
                    checkSignIn();
                else{
                    startActivity(new Intent(SignInSighUp.this,NoInterNetActivity.class));
                    overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                    finish();
                }

                break;

            case R.id.signUPSubmitLand:
//                if(txtReset1!= null)
//                    txtReset1.setVisibility(View.GONE);
//                if(txtResendOTPSignUp!= null)
//                    txtResendOTPSignUp.setVisibility(View.GONE);
//                if(txtReset!= null)
//                    txtReset.setVisibility(View.GONE);
//                if(txtResendOTPSignIn!= null)
//                    txtResendOTPSignIn.setVisibility(View.GONE);
                if(cd.isConnectingToInternet())
                    checkSignUp();
                else{
                    startActivity(new Intent(SignInSighUp.this,NoInterNetActivity.class));
                    overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                    finish();
                }

                break;

            case R.id.signUPLandLabel:
                if(txtReset1!= null)
                    txtReset1.setVisibility(View.GONE);
                if(txtResendOTPSignUp!= null)
                    txtResendOTPSignUp.setVisibility(View.GONE);
                if(txtReset!= null)
                    txtReset.setVisibility(View.GONE);
                if(txtResendOTPSignIn!= null)
                    txtResendOTPSignIn.setVisibility(View.GONE);
                LoginData.SignUpResendVisible = "No";
                LoginData.SignInResendVisible = "No";
                setSubmitEnable(false);
                sessionManager.setloginview("signUp","","","","","","","");
                setSignUpLandScape();
                LoginData.currentView = "signUp";
                LoginData.SignInField3= "No";
                LoginData.SignInField2= "No";
                cancelTimer();
                setHideContactUs(false);
                break;

            case R.id.signInLandLabel:
                if(txtReset1!= null)
                    txtReset1.setVisibility(View.GONE);
                if(txtResendOTPSignUp!= null)
                    txtResendOTPSignUp.setVisibility(View.GONE);
                if(txtReset!= null)
                    txtReset.setVisibility(View.GONE);
                if(txtResendOTPSignIn!= null)
                    txtResendOTPSignIn.setVisibility(View.GONE);
                LoginData.SignUpResendVisible = "No";
                LoginData.SignInResendVisible = "No";
                cancelTimer();
                setSubmitEnable(false);
                setSignInLandScape();
                LoginData.currentView = "SignIn";
                LoginData.SignUpField2= "No";
                LoginData.SignUpField3= "No";
                LoginData.SignUpField4= "No";
                LoginData.SignUpField5= "No";
                setHideContactUs(true);
                break;

            case R.id.txtSignUpLabelPortrait:
                if(txtReset1!= null)
                    txtReset1.setVisibility(View.GONE);
                if(txtResendOTPSignUp!= null)
                    txtResendOTPSignUp.setVisibility(View.GONE);
                if(txtReset!= null)
                    txtReset.setVisibility(View.GONE);
                if(txtResendOTPSignIn!= null)
                    txtResendOTPSignIn.setVisibility(View.GONE);
                LoginData.SignUpResendVisible = "No";
                LoginData.SignInResendVisible = "No";
                setSubmitEnable(false);
                setSignInPortrait();
                LoginData.currentView = "signUp";
                LoginData.SignInField3= "No";
                LoginData.SignInField2= "No";
                cancelTimer();
                setHideContactUs(false);
                break;

            case R.id.txtSignInLabelPortrait:
                if(txtReset1!= null)
                    txtReset1.setVisibility(View.GONE);
                if(txtResendOTPSignUp!= null)
                    txtResendOTPSignUp.setVisibility(View.GONE);
                if(txtReset!= null)
                    txtReset.setVisibility(View.GONE);
                if(txtResendOTPSignIn!= null)
                    txtResendOTPSignIn.setVisibility(View.GONE);
                LoginData.SignUpResendVisible = "No";
                LoginData.SignInResendVisible = "No";
                cancelTimer();
                setSubmitEnable(false);
                setSignUpPortrait();
                LoginData.currentView = "SignIn";
                LoginData.SignUpField2= "No";
                LoginData.SignUpField3= "No";
                LoginData.SignUpField4= "No";
                LoginData.SignUpField5= "No";
                setHideContactUs(true);
                break;

            case R.id.txtTermsConditions:
                startActivity(new Intent(SignInSighUp.this,TermsConditions.class));
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                break;


            case R.id.txtContactUs:
                startActivity(new Intent(SignInSighUp.this,ContactUs.class));
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                break;
        }
    }


    public static void setCursorColor(EditText view, String name) {
        try {

            if(name.equals("signin")){

                Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
                f.setAccessible(true);
                f.set(view, R.drawable.cursor_color_bg);


            } else{

                Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
                f.setAccessible(true);
                f.set(view, R.drawable.cursor_color_signup_bg);
            }




            /*// Get the cursor resource id
            Field field = TextView.class.getDeclaredField("mCursorDrawableRes");
            field.setAccessible(true);
            int drawableResId = field.getInt(view);

            // Get the editor
            field = TextView.class.getDeclaredField("mEditor");
            field.setAccessible(true);
            Object editor = field.get(view);

            // Get the drawable and set a color filter
            Drawable drawable = ContextCompat.getDrawable(view.getContext(), drawableResId);
            drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
            Drawable[] drawables = {drawable, drawable};

            // Set the drawables
            field = editor.getClass().getDeclaredField("mCursorDrawable");
            field.setAccessible(true);
            field.set(editor, drawables);*/
        } catch (Exception ignored) {
        }
    }

    private void setHideContactUs(boolean isShow ) {
        if(isShow) {
            if (txtContactUs != null)
                txtContactUs.setVisibility(View.VISIBLE);
            if (txtTermsConditions != null)
                txtTermsConditions.setVisibility(View.VISIBLE);
            if (txtTermsConditionsSignUp != null) {
                txtTermsConditionsSignUp.setVisibility(View.VISIBLE);
                txtContactUsSignUp.setVisibility(View.VISIBLE);
            }
        }else
        {
            if (txtContactUs != null)
                txtContactUs.setVisibility(View.GONE);
            if (txtTermsConditions != null)
                txtTermsConditions.setVisibility(View.GONE);
            if (txtTermsConditionsSignUp != null) {
                txtTermsConditionsSignUp.setVisibility(View.GONE);
                txtContactUsSignUp.setVisibility(View.GONE);
            }
        }

    }

    private void reSendOTPSignIn() {
        if(etemailandmobile.getText().toString().isEmpty())
        {
            AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_alert),getString(R.string.alert_enter_email_mobile));

        }else {
            signInEmailMobileVerification();

        }
    }

    private void reSendOTPSignUp() {
        if(llmobileSignUp.getVisibility() == View.GONE) {
            signupEmailVerification();
        }else
        {
            signUpMobileVerification("Resend");
        }
    }

    private void setSignUpPortrait() {

        try {
            rlSignUpLabelContainerPortrait.setVisibility(View.VISIBLE);
            rlSignInLabelContainerPortrait.setVisibility(View.GONE);
            txtHeadingPortrait.setTextColor(getResources().getColor(R.color.primary_meroon));
            txtHeadingPortrait.setText(getString(R.string.Signin));
            showLoginView();
            rlSignUpLabelContainerPortrait.startAnimation(leftToRoght);
            constrainPortraitFieldsContainer.startAnimation(RightToLeft);

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setSignInPortrait() {
        try {
            rlSignUpLabelContainerPortrait.setVisibility(View.GONE);
            rlSignInLabelContainerPortrait.setVisibility(View.VISIBLE);
            txtHeadingPortrait.setTextColor(getResources().getColor(R.color.mcq_blue));
            txtHeadingPortrait.setText(getString(R.string.Signup));
            showSignUpView();
            constrainPortraitFieldsContainer.startAnimation(leftToRoght);
            rlSignInLabelContainerPortrait.startAnimation(RightToLeft);

        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private void setSignUpLandScape() {
        reSetSignIn();
        currentView = 1;
        try {
            llSignInLand.setVisibility(View.GONE);
            llSignUpLand.setVisibility(View.VISIBLE);

            constrainSignUpRight.startAnimation(leftToRoght);
            rlSignUpLeft.startAnimation(RightToLeft);

        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private void setSignInLandScape() {

        reSetSignUp();
        try{
        currentView = 0;
        llSignInLand.setVisibility(View.VISIBLE);
        llpin.setVisibility(View.GONE);
        llSignUpLand.setVisibility(View.GONE);
        rlSignInRight.startAnimation(leftToRoght);
        constrainSignInLeft.startAnimation(RightToLeft);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void reSetSignUp() {
        try {

            etemailSignUp.setEnabled(true);
            etemailSignUp.setText("");
            etMobileSignUp.setEnabled(true);
            etMobileSignUp.setText("");
            etEnterMailotp.setText("");
            if (etEnterMobileOtp != null)
                etEnterMobileOtp.setText("");
            if (pinViewSignup.getValue() != null && !pinViewSignup.getValue().equals(""))
                pinViewSignup.setValue("");
            llmobileSignUp.setVisibility(View.GONE);
            llSignUpMailotp.setVisibility(View.GONE);
            llSignUpMobileOtp.setVisibility(View.GONE);
            llSetPinSignUp.setVisibility(View.GONE);
            llAcceptTerms.setVisibility(View.GONE);
            txtSignUpPinHint.setVisibility(View.VISIBLE);
            if (llAcceptTerms != null)
                llAcceptTerms.setVisibility(View.GONE);
        }catch (Exception e)
    {
        e.printStackTrace();
    }

    }

    private void reSetSignIn() {
        try{
        etemailandmobile.setEnabled(true);
        etemailandmobile.setText("");
        etenterotp.setText("");
        if(pinview.getValue() != null && !pinview.getValue().equals(""))
            pinview.setValue("");
        llotp.setVisibility(View.GONE);
        llpin.setVisibility(View.GONE);
        txtSignInPinHint.setVisibility(View.VISIBLE);
        if(llAcceptTerms != null)
            llAcceptTerms.setVisibility(View.GONE);
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private void checkSignIn() {
        keyboardhide = 1;
        LoginData.SignInField1 = "yes";
        LoginData.SignUpResendVisible = "No";
        LoginData.SignInResendVisible = "No";
        if(llpin.getVisibility() == View.GONE && llotp.getVisibility() == View.GONE)
        {

            if(etemailandmobile.getText().toString().isEmpty())
            {
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_alert),getString(R.string.alert_enter_email_mobile));

            }else {
                signInEmailMobileVerification();

            }
        }else if(llpin.getVisibility() == View.GONE && llotp.getVisibility() == View.VISIBLE)
        {
            if(etenterotp.getText().toString().isEmpty())
            {
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_alert),getString(R.string.alert_enter_otp));
            }else if(!etenterotp.getText().toString().equals(LoginData.SignIn_otp))
            {
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_alert),getString(R.string.alert_enter_valid_otp));
            }else {
                setSubmitEnable(false);

                signInUserAccount();

            }
        }else if(llpin.getVisibility() == View.VISIBLE && llotp.getVisibility() == View.GONE)
        {
            if(pinview.getValue()!=  null && !pinview.getValue().equals("")) {
                if (pinview.getValue().length() == 4) {
                    linkDeviceSignIn();
                }
                else
                    AlertBox.showSnackBox(SignInSighUp.this, getString(R.string.alert_alert), getString(R.string.ALERT_ENTER_Valid_PIN));
            }else
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_alert),getString(R.string.ENTER_UR_PIN));

        }

    }

    private void linkDeviceSignIn() {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(SignInSighUp.this);
        HashMap<String,String> params = new HashMap<>();
        params.put("device_id", Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID));
        params.put("device_name",Build.BRAND+", "+Build.MODEL);
        params.put("device_type",getResources().getBoolean(R.bool.isTablet)?"Tablet":"Mobile");
        params.put("user_id",LoginData.strUserId);
        params.put("device_os","Android");
        params.put("fcm_token","adGAdadfDfetrgFgdftrtrRtrteaasdtfRtrewtewgfgsdFgSSdfgsdFGsDFgSgfSdfgDfgsdfGdsFGDSFgDSfgDFgdsfgdgfs");
        mRequest.makeServiceRequest(IConstant.linkDevice, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------sighUpUserAccount Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1")) {
                        loader.dismissLoader();
                        JSONObject responseObj = object.getJSONObject("response");
                        sessionManager.createLoginSession(responseObj.getString("user_id"),responseObj.getString("_id"),responseObj.getString("phone"),responseObj.getString("email"),responseObj.getString("token"),"",responseObj.getString("first_name"),responseObj.getString("last_name"),responseObj.getString("time"));
                        sessionManager.updatePin(pinview.getValue());
                        LoginData.clearData();
                        EnterPinData.clearData();
                        if(getResources().getBoolean(R.bool.isTablet))
                        {
                            if(DisplayOrientation.getDisplayOrientation(SignInSighUp.this) == 0 || DisplayOrientation.getDisplayOrientation(SignInSighUp.this) == 2)
                                setSignInPortrait();
                            else
                                setSignInLandScape();
                        }
                        else
                            showLoginView();

                        if(txtReset1!= null)
                            txtReset1.setVisibility(View.GONE);
                        if(txtResendOTPSignUp!= null)
                            txtResendOTPSignUp.setVisibility(View.GONE);
                        if(txtReset!= null)
                            txtReset.setVisibility(View.GONE);
                        if(txtResendOTPSignIn!= null)
                            txtResendOTPSignIn.setVisibility(View.GONE);
                        LoginData.SignUpResendVisible = "No";
                        LoginData.SignInResendVisible = "No";
                        startActivity(new Intent(SignInSighUp.this,EnterPin.class));
                        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                        finish();
                    } else {
                        loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                Log.e("errorMessage",errorMessage);
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_oops),errorMessage);
            }
        });

    }

    private void signInUserAccount() {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(SignInSighUp.this);
        HashMap<String,String> params = new HashMap<>();
        params.put("username",LoginData.SignInUserName);
        params.put("type",LoginData.SignInType);
        params.put("otp",LoginData.SignIn_otp);
        params.put("device_id", Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID));
        params.put("device_name", Build.BRAND+", "+Build.MODEL);
        params.put("device_type",getResources().getBoolean(R.bool.isTablet)?"Tablet":"Mobile");
        params.put("device_os","Android");
        params.put("fcm_token","adGAdadfDfetrgFgdftrtrRtrteaasdtfRtrewtewgfgsdFgSSdfgsdFGsDFgSgfSdfgDfgsdfGdsFGDSFgDSfgDFgdsfgdgfs");
        mRequest.makeServiceRequest(IConstant.SignInUrl, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------SignInUrl Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1") && message.equalsIgnoreCase("Sign in successful")) {
                        LoginData.mobile_otp = "";
                        LoginData.otp = "";
                        JSONObject responseObj = object.getJSONObject("response");
                        LoginData.strUserId = responseObj.getString("_id");
                        loader.dismissLoader();
                        //sessionManager.setloginview("login",etemailandmobile.getText().toString(),etenterotp.getText().toString(),"","","","","");
                        llotp.setVisibility(View.GONE);
                        llpin.setVisibility(View.VISIBLE);
                        txtReset.setVisibility(View.VISIBLE);
                        txtResendOTPSignIn.setVisibility(View.GONE);
                        LoginData.SignUpResendVisible = "No";
                        LoginData.SignInResendVisible = "No";
                        cancelTimer();
                        LoginData.SignInField2 = "no";
                        LoginData.SignInField3 = "yes";
                        if (txtReset1 != null) {
                            txtReset1.setVisibility(View.VISIBLE);
                            txtResendOTPSignUp.setVisibility(View.GONE);
                        }
                        //customAlert.showAlertSuccess(getString(R.string.alert_success),message);

                    } else if(sStatus.equalsIgnoreCase("01")){
                        loader.dismissLoader();
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    } else {
                        loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_oops),errorMessage);
            }
        });

    }

    private void signInEmailMobileVerification() {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(SignInSighUp.this);
        HashMap<String,String> params = new HashMap<>();
        params.put("username",etemailandmobile.getText().toString());
        mRequest.makeServiceRequest(IConstant.signInPhoneEmailVerification, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------signInPhoneEmailVerification Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1")) {
                        LoginData.SignIn_otp = object.getJSONObject("response").getString("otp");
                        LoginData.SignInUserName = object.getJSONObject("response").getString("username");
                        LoginData.SignInType = object.getJSONObject("response").getString("type");
                        if(object.getJSONObject("response").getString("mode").equalsIgnoreCase("development"))
                            etenterotp.setText(object.getJSONObject("response").getString("otp"));
                        setSubmitEnable(false);
                        etemailandmobile.setEnabled(false);
                        llotp.setVisibility(View.VISIBLE);
                        LoginData.SignInField2 = "yes";
                        txtReset.setVisibility(View.VISIBLE);
                        cTimer = null;
                        LoginData.countDownTimer = LoginData.countDown;
                        setResendOTP();
//                        txtResendOTPSignIn.setVisibility(View.VISIBLE);
                        if (txtReset1 != null) {
                            txtReset1.setVisibility(View.VISIBLE);
//                            txtResendOTPSignUp.setVisibility(View.VISIBLE);
                        }
                        loader.dismissLoader();
                    } else {
                        loader.dismissLoader();
                        etemailandmobile.setEnabled(true);
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }


    private void setSubmitEnable(boolean isEnable) {

        if(isEnable) {
            if (signUPSubmit != null)
                signUPSubmit.setBackground(getResources().getDrawable(R.drawable.login_gradient));
            if (signUPSubmitLand != null)
                signUPSubmitLand.setBackground(getResources().getDrawable(R.drawable.login_gradient));
            if (signInSubmitLand != null)
                signInSubmitLand.setBackground(getResources().getDrawable(R.drawable.login_gradient));
        }else
        {
            if (signUPSubmit != null)
                signUPSubmit.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
            if (signUPSubmitLand != null)
                signUPSubmitLand.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
            if (signInSubmitLand != null)
                signInSubmitLand.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
        }
    }



    private void checkSignUp() {
        keyboardhide = 1;
        LoginData.SignUpResendVisible = "No";
        LoginData.SignInResendVisible = "No";
        if(llmobileSignUp.getVisibility() == View.GONE && llSignUpMailotp.getVisibility() == View.GONE)
        {
            if(etemailSignUp.getText().toString().isEmpty())
            {
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_alert),getString(R.string.alert_enter_email));
            }else if (!etemailSignUp.getText().toString().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")){
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_alert),getString(R.string.alert_enter_valid_email));
            }else{etemailSignUp.setEnabled(false);

                signupEmailVerification();
                llAcceptTerms.setVisibility(View.GONE);
            }

        }else if(llSignUpMailotp.getVisibility() == View.VISIBLE  && llmobileSignUp.getVisibility() == View.GONE)
        {

            if(etEnterMailotp.getText().toString().isEmpty())
            {
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_alert),getString(R.string.alert_enter_otp));
            }else if(!etEnterMailotp.getText().toString().equals(LoginData.otp))
            {AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_alert),getString(R.string.alert_enter_valid_otp));}
            else {
                LoginData.SignUpField2 = "no";
                LoginData.SignUpField3 = "yes";
                setSubmitEnable(false);
                sessionManager.setloginview("signUp","","",etemailSignUp.getText().toString(),"",etEnterMailotp.getText().toString(),"","");
                etMobileSignUp.setEnabled(true);
                llSignUpMailotp.setVisibility(View.GONE);
                llmobileSignUp.setVisibility(View.VISIBLE);
                llAcceptTerms.setVisibility(View.GONE);
                txtReset.setVisibility(View.VISIBLE);
                txtResendOTPSignIn.setVisibility(View.GONE);
                LoginData.SignUpResendVisible = "No";
                LoginData.SignInResendVisible = "No";
                cancelTimer();
                if (txtReset1 != null) {
                    txtReset1.setVisibility(View.VISIBLE);
                    txtResendOTPSignUp.setVisibility(View.GONE);
                }
            }

        }else if(llSignUpMailotp.getVisibility() == View.GONE && llmobileSignUp.getVisibility() == View.VISIBLE && llSetPinSignUp.getVisibility() == View.GONE && llSignUpMobileOtp.getVisibility() == View.GONE)
        {
            if(etMobileSignUp.getText().toString().isEmpty())
            {
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_alert),getString(R.string.alert_enter_mobile));
            }else if(etMobileSignUp.getText().toString().length()<10)
            {
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_alert),getString(R.string.alert_enter_valid_mobile));
            }else {
                signUpMobileVerification("Submit");


            }

        }else if(llSignUpMobileOtp.getVisibility() == View.VISIBLE && llmobileSignUp.getVisibility() == View.VISIBLE && llSignUpMailotp.getVisibility() == View.GONE)
        {
            if(etEnterMobileOtp.getText().toString().isEmpty())
            {
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_alert),getString(R.string.alert_enter_otp));
            }else if(!etEnterMobileOtp.getText().toString().equals(LoginData.mobile_otp))
            {
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_alert),getString(R.string.alert_enter_valid_otp));
            }else if(!signUpCheckBox.isChecked())
            {
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_alert),getString(R.string.alert_acceptTerms));
            }
            else {

                llAcceptTerms.setVisibility(View.GONE);
                sighUpUserAccount();
            }
        }
        if(llSetPinSignUp.getVisibility() == View.VISIBLE )
        {
            if( pinViewSignup !=null && pinViewSignup.getValue() != null && !pinViewSignup.getValue().equals("")){
            if (pinViewSignup.getValue().length() == 4)
            {

                setSubmitEnable(false);
                llmobileSignUp.setVisibility(View.VISIBLE);
                llSignUpMailotp.setVisibility(View.GONE);
                llSignUpMailotp.setVisibility(View.GONE);
                llSetPinSignUp.setVisibility(View.VISIBLE);
                txtReset.setVisibility(View.VISIBLE);
                txtResendOTPSignIn.setVisibility(View.GONE);
                LoginData.SignUpResendVisible = "No";
                LoginData.SignInResendVisible = "No";
                LoginData.SignUpResendVisible = "No";
                LoginData.SignInResendVisible = "No";
                if (txtReset1 != null) {

                    txtReset1.setVisibility(View.VISIBLE);
                    txtResendOTPSignUp.setVisibility(View.GONE);
                }

//                sessionManager.updatePin(pinViewSignup.getValue());
//                LoginData.clearData();
//                EnterPinData.clearData();
//                startActivity(new Intent(SignInSighUp.this,EnterPin.class));
//                finish();
                linkDevice();

            }
            else
                AlertBox.showSnackBox(SignInSighUp.this, getString(R.string.alert_alert), getString(R.string.ALERT_ENTER_Valid_PIN));
            }else
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_alert),getString(R.string.ENTER_UR_PIN));
        }



    }

    private void linkDevice() {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(SignInSighUp.this);
        HashMap<String,String> params = new HashMap<>();
        params.put("device_id", Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID));
        params.put("device_name",Build.BRAND+", "+Build.MODEL);
        params.put("device_type",getResources().getBoolean(R.bool.isTablet)?"Tablet":"Mobile");
        params.put("user_id",LoginData.strUserId);
        params.put("device_os","Android");
        params.put("fcm_token","adGAdadfDfetrgFgdftrtrRtrteaasdtfRtrewtewgfgsdFgSSdfgsdFGsDFgSgfSdfgDfgsdfGdsFGDSFgDSfgDFgdsfgdgfs");
        mRequest.makeServiceRequest(IConstant.linkDevice, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------sighUpUserAccount Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1")) {
                        loader.dismissLoader();
                        JSONObject responseObj = object.getJSONObject("response");
                        sessionManager.createLoginSession(responseObj.getString("user_id"),responseObj.getString("_id"),responseObj.getString("phone"),responseObj.getString("email"),responseObj.getString("token"),"","","",responseObj.getString("time"));
                        sessionManager.updatePin(pinViewSignup.getValue());
                        LoginData.clearData();
                        EnterPinData.clearData();
                        if(txtReset1!= null)
                            txtReset1.setVisibility(View.GONE);
                        if(txtResendOTPSignUp!= null)
                            txtResendOTPSignUp.setVisibility(View.GONE);
                        if(txtReset!= null)
                            txtReset.setVisibility(View.GONE);
                        if(txtResendOTPSignIn!= null)
                            txtResendOTPSignIn.setVisibility(View.GONE);
                        LoginData.SignUpResendVisible = "No";
                        LoginData.SignInResendVisible = "No";
                        startActivity(new Intent(SignInSighUp.this,EnterPin.class));
                        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                        finish();
                    } else {
                        loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                Log.e("errorMessage",errorMessage);
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void sighUpUserAccount() {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(SignInSighUp.this);
        HashMap<String,String> params = new HashMap<>();
        params.put("phone",etMobileSignUp.getText().toString());
        params.put("phone_otp",LoginData.mobile_otp);
        params.put("email",etemailSignUp.getText().toString());
        params.put("email_otp",LoginData.otp);
        params.put("device_id", Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID));
        params.put("device_name",Build.BRAND+", "+Build.MODEL);
        params.put("device_type",getResources().getBoolean(R.bool.isTablet)?"Tablet":"Mobile");
        params.put("device_os","Android");
        params.put("fcm_token","adGAdadfDfetrgFgdftrtrRtrteaasdtfRtrewtewgfgsdFgSSdfgsdFGsDFgSgfSdfgDfgsdfGdsFGDSFgDSfgDFgdsfgdgfs");
        mRequest.makeServiceRequest(IConstant.signUpUrl, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------sighUpUserAccount Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1") && message.equalsIgnoreCase("Sign up successful")) {
                        loader.dismissLoader();
                        LoginData.SignUpField5 = "yes";
                        setSubmitEnable(false);
                        llmobileSignUp.setVisibility(View.VISIBLE);
                        llSignUpMailotp.setVisibility(View.GONE);
                        llSignUpMobileOtp.setVisibility(View.GONE);
                        llSetPinSignUp.setVisibility(View.VISIBLE);
                        txtSignUpPinHint.setVisibility(View.VISIBLE);
                        txtReset.setVisibility(View.VISIBLE);
                        txtResendOTPSignIn.setVisibility(View.GONE);
                        if (txtReset1 != null) {

                            txtReset1.setVisibility(View.VISIBLE);
                            txtResendOTPSignUp.setVisibility(View.GONE);
                        }

                        LoginData.SignUpResendVisible = "No";
                        LoginData.SignInResendVisible = "No";
                        cancelTimer();
                        LoginData.mobile_otp = "";
                        LoginData.otp = "";
                        JSONObject responseObj = object.getJSONObject("response");
                        LoginData.SignUpField5 = "yes";
                        LoginData.strUserId = responseObj.getString("_id");
                        sessionManager.createLoginSession(responseObj.getString("user_id"),responseObj.getString("_id"),responseObj.getString("phone"),responseObj.getString("email"),"","","","",responseObj.getString("time"));

                        customAlert.showAlertSuccess(getString(R.string.alert_success),getString(R.string.register_success));
                    } else {
                        loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void signupEmailVerification() {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(SignInSighUp.this);
        HashMap<String,String> params = new HashMap<>();
        params.put("email",etemailSignUp.getText().toString());
        mRequest.makeServiceRequest(IConstant.signUpEmailVerification, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------signUpEmailVerification Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1") && message.equalsIgnoreCase("Success")) {
                        LoginData.otp = object.getJSONObject("response").getString("otp");
                        if(object.getJSONObject("response").getString("mode").equalsIgnoreCase("development"))
                            etEnterMailotp.setText(object.getJSONObject("response").getString("otp"));
                        sessionManager.setloginview("signUp","","",etemailSignUp.getText().toString(),"","","","");
                        llSignUpMailotp.setVisibility(View.VISIBLE);
                        llSignUpMobileOtp.setVisibility(View.GONE);
                        //llSignUpMailotp.startAnimation(slideupfrombottom);
                        txtReset.setVisibility(View.VISIBLE);
                        if (txtReset1 != null) {
                            txtReset1.setVisibility(View.VISIBLE);
                        }
                        cTimer = null;
                        LoginData.countDownTimer = LoginData.countDown;
                        setResendOTP();
                        LoginData.SignUpField2 = "yes";
                        setSubmitEnable(false);
                        loader.dismissLoader();
                    } else {
                        setSubmitEnable(true);
                        loader.dismissLoader();
                        etemailSignUp.setEnabled(true);
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void setResendOTP()
    {
       if(cTimer==null)
           startCountDownTimer();
    }
    private void signUpMobileVerification(final String from) {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(SignInSighUp.this);
        HashMap<String,String> params = new HashMap<>();
        params.put("phone",etMobileSignUp.getText().toString());
        mRequest.makeServiceRequest(IConstant.signUpPhoneVerification, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------signUpPhoneVerification Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    loader.dismissLoader();
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1") && message.equalsIgnoreCase("Success")) {
                        LoginData.SignUpField4 = "yes";
                        LoginData.mobile_otp = object.getJSONObject("response").getString("otp");
                        if(object.getJSONObject("response").getString("mode").equalsIgnoreCase("development"))
                            etEnterMobileOtp.setText(object.getJSONObject("response").getString("otp"));
                        sessionManager.setloginview("signUp","","",etemailSignUp.getText().toString(),etMobileSignUp.getText().toString(),etEnterMailotp.getText().toString(),"","");
                        etMobileSignUp.setEnabled(false);
                        llmobileSignUp.setVisibility(View.VISIBLE);
                        llSignUpMobileOtp.setVisibility(View.VISIBLE);
                        txtReset.setVisibility(View.VISIBLE);

//                        txtResendOTPSignIn.setVisibility(View.VISIBLE);
                        if (txtReset1 != null) {
                            txtReset1.setVisibility(View.VISIBLE);
//                            txtResendOTPSignUp.setVisibility(View.VISIBLE);
                        }
                        llSignUpMailotp.setVisibility(View.GONE);
                        if(signUpCheckBox != null && from.equalsIgnoreCase("Submit"))
                            signUpCheckBox.setChecked(false);
                        llAcceptTerms.setVisibility(View.VISIBLE);
                        setSubmitEnable(false);
                        //llSignUpMailotp.startAnimation(slideupfrombottom);
                        txtReset.setVisibility(View.VISIBLE);
//                        txtResendOTPSignIn.setVisibility(View.VISIBLE);
                        if (txtReset1 != null) {

                            txtReset1.setVisibility(View.VISIBLE);
//                            txtResendOTPSignUp.setVisibility(View.VISIBLE);
                        }
                        cTimer=null;
                        LoginData.countDownTimer = LoginData.countDown;
                        setResendOTP();
                    } else {
                        etMobileSignUp.setEnabled(true);
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBox(SignInSighUp.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void showLoginView() {
        currentView = 0;
        if(llLoginContainer != null)
            llLoginContainer.setVisibility(View.VISIBLE);
        if(txtSignUpInActive != null)
            txtSignUpInActive.setVisibility(View.VISIBLE);
        if(viewsignupInActive != null)
            viewsignupInActive.setVisibility(View.VISIBLE);
        if(txtSignInActive != null)
            txtSignInActive.setVisibility(View.VISIBLE);
        if(viewsigninActive != null)
            viewsigninActive.setVisibility(View.VISIBLE);
        if(llLSignupContainer != null)
            llLSignupContainer.setVisibility(View.GONE);
        if(txtSignUpActive != null)
            txtSignUpActive.setVisibility(View.GONE);
        if(viewsignupActive != null)
            viewsignupActive.setVisibility(View.GONE);
        if(txtSignInInActive != null)
            txtSignInInActive.setVisibility(View.GONE);
        if(txtSignInInActive != null)
            txtSignInInActive.setVisibility(View.GONE);
        llotp.setVisibility(View.GONE);
        llpin.setVisibility(View.GONE);
        etenterotp.setEnabled(true);
        etemailandmobile.setEnabled(true);
        etMobileSignUp.setEnabled(true);
        etemailSignUp.setEnabled(true);
        etEnterMailotp.setEnabled(true);
        etemailSignUp.setText("");
        etMobileSignUp.setText("");
        etEnterMailotp.setText("");
        etEnterMobileOtp.setText("");
        if(llAcceptTerms != null)
            llAcceptTerms.setVisibility(View.GONE);
        txtContactUs.setTextColor(getResources().getColor(R.color.mcq_blue));
        txtTermsConditions.setTextColor(getResources().getColor(R.color.mcq_blue));
        pinViewSignup.clearValue();



    }

    private void showSignUpView() {
        currentView = 1;
        if(txtSignUpInActive != null)
            txtSignUpInActive.setVisibility(View.GONE);
        if(llLoginContainer != null)
            llLoginContainer.setVisibility(View.GONE);
        if(viewsignupInActive != null)
            viewsignupInActive.setVisibility(View.GONE);
        if(txtSignInActive != null)
            txtSignInActive.setVisibility(View.GONE);
        if(viewsigninActive != null)
            viewsigninActive.setVisibility(View.GONE);
        if(txtSignInInActive != null)
            txtSignInInActive.setVisibility(View.VISIBLE);
        if(llLSignupContainer != null)
            llLSignupContainer.setVisibility(View.VISIBLE);
//        rlReset.setVisibility(View.VISIBLE);
        if(txtSignUpActive != null)
            txtSignUpActive.setVisibility(View.VISIBLE);
        if(viewsignupActive != null)
            viewsignupActive.setVisibility(View.VISIBLE);
        if(txtSignInInActive != null)
            txtSignInInActive.setVisibility(View.VISIBLE);
        llSignUpMailotp.setVisibility(View.GONE);
        llSignUpMobileOtp.setVisibility(View.GONE);
        llmobileSignUp.setVisibility(View.GONE);
        llSetPinSignUp.setVisibility(View.GONE);
        etenterotp.setText("");
        etemailandmobile.setText("");
        etMobileSignUp.setEnabled(true);
        etMobileSignUp.setEnabled(true);
        etemailSignUp.setEnabled(true);
        etEnterMailotp.setEnabled(true);
        etenterotp.setEnabled(true);
        etemailandmobile.setEnabled(true);
        txtContactUs.setTextColor(getResources().getColor(R.color.primary_meroon));
        txtTermsConditions.setTextColor(getResources().getColor(R.color.primary_meroon));
        pinview.clearValue();

    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            LoginData.clearData();
            finishAffinity();
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getResources().getString(R.string.on_back_pressed), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 4000);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {


        outState.putString("theWord", "The Word"); // Saving the Variable theWord
        //outState.putStringArrayList("fiveDefns", "The Desc"); // Saving the ArrayList fiveDefns
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onRestoreInstanceState(savedInstanceState, persistentState);

        Log.e("after ratation",savedInstanceState.getString("theWord")); // Restoring theWor)d
        // Log.e("after ratation",savedInstanceState.getStringArrayList("fiveDefns")); //Restoring fiveDefns

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if(ev.getAction() == MotionEvent.ACTION_UP) {
            final View view = getCurrentFocus();

            if(view != null) {
                final boolean consumed = super.dispatchTouchEvent(ev);

                final View viewTmp = getCurrentFocus();
                final View viewNew = viewTmp != null ? viewTmp : view;

                if(viewNew.equals(view)) {
                    final Rect rect = new Rect();
                    final int[] coordinates = new int[2];

                    view.getLocationOnScreen(coordinates);

                    rect.set(coordinates[0], coordinates[1], coordinates[0] + view.getWidth(), coordinates[1] + view.getHeight());

                    final int x = (int) ev.getX();
                    final int y = (int) ev.getY();

                    if(rect.contains(x, y)) {
                        return consumed;
                    }
                }
                else if(viewNew instanceof EditText || viewNew instanceof EditText) {
                    return consumed;
                }

                final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                inputMethodManager.hideSoftInputFromWindow(viewNew.getWindowToken(), 0);

                viewNew.clearFocus();

                return consumed;
            }
        }

        return super.dispatchTouchEvent(ev);
    }

    public void slideUp(final View view/*, final View llDomestic*/){

        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationY",0f);
        animation.setDuration(100);
//        llDomestic.setVisibility(View.GONE);
        animation.start();

    }

    // slide the view from below itself to the current position
    public void slideDown(View view/*,View llDomestic*/){
//        llDomestic.setVisibility(View.VISIBLE);
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationY",   0f);
        animation.setDuration(100);
        animation.start();
    }

    public void onPause()
    {
        super.onPause();
        if(currentView == 1) {
            LoginData.mail = etemailSignUp.getText().toString();
            LoginData.mailotp = etEnterMailotp.getText().toString();
            LoginData.mobile = etMobileSignUp.getText().toString();
            LoginData.mobile_otp = etEnterMobileOtp.getText().toString();
            LoginData.pin = pinViewSignup.getValue();
        }else
        {
            LoginData.mail = etemailandmobile.getText().toString();
            LoginData.mailotp = etenterotp.getText().toString();
            LoginData.pin = pinview.getValue();
        }

    }
}
