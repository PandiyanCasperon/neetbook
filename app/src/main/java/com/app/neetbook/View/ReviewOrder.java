package com.app.neetbook.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.neetbook.Adapter.ReviewOrderCartAdapter;
import com.app.neetbook.Adapter.subscription.SubscriptionBottomSheetAdapter;
import com.app.neetbook.CartRoom.CartDataBase;
import com.app.neetbook.Model.subscription.SubscriptionCart;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ActionBarActivityNeetBook;
import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.EventBusPojo.ActionEvent;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.Utils.ThreadExecutor.AppExecutors;
import com.app.neetbook.Utils.alerter.AlertBox;
import com.app.neetbook.Utils.widgets.DisplayOrientation;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.Inflater;

import com.bumptech.glide.load.model.FileLoader;
import com.google.gson.Gson;
import com.razorpay.Checkout;
import com.razorpay.Payment;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultListener;
import com.razorpay.PaymentResultWithDataListener;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;

public class ReviewOrder extends ActionBarActivityNeetBook implements  /*PaymentResultWithDataListener*/PaymentResultListener, View.OnClickListener {

    RelativeLayout rlBackContactUs;
    TextView txtBack,txtPayment,txtErrorPromo,txtSubGSTLabel,txtDiscountLabel,txtCartTotalAmount,txtCartTotal,txtTotalItems,txtPromoAmount,txtGST,txtServices,txtGrandTotal,txtSubTotal,txtWalletBalanceAmount;
    /*LinearLayout llCartItems;*/
    private AppExecutors exector;
    CartDataBase cartlistDatabase;
    List<SubscriptionCart> mSubscriptionCartList = new ArrayList<>();
    ConnectionDetector cd;
    Loader loader;
    SessionManager sessionManager;
    CustomAlert customAlert;
    float gst = 0,wallet_amount = 0,service = 0,sgst =0,promoAmt =0;
    double grandTotal = 0;
    TextView imgApplyPromo,txtHavePromo,txtPromoCodeApplied;
    EditText etPromoCode;
    RelativeLayout rlWalletBalance,rlDiscount;
    public static ReviewOrder reviewOrder;
    private CardView cardView;
    String strCartId = "";
    DecimalFormat formater;
    private RecyclerView rv_price;
    LinearSnapHelper snapHelper;
    ConstraintLayout parentContainer;
    LinearLayout rlRecycleContainer,llPromoApplied;
    private int recyclerViewHeightChanged = 0;
    Display display;
    int height1,width1;
    RelativeLayout rlSubTotal;
    private int loadingType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StatusBarColorChange.setMobilePagesStatusBarGradiant(ReviewOrder.this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_order);
        EventBus.getDefault().register(this);
        reviewOrder = this;
        init();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }

    private void init() {

        Checkout.preload(getApplicationContext());
        sessionManager = new SessionManager(ReviewOrder.this);
        loader = new Loader(ReviewOrder.this);
        customAlert = new CustomAlert(ReviewOrder.this);
        cd = new ConnectionDetector(ReviewOrder.this);
        formater = new DecimalFormat("#.##");
        txtPayment = findViewById(R.id.txtPayment);
        txtHavePromo = findViewById(R.id.txtHavePromo);
        parentContainer = findViewById(R.id.parentContainer);
        rlRecycleContainer = findViewById(R.id.rlRecycleContainer);
        rlBackContactUs = findViewById(R.id.rlBackContactUs);
        txtTotalItems = findViewById(R.id.txtTotalItems);
        txtCartTotal = findViewById(R.id.txtCartTotal);
        txtCartTotalAmount = findViewById(R.id.txtCartTotalAmount);
        txtBack = findViewById(R.id.txtBack);
        rv_price = findViewById(R.id.rv_price);
        txtWalletBalanceAmount = findViewById(R.id.txtWalletBalanceAmount);
        txtPromoAmount = findViewById(R.id.txtPromoAmount);
        txtSubTotal = findViewById(R.id.txtSubTotal);
        txtGST = findViewById(R.id.txtGST);
        txtGrandTotal = findViewById(R.id.txtGrandTotal);
        txtServices = findViewById(R.id.txtServices);
        txtDiscountLabel = findViewById(R.id.txtDiscountLabel);
        imgApplyPromo = findViewById(R.id.imgApplyPromo);
        rlSubTotal = findViewById(R.id.rlSubTotal);
        rlWalletBalance = findViewById(R.id.rlWalletBalance);

        etPromoCode = findViewById(R.id.etPromoCode);
        txtPromoCodeApplied = findViewById(R.id.txtPromoCodeApplied);
        llPromoApplied = findViewById(R.id.llPromoApplied);

        txtSubGSTLabel = findViewById(R.id.txtSubGSTLabel);
        rlDiscount = findViewById(R.id.rlDiscount);
        txtErrorPromo = findViewById(R.id.txtErrorPromo);
        cardView = findViewById(R.id.cardView);


        imgApplyPromo.setOnClickListener(this);
        txtBack.setOnClickListener(this);
        rlBackContactUs.setOnClickListener(this);
        exector=new AppExecutors();
        cartlistDatabase = CartDataBase.getAppDatabase(ReviewOrder.this);

        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width1= size.x;
        height1 = size.y;

        createSnap();
        listeners();
        getCartDetails(1);

        //populateCartItems();
    }

    private void listeners() {
        etPromoCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty())
                    txtErrorPromo.setVisibility(View.GONE);
            }
        });

        txtPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(grandTotal>0)
                    startPayment();
                else
                {
                    startActivity(new Intent(ReviewOrder.this,PaymentActivity.class)
                            .putExtra("status","success")
                            .putExtra("payId","")
                            .putExtra("amount",""+grandTotal)
                            .putExtra("cart_id",""+strCartId)
                            .putExtra("gst",""+gst)
                            .putExtra("service",""+service)
                            .putExtra("sgst",""+sgst)
                            .putExtra("wallet_amount",""+wallet_amount)
                    );
                    overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                }

            }
        });

        if(parentContainer!= null)
        {
            parentContainer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    Rect r = new Rect();
                    parentContainer.getWindowVisibleDisplayFrame(r);
                    int heightDiff = parentContainer.getRootView().getHeight() - (r.bottom - r.top);

                    if (heightDiff > 230 ) { // if more than 100 pixels, its probably a keyboard...
                        //ok now we know the keyboard is up...
                        if(mSubscriptionCartList.size()>2) {
                            ViewGroup.LayoutParams params = rlRecycleContainer.getLayoutParams();
                            if (getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(ReviewOrder.this) == 0 || DisplayOrientation.getDisplayOrientation(ReviewOrder.this) == 2))
                                params.height = (int) (getScreenHeightInDPs(ReviewOrder.this) * (0.28));
                            else
                                params.height = (int) (height1 * (0.26));

                            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                            rlRecycleContainer.setLayoutParams(params);
                        }

                        txtBack.setVisibility(View.GONE);
                        txtPayment.setVisibility(View.GONE);
                        rlWalletBalance.setVisibility(View.GONE);
                        rlSubTotal.setVisibility(View.GONE);


                    }else{
                        //ok now we know the keyboard is down...

                        recyclerViewHeightChanged = 0;
                        setDefaultHeight();
                        txtBack.setVisibility(View.VISIBLE);
                        txtPayment.setVisibility(View.VISIBLE);
                        rlWalletBalance.setVisibility(View.VISIBLE);
                        rlSubTotal.setVisibility(View.VISIBLE);


                    }
                }
            });
        }
    }

    private void getCartDetails(final int type) {
        loadingType = type;
        loader.dismissLoader();
        loader.showLoader();
        mSubscriptionCartList.clear();
        ServiceRequest mRequest = new ServiceRequest(ReviewOrder.this);
        HashMap<String,String> params = new HashMap<>();
        params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));

        mRequest.makeServiceRequest(IConstant.cartDetails, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------cartDetails Response----------------" + response);
                String sStatus = "";
                String message = "";
                double discountRate = 0;
                float total = 0;
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1")) {

                        JSONObject tax = object.getJSONObject("tax");
                        JSONObject cart = object.getJSONObject("cart");
                        strCartId = cart.getString("_id");
                        rlWalletBalance.setVisibility(View.VISIBLE);

                        wallet_amount = Float.parseFloat(cart.getString("wallet_amount"));
                        //txtServices.setText(getString(R.string.currency_symbol)+" "+tax.getString("service"));
                        gst = Float.parseFloat(tax.getString("gst"));
                        sgst = Float.parseFloat(tax.getString("sgst"));
                        if(type ==2) {
                            if (cart.has("coupon_code")) {
                                etPromoCode.setText(cart.getString("coupon_code"));
                                etPromoCode.setEnabled(false);

//                                imgApplyPromo.setVisibility(View.GONE);
                            }
                        }
                        service = Float.parseFloat(tax.getString("service"));

                        JSONArray packagesArray = cart.getJSONArray("packages");
                        JSONArray free_packages = cart.getJSONArray("free_packages");
                        if(packagesArray.length()>0)
                        {
                            for(int i=0;i<packagesArray.length();i++)
                            {
                                JSONObject packagesObject = packagesArray.getJSONObject(i);
                                SubscriptionCart subscriptionCart = new SubscriptionCart();
                                subscriptionCart.setStrId(packagesObject.getString("id"));
                                subscriptionCart.setTitle(packagesObject.getString("subj_name"));
                                float packagePrice = Float.parseFloat(packagesObject.getString("price"));
                                float afterGst = packagePrice+packagePrice*gst;
                                float serviceCharge = afterGst*service;
                                float sgstCharge = serviceCharge*sgst;
                                subscriptionCart.setPrice(String.valueOf(Math.round(afterGst+serviceCharge+sgstCharge)));
                                if(packagesObject.getString("status").equalsIgnoreCase("1") &&packagesObject.getString("s_status").equalsIgnoreCase("1") && packagesObject.getString("g_status").equalsIgnoreCase("1") && packagesObject.getString("g_trial").equalsIgnoreCase("0")) {
                                    subscriptionCart.setSubscription(packagesObject.getString("duration_days") + " " + getString(R.string.days) + " " + packagesObject.getString("duration_hours") +  " " +getString(R.string.hours));
                                }
                                else {
                                    subscriptionCart.setSubscription("Not Available");
                                    subscriptionCart.setPrice("0");
                                }

                                total= total+Float.parseFloat(subscriptionCart.getPrice());
                                mSubscriptionCartList.add(subscriptionCart);

                            }



                        }
                        if(cart.has("promo_type") && type == 2)
                        {
                            if(cart.getString("promo_type").equalsIgnoreCase("1"))
                            {
                                rlDiscount.setVisibility(View.VISIBLE);
                                txtDiscountLabel.setText(getString(R.string.discount));

                                discountRate= Math.round(Double.parseDouble(cart.getString("discount_amount")));
                                Log.e("discountRate",""+discountRate);
                                txtServices.setText(cart.getString("discount_amount").equals("0")?getString(R.string.currency_symbol) + " " + cart.getString("discount_amount"):"-"+getString(R.string.currency_symbol) + " " +cart.getString("discount_amount"));
                                if(cart.getString("discount_type").equalsIgnoreCase("1"))
                                {
                                    txtDiscountLabel.setText(getString(R.string.discount) +" "+ cart.getString("discount_value")+"%");
                                }
                            }else if(cart.getString("promo_type").equalsIgnoreCase("2"))
                            {
                                if(cart.has("wallet_credit_amt")) {

                                    txtSubGSTLabel.setVisibility(View.VISIBLE);
                                    txtSubGSTLabel.setText("Wallet Cash Back : " + getString(R.string.currency_symbol) +" "+cart.getString("wallet_credit_amt"));
                                }
                            }else if(cart.getString("promo_type").equalsIgnoreCase("3"))
                            {
                                txtSubGSTLabel.setVisibility(View.VISIBLE);
                                txtSubGSTLabel.setText("Bonus Duration : " +cart.getString("duration_days")+" "+getString(R.string.days)+" "+cart.getString("duration_hours")+" "+getString(R.string.hours));
                            }
                            else if(cart.getString("promo_type").equalsIgnoreCase("4"))
                            {
                                txtSubGSTLabel.setVisibility(View.VISIBLE);
                                txtSubGSTLabel.setText("Free Packages : " + free_packages.length());
                            }
                        }



                        txtSubTotal.setText(getString(R.string.currency_symbol)+" "+Math.round(total));
                        txtCartTotalAmount.setText(getString(R.string.currency_symbol)+" "+Math.round(total));
                        grandTotal = total-wallet_amount-promoAmt-discountRate>0 ? Math.round(total-wallet_amount-promoAmt-discountRate) : 0;
                        txtGrandTotal.setText(getString(R.string.currency_symbol)+" "+Math.round(grandTotal));
                        txtPayment.setText(grandTotal>0?getString(R.string.PAYMENT) : getString(R.string.PAY));
                        txtWalletBalanceAmount.setText(cart.getString("wallet_amount").equals("0")?getString(R.string.currency_symbol) + " " + cart.getString("wallet_amount"):wallet_amount>total?"-"+getString(R.string.currency_symbol) + " " + Math.round(total) : "-"+getString(R.string.currency_symbol) + " " + Math.round(Float.parseFloat(cart.getString("wallet_amount"))));
                        if(free_packages.length()>0)
                        {
                            for(int i=0;i<free_packages.length();i++)
                            {
                                JSONObject packagesObject = free_packages.getJSONObject(i);
                                SubscriptionCart subscriptionCart = new SubscriptionCart();
                                subscriptionCart.setStrId(packagesObject.getString("id"));
                                subscriptionCart.setTitle(packagesObject.getString("subj_name")+" (Free Package)");
                                subscriptionCart.setSubscription(packagesObject.getString("duration_days") +" " + getString(R.string.days) + " " + packagesObject.getString("duration_hours") + " " +getString(R.string.hours));

                                float packagePrice = Float.parseFloat(packagesObject.getString("price"));
                                float afterGst = packagePrice+packagePrice*gst;
                                float serviceCharge = afterGst*service;
                                float sgstCharge = serviceCharge*sgst;
                                subscriptionCart.setPrice(String.valueOf(Math.round(afterGst+serviceCharge+sgstCharge)));
                                mSubscriptionCartList.add(subscriptionCart);

                            }
//                            listoutCartItems();


                        }
                        loader.dismissLoader();
                        txtTotalItems.setText(mSubscriptionCartList.size()+"");

                        listoutCartItems();




                    } else if(sStatus.equals("00")){
                        loader.dismissLoader();
                        customAlert.singleLoginAlertLogout();
                    } else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    } else {
                        loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(ReviewOrder.this,getString(R.string.alert_oops),errorMessage);
            }
        });

    }
    public  String roundTwoDecimalsString(double d) {
        /*DecimalFormat twoDForm = new DecimalFormat("#.##");*/
//        DecimalFormat df = new DecimalFormat("#.##");
//        String formatted = df.format(d);

        return String.format("%.2f", d);
    }
    private void populateCartItems() {
        exector.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mSubscriptionCartList.clear();
                mSubscriptionCartList = cartlistDatabase.cartListDao().getAll();
                ActionEvent event=new ActionEvent();
                event.setAction("successReviewCart");
                EventBus.getDefault().post(event);
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txtBack:
                finish();
                break;

            case R.id.rlBackContactUs:
                finish();
                break;


           /* case R.id.txt:

                break;*/

            case R.id.imgApplyPromo:
                if(imgApplyPromo.getText().toString().equalsIgnoreCase("Apply")) {
                    if (etPromoCode.getText().toString().isEmpty())
                        AlertBox.showSnackBox(ReviewOrder.this, getString(R.string.alert_oops), "please enter promo code");
                    else
                        applyPromo();
                }else
                {
                    removePromo();
                }
                break;
        }
    }

    private void applyPromo() {
        loader.showLoader();

        ServiceRequest mRequest = new ServiceRequest(ReviewOrder.this);
        HashMap<String,String> params = new HashMap<>();
        params.put("cart_id",strCartId);
        params.put("coupon_code",etPromoCode.getText().toString());
        params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));

        mRequest.makeServiceRequest(IConstant.applyPromo, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------applyPromo Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    txtErrorPromo.setVisibility(View.GONE);
                    if (sStatus.equalsIgnoreCase("1")) {

                        etPromoCode.setVisibility(View.GONE);
                        txtHavePromo.setVisibility(View.GONE);

                        llPromoApplied.setVisibility(View.VISIBLE);
                        imgApplyPromo.setText("Remove");
                        txtPromoCodeApplied.setText(etPromoCode.getText().toString());
                        imgApplyPromo.setTextColor(Color.RED);
                        etPromoCode.setEnabled(false);
                        customAlert.showAlertOk(getString(R.string.alert_success),message);

                        loader.dismissLoader();
                        getCartDetails(2);
                    } else if(sStatus.equals("00")){
                        loader.dismissLoader();
                        customAlert.singleLoginAlertLogout();
                    } else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    } else if(sStatus.equalsIgnoreCase("0") && message.equalsIgnoreCase("Invalid promo code")){
                        txtErrorPromo.setVisibility(View.VISIBLE);
                        txtErrorPromo.setText(message);
                        loader.dismissLoader();
                    }else
                    {
                        loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(ReviewOrder.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void removePromo() {
        loader.showLoader();

        ServiceRequest mRequest = new ServiceRequest(ReviewOrder.this);
        HashMap<String,String> params = new HashMap<>();
        params.put("cart_id",strCartId);
        params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));

        mRequest.makeServiceRequest(IConstant.removePromo, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------removePromo Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");

                    if (sStatus.equalsIgnoreCase("1")) {
                        rlDiscount.setVisibility(View.GONE);
                        rlWalletBalance.setVisibility(View.GONE);
                        txtSubGSTLabel.setVisibility(View.GONE);

                        etPromoCode.setVisibility(View.VISIBLE);
                        txtHavePromo.setVisibility(View.VISIBLE);
                        llPromoApplied.setVisibility(View.GONE);
                        etPromoCode.setEnabled(true);
                        etPromoCode.setText("");
                        txtPromoCodeApplied.setText("");
                        imgApplyPromo.setText("Apply");
                        imgApplyPromo.setTextColor(getResources().getColor(R.color.mcq_blue));
                        customAlert.showAlertOk(getString(R.string.alert_success),message);

                        loader.dismissLoader();
                        getCartDetails(2);
                    } else if(sStatus.equals("00")){
                        loader.dismissLoader();
                        customAlert.singleLoginAlertLogout();
                    } else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    } else {
                        loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(ReviewOrder.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ActionEvent event) {

        if(event.getAction().equals("successReviewCart")){
            listoutCartItems();
        }

    }

    private void listoutCartItems() {
        rv_price
                .setLayoutManager(new LinearLayoutManager(ReviewOrder.this, RecyclerView.VERTICAL, false));
        ReviewOrderCartAdapter priceAdapter = new ReviewOrderCartAdapter(ReviewOrder.this, mSubscriptionCartList,height1);
        rv_price.setAdapter(priceAdapter);

        snapHelper.attachToRecyclerView(rv_price);
        //setDefaultHeight();
    }

    private void setDefaultHeight() {
        int width = parentContainer.getWidth();
        int height = parentContainer.getHeight();

        if(height1<1200)
        {
            if(mSubscriptionCartList.size()>2)
            {
                ViewGroup.LayoutParams params=rlRecycleContainer.getLayoutParams();
                if(getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(ReviewOrder.this) == 0 || DisplayOrientation.getDisplayOrientation(ReviewOrder.this) == 2))
                    params.height = (int) (getScreenHeightInDPs(ReviewOrder.this)*(0.42));
                else
                    params.height=(int) (height1*(0.33));
                Log.e("height",""+params.height);
                params.width= ViewGroup.LayoutParams.MATCH_PARENT;
                rlRecycleContainer.setLayoutParams(params);
            }else{
                ViewGroup.LayoutParams params=rlRecycleContainer.getLayoutParams();
                params.height= ViewGroup.LayoutParams.WRAP_CONTENT;
                params.width= ViewGroup.LayoutParams.MATCH_PARENT;
                rlRecycleContainer.setLayoutParams(params);
            }
        }else if(height1<1700 && height1>1200)
        {
            if(mSubscriptionCartList.size()>3)
            {
                ViewGroup.LayoutParams params=rlRecycleContainer.getLayoutParams();
                if(getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(ReviewOrder.this) == 0 || DisplayOrientation.getDisplayOrientation(ReviewOrder.this) == 2))
                    params.height = (int) (getScreenHeightInDPs(ReviewOrder.this)*(0.42));
                else
                    params.height=(int) (height1*(0.33));
                Log.e("height",""+params.height);
                params.width= ViewGroup.LayoutParams.MATCH_PARENT;
                rlRecycleContainer.setLayoutParams(params);
            }else{
                ViewGroup.LayoutParams params=rlRecycleContainer.getLayoutParams();
                params.height= ViewGroup.LayoutParams.WRAP_CONTENT;
                params.width= ViewGroup.LayoutParams.MATCH_PARENT;
                rlRecycleContainer.setLayoutParams(params);
            }
        }else {
            if(mSubscriptionCartList.size()>3)
            {
                ViewGroup.LayoutParams params=rlRecycleContainer.getLayoutParams();
                if(getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(ReviewOrder.this) == 0 || DisplayOrientation.getDisplayOrientation(ReviewOrder.this) == 2))
                    params.height = (int) (getScreenHeightInDPs(ReviewOrder.this)*(0.42));
                else
                    params.height=(int) (height1*(0.43));
                Log.e("height",""+params.height);
                params.width= ViewGroup.LayoutParams.MATCH_PARENT;
                rlRecycleContainer.setLayoutParams(params);
            }else{
                ViewGroup.LayoutParams params=rlRecycleContainer.getLayoutParams();
                params.height= ViewGroup.LayoutParams.WRAP_CONTENT;
                params.width= ViewGroup.LayoutParams.MATCH_PARENT;
                rlRecycleContainer.setLayoutParams(params);
            }
        }

    }
    private  int getScreenHeightInDPs(Context context){
        DisplayMetrics dm = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(dm);
        /*
            In this example code we converted the float value
            to nearest whole integer number. But, you can get the actual height in dp
            by removing the Math.round method. Then, it will return a float value, you should
            also make the necessary changes.
        */

        /*
            public int heightPixels
                The absolute height of the display in pixels.

            public float density
             The logical density of the display.
        */
        int heightInDP = Math.round(dm.heightPixels / dm.density);
        return heightInDP;
    }
    private void  deletePackage(final SubscriptionCart priceList,final int position) {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(ReviewOrder.this);
        HashMap<String,String> params = new HashMap<>();
        params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("package_id",priceList.getStrId());
        mRequest.makeServiceRequest(IConstant.remove_package, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------remove_package Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1")) {

                        mSubscriptionCartList.remove(position);
                        cartlistDatabase.cartListDao().delete(priceList);
                        Log.e("DeletedCartItems", "deleted");
                        listoutCartItems();


                    } else if(sStatus.equals("00")){
                        customAlert.singleLoginAlertLogout();
                    } else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(ReviewOrder.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    public void startPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", "NeetBook");
            options.put("description", "Payable Amount");
            //You can omit the image option to fetch the image from dashboard
           // options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", String.valueOf(roundTwoDecimalsString(grandTotal*100)));

            JSONObject preFill = new JSONObject();
            preFill.put("email", sessionManager.getUserDetails().get(SessionManager.KEY_EMAIL));
            preFill.put("contact", sessionManager.getUserDetails().get(SessionManager.KEY_MOBILE));

            options.put("prefill", preFill);

            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }
    @Override
    public void onPaymentSuccess(final String s) {
        startActivity(new Intent(ReviewOrder.this,PaymentActivity.class)
                .putExtra("status","success")
                .putExtra("payId",s)
                .putExtra("amount",""+grandTotal)
                .putExtra("cart_id",""+strCartId)
                .putExtra("gst",""+gst)
                .putExtra("service",""+service)
                .putExtra("sgst",""+sgst)
                .putExtra("wallet_amount",""+wallet_amount)
        );
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
       /* try {
            loader.showLoader();
            ServiceRequest mRequest = new ServiceRequest(ReviewOrder.this);

            mRequest.makeServiceRequestExceptHeader(IConstant.getPaymentDetails+s, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
                @Override
                public void onCompleteListener(String response) {
                    System.out.println("------------getPaymentDetails Response----------------" + response);
                    loader.dismissLoader();
                    savePaymentDetails(s,response);
                }
                @Override
                public void onErrorListener(String errorMessage) {
                    loader.dismissLoader();
                    AlertBox.showSnackBoxPrimaryBa(ReviewOrder.this,getString(R.string.alert_oops),errorMessage);
                }
            });


            Log.e("Payment", "Payment Successful: razor pay PaymentID = " + s);
        } catch (Exception e) {
            Log.e("Payment", "Exception in onPaymentSuccess", e);
        }
*/
//        try {
//        RazorpayClient razorpay = new RazorpayClient("rzp_test_IltK5ri6eSjMKq","pxImD9Hai6c8dycNbNfGvTqJ");
//
//            Payment payment = razorpay.Payments.fetch(s);
//            Log.e("Payment", new Gson().toJson(payment));
//        } catch (RazorpayException e) {
//            // Handle Exception
//            System.out.println(e.getMessage());
//            Log.e("Payment", e.getMessage());
//        }

    }
    @Override
    public void onPaymentError(int i, String s) {
        try {
            startActivity(new Intent(ReviewOrder.this,PaymentActivity.class).putExtra("status",s));
            overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
            Log.e("Payment", "Payment failed: " + i + " " + s);
        } catch (Exception e) {
            Log.e("Payment", "Exception in onPaymentError", e);
        }
    }
    private void savePaymentDetails(final String s, final String gateWayResponse) {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(ReviewOrder.this);
        HashMap<String,String> params = new HashMap<>();
        params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("cart_id",strCartId);
        params.put("gst", String.valueOf(gst));
        params.put("service", String.valueOf(service));
        params.put("sgst", String.valueOf(sgst));
        params.put("wallet_amount", String.valueOf(wallet_amount));
        params.put("amount", String.valueOf(grandTotal));

        mRequest.makeServiceRequest(IConstant.completePayment, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------remove_package Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1")) {




                    } else if(sStatus.equals("00")){
                        customAlert.singleLoginAlertLogout();
                    } else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(ReviewOrder.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {


        outState.putInt("llPromoApplied", llPromoApplied.getVisibility());
        Log.e("onSaveInstanceState","llPromoApplied.getVisibility() > "+llPromoApplied.getVisibility());
        Log.e("onSaveInstanceState","etPromoCode.getVisibility() > "+etPromoCode.getVisibility());
        Log.e("onSaveInstanceState","txtHavePromo.getVisibility() > "+txtHavePromo.getVisibility());
        Log.e("onSaveInstanceState","imgApplyPromo.gettext() > "+imgApplyPromo.getText().toString());
        Log.e("onSaveInstanceState","txtPromoCodeApplied.gettext() > "+txtPromoCodeApplied.getText().toString());
        Log.e("onSaveInstanceState","imgApplyPromo.setTextColor() > "+Color.RED);
        Log.e("onSaveInstanceState","etPromoCode.setenable() > "+etPromoCode.isEnabled());
        outState.putString("promoCode", etPromoCode.getText().toString().isEmpty()?"":etPromoCode.getText().toString());
        outState.putInt("type", loadingType);
        outState.putInt("llPromoAppliedVisiblity", llPromoApplied.getVisibility());
        outState.putInt("etPromoCodedVisiblity", etPromoCode.getVisibility());
        outState.putInt("txtHavePromoVisiblity", txtHavePromo.getVisibility());
        outState.putString("imgApplyPromoText", imgApplyPromo.getText().toString());
        outState.putString("txtPromoCodeAppliedText", txtPromoCodeApplied.getText().toString());
        outState.putInt("imgApplyPromoTextColor", Color.RED);
        outState.putBoolean("etPromoCodeSatus", etPromoCode.isEnabled());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        int mCurrentType = savedInstanceState.getInt("type");
        getCartDetails(mCurrentType);
        if(savedInstanceState.getInt("llPromoAppliedVisiblity") == 0)
        {
            llPromoApplied.setVisibility(View.VISIBLE);
            etPromoCode.setVisibility(View.GONE);
            txtHavePromo.setVisibility(View.GONE);
            imgApplyPromo.setText("Remove");
            txtPromoCodeApplied.setText(savedInstanceState.getString("txtPromoCodeAppliedText"));
            imgApplyPromo.setTextColor(Color.RED);
            etPromoCode.setEnabled(false);
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

//    @Override
//    public void onPaymentSuccess(String s, PaymentData paymentData) {
//        Log.e("s",s);
//        Log.e("paymentData",new Gson().toJson(paymentData).toString());
//
//    }
//
//    @Override
//    public void onPaymentError(int i, String s, PaymentData paymentData) {
//        Log.e("paymentData",new Gson().toJson(paymentData).toString());
//    }
}
