package com.app.neetbook.View.SideMenu;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.neetbook.Adapter.ArticleTabsAdapter;
import com.app.neetbook.Adapter.HomeGroupNameAdapter;
import com.app.neetbook.Interfaces.ArticleContentSummaryGet;
import com.app.neetbook.Interfaces.FragmentRefreshListener;
import com.app.neetbook.Interfaces.NetworkStateReceiverListener;
import com.app.neetbook.Interfaces.OnItemCycleMenuStateChangedListener;
import com.app.neetbook.Interfaces.OnMobileHomeMenuSelectedListioner;
import com.app.neetbook.Model.HeadData;
import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.PointsContent;
import com.app.neetbook.Model.SuperDetails;
import com.app.neetbook.Model.sidemenuFromContent.CategoryArticle;
import com.app.neetbook.Model.sidemenuFromContent.CategoryPoints;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ActionBarActivityNeetBook;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.NetworkStateReceiver;
import com.app.neetbook.Utils.OnSwipeTouchListener;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.SmoothScroll.LinearLayoutManagerWithSmoothScroller;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.Utils.Thread.AppExecutors;
import com.app.neetbook.View.DrawerContentSlideActivity;
import com.app.neetbook.View.DynamicFragmentActivity;
import com.app.neetbook.View.Fragments.subscription.SubscriptionListFragment;
import com.app.neetbook.View.ScrollingActivity;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.cleveroad.sy.cyclemenuwidget.CycleMenuWidget;
import com.cleveroad.sy.cyclemenuwidget.OnMenuItemClickListener;
import com.cleveroad.sy.cyclemenuwidget.OnStateChangedListener;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.linroid.filtermenu.library.FilterMenu;
import com.linroid.filtermenu.library.FilterMenuLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.Fade;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;
import androidx.viewpager.widget.ViewPager;

import hugo.weaving.DebugLog;

public class SubscriptionArticleActivity extends ActionBarActivityNeetBook implements OnMenuItemClickListener, NetworkStateReceiverListener, View.OnClickListener, OnStateChangedListener, ArticleContentSummaryGet, OnItemCycleMenuStateChangedListener {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Context context;
    private TextView txt_subscription_article_header;
    private ImageView imageViewArticle, imageViewArticleSummary, imageViewBookmarkStable;
    private String strArticleId = "";
    private String strSummary = "";
    Loader loader;
    private SessionManager sessionManager;
    private CustomAlert customAlert;
    private int page = 1;
    int loadMoreEnable = 0;
    private int totalSubsCount;
    ArrayList<PointsContent> pointsContents;
    String strSubjectId = "", strHeadId = "", strHeadName = "", strLongName = "", strsuperHeading = "", chapter_name = "";
    private ImageView imageView2, imageView3, imageViewBookmark;
    private TextView maintitle_tv, textView3, txtSurgical, textView2Pin, textView3Pin;
    public static ArrayList<HeadDetails> headDataArrayList;
    private ArrayList<HeadData> headDataList;
    private NetworkStateReceiver networkStateReceiver;
    private FragmentRefreshListener fragmentRefreshListener;
    private LinearLayout top_reclerview_layout;

    private int currentSelectedPosition = 0;
    ArticleTabsAdapter homeGroupNameAdapter;
    LinearLayoutManager layoutManager;
    LinearLayoutManagerWithSmoothScroller layoutManagersmoothscroll;
    LinearSnapHelper snapHelper;
    RecyclerView recyclerViewGroups;
    Typeface tfBold, tfMedium, tfRegular;
    private CycleMenuWidget mCycleMenuWidget;
    private RelativeLayout cycle_menu_layout;
    private View side_view;
    private ArrayList<PointsContent> mPointsList = new ArrayList<>();
    private RelativeLayout rlParentRight, rlParent;
    ImageView imgClose;
    Typeface tf;
    AppBarLayout appBarLayout;
    CollapsingToolbarLayout collapsingToolbarLayout;
    private TextView content_title_tv;
    private TextView article_title_tv;
    private RelativeLayout content_title_layout;
    private LinearLayout llRowOne;
    private RelativeLayout main_history_title_layout;
    private RelativeLayout history_title_layout;
    private RelativeLayout summary_icon_layout;
    ConstraintLayout cLConstrain;
    Toolbar toolbar;
    Display display;
    int height1, width1;
    private String Subject_name="";
    private String is_coming_page="";

    AppExecutors appExecutors;

    Animation FadeinAnimation;
    Animation FadeoutAnimation;
    private FilterMenuLayout filter_menu1;
    private FilterMenuLayout filter_menu2;

    public SubscriptionArticleActivity() {
        // Required empty public constructor
    }

    //
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_subscription_article);
        StatusBarColorChange.updateStatusBarColor(SubscriptionArticleActivity.this, getResources().getColor(R.color.article_content_page_pink));
   /* getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
            WindowManager.LayoutParams.FLAG_SECURE);*/

        context = getApplicationContext();
        appExecutors=new AppExecutors();
        init();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }

    @SuppressLint("ResourceAsColor")
    private void init() {

        FadeinAnimation = AnimationUtils.loadAnimation(this,
                R.anim.fade_in);
        FadeoutAnimation = AnimationUtils.loadAnimation(this,
               R.anim.fade_out);
        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width1 = size.x;
        height1 = size.y;
        tfBold = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaRegular.otf");
        sessionManager = new SessionManager(SubscriptionArticleActivity.this);
        customAlert = new CustomAlert(SubscriptionArticleActivity.this);
        loader = new Loader(SubscriptionArticleActivity.this);
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        viewPager = findViewById(R.id.viewpager_subscription_category);
        imageViewBookmark = findViewById(R.id.imageViewBookmark);
        tabLayout = findViewById(R.id.tabs_subscription_category);
        imageView2 = findViewById(R.id.imageView2);
        imageView3 = findViewById(R.id.imageView3);
        maintitle_tv = findViewById(R.id.textView);
        article_title_tv=(TextView)findViewById(R.id.article_title_tv);
        llRowOne=(LinearLayout)findViewById(R.id.llRowOne);
        main_history_title_layout=(RelativeLayout)findViewById(R.id.main_history_title_layout);
        history_title_layout=(RelativeLayout)findViewById(R.id.history_title_layout);
        summary_icon_layout=(RelativeLayout)findViewById(R.id.summary_icon_layout);
        txtSurgical = findViewById(R.id.textView2);
        textView3 = findViewById(R.id.textView3);
        textView2Pin = findViewById(R.id.textView2Pin);
        textView3Pin = findViewById(R.id.textView3Pin);
        rlParent = findViewById(R.id.rlParent);
        rlParentRight = findViewById(R.id.rlParentRight);
        toolbar = findViewById(R.id.toolbar);
        txt_subscription_article_header = findViewById(R.id.txt_subscription_article_header);
        imageViewArticle = findViewById(R.id.imageViewArticle);
        imageViewArticleSummary = findViewById(R.id.imageViewArticleSummary);
        imageViewBookmarkStable = findViewById(R.id.imageViewBookmarkStable);
        recyclerViewGroups = findViewById(R.id.recyclerViewGroups);
        cLConstrain = findViewById(R.id.cLConstrain);
        top_reclerview_layout=findViewById(R.id.top_reclerview_layout);
      //  cycle_menu_layout=(RelativeLayout)findViewById(R.id.cycle_menu_layout);
        side_view=(View)findViewById(R.id.side_view);
        mCycleMenuWidget = (CycleMenuWidget) findViewById(R.id.itemCycleMenuWidget);
        mCycleMenuWidget.setVisibility(View.GONE);
        mCycleMenuWidget.setStateChangeListener(this);
       // mCycleMenuWidget.setCorner(CycleMenuWidget.CORNER.RIGHT_TOP);
        //mCycleMenuWidget.setMode(sessionManager.isNightModeEnabled() ? 1 : 0);
       // mCycleMenuWidget.setAlpha(0.9f);
        mCycleMenuWidget.setOnMenuItemClickListener(this);

        filter_menu1=(FilterMenuLayout)findViewById(R.id.filter_menu1);
        filter_menu2=(FilterMenuLayout)findViewById(R.id.filter_menu2);
        attachMenu1(filter_menu1);
        attachMenu2(filter_menu2);

        headDataList = new ArrayList<>();//superHeading
        if (getIntent().getExtras() != null) {
            strArticleId = getIntent().getStringExtra("chapter_id");
            strHeadId = getIntent().getStringExtra("head_id");
            strSubjectId = getIntent().getStringExtra("subject_id");
            maintitle_tv.setText(getIntent().getStringExtra("subject_name")); //main title
            strLongName = getIntent().getStringExtra("subject_name");
            chapter_name = getIntent().getStringExtra("chapter_name");

            Subject_name=getIntent().getStringExtra("subject_name");

            if(getIntent().getExtras().containsKey("coming_page")){

                is_coming_page=getIntent().getStringExtra("coming_page");
            }

            txtSurgical.setText(chapter_name);  //second title
            textView2Pin.setText(chapter_name);



            currentSelectedPosition = 0;
           if (sessionManager.getViewToBookMark() != null && !sessionManager.getViewToBookMark().isEmpty() && sessionManager.getViewToBookMark().equalsIgnoreCase("Yes")) {
                headDataArrayList =
                        new Gson().fromJson(sessionManager.getBookmarkDetails().get(SessionManager.HEAD_LIST), new TypeToken<List<HeadDetails>>() {
                        }.getType());
          }
            //headDetailsArrayList = (ArrayList<HeadDetails>) getIntent().getExtras().getSerializable("arrayList");
            //Log.e("arrayListSize",String.valueOf(headDetailsArrayList.size()));
            // getArticleContent();
        }



        getPoints();
        createSnap();
        setupViewPager(viewPager);

        listener();

        setFontSize();
    }

    private FilterMenu attachMenu1(FilterMenuLayout layout){
        return new FilterMenu.Builder(this)
                .addItem(R.drawable.point_icon_1)
                .addItem(R.drawable.point_icon_2)
                .addItem(R.drawable.point_icon_3)
                .addItem(R.drawable.point_icon_4)
                .addItem(R.drawable.point_icon_5)
                .attach(layout)
                .withListener(listener)
                .build();
    }



    private FilterMenu attachMenu2(FilterMenuLayout layout){
        return new FilterMenu.Builder(this)
                .addItem(R.drawable.point_icon_1)
                .addItem(R.drawable.point_icon_2)
                .addItem(R.drawable.point_icon_3)
                .addItem(R.drawable.point_icon_4)
                .addItem(R.drawable.point_icon_5)
                .attach(layout)
                .withListener(listener)
                .build();
    }


    FilterMenu.OnMenuChangeListener listener = new FilterMenu.OnMenuChangeListener() {
        @DebugLog
        @Override
        public void onMenuItemClick(View view, int position) {
            Toast.makeText(SubscriptionArticleActivity.this, "Touched position " + position, Toast.LENGTH_SHORT).show();
        }

        @DebugLog
        @Override
        public void onMenuCollapse() {

        }

        @DebugLog
        @Override
        public void onMenuExpand() {

        }
    };

    private void setCollapseingToolBar() {

        article_title_tv.setText(strHeadName);

        collapsingToolbarLayout =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        content_title_tv=(TextView)findViewById(R.id.content_title_tv);

        content_title_layout=(RelativeLayout)findViewById(R.id.content_title_layout);
        content_title_tv.setText(strHeadName);

        collapsingToolbarLayout.setTitle("");
        imgClose = findViewById(R.id.imgClose);
        appBarLayout = (AppBarLayout)findViewById(R.id.appBarLayout);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(is_coming_page.equals("homepage")){

                    Intent s=new Intent(getApplicationContext(),TabMainActivity.class);
                    s.putExtra("subjectId",strSubjectId);
                    s.putExtra("subjectName",sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_NAME));
                    s.putExtra("isTrial",sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_TRIAL_STATUS));
                    s.putExtra("subjectShortName",sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_SHORT_NAME));
                    s.putExtra("showing_page_position","0");
                    startActivity(s);
                    finish();
                    overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);

                } else{

                    finish();
                }


            }
        });
        collapsingToolbarLayout.setCollapsedTitleTextColor(getResources().getColor(R.color.white));
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(R.color.article_title_color));
        if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
            collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbarSmall);
            collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbarSmall);
            content_title_tv.setTextAppearance(context,R.style.expandedappbarSmall);
            article_title_tv.setTextAppearance(context,R.style.collapsedappbarMedium);
        } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
            collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbarMedium);
            collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbarMedium);
            content_title_tv.setTextAppearance(context,R.style.collapsedappbarMedium);
            article_title_tv.setTextAppearance(context,R.style.collapsedappbarMedium);
        } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
            collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbarLarge);
            collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbarLarge);
            content_title_tv.setTextAppearance(context,R.style.collapsedappbarLarge);
            article_title_tv.setTextAppearance(context,R.style.collapsedappbarLarge);
        }

        tf = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaBold.otf");
        collapsingToolbarLayout.setCollapsedTitleTypeface(tf);
        collapsingToolbarLayout.setExpandedTitleTypeface(tf);
        content_title_tv.setTypeface(tf);
        article_title_tv.setTypeface(tf);


        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                System.out.println("Collapse---"+verticalOffset);
                Log.e("Collapse---", String.valueOf(verticalOffset));

                if (verticalOffset < -30) {

                    TitleVisibleGoneAnimation(false);
                }


                if (verticalOffset < -100) {

                    // collapsingToolbarLayout.setTitle(strHeadName);
                    CollapsingToolbarLayout.LayoutParams layoutParams = (CollapsingToolbarLayout.LayoutParams) toolbar.getLayoutParams();
                    layoutParams.width = textView3Pin.getText().toString().isEmpty() ? (int) (width1 * (0.5)) : (int) (width1 * (0.2));
                    toolbar.setLayoutParams(layoutParams);

                    if(top_reclerview_layout.getVisibility()==View.VISIBLE){

                        VisibleGoneAnimation(false);

                    }
                   // recyclerViewGroups.setVisibility(View.GONE);
                    SummaryVisibleGoneAnimation(true);
                    //imageViewArticleSummary.setVisibility(View.VISIBLE);
                    textView2Pin.setVisibility(View.GONE);
                    textView3Pin.setVisibility(View.VISIBLE);

                   // cycle_menu_layout.setPadding(0,0,0,0);
                    //side_view.setVisibility(View.VISIBLE);
                   // side_view.setBackgroundColor(Color.TRANSPARENT);

                } else {

                    collapsingToolbarLayout.setTitle("");
                    CollapsingToolbarLayout.LayoutParams layoutParams = (CollapsingToolbarLayout.LayoutParams) toolbar.getLayoutParams();
                    layoutParams.width = (int) (width1 * (0.9));
                    toolbar.setLayoutParams(layoutParams);



                    if(top_reclerview_layout.getVisibility()==View.INVISIBLE || top_reclerview_layout.getVisibility()==View.GONE){

                        VisibleGoneAnimation(true);

                    }

                    TitleVisibleGoneAnimation(true);

                    //recyclerViewGroups.setVisibility(View.VISIBLE);
                    SummaryVisibleGoneAnimation(false);
                   // imageViewArticleSummary.setVisibility(View.GONE);
                    textView2Pin.setVisibility(View.GONE);
                    textView3Pin.setVisibility(View.GONE);

                   // cycle_menu_layout.setPadding(0,0,0,0);
                    side_view.setVisibility(View.GONE);

                }
            }
        });
    }


    private void VisibleGoneAnimation(Boolean show){

        try{

            Transition transition = new Fade();
            transition.setDuration(400);
            transition.addTarget(R.id.top_reclerview_layout);
            TransitionManager.beginDelayedTransition(top_reclerview_layout, transition);
            top_reclerview_layout.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
           // recyclerViewGroups.setVisibility(show ? View.VISIBLE : View.INVISIBLE);

            if(top_reclerview_layout.getVisibility()==View.INVISIBLE
                    || top_reclerview_layout.getVisibility()==View.GONE){

                collapse(top_reclerview_layout);

                if(main_history_title_layout.getVisibility()==View.VISIBLE){

                    MainTitleSetAnimation(false);
                }



            } else if(top_reclerview_layout.getVisibility()==View.VISIBLE){

                expand(top_reclerview_layout);


                if(main_history_title_layout.getVisibility()==View.GONE){

                    MainTitleSetAnimation(true);
                }


            }

        }catch(Exception e){

        }
    }


    private void TitleVisibleGoneAnimation(Boolean show){

        try{

            Transition transition = new Fade();
            transition.setDuration(300);
            transition.addTarget(R.id.content_title_layout);
            TransitionManager.beginDelayedTransition(content_title_layout, transition);
            content_title_layout.setVisibility(show ? View.VISIBLE : View.INVISIBLE);

        }catch(Exception e){

        }
    }

    private void SummaryVisibleGoneAnimation(Boolean show){

        try{

            Transition transition = new Fade();
            transition.setDuration(500);
            transition.addTarget(R.id.summary_icon_layout);
            TransitionManager.beginDelayedTransition(summary_icon_layout, transition);
            summary_icon_layout.setVisibility(show ? View.VISIBLE : View.GONE);
            if(show){

                filter_menu1.setVisibility(View.GONE);
                filter_menu2.setVisibility(View.VISIBLE);

            } else{

                filter_menu2.setVisibility(View.GONE);
                filter_menu1.setVisibility(View.VISIBLE);
            }



        }catch(Exception e){

            Log.e("Exception",e.toString());
        }
    }

    private void MainTitleSetAnimation(Boolean show){

        try{

            Transition transition = new Fade();
            transition.setDuration(500);
            transition.addTarget(R.id.main_history_title_layout);
            TransitionManager.beginDelayedTransition(main_history_title_layout, transition);
            main_history_title_layout.setVisibility(show ? View.VISIBLE : View.GONE);

        }catch(Exception e){

            Log.e("Exception",e.toString());
        }
    }

    private void getPoints() {

        mPointsList.clear();
        ServiceRequest mRequest = new ServiceRequest(SubscriptionArticleActivity.this);
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("subject_id", strSubjectId);
        params.put("chapter_id", strArticleId);

        mRequest.makeServiceRequest(IConstant.pointsContent, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------pointsContent Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message") ? obj.getString("message") : "";

                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalSubsCount = object.getInt("atotal");
                        JSONArray jsonArray = object.getJSONArray("points");

                        if (jsonArray.length() > 0) {
                            for (int j = jsonArray.length() - 1; j >= 0; j--) {
                                JSONObject point_object=jsonArray.getJSONObject(j);
                                PointsContent pointsContent = new PointsContent();
                                pointsContent.set_id(jsonArray.getJSONObject(j).getString("_id"));
                                pointsContent.setShort_name(jsonArray.getJSONObject(j).getString("short_name"));
                                pointsContent.setLong_name(jsonArray.getJSONObject(j).getString("long_name"));
                                pointsContent.setContent(jsonArray.getJSONObject(j).getString("content"));


//                    pointsContent.setStatus(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("status"));
//                    pointsContent.setCstatus(jsonObject.getJSONArray("pointDetails").getJSONObject(j).getString("cstatus"));
                                pointsContent.setPoint_name(jsonArray.getJSONObject(j).getString("point_name"));
                                mPointsList.add(pointsContent);
                            }


                          //  mCycleMenuWidget.setMenuRes(R.menu.points_menu, mPointsList.size());
                            mCycleMenuWidget.setMenuRes(R.menu.points_menu);
                            //mCycleMenuWidget.open(true);
                        }

                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });

    }

    private void setFontSize() {

    /*if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
      textView.setTextAppearance(SubscriptionArticleActivity.this,R.style.textViewSmallSubjectName);
      txtSurgical.setTextAppearance(SubscriptionArticleActivity.this,R.style.textViewSmallChaptName);
      textView3.setTextAppearance(SubscriptionArticleActivity.this,R.style.textViewSmallChaptName);
      textView2Pin.setTextAppearance(SubscriptionArticleActivity.this,R.style.textViewSmallChaptName);
      textView3Pin.setTextAppearance(SubscriptionArticleActivity.this,R.style.textViewSmallChaptName);
      txt_subscription_article_header.setTextAppearance(SubscriptionArticleActivity.this,R.style.textViewSmallContentTitle);
    }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {*/
        maintitle_tv.setTextAppearance(SubscriptionArticleActivity.this, R.style.textViewMediumSubjectName);
        txtSurgical.setTextAppearance(SubscriptionArticleActivity.this, R.style.textViewMediumChaptName);
        textView3.setTextAppearance(SubscriptionArticleActivity.this, R.style.textViewMediumChaptName);
        textView3Pin.setTextAppearance(SubscriptionArticleActivity.this, R.style.textViewMediumChaptName);
        textView2Pin.setTextAppearance(SubscriptionArticleActivity.this, R.style.textViewMediumChaptName);
        txt_subscription_article_header.setTextAppearance(SubscriptionArticleActivity.this, R.style.textViewMediumContentTitle);
   /* }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
      textView.setTextAppearance(SubscriptionArticleActivity.this,R.style.textViewLargeSubjectName);
      txtSurgical.setTextAppearance(SubscriptionArticleActivity.this,R.style.textViewLargeChaptName);
      textView3.setTextAppearance(SubscriptionArticleActivity.this,R.style.textViewLargeChaptName);
      textView3Pin.setTextAppearance(SubscriptionArticleActivity.this,R.style.textViewLargeChaptName);
      textView2Pin.setTextAppearance(SubscriptionArticleActivity.this,R.style.textViewLargeChaptName);
      txt_subscription_article_header.setTextAppearance(SubscriptionArticleActivity.this,R.style.textViewLargeContentTitle);
    }*/

        maintitle_tv.setTypeface(tfMedium);
        txtSurgical.setTypeface(tfMedium);
        textView3.setTypeface(tfMedium);
        textView3Pin.setTypeface(tfMedium);
        textView2Pin.setTypeface(tfMedium);
        txt_subscription_article_header.setTypeface(tfBold);
    }

    private void listener() {

        imageView3.setOnClickListener(this);
        imageView2.setOnClickListener(this);
        imageViewBookmark.setOnClickListener(this);
        imageViewBookmarkStable.setOnClickListener(this);
        imageViewArticle.setOnClickListener(this);
        imageViewArticleSummary.setOnClickListener(this);

        rlParent.setOnTouchListener(new OnSwipeTouchListener(SubscriptionArticleActivity.this) {
            public void onSwipeTop() {
                //Toast.makeText(SubscriptionArticleActivity.this, "top", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeRight() {
                //Toast.makeText(SubscriptionArticleActivity.this, "right", Toast.LENGTH_SHORT).show();
                finish();
            }

            public void onSwipeLeft() {
                startActivity(new Intent(SubscriptionArticleActivity.this, DynamicFragmentActivity.class).putExtra("subject_id", strSubjectId).putExtra("head_id", strHeadId).putExtra("title", strHeadName).putExtra("long_name", strLongName).putExtra("isPoints", "No").putExtra("chapterName", chapter_name));
            }

            public void onSwipeBottom() {
                //Toast.makeText(SubscriptionArticleActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });
        rlParentRight.setOnTouchListener(new OnSwipeTouchListener(SubscriptionArticleActivity.this) {
            public void onSwipeTop() {
                //Toast.makeText(SubscriptionArticleActivity.this, "top", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeRight() {

            }

            public void onSwipeLeft() {
                startActivity(new Intent(SubscriptionArticleActivity.this, DynamicFragmentActivity.class).putExtra("subject_id", strSubjectId).putExtra("head_id", strHeadId).putExtra("title", strHeadName).putExtra("long_name", strLongName).putExtra("isPoints", "No").putExtra("chapterName", chapter_name));
            }

            public void onSwipeBottom() {
                //Toast.makeText(SubscriptionArticleActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });

        mCycleMenuWidget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // mCycleMenuWidget.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageView2:
                finish();
                break;

            case R.id.imageViewArticleSummary:
                startActivity(new Intent(SubscriptionArticleActivity.this, ArticleDetailActivity.class).putExtra("title", strHeadName).putExtra("summary", strSummary));
                break;


            case R.id.imageViewArticle:
               // startActivity(new Intent(SubscriptionArticleActivity.this, ScrollingActivity.class).putExtra("title", strHeadName).putExtra("summary", strSummary));

                startActivity(new Intent(SubscriptionArticleActivity.this, ArticleDetailActivity.class).putExtra("title", strHeadName).putExtra("summary", strSummary));
                break;

            case R.id.imageViewBookmark:
                mCycleMenuWidget.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mCycleMenuWidget.open(true);
                    }
                }, 100);


                //sessionManager.updateBookMark(strHeadId,strSubjectId,strLongName,new Gson().toJson(headDataArrayList),strArticleId,chapter_name);
                break;

            case R.id.imageViewBookmarkStable:
                mCycleMenuWidget.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mCycleMenuWidget.open(true);
                    }
                }, 100);

                break;

        }
    }

    private void setupViewPager(final ViewPager viewPager) {
        final ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this);
        try {
            if (headDataArrayList.size() > 0) {
                Log.e("headDataArrayList", new Gson().toJson(headDataArrayList));
                for (int i = 0; i < headDataArrayList.size(); i++) {
                    if (headDataArrayList.get(i).isSH.equals("Yes")) {
                        HeadData data = new HeadData();
                        data._id = headDataArrayList.get(i).headData.get(0)._id;
                        data.short_name = headDataArrayList.get(i).headData.get(0).short_name;
                        data.long_name = headDataArrayList.get(i).headData.get(0).long_name;
                        data.url = headDataArrayList.get(i).headData.get(0).url;
                        data.superHeadingName = headDataArrayList.get(i).isSH.equals("1") ? headDataArrayList.get(i).superDetails.short_name : "";
                        headDataList.add(data);
                        viewPagerAdapter.addFragment(new SubscriptionArticleFragment(headDataArrayList.get(i).headData.get(0)._id, strSubjectId, headDataArrayList.get(i).headData.get(0).long_name, i,headDataArrayList.get(i).headData.get(0).url,Integer.parseInt(headDataList.get(0).count)), headDataArrayList.get(i).headData.get(0).short_name);
                    } else {
                        if (headDataArrayList.get(i).headData.size() > 0) {
                            for (int j = 0; j < headDataArrayList.get(i).headData.size(); j++) {
                                HeadData data = new HeadData();
                                data._id = headDataArrayList.get(i).headData.get(j)._id;
                                data.short_name = headDataArrayList.get(i).headData.get(j).short_name;
                                data.long_name = headDataArrayList.get(i).headData.get(j).long_name;
                                data.url = headDataArrayList.get(i).headData.get(j).url;
                                data.superHeadingName = headDataArrayList.get(i).isSH.equals("1") ? headDataArrayList.get(i).superDetails.short_name : "";
                                headDataList.add(data);
                                viewPagerAdapter.addFragment(new SubscriptionArticleFragment(headDataArrayList.get(i).headData.get(j)._id, strSubjectId, headDataArrayList.get(i).headData.get(0).long_name, i,headDataArrayList.get(i).headData.get(0).url,Integer.parseInt(headDataList.get(0).count)), headDataArrayList.get(i).headData.get(j).short_name);
                            }
                        }
                    }
                }
            }

        }catch (Exception e){

        }


        if (headDataList.size() > 0) {
            for (int i = 0; i < headDataList.size(); i++) {
                if (strHeadId.equals(headDataList.get(i)._id)) {
                    currentSelectedPosition = i;
                    textView3.setText(headDataList.get(i).superHeadingName.equals("") ? "" : headDataList.get(i).superHeadingName);
                    textView3Pin.setText(headDataList.get(i).superHeadingName.equals("") ? "" : headDataList.get(i).superHeadingName);
                    textView3.setVisibility(!headDataList.get(i).superHeadingName.equals("") ? View.VISIBLE : View.GONE);
                    textView3Pin.setVisibility(!headDataList.get(i).superHeadingName.equals("") ? View.VISIBLE : View.INVISIBLE);
                    strsuperHeading = headDataList.get(i).superHeadingName.equals("") ? "" : headDataList.get(i).superHeadingName;
                    strHeadName = headDataList.get(i).long_name;
                    sessionManager.updateBookMark("Article", headDataList.get(i)._id, strSubjectId, headDataList.get(i).long_name, new Gson().toJson(headDataArrayList), strArticleId, chapter_name, strsuperHeading, "", "");
                    sessionManager.LastReadSubjectBookmark(sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                            sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                                    sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME));
                }
            }
            txt_subscription_article_header.setText(headDataList.get(0).long_name);
        }

        setCollapseingToolBar();
        viewPager.setAdapter(viewPagerAdapter);

        tabLayout.setupWithViewPager(viewPager);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(viewPagerAdapter.getTabView(i));
            }
        }
        setGroupAdapter(currentSelectedPosition);
        viewPager.setCurrentItem(currentSelectedPosition);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
       /* setGroupAdapter(position);
        layoutManager = ((LinearLayoutManager)recyclerViewGroups.getLayoutManager());
        int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
        int centeredItemPosition = totalVisibleItems / 2;
        recyclerViewGroups.smoothScrollToPosition(position != 0 ?position-1 : position);
        strHeadId = headDataList.get(position)._id;
//        strHeadName = headDataList.get(position).short_name;
        strHeadName = headDataList.get(position).long_name;
        txt_subscription_article_header.setText(headDataList.get(position).long_name);
        textView3.setText(headDataList.get(position).superHeadingName.equals("")?"":headDataList.get(position).superHeadingName);
        textView3Pin.setText(headDataList.get(position).superHeadingName.equals("")?"":headDataList.get(position).superHeadingName);
        textView3.setVisibility(!headDataList.get(position).superHeadingName.equals("")?View.VISIBLE:View.INVISIBLE);
        textView3Pin.setVisibility(!headDataList.get(position).superHeadingName.equals("")?View.VISIBLE:View.INVISIBLE);
        collapsingToolbarLayout.setTitle(strHeadName);
        sessionManager.updateBookMark("Article",strHeadId,strSubjectId,headDataList.get(position).long_name,new Gson().toJson(headDataArrayList),strArticleId,chapter_name,headDataList.get(position).superHeadingName.equals("")?"":headDataList.get(position).superHeadingName,"","");
        appBarLayout.setExpanded(true);
        currentSelectedPosition = position;*/

                try{


                    setGroupAdapter(position);


                       /*   layoutManagersmoothscroll = ((LinearLayoutManagerWithSmoothScroller)recyclerViewGroups.getLayoutManager());
               int totalVisibleItems = layoutManagersmoothscroll.findLastVisibleItemPosition() - layoutManagersmoothscroll.findFirstVisibleItemPosition();
               int centeredItemPosition = totalVisibleItems / 2;
               recyclerViewGroups.smoothScrollToPosition(position != 0 ?position-1 : position);*/

                    strHeadId = headDataList.get(position)._id;
//        strHeadName = headDataList.get(position).short_name;
                    strHeadName = headDataList.get(position).long_name;
                    txt_subscription_article_header.setText(headDataList.get(position).long_name);
                    textView3.setText(headDataList.get(position).superHeadingName.equals("")?"":headDataList.get(position).superHeadingName);
                    textView3Pin.setText(headDataList.get(position).superHeadingName.equals("")?"":headDataList.get(position).superHeadingName);
                    textView3.setVisibility(!headDataList.get(position).superHeadingName.equals("")?View.VISIBLE:View.GONE);
                    textView3Pin.setVisibility(!headDataList.get(position).superHeadingName.equals("")?View.VISIBLE:View.INVISIBLE);
                    //collapsingToolbarLayout.setTitle(strHeadName);
                    content_title_tv.setText(strHeadName);
                    article_title_tv.setText(strHeadName);  //article_title_tv
                    sessionManager.updateBookMark("Article",strHeadId,strSubjectId,headDataList.get(position).long_name,new Gson().toJson(headDataArrayList),strArticleId,chapter_name,headDataList.get(position).superHeadingName.equals("")?"":headDataList.get(position).superHeadingName,"","");
                    sessionManager.LastReadSubjectBookmark(sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                            sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                            sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME));
                    appBarLayout.setExpanded(true);
                    currentSelectedPosition = position;

                }catch(Exception e){

                    Log.e("Exception",e.toString());
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

       /* viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                if (position != 0 && position != headDataList.size() - 1) {
                    page.setAlpha(0f);
                    page.setVisibility(View.VISIBLE);

                    // Start Animation for a short period of time
                    page.animate()
                            .alpha(1f)
                            .setDuration(page.getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }
        });*/
    }

    @Override
    public void networkAvailable() {

    }

    @Override
    public void networkUnavailable() {

    }

    @Override
    public void onPause() {
        // new AsyncUpdateSessionRunner("1").execute();
        super.onPause();
    }


    @Override
    public void onStop() {
        super.onStop();
        // Log.e("onStopMainActivity","Called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    private void setGroupAdapter(final int position) {

        if(homeGroupNameAdapter==null ) {

            layoutManagersmoothscroll=new LinearLayoutManagerWithSmoothScroller(SubscriptionArticleActivity.this, RecyclerView.HORIZONTAL, false);

            layoutManager=new LinearLayoutManager(SubscriptionArticleActivity.this, RecyclerView.HORIZONTAL, false);
            layoutManager.setStackFromEnd(true);
            homeGroupNameAdapter = new ArticleTabsAdapter(headDataList, SubscriptionArticleActivity.this, position);
            recyclerViewGroups.setLayoutManager(layoutManager);
            //snapHelper.attachToRecyclerView(recyclerViewGroups);
            recyclerViewGroups.setAdapter(homeGroupNameAdapter);

           // scrollToCenter(layoutManager,recyclerViewGroups,position);
            recyclerViewGroups.scrollToPosition(position);

        } else{

            homeGroupNameAdapter.notifyPosition(position);
            scrollToCenter(layoutManager,recyclerViewGroups,position);
        }

               // recyclerViewGroups.smoothScrollToPosition(position);
               // recyclerViewGroups.scrollToPosition(position);



                homeGroupNameAdapter.setOnGroupMenuSelectedListener(new OnMobileHomeMenuSelectedListioner() {
                    @Override
                    public void onGroupSelected(String groupId, int position) {

                        try {

//                    viewPager.setCurrentItem(position);
//                    layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
//                    int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
//                    int centeredItemPosition = totalVisibleItems / 2;
//                    recyclerViewGroups.smoothScrollToPosition(position != 0 ? position - 1 : position);
//                    recyclerViewGroups.setScrollY(centeredItemPosition);


                            viewPager.setCurrentItem(position);
                            scrollToCenter(layoutManager,recyclerViewGroups,position);
                          //  scrollToCenter(layoutManager,recyclerViewGroups,position);

                           /* layoutManagersmoothscroll = ((LinearLayoutManagerWithSmoothScroller) recyclerViewGroups.getLayoutManager());
                            int totalVisibleItems = layoutManagersmoothscroll.findLastVisibleItemPosition() - layoutManagersmoothscroll.findFirstVisibleItemPosition();
                            int centeredItemPosition = totalVisibleItems / 2;
                            recyclerViewGroups.smoothScrollToPosition(position != 0 ? position - 1 : position);
                            //  recyclerViewGroups.setScrollY(centeredItemPosition);*/


                        } catch (Exception e) {

                            Log.e("Exception",e.toString());

                        }

                    }
                });



    }




    public void scrollToCenter(LinearLayoutManager layoutManager, RecyclerView recyclerList, int clickPosition) {
        RecyclerView.SmoothScroller smoothScroller = createSnapScroller(recyclerList, layoutManager);

        if (smoothScroller != null) {
            smoothScroller.setTargetPosition(clickPosition);
            layoutManager.startSmoothScroll(smoothScroller);
        }
    }

    // This number controls the speed of smooth scroll
    private static final float MILLISECONDS_PER_INCH = 200f;

    private final static int DIMENSION = 2;
    private final static int HORIZONTAL = 0;
    private final static int VERTICAL = 1;

    @Nullable
    private LinearSmoothScroller createSnapScroller(RecyclerView mRecyclerView, final RecyclerView.LayoutManager layoutManager) {
        if (!(layoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider)) {
            return null;
        }
        return new LinearSmoothScroller(mRecyclerView.getContext()) {
            @Override
            protected void onTargetFound(View targetView, RecyclerView.State state, Action action) {
                int[] snapDistances = calculateDistanceToFinalSnap(layoutManager, targetView);
                final int dx = snapDistances[HORIZONTAL];
                final int dy = snapDistances[VERTICAL];
                final int time = calculateTimeForDeceleration(Math.max(Math.abs(dx), Math.abs(dy)));
                if (time > 0) {
                    action.update(dx, dy, time, mDecelerateInterpolator);
                }
            }


            @Override
            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
            }
        };
    }


    private int[] calculateDistanceToFinalSnap(@NonNull RecyclerView.LayoutManager layoutManager, @NonNull View targetView) {
        int[] out = new int[DIMENSION];
        if (layoutManager.canScrollHorizontally()) {
            out[HORIZONTAL] = distanceToCenter(layoutManager, targetView,
                    OrientationHelper.createHorizontalHelper(layoutManager));
        }

        if (layoutManager.canScrollVertically()) {
            out[VERTICAL] = distanceToCenter(layoutManager, targetView,
                    OrientationHelper.createHorizontalHelper(layoutManager));
        }
        return out;
    }


    private int distanceToCenter(@NonNull RecyclerView.LayoutManager layoutManager,
                                 @NonNull View targetView, OrientationHelper helper) {
        final int childCenter = helper.getDecoratedStart(targetView)
                + (helper.getDecoratedMeasurement(targetView) / 2);
        final int containerCenter;
        if (layoutManager.getClipToPadding()) {
            containerCenter = helper.getStartAfterPadding() + helper.getTotalSpace() / 2;
        } else {
            containerCenter = helper.getEnd() / 2;
        }
        return childCenter - containerCenter;
    }


    @Override
    public void onMenuItemClick(View view, int itemPosition) {
        startActivity(new Intent(SubscriptionArticleActivity.this, DynamicFragmentActivity.class).putExtra("isPoints", "Yes").putExtra("pointsTitle", mPointsList.get(itemPosition).getLong_name()).putExtra("pointsContent", mPointsList.get(itemPosition).getContent()).putExtra("chapterName", chapter_name));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
              mCycleMenuWidget.setVisibility(View.GONE);
              mCycleMenuWidget.close(true);
            }
        }, 300);
    }

    @Override
    public void onMenuItemLongClick(View view, int itemPosition) {

    }

    @Override
    public void onStateChanged(CycleMenuWidget.STATE state) {

    }

    @Override
    public void onOpenComplete() {

        //imageViewBookmarkStable.setVisibility(View.GONE);

        /*if(top_reclerview_layout.getVisibility()==View.GONE){

            side_view.setBackgroundColor(ContextCompat.getColor(context,R.color.subscription_green));
            side_view.setVisibility(View.VISIBLE);

        }*/

    }

    @Override
    public void onCloseComplete() {
        mCycleMenuWidget.setVisibility(View.GONE);
        //mCycleMenuWidget.startAnimation(FadeinAnimation);
       // imageViewBookmarkStable.setVisibility(View.VISIBLE);
       // imageViewBookmarkStable.startAnimation(FadeinAnimation);

       /* if(top_reclerview_layout.getVisibility()==View.GONE){

            side_view.setVisibility(View.GONE);
        }
*/
    }

    @Override
    public void onArticleContentChanged(String summary, int CurrentPosition) {
        if (currentSelectedPosition == CurrentPosition)
            strSummary = summary;
    }

    @Override
    public void onOpen(int position) {

        if(top_reclerview_layout.getVisibility()==View.GONE){

            side_view.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClose(int position) {

        if(top_reclerview_layout.getVisibility()==View.GONE){

            side_view.setVisibility(View.GONE);
        }
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        List<Fragment> fragmentList = new ArrayList<>();
        List<String> fragmentTitles = new ArrayList<>();
        Context mContext;
        TextView tabTitles;


        ViewPagerAdapter(FragmentManager fragmentManager,
                         FragmentActivity activity) {
            super(fragmentManager);
            mContext = activity;
        }

        @Override
        public Fragment getItem(int position) {
            return new SubscriptionArticleFragment(headDataList.get(position)._id, strSubjectId, headDataList.get(position).long_name, position,headDataList.get(position).url,Integer.parseInt(headDataList.get(position).count));
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitles.get(position);
        }

        void addFragment(Fragment fragment, String name) {
            fragmentList.add(fragment);
            fragmentTitles.add(name);
        }

        View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_items, null);
            tabTitles = v.findViewById(R.id.txtTabTitle);
            tabTitles.setText(fragmentTitles.get(position));
            tabTitles.setTextColor(getResources().getColor(R.color.white));
            return v;
        }
    }

    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
    }



    public static void expand(final View v) {
        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? WindowManager.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        int duration=(int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density);

        Log.e("collapseduration", String.valueOf(duration));

        // Expansion speed of 1dp/ms
        a.setDuration(150);
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        int duration=(int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density);

        Log.e("collapseduration", String.valueOf(duration));

        // Collapse speed of 1dp/ms
        a.setDuration(150);
        v.startAnimation(a);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(is_coming_page.equals("homepage")){

            Intent s=new Intent(getApplicationContext(),TabMainActivity.class);
            s.putExtra("subjectId",strSubjectId);
            s.putExtra("subjectName",sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_NAME));
            s.putExtra("isTrial",sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_TRIAL_STATUS));
            s.putExtra("subjectShortName",sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_SHORT_NAME));
            s.putExtra("showing_page_position","0");
            startActivity(s);
            finish();
            overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);

        } else{

            finish();
        }
    }
}
