package com.app.neetbook.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.app.neetbook.Model.Groupslist;
import com.app.neetbook.Model.TabSideMenu;
import com.app.neetbook.R;
import com.app.neetbook.Utils.AppWebViewClients;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.Utils.widgets.HTMLTextConverter;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class TermsConditions extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout rlBackContactUs;

    WebView webView;
    Loader loader;
    SessionManager sessionManager;
    CustomAlert customAlert;
    TextView txtTermsAndConditions;
    public static TermsConditions PagesActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(getResources().getBoolean(R.bool.isTablet))
        {
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }else
        {StatusBarColorChange.setStatusBarGradiant(TermsConditions.this);}
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_terms_conditions);
        PagesActivity = this;
        init();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }


    private void init() {
        sessionManager = new SessionManager(TermsConditions.this);
        customAlert = new CustomAlert(TermsConditions.this);
        loader = new Loader(TermsConditions.this);
        webView = (WebView) findViewById(R.id.webView);
        rlBackContactUs = findViewById(R.id.rlBackContactUs);
        webView.requestFocus();
        webView.getSettings().setLightTouchEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setGeolocationEnabled(true);
        webView.setWebViewClient(new AppWebViewClients(loader,TermsConditions.this));
        webView.setSoundEffectsEnabled(true);
        prepareTermsAndConditions();
        rlBackContactUs.setOnClickListener(this);


    }

    private void prepareTermsAndConditions() {


        ServiceRequest mRequest = new ServiceRequest(TermsConditions.this);

        mRequest.makeServiceRequest(IConstant.terms_and_conditions, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------terms_and_conditions Response----------------" + response);
                String sStatus = "";

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    if(jsonObject.getString("status").equals("1")) {
                        webView.loadData(jsonObject.getString("description"), "text/html; charset=UTF-8", null);
                    } else if(sStatus.equals("00")){

                        customAlert.singleLoginAlertLogout();
                    }else if(sStatus.equalsIgnoreCase("01")){

                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),jsonObject.getString("message"));
                    } else {

                        customAlert.showAlertOk(getString(R.string.alert_oops),jsonObject.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener(String errorMessage) {
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.rlBackContactUs:
                finish();
                break;
        }
    }

}
