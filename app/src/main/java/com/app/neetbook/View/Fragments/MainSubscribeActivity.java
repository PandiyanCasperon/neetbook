package com.app.neetbook.View.Fragments;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.neetbook.CartRoom.CartDataBase;
import com.app.neetbook.Model.TabSideMenu;
import com.app.neetbook.R;
import com.app.neetbook.Utils.Data.ChangeMobileData;
import com.app.neetbook.Utils.Data.ChangePinData;
import com.app.neetbook.Utils.Data.CompleteProfileData;
import com.app.neetbook.Utils.Data.ContactUsData;
import com.app.neetbook.Utils.Data.MyProfileData;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.View.Fragments.subscription.SubscriptionFragment;
import com.app.neetbook.View.MobilePagesActivity;
import com.app.neetbook.View.SignInSighUp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MainSubscribeActivity extends AppCompatActivity implements View.OnClickListener {
    CartDataBase cartlistDatabase;
    TextView txtPageTitle;
    String strMenuName = "";
    RelativeLayout rlBack;
    TabSideMenu tabSideMenu;
    SessionManager sessionManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_subscribe);

        if(!getResources().getBoolean(R.bool.isTablet))
        {
            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else
            StatusBarColorChange.setLoginStatusBarGradiant(MainSubscribeActivity.this);
        init();
    }
    private void init() {
        txtPageTitle = findViewById(R.id.txtPageTitle);
        rlBack = findViewById(R.id.rlBack);


        sessionManager = new SessionManager(this);
        cartlistDatabase = CartDataBase.getAppDatabase(MainSubscribeActivity.this);
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        switchToFragment();
    }

    private void switchToFragment() {
        Fragment fragment = new SubscriptionFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameContainer, fragment);
        fragmentTransaction.commit();
    }

    public void onBackPressed()
    {
        cartlistDatabase.cartListDao().nukeTable();
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.rlBack:
/*
                CompleteProfileData.clearData();
                MyProfileData.clearData();
                ChangePinData.clearData();
                ChangeMobileData.cleatData();
                ContactUsData.clearData();*/
                finish();

                break;
        }
    }
}
