package com.app.neetbook.View.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.CountDownTimer;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.app.neetbook.Interfaces.OnMobileNumberChanged;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Data.ChangeMobileData;

import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.Utils.alerter.AlertBox;
import com.app.neetbook.Utils.widgets.DisplayOrientation;
import com.app.neetbook.Utils.widgets.Pinview;

import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class ChangeMobileFragment extends Fragment implements View.OnClickListener {


    RelativeLayout rlChangeMobileSubmit,rlPinContainer;
    LinearLayout llOTP,llEnterPin;
    EditText etMobile,etOTP;
    Pinview pinview;
    TextView txtReset,txtResendOTP,enterPinHint;
    Loader loader;
    CustomAlert customAlert;
    ConnectionDetector cd;
    SessionManager sessionManager;
    OnMobileNumberChanged onMobileNumberChanged;
    CountDownTimer cTimer;
    SpannableString ss;
    public ChangeMobileFragment() {
        // Required empty public constructor
    }


    public static ChangeMobileFragment newInstance(String param1, String param2) {
        ChangeMobileFragment fragment = new ChangeMobileFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        sessionManager = new SessionManager(getActivity());
        loader = new Loader(getActivity());
        customAlert = new CustomAlert(getActivity());
        cd = new ConnectionDetector(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = null;
        if(getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(getActivity()) == 0 || DisplayOrientation.getDisplayOrientation(getActivity()) == 2))
            rootView = inflater.inflate(R.layout.fragment_change_mobile_portrait, container, false);
        else if(getResources().getBoolean(R.bool.isTablet) && (DisplayOrientation.getDisplayOrientation(getActivity()) == 1 || DisplayOrientation.getDisplayOrientation(getActivity()) == 3))
            rootView = inflater.inflate(R.layout.fragment_change_mobile, container, false);
        else if(!getResources().getBoolean(R.bool.isTablet))
            rootView = inflater.inflate(R.layout.fragment_change_mobile, container, false);
        init(rootView);
        return rootView;
    }

    private void init(View rootView) {
        rlChangeMobileSubmit = rootView.findViewById(R.id.rlChangeMobileSubmit);
        rlPinContainer = rootView.findViewById(R.id.rlPinContainer);
        llOTP = rootView.findViewById(R.id.llOTP);
        llEnterPin = rootView.findViewById(R.id.llEnterPin);
        etOTP = rootView.findViewById(R.id.etOTP);
        etMobile = rootView.findViewById(R.id.etMobile);
        pinview = rootView.findViewById(R.id.pinViewOld);
        txtReset = rootView.findViewById(R.id.txtReset);
        txtResendOTP = rootView.findViewById(R.id.txtResendOTP);
        enterPinHint = rootView.findViewById(R.id.enterPinHint);
        txtResendOTP.setTextColor(getActivity().getResources().getColor(R.color.mcq_blue));
        setPinWidth();
        getUserProfileData();

        listeners();
        if(getResources().getBoolean(R.bool.isTablet))
            setOrientationRestoreData();

    }

    private void setPinWidth() {
        if(getResources().getBoolean(R.bool.isTablet)) {
            double wisth = StatusBarColorChange.getViewWidth(rlPinContainer);
            if(DisplayOrientation.getDisplayOrientation(getActivity()) == 0 || DisplayOrientation.getDisplayOrientation(getActivity()) == 2) {
                pinview.setPinWidth((int) Math.round(wisth / 30.0));
                pinview.setSplitWidth((int) Math.round(wisth / 20.0));
            }else
            {
                pinview.setPinWidth((int) Math.round(wisth / 28.0));
                pinview.setSplitWidth((int) Math.round(wisth / 24.0));
            }
        }
    }

    private void setOrientationRestoreData() {
        etMobile.setText(ChangeMobileData.strMobile);
        etOTP.setText(ChangeMobileData.strEnteredOtp);
        pinview.setValue(ChangeMobileData.strPin);

        if(ChangeMobileData.strOtpVisible != null && !ChangeMobileData.strOtpVisible.equals("") &&  ChangeMobileData.strOtpVisible.equals("Yes"))
        {
            txtResendOTP.setVisibility(View.VISIBLE);
            txtReset.setVisibility(View.VISIBLE);
            llOTP.setVisibility(View.VISIBLE);
            llEnterPin.setVisibility(View.GONE);
//            if(ChangeMobileData.strOtp.length()==6)
//                rlChangeMobileSubmit.setBackground(getActivity().getResources().getDrawable(R.drawable.login_gradient));

        }else
        {
//            if(!etMobile.getText().toString().isEmpty() && etMobile.getText().toString().length()==10)
//            {
//                rlChangeMobileSubmit.setBackground(getActivity().getResources().getDrawable(R.drawable.login_gradient));
//            }else
//                rlChangeMobileSubmit.setBackground(getActivity().getResources().getDrawable(R.drawable.in_active_round_button));
            llOTP.setVisibility(View.GONE);

        }
        setSubmitEnableDisable();
    }

    private void listeners() {
        rlChangeMobileSubmit.setOnClickListener(this);
        txtResendOTP.setOnClickListener(this);
        txtReset.setOnClickListener(this);

        etMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setSubmitEnableDisable();
                if(!s.toString().isEmpty())
                    ChangeMobileData.strMobile = etMobile.getText().toString();
                if(!s.toString().isEmpty() && s.toString().length() == 10)
                    StatusBarColorChange.hideSoftKeyboard(getActivity());

            }
        });

        etOTP.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setSubmitEnableDisable();
                ChangeMobileData.strEnteredOtp = etOTP.getText().toString();
                if(!s.toString().isEmpty() && s.toString().length() == 6)
                    StatusBarColorChange.hideSoftKeyboard(getActivity());

            }
        });


        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                setSubmitEnableDisable();
                StatusBarColorChange.hideSoftKeyboard(getActivity());
            }

            @Override
            public void onDataEntering(Pinview pinview, boolean fromUser) {
                setSubmitEnableDisable();
                Log.e("OTP Typing","started");
                if(pinview.getValue() != null && !pinview.getValue().equals("") && pinview.getValue().length()>0) {
                    enterPinHint.setVisibility(View.GONE);
                    ChangeMobileData.strPin = pinview.getValue();
                }
                else{
                    enterPinHint.setVisibility(View.VISIBLE);
                    ChangeMobileData.strPin = "";}

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.rlChangeMobileSubmit:
                if(llEnterPin.getVisibility() == View.VISIBLE && !etMobile.getText().toString().isEmpty() && llOTP.getVisibility()==View.GONE)
                {
                    if(pinview.getValue() != null && !pinview.getValue().equals("") && pinview.getValue().length()>0)
                    {
                        if(pinview.getValue().equals(sessionManager.getPin()))
                        {
                            if(etMobile.getText().toString().length()==10) {
                                verifyMobile();
                            }else
                            {
                                AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),getString(R.string.alert_enter_valid_mobile));
                            }
                        }else
                        {
                            AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),getString(R.string.INVALID_PIN));
                        }
                    }else
                    {
                        AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),getString(R.string.ALERT_ENTER_PIN));
                    }

                }else if(!etMobile.getText().toString().isEmpty() && llOTP.getVisibility()==View.VISIBLE && llEnterPin.getVisibility() == View.GONE)
                {
                    if(!etOTP.getText().toString().isEmpty()) {
                        if(etOTP.getText().toString().length()==6) {
                        if(etOTP.getText().toString().equals(ChangeMobileData.strOtp)) {
                            changeMobileNumber();
                        }else
                        {
                            AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),getString(R.string.alert_enter_valid_otp));
                        }}else
                        {
                            AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),getString(R.string.alert_enter_valid_otp));
                        }
                    }else
                    {
                        AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),getString(R.string.alert_enter_valid_mobile));
                    }
                }
                break;


            case R.id.txtReset:
                {
                    resetFields();

                }
                break;

            case R.id.txtResendOTP:
                {
                    if(txtResendOTP.getText().toString().equalsIgnoreCase(getString(R.string.resend_otp)))
                        reSendOT();
                }
                break;
        }
    }
    private void reSendOT() {
        if(etMobile.getText().toString().isEmpty())
        {
            AlertBox.showSnackBox(getActivity(),getString(R.string.alert_alert),getString(R.string.alert_enter_email_mobile));

        }else {
            if(etMobile.getText().toString().length()==10) {
                verifyMobile();
            }else
            {
                AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),getString(R.string.alert_enter_valid_mobile));
            }

        }
    }
    private void setResendOTP()
    {
        if(cTimer==null)
            startCountDownTimer();
    }

    private void startCountDownTimer() {
        txtResendOTP.setVisibility(View.VISIBLE);
        cTimer = new CountDownTimer(ChangeMobileData.countDownTimer, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                ChangeMobileData.countDownTimer = millisUntilFinished;
                long Minutes = millisUntilFinished / (60 * 1000) % 60;
                long Seconds = millisUntilFinished / 1000 % 60;
                ss = new SpannableString(getString(R.string.resend_otp)+" in "+Minutes+":"+(Seconds>9?Seconds:"0"+Seconds));
                ss.setSpan(new ForegroundColorSpan(getActivity().getResources().getColor(R.color.light_gray)), 0, 13, 0);
                txtResendOTP.setText(ss);
            }

            @Override
            public void onFinish() {
                txtResendOTP.setText(getResources().getString(R.string.resend_otp));
                txtResendOTP.setTextColor(getActivity().getResources().getColor(R.color.mcq_blue));

                cTimer.cancel();
            }
        };
        cTimer.start();
    }

    private void resetFields()
    {
        ChangeMobileData.countDownTimer = ChangeMobileData.countDown;
        if(cTimer != null)
            cTimer.cancel();
        cTimer = null;
        llOTP.setVisibility(View.GONE);
        llEnterPin.setVisibility(View.VISIBLE);
        etMobile.setText("");
        pinview.setValue("");
        etOTP.setText("");
        etMobile.setEnabled(true);
        rlChangeMobileSubmit.setBackground(getActivity().getResources().getDrawable(R.drawable.in_active_round_button));
        txtReset.setVisibility(View.GONE);
        txtResendOTP.setVisibility(View.GONE);
        ChangeMobileData.cleatData();
    }
    private void verifyMobile() {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String,String> params = new HashMap<>();
        params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("phone",etMobile.getText().toString());
        mRequest.makeServiceRequest(IConstant.changeMobileOtp, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------changeMobile Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1") && message.equalsIgnoreCase("Success")) {
                        ChangeMobileData.strOtp = object.getJSONObject("response").getString("otp");
                        etOTP.setText(object.getJSONObject("response").getString("mode").equalsIgnoreCase("development")?object.getJSONObject("response").getString("otp") :"");
                        llOTP.setVisibility(View.VISIBLE);
                        llEnterPin.setVisibility(View.GONE);
                        etMobile.setEnabled(false);
                        pinview.setEnabled(false);
                        txtReset.setVisibility(View.VISIBLE);
                        ChangeMobileData.countDownTimer = ChangeMobileData.countDown;
                        cTimer = null;
                        setResendOTP();
                        ChangeMobileData.strOtpVisible = "Yes";
                        setSubmitEnableDisable();
                    }  else if(sStatus.equals("00")){
                        loader.dismissLoader();
                        customAlert.singleLoginAlertLogout();
                    } else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    }else {
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void  changeMobileNumber() {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String,String> params = new HashMap<>();
        params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("phone",etMobile.getText().toString());
        params.put("otp",etOTP.getText().toString());
        mRequest.makeServiceRequest(IConstant.changeMobileNumber, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------changeMobileNumber Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1")) {
                        ChangeMobileData.cleatData();
                        resetFields();
                        JSONObject responseObj = object.getJSONObject("response");
                        sessionManager.updateToken(responseObj.getString("token"));
                        showAlertSuccess(getString(R.string.alert_success),message);

                    } else if(sStatus.equals("00")){
                        loader.dismissLoader();
                        customAlert.singleLoginAlertLogout();
                    } else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(getActivity(),getString(R.string.alert_oops),errorMessage);
            }
        });
    }
    private void showAlertSuccess(String title, String desc)
    {
        final Dialog alertDialog = new Dialog(getActivity());
        alertDialog.setContentView(R.layout.custom_alert);
        alertDialog.setCancelable(false);

//        if(progressDialog.getWindow() != null)
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textViewTitle = alertDialog.findViewById(R.id.txtAlertTitle);
        TextView textViewDesc = alertDialog.findViewById(R.id.txtAlertDesc);
        ImageView imgAlert = alertDialog.findViewById(R.id.imgAlert);
        Picasso.with(getActivity()).load(R.drawable.right_mark).into(imgAlert);
        textViewTitle.setText(title);
        textViewDesc.setText(desc);
        Button btnOk = alertDialog.findViewById(R.id.btnOk);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        btnCancel.setVisibility(View.GONE);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if(onMobileNumberChanged != null)
                    onMobileNumberChanged.onMobileNumberChanged();
            }
        });
        alertDialog.show();
    }
    public void onStop()
    {
        super.onStop();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof OnMobileNumberChanged){
            onMobileNumberChanged = (OnMobileNumberChanged) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onMobileNumberChanged = null;
    }
    private void setSubmitEnableDisable()
    {
        if(llOTP.getVisibility() == View.GONE)
        {
            if(pinview.getValue() != null && !pinview.getValue().equals("") && pinview.getValue().length()==4 ) {
                if(!etMobile.getText().toString().isEmpty() && etMobile.getText().toString().length()==10)
                    rlChangeMobileSubmit.setBackground(getResources().getDrawable(R.drawable.login_gradient));
                else
                    rlChangeMobileSubmit.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
            }
            else {
                rlChangeMobileSubmit.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
            }
        }else
        {
            if(!etOTP.getText().toString().isEmpty() && etOTP.getText().toString().length()==6)
                rlChangeMobileSubmit.setBackground(getResources().getDrawable(R.drawable.login_gradient));
            else
                rlChangeMobileSubmit.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
        }

    }

    private void getUserProfileData() {

        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String,String> params = new HashMap<>();
        params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));

        mRequest.makeServiceRequest(IConstant.getUserProfileData, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------getUserProfileData Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1")) {


                    }  else if(sStatus.equals("00")){

                        customAlert.singleLoginAlertLogout();
                    } else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    }else {
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {

                try {
                    AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), errorMessage);
                }catch ( Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

    }
}
