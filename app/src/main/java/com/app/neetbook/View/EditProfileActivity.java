package com.app.neetbook.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.neetbook.Interfaces.OnSpinerItemClick;
import com.app.neetbook.Model.CollegeBean;
import com.app.neetbook.Model.StateBean;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ActionBarActivityNeetBook;
import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.app.neetbook.Utils.alerter.AlertBox;
import com.app.neetbook.Utils.widgets.SpinnerDialog;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class EditProfileActivity extends ActionBarActivityNeetBook implements View.OnClickListener {
    ArrayList<String> countryList;
    ArrayList<StateBean> countryBeanList;
    ArrayList<String> stateList;
    ArrayList<String> countryStateList;
    ArrayList<StateBean> countryStateBeanList;
    ArrayList<StateBean> cityBeanList;
    ArrayList<StateBean> stateBeanList;
    ArrayList<String> cityList;
    ArrayList<String> collegeList;
    ArrayList<CollegeBean> collegeBeanList;
    TextView txtMail,txtMobileNumber,txtResetCompleteProfile;
    Spinner spinnerStateEducation,spinnerState,spinnerCountry,spinnerCity,spinnerCollege;
    SessionManager sessionManager;
    EditText etAddress2,etFirstName,etLastName,etAddress1,etPincode;
    RelativeLayout rlCompleteProfileSubmit;
    Loader loader;
    CustomAlert customAlert;
    RelativeLayout rlBack;
    public static EditProfileActivity editProfileActivity;
    String strCountry = "",strState = "", strCity = "", strCountryId = "", strStateId = "", strCityId = "";
    TextView txtStateHint,txtEduStateHint,txtCityHint,txtCountryHint,txtEduCollegeHint;
    SpinnerDialog dialogCountry,dialogState,dialogCity;
    ConnectionDetector cd;
    private boolean showLoader = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StatusBarColorChange.setMobilePagesStatusBarGradiant(EditProfileActivity.this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile2);
        editProfileActivity = this;
        init();
        
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }

    private void init() {
        sessionManager = new SessionManager(EditProfileActivity.this);
        loader = new Loader(EditProfileActivity.this);
        customAlert = new CustomAlert(EditProfileActivity.this);
        cd = new ConnectionDetector(EditProfileActivity.this);
        showLoader = false;
        rlBack =  findViewById(R.id.rlBack);
        txtMail =  findViewById(R.id.txtMail);
        txtMobileNumber = findViewById(R.id.txtMobileNumber);
        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);
        etAddress1 = findViewById(R.id.etAddress1);
        etAddress2 = findViewById(R.id.etAddress2);
        etPincode = findViewById(R.id.etPincode);
        spinnerCountry = findViewById(R.id.spinnerCountry);
        spinnerCity = findViewById(R.id.spinnerCity);
        spinnerState = findViewById(R.id.spinnerState);
        spinnerStateEducation = findViewById(R.id.spinnerStateEducation);
        spinnerCollege = findViewById(R.id.spinnerCollege);
        txtResetCompleteProfile = findViewById(R.id.txtResetCompleteProfile);
        rlCompleteProfileSubmit = findViewById(R.id.rlCompleteProfileSubmit);
        txtCountryHint = findViewById(R.id.txtCountryHint);
        txtStateHint = findViewById(R.id.txtStateHint);
        txtCityHint = findViewById(R.id.txtCityHint);
        rlCompleteProfileSubmit.setOnClickListener(this);
        txtResetCompleteProfile.setOnClickListener(this);
        rlBack.setOnClickListener(this);

        if(cd.isConnectingToInternet()) {
            getUserProfileData();
        }else
        {
            startActivity(new Intent(EditProfileActivity.this,NoInterNetActivity.class));
            overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
            finish();
        }

        setNameandmobile();

        listioners();
    }

    private void listioners() {


        txtCountryHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogCountry.showSpinerDialog();
            }
        });
        txtStateHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogState.showSpinerDialog();
            }
        });
        txtCityHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCity.showSpinerDialog();
            }
        });
        spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                txtCountryHint.setText(spinnerCountry.getSelectedItem().toString());
                txtCountryHint.setTextColor(position==0?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
                if(position != 0) {
                    showLoader = true;
                    if(cd.isConnectingToInternet()) {
                        getCountryStateList(countryBeanList.get(position).get_id());
                    }else
                    {
                        startActivity(new Intent(EditProfileActivity.this,NoInterNetActivity.class));
                        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                        finish();
                    }

                }
                else{
                    spinnerState.setSelection(0);
                    countryStateBeanList.clear();
                    countryStateList.clear();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                txtStateHint.setText(spinnerState.getSelectedItem().toString());
                txtStateHint.setTextColor(position==0?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
                if(position != 0 && countryStateBeanList.size()>0) {

                    if(cd.isConnectingToInternet()) {
                        getCityList(countryStateBeanList.get(position).get_id());
                    }else
                    {
                        startActivity(new Intent(EditProfileActivity.this,NoInterNetActivity.class));
                        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                        finish();
                    }
                }
                else {
                    spinnerCity.setSelection(0);
                    cityBeanList.clear();
                    cityList.clear();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                txtCityHint.setText(spinnerCity.getSelectedItem().toString());
                txtCityHint.setTextColor(position==0?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerStateEducation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position != 0)
                    getCollegeList(stateBeanList.get(position).get_id());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        etAddress1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                enableDisableSubmit();
            }
        });
        etFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                enableDisableSubmit();
            }
        });
        etLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                enableDisableSubmit();
            }
        });
        etAddress2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                enableDisableSubmit();
            }
        });
        etPincode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                enableDisableSubmit();
            }
        });

    }

    private void setNameandmobile() {
        txtMail.setText(sessionManager.getUserDetails().get(SessionManager.KEY_EMAIL));
        txtMobileNumber.setText(sessionManager.getUserDetails().get(SessionManager.KEY_MOBILE));
        etFirstName.setText(sessionManager.getUserDetails().get(SessionManager.KEY_FNAME));
        etLastName.setText(sessionManager.getUserDetails().get(SessionManager.KEY_LNAME));


    }

    private void getUserProfileData() {
        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(EditProfileActivity.this);
        HashMap<String,String> params = new HashMap<>();
        params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));

        mRequest.makeServiceRequest(IConstant.getUserProfileData, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------getUserProfileData Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1")) {

                        JSONObject respo = object.getJSONObject("respo");
                        JSONObject education = respo.getJSONObject("education");
                        JSONObject residence = respo.getJSONObject("residence");

                        etAddress1.setText(residence.getString("line_1"));
                        etAddress2.setText(residence.getString("line_2"));
                        etPincode.setText(residence.getString("pincode"));

                        strCountry = residence.getString("country");
                        strState = residence.getString("state");
                        strCity = residence.getString("city");
                        strCountryId = residence.getString("country_id");
                        strStateId = residence.getString("state_id");
                        strCityId = residence.getString("city_id");
//                        strEduState = education.getString("state_name");
//                        strCollege = education.getString("college_name");

                        txtCountryHint.setTextColor(getResources().getColor(R.color.black));
                        txtCountryHint.setText(strCountry);
                        txtStateHint.setTextColor(getResources().getColor(R.color.black));
                        txtStateHint.setText(strState);
                        txtCityHint.setTextColor(getResources().getColor(R.color.black));
                        txtCityHint.setText(strCity);
                        enableDisableSubmit();
                        loader.dismissLoader();
                        prepareSpinners();
                    } else if(sStatus.equals("00")){
                        loader.dismissLoader();
                        customAlert.singleLoginAlertLogout();
                    } else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    } else {
                         loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(EditProfileActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });

    }

    private void prepareSpinners() {
        countryList = new ArrayList<>();
        countryBeanList  = new ArrayList<>();
        countryStateList = new ArrayList<>();
        countryStateBeanList = new ArrayList<>();
        stateList = new ArrayList<>();
        stateBeanList = new ArrayList<StateBean>();
        cityList = new ArrayList<>();
        cityBeanList = new ArrayList<>();
        collegeList = new ArrayList<>();
        collegeBeanList = new ArrayList<CollegeBean>();
        dialogCountry = new SpinnerDialog(EditProfileActivity.this, countryList,
                "COUNTRY");
        dialogState = new SpinnerDialog(EditProfileActivity.this, countryStateList,
                "STATE");
        dialogCity = new SpinnerDialog(EditProfileActivity.this, cityList,
                "CITY");
        getCountryList();
        getCountryStateList(strCountryId);
        getCityList(strStateId);
        setListeners();
//        getStateList();

    }

    private void setListeners() {
        dialogCountry.setCancellable(true);
        dialogCountry.setShowKeyboard(false);

        dialogCountry.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {

                txtCountryHint.setText(item);

                txtCountryHint.setTextColor(position==0?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
                showLoader = true;
                if(position != 0) {
                    if (cd.isConnectingToInternet())
                        getCountryStateList(countryBeanList.get(position).get_id());
                    else {
                        startActivity(new Intent(EditProfileActivity.this,NoInterNetActivity.class));
                        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                        finish();
                    }

                    strCountryId = countryBeanList.get(position).get_id();
                }
                txtStateHint.setText("STATE");
                txtStateHint.setTextColor(getResources().getColor(R.color.hint_color));
                txtCityHint.setText("CITY");
                txtCityHint.setTextColor(getResources().getColor(R.color.hint_color));
                enableDisableSubmit();
            }
        });
        dialogState.setCancellable(true);
        dialogState.setShowKeyboard(false);

        dialogState.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {

                txtStateHint.setText(item);

                txtStateHint.setTextColor(position==0?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));
                showLoader = true;
                if(position != 0) {
                    strStateId = countryStateBeanList.get(position).get_id();
                    if (cd.isConnectingToInternet())
                        getCityList(countryStateBeanList.get(position).get_id());
                    else {
                        startActivity(new Intent(EditProfileActivity.this,NoInterNetActivity.class));
                        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                        finish();
                    }

                }
                txtCityHint.setText("CITY");
                txtCityHint.setTextColor(getResources().getColor(R.color.hint_color));
                enableDisableSubmit();
            }
        });
        dialogCity.setCancellable(true);
        dialogCity.setShowKeyboard(false);

        dialogCity.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                StatusBarColorChange.hideSoftKeyboard(EditProfileActivity.this);
                txtCityHint.setText(item);
                enableDisableSubmit();
                showLoader = true;
                if(position!=0)
                    strCityId = cityBeanList.get(position).get_id();
                txtCityHint.setTextColor(position==0?getResources().getColor(R.color.hint_color):getResources().getColor(R.color.black));

            }
        });
    }

    private void getCountryList()
    {
        if(showLoader)
            loader.showLoader();
        countryList.clear();
        countryBeanList.clear();
        ServiceRequest mRequest = new ServiceRequest(EditProfileActivity.this);
        HashMap<String,String> params = new HashMap<>();

        mRequest.makeServiceRequest(IConstant.getCountryList, Request.Method.POST, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------getCountryList Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1") && message.equalsIgnoreCase("Success")) {
                        countryList.add("COUNTRY");
                        StateBean stateBean1 = new StateBean();
                        stateBean1.set_id("-1");
                        stateBean1.setState_name("COUNTRY");
                        countryBeanList.add(stateBean1);
                        JSONArray stateListArray = object.getJSONArray("list");
                        if(stateListArray.length()>0)
                        {
                            for(int index =0; index<stateListArray.length();index++)
                            {
                                countryList.add(stateListArray.getJSONObject(index).getString("country_name"));
                                StateBean stateBean = new StateBean();
                                stateBean.set_id(stateListArray.getJSONObject(index).getString("_id"));
                                stateBean.setState_name(stateListArray.getJSONObject(index).getString("country_name"));
                                countryBeanList.add(stateBean);
                            }
                        }
                        //loader.dismissLoader();
//                        strCountryId = countryBeanList.get(countryList.indexOf(strCountry)).get_id();
//                        getCountryStateList(countryBeanList.get(countryList.indexOf(strCountry)).get_id());

                    }  else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    }else {

                        // loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(EditProfileActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }
    private void getCountryStateList(String id) {
        if(showLoader)
            loader.showLoader();
        countryStateList.clear();
        countryStateBeanList.clear();
        ServiceRequest mRequest = new ServiceRequest(EditProfileActivity.this);
        HashMap<String,String> params = new HashMap<>();
        params.put("country_id",id);

        mRequest.makeServiceRequest(IConstant.getCountrySateList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------getCountrySateList Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    loader.dismissLoader();
                    if (sStatus.equalsIgnoreCase("1") && message.equalsIgnoreCase("Success")) {

                        countryStateList.add("STATE");
                        StateBean stateBean1 = new StateBean();
                        stateBean1.set_id("-1");
                        stateBean1.setState_name("STATE");
                        countryStateBeanList.add(stateBean1);
                        JSONArray stateListArray = object.getJSONArray("list");
                        if(stateListArray.length()>0)
                        {
                            for(int index =0; index<stateListArray.length();index++)
                            {
                                countryStateList.add(stateListArray.getJSONObject(index).getString("state_name"));
                                StateBean stateBean = new StateBean();
                                stateBean.set_id(stateListArray.getJSONObject(index).getString("_id"));
                                stateBean.setState_name(stateListArray.getJSONObject(index).getString("state_name"));
                                countryStateBeanList.add(stateBean);
                            }
                        }
                        //loader.dismissLoader();
//                        ArrayAdapter stateAdapter = new ArrayAdapter(EditProfileActivity.this, R.layout.spinner_item,countryStateList);
//                        stateAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
//                        spinnerState.setAdapter(stateAdapter);
//
//                        if(!strState.equals(""))
//                        {
//                            spinnerState.setSelection(countryStateList.contains(strState)?countryStateList.indexOf(strState):0);
//                        }
//                        strStateId = countryStateBeanList.get(countryStateList.indexOf(strState)).get_id();
//                        getCityList(countryStateBeanList.get(countryStateList.indexOf(strState)).get_id());


                    }  else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    }else {

                        // loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(EditProfileActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    private void getCityList(String id) {

        cityBeanList.clear();
        cityList.clear();
        if(showLoader)
            loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(EditProfileActivity.this);
        HashMap<String,String> params = new HashMap<>();
        params.put("state_id",id);

        mRequest.makeServiceRequest(IConstant.getCityList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------getCityList Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    loader.dismissLoader();
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1") && message.equalsIgnoreCase("Success")) {

                        cityList.add("CITY");
                        StateBean stateBean1 = new StateBean();
                        stateBean1.set_id("-1");
                        stateBean1.setState_name("CITY");
                        cityBeanList.add(stateBean1);
                        JSONArray stateListArray = object.getJSONArray("list");
                        if(stateListArray.length()>0)
                        {
                            for(int index =0; index<stateListArray.length();index++)
                            {
                                cityList.add(stateListArray.getJSONObject(index).getString("city_name"));
                                StateBean stateBean = new StateBean();
                                stateBean.set_id(stateListArray.getJSONObject(index).getString("_id"));
                                stateBean.setState_name(stateListArray.getJSONObject(index).getString("city_name"));
                                cityBeanList.add(stateBean);
                            }
                        }
                        //loader.dismissLoader();
//                        ArrayAdapter stateAdapter = new ArrayAdapter(EditProfileActivity.this, R.layout.spinner_item,cityList);
//                        stateAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
//                        spinnerCity.setAdapter(stateAdapter);
//                        if(cityList.contains(strCity))
//                            spinnerCity.setSelection(cityList.indexOf(strCity));



                    }  else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    }else {

                        // loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
//                AlertBox.showSnackBoxPrimaryBa(EditProfileActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }
    private void getStateList()
    {
        //loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(EditProfileActivity.this);
        HashMap<String,String> params = new HashMap<>();

        mRequest.makeServiceRequest(IConstant.getStateList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------getStateList Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1") && message.equalsIgnoreCase("Success")) {
                        stateList.add("STATE");
                        StateBean stateBean1 = new StateBean();
                        stateBean1.set_id("-1");
                        stateBean1.setState_name("STATE");
                        stateBeanList.add(stateBean1);
                        JSONArray stateListArray = object.getJSONArray("list");
                        if(stateListArray.length()>0)
                        {
                            for(int index =0; index<stateListArray.length();index++)
                            {
                                stateList.add(stateListArray.getJSONObject(index).getString("state_name"));
                                StateBean stateBean = new StateBean();
                                stateBean.set_id(stateListArray.getJSONObject(index).getString("_id"));
                                stateBean.setState_name(stateListArray.getJSONObject(index).getString("state_name"));
                                stateBeanList.add(stateBean);
                            }
                        }
                        //loader.dismissLoader();
//                        ArrayAdapter stateAdapter = new ArrayAdapter(EditProfileActivity.this, R.layout.spinner_item,stateList);
//                        stateAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
//                        spinnerState.setAdapter(stateAdapter);
//                        if(strState != null && !strState.equals("") && stateList.contains(strState))
//                            spinnerState.setSelection(stateList.indexOf(strState));
//                        ArrayAdapter stateEduAdapter = new ArrayAdapter(EditProfileActivity.this, R.layout.spinner_item,stateList);
//                        stateEduAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
//                        spinnerStateEducation.setAdapter(stateEduAdapter);
//                        if(strEduState != null && !strEduState.equals("") && stateList.contains(strEduState))
//                            spinnerStateEducation.setSelection(stateList.indexOf(strEduState));
                    } else {

                        // loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    //loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                AlertBox.showSnackBoxPrimaryBa(EditProfileActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }
    private void getCollegeList(String id)
    {
        loader.showLoader();
        collegeBeanList.clear();
        collegeList.clear();
        ServiceRequest mRequest = new ServiceRequest(EditProfileActivity.this);
        HashMap<String,String> params = new HashMap<>();
        params.put("state_id",id);

        mRequest.makeServiceRequest(IConstant.getCollegeList, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------getCollegeList Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1") && message.equalsIgnoreCase("Success")) {
//                        collegeList.add("COLLEGE");
//                        CollegeBean collegeBean1 = new CollegeBean();
//                        collegeBean1.set_id("-1");
//                        collegeBean1.setCollege_name("COLLEGE");
////                                collegeBean.setShortName(stateListArray.getJSONObject(index).getString("short_name"));
////                                collegeBean.setLongName(stateListArray.getJSONObject(index).getString("long_name"));
//                        collegeBean1.setState_id("state_id");
//                        collegeBeanList.add(collegeBean1);
                        JSONArray stateListArray = object.getJSONArray("list");
                        if(stateListArray.length()>0)
                        {
                            for(int index =0; index<stateListArray.length();index++)
                            {
                                collegeList.add(stateListArray.getJSONObject(index).getString("college_name"));
                                CollegeBean collegeBean = new CollegeBean();
                                collegeBean.set_id(stateListArray.getJSONObject(index).getString("_id"));
                                collegeBean.setCollege_name(stateListArray.getJSONObject(index).getString("college_name"));
                                collegeBean.setShortName(stateListArray.getJSONObject(index).getString("short_name"));
                                collegeBean.setLongName(stateListArray.getJSONObject(index).getString("long_name"));
                                collegeBean.setState_id(stateListArray.getJSONObject(index).getString("state_id"));
                                collegeBeanList.add(collegeBean);
                            }
                        }
                        loader.dismissLoader();
//                        ArrayAdapter collegeAdapter = new ArrayAdapter(EditProfileActivity.this, R.layout.spinner_item,collegeList);
//                        collegeAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
//                        spinnerCollege.setAdapter(collegeAdapter);
//                        if(collegeList.contains(strCollege))
//                            spinnerCollege.setSelection(collegeList.indexOf(strCollege));


                    } else {

                        loader.dismissLoader();
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(EditProfileActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.txtResetCompleteProfile:
                resetFields();
                break;


            case R.id.rlCompleteProfileSubmit:
                if(etFirstName.getText().toString().isEmpty())
                    AlertBox.showSnackBoxPrimaryBa(EditProfileActivity.this,getString(R.string.alert_oops),getString(R.string.firstNameAlert));
                else if(etLastName.getText().toString().isEmpty())
                    AlertBox.showSnackBoxPrimaryBa(EditProfileActivity.this,getString(R.string.alert_oops),getString(R.string.lastNameAlert));
                else if(etAddress1.getText().toString().isEmpty())
                    AlertBox.showSnackBoxPrimaryBa(EditProfileActivity.this,getString(R.string.alert_oops),getString(R.string.address1Alert));
                else if(etAddress2.getText().toString().isEmpty())
                    AlertBox.showSnackBoxPrimaryBa(EditProfileActivity.this,getString(R.string.alert_oops),getString(R.string.address1Alert));
                else if(countryList.size() == 0 || txtCountryHint.getText().toString().equalsIgnoreCase("Country"))
                    AlertBox.showSnackBoxPrimaryBa(EditProfileActivity.this,getString(R.string.alert_oops),getString(R.string.countrySelectAlert));
                else if(countryStateList.size() == 0 || txtStateHint.getText().toString().equalsIgnoreCase("State"))
                    AlertBox.showSnackBoxPrimaryBa(EditProfileActivity.this,getString(R.string.alert_oops),getString(R.string.stateSelectAlert));
                else if(cityList.size() == 0 || txtCityHint.getText().toString().equalsIgnoreCase("City"))
                    AlertBox.showSnackBoxPrimaryBa(EditProfileActivity.this,getString(R.string.alert_oops),getString(R.string.citySelectAlert));
                else if(etPincode.getText().toString().isEmpty())
                    AlertBox.showSnackBoxPrimaryBa(EditProfileActivity.this,getString(R.string.alert_oops),getString(R.string.pinAlert));
                else if(etPincode.getText().toString().length()<4)
                    AlertBox.showSnackBoxPrimaryBa(EditProfileActivity.this,getString(R.string.alert_oops),getString(R.string.pinAlert));
                else {
                    if (cd.isConnectingToInternet())
                        completeProfile();
                    else {
                        startActivity(new Intent(EditProfileActivity.this, NoInterNetActivity.class));
                        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                        finish();
                    }

                }
                break;

            case R.id.rlBack:
                finish();
                break;
        }
    }
    private void completeProfile() {

        loader.showLoader();
        ServiceRequest mRequest = new ServiceRequest(EditProfileActivity.this);
        HashMap<String,String> params = new HashMap<>();
        params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("first_name",etFirstName.getText().toString());
        params.put("last_name",etLastName.getText().toString());
        params.put("line_1",etAddress1.getText().toString());
        params.put("line_2",etAddress2.getText().toString());
        params.put("city",txtCityHint.getText().toString());
        params.put("city_id",strCityId);
        params.put("state",txtStateHint.getText().toString());
        params.put("state_id",strStateId);
        params.put("country",txtCountryHint.getText().toString());
        params.put("country_id",strCountryId);
        params.put("pincode",etPincode.getText().toString());

        mRequest.makeServiceRequest(IConstant.updateProfileAddress, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------updateProfileAddress Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1")) {
                        loader.dismissLoader();
                        sessionManager.updateName(etFirstName.getText().toString(),etLastName.getText().toString());
                        customAlert.showAlertSuccessCompleteProfile(getString(R.string.alert_success),message,"editProfile");
                    }  else if(sStatus.equals("00")){
                        loader.dismissLoader();
                        customAlert.singleLoginAlertLogout();
                    } else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    loader.dismissLoader();
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {
                loader.dismissLoader();
                AlertBox.showSnackBoxPrimaryBa(EditProfileActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });


    }
    private void resetFields() {

        etFirstName.setText("");
        etLastName.setText("");
        etAddress1.setText("");
        etAddress2.setText("");
        etPincode.setText("");
        txtCityHint.setText("CITY");
        etFirstName.setHint(getString(R.string.FIRST_NAME));
        etLastName.setHint(getString(R.string.LAST_NAME));
        txtCityHint.setTextColor(getResources().getColor(R.color.hint_color));
        txtStateHint.setText("STATE");
        txtStateHint.setTextColor(getResources().getColor(R.color.hint_color));
        txtCountryHint.setText("COUNTRY");
        txtCountryHint.setTextColor(getResources().getColor(R.color.hint_color));

    }

    private void enableDisableSubmit()
    {
       if(!etFirstName.getText().toString().isEmpty()&&!etLastName.getText().toString().isEmpty()&&!etAddress1.getText().toString().isEmpty()&&!etAddress2.getText().toString().isEmpty()&&!etPincode.getText().toString().isEmpty() && etPincode.getText().toString().length()>3 && !txtCountryHint.getText().toString().equalsIgnoreCase("Country") && !txtStateHint.getText().toString().equalsIgnoreCase("State") && !txtCityHint.getText().toString().equalsIgnoreCase("City"))
       {
           rlCompleteProfileSubmit.setBackground(getResources().getDrawable(R.drawable.login_gradient));
       }else
       {
           rlCompleteProfileSubmit.setBackground(getResources().getDrawable(R.drawable.in_active_round_button));
       }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

}
