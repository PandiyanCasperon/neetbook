package com.app.neetbook.View.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.app.neetbook.Adapter.MyViewPagerAdapter;
import com.app.neetbook.Interfaces.OnFragmentInteractionListener;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Data.HelpData;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.alerter.AlertBox;
import com.app.neetbook.Utils.widgets.DisplayOrientation;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class HelpFragment extends Fragment {
    private ViewPager viewPager;
    private HelpPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    TextView txtArticleIndicator,txtPointsIndicator,txtMCQIndicator,txtTestIndicator;
    ConnectionDetector cd;
    SessionManager sessionManager;
    CustomAlert customAlert;
    public HelpFragment() {
        // Required empty public constructor
    }


    public static HelpFragment newInstance(String param1, String param2) {
        HelpFragment fragment = new HelpFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
        cd = new ConnectionDetector(getActivity());
        sessionManager = new SessionManager(getActivity());
        customAlert = new CustomAlert(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_help, container, false);
        init(rootView);
        return rootView;
    }

    private void init(View rootView) {
        viewPager = (ViewPager) rootView.findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) rootView.findViewById(R.id.layoutDots);
        txtPointsIndicator =  rootView.findViewById(R.id.txtPointsIndicator);
        txtMCQIndicator =  rootView.findViewById(R.id.txtMCQIndicator);
        txtArticleIndicator =  rootView.findViewById(R.id.txtArticleIndicator);
        txtTestIndicator =  rootView.findViewById(R.id.txtTestIndicator);
        getUserProfileData();

        if(getResources().getBoolean(R.bool.isTablet))
        {
            if(DisplayOrientation.getDisplayOrientation(getActivity()) == 1 || DisplayOrientation.getDisplayOrientation(getActivity()) == 3) {
                layouts = new int[]{
                        R.layout.help_slide_1_land,
                        R.layout.help_slide_3_land,
                        R.layout.help_slide_4_land,
                        R.layout.help_slide_2_land
                        };
            }else
            {
                layouts = new int[]{
                        R.layout.help_slide_1,
                        R.layout.help_slide_3,
                        R.layout.help_slide_4,
                        R.layout.help_slide_2
                        };
            }
        }else {
            layouts = new int[]{
                    R.layout.help_slide_1,
                    R.layout.help_slide_3,
                    R.layout.help_slide_4,
                    R.layout.help_slide_2};
        }
        // adding bottom dots
        addBottomDots(HelpData.currentPosition);

        myViewPagerAdapter = new HelpPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        if(getActivity().getResources().getBoolean(R.bool.isTablet))
            viewPager.setCurrentItem(HelpData.currentPosition);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
    }
    private void addBottomDots(int currentPage) {
        if(currentPage == 0) {
            setArticalIndicator();

        }else if(currentPage == 1) {
            setPopintsIndicator();
        }else if(currentPage == 2) {
            setMcqIdicator();
        }else if(currentPage == 3) {
            setTestIndicator();

        }

//        dots = new TextView[layouts.length];
//
//        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
//        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);
//
//        dotsLayout.removeAllViews();
//        for (int i = 0; i < dots.length; i++) {
//            dots[i] = new TextView(getActivity());
//            dots[i].setText(Html.fromHtml("&#8226;"));
//            dots[i].setTextSize(35);
//            dots[i].setTextColor(colorsInactive[currentPage]);
//            dotsLayout.addView(dots[i]);
//        }
//
//        if (dots.length > 0)
//            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }
    private void setArticalIndicator() {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) txtArticleIndicator.getLayoutParams();
        LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) txtPointsIndicator.getLayoutParams();
        LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) txtMCQIndicator.getLayoutParams();
        LinearLayout.LayoutParams params3 = (LinearLayout.LayoutParams) txtTestIndicator.getLayoutParams();
        params.height = getResources().getDimensionPixelSize(R.dimen._8sdp);
        params.width = getResources().getDimensionPixelSize(R.dimen._8sdp);

        params1.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params1.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        params2.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params2.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        params3.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params3.width = getResources().getDimensionPixelSize(R.dimen._6sdp);


        txtArticleIndicator.setBackground(getResources().getDrawable(R.drawable.artical_indicator_dark));
        txtPointsIndicator.setBackground(getResources().getDrawable(R.drawable.points_indicator));
        txtMCQIndicator.setBackground(getResources().getDrawable(R.drawable.mcq_indicator));
        txtTestIndicator.setBackground(getResources().getDrawable(R.drawable.test_indicator));

        txtArticleIndicator.setLayoutParams(params);
        txtPointsIndicator.setLayoutParams(params1);
        txtMCQIndicator.setLayoutParams(params2);
        txtTestIndicator.setLayoutParams(params3);
    }

    private void setPopintsIndicator() {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) txtArticleIndicator.getLayoutParams();
        LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) txtPointsIndicator.getLayoutParams();
        LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) txtMCQIndicator.getLayoutParams();
        LinearLayout.LayoutParams params3 = (LinearLayout.LayoutParams) txtTestIndicator.getLayoutParams();
        params.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        params1.height = getResources().getDimensionPixelSize(R.dimen._8sdp);
        params1.width = getResources().getDimensionPixelSize(R.dimen._8sdp);

        params2.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params2.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        params3.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params3.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        txtArticleIndicator.setBackground(getResources().getDrawable(R.drawable.artical_indicator));
        txtPointsIndicator.setBackground(getResources().getDrawable(R.drawable.points_indicator_dark));
        txtMCQIndicator.setBackground(getResources().getDrawable(R.drawable.mcq_indicator));
        txtTestIndicator.setBackground(getResources().getDrawable(R.drawable.test_indicator));

        txtArticleIndicator.setLayoutParams(params);
        txtPointsIndicator.setLayoutParams(params1);
        txtMCQIndicator.setLayoutParams(params2);
        txtTestIndicator.setLayoutParams(params3);
    }

    private void setMcqIdicator() {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) txtArticleIndicator.getLayoutParams();
        LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) txtPointsIndicator.getLayoutParams();
        LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) txtMCQIndicator.getLayoutParams();
        LinearLayout.LayoutParams params3 = (LinearLayout.LayoutParams) txtTestIndicator.getLayoutParams();
        params.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        params1.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params1.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        params2.height = getResources().getDimensionPixelSize(R.dimen._8sdp);
        params2.width = getResources().getDimensionPixelSize(R.dimen._8sdp);

        params3.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params3.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        txtArticleIndicator.setBackground(getResources().getDrawable(R.drawable.artical_indicator));
        txtPointsIndicator.setBackground(getResources().getDrawable(R.drawable.points_indicator));
        txtMCQIndicator.setBackground(getResources().getDrawable(R.drawable.mcq_indicator_dark));
        txtTestIndicator.setBackground(getResources().getDrawable(R.drawable.test_indicator));

        txtArticleIndicator.setLayoutParams(params);
        txtPointsIndicator.setLayoutParams(params1);
        txtMCQIndicator.setLayoutParams(params2);
        txtTestIndicator.setLayoutParams(params3);
    }

    private void setTestIndicator() {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) txtArticleIndicator.getLayoutParams();
        LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) txtPointsIndicator.getLayoutParams();
        LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) txtMCQIndicator.getLayoutParams();
        LinearLayout.LayoutParams params3 = (LinearLayout.LayoutParams) txtTestIndicator.getLayoutParams();
        params.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        params1.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params1.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        params2.height = getResources().getDimensionPixelSize(R.dimen._6sdp);
        params2.width = getResources().getDimensionPixelSize(R.dimen._6sdp);

        params3.height = getResources().getDimensionPixelSize(R.dimen._8sdp);
        params3.width = getResources().getDimensionPixelSize(R.dimen._8sdp);

        txtArticleIndicator.setBackground(getResources().getDrawable(R.drawable.artical_indicator));
        txtPointsIndicator.setBackground(getResources().getDrawable(R.drawable.points_indicator));
        txtMCQIndicator.setBackground(getResources().getDrawable(R.drawable.mcq_indicator));
        txtTestIndicator.setBackground(getResources().getDrawable(R.drawable.test_indicator_dark));
        txtArticleIndicator.setLayoutParams(params);
        txtPointsIndicator.setLayoutParams(params1);
        txtMCQIndicator.setLayoutParams(params2);
        txtTestIndicator.setLayoutParams(params3);
    }
    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }
    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
            if(getActivity().getResources().getBoolean(R.bool.isTablet))
                HelpData.currentPosition = position;
            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == layouts.length - 1) {
                // last page. make button text to GOT IT

            } else {
                // still pages are left

            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };
    public class HelpPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;


        public HelpPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);

            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
    private void getUserProfileData() {

        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String,String> params = new HashMap<>();
        params.put("user_id",sessionManager.getUserDetails().get(SessionManager.KEY_ID));

        mRequest.makeServiceRequest(IConstant.getUserProfileData, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------getUserProfileData Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    message = object.getString("message");
                    if (sStatus.equalsIgnoreCase("1")) {


                    }  else if(sStatus.equals("00")){
                        customAlert.singleLoginAlertLogout();
                    } else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    }else {
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onErrorListener(String errorMessage) {

                try {
                    AlertBox.showSnackBoxPrimaryBa(getActivity(), getString(R.string.alert_oops), errorMessage);
                }catch ( Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

    }
}
