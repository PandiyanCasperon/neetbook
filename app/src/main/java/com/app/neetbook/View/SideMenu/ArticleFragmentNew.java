package com.app.neetbook.View.SideMenu;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.app.neetbook.Adapter.sideMenuContent.SubscriptionCategoryArticlesAdapter;
import com.app.neetbook.Adapter.subscription.SubscriptionCategoryArticlesAdapterNew;
import com.app.neetbook.Model.HeadData;
import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.SuperDetails;
import com.app.neetbook.Model.sidemenuFromContent.CategoryArticle;
import com.app.neetbook.R;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.ItemOffsetDecoration;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.serviceRequest.IConstant;
import com.app.neetbook.serviceRequest.ServiceRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class ArticleFragmentNew extends Fragment {
    private ArrayList<CategoryArticle> articleList = new ArrayList<>();
    private SubscriptionCategoryArticlesAdapterNew categoryAdapter;
    private RecyclerView mRecyclerView;
    private Context context;
    Loader loader;
    SessionManager sessionManager;
    private CustomAlert customAlert;
    private int page = 1;
    private int totalSubsCount = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getActivity());
        customAlert = new CustomAlert(getActivity());
        loader = new Loader(getActivity());
        setupStatusBarColor();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_article, container, false);
        init(root);
        return root;
    }

    private void init(View rootView) {
        mRecyclerView = rootView.findViewById(R.id.recyclerViewArticle);
        populateList();
    }

    private void populateList() {

//    for (int i = 1; i < 15; i++) {
//      listItem.title = "Magnetic Resonance Imaging in Orthopedics";
//      articleList.add(listItem);
//    }

        ServiceRequest mRequest = new ServiceRequest(getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", sessionManager.getUserDetails().get(SessionManager.KEY_ID));
        params.put("subject_id","");
        params.put("page",""+page);
        params.put("skip","0");
        params.put("limit","10");
        mRequest.makeServiceRequest(IConstant.articleIndex, Request.Method.POST, params, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------articleIndex Response----------------" + response);
                String sStatus = "";
                String message = "";
                try {

                    JSONObject obj = new JSONObject(response);
                    sStatus = obj.getString("status");
                    message = obj.has("message")?obj.getString("message"):"";


                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject object = obj.getJSONObject("response");
                        totalSubsCount = object.getInt("ctotal");
                        JSONArray jsonArray = object.getJSONArray("chapters");
                        if(jsonArray.length()>0) {
                            //llEmpty.setVisibility(View.GONE);
                            for(int i=0;i<jsonArray.length();i++)
                            {
                                JSONObject chaptersObject = jsonArray.getJSONObject(i);
                                CategoryArticle listItem = new CategoryArticle();
                                listItem._id = chaptersObject.getString("_id");
                                listItem.chapt_name = chaptersObject.getString("chapt_name");
                                listItem.short_name = chaptersObject.getString("short_name");
                                listItem.long_name = chaptersObject.getString("long_name");

                                ArrayList<HeadDetails> headDetailsArrayList = new ArrayList<HeadDetails>();
                                JSONArray headDetailsArray = chaptersObject.getJSONArray("headDetails");

                                if(headDetailsArray.length()>0)
                                {
                                    for(int j=0;j<headDetailsArray.length();j++)
                                    {
                                        HeadDetails headDetails = new HeadDetails();
                                        JSONObject headDetailsObject = headDetailsArray.getJSONObject(j);
                                        headDetails.isSH = headDetailsObject.getString("isSH");
                                        SuperDetails superDetails = new SuperDetails();
                                        if(headDetailsObject.getString("isSH").equals("1"))
                                        {
                                            JSONObject superDetailsObject = headDetailsObject.getJSONObject("superDetails");
                                            superDetails._id = superDetailsObject.getString("_id");
                                            superDetails.chapter = superDetailsObject.getString("chapter");
                                            superDetails.supr_head_name = superDetailsObject.getString("supr_head_name");
                                            superDetails.short_name = superDetailsObject.getString("short_name");
                                            superDetails.long_name = superDetailsObject.getString("long_name");
                                            superDetails.firsthead = superDetailsObject.getString("firsthead");
                                            superDetails.head = superDetailsObject.getString("head");

                                        }
                                        headDetails.superDetails = superDetails;
                                        JSONArray headDataArray = headDetailsObject.getJSONArray("headData");
                                        ArrayList<HeadData> headDataArrayList = new ArrayList<HeadData>();
                                        if(headDataArray.length()>0) {
                                            for (int k = 0; k < headDataArray.length(); k++) {
                                                HeadData headData = new HeadData();
                                                JSONObject headDataObject = headDataArray.getJSONObject(k);
                                                headData._id = headDataObject.getString("_id");
                                                headData.head_name = headDataObject.getString("head_name");
                                                headData.long_name = headDataObject.getString("long_name");
                                                headData.short_name = headDataObject.getString("short_name");
                                                headData.chapter_id = headDataObject.getString("chapter_id");
                                                headData.position = headDataObject.getString("position");
                                                headData.isSH = headDataObject.getString("isSH");
                                                headData.firsthead = headDataObject.getString("firsthead");
                                                headDataArrayList.add(headData);
                                            }
                                        }
                                        headDetails.headData = headDataArrayList;
                                        headDetailsArrayList.add(headDetails);
                                    }
                                }
                                listItem.headDetailsArrayList = headDetailsArrayList;
                                articleList.add(listItem);
                            }
                            setupRecyclerView();
                        }
//            else{
//              llEmpty.setVisibility(View.VISIBLE);}
//            mShimmerViewContainer.stopShimmerAnimation();
//            mShimmerViewContainer.setVisibility(View.GONE);
                    } else if(sStatus.equals("00")){
                        customAlert.singleLoginAlertLogout();
                    }else if(sStatus.equalsIgnoreCase("01")){
                        customAlert.UserInActiveAlertLogout(getString(R.string.alert_oops),message);
                    } else {
                        customAlert.showAlertOk(getString(R.string.alert_oops),message);
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener(String errorMessage) {
                // AlertBox.showSnackBox(SplashActivity.this,getString(R.string.alert_oops),errorMessage);
            }
        });
        //setupRecyclerView();
    }

    private void setupRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                RecyclerView.VERTICAL, false));
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(context, R.dimen._4sdp);
        mRecyclerView.addItemDecoration(itemDecoration);
        categoryAdapter = new SubscriptionCategoryArticlesAdapterNew(context, articleList);
        mRecyclerView.setAdapter(categoryAdapter);
    }

    private void setupStatusBarColor() {
        ((SubscriptionCategoryActivity) context).updateStatusBarColor(context.getResources()
                .getColor(R.color.category_article_header_maroon));
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
