package com.app.neetbook.View.SideMenu;


import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.Fade;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;
import androidx.viewpager.widget.ViewPager;

import com.app.neetbook.Adapter.ArticleTabsAdapter;
import com.app.neetbook.Interfaces.OnMobileHomeMenuSelectedListioner;
import com.app.neetbook.Model.HeadData;
import com.app.neetbook.Model.HeadDetails;
import com.app.neetbook.Model.McqExam;
import com.app.neetbook.R;
import com.app.neetbook.Utils.ActionBarActivityNeetBook;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.Loader;
import com.app.neetbook.Utils.OnSwipeTouchListener;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.StatusBarColorChange;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class SubscriptionDetailActivity extends ActionBarActivityNeetBook implements View.OnClickListener {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Context context;
    Loader loader;
    SessionManager sessionManager;
    private CustomAlert customAlert;
    private int page = 1;
    int loadMoreEnable = 0;
    private int totalSubsCount;
    String strSubjectId = "", strSubjectName = "", strChaptortName = "", strHeadId = "", strChapterId = "", loadingType = "";
    ViewPagerAdapter viewPagerAdapter;
    private ImageView imageView2, imageView3;
    private TextView textView, textView3, txtSurgical, textView3Pin, textView2Pin;
    public static ArrayList<HeadDetails> mcqHeadList;
    public static ArrayList<McqExam> mcqExamArrayList;
    private ArrayList<HeadData> headDataList;
    private int currentSelectedPosition = 0;
    ArticleTabsAdapter homeGroupNameAdapter;
    LinearLayoutManager layoutManager;
    LinearSnapHelper snapHelper;
    RecyclerView recyclerViewGroups;
    Typeface tfBold, tfMedium, tfRegular;
    private RelativeLayout rlParent;

    ImageView imgClose;
    Typeface tf;
    AppBarLayout appBarLayout;
    CollapsingToolbarLayout collapsing_toolbar_mcq;
    Toolbar toolbar;
    Display display;
    int height1, width1;

    private RelativeLayout main_history_title_layout;
    private TextView article_title_tv;
    private LinearLayout top_reclerview_layout;
    private RelativeLayout content_title_layout;
    private TextView content_title_tv, superHeading;
    private String strHeadName = "";
    private String is_coming_page = "";

    public SubscriptionDetailActivity() {
        // Required empty public constructor
    }

    //
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_detail);
//    getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
//            WindowManager.LayoutParams.FLAG_SECURE);
        context = getApplicationContext();
        StatusBarColorChange.updateStatusBarColor(SubscriptionDetailActivity.this, getResources().getColor(R.color.test_completed_blue));
        init();


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        final Configuration override = new Configuration(newBase.getResources().getConfiguration());
        override.fontScale = 1.0f;
        applyOverrideConfiguration(override);
    }

    private void init() {
        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width1 = size.x;
        height1 = size.y;
        sessionManager = new SessionManager(SubscriptionDetailActivity.this);
        tfBold = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaBold.otf");
        tfMedium = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaSemibold.otf");
        tfRegular = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaRegular.otf");

        viewPager = findViewById(R.id.viewpager_subscription_category);
        tabLayout = findViewById(R.id.tabs_subscription_category);
        recyclerViewGroups = findViewById(R.id.recyclerViewGroups);
        viewPager = findViewById(R.id.viewpager_subscription_category);
        tabLayout = findViewById(R.id.tabs_subscription_category);
        imageView2 = findViewById(R.id.imageView2);
        imageView3 = findViewById(R.id.imageView3);
        textView = findViewById(R.id.textView);
        txtSurgical = findViewById(R.id.textView2);
        textView3 = findViewById(R.id.textView3);
        textView2Pin = findViewById(R.id.textView2Pin);
        textView3Pin = findViewById(R.id.textView3Pin);
        rlParent = findViewById(R.id.rlParent);
        toolbar = findViewById(R.id.toolbar);
        collapsing_toolbar_mcq =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_mcq);
        headDataList = new ArrayList<>();


        main_history_title_layout = findViewById(R.id.main_history_title_layout);
        article_title_tv = findViewById(R.id.article_title_tv);
        top_reclerview_layout = findViewById(R.id.top_reclerview_layout);
        content_title_layout = findViewById(R.id.content_title_layout);
        content_title_tv = findViewById(R.id.content_title_tv);
        superHeading = findViewById(R.id.superHeadingTxt);


        if (getIntent().getExtras() != null) {
            strHeadId = getIntent().getStringExtra("head_id");
            strSubjectId = getIntent().getStringExtra("subject_id");
            strSubjectName = getIntent().getStringExtra("subject_name");
            strChaptortName = getIntent().getStringExtra("chapter_name");
            textView.setText(strSubjectName);
            txtSurgical.setText(strChaptortName);
            textView2Pin.setText(strChaptortName);
            loadingType = getIntent().getStringExtra("loading_type");

            if (getIntent().getExtras().containsKey("coming_page")) {

                is_coming_page = getIntent().getStringExtra("coming_page");
            }


            if (!getIntent().getStringExtra("loading_type").equals("1")) {
                strChapterId = getIntent().getStringExtra("chapter_id");
            }
            if (sessionManager.getViewToBookMark() != null && !sessionManager.getViewToBookMark().isEmpty() && sessionManager.getViewToBookMark().equalsIgnoreCase("Yes")) {
                if (sessionManager.getBookmarkDetails().get(SessionManager.LOADING_TYPE).equals("1")) {
                    mcqHeadList =
                            new Gson().fromJson(sessionManager.getBookmarkDetails().get(SessionManager.HEAD_LIST), new TypeToken<List<HeadDetails>>() {
                            }.getType());
                } else {
                    mcqExamArrayList =
                            new Gson().fromJson(sessionManager.getBookmarkDetails().get(SessionManager.HEAD_LIST), new TypeToken<List<McqExam>>() {
                            }.getType());
                }
            }
        }
        createSnap();
        setupViewPager(viewPager);
        listener();
        setFontSize();
    }

    private void setFontSize() {

    /*if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
      textView.setTextAppearance(SubscriptionDetailActivity.this,R.style.textViewSmallSubjectName);
      txtSurgical.setTextAppearance(SubscriptionDetailActivity.this,R.style.textViewSmallChaptName);
      textView3.setTextAppearance(SubscriptionDetailActivity.this,R.style.textViewSmallChaptName);
      textView2Pin.setTextAppearance(SubscriptionDetailActivity.this,R.style.textViewSmallChaptName);
      textView3Pin.setTextAppearance(SubscriptionDetailActivity.this,R.style.textViewSmallChaptName);
    }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {*/
        textView.setTextAppearance(SubscriptionDetailActivity.this, R.style.textViewMediumSubjectName);
        txtSurgical.setTextAppearance(SubscriptionDetailActivity.this, R.style.textViewMediumChaptName);
        textView3.setTextAppearance(SubscriptionDetailActivity.this, R.style.textViewMediumChaptName);
        textView2Pin.setTextAppearance(SubscriptionDetailActivity.this, R.style.textViewMediumChaptName);
        textView3Pin.setTextAppearance(SubscriptionDetailActivity.this, R.style.textViewMediumChaptName);
   /* }else if(sessionManager.getFontSize()!= null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
      textView.setTextAppearance(SubscriptionDetailActivity.this,R.style.textViewLargeSubjectName);
      txtSurgical.setTextAppearance(SubscriptionDetailActivity.this,R.style.textViewLargeChaptName);
      textView3.setTextAppearance(SubscriptionDetailActivity.this,R.style.textViewLargeChaptName);
      textView2Pin.setTextAppearance(SubscriptionDetailActivity.this,R.style.textViewLargeChaptName);
      textView3Pin.setTextAppearance(SubscriptionDetailActivity.this,R.style.textViewLargeChaptName);
    }*/

        textView.setTypeface(tfMedium);
        txtSurgical.setTypeface(tfMedium);
        textView3.setTypeface(tfMedium);
        textView2Pin.setTypeface(tfMedium);
        textView3Pin.setTypeface(tfMedium);
    }

    private void setCollapseingToolBar() {

        article_title_tv.setText(strHeadName);

        imgClose = findViewById(R.id.imgClose);
        appBarLayout = findViewById(R.id.appBarLayout);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (is_coming_page.equals("homepage")) {

                    Intent s = new Intent(getApplicationContext(), TabMainActivity.class);
                    s.putExtra("subjectId", strSubjectId);
                    s.putExtra("subjectName", sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_NAME));
                    s.putExtra("isTrial", sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_TRIAL_STATUS));
                    s.putExtra("subjectShortName", sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_SHORT_NAME));
                    s.putExtra("showing_page_position", "2");
                    startActivity(s);
                    finish();
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                } else {

                    finish();
                }
            }
        });
        if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("small")) {
            collapsing_toolbar_mcq.setExpandedTitleTextAppearance(R.style.expandedappbarSmall);
            collapsing_toolbar_mcq.setCollapsedTitleTextAppearance(R.style.collapsedappbarSmall);
            content_title_tv.setTextAppearance(context, R.style.expandedappbarSmall);
            article_title_tv.setTextAppearance(context, R.style.collapsedappbarMedium);
        } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("medium")) {
            collapsing_toolbar_mcq.setExpandedTitleTextAppearance(R.style.expandedappbarMedium);
            collapsing_toolbar_mcq.setCollapsedTitleTextAppearance(R.style.collapsedappbarMedium);
            content_title_tv.setTextAppearance(context, R.style.collapsedappbarMedium);
            article_title_tv.setTextAppearance(context, R.style.collapsedappbarMedium);
        } else if (sessionManager.getFontSize() != null && sessionManager.getFontSize().equalsIgnoreCase("large")) {
            collapsing_toolbar_mcq.setExpandedTitleTextAppearance(R.style.expandedappbarLarge);
            collapsing_toolbar_mcq.setCollapsedTitleTextAppearance(R.style.collapsedappbarLarge);
            content_title_tv.setTextAppearance(context, R.style.collapsedappbarLarge);
            article_title_tv.setTextAppearance(context, R.style.collapsedappbarLarge);
        }
        tf = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaBold.otf");
        collapsing_toolbar_mcq.setCollapsedTitleTypeface(tf);
        collapsing_toolbar_mcq.setExpandedTitleTypeface(tf);
        content_title_tv.setTypeface(tf);
        article_title_tv.setTypeface(tf);


        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {


                if (verticalOffset < -30) {

                    TitleVisibleGoneAnimation(false);
                }


                if (verticalOffset < -100) {

                    if (top_reclerview_layout.getVisibility() == View.VISIBLE) {

                        VisibleGoneAnimation(false);

                    }

                    //imageViewArticleSummary.setVisibility(View.VISIBLE);
                    textView2Pin.setVisibility(View.GONE);
                    textView3Pin.setVisibility(View.VISIBLE);

                    content_title_tv.setTextColor(ContextCompat.getColor(context, R.color.subsDetail_blue));

                } else {

                    if (top_reclerview_layout.getVisibility() == View.INVISIBLE || top_reclerview_layout.getVisibility() == View.GONE) {

                        VisibleGoneAnimation(true);

                    }

                    TitleVisibleGoneAnimation(true);

                    textView2Pin.setVisibility(View.GONE);
                    textView3Pin.setVisibility(View.GONE);

                    content_title_tv.setTextColor(ContextCompat.getColor(context, R.color.subsDetail_blue));
                }




                /*    if(verticalOffset<-190) {
                 *//* collapsingToolbarLayout.setTitle(strHeadName);*//*
          CollapsingToolbarLayout.LayoutParams layoutParams = (CollapsingToolbarLayout.LayoutParams) toolbar.getLayoutParams();
          layoutParams.width = textView3Pin.getText().toString().isEmpty()?(int) (width1*(0.58)):(int) (width1*(0.3));
          toolbar.setLayoutParams(layoutParams);
          recyclerViewGroups.setVisibility(View.GONE);
          collapsing_toolbar_mcq.setCollapsedTitleTextColor(getResources().getColor(R.color.white));
        }
        else {
          *//*collapsingToolbarLayout.setTitle("");*//*
          CollapsingToolbarLayout.LayoutParams layoutParams = (CollapsingToolbarLayout.LayoutParams) toolbar.getLayoutParams();
          layoutParams.width = (int) (width1*(0.9));
          toolbar.setLayoutParams(layoutParams);
          recyclerViewGroups.setVisibility(View.VISIBLE);
          collapsing_toolbar_mcq.setExpandedTitleColor(getResources().getColor(R.color.mcqPageTitle));


        }*/
            }
        });
    }


    private void VisibleGoneAnimation(Boolean show) {

        try {

            Transition transition = new Fade();
            transition.setDuration(400);
            transition.addTarget(R.id.top_reclerview_layout);
            TransitionManager.beginDelayedTransition(top_reclerview_layout, transition);
            top_reclerview_layout.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
            // recyclerViewGroups.setVisibility(show ? View.VISIBLE : View.INVISIBLE);

            if (top_reclerview_layout.getVisibility() == View.INVISIBLE
                    || top_reclerview_layout.getVisibility() == View.GONE) {

                collapse(top_reclerview_layout);

                if (main_history_title_layout.getVisibility() == View.VISIBLE) {

                    MainTitleSetAnimation(false);
                }


            } else if (top_reclerview_layout.getVisibility() == View.VISIBLE) {

                expand(top_reclerview_layout);


                if (main_history_title_layout.getVisibility() == View.GONE) {

                    MainTitleSetAnimation(true);
                }


            }

        } catch (Exception e) {

        }
    }


    private void TitleVisibleGoneAnimation(Boolean show) {

        try {

            Transition transition = new Fade();
            transition.setDuration(300);
            transition.addTarget(R.id.content_title_layout);
            TransitionManager.beginDelayedTransition(content_title_layout, transition);
            content_title_layout.setVisibility(show ? View.VISIBLE : View.INVISIBLE);

        } catch (Exception e) {

        }
    }


    private void MainTitleSetAnimation(Boolean show) {

        try {

            Transition transition = new Fade();
            transition.setDuration(500);
            transition.addTarget(R.id.main_history_title_layout);
            TransitionManager.beginDelayedTransition(main_history_title_layout, transition);
            main_history_title_layout.setVisibility(show ? View.VISIBLE : View.GONE);

        } catch (Exception e) {

            Log.e("Exception", e.toString());
        }
    }


    private void listener() {

        imageView3.setOnClickListener(this);
        imageView2.setOnClickListener(this);

        rlParent.setOnTouchListener(new OnSwipeTouchListener(SubscriptionDetailActivity.this) {
            public void onSwipeTop() {
                //Toast.makeText(SubscriptionArticleActivity.this, "top", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeRight() {
                //Toast.makeText(SubscriptionArticleActivity.this, "right", Toast.LENGTH_SHORT).show();
                finish();
            }

            public void onSwipeLeft() {

            }

            public void onSwipeBottom() {
                //Toast.makeText(SubscriptionArticleActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageView2:
                finish();
                break;

            case R.id.imageView3:
                /* finish();*/
                break;
        }
    }

    private void setupViewPager(final ViewPager viewPager) {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this);

        if (loadingType.equals("1")) {
            if (mcqHeadList.size() > 0) {
                for (int i = 0; i < mcqHeadList.size(); i++) {
                    if (mcqHeadList.get(i).isSH.equals("Yes")) {

                        HeadData data = new HeadData();
                        data._id = mcqHeadList.get(i).headData.get(0)._id;
                        data.short_name = mcqHeadList.get(i).headData.get(0).short_name;
                        data.long_name = mcqHeadList.get(i).headData.get(0).long_name;
                        data.superHeadingName = mcqHeadList.get(i).isSH.equals("1") ? mcqHeadList.get(i).superDetails.short_name : "";
                        headDataList.add(data);
                        viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(strSubjectId, strChapterId, mcqHeadList.get(i).headData.get(0)._id, mcqHeadList.get(i).headData.get(0).long_name, loadingType), mcqHeadList.get(i).headData.get(0).short_name);


                    } else {
                        if (mcqHeadList.get(i).headData.size() > 0) {
                            for (int j = 0; j < mcqHeadList.get(i).headData.size(); j++) {

                                HeadData data = new HeadData();
                                data._id = mcqHeadList.get(i).headData.get(j)._id;
                                data.short_name = mcqHeadList.get(i).headData.get(j).short_name;
                                data.long_name = mcqHeadList.get(i).headData.get(j).long_name;
                                data.superHeadingName = mcqHeadList.get(i).isSH.equals("1") ? mcqHeadList.get(i).superDetails.short_name : "";
                                headDataList.add(data);
                                viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(strSubjectId, strChapterId, mcqHeadList.get(i).headData.get(j)._id, mcqHeadList.get(i).headData.get(j).long_name, loadingType), mcqHeadList.get(i).headData.get(j).short_name);
                            }
                        }
                    }
                }
            }
        } else if (loadingType.equals("2") || loadingType.equals("3")) {
            if (mcqExamArrayList.size() > 0) {
                for (int i = 0; i < mcqExamArrayList.size(); i++) {
                    HeadData data = new HeadData();
                    data._id = mcqExamArrayList.get(i).get_id();
                    data.short_name = mcqExamArrayList.get(i).getShort_name();
                    data.long_name = mcqExamArrayList.get(i).getLong_name();
                    data.superHeadingName = "";
                    headDataList.add(data);
                    viewPagerAdapter.addFragment(new SubscrptionDetailListFragment(strSubjectId, strChapterId, mcqExamArrayList.get(i).get_id(), mcqExamArrayList.get(i).getLong_name(), loadingType), mcqExamArrayList.get(i).getLong_name());

                }

            }
        }


        if (headDataList.size() > 0) {
            for (int i = 0; i < headDataList.size(); i++) {
                if (strHeadId.equalsIgnoreCase(headDataList.get(i)._id)) {
                    currentSelectedPosition = i;
                    textView3.setText(headDataList.get(i).superHeadingName.equals("") ? "" : headDataList.get(i).superHeadingName);
                    textView3Pin.setText(headDataList.get(i).superHeadingName.equals("") ? "" : headDataList.get(i).superHeadingName);
                    textView3.setVisibility(!headDataList.get(i).superHeadingName.equals("") ? View.VISIBLE : View.GONE);
                    textView3Pin.setVisibility(!headDataList.get(i).superHeadingName.equals("") ? View.VISIBLE : View.GONE);
                    collapsing_toolbar_mcq.setTitle("");
                    content_title_tv.setText(headDataList.get(i).long_name);
                    strHeadName = headDataList.get(i).long_name;
                    sessionManager.updateBookMark("MCQ", strHeadId, strSubjectId, getIntent().getStringExtra("subject_name"), new Gson().toJson(loadingType.equals("1") ? mcqHeadList : mcqExamArrayList), strChapterId, getIntent().getStringExtra("chapter_name"), headDataList.get(i).superHeadingName.equals("") ? "" : headDataList.get(i).superHeadingName, "", loadingType);

                    sessionManager.LastReadSubjectBookmark(sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                            sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                            sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME));
                }
            }
        }
        setCollapseingToolBar();
        setGroupAdapter(currentSelectedPosition);
        layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
        int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
        int centeredItemPosition = totalVisibleItems / 2;
        recyclerViewGroups.smoothScrollToPosition(currentSelectedPosition != 0 ? currentSelectedPosition - 1 : currentSelectedPosition);
        recyclerViewGroups.setScrollY(centeredItemPosition);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(currentSelectedPosition);
        tabLayout.setupWithViewPager(viewPager);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(viewPagerAdapter.getTabView(i));
            }
        }

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setGroupAdapter(position);
                layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
                int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                int centeredItemPosition = totalVisibleItems / 2;
                recyclerViewGroups.smoothScrollToPosition(position != 0 ? position - 1 : position);
                textView3.setText(headDataList.get(position).superHeadingName.equals("") ? "" : headDataList.get(position).superHeadingName);
                textView3Pin.setText(headDataList.get(position).superHeadingName.equals("") ? "" : headDataList.get(position).superHeadingName);
//        textView3.setVisibility(!headDataList.get(position).superHeadingName.equals("")?View.VISIBLE:View.GONE);
                textView3Pin.setVisibility(!headDataList.get(position).superHeadingName.equals("") ? View.VISIBLE : View.INVISIBLE);
                collapsing_toolbar_mcq.setTitle("");

                content_title_tv.setText(headDataList.get(position).long_name);
                article_title_tv.setText(headDataList.get(position).long_name);
                if (headDataList.get(position).superHeadingName != null && !headDataList.get(position).superHeadingName.equals(""))
                    superHeading.setText(headDataList.get(position).superHeadingName);
                else
                    superHeading.setText("");
                sessionManager.updateBookMark("MCQ", headDataList.get(position)._id, strSubjectId, getIntent().getStringExtra("subject_name"), new Gson().toJson(loadingType.equals("1") ? mcqHeadList : mcqExamArrayList), strChapterId, getIntent().getStringExtra("chapter_name"), headDataList.get(position).superHeadingName.equals("") ? "" : headDataList.get(position).superHeadingName, "", loadingType);

                sessionManager.LastReadSubjectBookmark(sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                        sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME),
                        sessionManager.getReadBookmarkDetails().get(SessionManager.READ_SUBJECT_NAME));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                if (position != 0 && position != headDataList.size() - 1) {
                    page.setAlpha(0f);
                    page.setVisibility(View.VISIBLE);

                    // Start Animation for a short period of time
                    page.animate()
                            .alpha(1f)
                            .setDuration(page.getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }
        });
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private void setGroupAdapter(int position) {

        if (homeGroupNameAdapter == null) {
            homeGroupNameAdapter = new ArticleTabsAdapter(headDataList, SubscriptionDetailActivity.this, position);
            layoutManager = new LinearLayoutManager(SubscriptionDetailActivity.this, RecyclerView.HORIZONTAL, false);
            layoutManager.setStackFromEnd(true);
            recyclerViewGroups.setLayoutManager(layoutManager);
            //snapHelper.attachToRecyclerView(recyclerViewGroups);
            recyclerViewGroups.setAdapter(homeGroupNameAdapter);

            recyclerViewGroups.scrollToPosition(position);

        } else {

            homeGroupNameAdapter.notifyPosition(position);
            scrollToCenter(layoutManager, recyclerViewGroups, position);

        }


        homeGroupNameAdapter.setOnGroupMenuSelectedListener(new OnMobileHomeMenuSelectedListioner() {
            @Override
            public void onGroupSelected(String groupId, int position) {
                viewPager.setCurrentItem(position);
                layoutManager = ((LinearLayoutManager) recyclerViewGroups.getLayoutManager());
                assert layoutManager != null;
                int totalVisibleItems = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();
                int centeredItemPosition = totalVisibleItems / 2;
                scrollToCenter(layoutManager, recyclerViewGroups, position);
            }
        });

    }


    public void scrollToCenter(LinearLayoutManager layoutManager, RecyclerView recyclerList, int clickPosition) {
        RecyclerView.SmoothScroller smoothScroller = createSnapScroller(recyclerList, layoutManager);

        if (smoothScroller != null) {
            smoothScroller.setTargetPosition(clickPosition);
            layoutManager.startSmoothScroll(smoothScroller);
        }
    }

    // This number controls the speed of smooth scroll
    private static final float MILLISECONDS_PER_INCH = 200f;

    private final static int DIMENSION = 2;
    private final static int HORIZONTAL = 0;
    private final static int VERTICAL = 1;

    @Nullable
    private LinearSmoothScroller createSnapScroller(RecyclerView mRecyclerView, final RecyclerView.LayoutManager layoutManager) {
        if (!(layoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider)) {
            return null;
        }
        return new LinearSmoothScroller(mRecyclerView.getContext()) {
            @Override
            protected void onTargetFound(View targetView, RecyclerView.State state, Action action) {
                int[] snapDistances = calculateDistanceToFinalSnap(layoutManager, targetView);
                final int dx = snapDistances[HORIZONTAL];
                final int dy = snapDistances[VERTICAL];
                final int time = calculateTimeForDeceleration(Math.max(Math.abs(dx), Math.abs(dy)));
                if (time > 0) {
                    action.update(dx, dy, time, mDecelerateInterpolator);
                }
            }


            @Override
            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
            }
        };
    }


    private int[] calculateDistanceToFinalSnap(@NonNull RecyclerView.LayoutManager layoutManager, @NonNull View targetView) {
        int[] out = new int[DIMENSION];
        if (layoutManager.canScrollHorizontally()) {
            out[HORIZONTAL] = distanceToCenter(layoutManager, targetView,
                    OrientationHelper.createHorizontalHelper(layoutManager));
        }

        if (layoutManager.canScrollVertically()) {
            out[VERTICAL] = distanceToCenter(layoutManager, targetView,
                    OrientationHelper.createHorizontalHelper(layoutManager));
        }
        return out;
    }


    private int distanceToCenter(@NonNull RecyclerView.LayoutManager layoutManager,
                                 @NonNull View targetView, OrientationHelper helper) {
        final int childCenter = helper.getDecoratedStart(targetView)
                + (helper.getDecoratedMeasurement(targetView) / 2);
        final int containerCenter;
        if (layoutManager.getClipToPadding()) {
            containerCenter = helper.getStartAfterPadding() + helper.getTotalSpace() / 2;
        } else {
            containerCenter = helper.getEnd() / 2;
        }
        return childCenter - containerCenter;
    }


    private void createSnap() {
        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                int targetPosition = -1;
                try {
                    View centerView = findSnapView(layoutManager);
                    if (centerView == null)
                        return RecyclerView.NO_POSITION;

                    int position = layoutManager.getPosition(centerView);

                    if (layoutManager.canScrollHorizontally()) {
                        if (velocityX < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    if (layoutManager.canScrollVertically()) {
                        if (velocityY < 0) {
                            targetPosition = position - 1;
                        } else {
                            targetPosition = position + 1;
                        }
                    }

                    final int firstItem = 0;
                    final int lastItem = layoutManager.getItemCount() - 1;
                    targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return targetPosition;
            }
        };
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        List<Fragment> fragmentList = new ArrayList<>();
        List<String> fragmentTitles = new ArrayList<>();
        Context mContext;
        TextView tabTitile;


        ViewPagerAdapter(FragmentManager fragmentManager,
                         FragmentActivity activity) {
            super(fragmentManager);
            mContext = activity;
        }

        @Override
        public Fragment getItem(int i) {
            return new SubscrptionDetailListFragment(strSubjectId, strChapterId, loadingType.equals("1") ? headDataList.get(i)._id : mcqExamArrayList.get(i).get_id(), loadingType.equals("1") ? headDataList.get(i).long_name : mcqExamArrayList.get(i).getLong_name(), loadingType);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitles.get(position);
        }

        void addFragment(Fragment fragment, String name) {
            fragmentList.add(fragment);
            fragmentTitles.add(name);
        }

        View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_items, null);
            tabTitile = v.findViewById(R.id.txtTabTitle);
            tabTitile.setText(fragmentTitles.get(position));
            tabTitile.setTextColor(getResources().getColor(R.color.white));
            return v;
        }
    }


    public static void expand(final View v) {
        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? WindowManager.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        int duration = (int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density);

        Log.e("collapseduration", String.valueOf(duration));

        // Expansion speed of 1dp/ms
        a.setDuration(150);
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        int duration = (int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density);

        Log.e("collapseduration", String.valueOf(duration));

        // Collapse speed of 1dp/ms
        a.setDuration(150);
        v.startAnimation(a);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (is_coming_page.equals("homepage")) {

            Intent s = new Intent(getApplicationContext(), TabMainActivity.class);
            s.putExtra("subjectId", strSubjectId);
            s.putExtra("subjectName", sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_NAME));
            s.putExtra("isTrial", sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_TRIAL_STATUS));
            s.putExtra("subjectShortName", sessionManager.getLastReadBookmarkDetails().get(SessionManager.LAST_READ_SUBJECT_SHORT_NAME));
            s.putExtra("showing_page_position", "0");
            startActivity(s);
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        } else {

            finish();
        }
    }

}
