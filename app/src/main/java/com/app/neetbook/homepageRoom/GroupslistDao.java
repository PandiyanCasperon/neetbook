package com.app.neetbook.homepageRoom;

import com.app.neetbook.Model.Groups;
import com.app.neetbook.Model.Groupslist;


import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface GroupslistDao {
//    @Query("Select * from Groups")
//    List<Groups> getroupsList();
//
//    @Insert
//    void insertGroupsList(Groups groupslist);
//
//    @Query("DELETE FROM Groups")
//    void nukeTable();
@Query("SELECT * FROM GroupsList")
List<Groupslist> getAll();

    @Query("SELECT COUNT(*) from GroupsList")
    int countCart();

    @Query("SELECT * FROM GroupsList WHERE homeGroupId == :id")
    public abstract Groupslist getGroupItem(String id);

    @Insert(onConflict = REPLACE)
    void insertAll(Groupslist... users);

    @Update
    void update(Groupslist repos);

    @Query("DELETE FROM GroupsList WHERE homeGroupId = :cartId")
    abstract void deleteByCartId(String cartId);

    @Delete
    void delete(Groupslist user);

    @Query("DELETE FROM GroupsList")
    public void nukeTable();
}
