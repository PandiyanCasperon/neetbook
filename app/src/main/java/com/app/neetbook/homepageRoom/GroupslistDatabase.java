package com.app.neetbook.homepageRoom;

import android.content.Context;

import com.app.neetbook.Model.Groups;
import com.app.neetbook.Model.Groupslist;
import com.app.neetbook.Subscriptionroom.SubscriptionGroupDao;
import com.app.neetbook.Subscriptionroom.SubscriptionGroupDatabase;

import androidx.room.Database;
import androidx.room.Entity;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = Groupslist.class,exportSchema =  false,version = 1)
@TypeConverters({Converters.class})
public abstract class GroupslistDatabase extends RoomDatabase {
    private static GroupslistDatabase INSTANCE;

    public abstract GroupslistDao groupslistDao();

    public static GroupslistDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), GroupslistDatabase.class, "HomeGroupDatabase")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
