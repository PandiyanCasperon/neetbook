package com.app.neetbook.homepageRoom

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * @author Naveen T P
 * @since 08/11/18
 */

@Dao
interface homePageDao {

    @Insert
    suspend fun saveTodo(todoRecord: homePageRecord)

    @Delete
    suspend fun deleteTodo(todoRecord: homePageRecord)

    @Update
    suspend fun updateTodo(todoRecord: homePageRecord)

    @Query("SELECT * FROM homemenu")
    fun getAllTodoList(): List<homePageRecord>
}