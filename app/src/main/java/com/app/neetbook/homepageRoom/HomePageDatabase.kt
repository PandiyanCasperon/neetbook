package com.app.neetbook.homepageRoom

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

/**
 * @author Naveen T P
 * @since 08/11/18
 */

@Database(entities = [homePageRecord::class], version = 1, exportSchema = false)
abstract class HomePageDatabase : RoomDatabase() {

    abstract fun homePageDao(): homePageDao

    companion object {
        private var INSTANCE: HomePageDatabase? = null

        fun getInstance(context: Context): HomePageDatabase? {
            if (INSTANCE == null) {
                synchronized(HomePageDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context,
                            HomePageDatabase::class.java,
                            "homemenu_db")
                            .build()
                }
            }
            return INSTANCE
        }
    }
}