package com.app.neetbook.homepageRoom;


import com.app.neetbook.Model.Groups;
import com.app.neetbook.Model.Groupslist;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import androidx.room.TypeConverter;

public class Converters implements Serializable {

    @TypeConverter // note this annotation
    public String fromOptionValuesList(List<Groupslist> optionValues) {
        if (optionValues == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Groupslist>>() {
        }.getType();
        String json = gson.toJson(optionValues, type);
        return json;
    }

    @TypeConverter // note this annotation
    public List<Groupslist> toOptionValuesList(String optionValuesString) {
        if (optionValuesString == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Groupslist>>() {
        }.getType();
        List<Groupslist> productCategoriesList = gson.fromJson(optionValuesString, type);
        return productCategoriesList;
    }
}
