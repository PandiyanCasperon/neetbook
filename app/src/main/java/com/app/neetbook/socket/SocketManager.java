package com.app.neetbook.socket;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.app.neetbook.ReceviceMessageEvent;
import com.app.neetbook.Utils.CustomAlert;
import com.app.neetbook.Utils.MyCustomEvent;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.Utils.alerter.AlertBox;
import com.app.neetbook.serviceRequest.IConstant;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.HashSet;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class SocketManager {
    private Activity activity;
    private SocketCallBack callBack;
    public static final String EVENT_NEW_MESSAGE = "notification";

    public static final String EVENT_JOINNEET = "join_neetbook";
    public static final String EVENT_LOGOUT = "logout_intimate";

    private HashSet<Object> uniqueBucket;
    public static boolean isConnected;
    private SocketConnectCallBack location;
    public static final String JOIN_NETWORK = "join network";
    public static final String NETWORK_CREATED = "network created";
    public static final String ROOM_STRING_SWITCH = "switch room";
    public static final String EVENT_LOCATION = "tasker tracking";
    public static final String EVENT_TASKER_LOCATION_UPDATE = "usermap_tracking";
    public static final String EVENT_AVILABILITY_UPDATE = "availability_tracking";
    public static final String EVENT_SESSION_LOGOUT = "session_check";
    private Context context;
    SessionManager session;
    public static String Userid = "";
    private String sTaskID = "";

    public static interface SocketCallBack {
        void onSuccessListener(String eventName,Object response);
    }

    public static interface SocketConnectCallBack {
        void onSuccessListener(Object response);
    }

    public SocketManager(Activity activity, SocketConnectCallBack location) {
        this.activity = activity;
        this.location = location;
    }

    public SocketManager(Context activity) {
        this.context = activity;
    }


    public SocketManager(SocketCallBack callBack, Context mContext) {
        this.callBack = callBack;
        this.context = mContext;
        session = new SessionManager(context);

    }



    private Socket mSocket;

    {
        try {
            mSocket = IO.socket(IConstant.SOCKET_HOST_URL);
            mSocket.io().reconnection(true);
        } catch (URISyntaxException e) {
        }
    }

    public void connect() {
        Log.d("SOCKET MANAGER", "Connecting..");
        try {
            if (session != null) {
                if (session.isLoggedIn()) {
                    //mSocket.off(EVENT_NEW_MESSAGE, onNewMessage);
                    mSocket.off(EVENT_TASKER_LOCATION_UPDATE, onTaskerLocation_Update);
                    // mSocket.on(EVENT_NEW_MESSAGE, onNewMessage);

                    mSocket.on(Socket.EVENT_CONNECT, onConnectMessage);
                    mSocket.on(Socket.EVENT_DISCONNECT, onDisconnectMessage);
                    mSocket.on(NETWORK_CREATED, onNetworkJoinListener);
                    mSocket.on(EVENT_TASKER_LOCATION_UPDATE, onTaskerLocation_Update);
                    mSocket.connect();
                }
            }


        } catch (Exception e) {
        }
    }

    public boolean getconnected() {
        if (mSocket != null) {
            return mSocket.connected();
        }
        return false;
    }

    public boolean ConnectionChecking() {

        boolean connection = mSocket.connected();

        return connection;
    }

    private Emitter.Listener onNetworkJoinListener = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            Log.d("SOCKET MANAGER", "Joined To Network");

        }
    };


    private Emitter.Listener onTaskerLocation_Update = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d("SOCKET MANAGER", "onTaskerLocation_Update = " + args[0]);
            if (callBack != null) {
                callBack.onSuccessListener("onTaskerLocation_Update",args[0]);
            }
        }
    };


    private Emitter.Listener availability_update = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d("SOCKET MANAGER", "onTaskerLocation_Update = " + args[0]);
            if (callBack != null) {
                callBack.onSuccessListener("availability_update",args[0]);
            }
        }
    };


    private Emitter.Listener onupdatelocation = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.d("SOCKET MANAGER", "onUpdateLocation = " + args[0]);

            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (callBack != null) {
                            callBack.onSuccessListener("onupdatelocation",args[0]);
                        }
                    }
                });
            } else {
                if (callBack != null) {
                    callBack.onSuccessListener("onupdatelocation",args[0]);
                }
            }
        }
    };


    private Emitter.Listener sessionlogout = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.d("SOCKET MANAGER", "onUpdateLocation = " + args[0]);

            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (callBack != null) {
                            callBack.onSuccessListener("sessionlogout",args[0]);
                        }
                    }
                });
            } else {
                if (callBack != null) {
                    callBack.onSuccessListener("sessionlogout",args[0]);
                }
            }
        }
    };


    private Emitter.Listener onConnectMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d("SOCKET MANAGER", "Connected" + Userid);
            JSONObject object = new JSONObject();

                try {
                    object.put("user", Userid);
                    mSocket.off(EVENT_NEW_MESSAGE, onNewMessage);
                    mSocket.on(EVENT_NEW_MESSAGE, onNewMessage);

                    mSocket.off(EVENT_JOINNEET, onJoinNeet);
                    mSocket.on(EVENT_JOINNEET, onJoinNeet);

                    mSocket.off(EVENT_LOGOUT, onLogout);
                    mSocket.on(EVENT_LOGOUT, onLogout);

                    mSocket.off(EVENT_AVILABILITY_UPDATE, availability_update);
                    mSocket.on(EVENT_AVILABILITY_UPDATE, availability_update);

                    mSocket.off(EVENT_LOCATION, onupdatelocation);
                    mSocket.on(EVENT_LOCATION, onupdatelocation);

                    mSocket.off(EVENT_SESSION_LOGOUT, sessionlogout);
                    mSocket.on(EVENT_SESSION_LOGOUT, sessionlogout);

                    } catch (JSONException e) {
                    e.printStackTrace();
                }
                isConnected = true;
        }
    };

    private Emitter.Listener onDisconnectMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d("SOCKET MANAGER", "DISCONNECTED");
            isConnected = false;
        }
    };

    public void disconnect() {
        try {
            mSocket.off(EVENT_NEW_MESSAGE, onNewMessage);
            // mSocket.off(EVENT_TASKER_LOCATION_UPDATE,onTaskerLocation_Update);
            mSocket.disconnect();
        } catch (Exception e) {
        }
    }

    public void sendMessage(String message) {
        if (TextUtils.isEmpty(message)) {
            return;
        }
        mSocket.emit(EVENT_NEW_MESSAGE, message);
    }
    public void onJoinNeet(Object message) {

        mSocket.emit(EVENT_JOINNEET, message);
    }


    private Emitter.Listener    onJoinNeet = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            Log.e("SOCKET MANAGER", "onJoinNeet = " + args[0]);

            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (callBack != null) {
                            callBack.onSuccessListener("onJoinNeet",args[0]);
                        }
                    }
                });
            } else {
                if (callBack != null) {
                    callBack.onSuccessListener("onJoinNeet",args[0]);
                }
            }
        }
    };
    private Emitter.Listener    onLogout = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
       //     EventBus.getDefault().post(new MyCustomEvent(""+args[0]));
            Log.e("SOCKET MANAGER", "onLogout = " + args[0]);
            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                   //     new CustomAlert(activity).showAlert("Logout","message");
                        if (callBack != null) {
                            callBack.onSuccessListener("onLogout",args[0]);
                        }
                    }
                });
            } else {
                if (callBack != null) {
                    callBack.onSuccessListener("onLogout",args[0]);
                }
            }
        }
    };
    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.d("SOCKET MANAGER", "onNewMessage = " + args[0]);

            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (callBack != null) {
                            callBack.onSuccessListener("onNewMessage",args[0]);
                        }
                    }
                });
            } else {
                if (callBack != null) {
                    callBack.onSuccessListener("onNewMessage",args[0]);
                }
            }
        }
    };

    public void createRoom(Object userID) {
        if (userID == null) {
            return;
        }
        if (mSocket != null) {
            mSocket.emit(EVENT_JOINNEET, userID);
        }

    }


    public void createSwitchRoom(String userID) {
        if (TextUtils.isEmpty(userID)) {
            return;
        }
        if (mSocket != null) {
            mSocket.emit(ROOM_STRING_SWITCH, userID);
        }

    }
}
