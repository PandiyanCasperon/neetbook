package com.app.neetbook.socket;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Messenger;

import com.app.neetbook.Interfaces.MyListener;
import com.app.neetbook.Utils.SessionManager;

import org.json.JSONObject;

import java.util.ArrayList;

public class SocketHandler {
    private Context context;
    private SocketManager manager;

    private static SocketHandler instance;
    private ArrayList<Messenger> listenerMessenger;
    private SessionManager sessionManager;
    MyListener myListener;

    private SQLiteDatabase dataBase;
    public static String user;
    private SocketHandler(Context context) {
        this.context = context;
        this.manager = new SocketManager(callBack,context);


        this.listenerMessenger = new ArrayList<Messenger>();
        sessionManager=new SessionManager(context);
    }

    public static SocketHandler getInstance(Context context) {
        if (instance == null) {
            instance = new SocketHandler(context);
        }
        return instance;
    }

    public void addChatListener(Messenger messenger) {
        listenerMessenger.add(messenger);
    }

    public SocketManager getSocketManager() {


        return manager;
    }

    public SocketManager.SocketCallBack callBack = new SocketManager.SocketCallBack() {
        boolean isChat = false;

        @Override
        public void onSuccessListener(String eventname,Object response) {


            if (response instanceof JSONObject) {
                JSONObject jsonObject = (JSONObject) response;
                System.out.println("SocketHandler -----------SocketHandlerjsonObject------------"+jsonObject);

                myListener.callback(response);


            }
        }
    };

}
