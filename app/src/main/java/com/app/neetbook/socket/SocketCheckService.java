package com.app.neetbook.socket;

import android.app.ActivityManager;
import android.app.Service;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;

import com.app.neetbook.Utils.ConnectionDetector;
import com.app.neetbook.Utils.SessionManager;
import com.app.neetbook.View.SplashActivity;
import com.app.neetbook.serviceRequest.ServiceRequest;

import java.util.HashMap;
import java.util.List;

import androidx.annotation.RequiresApi;


@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class SocketCheckService  extends JobService {
    private Context myContext;
    private String UserID = "";
    private SessionManager session;
    private ServiceRequest mRequest;
    private SocketHandler socketHandler;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;

    @Override
    public void onCreate() {
        super.onCreate();
        myContext = getApplicationContext();
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        socketHandler = SocketHandler.getInstance(this);
        session = new SessionManager(myContext);

        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_UID);

        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isInternetPresent = cd.isConnectingToInternet();
                SocketCheckMethod();
                handler.postDelayed(this,1000);
            }
        },1000);
    }

    private void SocketCheckMethod() {

        if(session.isLoggedIn()) {

            if(!isApplicationToBackground()){

                if (!socketHandler.getSocketManager().isConnected && isInternetPresent) {
                    socketHandler.getSocketManager().connect();
                }
            }


        }
    }
    public boolean  getConnected(){
        return socketHandler.getSocketManager().getconnected();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //TODO do something useful

        return Service.START_STICKY;
    }

    private boolean isApplicationToBackground() {

        try {

            ActivityManager am = (ActivityManager) myContext.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
            if (!tasks.isEmpty()) {
                ComponentName topActivity = tasks.get(0).topActivity;
                if (!topActivity.getPackageName().equals(myContext.getPackageName())) {
                    return true;
                }
            }

        } catch (Exception e) {

        }
        return false;
    }






    @Override
    public boolean onStartJob(JobParameters params) {
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Intent service = new Intent(SocketCheckService.this, SocketCheckService.class);
        startService(service);
        return false;
    }
}
