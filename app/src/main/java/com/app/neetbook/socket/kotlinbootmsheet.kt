package com.app.neetbook.socket

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.app.neetbook.R
import com.google.android.material.bottomsheet.BottomSheetDialog

class kotlinbootmsheet : AppCompatActivity()
{

    var bottomSheetDialog: BottomSheetDialog?=null
    var sTitle:String=""
    var sMessage:String=""
    var action:String=""

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pushnotification_alert)

        val intent = intent
        sTitle = intent.getStringExtra("title").toString()
        sMessage = intent.getStringExtra("message").toString()
        action = intent.getStringExtra("action").toString()
        if(bottomSheetDialog != null)
        {
            bottomSheetDialog!!.dismiss()
        }
        openmysheet(sTitle,sMessage,action)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        sTitle = intent!!.getStringExtra("title").toString()
        sMessage = intent!!.getStringExtra("message").toString()
        action = intent!!.getStringExtra("action").toString()
        // Toast.makeText(applicationContext,sTitle,Toast.LENGTH_LONG).show()
        if(bottomSheetDialog != null)
        {
            bottomSheetDialog!!.dismiss()
        }
        openmysheet(sTitle,sMessage,action)
    }

    fun openmysheet(sTitle:String,sMessage:String,action:String)
    {
        bottomSheetDialog =  BottomSheetDialog(this,R.style.SheetDialog);
        val view = getLayoutInflater().inflate(R.layout.notiifcationsheet, null);
        bottomSheetDialog!!.setContentView(view)
        bottomSheetDialog!!.setCancelable(false)

        val heading = view.findViewById(R.id.heading) as TextView
        val message = view.findViewById(R.id.message) as TextView
        val donebutton = view.findViewById(R.id.donebutton) as TextView
        val closebutton = view.findViewById(R.id.closebutton) as TextView

        heading.text = sTitle
        message.text = sMessage

        closebutton.setOnClickListener {
            bottomSheetDialog!!.dismiss()
            finish()
        }
        donebutton.setOnClickListener {
            bottomSheetDialog!!.dismiss()

        }

        bottomSheetDialog!!.show()
    }

}