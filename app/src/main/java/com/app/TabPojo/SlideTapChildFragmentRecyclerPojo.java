package com.app.TabPojo;

public class SlideTapChildFragmentRecyclerPojo {

    private String Question_no="";
    private String Question_Title="";
    private String Question_answer="";

    public String getQuestion_no() {
        return Question_no;
    }

    public void setQuestion_no(String question_no) {
        Question_no = question_no;
    }

    public String getQuestion_Title() {
        return Question_Title;
    }

    public void setQuestion_Title(String question_Title) {
        Question_Title = question_Title;
    }

    public String getQuestion_answer() {
        return Question_answer;
    }

    public void setQuestion_answer(String question_answer) {
        Question_answer = question_answer;
    }
}
