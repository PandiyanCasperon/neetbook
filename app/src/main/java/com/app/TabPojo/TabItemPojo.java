package com.app.TabPojo;

import java.util.List;

public class TabItemPojo {

    private String Question="";
    private Boolean is_selection=false;
    private List child_list;

    public List getChild_list() {
        return child_list;
    }

    public void setChild_list(List child_list) {
        this.child_list = child_list;
    }

    public Boolean getIs_selection() {
        return is_selection;
    }

    public void setIs_selection(Boolean is_selection) {
        this.is_selection = is_selection;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }


}
