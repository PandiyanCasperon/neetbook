package com.app.TabPojo;

public class EventbusPojo {

    private String fragment_name="";

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumOfquestion() {
        return numOfquestion;
    }

    public void setNumOfquestion(String numOfquestion) {
        this.numOfquestion = numOfquestion;
    }

    public String getIsFinished() {
        return isFinished;
    }

    public void setIsFinished(String isFinished) {
        this.isFinished = isFinished;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getLongname() {
        return longname;
    }

    public void setLongname(String longname) {
        this.longname = longname;
    }

    public String getC_a_m() {
        return c_a_m;
    }

    public void setC_a_m(String c_a_m) {
        this.c_a_m = c_a_m;
    }

    public String getW_a_m() {
        return w_a_m;
    }

    public void setW_a_m(String w_a_m) {
        this.w_a_m = w_a_m;
    }

    private String type="";
    private String numOfquestion="";
    private String isFinished="";
    private String questionType="";
    private String instruction="";
    private String longname="";
    private String c_a_m="";
    private String w_a_m="";

    public String getMcqLoadingType() {
        return mcqLoadingType;
    }

    public void setMcqLoadingType(String mcqLoadingType) {
        this.mcqLoadingType = mcqLoadingType;
    }

    private String mcqLoadingType="";

    public String getHeadId() {
        return headId;
    }

    public void setHeadId(String headId) {
        this.headId = headId;
    }

    private String headId="";

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getChapterId() {
        return chapterId;
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public String getIsSuperHeading() {
        return isSuperHeading;
    }

    public void setIsSuperHeading(String isSuperHeading) {
        this.isSuperHeading = isSuperHeading;
    }

    public String getSuperHeadingName() {
        return superHeadingName;
    }

    public void setSuperHeadingName(String superHeadingName) {
        this.superHeadingName = superHeadingName;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    private String chapterName="";
    private String chapterId="";
    private String isSuperHeading="";
    private String superHeadingName="";
    private String articleId="";

    public String getFragment_name() {
        return fragment_name;
    }

    public void setFragment_name(String fragment_name) {
        this.fragment_name = fragment_name;
    }
}
