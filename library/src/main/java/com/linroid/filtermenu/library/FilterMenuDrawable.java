package com.linroid.filtermenu.library;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.util.Log;

/**
 * Created by linroid on 15/3/10.
 */
public class FilterMenuDrawable extends Drawable {
    private Context ctx;
    private Paint paint;
    private IconState state = IconState.COLLAPSED;
    private int radius;

    private int lineWidth = 6;
    private float expandProgress = 0;

    public FilterMenuDrawable(Context ctx, int color, int radius) {
        this.ctx = ctx;
        this.radius = radius;

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(color);
        paint.setStrokeWidth(lineWidth);

    }
    public enum IconState{
        COLLAPSED,
        EXPANDED
    }

    public float getExpandProgress() {
        return expandProgress;
    }

    public void setExpandProgress(float expandProgress) {
        this.expandProgress = expandProgress;
        invalidateSelf();
    }

    @Override
    public int getIntrinsicWidth() {
        return (int) (0.0f);
    }

    @Override
    public int getIntrinsicHeight() {
        return (int) (0.0f);
    }

    @Override
    public void draw(Canvas canvas) {
        //draw three line
//        paint.setColor(Color.BLACK);
//        canvas.drawRect(getBounds(), paint);
//        paint.setColor(Color.WHITE);
        if(expandProgress<=0.5f){

            /*drawTopLine(canvas, expandProgress);
            drawMiddleLine(canvas, expandProgress);
            drawBottomLine(canvas, expandProgress);*/

   /*         Paint paint1 = new Paint();
            paint1.setColor(Color.GREEN);
            Resources res = ctx.getResources();
            Bitmap bitmap = BitmapFactory.decodeResource(res, R.drawable.filter);
            int left_value=getBounds().left;
            int top_value=getBounds().top;
            int intr_value=getIntrinsicWidth();
            int final_value=top_value-intr_value;
          //  canvas.drawBitmap(bitmap, getBounds().left-20, final_value+24, paint1);
            float density = ctx.getResources().getDisplayMetrics().density;
            float widthPixels = ctx.getResources().getDisplayMetrics().widthPixels;
            float heightPixels = ctx.getResources().getDisplayMetrics().heightPixels;
            float xdpi = ctx.getResources().getDisplayMetrics().xdpi;
            float densityDpi = ctx.getResources().getDisplayMetrics().densityDpi;
            Log.e("density","density "+density);
            Log.e("density1","widthPixels "+widthPixels);
            Log.e("density2","heightPixels "+heightPixels);
            Log.e("density3","xdpi "+xdpi);
            Log.e("density4","densityDpi "+densityDpi);
            canvas.drawBitmap(bitmap, getBounds().left+5, final_value+5, paint1);*/

            /*if (heightPixels<=1180 && xdpi<=420 ){
                canvas.drawBitmap(bitmap, getBounds().left-10, final_value+12, paint1);
            }
            else if (heightPixels==1198 ){
                canvas.drawBitmap(bitmap, getBounds().left-10, final_value+16, paint1);
            } else if (heightPixels<=800 && xdpi<=220 ){
                canvas.drawBitmap(bitmap, getBounds().left-10, final_value+10, paint1);
            }
            else if (heightPixels<=2960 ){
                canvas.drawBitmap(bitmap, getBounds().left-20, final_value+18, paint1);
            }
            else if (heightPixels<=720 && xdpi<=260 ){
                canvas.drawBitmap(bitmap, getBounds().left-20, final_value+14, paint1);
            }
            else if (heightPixels<=2100 && xdpi<=420 ){
                canvas.drawBitmap(bitmap, getBounds().left-20, final_value+24, paint1);
            }
            else if (heightPixels<=1280  ){
                canvas.drawBitmap(bitmap, getBounds().left-20, final_value+24, paint1);
            } else if (heightPixels<=1080){
                canvas.drawBitmap(bitmap, getBounds().left-20, final_value+24, paint1);
            }
            else {
                canvas.drawBitmap(bitmap, getBounds().left-20, final_value+24, paint1);
            }*/
        // draw cancel
        }else{

            drawTopLeftLine(canvas, expandProgress);
            drawBottomLeftLine(canvas, expandProgress);
        }
    }

    private void drawBottomLeftLine(Canvas canvas, float progress) {
        int ly = (int) (getBounds().bottom-getIntrinsicHeight()*progress);
        int ry = (int) (getBounds().top+ getIntrinsicHeight()*progress);
        canvas.drawLine(getBounds().left, ly, getBounds().right, ry, paint);
    }

    private void drawTopLeftLine(Canvas canvas, float progress) {
        int ry = (int) (getBounds().bottom-getIntrinsicHeight()*progress);
        int ly = (int) (getBounds().top+ getIntrinsicHeight()*progress);
        canvas.drawLine(getBounds().left, ly, getBounds().right, ry, paint);
    }


    private void drawTopLine(Canvas canvas, float progress) {
        int y = getBounds().top + (int) (getIntrinsicHeight()* progress) + lineWidth;
        canvas.drawLine(getBounds().left, y, getBounds().left+getIntrinsicWidth(), y, paint);
    }

    private void drawMiddleLine(Canvas canvas, float progress) {
        int y = getBounds().top + getIntrinsicHeight() / 2;
        int len = getIntrinsicWidth() /2;
        int centerX = getBounds().centerX();
        canvas.drawLine(centerX-len/2, y, centerX+len/2, y, paint);
    }
    private void drawBottomLine(Canvas canvas, float progress) {
        int y = getBounds().top + (int) (getIntrinsicHeight() * (1-progress)) - lineWidth;
        int len = getIntrinsicWidth() /4;
        int centerX = getBounds().centerX();
        canvas.drawLine(centerX-len/2, y, centerX+len/2, y, paint);
    }

    @Override
    public void setAlpha(int alpha) {
        paint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        paint.setColorFilter(cf);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }
}
